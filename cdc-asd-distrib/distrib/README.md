# CONTENT
- `lib`: the necessary jars
- `doc`: available documentation
- `bin`: scripts (currently for Windows)
- `example-asd-models-comparator.cmd`: example of usage of the asd-suite tool
- `example-asd-suite.cmd`: example of usage of the asd-(models-comparator tool
- `README.md`: this file

The example files must be adapted to your data

# INSTALLATION
No JRE (Java Runtime Environment) is provided. A JRE (>= Java 17) must be installed.  
Edit `bin/cdc-asd-setup.cmd` and modify or comment those 2 lines:  
set JRE_HOME=...  
set PATH=%JRE_HOME%\bin;%PATH%

# DOCUMENTATION
- See `doc/cdc-asd-README.pdf` for global explanations on available tools.
- See `doc/AsdSuite.pdf` for explanations on `asd-suite` (conversion and analysis of an EAP model)
- See `doc/AsdModelsComparator.pdf` for explanations on `asd-models-comparator` (comparison of XML ASD models)