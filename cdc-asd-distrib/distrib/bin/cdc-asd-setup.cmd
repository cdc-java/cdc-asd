@echo OFF

if "%CDC_ASD_SETUP_DONE%"=="1" goto skip_setup
set CDC_ASD_SETUP_DONE=1

:: Set console encoding to UTF8
@echo Set UTF8
@chcp 65001

:: Set these 2 lines if no appropriate Java (>= 1.17) is available
set JRE_HOME=C:\Temp\SOFT\jre-21+35
set PATH=%JRE_HOME%\bin;%PATH%

set CDC_ASD=%~dp0..
set CDC_ASD_LIB=%CDC_ASD%\lib
set CDC_ASD_BIN=%CDC_ASD%\bin

:: There is an issue when Saxon is used with PlantUML
set CDC_ASD_FIX_XML=-Djavax.xml.transform.TransformerFactory=com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl
set CDC_ASD_LOG4J=-Dlog4j2.configurationFile="%CDC_ASD_LIB%\log4j2.xml"

:: Default command arguments
set CDC_ASD_CMD_ARGS=--help-width 132
set CDC_ASD_JAVA_ARGS=-ea -Xmx4000M %CDC_ASD_LOG4J% %CDC_ASD_FIX_XML% -cp "%CDC_ASD_LIB%\*"
:skip_setup