@echo OFF

:: Example of cdc-asd-suite usage

:: Output directory that will contain generated files
set CDC_OUTPUT_DIR="output"

:: EAP file to analyse
set CDC_EAP_FILE="L:/D. Carbonne/S-Series/S6000T_3-0_Data_model_000-09.EAP"

set CDC_IGNORE_PACKAGES=
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% "/Builtin"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/S.Series_Compound_Attributes.*"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/S.Series_Base_Object_Definition.*"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/S.Series_Primitives.*"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM UoF Remark"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM UoF Digital File"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM UoF Security Classification"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM UoF Applicability Statement"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM Remark"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM Digital File"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM Security Classification"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM Applicability Statement"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM Project Specific Attribute"
set CDC_IGNORE_PACKAGES=%CDC_IGNORE_PACKAGES% ".*/CDM Organization"

set CDC_REMOVE_NAME_PARTS=
set CDC_REMOVE_NAME_PARTS=%CDC_REMOVE_NAME_PARTS% "CDM "

set CDC_COMMON_OPTIONS=--verbose --output-dir %CDC_OUTPUT_DIR% --append-basename 
set CDC_PROFILE_OPTIONS=--profile-all
set CDC_DUMP_OPTIONS=--dump-no-force
set CDC_MODEL_OPTIONS=--model-fix-direction
set CDC_CLEAN_OPTIONS=--clean-deduplicate-names
::--clean-replace-html-entities --clean-remove-html-tags
set CDC_CHECK_OPTIONS=
set CDC_OFFICE_OPTIONS=
set CDC_HTML_OPTIONS=--html-flat-dirs --html-img-svg --html-img-show-ids --html-show-ids --html-show-guids --html-show-indices --html-sort-tags --html-model-overview-ignore-package %CDC_IGNORE_PACKAGES% --html-name-remove-part %CDC_REMOVE_NAME_PARTS%
set CDC_S1000D_OPTIONS=--s1000d-glossary --s1000d-spec
set CDC_OPTIONS=%CDC_COMMON_OPTIONS% %CDC_PROFILE_OPTIONS% %CDC_DUMP_OPTIONS% %CDC_MODEL_OPTIONS% %CDC_CLEAN_OPTIONS% %CDC_CHECK_OPTIONS% %CDC_OFFICE_OPTIONS% %CDC_HTML_OPTIONS% %CDC_S1000D_OPTIONS% 

call bin\cdc-asd-suite %CDC_OPTIONS% --run-all --eap-file %CDC_EAP_FILE%

pause