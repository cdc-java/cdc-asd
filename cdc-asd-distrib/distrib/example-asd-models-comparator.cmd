@echo OFF

:: Example of cdc-asd-models-comparator usage

:: Output directory that will contain generated files
set CDC_OUTPUT_DIR="output"

:: Currently the output directory is not created by cdc-asd-models-comparator
mkdir %CDC_OUTPUT_DIR%

:: Generated file
set CDC_OUTPUT=%CDC_OUTPUT_DIR%/comparison.xlsx

:: List of XML models to compare
set CDC_MODELS=
set CDC_MODELS=%CDC_MODELS% "L:/D. Carbonne/S-Series/output/cdc-asd-0.18.0/s3000l-2.0.001.00/s3000l-2.0.001.00-model.xml"
set CDC_MODELS=%CDC_MODELS% "L:/D. Carbonne/S-Series/output/cdc-asd-0.18.0/s3000l-2.0.002.00/s3000l-2.0.002.00-model.xml"

:: Use --diff-id to compare elements based on EAP identifiers
set CDC_OPTIONS=--verbose --output %CDC_OUTPUT% --model %CDC_MODELS% --best --show-all --diff-name

call bin\cdc-asd-models-comparator %CDC_OPTIONS%

pause