#!/bin/sh

# Converts MD files using Chromium in headless mode.
# Internet access is used

generatePDF()
{
   targetDir=../../target/distrib/doc
   mkdir -p $targetDir
   source=$1
   target=$2
   echo "$source --> $target"
   chromium --headless --disable-gpu --run-all-compositor-stages-before-draw --no-pdf-header-footer --virtual-time-budget=10000 --print-to-pdf=$targetDir/$target $source
} 


generateSimplePDF() {
   generatePDF https://gitlab.com/cdc-java/$1/-/blob/main/$2.md?ref_type=heads $2.pdf
}

generatePrefixedPDF() {
   generatePDF https://gitlab.com/cdc-java/$1/-/blob/main/$2.md?ref_type=heads $1-$2.pdf
}

generateSimplePDF cdc-mf EaDumpToMf
generateSimplePDF cdc-mf EaProfileExporter
generateSimplePDF cdc-mf EapDumper
generateSimplePDF cdc-mf MfToHtml
generateSimplePDF cdc-mf MfToOffice

generatePrefixedPDF cdc-mf CHANGELOG
generatePrefixedPDF cdc-mf README

generateSimplePDF cdc-asd AsdCdcMappingExporter
generateSimplePDF cdc-asd AsdModelChecker
generateSimplePDF cdc-asd AsdModelCleaner
generateSimplePDF cdc-asd AsdModelToS1000D
generateSimplePDF cdc-asd AsdModelsComparator
generateSimplePDF cdc-asd AsdProfileExporter
generateSimplePDF cdc-asd AsdSuite
generateSimplePDF cdc-asd XsdAnalyzer

generatePrefixedPDF cdc-asd CHANGELOG
generatePrefixedPDF cdc-asd README

