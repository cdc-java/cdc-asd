# AsdSuite

`AsdSuite` can run several ASD tools in a cosnstent way, from EAP database dump to S1000D generation.


## AsdSuite options
Options of `AsdSuite `are:

````
USAGE
AsdSuite [--append-basename] [--args-file <arg>] [--args-file-charset <arg>] [--basename <arg>] [--check-disable <arg> |
               --check-enable <arg>] [--check-disable-all | --check-enable-all]   [--check-profile-config <arg>] [--check-ref-model
               <arg>] [--clean-add-missing-xml-name-tags] [--clean-add-missing-xml-ref-name-tags] [--clean-all]
               [--clean-create-base-object-inheritance] [--clean-create-enumerations] [--clean-deduplicate-names]
               [--clean-fix-tag-name-case] [--clean-remove-extra-spaces] [--clean-remove-html-tags] [--clean-replace-html-entities]
               [--clean-split-ref-tags] [--dump-all | --dump-min] [--dump-csv | --dump-xlsx] [--dump-force | --dump-no-force]
               --eap-file <arg> [-h | -v] [--help-width <arg>] [--html-fail | --html-no-fail] [--html-flat-dirs] [--html-force |
               --html-no-force] [--html-frames | --html-single-page] [--html-img-colors | --html-img-no-colors]
               [--html-img-hide-all-metas | --html-img-show-all-metas] [--html-img-hide-all-tags | --html-img-show-all-tags]
               [--html-img-hide-meta <arg> | --html-img-show-meta <arg>] [--html-img-hide-tag <arg> | --html-img-show-tag <arg>]
               [--html-img-max-tag-values <arg>]  [--html-img-no-shadows | --html-img-shadows] [--html-img-png | --html-img-svg]
               [--html-img-tag-value-max-length <arg>] [--html-layout-depth <arg>] [--html-ltor-threshold <arg>]
               [--html-model-overview-ignore-package <arg>] [--html-name-remove-part <arg>]   [--html-no-parallel | --html-parallel]
               [--html-show-ids] [--html-show-non-navigable-tips]  [--html-sort-tags] [--model-author <arg>] [--model-fix-direction
               | --model-no-fix-direction] [--model-name <arg>]  [--no-run-asd-cdc-mapping-exporter] [--no-run-asd-profile-exporter
               | --run-asd-profile-exporter] [--no-run-ea-profile-exporter | --run-ea-profile-exporter] [--no-run-eap-dumper |
               --run-eap-dumper] [--no-run-html-generator | --run-html-generator] [--no-run-model-checker | --run-model-checker]
               [--no-run-model-cleaner | --run-model-cleaner] [--no-run-model-generator | --run-model-generator]
               [--no-run-office-generator | --run-office-generator] [--no-run-s1000d-generator | --run-s1000d-generator]
               [--no-run-xsd-generator | --run-xsd-generator] [--office-best | --office-fastest]  --output-dir <arg> [--profile-all]
               [--profile-config-xlsx] [--profile-html] [--profile-md] [--profile-show-empty-sections] [--profile-show-rules-domain]
               [--profile-show-rules-enabling] [--profile-show-rules-severities] [--profile-xlsx] [--profile-xml] [--run-all]
               [--run-asd-cdc-mapping-exporter]           [--s1000d-glossary] [--s1000d-spec]  [--verbose] [--xsd-copyright <arg>]
               [--xsd-issue-date <arg>] [--xsd-issue-number <arg>] [--xsd-namespace <arg>] [--xsd-no-parallel | --xsd-parallel]
               [--xsd-specification <arg>] [--xsd-specification-url <arg>] [--xsd-valid-values-external-libraries <arg>]
               [--xsd-valid-values-libraries <arg>] [--xsd-valid-values-units <arg>] [--xsd-xml-schema-release-date <arg>]
               [--xsd-xml-schema-release-number <arg>]

Utility that can run several tools, in that order:
  - EapDumper
  - EaDumpToMf
  - AsdModelCleaner
  - AsdModelChecker
  - MfToOffice
  - MfToHtml
  - AsdModelToS1000D
  - AsdModelToXsd
A tool is executed if the corresponding '--run-xxx' option is enabled.

The following naming conventions are applied:
  - The EAP dumps are located in ${outputDir}/eap
  - The model file is ${outputDir}/${basename}-model.xml
  - The cleaned model file is ${outputDir}/${basename}-cleaned-model.xml
  - The model issues file is ${outputDir}/${basename}-model-issues.???
  - The HTML report is located in ${outputDir}/html
  - The S1000D data modules are located in ${outputDir}/s1000d
  - The XSD files are located in ${outputDir}/xsd

OPTIONS
    --append-basename                             Append basename to output directory.
    --args-file <arg>                             Optional name of the file from which options can be read.
                                                  A line is either ignored or interpreted as a single argument (option or value).
                                                  A line is ignored when it is empty or starts by any number of white spaces
                                                  followed by '#'.
                                                  A line that only contains white spaces is an argument.
                                                  A comment starts by a '#' not following a '\'. The "\#" sequence is read as '#'.
    --args-file-charset <arg>                     Optional name of the args file charset.
                                                  It may be used if args file encoding is not the OS default file encoding.
    --basename <arg>                              Optional base name of generated files. If missing, it is automatically deduced
                                                  from the EAP file name.
    --check-disable <arg>                         Name(s) of rule checkers to disable.
    --check-disable-all                           Disable all rules checkers. Use with --check-enable to enable individual rule
                                                  checkers.
    --check-enable <arg>                          Name(s) of rule checkers to enable.
    --check-enable-all                            Enable all rules checkers. Use with --check-disable to disable individual rule
                                                  checkers.
    --check-profile-config <arg>                  Optional name of the ASD Profile Config file to load.
    --check-ref-model <arg>                       Optional name of the reference XML ASD MF model to check against.
                                                  Used by some checkers as a reference to compare the checked model.
    --clean-add-missing-xml-name-tags             Add xmlName tags that are missing.
    --clean-add-missing-xml-ref-name-tags         Add xmlRefName tags that are missing.
    --clean-all                                   Enable all cleaning options.
    --clean-create-base-object-inheritance        Make BaseObject inheritance explicit.
                                                  This does not fix the data model but can help reviewing it.
                                                  DO NOT USE with checks.
    --clean-create-enumerations                   Create enumerations for properties that have validValue tags.
                                                  This does not fix the data model but can help reviewing it.
    --clean-deduplicate-names                     When several sibling packages, classes, interfaces have the same name, they are
                                                  renamed to make their name unique.
    --clean-fix-tag-name-case                     When a tag name case is invalid, fix it.
    --clean-remove-extra-spaces                   All leading and trailing spaces are removed from notes and tag values.
    --clean-remove-html-tags                      All html tags are removed from notes.
    --clean-replace-html-entities                 Some html entities are replaced in notes.
    --clean-split-ref-tags                        All ref tags that contain several refs are split into tags that contain one ref.
    --dump-all                                    Dumps all tables.
    --dump-csv                                    Uses CSV format to generate/read EAP dumps.
    --dump-force                                  Dump EAP database even if output is already generated (default).
    --dump-min                                    Dumps only tables that are necessary for post processing (Default).
    --dump-no-force                               Do not dump EAP database if output is already generated.
                                                  Does not work with '--dump-min'.
    --dump-xlsx                                   Uses XLSX format to generate/read EAP dumps (default).
    --eap-file <arg>                              Mandatory name of the input EAP file.
 -h,--help                                        Prints this help and exits.
    --help-width <arg>                            Optional help width (default: 74).
    --html-fail                                   Fail in case of detected implementation error.
    --html-flat-dirs                              Use flat directories (hashcodes). Otherwise, use hierarchy of directories.
                                                  Useful with frames.
    --html-force                                  Force image generation, even when images have not changed.
    --html-frames                                 Generate a multi-frame document (default).
    --html-img-colors                             Generate colored images (default).
    --html-img-hide-all-metas                     Hide all metas by default in images. Use with --html-img-show-meta to show
                                                  individual metas.
    --html-img-hide-all-tags                      Hide all tags by default in images. Use with --html-img-show-tag to show
                                                  individual tags.
    --html-img-hide-meta <arg>                    Name(s) of metas to hide in images. Use with --html-img-show-all-metas.
    --html-img-hide-tag <arg>                     Name(s) of tags to hide in images. Use with --html-img-show-all-tags.
    --html-img-max-tag-values <arg>               Maximum number of values to show for a tag in images (default: -1).
    --html-img-no-colors                          Generate monochrome (grey) images.
    --html-img-no-shadows                         Do not generate shadows in images (default).
    --html-img-png                                Generate PNG images (default).
    --html-img-shadows                            Generate shadows in images.
    --html-img-show-all-metas                     Show all metas by default in images (default). Use with --html-img-hide-meta to
                                                  hide individual metas.
    --html-img-show-all-tags                      Show all tags by default in images (default). Use with --html-img-hide-tag to hide
                                                  individual tags.
    --html-img-show-meta <arg>                    Name(s) of metas to show in images. Use with --html-img-hide-all-metas.
    --html-img-show-tag <arg>                     Name(s) of tags to show in images. Use with --html-img-hide-all-tags.
    --html-img-svg                                Generate SVG images.
    --html-img-tag-value-max-length <arg>         Maximum length of values to show for a tag in images (default: -1).
    --html-layout-depth <arg>                     The maximum depth used to spread elements in images (default 5).
    --html-ltor-threshold <arg>                   Number of elements from which images are oriented left to right (default 25).
                                                  If the number of elements in the image is smaller, the the image is oriented top
                                                  to bottom.
    --html-model-overview-ignore-package <arg>    Optional paths patterns of packages that should be ignored in model overview
                                                  image.
    --html-name-remove-part <arg>                 Optional patterns of name parts that must be removed in images.
    --html-no-fail                                Do not fail in case of detected implementation error (default).
    --html-no-force                               Do not force image generation (default).
    --html-no-parallel                            Do not use parallel tasks for HTML generation.
    --html-parallel                               Use parallel tasks for HTML generation (default).
    --html-show-ids                               Show identifiers.
    --html-show-non-navigable-tips                Show non-navigable tips in usage.
    --html-single-page                            Generate a single page document.
    --html-sort-tags                              Sort tags using their name and value.
    --model-author <arg>                          Optional author of the  model. If missing, it is automatically deduced from the
                                                  EAP file name.
    --model-fix-direction                         Fix direction and navigability of aggregations and compositions from the whole to
                                                  the part.
    --model-name <arg>                            Optional name of the model. If missing, it is automatically deduced from the EAP
                                                  file name.
    --model-no-fix-direction                      Do not fix direction and navigability of aggregations and compositions.
    --no-run-asd-cdc-mapping-exporter             Do not export the ASD/CDC mapping of rules.
    --no-run-asd-profile-exporter                 Do not export the ASD profile. Use with --run-all option.
    --no-run-ea-profile-exporter                  Do not export the EA profile. Use with --run-all option.
    --no-run-eap-dumper                           Do not dump the EAP database. Use with --run-all option.
    --no-run-html-generator                       Do not generate an HTML report. Use with --run-all option.
    --no-run-model-checker                        Do not check the ASD model. Use with --run-all option.
    --no-run-model-cleaner                        Do not clean the ASD model. Use with --run-all option.
    --no-run-model-generator                      Do not generate the ASD model from the EA dumps. Use with --run-all option.
    --no-run-office-generator                     Do not generate Office files from the ASD model. Use with --run-all option.
    --no-run-s1000d-generator                     Do not generate S1000D. Use with --run-all option.
    --no-run-xsd-generator                        Do not generate XSD. Use with --run-all option.
    --office-best                                 Use options that generate best office files.
    --office-fastest                              Use options that are fast to generate office files (default).
    --output-dir <arg>                            Mandatory name of the output directory. If this directory does not exist, it is
                                                  created.
    --profile-all                                 EA/ASD profiles and ASD profile config are exported as XML, MD, XLSX and HTML.
    --profile-config-xlsx                         ASD profile config is exported as XLSX.
    --profile-html                                EA/ASD profiles are exported as HTML.
    --profile-md                                  EA/ASD profiles are exported as MD.
    --profile-show-empty-sections                 If enabled, empty sections of EA/ASD profiles are exported.
    --profile-show-rules-domain                   If enabled, rules domain of EA/ASD profiles is exported.
    --profile-show-rules-enabling                 If enabled, rules enabling of EA/ASD profiles is exported.
    --profile-show-rules-severities               If enabled, rules severities of EA/ADS profiles are exported.
    --profile-xlsx                                EA/ASD profiles are exported as XLSX.
    --profile-xml                                 EA/ASD profiles are exported as XML.
    --run-all                                     Run all tools, except the one listed with '--no-run option-*'
    --run-asd-cdc-mapping-exporter                Export the ASD/CDC mapping of rules.
    --run-asd-profile-exporter                    Export the ASD profile. See --profile-* options.
    --run-ea-profile-exporter                     Export the EA profile. See --profile-* options.
    --run-eap-dumper                              Dump the EAP database. See --dump-* options.
    --run-html-generator                          Generate an HTML report. See --html-* options.
    --run-model-checker                           Check the ASD model.  See --model-* options.
    --run-model-cleaner                           Clean the ASD model.
    --run-model-generator                         Generate the ASD model from the EA dumps. See --model-* options.
    --run-office-generator                        Generate Office files from the ASD model. See --office-* options.
    --run-s1000d-generator                        Generate S1000D.  See --s1000d-* options.
    --run-xsd-generator                           Generate XSD.  See --xsd-* options.
    --s1000d-glossary                             Generate glossary data modules (SX001G).
    --s1000d-spec                                 Generate data model spec data modules (S2000M, S3000L, ...).
 -v,--version                                     Prints version and exits.
    --verbose                                     Prints messages.
    --xsd-copyright <arg>                         Optional copyright.
    --xsd-issue-date <arg>                        Mandatory (If XSD generation is enabled) issue date of the specification.
    --xsd-issue-number <arg>                      Mandatory (If XSD generation is enabled) issue number of the specification.
    --xsd-namespace <arg>                         Mandatory namespace.
    --xsd-no-parallel                             Do not use parallel tasks for XSD generation.
    --xsd-parallel                                Use parallel tasks for XSD generation (default).
    --xsd-specification <arg>                     Mandatory (If XSD generation is enabled) name of the specification.
    --xsd-specification-url <arg>                 Mandatory (If XSD generation is enabled) name of the specification URL.
    --xsd-valid-values-external-libraries <arg>   Optional URL of the input valid values external libraries (Office).
    --xsd-valid-values-libraries <arg>            Optional URL of the input valid values libraries (Office).
    --xsd-valid-values-units <arg>                Optional URL of the input valid values units (Office).
    --xsd-xml-schema-release-date <arg>           Mandatory (If XSD generation is enabled) release date of the valid values.
    --xsd-xml-schema-release-number <arg>         Mandatory (If XSD generation is enabled) release number of the valid values.
````