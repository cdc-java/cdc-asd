# XsdAnalyzer
`XsdAnalyzer` loads a set of ASD XML schemas and checks their consistency.  
Currently, the following checks are done:
- Search declared and unused global `complex types`, `simple types` and `groups`.
- Search `elements`, `attributes` and `types` whose names are not compliant with standard naming convention.

Results are dumped to an XLSX file, and a PDF and SVG files are generated.

This was created when it was found that some declarations were missing in S3000L-2.0 XSD.

## XsdAnalyzer example
Analysis of S3000L-2.0 schemas detects these issues:  
![Image](cdc-asd-tools/src/main/javadoc/cdc/asd/tools/xsd/doc-files/s3000l-2.0-issues.png)

Here is snapshot of a part of the generated graph:  
![Image](cdc-asd-tools/src/main/javadoc/cdc/asd/tools/xsd/doc-files/s3000l-2.0-graph-snapshot.png)


## XsdAnalyzer options
Options of `XsdAnalyzer` are:

````
USAGE
XsdAnalyzer [--args-file <arg>] [--args-file-charset <arg>] --basename <arg> [--compress]
                  [--directed | --undirected] [--exclude-qname <arg>] [-h | -v] [--help-width <arg>]
                  [--namespace-prefix <arg>] [--no-xsd] --output-dir <arg> [--path <arg>] --schema
                  <arg>   [--verbose]

Utility that analyzes a set of S-Series schemas and checks them:
- global types and groups that are not used.
- non-compliant element and type names.
A graph (PDF and SVG) is generated using GraphViz.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option
                                or value).
                                A line is ignored when it is empty or starts by any number of white
                                spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is
                                read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file
                                encoding.
    --basename <arg>            Base name of generated files.
    --compress                  Generates a compressed graph that only contains first level nodes.
    --directed                  Generates directed graph. This may take a long time (several
                                minutes).
    --exclude-qname <arg>       QName(s) of the nodes that must be excluded. Use prefixes defined
                                with '--namespace-prefix' option.
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --namespace-prefix <arg>    Prefix(es) that should be used to represent namespace(s). Has the
                                form: namespace::prefix
    --no-xsd                    Removes XSD nodes.
    --output-dir <arg>          Name of the output directory.
    --path <arg>                Directory(ies) where external binaries (GraphViz) can be found.
    --schema <arg>              Name(s) of the input schema(s).
    --undirected                Generates undirected graph (default).
 -v,--version                   Prints version and exits.
    --verbose                   Prints messages.
````