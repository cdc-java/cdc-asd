# ASD Profile
## Metas
- **Version**: SNAPSHOT

Rules used to check an ASD model.


## A01: ATTRIBUTE_CARDINALITY_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[attributes]` must have a `[cardinality]`.

### Applies to
- All attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAC05** ATTRIBUTE_CARDINALITY: The attribute cardinality must be numeric or be a numeric range (including '*' for unlimited upper range), and not zero.

### Remarks
- Should we consider that a missing cardinality is equivalent to 1?
- In that case, `[UML Writing Rules and Style Guide 2.0]` should be modified.


## A02: ATTRIBUTE_NAME_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[attributes]` must have a `[name]`.

### Applies to
- All attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAC02** ATTRIBUTE_NAME_CASE: The attribute name must exist, start with a letter, be in lowerCamelCase and may not include spaces.


## A03: ATTRIBUTE_NAME_MUST_BE_LOWER_CAMEL_CASE
### Metas
- **since**: 0.1.0

All defined `[attribute]` `[names]` must match this pattern: `^[a-z][a-zA-Z0-9]*$`  
They must be lowerCamelCase.

### Applies to
- All defined attribute names

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1
- `[UML Writing Rules and Style Guide 2.0]` 13.1.1

### Related to
- **RAC02** ATTRIBUTE_NAME_CASE: The attribute name must exist, start with a letter, be in lowerCamelCase and may not include spaces.
- **RAC10** ATTRIBUTE_NAME_NO_SPACE: The attribute name may not contain spaces and must start with a lowercase letter.


## A04: ATTRIBUTE_NAME_SHOULD_NOT_CONTAIN_CODE
### Metas
- **since**: 0.1.0

No defined `[attribute]` `[name]` should match this pattern: `^.*Code.*$`  
They should not contain 'Code', except if they come from another standard or specification.

### Applies to
- All defined attribute names

### Exceptions
- disassemblyCode
- disassemblyCodeVariant
- exportControlPartyClearanceCode
- exportControlRegulationLegalCode
- extensionCode
- informationCode
- informationCodeVariant
- itemLocationCode
- learnCode
- learnEventCode
- materialItemCategoryCode
- modelIdentificationCode
- postalCode
- skillCode
- subtaskInformationCode
- systemDifferenceCode
- taskInformationCode
- taskRequirementInformationCode

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAC08** ATTRIBUTE_NAME_NO_CODE: 'Code' must not be used as part of an attribute name. Use ‘Classification’ or ‘Identifier’ instead in order to distinguish between different uses of codes.


## A05: ATTRIBUTE_NAME_SHOULD_START_WITH_CLASS_NAME
### Metas
- **since**: 0.12.0

### Labels
- **Contradiction**

The `[name]` of an `[attribute]` should start with the `[name]` of its `[class]`, in lowerCamelCase.  
It can also start with the name of another closely related class (ancestor, composition owner, ...) 

### Applies to
- All class attributes

### Exceptions
- attributes of `<<attributeGroup>>` classes
- attributes of `<<compoundAttribute>>` classes
- attributes of `<<primitive>>` classes
- attributes of `<<umlPrimitive>>` classes
- attributes of BaseObject
- attributes of BreakdownElement
- attributes of BreakdownElementRevision
- attributes of BreakdownElementStructure
- attributes of BreakdownElementUsageInBreakdown
- attributes of DegradationMechanismAcceptanceCriteria
- attributes of FailureModeSymptomsSignature
- attributes of GlobalPosition
- attributes of HardwareElementAttachingConnector
- attributes of InstalledPart
- attributes of ParameterThresholdDefinition
- attributes of PartDefinitionPartsListEntry
- attributes of PartDefinitionPartsListRevision
- attributes of S1000DDataModule
- attributes of S1000DDataModuleIssue
- attributes of S1000DPublicationModule
- attributes of S1000DPublicationModuleIssue
- attributes of SerializedProductVariantConfigurationConformance
- attributes of StreetAddress
- attributes of Task
- attributes of TaskRevision
- allowedProductConfigurationIdentifier
- applicabilityDateRange
- applicableSerialNumberRange
- circuitBreakerState
- evaluationByAssertionRole
- eventThresholdNumberOfEventOccurrences
- packagedTask
- partDefinitionIdentifier
- partIdentifier
- partName
- partsListType
- productDefinitionIdentifier
- productVariantDefinitionIdentifier
- sourceDocumentPortion
- specialEventOccurenceRatio
- workItemTimelineEvent
- workItemTimelineLag

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1 (recommendation).

### Related to
- **RAC12** ATTRIBUTE_NAME_CLASS: An attribute name must start with the class name.

### Remarks
- `[UML Writing Rules and Style Guide 2.0]` recommends this naming, whilst ATTRIBUTE_NAME_CLASS requires it.
- There are many cases of non-compliance with this rule.
- Should this be applied to attributes of all classes?


## A06: ATTRIBUTE_NOTES_ARE_MANDATORY
### Metas
- **since**: 0.1.0

All `[attributes]` must have `[notes]`.

### Applies to
- All attributes, except those of `<<builtin>>` classes and of BaseObject

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAD01** ATTRIBUTE_DEFINITION: An attribute must have a definition

### Remarks
- Scope is reduced.


## A07: ATTRIBUTE_NOTES_MUST_START_WITH_NAME
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[attribute]` `[notes]` must start with the `[attribute]` `[name]`.

### Applies to
- All attributes notes

### Related to
- **RAD02** ATTRIBUTE_DEFINITION_AUTHORING: An attribute definition must start with the attribute name


## A08: ATTRIBUTE_STEREOTYPE_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[attributes]` must have a `[stereotype]`.

### Applies to
- All attributes of classes that are neither BaseObject, `<<primitive>>`, `<<umlPrimitive>>` or `<<compoundAttribute>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAS01** ATTRIBUTE_STEREOTYPES: Attributes can only have the "key", "compositeKey", "relationshipkey", "characteristic" or "metadata" stereotypes. The stereotype is mandatory.

### Remarks
- `[UML Writing Rules and Style Guide 2.0]` should be fixed.
- ATTRIBUTE_STEREOTYPES should be fixed.


## A09: ATTRIBUTE_STEREOTYPES_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[attribute]` `[stereotypes]` must be allowed.  
They must be one of:  
- `<<characteristic>>`  
- `<<compositeKey>>`  
- `<<key>>`  
- `<<metadata>>`  
- `<<relationshipKey>>`

### Applies to
- All recognized stereotypes of all attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.2

### Related to
- **RAS01** ATTRIBUTE_STEREOTYPES: Attributes can only have the "key", "compositeKey", "relationshipkey", "characteristic" or "metadata" stereotypes. The stereotype is mandatory.


## A10: ATTRIBUTE_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[attribute]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- ref  
- source  
- unit  
- validValue  
- validValueLibrary  
- xmlName

### Applies to
- All recognized tag names of all attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.5
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.4 for unit
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.7 for source
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example

### Related to
- **RAT01** ATTRIBUTE_TAGS: An attribute can only have the following tags: xmlName, validValue, validValueLibrary, unit, note, example, source and ref.


## A11: ATTRIBUTE_TAG(UNIT)_MUST_FOLLOW_AUTHORING
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All `[attribute]` `[unit tag]` `[values]` must match this pattern: `^[\S]+$`  
They must neither contain spaces nor be empty.

### Applies to
- The value of all unit tags of all attributes

### Related to
- **RTD10** UNIT_TYPE: A "unit" tag must have a value "unit" or one of the approved/predefined unit types. It may not be blank.


## A12: ATTRIBUTE_TAG(VALID_VALUE)_NOTES_ARE_MANDATORY
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[attribute]` `[validValue tags]` must have `[notes]`.

### Applies to
- validValue attribute tags that are non-empty

### Related to
- **RTD08** VALID_VALUE_DEFINITION: A non-blank validValue must have a definition in the tagged value notes and the definition must start with "SX001G:"


## A13: ATTRIBUTE_TAG(VALID_VALUE)_NOTES_MUST_FOLLOW_AUTHORING
### Metas
- **since**: 0.1.0

All defined `[attribute]` `[validValue tag]` `[notes]` must match this pattern: `^[a-zA-Z0-9]+:[a-zA-Z0-9]+$`  
The notes must contain the source and the term separated by a colon.

### Applies to
- validValue attribute tags that are non-empty

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.11

### Related to
- **RTD08** VALID_VALUE_DEFINITION: A non-blank validValue must have a definition in the tagged value notes and the definition must start with "SX001G:"
- **RTD09** VALID_VALUE_DEFINITION_NAME: The definition name of the validValue must be a single lowerCamelCase word.

### Remarks
- **RTD08**/**RTD09** are stricter than `[UML Writing Rules and Style Guide 2.0]`
- Source can be different from SX001G.
- Term can be anything.


## A14: ATTRIBUTE_TAG(VALID_VALUE)_VALUE_MUST_BE_UNIQUE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[attribute]` `[validValue tag]` `[values]` must be unique.

### Related to
- **RTD07** VALID_VALUE_CODE: A validValue code must be unique within a same attribute.


## A15: ATTRIBUTE_TAG(XML_NAME)_COUNT_MUST_BE_1
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All `[attributes]` must have exactly 1 `[xmlName tag]`.

### Applies to
- All attributes

### Exceptions
- uri

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.5

### Related to
- **RAT02** ATTRIBUTE_XML_NAME: An attribute must have exactly one xmlName.


## A16: ATTRIBUTE_TYPE_IS_MANDATORY
### Metas
- **since**: 0.9.0

### Labels
- **No_UWRSG_source**

All `[attributes]` must have a `[type]`.

### Applies to
- All attributes


## A17: ATTRIBUTE_TYPE_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

The `[type]` of an `[attribute]` must be a `[<<primitive>> class]`, `[<<umlPrimitive>> class]`, `[<<builtin>> class]`, `[<<class>> class]` or `[<<compoundAttribute>> class]`.

### Applies to
- Any valid type of an attribute

### Related to
- **RAC11** ATTRIBUTE_TYPE: An attribute must exist and be either a primitive, compoundattribute, existing class or UML primitive.


## A18: ATTRIBUTE_VISIBILITY_MUST_BE_PUBLIC
### Metas
- **since**: 0.1.0

The `[attribute]` `[visibility]` must be public (or undefined).

### Applies to
- All attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAC09** ATTRIBUTE_VISIBLE: An attribute must be visible.


## A19: ATTRIBUTE(IDENTIFIER)_TAG(VALID_VALUE)_VALUE_MUST_NOT_BE_EMPTY
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

The `[validValue tag]` `[values]` of some `[attributes]` must match this pattern: `^[\S]+$`  
They must neither contain spaces nor be empty.

### Applies to
- All IdentifierType attributes

### Related to
- **RAT04** IDENTIFER_VALID_VALUE_OR_LIBRARY: All attributes of identifier type must have at least one (non-blank) validValue/validValueLibrary.


## A20: ATTRIBUTE(SOME)_CARDINALITY_LOWER_BOUND_MUST_BE_ONE
### Metas
- **since**: 0.1.0

When the effective `[cardinality]` of some `[attributes]` is valid, its `[lower bound]` must be '1'.

### Applies to
- All `<<key>>` attributes
- All `<<compositeKey>>` attributes
- All `<<relationshipKey>>` attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAC06** ATTRIBUTE_KEY_LOWER_CARDINALITY:   Key attributes (stereotype key, compositeKey and relationshipKey) must always have the lower multiplicity defined as ‘.


## A21: ATTRIBUTE(SOME)_CARDINALITY_UPPER_BOUND_MUST_BE_UNBOUNDED
### Metas
- **since**: 0.1.0

When the effective `[cardinality]` of some `[attributes]` is valid, its `[upper bound]` must be '*'.

### Applies to
- All DescriptorType attributes
- All NameType attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.1

### Related to
- **RAC07** ATTRIBUTE_UPPER_CARDINALITY:   Attributes of type DescriptorType and NameType must always have their upper cardinality defied as ‘*’ (ie, many)


## A22: ATTRIBUTE(SOME)_STEREOTYPE_MUST_NOT_BE_ANY_KEY
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

The `[stereotypes]` of some `[attributes]` must not be `<<key>>`, `<<relationshipKey>>` or `<<compositeKey>>`.  
Only IdentifierType, ClassificationType and NameType attributes can have a `<<key>>`, `<<compositeKey>>` or `<<relationshipKey>>` stereotype.

### Applies to
- All attributes that are neither IdentifierType, ClassificationType nor NameType

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.2.1 excludes DescriptorType with `<<key>>` or `<<compositeKey>>`.
- `[UML Writing Rules and Style Guide 2.0]` 11.2.2.2 excludes DescriptorType with `<<relationshipKey>>`.

### Related to
- **RAS02** ATTRIBUTE_KEYS: Only IdentifierType and ClassificationType attributes can have the `<<key>>`, `<<compositeKey>>` or `<<relationshipKey>>` stereotypes.

### Remarks
- Added NameType to ASD rule.
- umlString, Organization, DateTimeType are key attributes in GlobalPosition, StreetAddress, ...: are they valid exceptions?


## A23: ATTRIBUTE(SOME)_TAG_MUST_HAVE_VALID_VALUE_OR_VALID_VALUE_LIBRARY
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

Some `[attributes]` must have one among `[validValue tag]` `[validValueLibrary tag]`.

### Applies to
- All IdentifierType attributes
- All ClassificationType attributes
- All StateType attributes
- All DatedClassification attributes
- All TimeStampedClassification attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.11
- `[UML Writing Rules and Style Guide 2.0]` 11.8.12

### Related to
- **RAT04** IDENTIFER_VALID_VALUE_OR_LIBRARY: All attributes of identifier type must have at least one (non-blank) validValue/validValueLibrary.
- **RAT05** CLASSIFICATION_VALID_VALUE_OR_LIBRARY: All attributes of classification type must have at least one validValue/validValueLibrary. The validValue can be blank.
- **RAT06** DATED_CLASSIFICATION_VALID_VALUE_OR_LIBRARY: All attributes of DatedClassification type must have at least one validValue/validValueLibrary. The validValue can be blank.
- **RAT07** TIMED_CLASSIF_VALID_VALUE_OR_LIBRARY: All attributes of TimeStampedClassification type must have at least one validValue/validValueLibrary. The validValue can be blank.
- **RAT08** VALID_VALUE_AND_LIBRARY: It is not allowed to have simultaneously a validValue and validValueLibrary for the same attribute.
- **RAT20** STATE_VALID_VALUE_OR_LIBRARY: All attributes of state type must have at least one validValue/validValueLibrary. The validValue can be blank.


## A24: ATTRIBUTE(SOME)_TAG(UNIT)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

Some `[attributes]` must not have any `[unit tag]`.

### Applies to
- All attributes that are neither PropertyType, nor NumericalPropertyType or any of its descendants.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.5
- `[UML Writing Rules and Style Guide 2.0]` 11.8.4

### Related to
- **RAT12** UNIT: All attributes of Property type (or its specializations) must have exactly one unit tag.
- **RAT13** NO_UNIT_ALLOWED: The unit tag is not allowed for IdentifierType, DescriptorType, NameType, ClassificationType, DateType, DateTimeType or StateType primitives.


## A25: ATTRIBUTE(SOME)_TAG(UNIT)_COUNT_MUST_BE_1
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

Some `[attributes]` must have exactly 1 `[unit tag]`.

### Applies to
- All attributes that are PropertyType, NumericalPropertyType or descendant of NumericalPropertyType.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.5
- `[UML Writing Rules and Style Guide 2.0]` 11.8.4

### Related to
- **RAT12** UNIT: All attributes of Property type (or its specializations) must have exactly one unit tag.


## A26: ATTRIBUTE(SOME)_TAG(VALID_VALUE)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

Some `[attributes]` must not have any `[validValue tag]`.

### Applies to
- All attributes that are neither IdentifierType, ClassificationType, StateType, DatedClassification nor TimeStampedClassification

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.5

### Related to
- **RAT10** NO_VALID_VALUE:  validValue/validValueLibrary tags not allowed for DateType, DateTimeType, DescriptorType, NameType or UML primitives.
- **RAT09** VALID_VALUE_LIBRARY: An attribute can only have zero or one single validValueLibary.


## A27: ATTRIBUTE(SOME)_TAG(VALID_VALUE_LIBRARY)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

Some `[attributes]` must not have any `[validValueLibrary tag]`.

### Applies to
- All attributes that are neither IdentifierType, ClassificationType, StateType, DatedClassification or TimeStampedClassification

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.5

### Related to
- **RAT10** NO_VALID_VALUE:  validValue/validValueLibrary tags not allowed for DateType, DateTimeType, DescriptorType, NameType or UML primitives.
- **RAT09** VALID_VALUE_LIBRARY: An attribute can only have zero or one single validValueLibary.


## A28: ATTRIBUTE(SOME)_TAG(VALID_VALUE_LIBRARY)_COUNT_MUST_BE_0_1
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

Some `[attributes]` can optionally have 1 `[validValueLibrary tag]`.

### Applies to
- All IdentifierType attributes
- All ClassificationType attributes
- All StateType attributes
- All DatedClassification attributes
- All TimeStampedClassification attributes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.2.5
- `[UML Writing Rules and Style Guide 2.0]` 11.8.12

### Related to
- **RAT09** VALID_VALUE_LIBRARY: An attribute can only have zero or one single validValueLibary.


## A29: ATTRIBUTE(SOME)_TAG(VALID_VALUE)_VALUE_MUST_NOT_BE_EMPTY
### Metas
- **since**: 0.15.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

If an `[attribute]` has only one `[validValue tag]`, it cannot have an empty `[value]`.

### Applies to
- All validValue tag values of attributes that have 2 or more validValue tags.

### Related to
- **RAT11** NO_BLANK_VALID_VALUE: If an attribute has more than one validValue, then none of them must be blank.


## C01: CLASS_ATTRIBUTE_NAMES_MUST_BE_UNIQUE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[class]` `[attribute]` `[names]`, including inherited ones, must be unique.

### Related to
- **RAC03** ATTRIBUTE_NAME_UNIQUE: The attribute name must be unique in the class (included inherited attributes)


## C02: CLASS_ATTRIBUTE_TAGS(XML_NAME)_MUST_BE_UNIQUE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[class]` `[attribute]` `[xmlName tag]` `[values]`, including inherited `[attributes]`, must be unique.

### Related to
- **RAT03** ATTRIBUTE_XML_NAME_UNIQUE: The xmlName of an attribute must be unique within its class (including inherited attributes).


## C03: CLASS_MULTIPLE_INHERITANCE_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

A `[class]` cannot inherit from several `[classes]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.3

### Related to
- **RCH01** CLASS_SPECIALIZATION: A class cannot be a specialization of more than one class (multiple inheritance not allowed).


## C04: CLASS_MUST_DIRECTLY_EXTEND_AT_MOST_ONCE_A_CLASS
### Metas
- **since**: 0.5.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

A `[class]` must directly specialize at most once a `[class]`.


## C05: CLASS_MUST_DIRECTLY_IMPLEMENT_AT_MOST_ONCE_AN_INTERFACE
### Metas
- **since**: 0.5.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

A `[class]` must directly implement at most once an `[interface]`.


## C06: CLASS_MUST_NOT_BE_COMPOSITION_PART_OF_EXCHANGE_AND_NON_EXCHANGE_CLASSES
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

A `[class]` cannot simultaneously be `[composition]` `[part]` of an `[<<exchange>> class]` and of a `[non <<exchange>> class]`.

### Related to
- **RCX03** WRONG_CLASS_IN_EXCHANGE: A class that is part of a composition must not be part of an `<<exchange>>`.

### Remarks
- Definition may need to be refined


## C07: CLASS_MUST_NOT_DECLARE_USELESS_INHERITANCE
### Metas
- **since**: 0.19.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

A `[class]` must not directly and indireclty inherit from a type.


## C09: CLASS_NAME_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[classes]` must have a `[name]`.

### Applies to
- All classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.1

### Related to
- **RCC01** CLASS_NAME_RULES: A class name name must exist, start with a letter, be in UpperCamelCase and may not include spaces.


## C10: CLASS_NOTES_MUST_START_WITH_NAME
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[class]` `[notes]` must start with the `[class]` `[name]`.

### Related to
- **RCD02** CLASS_DEFINITION_AUTHORING: A class definition must start with the class name.

### Applies to
- All classes notes


## C11: CLASS_SPECIALIZATION_MUST_NOT_DECLARE_ANY_KEY_ATTRIBUTE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

A `[class specialization]` must not declare any `[<<key>>]`, `[<<compositeKey>>]` or `[<<relationshipKey>>]` `[attribute]`.

### Applies to
- All classes that are a specialization of a class that is not BaseObject

### Related to
- **RCA01** CLASS_NO_KEY_IF_SPECIALIZED: A Class which is a specialization of another Class must not add any key attributes (`<<key>>`, `<<compositeKey>>` and `<<relationshipKey>>`).


## C12: CLASS_STEREOTYPE_IS_MANDATORY
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All `[classes]` must have a `[stereotype]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.1

### Related to
- **RCM01** CLASS_STEREOTYPES: A class must have a stereotype, either `<<class>>`, `<<relationship>>`, `<<attributeGroup>>`, `<<exchange>>` or `<<proxy>>`.


## C13: CLASS_STEREOTYPE_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All defined `[class]` `[stereotypes]` must be allowed.  
They must be one of:  
- `<<attributeGroup>>`  
- `<<builtin>>`  
- `<<class>>`  
- `<<compoundAttribute>>`  
- `<<exchange>>`  
- `<<metaclass>>`  
- `<<primitive>>`  
- `<<relationship>>`  
- `<<umlPrimitive>>`

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.2

### Related to
- **RCM01** CLASS_STEREOTYPES: A class must have a stereotype, either `<<class>>`, `<<relationship>>`, `<<attributeGroup>>`, `<<exchange>>` or `<<proxy>>`.

### Remarks
- 11.1.2 lists only 4 stereotypes.


## C14: CLASS_TAG(XML_REF_NAME)_MUST_BE_LOWER_CAMEL_CASE
### Metas
- **since**: 0.1.0

All `[class]` `[xmlRefName tag]` `[values]` must match this pattern: `^[a-z][a-zA-Z0-9]*$`  
They must be lowerCamelCase.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.2

### Related to
- **RTD06** CLASS_XML_REF_NAME_CASE: A class xmlRefName must be defined in lowerCamelCase.


## C15: CLASS_VERSION_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[class]` `[versions]` must match this pattern: `^[\d][\d][\d]-00$`

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 4.1.3

### Related to
- **RCM04** CLASS_VERSION: A class must have a version number with a format NNN-NN, where NNN must be three digits and not null and NN must be "00".


## C16: CLASS_VISIBILITY_MUST_BE_PUBLIC
### Metas
- **since**: 0.1.0

The `[class]` `[visibility]` must be public (or undefined).

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.1

### Related to
- **RCM02** CLASS_PUBLIC: The scope of a class must be Public.


## C17: CLASS(ATTRIBUTE_GROUP)_MUST_BE_COMPOSITION_PART
### Metas
- **since**: 0.1.0

An `[<<attributeGroup>> class]` must be a `[composition]` `[part]` of a `[<<class>> class]`, `[<<relationship>> class]` or `[<<extend>> class]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.2.3

### Related to
- **RGY02** ATTRIBUTE_GROUP_COMPOSITION: An attributeGroup must always be a composition of a class.

### Remarks
- Rule needs to be refined.


## C18: CLASS(ATTRIBUTE_GROUP)_MUST_NOT_BE_ABSTRACT
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

An `[<<attributeGroup>> class]` cannot be abstract.

### Related to
- **RGC01** ATTRIBUTE_GROUP_NOT_ABSTRACT: attributeGroups cannot be abstract.


## C19: CLASS(ATTRIBUTE_GROUP)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<attributeGroup>> class]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- xmlName  
- note  
- ref  
- replaces  
- changeNote  
- source  
- example

### Applies to
- All recognized tag names of all `<<attributeGroup>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.5 for changeNote
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.7 for source
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example
- `[UML Writing Rules and Style Guide 2.0]` 11.8.10 for replaces

### Related to
- **RGT01** ATTRIBUTE_GROUP_TAGS: Allowed tags for attribute groups are xmlName, note, example, replaces, source, ref and changeNote.


## C20: CLASS(CLASS)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<class>> class]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- changeNote  
- example  
- note  
- ref  
- replaces  
- source  
- uidPattern  
- xmlName  
- xmlRefName

### Applies to
- All recognized tag names of all `<<class>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.2 for xmlRefName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.3 for uidPattern
- `[UML Writing Rules and Style Guide 2.0]` 11.8.5 for changeNote
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.7 for source
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example
- `[UML Writing Rules and Style Guide 2.0]` 11.8.10 for replaces

### Related to
- **RCT01** CLASS_TAGS: Allowed tags for classes and relationships are uidPattern, xmlName, xmlRefName, note, example, replaces, source, ref and changeNote.


## C21: CLASS(COMPOSITION_PART)_ATTRIBUTE(KEY)_STEREOTYPE_MUST_BE_COMPOSITE_KEY
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All `[classes]` that are `[composition]` `[parts]` of [non-`<<exchange>>`] `[classes]` must use `[<<compositeKey>> stereotype]` for their key `[attributes]`.  
`[<<key>> stereotype]` and `[<<relationshipKey>> stereotype]` are forbidden.

### Applies to
- All attributes of classes that are composition parts of non-`<<exhange>>` classes

### Related to
- **RAS04** ATTRIBUTE_COMPOSITE_KEYS: Key attributes in composition classes  must use `<<compositeKey>>` stereotype.


## C22: CLASS(COMPOUND_ATTRIBUTE)_MUST_HAVE_AT_LEAST_1_ATTRIBUTE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[concrete <<compoundAttribute>> classes]` must have at least 1 `[attribute]`.

### Related to
- **RUC01** COMPOUND_ATTRIBUTE_NUMBER_OF_ATTRIBUTES: A compoundAttribute must have at least two attributes.

### Remarks
- This rule is an adaptation of ASD Rule.
- This could be applied to other concrete classes.


## C23: CLASS(COMPOUND_ATTRIBUTE)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<compoundAttribute>> class]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- ref  
- xmlName

### Applies to
- All `<<compoundAttribute>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example


## C24: CLASS(EXCHANGE)_ATTRIBUTES_ARE_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

An `[<<exchange>> class]` cannot have any `[attribute]`.

### Applies to
- All `<<exchange>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.2.4

### Related to
- **REC01** EXCHANGE_NO_ATTRIBUTES: `<<exchange>>` classes cannot have attributes.


## C25: CLASS(EXCHANGE)_MUST_NOT_OWN_ANY_AGGREGATION
### Metas
- **since**: 0.12.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All `[<<exchange>> class]` must not own any `[aggregation]`.

### Applies to
- All `<<exchange>>` classes

### Related to
- **REY02** EXCHANGE_CONNECTOR: exchange classes cannot have any connectors (aggregations, associations, implementations, etc) except those listed in rule **REY01** or a link to a Note.


## C26: CLASS(EXCHANGE)_MUST_NOT_OWN_ANY_ASSOCIATION
### Metas
- **since**: 0.12.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All `[<<exchange>> class]` must not own any `[association]`.

### Applies to
- All `<<exchange>>` classes

### Related to
- **REY02** EXCHANGE_CONNECTOR: exchange classes cannot have any connectors (aggregations, associations, implementations, etc) except those listed in rule **REY01** or a link to a Note.


## C27: CLASS(EXCHANGE)_MUST_NOT_OWN_ANY_IMPLEMENTATION
### Metas
- **since**: 0.12.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All `[<<exchange>> class]` must not own any `[implementation]`.

### Applies to
- All `<<exchange>>` classes

### Related to
- **REY02** EXCHANGE_CONNECTOR: exchange classes cannot have any connectors (aggregations, associations, implementations, etc) except those listed in rule **REY01** or a link to a Note.


## C28: CLASS(EXCHANGE)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<exchange>> class]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- changeNote  
- example  
- note  
- ref  
- replaces  
- source  
- xmlName  
- xmlSchemaName

### Applies to
- All `<<exchange>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.5 for changeNote
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.7 for source
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example
- `[UML Writing Rules and Style Guide 2.0]` 11.8.10 for replaces
- `[UML Writing Rules and Style Guide 2.0]` 11.8.13 for xmlSchemaName

### Related to
- **RET01** EXCHANGE_TAGS: Allowed `<<exchange>>` tags are xmlName, xmlSchemaName, note, example, replaces, source, ref and changeNote.


## C29: CLASS(NON_ABSTRACT_RELATIONSHIP)_MUST_HAVE_TWO_KEY_ASSOCIATIONS
### Metas
- **since**: 0.21.0

### Labels
- **Draft**
- **No_UWRSG_source**
- **XSD_generation**

All [non-abstract `<<relationship>>`] `[classes]` must have 2 `[associations]` without `[noKey]` `[tag]`.

### Applies to
- All non-abstract associations

### Related to
- **RMT01** RELATIONSHIP_NO_KEY: A relationship  with more than two associations must have the additional ones tagged as "NoKey".

### Remarks
- To be confirmed


## C30: CLASS(NOT_COMPOSITION_PART)_ATTRIBUTE(COMPOSITE_KEY)_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All `[classes]` that are not `[composition]` `[parts]` cannot have `[<<compositeKey>> attributes]`.

### Applies to
- All attributes of classes that are not composition parts

### Related to
- **RAS05** ATTRIBUTE_NO_COMPOSITE_KEYS: Non-composition classes must not use `<<compositeKey>>` as stereotype.


## C31: CLASS(NOT_RELATIONSHIP)_ATTRIBUTE(RELATIONSHIP_KEY)_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[classes]` that are not `[<<relationship>> classes]` cannot have `[<<relationshipKey>> attributes]`.

### Applies to
- All attributes of classes that are not relationships

### Related to
- **RAS03** ATTRIBUTE_CLASS_KEYS: Non-relationship classes must not use `<<relationshipKey>>` as stereotype.


## C32: CLASS(PRIMITIVE)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<primitive>> class]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- ref  
- xmlName

### Applies to
- All `<<primitive>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example


## C33: CLASS(RELATIONSHIP)_ATTRIBUTE(KEY)_STEREOTYPE_MUST_BE_RELATIONSHIP_KEY
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All `[<<relationship>> classes]` must use `[<<relationshipKey>> stereotype]` for their key `[attributes]`.  
`[<<key>> stereotype]` and `[<<compositeKey>> stereotype]` are forbidden.

### Applies to
- All attributes of `<<relationship>>` classes

### Related to
- **RAS06** ATTRIBUTE_RELATIONSHIP_KEYS: Key attributes in relationship classes must use the `<<relationshipKey>>` stereotype.


## C34: CLASS(RELATIONSHIP)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<relationship>> class]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- changeNote  
- example  
- note  
- ref  
- replaces  
- source  
- uidPattern  
- xmlName  
- xmlRefName

### Applies to
- All `<<relationship>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.6
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.2 for xmlRefName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.3 for uidPattern
- `[UML Writing Rules and Style Guide 2.0]` 11.8.5 for changeNote
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.7 for source
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example
- `[UML Writing Rules and Style Guide 2.0]` 11.8.10 for replaces

### Related to
- **RCT01** CLASS_TAGS: Allowed tags for classes and relationships are uidPattern, xmlName, xmlRefName, note, example, replaces, source, ref and changeNote.


## C35: CLASS(REVISION)_MUST_HAVE_REVISION_ATTRIBUTES
### Metas
- **since**: 0.1.0

A `[class revision]` must have Rationale, Date and Status `[attributes]`, in that order, in last position.

### Applies to
- All root revision classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 15.1.2

### Related to
- **RCX02** CLASS_REVISION: Class revisions (class name ends with 'Revision'): It must include Rationale, Date, Status, in that order, at the end of the class.


## C36: CLASS(SOME)_AUTHOR_IS_MANDATORY
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All `[classes]` must have an `[author]`.

### Applies to
- All classes except `<<builtin>>` ones

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.1

### Related to
- **RCM03** CLASS_AUTHOR: A class must have an author; the author name must be one of the specification numbers or "DMEWG".


## C37: CLASS(SOME)_AUTHOR_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

A defined `[class]` `[author]` must be allowed.  
It must be one of:  
- DMEWG  
- S1000D  
- S2000M  
- S3000L  
- S4000P  
- S5000F  
- S6000T  
- SX000i

### Applies to
- All classes except `<<builtin>>` ones

### Related to
- **RCM03** CLASS_AUTHOR: A class must have an author; the author name must be one of the specification numbers or "DMEWG".


## C38: CLASS(SOME)_MUST_NOT_BE_AGGREGATION_PART
### Metas
- **since**: 0.20.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

Some `[classes]` must not be an `[aggregation]` `[part]` (they have no identity).

### Applies to
- All `<<attributeGroup>>` classes
- All `<<compoundAttribute>>` classes
- All `<<primitive>>` classes
- All `<<umlPrimitive>>` classes

### Related to
- **RGY01** ATTRIBUTE_GROUP_NO_ASSOCIATIONS: An attributeGroup can have no incoming or outgoing associations.
- **RUY01** COMPOUND_ATTRIBUTE_ASSOCIATIONS: A compoundAttribute can have no associations, neither incoming nor outgoing.


## C39: CLASS(SOME)_MUST_NOT_BE_ASSOCIATION_TARGET
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

Some `[classes]` must not be an `[association]` `[target]` (they have no identity).

### Applies to
- All `<<attributeGroup>>` classes
- All `<<compoundAttribute>>` classes
- All `<<primitive>>` classes
- All `<<umlPrimitive>>` classes

### Related to
- **RGY01** ATTRIBUTE_GROUP_NO_ASSOCIATIONS: An attributeGroup can have no incoming or outgoing associations.
- **RUY01** COMPOUND_ATTRIBUTE_ASSOCIATIONS: A compoundAttribute can have no associations, neither incoming nor outgoing.


## C40: CLASS(SOME)_MUST_NOT_BE_PART_OF_SEVERAL_COMPOSITIONS
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

Some `[classes]` cannot be part of several `[compositions]`.

### Applies to
- Classes that have a business id (one or more local or inherited key).

### Related to
- **RCY01** CLASS_COMPOSITION: A class cannot be a composition of more than one class or `<<extend>>` interface.


## C41: CLASS(SOME)_MUST_NOT_DIRECTLY_BE_AGGREGATION_WHOLE
### Metas
- **since**: 0.20.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

Some `[classes]` must not directly be an `[aggregation]` `[whole]`.

### Applies to
- All `<<attributeGroup>>` classes
- All `<<compoundAttribute>>` classes
- All `<<primitive>>` classes
- All `<<umlPrimitive>>` classes

### Related to
- **RGY01** ATTRIBUTE_GROUP_NO_ASSOCIATIONS: An attributeGroup can have no incoming or outgoing associations.
- **RUY01** COMPOUND_ATTRIBUTE_ASSOCIATIONS: A compoundAttribute can have no associations, neither incoming nor outgoing.

### Remarks
- This rule needs justifications.


## C42: CLASS(SOME)_MUST_NOT_DIRECTLY_BE_ASSOCIATION_SOURCE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

Some `[classes]` must not directly be an `[association]` `[source]`.

### Applies to
- All `<<attributeGroup>>` classes
- All `<<compoundAttribute>>` classes
- All `<<primitive>>` classes
- All `<<umlPrimitive>>` classes

### Related to
- **RGY01** ATTRIBUTE_GROUP_NO_ASSOCIATIONS: An attributeGroup can have no incoming or outgoing associations.
- **RUY01** COMPOUND_ATTRIBUTE_ASSOCIATIONS: A compoundAttribute can have no associations, neither incoming nor outgoing.

### Remarks
- This rule needs justifications.


## C43: CLASS(SOME)_MUST_NOT_DIRECTLY_BE_COMPOSITION_WHOLE
### Metas
- **since**: 0.20.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

Some `[classes]` must not directly be an `[composition]` `[whole]`.

### Applies to
- All `<<attributeGroup>>` classes
- All `<<compoundAttribute>>` classes
- All `<<primitive>>` classes
- All `<<umlPrimitive>>` classes

### Related to
- **RGY01** ATTRIBUTE_GROUP_NO_ASSOCIATIONS: An attributeGroup can have no incoming or outgoing associations.
- **RUY01** COMPOUND_ATTRIBUTE_ASSOCIATIONS: A compoundAttribute can have no associations, neither incoming nor outgoing.

### Remarks
- This rule needs justifications.


## C44: CLASS(SOME)_MUST_NOT_IMPLEMENT_EXTEND_INTERFACE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

Some `[classes]` cannot implement an `[<<extend>> interface]`.  
Only `[<<class>> classes]` and `[<<relationship>> classes]` can.  
`[<<extend>> interfaces]` that have 1 `[composition]` are ignored.

### Applies to
- All `<<attributeGroup>>` classes
- All `<<builtin>>` packages, classes
- All `<<compoundAttribute>>` classes
- All `<<exchange>>` classes
- All `<<metaclass>>` classes
- All `<<primitive>>` classes
- All `<<umlPrimitive>>` classes

### Related to
- **RIA06** NO_PRIMITIVE_EXTEND: `<<primitives>>` cannot implement `<<extend>>` interfaces (ie, only `<<class>>` and `<<relationship>>` allowed).  
Exception is if that extend interface ONLY has one composition (eg, RemarkItem)
- **RIA07** NO_ATTRIBUTE_GROUP_EXTEND: `<<attributeGroup>>` cannot implement `<<extend>>` interfaces (ie, only `<<class>>` and `<<relationship>>` allowed).
- **RIA08** NO_COMPOUND_ATTRIBUTE_EXTEND: `<<compoundAttribute>>` cannot implement `<<extend>>` interfaces (ie, only `<<class>>` and `<<relationship>>` allowed).

### Remarks
- This rule is wrong and should be ignored at the moment.
- Should this rule be applied to all those classes?
- What about `<<compoundAttribute>>`, `<<exchange>>`, `<<metaclass>>` and `<<umlPrimitive>>`?
- There are many `<<primitive>>`, ... that implement an `<<extend>>` interface that does not own 1 single composition.
- As stated, ASD rules seem contradictory.
- How should we understand composition?
- ApplicabilityStatementItem owns 1 association.
- OrganizationReferencingItem owns 1 association.
- SecurityClassificationItem owns 1 association.


## C45: CLASS(SOME)_NAME_MUST_BE_UPPER_CAMEL_CASE
### Metas
- **since**: 0.1.0

All `[class]` `[names]` must match this pattern: `^[A-Z][a-zA-Z0-9]*$`  
They must be UpperCamelCase.

### Applies to
- Name of all classes, except `<<umlPrimitive>>` and `<<builtin>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 13
- `[UML Writing Rules and Style Guide 2.0]` 13.1.1 for Camel Case
- `[UML Writing Rules and Style Guide 2.0]` 13.1.2 for valid characters

### Related to
- **RCC01** CLASS_NAME_RULES: A class name name must exist, start with a letter, be in UpperCamelCase and may not include spaces.


## C46: CLASS(SOME)_NOTES_ARE_MANDATORY
### Metas
- **since**: 0.1.0

All `[classes]` must have `[notes]`.

### Applies to
- All classes except `<<builtin>>` ones

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.1

### Related to
- **RCD01** CLASS_DEFINITION: Classes must have a definition.


## C47: CLASS(SOME)_TAG(UID_PATTERN)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

### Labels
- **Deprecated**

Some `[classes]` must not have any `[uidPattern tag]`.

### Applies to
- All generalized `<<class>>` classes
- All generalized `<<relationship>>` classes
- All generalized `<<proxy>>` classes
- All `<<attributeGroup>>` classes
- All `<<builtin>>` classes
- All `<<compoundAttribute>>` classes
- All `<<exchange>>` classes
- All `<<metaclass>>` classes
- All `<<primitive>>` classes
- All `<<umlPrimitive>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3

### Related to
- **RCT04** UID_PATTERN_EXCLUSION: uidPattern must not be defined for specialized classes.

### Remarks
- 11.1.3 does not require uidPattern for `<<proxy>>` classes.
- specialized was replaced by generalized.
- A generalized class is the specialization of another class: it extends another class.
- Wording of `[UML Writing Rules and Style Guide 2.0]` and UID_PATTERN_EXCLUSION should be fixed.


## C48: CLASS(SOME)_TAG(UID_PATTERN)_COUNT_MUST_BE_1
### Metas
- **since**: 0.1.0

### Labels
- **Deprecated**
- **XSD_generation**

Some [non-generalized classes] must have exactly 1 `[uidPattern tag]`.

### Applies to
- All non-generalized `<<class>>` classes
- All non-generalized `<<relationship>>` classes
- All non-generalized `<<proxy>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3
- `[UML Writing Rules and Style Guide 2.0]` 11.8.3

### Related to
- **RCT03** UID_PATTERN: There must be exactly one uidPattern for non-specialized classes, relationships and proxies.

### Remarks
- 11.1.3 does not require uidPattern for `<<proxy>>` classes.
- non-specialized was replaced by non-generalized.
- A non-generalized class is not the specialization of any other class: it does not extend any other class.
- Wording of `[UML Writing Rules and Style Guide 2.0]` and UID_PATTERN should be fixed.


## C49: CLASS(SOME)_TAG(XML_NAME)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

Some `[classes]` must not have any `[xmlName tag]`.

### Applies to
- All `<<builtin>>` classes
- All `<<metaclass>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3

### Remarks
- 11.1.3 requires xmlName to be defined for all classes.


## C50: CLASS(SOME)_TAG(XML_NAME)_COUNT_MUST_BE_1
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

Some `[classes]` must have exactly 1 `[xmlName tag]`.

### Applies to
- All classes except `<<builtin>>` and `<<metaclass>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1

### Related to
- **RCT02** CLASS_XML_NAME: Each class must have exactly one xmlName.

### Remarks
- 11.1.3 requires xmlName to be defined for all classes.


## C51: CLASS(SOME)_TAG(XML_REF_NAME)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

Some `[classes]` must not have any `[xmlRefName tag]`.

### Applies to
- All classes except `<<class>>`, `<<relationship>>` and `<<proxy>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3

### Related to
- **RCT05** XML_REF_NAME: Classes, relationships & proxies must have either zero or 1 xmlRefName.


## C52: CLASS(SOME)_TAG(XML_REF_NAME)_COUNT_MUST_BE_0_1
### Metas
- **since**: 0.1.0

A `[class]` can optionally have 1 `[xmlRefName tag]`.

### Applies to
- All `<<class>>` classes
- All `<<relationship>>` classes
- All `<<proxy>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3

### Related to
- **RCT05** XML_REF_NAME: Classes, relationships & proxies must have either zero or 1 xmlRefName.


## C53: CLASS(SOME)_TAG(XML_SCHEMA_NAME)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

Some `[classes]` must not have any `[xmlSchemaName tag]`.

### Applies to
- All classes that are not first indenture level `<<exchange>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3

### Related to
- **RET03** XML_SCHEMA_NAME_EXCL: xmlSchemaName must not be used for `<<exchange>>` classes which are NOT first indenture level specializations of the CDM MessageContent class.


## C54: CLASS(SOME)_TAG(XML_SCHEMA_NAME)_COUNT_MUST_BE_1
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

Some `[classes]` must have exactly 1 `[xmlSchemaName tag]`.

### Applies to
- All first indenture level `<<exchange>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.3

### Related to
- **RET02** XML_SCHEMA_NAME: xmlSchemaName is mandatory for `<<exchange>>` classes which are first indenture level specializations of the CDM MessageContent class.


## C55: CLASS(SOME)_VERSION_IS_MANDATORY
### Metas
- **since**: 0.1.0

Some `[classes]` must have a `[version]`.

### Applies to
- All classes except `<<builtin>>` classes and BaseObject

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.1

### Related to
- **RCM04** CLASS_VERSION: A class must have a version number with a format NNN-NN, where NNN must be three digits and not null and NN must be "00".


## C56: CLASS(UML_PRIMITIVE)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<umlPrimitive>> class]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- ref  
- xmlName

### Applies to
- All `<<umlPrimitive>>` classes

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example


## C57: CLASS(SOME)_MUST_BE_GRAPHICALLY_REPRESENTED
### Metas
- **since**: 0.23.0

All `[classes]` must be graphically represented in at least one `[diagram]`.

### Applies to
- All non-`<<builtin>>` classes

### Related to
- **RCX01** CLASS_USED: Class is used in at least one diagram.


## D01: DIAGRAM_NAME_IS_MANDATORY
### Metas
- **since**: 0.23.0

All `[diagrams]` must have a `[name]`.

### Applies to
- All diagrams

### Related to
- **RDC01** DIAGRAM_NAME: A diagram name must start with the specification number or "CDM", must be written in capitalization style and must not include hyphens or underscores to separate words.


## D02: DIAGRAM_NAME_MUST_FOLLOW_AUTHORING
### Metas
- **since**: 0.23.0

The `[name]` of a `[diagram]` should start with CDM or the specification name.  
It must be written in capitalization style and use space to separate words.

### Applies to
- All diagrams

### Related to
- **RDC01** DIAGRAM_NAME: A diagram name must start with the specification number or "CDM", must be written in capitalization style and must not include hyphens or underscores to separate words.


## D03: DIAGRAM_STEREOTYPES_MUST_BE_ALLOWED
### Metas
- **since**: 0.24.0

All defined `[diagram]` `[stereotypes]` must be allowed.  
They must be one of:  
- `<<noPrint>>`

### Applies to
- All diagrams

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 6.3.1


## G01: CARDINALITY_MUST_BE_VALID
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All well formed `[cardinalities]` must be valid.

### Applies to
- The cardinality of all attributes
- The cardinality of all connector tips

### Related to
- **RAC05** ATTRIBUTE_CARDINALITY: The attribute cardinality must be numeric or be a numeric range (including '*' for unlimited upper range), and not zero.
- **RYT16** COMPOSITION_SOURCE_CARDINALITY: A composition must have  a multiplIcity (cardinality) for the source class.
- **RYT21** AGGREGATION_SOURCE_CARDINALITY: An aggregation must have a multiplicity (cardinality) for the source class.


## G02: CARDINALITY_MUST_BE_WELL_FORMED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All defined `[cardinalities]` must be well formed.

### Applies to
- The cardinality of all attributes
- The cardinality of all connector tips

### Related to
- **RAC05** ATTRIBUTE_CARDINALITY: The attribute cardinality must be numeric or be a numeric range (including '*' for unlimited upper range), and not zero.
- **RYT16** COMPOSITION_SOURCE_CARDINALITY: A composition must have  a multiplIcity (cardinality) for the source class.
- **RYT21** AGGREGATION_SOURCE_CARDINALITY: An aggregation must have a multiplicity (cardinality) for the source class.


## G03: SIBLINGS_MUST_HAVE_DIFFERENT_NAMES
### Metas
- **since**: 0.5.0

### Labels
- **No_UWRSG_source**

Some `[siblings]` must have different `[names]`.

### Applies to
- All sibling classes, interfaces, packages, attributes


## G04: INHERITANCE_MUST_BE_GRAPHICALLY_REPRESENTED
### Metas
- **since**: 0.23.0

All `[inheritances]` must be graphically represented in at least one `[diagram]`.

### Applies to
- All inheritances

### Related to
- **RCX01** CLASS_USED: Class is used in at least one diagram.


## G05: NOTES_MUST_BE_UNICODE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[notes]` must be Unicode.

### Applies to
- The notes of all attributes
- The notes of all classes
- The notes of all connectors
- The notes of all connector tips
- The notes of all interfaces
- The notes of all packages
- The notes of all tags


## G06: NOTES(SOME)_MUST_NOT_CONTAIN_HTML
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[notes]` must not contain any html tag or entity.

### Applies to
- All notes that are used in schema or doc generation
- All attribute notes
- All class notes
- All connector notes
- All connector tip notes
- All interface notes
- All tag notes

### Remarks
- Entities are detected.
- All tags are detected.


## G07: STEREOTYPE_MUST_BE_RECOGNIZED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

All defined `[stereotypes]` must be recognized.

### Related to
- **RAS01** ATTRIBUTE_STEREOTYPES: Attributes can only have the "key", "compositeKey", "relationshipkey", "characteristic" or "metadata" stereotypes. The stereotype is mandatory.
- **RCM01** CLASS_STEREOTYPES: A class must have a stereotype, either `<<class>>`, `<<relationship>>`, `<<attributeGroup>>`, `<<exchange>>` or `<<proxy>>`.
- **RDS01** DIAGRAM_STEREOTYPES: Diagram stereotypes may be only `<<NoPrint>>` or null.
- **RIM01** INTERFACE_STEREOTYPES: An interface must have a stereotype, either `<<select>>` or `<<extend>>`.
- **RPS01** PACKAGE_STEREOTYPES: All package stereotypes (except the model itself) must be `<<Domain>>`, `<<FunctionalArea>>`, `<<UoF>>` or null.


## I01: INTERFACE_ATTRIBUTES_ARE_FORBIDDEN
### Metas
- **since**: 0.1.0

An `[interface]` must not have any `[attribute]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<select>>` interfaces

### Related to
- **RIA04** INTERFACE_NO_ATTRIBUTES: Interfaces may not have any attributes.


## I02: INTERFACE_AUTHOR_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[interfaces]` must have an `[author]`.

### Applies to
- All interfaces

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<select>>`
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.1 for `<<extend>>`


## I03: INTERFACE_INHERITANCE_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

An `[interface]` must not extend another `[interface]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<select>>` interfaces
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.1 for `<<extend>>` interfaces

### Related to
- **RIA09** NO_INTERFACE_EXTEND: Interfaces cannot extend `<<extend>>` interfaces (ie, only `<<class>>` and `<<relationship>>` allowed).
- **RIA10** NO_INTERFACE_SELECT: Interfaces cannot extend `<<select>>` interfaces.


## I04: INTERFACE_MUST_DIRECTLY_EXTEND_AT_MOST_ONCE_AN_INTERFACE
### Metas
- **since**: 0.5.0

### Labels
- **No_UWRSG_source**

A `[interface]` must directly specialize at most once a `[interface]`.


## I05: INTERFACE_MUST_NOT_DECLARE_USELESS_INHERITANCE
### Metas
- **since**: 0.19.0

### Labels
- **No_UWRSG_source**

A `[interface]` must not directly and indireclty inherit from a type.


## I06: INTERFACE_NAME_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[interfaces]` must have a `[name]`.

### Applies to
- All interfaces

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<select>>`
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.1 for `<<extend>>`

### Related to
- **RIC01** INTERFACE_NAME_RULES: An interface name name must exist, start with a letter, be in UpperCamelCase and may not include spaces.


## I07: INTERFACE_NAME_MUST_BE_UPPER_CAMEL_CASE
### Metas
- **since**: 0.1.0

All `[interface]` `[names]` must match this pattern: `^[A-Z][a-zA-Z0-9]*$`  
They must be UpperCamelCase.

### Applies to
- The name of all interfaces

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.3.1

### Related to
- **RIC01** INTERFACE_NAME_RULES: An interface name name must exist, start with a letter, be in UpperCamelCase and may not include spaces.


## I08: INTERFACE_NOTES_ARE_MANDATORY
### Metas
- **since**: 0.1.0

All `[interfaces]` must have `[notes]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<select>>`
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.1 for `<<extend>>`


## I09: INTERFACE_NOTES_MUST_START_WITH_NAME
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[interface]` `[notes]` must start with the `[interface]` `[name]`.


## I10: INTERFACE_STEREOTYPE_IS_MANDATORY
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All `[interfaces]` must have a `[stereotype]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<select>>`
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.1 for `<<extend>>`

### Related to
- **RIM01** INTERFACE_STEREOTYPES: An interface must have a stereotype, either `<<select>>` or `<<extend>>`.


## I11: INTERFACE_STEREOTYPES_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[interface]` `[stereotypes]` must be allowed.  
They must be one of:  
- `<<extend>>`  
- `<<select>>`

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.1

### Related to
- **RIM01** INTERFACE_STEREOTYPES: An interface must have a stereotype, either `<<select>>` or `<<extend>>`.


## I12: INTERFACE_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[interface]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- ref  
- replaces  
- xmlName

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.2 for `<<select>>`
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.2 for `<<extend>>`
- `[UML Writing Rules and Style Guide 2.0]` 11.5.4
- `[UML Writing Rules and Style Guide 2.0]` 11.8.1 for xmlName
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6 for ref
- `[UML Writing Rules and Style Guide 2.0]` 11.8.8 for note
- `[UML Writing Rules and Style Guide 2.0]` 11.8.9 for example
- `[UML Writing Rules and Style Guide 2.0]` 11.8.10 for replaces


## I13: INTERFACE_TAG(XML_REF_NAME)_COUNT_MUST_BE_0
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

An `[interface]` must not have any `[xmlRefName tag]`.


## I14: INTERFACE_VERSION_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[interfaces]` must have a `[version]`.

### Applies to
- All interfaces

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<select>>` interfaces
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.1 for `<<extend>>` interfaces


## I15: INTERFACE_VERSION_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[interface]` `[versions]` must match this pattern: `^[\d][\d][\d]-00$`

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 4.1.3
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.1 for `<<extend>>` interfaces
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.1 for `<<select>>` interfaces


## I16: INTERFACE(EXTEND)_MUST_NOT_BE_ASSOCIATION_TARGET
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

An `[<<extend>> interface]` must not be an `[association]` `[target]`.

### Related to
- **RIA03** EXTEND_NOT_ASSOCIATION_TARGET: `<<extend>>` interfaces must not be used as the target for directed associations.

### Remarks
- Does this apply to compositions and aggregations?


## I17: INTERFACE(EXTEND)_TAG(XML_NAME)_COUNT_MUST_BE_1
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All `[<<extend>> interfaces]` must have exactly 1 `[xmlName tag]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.3.2


## I18: INTERFACE(SELECT)_MUST_NOT_BE_ASSOCIATION_SOURCE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

A `[<<select>> interface]` must not be an `[association]` `[source]`.

### Related to
- **RIA01** SELECT_NOT_ASSOCIATION_SOURCE: `<<select>>` interfaces must not be used as the source for directed associations.

### Remarks
- Does this apply to compositions and aggregations?


## I19: INTERFACE(SELECT)_MUST_NOT_BE_COMPOSITION_PART
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

A `[<<select>> interface]` must not be a `[composition part]`.

### Related to
- **RIA02** SELECT_NOT_COMPOSITION_SOURCE: `<<select>>` interfaces must not be used as the source for compositions.


## I20: INTERFACE(SELECT)_TAG(XML_NAME)_COUNT_MUST_BE_1
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

A `[<<select>> interface]` must have exactly 1 `[xmlName tag]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.5.2.2

### Related to
- **RIT01** SELECT_XML_NAME: `<<select>>` interfaces must have exactly one xmlName tag.


## I21: INTERFACE_MUST_BE_GRAPHICALLY_REPRESENTED
### Metas
- **since**: 0.23.0

All `[interfaces]` must be graphically represented in at least one `[diagram]`.

### Applies to
- All interfaces

### Related to
- **RCX01** CLASS_USED: Class is used in at least one diagram.


## M01: MODEL_ATTRIBUTES_WITH_SAME_NAME_MUST_HAVE_SAME_DEFINITIONS
### Metas
- **since**: 0.15.0

### Labels
- **No_UWRSG_source**

In a `[model]`, 2 `[attributes]` with the same `[name]` must have the same `[notes]`.

### Related to
- **RAC04** ATTRIBUTE_NAME_CONSISTENT: If an attribute name is repeated in another class, then the type and the definition must be the same.


## M02: MODEL_ATTRIBUTES_WITH_SAME_NAME_MUST_HAVE_SAME_TYPES
### Metas
- **since**: 0.15.0

### Labels
- **No_UWRSG_source**

In a `[model]`, 2 `[attributes]` with the same `[name]` must have the same `[types]`.

### Related to
- **RAC04** ATTRIBUTE_NAME_CONSISTENT: If an attribute name is repeated in another class, then the type and the definition must be the same.


## M03: MODEL_ATTRIBUTES_WITH_SAME_NAME_MUST_HAVE_SAME_VALID_VALUES
### Metas
- **since**: 0.15.0

### Labels
- **No_UWRSG_source**

In a `[model]`, 2 `[attributes]` with the same `[name]` must have the same `[validValues]`.

### Related to
- **RAT15** SAME_ATTRIBUTE_VALID_VALUES: Two attributes with the same name must have the same validValues.


## M04: MODEL_CLASS_NAMES_MUST_BE_UNIQUE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

A `[model]` cannot have duplicate `[class]` `[names]`.

### Related to
- **RCC03** CLASS_NAME_UNIQUE: A class name must be unique, except if it is a specialization (extension) of a class of the CDM (same name).


## M05: MODEL_ELEMENT_GUID_MUST_BE_UNIQUE
### Metas
- **since**: 0.22.0

### Labels
- **No_UWRSG_source**




## M06: MODEL_TYPE_TAGS(XML_NAME)_MUST_BE_UNIQUE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

A `[model]` cannot have duplicate `[type]` `[xmlName tag]` `[values]`.

### Related to
- **RTD04** CLASS_XML_NAME_UNIQUE: A class xmlName must be unique in the context of an S-Series ILS specifications data model, including any imported data models.
- **RID01** INTERFACE_XML_NAME_UNIQUE: An interface xmlName must be     unique in the context of an S-Series ILS specifications data model, including any imported data models.


## M07: MODEL_TAGS(UID_PATTERN)_MUST_BE_UNIQUE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

A `[model]` cannot have duplicate `[uidPattern tag]` `[values]`.

### Related to
- **RTD01** UID_PATTERN_UNIQUE: uidPattern must be unique in the context of an S-Series ILS specifications data model, including any imported data models.


## P01: PACKAGE_NAME_IS_MANDATORY
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[packages]` must have a `[name]`.

### Applies to
- All packages


## P02: PACKAGE_NOTES_ARE_MANDATORY
### Metas
- **since**: 0.1.0

All `[packages]` must have `[notes]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 6.1

### Related to
- **RPD01** PACKAGE_DEFINITION: A package must have a definition.


## P03: PACKAGE_STEREOTYPES_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[package]` `[stereotypes]` must be allowed.  
They must be one of:  
- `<<Domain>>`  
- `<<FunctionalArea>>`  
- `<<uof>>`

### Applies to
- All packages except `<<builtin>>` package

### Related to
- **RPS01** PACKAGE_STEREOTYPES: All package stereotypes (except the model itself) must be `<<Domain>>`, `<<FunctionalArea>>`, `<<UoF>>` or null.


## P04: PACKAGE(SOME)_MUST_NOT_CONTAIN_TYPES
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

A `[<<Domain>> package]` or `[<<FunctionalArea>> package]` cannot contain any `[class]` or `[interface]` declarations.

### Related to
- **RXX01** CLASS_IN_UOF: A class may only be defined under an UoF, not under domain or functional area.


## P05: PACKAGE(SOME)_MUST_NOT_HAVE_ANY_TAG
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[<<Domain>> packages]` and `[<<FunctionalArea>> packages]` cannot have any `[tag]`.

### Applies to
- All `<<Domain>>` packages
- All `<<FunctionalArea>>` packages.

### Related to
- **RPT01** PACKAGE_NO_TAGS: A package with "Domain" or "FunctionalArea" stereotypes should have no tags.


## P06: PACKAGE(SOME)_NAME_MUST_BE_CAPITAL_CASE
### Metas
- **since**: 0.1.0

Some `[package]` `[names]`, when defined, must match this pattern: `^[A-Z][A-Za-z0-9]*( (and|or|[A-Z][A-Za-z0-9]*))*$`  
They must be Capital Case.

### Applies to
- All `<<uof>>` packages
- All packages that have no stereotype but are UoF, excluding packages whose name contains 'Primitives', 'Compound_Attributes' or 'Base_Object_Definition',
- All `<<Domain>>` packages
- All `<<FunctionalArea>>` packages

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 6.3.1 for UoF
- `[UML Writing Rules and Style Guide 2.0]` 7.2 for Functional Area
- `[UML Writing Rules and Style Guide 2.0]` 7.3 for Domain

### Related to
- **RPC01** PACKAGE_NAME: A package name (except the root node) must start with the specification number or "CDM", must be written in capitalization style and must not include hyphens or underscores to separate words.

### Remarks
- 'Product and Package' is not strictly Capital Case, but the pattern accepts it.


## P07: PACKAGE(SOME)_STEREOTYPE_IS_MANDATORY
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

Some `[packages]` must have a `[stereotype]`.

### Applies to
- All packages whose parent package has a stereotype

### Related to
- **RPS01** PACKAGE_STEREOTYPES: All package stereotypes (except the model itself) must be `<<Domain>>`, `<<FunctionalArea>>`, `<<UoF>>` or null.


## P08: PACKAGE(SOME)_STEREOTYPE_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

Some `[packages]` must not have any `[stereotype]`.

### Applies to
- All root packages, except the `<<builtin>>` package

### Related to
- **RPS01** PACKAGE_STEREOTYPES: All package stereotypes (except the model itself) must be `<<Domain>>`, `<<FunctionalArea>>`, `<<UoF>>` or null.


## P09: PACKAGE(UOF)_NOTES_MUST_START_WITH_NAME
### Metas
- **since**: 0.1.0

All defined `[package]` `[notes]` must start with the `[package]` `[name]`.  
It should start with 'The xxx UoF' or 'xxx UoF'.

### Applies to
- All `<<uof>>` packages
- All packages without any stereotype but are however UoF.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 6.7

### Related to
- **RPD02** PACKAGE_DEFINITION_AUTHORING: A package definition must start with the package name.

### Remarks
- Rule was relaxed when compared to `[UML Writing Rules and Style Guide 2.0]`


## P10: PACKAGE(UOF)_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

All defined `[<<uof>> or no stereotype package]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- ICN  
- longDescription  
- replaces

### Applies to
- All `<<uof>>` packages
- All package without any stereotype but are however UoF.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 6.3.1 (ICN)
- `[UML Writing Rules and Style Guide 2.0]` 6.7 (long description)

### Related to
- **RPT02** UOF_TAGS: A  package with no stereotype or "UoF" one can have only the following tags: "NoPrint", "ICN", "replaces".

### Remarks
- NoPrint is ignored. As a tag it is not described in `[UML Writing Rules and Style Guide 2.0]`.
- There is a `<<noPrint>>` stereotype.
- replaces seems to be usable with classes and interfaces according to `[UML Writing Rules and Style Guide 2.0]` 11.8.10.


## T01: TAG_NAME_IS_MANDATORY
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[tags]` must have a `[name]`.

### Applies to
- All attribute tags
- All connector tags
- All class tags
- All interface tags
- All package tags

### Related to
- **RYT04** AGGREGATION_TAGS: An aggregation can have only the following tags: xmlName, note, example, ref.
- **RYT01** ASSOCIATION_TAGS: An association can have the following tags: xmlName, note, example, ref, noKey.
- **RAT01** ATTRIBUTE_TAGS: An attribute can only have the following tags: xmlName, validValue, validValueLibrary, unit, note, example, source and ref.
- **RGT01** ATTRIBUTE_GROUP_TAGS: Allowed tags for attribute groups are xmlName, note, example, replaces, source, ref and changeNote.
- **RCT01** CLASS_TAGS: Allowed tags for classes and relationships are uidPattern, xmlName, xmlRefName, note, example, replaces, source, ref and changeNote.
- **RYT07** COMPOSITION_TAGS: A composition can have only the following tags: xmlName, note, example, ref
- **RET01** EXCHANGE_TAGS: Allowed `<<exchange>>` tags are xmlName, xmlSchemaName, note, example, replaces, source, ref and changeNote.


## T02: TAG_NAME_MUST_BE_RECOGNIZED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[tag]` `[names]` must be recognized.

### Applies to
- All attribute tag names
- All connector tag names
- All class tag names
- All interface tag names
- All package tag names

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8

### Related to
- **RYT04** AGGREGATION_TAGS: An aggregation can have only the following tags: xmlName, note, example, ref.
- **RYT01** ASSOCIATION_TAGS: An association can have the following tags: xmlName, note, example, ref, noKey.
- **RAT01** ATTRIBUTE_TAGS: An attribute can only have the following tags: xmlName, validValue, validValueLibrary, unit, note, example, source and ref.
- **RGT01** ATTRIBUTE_GROUP_TAGS: Allowed tags for attribute groups are xmlName, note, example, replaces, source, ref and changeNote.
- **RCT01** CLASS_TAGS: Allowed tags for classes and relationships are uidPattern, xmlName, xmlRefName, note, example, replaces, source, ref and changeNote.
- **RYT07** COMPOSITION_TAGS: A composition can have only the following tags: xmlName, note, example, ref
- **RET01** EXCHANGE_TAGS: Allowed `<<exchange>>` tags are xmlName, xmlSchemaName, note, example, replaces, source, ref and changeNote.


## T03: TAG_VALUE_MUST_BE_UNICODE
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All `[tag]` `[values]` must be Unicode.

### Applies to
- All attribute tag values
- All connector tag values
- All class tag values
- All interface tag values
- All package tag values


## T04: TAG(LONG_DESCRIPTION)_NOTES_MUST_HAVE_VALID_MARKUP
### Metas
- **since**: 0.17.0

The `[longDescription tag]` `[notes]` must have valid markup.

### Applies to
- All longDescription notes


## T05: TAG(NOTE)_VALUE_MUST_BE_UNIQUE
### Metas
- **since**: 0.17.0

### Labels
- **No_UWRSG_source**

All `[item]` `[note tag]` `[values]` must be unique.

### Applies to
- All attributes
- All connectors
- All classes
- All interfaces


## T06: TAG(REF)_VALUE_SHOULD_CONTAIN_ONE_CONCEPT
### Metas
- **since**: 0.1.0

Each `[ref tag]` `[value]` that contains one token should be a recognized concept.

### Applies to
- All ref tag values that contain one token.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.6

### Related to
- **RAT14** ATTRIBUTE_REF_INCORRECT: An interface reference must reference an existing class or interface.
- **RCT07** CLASS_REF_INCORRECT: A class reference must reference an existing class or interface.
- **RIT02** INTERFACE_REF_INCORRECT: An interface reference must reference an existing class or interface.
- **RYT22** ASSOCIATION_REF_INCORRECT: An association reference must reference an existing class or interface.

### Remarks
- As ref tag can also refer to external sources of information, it can produce false positives.
- Classes, interfaces and attributes names are considered as concepts: should we exclude attributes?


## T07: TAG(REPLACES)_VALUE_MUST_EXIST_IN_REF_MODEL
### Metas
- **since**: 0.15.0

All `[replaces tag]` `[values]` must exist in the `[ref model]`.

### Applies to
- All replaces tag values (if a reference model is passed)

### Remarks
- Choice of reference model impacts results.


## T08: TAG(SOME)_VALUE_MUST_CONTAIN_ONE_TOKEN
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

Each `[ref tag]`, `[unit tag]` or `[validValue tag]` `[value]` must contain one token.

### Applies to
- All ref tag values
- All unit tag values
- All validValue tag values


## T09: TAG(UID_PATTERN)_MUST_BE_LOWER_CASE
### Metas
- **since**: 0.1.0

### Labels
- **Deprecated**
- **XSD_generation**

All `[uidPattern tag]` `[values]` must match this pattern: `^[a-z]+$`  
They must be lower case.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.3

### Related to
- **RTD03** UID_PATTERN_CASE: uidPattern must be written using only lowercase characters.


## T10: TAG(UID_PATTERN)_VALUE_SHOULD_BE_AT_MOST_8_CHARS
### Metas
- **since**: 0.1.0

### Labels
- **Deprecated**
- **XSD_generation**

All defined `[uidPattern tag]` `[values]` should be at most 8 chars.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.3

### Related to
- **RTD02** UID_PATTERN_LENGTH: uidPattern should be as short as possible and should not exceed eight character.


## T11: TAG(XML_NAME)_VALUE_MUST_BE_LOWER_CAMEL_CASE
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All `[xmlName tag]` `[values]` must match this pattern: `^[a-z][a-zA-Z0-9]*$`  
They must be lowerCamelCase.

### Applies to
- All attribute xmlName tag values
- All class xmlName tag values
- All interface xmlName tag values

### Sources
- `[UML Writing Rules and Style Guide 2.0]`  11.8.1

### Related to
- **RTD05** XML_NAME_CASE: xmlName must be defined in lowerCamelCase for classes, interfaces and attributes.


## T12: TAG(XML_REF_NAME)_VALUE_MUST_FOLLOW_AUTHORING
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

All `[xmlRefName tag]` `[values]` must be built by appending 'Ref' to the `[xmlName tag]` `[value]`.

### Applies to
- All xmlRefName tag values

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.8.2

### Related to
- **RCT06** XML_REF_NAME_DEF: xmlRefName must be equal to xmlName + 'Ref'.


## X01: AGGREGATION_PART_CARDINALITY_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[aggregation]` `[part tips]` must have a `[cardinality]`.

### Applies to
- The part tip of all aggregations

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.2.1

### Related to
- **RYT21** AGGREGATION_SOURCE_CARDINALITY: An aggregation must have a multiplicity (cardinality) for the source class.

### Remarks
- Should we consider that a missing cardinality is equivalent to 1?
- In that case, `[UML Writing Rules and Style Guide 2.0]` should be modified.


## X02: AGGREGATION_PART_TYPE(SOME)_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

The `[part tip]` `[type]` of an `[aggregation]` cannot be a `<<relationship>>` or `<<compoundAttribute>>`.

### Applies to
- The part tip of all aggregations

### Related to
- **RYT11** RELATIONSHIP_AGGREGATION: An aggregation  must not use a relationship as its part.
- **RUY02** COMPOUND_ATTRIBUTE_AGGREGATIONS: A compoundAttribute cannot be part of an aggregation or a composition.


## X03: AGGREGATION_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[aggregation]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- ref  
- xmlName

### Applies to
- All recognized tag names of all aggregations

### Related to
- **RYT04** AGGREGATION_TAGS: An aggregation can have only the following tags: xmlName, note, example, ref.


## X04: AGGREGATION_TAG(XML_NAME)_COUNT_MUST_BE_0_1
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

An `[aggregation]` can optionally have 1 `[xmlName tag]`.

### Applies to
- All aggregations

### Related to
- **RYT06** AGGREGATION_XML_NAME_UNIQUE: An aggregation cannot have more than one xmlName


## X05: AGGREGATION_WHOLE_CARDINALITY_MUST_BE_ONE
### Metas
- **since**: 0.1.0

### Labels
- **Contradiction**
- **XSD_generation**

When the effective `[cardinality]` of the `[whole tip]` of an `[aggregation]` is valid, it must be '1..1'.

### Applies to
- The cardinality of the whole tip of all aggregations

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.2.1

### Related to
- **RYT20** AGGREGATION_TARGET_CARDINALITY: The target multiplicity (cardinality) of an aggregation must be either not filled in or 1.

### Remarks
- This rule contradicts 11.4.2.1 that say that this can be any cardinality for aggregations.


## X06: AGGREGATION_WHOLE_ROLE_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

The `[whole tip]` of an `[aggregation]` must not have any `[role]`.

### Applies to
- The whole tip of all aggregations

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.2.1

### Related to
- **RYT19** AGGREGATION_TARGET: An aggregation must not have a target role.


## X07: ASSOCIATION(FROM_EXTEND)_TAG(NO_KEY)_COUNT_MUST_BE_1
### Metas
- **since**: 0.11.0

### Labels
- **XSD_generation**

Some `[associations]` must have exactly 1 `[noKey tag]`.

### Applies to
- All directed associations whose source is an `<<extend>>` interface

### Remarks
- [#32] Since 2024-02-17. DMEWG 35.01: All directed associations of `<<extend>>` interface MUST be tagged noKey.
- An association is directed when its target is navigable and its source is not navigable.


## X08: ASSOCIATION_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[association]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- noKey  
- ref  
- xmlName

### Applies to
- All recognized tag names of all associations

### Related to
- **RYT01** ASSOCIATION_TAGS: An association can have the following tags: xmlName, note, example, ref, noKey.


## X09: ASSOCIATION_TAG(XML_NAME)_COUNT_MUST_BE_0_1
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

An `[association]` can optionally have 1 `[xmlName tag]`.

### Applies to
- All associations

### Related to
- **RYT03** ASSOCIATION_XML_NAME_UNIQUE: An association cannot have more than one xmlName.


## X10: ASSOCIATION_TARGET_ROLE_IS_MANDATORY
### Metas
- **since**: 0.1.0

The `[target tip]` of an `[association]` must have a `[role]`.

### Applies to
- The target tip of all associations

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.2.2.1
- `[UML Writing Rules and Style Guide 2.0]` 11.4.1.1

### Related to
- **RYC02** ASSOCIATION_HAS_TARGET: All directed associations must have a target role.


## X11: ASSOCIATION_TIP_ROLE_MUST_BE_LOWER_CAMEL_CASE
### Metas
- **since**: 0.1.0

All defined `[association]` `[tip]` `[roles]` must match this pattern: `^[a-z][a-zA-Z0-9]*$`  
They must be lowerCamelCase.

### Applies to
- All defined association tip roles

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.13.1


## X12: ASSOCIATION(TO_RELATIONSHIP)_SOURCE_CARDINALITY_MUST_BE_ONE
### Metas
- **since**: 0.1.0

When the effective `[cardinality]` of the `[source tip]` of some `[associations]` is valid, it must be '1..1'.

### Applies to
- The source tip of all associations targeting a `<<relationship>>` class

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.2.2.1


## X13: ASSOCIATION(TO_RELATIONSHIP)_TARGET_CARDINALITY_MUST_ZERO_TO_MANY_OR_ONE_TO_MANY
### Metas
- **since**: 0.1.0

When the effective `[cardinality]` of the `[target tip]` of some `[associations]` is valid, it must be '0..*', or '1..*'.

### Applies to
- The target tip of all associations targeting a `<<relationship>>` class

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.1.2.2.1


## X14: COMPOSITION_PART_CARDINALITY_IS_MANDATORY
### Metas
- **since**: 0.1.0

All `[composition]` `[part tips]` must have a `[cardinality]`.

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.2.1

### Applies to
- The cardinality of the part tip of all compositions

### Related to
- **RYT16** COMPOSITION_SOURCE_CARDINALITY: A composition must have  a multiplIcity (cardinality) for the source class.

### Remarks
- Should we consider that a missing cardinality is equivalent to 1?
- In that case, `[UML Writing Rules and Style Guide 2.0]` should be modified.


## X15: COMPOSITION_PART_TYPE_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**
- **XSD_generation**

The `[part tip]` `[type]` of a `[composition]` cannot be a `<<relationship>>` or a `<<compoundAttribute>>`.

### Applies to
- The part tip of all compositions

### Related to
- **RYT10** RELATIONSHIP_COMPOSITION: A composite association must not use a relationship as its part.
- **RUY02** COMPOUND_ATTRIBUTE_AGGREGATIONS: A compoundAttribute cannot be part of an aggregation or a composition.


## X16: COMPOSITION_TAG_NAME_MUST_BE_ALLOWED
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

All defined `[composition]` `[tag]` `[names]` must be allowed.  
They must be one of:  
- example  
- note  
- ref  
- xmlName

### Applies to
- All defined tag names of all composition

### Related to
- **RYT07** COMPOSITION_TAGS: A composition can have only the following tags: xmlName, note, example, ref


## X17: COMPOSITION_TAG(XML_NAME)_COUNT_MUST_BE_0_1
### Metas
- **since**: 0.1.0

### Labels
- **No_UWRSG_source**

A `[composition]` can optionally have 1 `[xmlName tag]`.

### Applies to
- All compositions

### Related to
- **RYT09** COMPOSITION_XML_NAME_UNIQUE: A composition cannot have more than one xmlName


## X18: COMPOSITION_WHOLE_CARDINALITY_MUST_BE_ONE
### Metas
- **since**: 0.1.0

### Labels
- **XSD_generation**

When the effective `[cardinality]` of the `[whole tip]` of a `[composition]` is valid, it must be '1..1'.

### Applies to
- The cardinality of the whole tip of all compositions

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.2.1

### Related to
- **RYT18** COMPOSITION_TARGET_CARDINALITY: The target multiplicity (cardinality) of a composition must be either not filled in or 1.


## X19: COMPOSITION_WHOLE_ROLE_IS_FORBIDDEN
### Metas
- **since**: 0.1.0

The `[whole tip]` of a `[composition]` must not have any `[role]`.

### Applies to
- The whole tip of all compositions

### Sources
- `[UML Writing Rules and Style Guide 2.0]` 11.4.2.1

### Related to
- **RYT17** COMPOSITION_TARGET: A composition must not have a target role.


## X20: CONNECTOR_MUST_BE_GRAPHICALLY_REPRESENTED
### Metas
- **since**: 0.23.0

All `[connectors]` must be graphically represented in at least one `[diagram]`.

### Applies to
- All connectors

### Related to
- **RCX01** CLASS_USED: Class is used in at least one diagram.
