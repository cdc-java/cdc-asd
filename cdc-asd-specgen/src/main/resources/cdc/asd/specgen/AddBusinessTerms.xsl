<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema" 
	xmlns:d="urn:x-dmewg" 
	exclude-result-prefixes="#all">
	<!--
	~~~~
	AddBusinessTerms - v0.3

	This stylesheet adds terms and abbreviation to each letter data module for SX001G.
	Usage
		Use this stylesheet onto every single data module. The resulting output will 
		contain the input, as well as the terms whose name start with the same letter
		than the other terms in that data module.

	Settings
		The terms list is managed in a variable named `addedTermsTable`. You can add or
		remove terms and abbreviations from there You can also introduce S1000D change marks 
		which will be copied in the output.
		
	Process:
		The xslt will detect the correct letter based on the first letter of the AssyCode of the DMC.
		It will further add new terms and abbreviations from the parameter path provided. 
		These should be sorted into the list alphabetically (ignoring casing)
		Furthermore, @reasonForUpdaterefIds listed on the terms them selves
		or on <referTo> elements should lead to entries into the <reasonForUpdate> Section in the header of the data module
		unless they are already present. 
		
		A list of predefined RFUs is provided in the variable $reasonForUpdates. 
		
		Note: the XML input for new terms and abbreviations is not 100% s1000d and needs to be transformed to 
		S1000D <brDoc> as well. 
	~~~~
	-->
	<!-- omit xml declaration because we keep it in the root template -->
	<xsl:output indent="yes" encoding="UTF-8" method="xml" omit-xml-declaration="yes"/>
	
	<xsl:param name="addedTermsTablePath" as="xs:string">addedTerms.xml</xsl:param>
	
	<xsl:variable name="currDMC" select="/dmodule/identAndStatusSection/dmAddress/dmIdent/dmCode"/>
	<xsl:variable name="currLetter" select="/dmodule/identAndStatusSection/dmAddress/dmIdent/dmCode/d:firstLetter(@assyCode)"/>
	
	<xsl:mode on-no-match="shallow-copy"/>
	<xsl:mode on-no-match="shallow-copy" name="transformToBRDoc"/>
	<xsl:mode on-no-match="shallow-copy" name="enrichBR"/>
	
	<xsl:variable name="addedTermsTable" as="element(*)*">
		<xsl:choose>
			<xsl:when test="doc-available($addedTermsTablePath)">
				<!-- only take and transform the relevant ones -->
				<xsl:sequence select="document($addedTermsTablePath)/terms/*"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:message terminate="no">Provided added terms file does not exist. This execution will not add any terms or abbreviations to the glossary files.</xsl:message>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	
	<xsl:variable name="transformedTerms" as="element(brLevelledPara)*">
		<xsl:apply-templates select="$addedTermsTable[name[starts-with(upper-case(.), $currLetter)]]" mode="transformToBRDoc"/>
	</xsl:variable>
	
	<xsl:variable name="reasonForUpdates" as="element(reasonForUpdate)*">
		<reasonForUpdate id="DMEWG-CDM2024-BROKEN-REFERENCES" updateReasonType="urt02" updateHighlight="1">
			<simplePara>Removed broken references</simplePara>
		</reasonForUpdate>
		<reasonForUpdate id="DMEWG-CDM2024-PARTASDESIGNED" updateReasonType="urt02" updateHighlight="1">
			<simplePara>Updated reference further to rename of entity</simplePara>
		</reasonForUpdate>
	</xsl:variable>
	
	<xsl:template match="brDoc">
		
		<xsl:variable name="enrichedBRParas" as="element(brLevelledPara)*">
			<xsl:apply-templates select="brLevelledPara" mode="enrichBR"/>
		</xsl:variable>
		
		<xsl:copy>
			<!-- combine existing and new entries. reprint them sorted by title -->
			<xsl:apply-templates select="$enrichedBRParas | $transformedTerms">
				<xsl:sort order="ascending" select="upper-case(title)" data-type="text"/>
			</xsl:apply-templates>
			
		</xsl:copy>
	</xsl:template>
	
	<!-- SECTION: TRANSFORM TO S1000D BRDOC FORMAT -->
	
	<xsl:template match="term | abbreviation" mode="transformToBRDoc">
		<brLevelledPara id="{d:getId(name)}">
			<!-- take over the attributes from the input XML,
				those are @changeType and @reasonForUpdateRefIds - so valid S1000D attributes -->
			<xsl:apply-templates select="@*"/>
			<!-- QUESTION: why is @changeType not also in the input-XML? -->
			<xsl:sequence select="d:markChanges(.)"/>
			
			<title><xsl:value-of select="name"/></title>
			<!-- call in explicit order -->
			<xsl:apply-templates select="definition" mode="#current"/>
			<xsl:if test="note">
				<para>
					<randomList listItemPrefix="pf02">
						<title><xsl:value-of select="
							if (count(example)>1) then 'Notes'
							else 'Note'"/>
						</title>
						<xsl:apply-templates select="note" mode="#current"/>
					</randomList>
				</para>
			</xsl:if>
			<xsl:if test="example">
				<para>
					<randomList listItemPrefix="pf02">
						<title><xsl:value-of select="
							if (count(example)>1) then 'Examples'
							else 'Example'"/>
						</title>
						<xsl:apply-templates select="example" mode="#current"/>
					</randomList>
				</para>
			</xsl:if>
			<xsl:if test="referTo">
				<para>
					<randomList> <!-- QUESTION: no explicit prefix definition here? -->
						<title>References</title>
						<xsl:apply-templates select="referTo" mode="#current"/>
					</randomList>
				</para>
			</xsl:if>
			<para>
				<randomList listItemPrefix="pf02">
					<title>Type</title>
					<!-- QUESTION: did PH approve this, a list with a single entry? -->
					<listItem>
						<para>
							<xsl:choose>
								<xsl:when test="self::term">Business Term</xsl:when>
								<xsl:when test="self::abbreviation">Acronym</xsl:when>
								<xsl:otherwise>None</xsl:otherwise>
							</xsl:choose>
						</para>
					</listItem>
				</randomList>
			</para>
		</brLevelledPara>
	</xsl:template>

	<xsl:template match="definition" mode="transformToBRDoc">
		<para>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:sequence select="d:markChanges(.)"/>
			<xsl:apply-templates select="node()" mode="#current"/>
		</para>
	</xsl:template>
	
	<xsl:template match="note | example | referTo" mode="transformToBRDoc">
		<listItem>
			<xsl:apply-templates select="@*" mode="#current"/>
			<xsl:sequence select="d:markChanges(.)"/>
			<para><xsl:value-of select="."/>
				<xsl:if test="self::referTo">
					<xsl:text>, refer to </xsl:text>
					<xsl:sequence select="d:createRef(.)"/>
				</xsl:if>
			</para>
		</listItem>
	</xsl:template>
	
	<xsl:function name="d:markChanges" as="attribute(changeMark)?">
		<xsl:param name="context" as="element(*)"/>
		<xsl:if test="$context/@changeType">
			<xsl:attribute name="changeMark">1</xsl:attribute>
		</xsl:if>
		
	</xsl:function>
	
	<xsl:function name="d:createRef" as="element(*)?">
		<xsl:param name="targetTerm" as="xs:string"/>
		
		<xsl:choose>
			<xsl:when test="starts-with(upper-case($targetTerm), $currLetter)">
				<internalRef internalRefTargetType="irtt01" internalRefId="{d:getId($targetTerm)}"/>
			</xsl:when>
			<xsl:otherwise>
				<dmRef referredFragment="{d:getId($targetTerm)}">
					<dmRefIdent>
						<dmCode>
							<xsl:copy-of select="$currDMC/@*"/>
							<xsl:attribute name="subSystemCode" select="d:subSystemCode(d:convertLetterToChapterNumber(d:firstLetter($targetTerm)))"/>
							<xsl:attribute name="subSubSystemCode" select="d:subSubSystemCode(d:convertLetterToChapterNumber(d:firstLetter($targetTerm)))"/>
							<xsl:attribute name="assyCode" select="d:firstLetter($targetTerm) || '000'"/>
						</dmCode>
					</dmRefIdent>
					<dmRefAddressItems>
						<dmTitle>
							<techName>Glossary - <xsl:value-of select="d:firstLetter($targetTerm)"/></techName>
						</dmTitle>
					</dmRefAddressItems>
				</dmRef>
			</xsl:otherwise>
		</xsl:choose>
		
	</xsl:function>
	
	<!-- END SECTION: TRANSFORM TO S1000D BRDOC FORMAT -->

	<!-- QUESTION: is that necessary to keep the doctype in your execution environment? 
				if yes, this should be done by the tool executing the XSLT (in my opinion) -->
	<xsl:template match="/">
		<xsl:value-of disable-output-escaping="yes" select="replace(
                                unparsed-text(base-uri()), 
                                '^(&lt;\?xml[^>]+\?>[\r\n\t\s]+&lt;!DOCTYPE.+?\]>).+?&lt;.*', 
                                '$1', 
                                's')"/>
		<xsl:apply-templates/>
	</xsl:template>
	
	<!-- add (data model) to existing brLevelledPara if it collides with an added term -->
	<xsl:template match="brLevelledPara/title/text()" mode="enrichBR">
		<xsl:next-match/>
		<xsl:if test="d:inTermsTable(..) and not(starts-with(../../@id, 'glossary-member-term'))">
			<xsl:text> (data model)</xsl:text>
		</xsl:if>
	</xsl:template>
	
	<!-- include RFUs -->
	
	<xsl:template match="/dmodule/identAndStatusSection/dmStatus">
		<xsl:variable name="dmStatus" select="current()"/>
		<xsl:copy>
			<xsl:apply-templates select="*"/>
			
			<xsl:apply-templates select="$transformedTerms//@reasonForUpdateRefIds" mode="handleRFUs">
				<xsl:with-param name="existingRFUs" select="reasonForUpdate/@id"/>
			</xsl:apply-templates>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="@reasonForUpdateRefIds" mode="handleRFUs">
		<xsl:param name="existingRFUs" as="xs:string*"/>
		
		<xsl:if test="not(. = $existingRFUs)">
			<xsl:choose>
				<!-- take RFU from provided list, if it matches, otherwise construct one -->
				<xsl:when test="$reasonForUpdates[@id = current()]">
					<xsl:copy-of select="$reasonForUpdates[@id = current()]"/>
				</xsl:when>
				<xsl:otherwise>
					<reasonForUpdate id="{.}" updateReasonType="urt01" updateHighlight="1">
						<simplePara>No justification provided</simplePara>
					</reasonForUpdate>
				</xsl:otherwise>
			</xsl:choose>
			
		</xsl:if>
		
	</xsl:template>
	
	<xsl:function name="d:inTermsTable" as="xs:boolean">
		<xsl:param name="name" as="xs:string"/>
		<xsl:sequence select="exists($addedTermsTable[name = $name])"/>
	</xsl:function>
	
	<xsl:function name="d:getId" as="xs:string">
		<xsl:param name="name" as="xs:string"/>
		<xsl:choose>
			<xsl:when test="d:inTermsTable($name)">
				<xsl:value-of select="'glossary-member-term-' || encode-for-uri(replace($name, ' ', '_'))"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="'glossary-member-' || encode-for-uri(replace($name, ' ', '_'))"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:function>
	

	<xsl:function name="d:firstLetter" as="xs:string">
		<xsl:param name="value" as="xs:string"/>
		<xsl:value-of select="upper-case(substring($value, 1, 1))"/>
	</xsl:function>
	<xsl:function name="d:subSystemCode" as="xs:string">
		<xsl:param name="code" as="xs:string"/>
		<xsl:value-of select="substring($code, 1, 1)"/>
	</xsl:function>
	<xsl:function name="d:subSubSystemCode" as="xs:string">
		<xsl:param name="code" as="xs:string"/>
		<xsl:value-of select="substring($code, 2, 1)"/>
	</xsl:function>
	<xsl:function name="d:convertLetterToChapterNumber" as="xs:string">
		<xsl:param name="letter" as="xs:string"/>
		<xsl:if test="string-length($letter) ne 1">
			<xsl:message terminate="yes">Bad input. This function's argument takes a single letter. Input: <xsl:value-of select="$letter"/>
			</xsl:message>
		</xsl:if>
		<xsl:value-of select="format-number(string-to-codepoints($letter)[1]-64, '00')"/>
	</xsl:function>
</xsl:stylesheet>
