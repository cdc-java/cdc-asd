package cdc.asd.specgen.formatter;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.specgen.datamodules.GlossaryMember;
import cdc.asd.specgen.datamodules.UmlObjectDefinition;
import cdc.asd.specgen.s1000d5.S1000DNode;

/**
 * A formatter context, specifically for the glossary specification.
 * The context keeps hold of some data such as the term that is being written
 * into a data module as well as a terms map.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class GlossaryFormatterContext extends UmlDefinitionFormatterContext {

    private final Map<String, GlossaryMember> termsMap;

    private GlossaryFormatterContext(S1000DNode currentNode,
                                     GlossaryFormatterContext original) {
        this(original);
        this.currentNode = currentNode;
    }

    /**
     * Create a new context on the basis of a terms map.
     *
     * @param termsMap A mapping from a {@code String} to a {@code GlossaryDataModule}
     * @param redactedExpressions Expressions to redact during the formatting process
     */
    public GlossaryFormatterContext(Map<String, GlossaryMember> termsMap,
                                    Collection<String> redactedExpressions) {
        this.redactedExpressions =
                Stream.concat(this.redactedExpressions.stream(),
                              redactedExpressions.stream())
                      .collect(Collectors.toUnmodifiableSet());

        this.termsMap = Map.copyOf(termsMap);
    }

    private GlossaryFormatterContext(GlossaryFormatterContext original) {
        this(original.getCurrentTerm(), original);
    }

    private GlossaryFormatterContext(String term,
                                     GlossaryFormatterContext original) {
        super(term, original);
        this.termsMap = original.termsMap;
    }

    @Override
    public GlossaryFormatterContext currentNode(S1000DNode currentNode) {
        return new GlossaryFormatterContext(currentNode, this);
    }

    @Override
    public Set<String> getKeywords() {
        return termsMap.keySet();
    }

    /**
     * Create a new context on the basis of this one, with the supplied term
     * which will become that new context's current term.
     *
     * @param term The current term of that new context
     * @return A new context
     */
    public GlossaryFormatterContext currentTerm(String term) {
        return new GlossaryFormatterContext(term, this);
    }

    /**
     * Get the terms map
     *
     * @return An unmodifiable list with the terms.
     */
    @Override
    public Map<String, UmlObjectDefinition> getTermsMap() {
        return termsMap.entrySet()
                       .stream()
                       .map(entry -> Map.entry(entry.getKey(), (UmlObjectDefinition) entry.getValue()))
                       .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }
}