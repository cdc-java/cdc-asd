package cdc.asd.specgen.formatter;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import cdc.asd.specgen.datamodules.DataModules;
import cdc.asd.specgen.datamodules.UofForDataModule;
import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;
import cdc.io.data.Element;

/**
 * This formatter class appends a "Refer to" text element whenever a class
 * name is encountered in the source text, except if the class name matches
 * the one provided in the {@link FormatterContext} context object, or if it
 * has already been mentioned, in such case there already is a "Refer to" link.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class ClassReferToFormatter extends Formatter {

    private final DataModelChapterFormatterContext context;

    protected ClassReferToFormatter(DataModelChapterFormatterContext context) {
        super();
        this.context = context;
    }

    @Override
    public List<FormattingBoundary> getTextFormattingBoundaries(String sourceText) {
        final String sourceTextRedacted = redact(sourceText);

        final String currentClass = context.getCurrentClass();

        final Formatter verbatimFormatter =
                Formatter.createFormatter(FormattingPolicy.CLASS_VERBATIM,
                                          context);

        final List<FormattingBoundary> verbatimClassFormattingBoundaries =
                verbatimFormatter.getTextFormattingBoundaries(sourceTextRedacted);

        return verbatimClassFormattingBoundaries.stream()
                                                .sorted(Comparator.comparingInt(FormattingBoundary::getBeginning))
                                                .filter(item -> !item.getMatchedGroup()
                                                                     .equals(currentClass))
                                                .collect(ArrayList::new,
                                                         this::addIfFirstOccurrence,
                                                         List::addAll);
    }

    private void addIfFirstOccurrence(List<FormattingBoundary> boundaries,
                                      FormattingBoundary boundary) {
        final String matchedGroup = boundary.getMatchedGroup();

        if (boundaries.parallelStream()
                      .map(FormattingBoundary::getMatchedGroup)
                      .anyMatch(matchedGroup::equals)) {

            return;
        }

        boundaries.add(new FormattingBoundaryInstance(boundary.getBeginning(),
                                                      boundary.getEnd(),
                                                      matchedGroup,
                                                      this));
    }

    @Override
    public void formatText(Element element,
                           String sourceText) {

        final UofForDataModule relatingUof = context.getUof(sourceText);

        String classReference;

        if (relatingUof.uofMemberIsClass(sourceText)) {
            classReference = relatingUof.getClassParagraphIdByClassName(sourceText);
        } else {
            classReference = relatingUof.getInterfaceParagraphIdByInterfaceName(sourceText);
        }

        element.addText(" (Refer to ");

        if ((context.getCurrentClass() != null
                && relatingUof.equals(context.getUof(context.getCurrentClass())))
                || context.getCurrentUof().equals(relatingUof.getUofName())) {
            element.addChild(DataModules.createInternalRef(classReference));
        } else {
            element.addChild(DataModules.createDmRef(classReference,
                                                     relatingUof));
        }
        element.addText(")");

    }

    @Override
    protected FormatterContext getContext() {
        return context;
    }

    @Override
    protected int getCompareKey() {
        return super.getCompareKey() + 1;
    }
}
