package cdc.asd.specgen.formatter;

/**
 * Formatting boundaries define beginning and end
 * indexes that identify the substring on which to apply
 * some formatting rules.
 *
 * This construct is not meant for direct instantiation. Only
 * subclasses of {@link Formatter} can create these instances.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public interface FormattingBoundary extends Comparable<FormattingBoundary> {

    /**
     * Return the index of the beginning of the
     * boundaries.
     *
     * @return The beginning index.
     */
    public int getBeginning();

    /**
     * Return the index of the end of the boundaries.
     *
     * @return The end index.
     */
    public int getEnd();

    /**
     * Get the whole matched group.
     *
     * @return The matched group.
     */
    public String getMatchedGroup();

    /**
     * Get the formatter from which these boundaries originate
     *
     * @return The originating formatter.
     */
    public Formatter getFormatter();

    @Override
    default int compareTo(FormattingBoundary other) {
        return (getBeginning() + getFormatter().getCompareKey())
                - (other.getBeginning() + other.getFormatter().getCompareKey());

    }
}