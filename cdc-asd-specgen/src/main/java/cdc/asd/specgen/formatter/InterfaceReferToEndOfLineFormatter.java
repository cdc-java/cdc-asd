package cdc.asd.specgen.formatter;

import java.util.List;
import java.util.Set;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cdc.asd.specgen.datamodules.DataModules;
import cdc.asd.specgen.datamodules.UofForDataModule;
import cdc.io.data.Element;

/**
 * This {@link Formatter} looks up an interface name in the input text.
 * When running into a hit, it returns a single formatting boundary that
 * points to the end of the input text. The {@code formatText()} method
 * will then place a link to the matched interface, which will thus appear
 * at the end of the input text.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class InterfaceReferToEndOfLineFormatter extends Formatter {

    private final DataModelChapterFormatterContext context;

    protected InterfaceReferToEndOfLineFormatter(DataModelChapterFormatterContext context) {
        super();
        this.context = context;
    }

    @Override
    public List<FormattingBoundary> getTextFormattingBoundaries(String sourceText) {
        final String sourceTextRedacted = redact(sourceText);

        final Set<String> matchedClassNames = context.getKeywords()
                                                     .parallelStream()
                                                     .filter(sourceTextRedacted::contains)
                                                     .collect(Collectors.toSet());

        return matchedClassNames.stream()
                                .map(className -> Pattern.compile("\\b" + className + "\\b")
                                                         .matcher(sourceTextRedacted))
                                .flatMap(Matcher::results)
                                .map(MatchResult::group)
                                .filter(group -> context.getUof(group).uofMemberIsInterface(group))
                                .findFirst()
                                .map(group -> new FormattingBoundaryInstance(sourceText.length(),
                                                                             sourceText.length(),
                                                                             group, this))
                                .map(FormattingBoundary.class::cast)
                                .map(List::of)
                                .orElseGet(List::of);
    }

    @Override
    public void formatText(Element element,
                           String sourceText) {
        final UofForDataModule relatingUof = context.getUof(sourceText);

        final String interfaceReference = relatingUof.getInterfaceParagraphIdByInterfaceName(sourceText);

        element.addText(" (Refer to ");

        if (relatingUof.equals(context.getUof(context.getCurrentClass()))) {
            element.addChild(DataModules.createInternalRef(interfaceReference));
        } else {
            element.addChild(DataModules.createDmRef(interfaceReference,
                                                     relatingUof));
        }
        element.addText(")");

    }

    @Override
    protected FormatterContext getContext() {
        return context;
    }
}