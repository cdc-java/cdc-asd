package cdc.asd.specgen.formatter;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cdc.asd.specgen.datamodules.UmlObjectDefinition;
import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.io.data.Element;

/**
 * A keyword-based formatter for the {@link GlossaryFormatterContext} and
 * {@link DataDictionaryFormatterContext}.
 * The context provides a terms map, which links relevant keywords
 * to format. The exact formatting depends on the term, whose nature
 * is provided in the terms map.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class KeywordGlossaryFormatter extends Formatter {

    private final UmlDefinitionFormatterContext context;

    public KeywordGlossaryFormatter(DataDictionaryFormatterContext context) {
        super();
        this.context = context;
    }

    public KeywordGlossaryFormatter(GlossaryFormatterContext context) {
        super();
        this.context = context;
    }

    @Override
    protected UmlDefinitionFormatterContext getContext() {
        return context;
    }

    @Override
    public List<FormattingBoundary> getTextFormattingBoundaries(String sourceText) {
        final String sourceTextRedacted = redact(sourceText);

        final Set<String> matchedKeywords = context.getTermsMap()
                                                   .entrySet()
                                                   .parallelStream()
                                                   .map(Map.Entry::getValue)
                                                   .filter(t -> t.isClass() ||
                                                           t.isInterface() ||
                                                           t.getName().equals(context.getCurrentTerm()))
                                                   .map(UmlObjectDefinition::getName)
                                                   .filter(sourceTextRedacted::contains)
                                                   .collect(Collectors.toSet());

        return matchedKeywords.stream()
                              .map(className -> Pattern.compile("\\b" + className + "\\b")
                                                       .matcher(sourceTextRedacted))
                              .flatMap(Matcher::results)
                              .map(matchResult -> new FormattingBoundaryInstance(matchResult.start(),
                                                                                 matchResult.end(),
                                                                                 matchResult.group(), this))
                              .map(FormattingBoundary.class::cast)
                              .toList();
    }

    @Override
    public void formatText(Element element,
                           String sourceText) {
        final UmlObjectDefinition objectDefinition =
                context.getTermsMap()
                       .get(sourceText);

        if (objectDefinition.isClass() || objectDefinition.isInterface()
                || objectDefinition.isAttribute()) {

            final Element verbatimElement =
                    element.addElement(S1000DNode.VERBATIMTEXT);

            if (objectDefinition.isAttribute()) {
                verbatimElement.addAttribute(S1000DNode.VERBATIMSTYLE,
                                             S1000DNode.VERBATIMSTYLE_ATTRIBUTENAME);
            } else {
                verbatimElement.addAttribute(S1000DNode.VERBATIMSTYLE,
                                             S1000DNode.VERBATIMSTYLE_CLASSNAME);
            }

            verbatimElement.addText(sourceText.trim());
        }
    }
}
