package cdc.asd.specgen.formatter;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.asd.specgen.datamodules.DataDictionaryEntry;
import cdc.asd.specgen.datamodules.UmlObjectDefinition;
import cdc.asd.specgen.s1000d5.S1000DNode;

/**
 * A {@link FormatterContext} for the core definitions export.
 * 
 * @implNote As the export is a simple table, there is no need for context specific
 *           to a given node. As a result, {@link #currentNode(S1000DNode)} returns this same
 *           object.
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class DataDictionaryFormatterContext extends UmlDefinitionFormatterContext {

    private final Map<String, UmlObjectDefinition> termsMap;

    public DataDictionaryFormatterContext(Map<String, DataDictionaryEntry> termsMap,
                                           Collection<String> redactedExpressions) {
        super();
        this.termsMap = Map.copyOf(termsMap);

        this.redactedExpressions =
                redactedExpressions.stream()
                                   .collect(Collectors.toUnmodifiableSet());
    }

    private DataDictionaryFormatterContext(String currentTerm,
                                            DataDictionaryFormatterContext original) {
        super(currentTerm, original);
        this.termsMap = original.termsMap;
    }

    public DataDictionaryFormatterContext currentTerm(String currentTerm) {
        return new DataDictionaryFormatterContext(currentTerm, this);
    }

    @Override
    public DataDictionaryFormatterContext currentNode(S1000DNode currentNode) {
        return this;
    }

    @Override
    public Set<String> getKeywords() {
        return termsMap.keySet();
    }

    @Override
    public Map<String, UmlObjectDefinition> getTermsMap() {
        return termsMap;
    }
}
