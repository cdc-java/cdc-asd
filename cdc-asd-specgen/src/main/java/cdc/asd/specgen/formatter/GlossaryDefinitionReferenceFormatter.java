package cdc.asd.specgen.formatter;

import java.util.List;

import cdc.asd.specgen.datamodules.DataModules;
import cdc.asd.specgen.datamodules.GlossaryDataModule;
import cdc.asd.specgen.datamodules.GlossaryMember;
import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.io.data.Element;

/**
 * The reference formatter for the glossary is normally used in the
 * "References" field of the glossary. For every term, the following logic is
 * applied: if the term is an attribute or a class, wrap it in a vertimText element,
 * otherwise leave it in its original form.
 * Then put a link to the glossary member corresponding to that term.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class GlossaryDefinitionReferenceFormatter extends Formatter {

    private final GlossaryFormatterContext context;

    public GlossaryDefinitionReferenceFormatter(GlossaryFormatterContext context) {
        super();
        this.context = context;
    }

    @Override
    protected FormatterContext getContext() {
        return context;
    }

    @Override
    public List<FormattingBoundary> getTextFormattingBoundaries(String sourceText) {
        return List.of(new FormattingBoundaryInstance(0, sourceText.length(),
                                                      sourceText, this));
    }

    @Override
    public void formatText(Element element,
                           String sourceText) {
        final GlossaryMember member = (GlossaryMember) context.getTermsMap().get(sourceText);

        if (member == null) {
            // term is not found in map
            // in other words there is a reference
            // to a term that is not defined

            // TODO log error that a referenced term
            // cannot be found and skip it
            element.addText(sourceText);
            return;
        }

        final GlossaryDataModule dataModule = member.getDataModule();

        if (dataModule == null) {
            // no formatting if term is nowhere to be found
            element.addText(sourceText);
            return;
        }

        final GlossaryMember glossaryMember =
                dataModule.getMember(sourceText);

        final GlossaryDataModule currentTermDataModule =
                ((GlossaryMember) (context.getTermsMap().get(context.getCurrentTerm()))).getDataModule();

        if (glossaryMember.isClass() || glossaryMember.isAttribute()
                || glossaryMember.isInterface()) {
            final Element verbatimElement =
                    element.addElement(S1000DNode.VERBATIMTEXT);

            verbatimElement.addAttribute(S1000DNode.VERBATIMSTYLE,
                                         glossaryMember.isAttribute() ? S1000DNode.VERBATIMSTYLE_ATTRIBUTENAME
                                                 : S1000DNode.VERBATIMSTYLE_CLASSNAME);

            verbatimElement.addText(sourceText.trim());
        }

        if (currentTermDataModule != null) {
            element.addText(", refer to ");

            if (dataModule.equals(currentTermDataModule)) {
                element.addChild(DataModules.createInternalRef(GlossaryMember.getId(glossaryMember)));

            } else {
                element.addChild(DataModules.createDmRef(GlossaryMember.getId(glossaryMember),
                                                         dataModule));
            }
            element.addText(".");
        }
    }
}