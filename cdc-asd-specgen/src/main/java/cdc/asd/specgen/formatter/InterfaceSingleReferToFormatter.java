package cdc.asd.specgen.formatter;

import java.util.List;

import cdc.asd.specgen.datamodules.DataModules;
import cdc.asd.specgen.datamodules.UofForDataModule;
import cdc.io.data.Element;

/**
 * Put a "Refer to..." link for an interface.
 * For best performance, this formatter only works on a text node
 * that only contains an interface name.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class InterfaceSingleReferToFormatter extends Formatter {

    private final DataModelChapterFormatterContext context;

    protected InterfaceSingleReferToFormatter(DataModelChapterFormatterContext context) {
        super();
        this.context = context;
    }

    @Override
    public List<FormattingBoundary> getTextFormattingBoundaries(String sourceText) {
        return List.of(new FormattingBoundaryInstance(sourceText.length(),
                                                      sourceText.length(),
                                                      sourceText, this));
    }

    @Override
    public void formatText(Element element,
                           String sourceText) {
        final UofForDataModule relatingUof = context.getUof(sourceText);
        
        if (relatingUof != null) {
            final String interfaceReference =
                    relatingUof.getInterfaceParagraphIdByInterfaceName(sourceText);
                    
            element.addText(" (Refer to ");

            if (relatingUof.equals(context.getUof(context.getCurrentClass()))) {
                element.addChild(DataModules.createInternalRef(interfaceReference));
            } else {
                element.addChild(DataModules.createDmRef(interfaceReference, relatingUof));
            }
            element.addText(")");
        }
    }

    @Override
    protected FormatterContext getContext() {
        return context;
    }

    @Override
    protected int getCompareKey() {
        return super.getCompareKey() + 1;
    }
}