package cdc.asd.specgen.formatter;

import java.util.List;

import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;
import cdc.io.data.Element;

/**
 * Generic class for formatters for S1000D text nodes.
 *
 * A formatter class analyzes a text node and identifies the positions
 * in the text where inline formatting elements are to be inserted.
 *
 * A formatting element can be a tag, in such case the opening and closing
 * tag positions must be defined; alternatively it can be a chunk of text
 *
 * The analysis can be based on pseudo tags left in the source data model
 * tagged values, or can "guess" what words must be put in specific
 * formatting tags, in such case a {@link FormatterContext} context object
 * can be provided.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public abstract class Formatter {
    /**
     * This function must return the {@code FormatterContext} of this instance.
     * Each subclass should declare such an instance.
     * 
     * @return The formatter context.
     */
    protected abstract FormatterContext getContext();

    /**
     * Private implementation of {@link FormattingBoundary}.
     * Only subclasses of {@link Formatter} can instantiate it.
     *
     * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
     */
    protected static final class FormattingBoundaryInstance implements FormattingBoundary {

        private final int beginning;
        private final int end;
        private final String matchedGroup;
        private final Formatter formatter;

        public FormattingBoundaryInstance(int beginning,
                                          int end,
                                          String matchedGroup,
                                          Formatter formatter) {
            this.beginning = beginning;
            this.end = end;
            this.matchedGroup = matchedGroup;
            this.formatter = formatter;
        }

        @Override
        public int getBeginning() {
            return beginning;
        }

        @Override
        public int getEnd() {
            return end;
        }

        @Override
        public String getMatchedGroup() {
            return matchedGroup;
        }

        @Override
        public Formatter getFormatter() {
            return formatter;
        }
    }

    public static final Formatter createFormatter(FormattingPolicy policy,
                                                  DataDictionaryFormatterContext context) {
        switch (policy) {
        case CLASS_VERBATIM:
            return new VerbatimClassFormatter(context);
        case GLOSSARY_KEYWORD:
            return new KeywordGlossaryFormatter(context);
        default:
            throw new IllegalArgumentException("Unknown formatter " + policy.getClass());
        }
    }

    /**
     * Create a formatter based on the supplied policy.
     *
     * @param policy The formatting policy.
     * @param context The formatter context.
     * @return A new Formatter object.
     * @throws IllegalArgumentException in case the policy is not documented.
     */
    public static final Formatter createFormatter(FormattingPolicy policy,
                                                  DataModelChapterFormatterContext context) {
        switch (policy) {
        case CLASS_VERBATIM:
            return new VerbatimClassFormatter(context);
        case CLASS_REFERTO:
            return new ClassReferToFormatter(context);
        case INTERFACE_SINGLE_REFERTO:
            return new InterfaceSingleReferToFormatter(context);
        case INTERFACE_REFERTO_ENDOFLINE:
            return new InterfaceReferToEndOfLineFormatter(context);
        default:
            throw new IllegalArgumentException("Unknown formatter " + policy.getClass());

        }
    }

    /**
     * Create a formatter based on the supplied policy.
     *
     * @param policy The formatting policy.
     * @param context The formatter context.
     * @return A new Formatter object.
     * @throws IllegalArgumentException in case the policy is not documented.
     */
    public static final Formatter createFormatter(FormattingPolicy policy,
                                                  GlossaryFormatterContext context) {
        switch (policy) {
        case CLASS_VERBATIM:
            return new VerbatimClassFormatter(context);
        case GLOSSARY_TERM_REFERENCE:
            return new GlossaryDefinitionReferenceFormatter(context);
        case GLOSSARY_KEYWORD:
            return new KeywordGlossaryFormatter(context);
        default:
            throw new IllegalArgumentException("Unknown formatter " + policy.getClass());

        }
    }

    /**
     * This method must return the boundaries of the locations where
     * where there will be formatting.
     *
     * @param sourceText The text to style.
     * @return The formatting boundaries.
     */
    public abstract List<FormattingBoundary> getTextFormattingBoundaries(String sourceText);

    /**
     * This method must consume and modify the supplied {@link Element}
     * to introduce the new, styled, text.
     *
     * @param element The input element to modify with the styled text.
     * @param sourceText The text to style.
     */
    public abstract void formatText(Element element,
                                    String sourceText);

    /**
     * This function executes the formatters onto the source text
     * and places the result into the supplied element.
     *
     * @param element The supplied element which will be modified to
     *            include the new format.
     * @param sourceText The source, unformatted text.
     * @param boundariesList The list of boundaries with their relating
     *            formatter.
     */
    public static final void formatElement(Element element,
                                           String sourceText,
                                           List<FormattingBoundary> boundariesList) {
        int previousPosition = 0;

        for (final FormattingBoundary boundaries : boundariesList.stream().sorted().toList()) {
            if (previousPosition <= boundaries.getBeginning()) {
                element.addText(sourceText.substring(previousPosition, boundaries.getBeginning()));
            }

            boundaries.getFormatter().formatText(element, boundaries.getMatchedGroup());

            previousPosition = boundaries.getEnd();
        }

        final String remainder = sourceText.substring(previousPosition);

        // Normalize except if supplied text node
        // is exactly one whitespace. In such case
        // chances are it was added on purpose.
        if (remainder.equals(" ") || !remainder.strip().isBlank()) {
            element.addText(remainder);
        }
    }

    /**
     * This method must return the expressions to redact. A redacted
     * expression is removed from the {@code sourceText} and replaced with
     * a blank expression. The consequence is that the expressions will not
     * be formatted.
     *
     * @param sourceText The text to style.
     * @return The strings to redact.
     */
    protected String redact(String sourceText) {
        for (final CharSequence redactedWord : getContext().redactedExpressions) {
            sourceText = sourceText.replace(redactedWord, " ".repeat(redactedWord.length()));
        }

        return sourceText;
    }

    /**
     * A current limitation (to overcome in the future) with formatters is that they cannot
     * execute inside one another. Pending this evolution, we need to specify the order according
     * to which they are to be executed. If the wrong order is used, some artefacts may be visible
     * in the output such as misplaced verbatim text elements.
     * This method should be overridden whenever a formatter is likely to interfere with another one.
     * 
     * @return The compare key.
     */
    protected int getCompareKey() {
        return 0;
    }
}
