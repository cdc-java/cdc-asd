package cdc.asd.specgen.formatter;

import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.io.data.Element;

/**
 * This formatter class places words that match keyword names (which must
 * be provided in the {@link FormatterContext} object), into verbatimText
 * XML elements for styling.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class VerbatimClassFormatter extends Formatter {

    private final FormatterContext context;

    protected VerbatimClassFormatter(FormatterContext context) {
        super();
        this.context = context;
    }

    @Override
    public List<FormattingBoundary> getTextFormattingBoundaries(String sourceText) {
        final String sourceTextRedacted = redact(sourceText);

        final Set<String> matchedClassNames = context.getKeywords()
                                                     .parallelStream()
                                                     .filter(sourceTextRedacted::contains)
                                                     .collect(Collectors.toSet());

        return matchedClassNames.stream()
                                .map(className -> Pattern.compile("\\b" + className + "\\b")
                                                         .matcher(sourceTextRedacted))
                                .flatMap(Matcher::results)
                                .map(matchResult -> new FormattingBoundaryInstance(matchResult.start(),
                                                                                   matchResult.end(),
                                                                                   matchResult.group(), this))
                                .map(FormattingBoundary.class::cast)
                                .toList();
    }

    @Override
    public void formatText(Element element,
                           String substring) {
        final Element verbatimElement = element.addElement(S1000DNode.VERBATIMTEXT);
        verbatimElement.addAttribute(S1000DNode.VERBATIMSTYLE, S1000DNode.VERBATIMSTYLE_CLASSNAME);
        verbatimElement.addText(substring);
    }

    @Override
    protected FormatterContext getContext() {
        return context;
    }

}
