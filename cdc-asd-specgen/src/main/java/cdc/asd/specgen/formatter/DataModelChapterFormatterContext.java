package cdc.asd.specgen.formatter;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.specgen.datamodules.UofForDataModule;
import cdc.asd.specgen.s1000d5.S1000DNode;

/**
 * A formatter context, only suitable for the data model chapter generation.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class DataModelChapterFormatterContext extends FormatterContext {

    private final Map<String, UofForDataModule> uofs;
    private final String currentClass;
    private final String currentUof;

    public DataModelChapterFormatterContext(String currentClass,
                                            String currentUof,
                                            DataModelChapterFormatterContext original) {
        super(original);
        this.uofs = Map.copyOf(original.uofs);
        this.currentClass = currentClass;
        this.currentUof = currentUof;
    }

    /**
     * Set the current class. This allows the context to know which class is being dealt with,
     * so individual formatters can adapt their behavior depending on what terms are being
     * formatted.
     *
     * @param currentClass The current class.
     * @return A new context.
     */
    public DataModelChapterFormatterContext currentClass(String currentClass) {
        return new DataModelChapterFormatterContext(currentClass, this.currentUof, this);
    }

    /**
     * Set the current UoF. This allows the context to know which UoF is being dealt with,
     * so individual formatters can adapt their behavior depending on what terms are being
     * formatted.
     *
     * @param currentUof The current UoF.
     * @return A new context.
     */
    public DataModelChapterFormatterContext currentUof(String currentUof) {
        return new DataModelChapterFormatterContext(this.currentClass, currentUof, this);
    }

    /**
     * Create a {@code FormatterContext} with only the class names.
     *
     * @param uofMap a Map linking class names with their relating
     *            UoF.
     * @param redactedExpressions The list of expressions <i>not</i> to format when
     *            encountered.
     */
    public DataModelChapterFormatterContext(Map<String, UofForDataModule> uofMap,
                                            Collection<String> redactedExpressions) {
        super();
        this.redactedExpressions =
                Stream.concat(this.redactedExpressions.stream(),
                              redactedExpressions.stream())
                      .collect(Collectors.toUnmodifiableSet());
        this.uofs = Map.copyOf(uofMap);
        currentClass = null;
        currentUof = null;
    }

    private DataModelChapterFormatterContext(S1000DNode currentNode,
                                             DataModelChapterFormatterContext original) {
        this(original.currentClass, original.currentUof, original);
        this.currentNode = currentNode;
    }

    /**
     * Get the current class name that is being processed.
     *
     * @return The class name or {@code null}.
     */
    public final String getCurrentClass() {
        return currentClass;
    }

    /**
     * Get the current UoF that is being processed.
     *
     * @return The UoF name or <code>null</code>.
     */
    public final String getCurrentUof() {
        return currentUof;
    }

    @Override
    public final Set<String> getKeywords() {
        return uofs.keySet();
    }

    /**
     * Get the UoF object for the input class.
     *
     * @param className The input class.
     * @return The UoF object or {@code null}.
     */
    public final UofForDataModule getUof(String className) {
        return uofs.get(className);
    }

    @Override
    public final DataModelChapterFormatterContext currentNode(S1000DNode currentNode) {
        return new DataModelChapterFormatterContext(currentNode, this);
    }

}
