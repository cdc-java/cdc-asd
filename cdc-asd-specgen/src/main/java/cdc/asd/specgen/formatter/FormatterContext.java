package cdc.asd.specgen.formatter;

import java.util.Set;

import cdc.asd.specgen.s1000d5.S1000DNode;

/**
 * This class holds context data for the formatters.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public abstract class FormatterContext {

    protected S1000DNode currentNode;

    protected FormatterContext() {
        this.currentNode = null;
    }

    protected FormatterContext(FormatterContext original) {
        this.currentNode = original.currentNode;
        this.redactedExpressions = original.redactedExpressions;
    }

    /**
     * Redacted expressions are temporarily replaced with blank
     * expressions in the source text. This prevents the formatter
     * from matching a sequence to format. This unmodifiable set should be
     * redefined by subclasses if some expressions are to be redacted
     * in particular formatters.
     */
    protected Set<String> redactedExpressions = Set.of("Integrated Product Support");

    public abstract FormatterContext currentNode(S1000DNode currentNode);

    /**
     * Get the list of classes
     *
     * @return List of class names.
     */
    public abstract Set<String> getKeywords();

    /**
     * Get the node currently pointed to by the context
     *
     * @return The node or {@code null}.
     */
    public final S1000DNode getCurrentNode() {
        return currentNode;
    }
}
