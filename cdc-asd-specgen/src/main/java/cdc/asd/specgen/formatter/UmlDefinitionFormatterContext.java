package cdc.asd.specgen.formatter;

import java.util.Map;

import cdc.asd.specgen.datamodules.UmlObjectDefinition;

/**
 * A formatter context to control the formatting of things that represent
 * UML objects
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public abstract class UmlDefinitionFormatterContext extends FormatterContext {

    /**
     * The current term that is being printed. May be <code>null</code>.
     */
    private final String currentTerm;

    /**
     * Constructor
     */
    public UmlDefinitionFormatterContext() {
        super();
        this.currentTerm = null;
    }

    /**
     * Constructor with a current term on the basis of another
     * {@link UmlDefinitionFormatterContext} object.
     * 
     * @param currentTerm The term that is being printed. May be <code>null</code>
     * @param original The original context
     */
    public UmlDefinitionFormatterContext(String currentTerm,
                                         UmlDefinitionFormatterContext original) {
        super(original);
        this.currentTerm = currentTerm;
    }

    /**
     * Get the terms map
     * 
     * @return A {@link Map} relating the term as a String to its
     *         instantiation as a {@link UmlObjectDefinition}
     */
    abstract public Map<String, UmlObjectDefinition> getTermsMap();

    /**
     * Get the current term, that is being printed
     * 
     * @return The current term or <code>null</code>
     */
    public final String getCurrentTerm() {
        return currentTerm;
    }
}
