package cdc.asd.specgen;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Strictness;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.asd.specgen.datamodules.DataModules;
import cdc.asd.specgen.datamodules.PublicationModuleEntryForPublicationModule;
import cdc.asd.specgen.s1000d5.Figure;
import cdc.asd.specgen.s1000d5.Para;
import cdc.asd.specgen.s1000d5.S1000DGenericElementNode;
import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode;
import cdc.io.data.Attribute;
import cdc.io.data.Document;
import cdc.io.data.Element;
import cdc.io.data.xml.XmlDataReader;
import cdc.io.data.xml.XmlDataReader.Feature;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfConstraint;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfType;
import cdc.util.files.Resources;

/**
 * Various utilities for getting data from an EA model.
 */
public final class EaModelIoUtils {
    private static final Logger LOGGER = LogManager.getLogger(EaModelIoUtils.class);
    public static final String RESOURCES_PATH = "cdc/asd/specgen/";
    public static final String BRDOC_STATIC_XML = "brdoc_static.xml";
    public static final String PM_GLOSSARY_STATIC_XML = "pm_glossary_static.xml";
    public static final String PM_STATIC_XML = "pm_static.xml";

    /*
     * DTD artifacts
     */
    private static final String DTD_OPEN = "<!DOCTYPE %s [";
    private static final String DTD_PNG = "<!NOTATION png PUBLIC \"-//W3C//NOTATION Portable Network Graphics//EN\">";
    private static final String DTD_GIF =
            "<!NOTATION GIF89a PUBLIC \"-//CompuServe//NOTATION Graphics Interchange Format 89a//EN\">";
    private static final String DTD_ENTITY_TEMPLATE =
            "<!ENTITY %1$s SYSTEM \"%1$s.PNG\" NDATA png>";
    private static final String DTD_CLOSE = "]>";
    private static final int TYPICAL_ICN_PER_UOF = 10;
    private static final int TYPICAL_DTD_SIZE = 4096;

    /**
     * The pattern for finding, among the root packages, the one with the data model package.
     */
    public static final String ROOT_DATA_MODEL_PACKAGE_PATTERN = ".*Data_model.*";

    /**
     * The pattern for finding, from inside the right root package, the data model package.
     */
    public static final String DATA_MODEL_PACKAGE_PATTERN = ".*_Data_model_.*";

    /**
     * The pattern for finding, from inside the right root package, the primitives package.
     */
    public static final String PRIMITIVES_PACKAGE_PATTERN = "S[_-]Series_Primitives_.*";

    /**
     * The pattern for finding, from inside the right root package, the compound attributes package.
     */
    public static final String COMPOUND_ATTRIBUTES_PACKAGE_PATTERN = "S[_-]Series_Compound_Attributes_.*";

    /**
     * The stereotype for local connectors
     */
    private static final String CONNECTOR_LOCAL_CONSTRAINT_SPECIFICATION = "local";

    /**
     * The tagged value name for the illustrations of UoF.
     */
    private static final String ICN_TAGGED_VALUE_NAME = "ICN";

    /**
     * The prefix for UoF paragraph XML IDs.
     */
    private static final String UOF_PARA_PREFIX = "brlevelledpara-uof-";

    /**
     * The prefix for interface paragraph XML IDs.
     */
    private static final String INTERFACE_PARA_PREFIX = "brlevelledpara-interface-description-";

    /**
     * The prefix for class paragraph XML IDs.
     */
    private static final String CLASS_PARA_PREFIX = "brlevelledpara-class-description-";

    private EaModelIoUtils() {
    }

    private static InputStream getInputStream(String name) throws IOException {
        final URL url = Resources.getResource(name, true);
        if (url != null) {
            try {
                return url.openStream();
            } catch (final IOException e) {
                LOGGER.warn("getResourceAsStream({}) FAILED, {}", name, e.getMessage());
                throw new IOException("Failed to open stream  for " + url);
            }
        } else {
            throw new IOException("No resource named '" + name + "' found.");
        }
    }

    /**
     * Get the root data model package. The root data model package is the one
     * that has "Data_model" is its name (e.g. SX002D_2-1_Data_model_000-05).
     *
     * @param model The {@link MfPackage} which must contain a root data model package.
     *            May be <code>null</code>.
     * @return The root data model package or {@code null}.
     */
    public static MfPackage getRootDataModelPackage(MfModel model) {
        return Optional.ofNullable(model)
                       .stream()
                       .flatMap(m -> m.getChildren(MfPackage.class).stream())
                       .filter(hasName(ROOT_DATA_MODEL_PACKAGE_PATTERN))
                       .findFirst()
                       .orElse(null);
    }

    /**
     * Get the data model package. The data model package is located inside the root
     * data model package, and its name contains "_Data_model_".
     *
     * @param rootDataModelPackage The root data model package. May be <code>null</code>.
     * @return The data model package or {@code null}.
     */
    public static MfPackage getDataModelPackage(MfPackage rootDataModelPackage) {
        return Optional.ofNullable(rootDataModelPackage)
                       .map(MfPackage::getChildren)
                       .stream()
                       .flatMap(List::stream)
                       .filter(MfPackage.class::isInstance)
                       .map(MfPackage.class::cast)
                       .filter(hasName(DATA_MODEL_PACKAGE_PATTERN))
                       .findFirst()
                       .orElse(null);
    }

    /**
     * Get the primitives package. The primitives package is located inside the root
     * data model package.
     *
     * @param rootDataModelPackage The root data model package. May be <code>null</code>.
     * @return The primitives package or {@code null}.
     */
    public static MfPackage getPrimitivesPackage(MfPackage rootDataModelPackage) {
        return Optional.ofNullable(rootDataModelPackage)
                       .map(MfPackage::getChildren)
                       .stream()
                       .flatMap(List::stream)
                       .filter(MfPackage.class::isInstance)
                       .map(MfPackage.class::cast)
                       .filter(hasName(PRIMITIVES_PACKAGE_PATTERN))
                       .findFirst()
                       .orElse(null);
    }

    /**
     * Get the compound attributes package. The compound attributes package is located
     * inside the root data model package, and its name contains "_Data_model_".
     *
     * @param rootDataModelPackage The root data model package. May be <code>null</code>.
     * @return The compound attributes package or {@code null}.
     */
    public static MfPackage getCompoundAttributesPackage(MfPackage rootDataModelPackage) {
        return Optional.ofNullable(rootDataModelPackage)
                       .map(MfPackage::getChildren)
                       .stream()
                       .flatMap(List::stream)
                       .filter(MfPackage.class::isInstance)
                       .map(MfPackage.class::cast)
                       .filter(hasName(COMPOUND_ATTRIBUTES_PACKAGE_PATTERN))
                       .findFirst()
                       .orElse(null);
    }

    /**
     * Get the list of packages whose name matches {@code UOF_PATTERN}.
     *
     * @param pack The package from which to find UoF sub-packages.
     * @return The list.
     */
    public static List<MfPackage> getUofList(MfPackage pack) {
        return getUofList(pack, null, null);
    }

    /**
     * Get the list of packages which are UoF according to {@link AsdPackage#isUof()} and
     * append the list with the supplied primitives and compound attributes packages.
     *
     * @param pack The package from which to find UoF sub-packages.
     * @param primitives The primitives package. May be {@code null}.
     * @param compoundAttributes The compound attributes package. May be {@code null}.
     * @return The list of packages.
     */
    public static List<MfPackage> getUofList(MfPackage pack,
                                             MfPackage primitives,
                                             MfPackage compoundAttributes) {
        return Stream.concat(Stream.concat(Optional.ofNullable(pack)
                                                   .stream()
                                                   .map(MfPackage::getChildren)
                                                   .flatMap(List::parallelStream)
                                                   .filter(MfPackage.class::isInstance)
                                                   .map(MfPackage.class::cast)
                                                   .filter(p -> p.wrap(AsdPackage.class).isUof()),
                                           Optional.ofNullable(primitives)
                                                   .map(Stream::of)
                                                   .orElseGet(Stream::empty)),
                             Optional.ofNullable(compoundAttributes)
                                     .map(Stream::of)
                                     .orElseGet(Stream::empty))
                     .sorted(Comparator.comparing(MfPackage::getName))
                     .toList();
    }

    private static Predicate<MfPackage> hasName(String name) {
        return pack -> pack.getName() != null && pack.getName().matches(name);
    }

    /**
     * Shorthand for:
     * {@code new S1000DGenericElementNode(S1000D_LISTITEM).child(para)}
     *
     * @param para The "para" argument above.
     * @return A new {@link S1000DGenericElementNode}.
     */
    public static S1000DGenericElementNode listItem(Para para) {
        return new S1000DGenericElementNode(S1000DNode.LISTITEM).child(para);
    }

    /**
     * Shorthand for:
     * {@code new S1000DGenericElementNode(S1000DNode.S1000D_VERBATIMTEXT).child(new S1000DTextNode(content))}
     *
     * @param content The "content" argument above.
     * @return A new {@link S1000DGenericElementNode}.
     */
    public static S1000DGenericElementNode verbatimTextClass(String content) {
        return new S1000DGenericElementNode(S1000DNode.VERBATIMTEXT).attribute(S1000DNode.VERBATIMSTYLE,
                                                                               S1000DNode.VERBATIMSTYLE_CLASSNAME)
                                                                    .child(new S1000DTextNode(content));
    }

    /**
     * Shorthand for:
     * {@code new S1000DGenericElementNode(S1000DNode.S1000D_VERBATIMTEXT).child(new S1000DTextNode(content))}
     *
     * @param content The "content" argument above.
     * @return A new {@link S1000DGenericElementNode}.
     */
    public static S1000DGenericElementNode verbatimTextAttribute(String content) {
        return new S1000DGenericElementNode(S1000DNode.VERBATIMTEXT).attribute(S1000DNode.VERBATIMSTYLE,
                                                                               S1000DNode.VERBATIMSTYLE_ATTRIBUTENAME)
                                                                    .child(new S1000DTextNode(content));
    }

    /**
     * Wrap a valid-value into a paragraph suitable for rendering valid-values.
     *
     * @param code The XML code of the valid-value.
     * @param value The textual representation of the valid-value.
     * @return A new {@link Para}.
     */
    public static S1000DGenericElementNode formatValidValues(String code,
                                                             String value) {
        final S1000DNode listItemTerm =
                new S1000DGenericElementNode(S1000DNode.LISTITEMTERM).child(EaModelIoUtils.verbatimTextClass(code));
        final S1000DNode listItemDefinition =
                new S1000DGenericElementNode(S1000DNode.LISTITEMDEFINITION).child(new Para(EaModelIoUtils.verbatimTextAttribute(value)));

        return new S1000DGenericElementNode(S1000DNode.DEFINITIONLISTITEM).child(listItemTerm)
                                                                          .child(listItemDefinition);
    }

    /**
     * Create a DTD section for a Brdoc or PM {@link Document}.
     * This implementation browses each node, seeking any infoEntityIdent
     * attribute, and adds that infoEntityIdent to the DTD as an ENTITY.
     * It is assumed that all infoEntityIdent attributes refer to PNG images,
     * named exactly the same (plus the .PNG suffix).
     *
     * @param document The {@link Document} on which to add the DTD
     */
    public static void createDtd(Document document) {
        final Element root = document.getRootElement();
        final Set<String> infoEntityIdentReferences =
                getInfoEntityIdentReferencesRecursively(root,
                                                        new HashSet<>(TYPICAL_ICN_PER_UOF));

        final StringBuilder dtdString = new StringBuilder(TYPICAL_DTD_SIZE);

        dtdString.append(String.format(DTD_OPEN, root.getName()));
        dtdString.append(DTD_PNG);
        dtdString.append(DTD_GIF);
        // StringBuilder::append is rejected by OpenJDK compiler (ambiguity)
        dtdString.append(infoEntityIdentReferences.stream()
                                                  .map(icn -> String.format(DTD_ENTITY_TEMPLATE, icn))
                                                  .collect(StringBuilder::new,
                                                           StringBuilder::append,
                                                           (StringBuilder sb1,
                                                            StringBuilder sb2) -> sb1.append(sb2)));
        dtdString.append(DTD_CLOSE);

        document.setDTD(dtdString.toString());
    }

    /**
     * Recursively descend the supplied {@link Element} and look for infoEntityIdent
     * references. Return a set with all the encountered references.
     *
     * @param element The {@link Element} to search for that attribute, and then to descend.
     * @param infoEntityIdentReferences An existing set of infoEntityIdent references, it will
     *            be appended with new occurrences of infoEntityIdent
     * @return The same set as provided in icnReferences.
     */
    private static Set<String> getInfoEntityIdentReferencesRecursively(Element element,
                                                                       Set<String> infoEntityIdentReferences) {
        final Attribute infoEntityIdent = element.getAttribute(S1000DNode.INFOENTITYIDENT);

        if (infoEntityIdent != null) {
            infoEntityIdentReferences.add(infoEntityIdent.getValue());
        }

        for (final Element child : element.getChildren(Element.class)) {
            getInfoEntityIdentReferencesRecursively(child, infoEntityIdentReferences);
        }

        return infoEntityIdentReferences;
    }

    /**
     * Load a standard, empty Publication Module document.
     *
     * @return A document to populate.
     * @throws IOException Document cannot be loaded
     */
    public static Document loadPmFromStaticDocument() throws IOException {
        final InputStream is = getInputStream(RESOURCES_PATH + PM_STATIC_XML);
        return XmlDataReader.load(is);
    }

    /**
     * Load a Publication Module document, suitable for front matter and full ToC.
     *
     * @return A document to populate.
     * @throws IOException Document cannot be loaded
     */
    public static Document loadFmPmFromStaticDocument() throws IOException {
        final InputStream is = getInputStream(RESOURCES_PATH + PM_GLOSSARY_STATIC_XML);
        return XmlDataReader.load(is, Feature.ALLOW_MIXED_CONTENT);
    }

    /**
     * Load a standard, empty BrDdoc data module document.
     *
     * @return A document to populate.
     * @throws IOException Document cannot be loaded.
     */
    public static Document loadBrdocFromStaticDocument() throws IOException {
        return loadBrdocFromStaticDocument(BRDOC_STATIC_XML);
    }

    /**
     * Load a standard, empty BrDdoc data module document.
     *
     * @param resourceName The name of the BrDoc document to load.
     * @return A document to populate.
     * @throws IOException Document cannot be loaded.
     */
    public static Document loadBrdocFromStaticDocument(String resourceName) throws IOException {
        final InputStream is = getInputStream(RESOURCES_PATH + resourceName);
        return XmlDataReader.load(is);
    }

    /**
     * Populate a Publication Module template with "top-level" values.
     *
     * @param document The {@link Document} for the Publication Module. It will be
     *            modified to include the static/top-level values.
     * @param pm The Publication Module that contains the top-level values (e.g.
     *            Spec title, spec issue date, etc.)
     */
    public static void populateStaticPmDocumentAttributes(Document document,
                                                          PublicationModuleEntryForPublicationModule pm) {
        final Element pmAddress =
                document.getElementNamed(S1000DNode.PM)
                        .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                        .getElementNamed(S1000DNode.PMADDRESS);

        final Element pmCode =
                pmAddress.getElementNamed(S1000DNode.PMIDENT)
                         .getElementNamed(S1000DNode.PMCODE);

        final Element issueDate =
                pmAddress.getElementNamed(S1000DNode.PMADDRESSITEMS)
                         .getElementNamed(S1000DNode.ISSUEDATE);

        final Element pmTitle =
                pmAddress.getElementNamed(S1000DNode.PMADDRESSITEMS)
                         .getElementNamed(S1000DNode.PMTITLE);

        final Element pmEntry =
                document.getElementNamed(S1000DNode.PM)
                        .getElementNamed(S1000DNode.CONTENT)
                        .getElementNamed(S1000DNode.PMENTRY);

        final Element dmCode =
                pmEntry.getElementNamed(S1000DNode.DMREF)
                       .getElementNamed(S1000DNode.DMREFIDENT)
                       .getElementNamed(S1000DNode.DMCODE);

        issueDate.getAttribute(S1000DNode.ISSUEDATE_YEAR)
                 .setValue(pm.getIssueDate()
                             .getYear());
        issueDate.getAttribute(S1000DNode.ISSUEDATE_MONTH)
                 .setValue(String.format("%02d",
                                         pm.getIssueDate()
                                           .getMonthValue()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_DAY)
                 .setValue(String.format("%02d",
                                         pm.getIssueDate()
                                           .getDayOfMonth()));
        pmTitle.addText(pm.getPmTitle());

        document.getElementNamed(S1000DNode.PM)
                .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                .getElementNamed(S1000DNode.PMSTATUS)
                .getElementNamed(S1000DNode.LOGO)
                .getElementNamed(S1000DNode.SYMBOL)
                .getAttribute(S1000DNode.INFOENTITYIDENT)
                .setValue(pm.getLogoIcn());

        pmEntry.getElementNamed(S1000DNode.PMENTRYTITLE)
               .addText("Chapter " + pm.getChapterNumber());

        if (!pm.getPublicationModuleEntries().isEmpty()) {
            pmEntry.getElementNamed(S1000DNode.PMENTRY)
                   .getElementNamed(S1000DNode.PMENTRYTITLE)
                   .addText(pm.getPmTitle());
        } else {
            pmEntry.removeElementsNamed(S1000DNode.PMENTRY);
        }

        dmCode.getAttribute(S1000DNode.MODELIDENTCODE)
              .setValue(pm.getModelIdentCode());

        dmCode.getAttribute(S1000DNode.SYSTEMCODE)
              .setValue(pm.getSystemCode());

        pmCode.getAttribute(S1000DNode.MODELIDENTCODE)
              .setValue(pm.getModelIdentCode());
        pmCode.getAttribute(S1000DNode.PMNUMBER)
              .setValue(pm.getPmNumber());
        pmCode.getAttribute(S1000DNode.PMVOLUME)
              .setValue(pm.getPmVolume());
    }

    /**
     * Return the simple name of a UoF name.
     * For example, "CDM UoF Breakdown Structure" becomes
     * "Breakdown Structure".
     *
     * @param uofName The UoF name.
     * @return The simple UoF name or the same input if
     *         no change could be made
     */
    public static String getSimpleUofName(String uofName) {
        if (!uofName.contains(" UoF ")) {
            final Matcher matcher = DataModules.UOF_PATTERN.matcher(uofName);

            if (matcher.matches()) {
                return matcher.group(1);
            }
        } else {

            final Matcher legacyMatcher = DataModules.LEGACY_UOF_PATTERN.matcher(uofName);

            if (legacyMatcher.matches()) {
                return legacyMatcher.group(1);
            }
        }

        return uofName;
    }

    /**
     * Tells if a {@link MfTag} is that of an ICN.
     *
     * @param tag The {@link MfTag} to test.
     * @return <code>true</code> if the supplied {@link MfTag} is for an ICN.
     */
    public static boolean isIcnTag(MfTag tag) {
        return tag.getName().equals(ICN_TAGGED_VALUE_NAME);
    }

    public static Figure createFigureFromTag(MfTag icnTag) {
        return new Figure(icnTag.getValue(),
                          icnTag.wrap(AsdElement.class).getNotes());
    }

    /**
     * Return a {@link Predicate} that returns <code>true</code> if the
     * {@link Predicate}'s {@link MfConnector} has the supplied {@link MfType}
     * as its source. In the case of directed associations, the direction must
     * be forward (away from the source).
     *
     * @param owningType The {@link MfType} for which to test the ownership of the
     *            {@link Predicate}'s {@link MfConnector}.
     * @return A {@link Predicate} that returns <code>true</code> if the supplied {@link MfType}
     *         is the source of the {@link Predicate}'s {@link MfConnector}, <code>false</code> otherwise.
     */
    public static Predicate<MfConnector> belongsTo(MfType owningType) {
        return connector -> {
            if (connector.getConstraints()
                         .stream()
                         .map(MfConstraint::getSpecification)
                         .anyMatch(CONNECTOR_LOCAL_CONSTRAINT_SPECIFICATION::equals)) {
                if (!connector.getSourceTip().getType().equals(owningType)) {
                    return false;
                }
            }

            if (!Optional.ofNullable(owningType)
                         .map(type -> type.isDescendantOf(connector.getSourceTip().getType(), Strictness.LOOSE))
                         .orElse(false)) {
                return false;
            }

            return true;
        };
    }

    /**
     * Get the identifier of top paragraph (brlevelledpara) on the supplied {@link MfPackage} UoF.
     *
     * @param uofPackage The {@link MfPackage} UoF to identify or refer to.
     * @return A string that provides a unique identifier to identify or refer to the supplied
     *         {@link MfPackage} paragraph in the output XML documents.
     */
    public static String getXmlParaIdFromUofPackage(MfPackage uofPackage) {
        return UOF_PARA_PREFIX
                + URLEncoder.encode(uofPackage.getId()
                                              .replace('@', 'P'),
                                    StandardCharsets.UTF_8);
    }

    /**
     * Get the identifier of paragraph (brlevelledpara) on the supplied {@link MfClass}.
     *
     * @param someClass The {@link MfClass} to identify or refer to.
     * @return A string that provides a unique identifier to identify or refer to the supplied
     *         {@link MfClass} paragraph in the output XML documents.
     */
    public static String getXmlParaIdFromClass(MfClass someClass) {
        return CLASS_PARA_PREFIX
                + URLEncoder.encode(someClass.getId()
                                             .replace('@', 'P'),
                                    StandardCharsets.UTF_8);
    }

    /**
     * Get the identifier of paragraph (brlevelledpara) on the supplied {@link MfInterface}.
     *
     * @param someInterface The {@link MfInterface} to identify or refer to.
     * @return A string that provides a unique identifier to identify or refer to the supplied
     *         {@link MfInterface} paragraph in the output XML documents.
     */
    public static String getXmlParaIdFromInterface(MfInterface someInterface) {
        return INTERFACE_PARA_PREFIX
                + URLEncoder.encode(someInterface.getId()
                                                 .replace('@', 'P'),
                                    StandardCharsets.UTF_8);
    }
}