package cdc.asd.specgen.datamodules;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import cdc.asd.specgen.s1000d5.ChangeMark;
import cdc.asd.specgen.s1000d5.ChangeMark.ChangeType;

/**
 * A glossary entry (or member), specifically one that discusses an
 * attribute in the data model.
 * More specifically, this type of entry is one that has a set of valid-values
 * as well as a parent class.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class AttributeGlossaryMember extends GlossaryMember {

    /**
     * A simple structure for defining valid values (XML code and textual value)
     */
    public static final class ValidValue {
        public final String code;
        public final String value;

        public ValidValue(String code,
                          String value) {
            Objects.requireNonNull(code);
            Objects.requireNonNull(value);
            this.code = code;
            this.value = value;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof final ValidValue v) {
                return super.equals(obj) ||
                        code.equals(v.code) && value.equals(v.value);

            }

            return false;
        }

        @Override
        public int hashCode() {
            return Objects.hash(code, value);
        }
    }

    /**
     * This attribute's parent class name.
     */
    public final String parentClassName;

    /**
     * This attribute's type name
     */
    public final String typeName;

    /**
     * This attribute's current valid-values.
     */
    private List<ValidValue> validValuesField = List.of();

    /**
     * Copy constructor.
     *
     * @param name Name of this {@link AttributeGlossaryMember}
     * @param definition Definition of this {@link AttributeGlossaryMember}
     * @param parentClassName Name of parent class
     * @param typeName The type name (PropertyType, ClassificationType, etc.)
     * @param previousIssue The previous issue of this
     *            {@link AttributeGlossaryMember}. May be <code>null</code>.
     */
    public AttributeGlossaryMember(String name,
                                   String definition,
                                   String parentClassName,
                                   String typeName,
                                   AttributeGlossaryMember previousIssue) {
        super(name, definition, ObjectType.ATTRIBUTE, previousIssue);
        this.parentClassName = parentClassName;
        this.typeName = typeName;
    }

    /**
     * Copy constructor.
     *
     * @param original Original object.
     */
    public AttributeGlossaryMember(AttributeGlossaryMember original) {
        this(List.of(), Set.of(), List.of(), original.dataModule,
             original.changeMark, original.previousIssue, original);
    }

    /**
     * Copy constructor with notes, references, examples.
     *
     * @param notes The notes to add. Can be an empty list if nothing to add.
     * @param references The references to add. Can be an empty list if nothing to add.
     * @param examples The examples to add. Can be an empty list if nothing to add.
     * @param dataModule The data module to set for this new object.
     * @param changeMark The {@link ChangeMark} associated with this object.
     * @param previousIssue The previous issue of the same {@link AttributeGlossaryMember}.
     * @param original The original object. Each attribute will be copied over.
     */
    private AttributeGlossaryMember(List<String> notes,
                                    Set<String> references,
                                    List<String> examples,
                                    GlossaryDataModule dataModule,
                                    ChangeMark changeMark,
                                    GlossaryMember previousIssue,
                                    AttributeGlossaryMember original) {
        super(notes, references, examples, dataModule, changeMark, previousIssue, original);
        this.parentClassName = original.parentClassName;
        this.validValuesField = original.validValuesField;
        this.typeName = original.typeName;
    }

    /**
     * Copy constructor.
     *
     * @param validValues List of valid-values to add
     * @param original Original object
     */
    private AttributeGlossaryMember(List<ValidValue> validValues,
                                    AttributeGlossaryMember original) {
        this(original);

        this.validValuesField = Stream.concat(this.validValuesField.stream(),
                                              validValues.stream())
                                      .toList();
    }

    @Override
    public AttributeGlossaryMember note(String note) {
        return new AttributeGlossaryMember(List.of(note), Set.of(),
                                           List.of(), this.dataModule,
                                           this.changeMark, this.previousIssue,
                                           this);
    }

    @Override
    public AttributeGlossaryMember notes(List<String> notes) {
        if (notes.isEmpty()) {
            return this;
        }

        return new AttributeGlossaryMember(notes, Set.of(),
                                           List.of(), this.dataModule,
                                           this.changeMark, this.previousIssue,
                                           this);
    }

    @Override
    public AttributeGlossaryMember references(Set<String> references) {
        if (references.isEmpty()) {
            return this;
        }

        return new AttributeGlossaryMember(List.of(), references,
                                           List.of(), this.dataModule,
                                           this.changeMark, this.previousIssue,
                                           this);
    }

    @Override
    public AttributeGlossaryMember reference(String reference) {
        return new AttributeGlossaryMember(List.of(), Set.of(reference),
                                           List.of(), this.dataModule,
                                           this.changeMark, this.previousIssue,
                                           this);

    }

    /**
     * Add valid-values.
     *
     * @param validValues The valid-values to add. May be empty.
     * @return A new {@code GlossaryMember} or this object
     *         if supplied input is empty.
     */
    public AttributeGlossaryMember validValues(List<ValidValue> validValues) {
        if (validValues.isEmpty()) {
            return this;
        }

        return new AttributeGlossaryMember(validValues, this);
    }

    /**
     * Get the valid-values of this glossary member.
     *
     * @return A list of valid-values
     */
    public List<ValidValue> getValidValues() {
        return validValuesField;
    }

    /**
     * Get the {@link ChangeMark} of the valid-values section of this object.
     *
     * @return The {@link ChangeMark}. If no previous issue is provided,
     *         then an empty {@link ChangeMark} is returned.
     */
    public ChangeMark getValidValuesChangeMark() {
        if (notAddedNorDeleted()) {
            return getChanges(validValuesField, ((AttributeGlossaryMember) previousIssue).validValuesField);
        }

        return new ChangeMark();
    }

    @Override
    public AttributeGlossaryMember examples(List<String> examples) {
        if (examples.isEmpty()) {
            return this;
        }

        return new AttributeGlossaryMember(List.of(), Set.of(),
                                           examples, this.dataModule,
                                           this.changeMark, this.previousIssue,
                                           this);
    }

    /**
     * Get this attribute's parent class name.
     *
     * @return The parent class name
     */
    public String getParentClassName() {
        return parentClassName;
    }

    @Override
    public AttributeGlossaryMember dataModule(GlossaryDataModule dataModule) {
        return new AttributeGlossaryMember(List.of(), Set.of(),
                                           List.of(), dataModule,
                                           this.changeMark, this.previousIssue,
                                           this);
    }

    /**
     * Get this attribute's type name
     *
     * @return The type name
     */
    public String getTypeName() {
        return typeName;
    }

    @Override
    public AttributeGlossaryMember changeMark(ChangeMark changeMark) {
        return new AttributeGlossaryMember(List.of(), Set.of(), List.of(),
                                           dataModule, changeMark, this.previousIssue,
                                           this);
    }

    @Override
    public AttributeGlossaryMember getPreviousIssue() {
        return (AttributeGlossaryMember) previousIssue;
    }

    @Override
    public AttributeGlossaryMember previousIssue(GlossaryMember previousIssue) {
        if (previousIssue == this.previousIssue) {
            return this;
        }

        if (!(previousIssue instanceof AttributeGlossaryMember)) {
            throw new IllegalArgumentException("previousIssue must be of type "
                    + AttributeGlossaryMember.class.getName());
        }

        return new AttributeGlossaryMember(List.of(), Set.of(), List.of(),
                                           this.dataModule, this.changeMark, previousIssue,
                                           this);

    }

    @Override
    public boolean hasAnyChangeMark() {
        return super.hasAnyChangeMark() || Set.of(ChangeType.ADDED,
                                                  ChangeType.MODIFIED,
                                                  ChangeType.DELETED)
                                              .contains(getValidValuesChangeMark().getChangeType());
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof final AttributeGlossaryMember g) {
            return super.equals(g) ||
                    parentClassName.equals(g.parentClassName) &&
                            name.equals(g.name);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(parentClassName,
                            name,
                            getClass().getCanonicalName());
    }
}