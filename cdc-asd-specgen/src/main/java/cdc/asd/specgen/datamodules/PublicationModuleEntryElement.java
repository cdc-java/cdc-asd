package cdc.asd.specgen.datamodules;

/**
 * Interface for items that are suitable for referencing
 * from the S1000D pmEntry element.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public interface PublicationModuleEntryElement {
    // Nothing
}