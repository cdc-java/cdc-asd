package cdc.asd.specgen.datamodules;

import java.util.List;

/**
 * A type (as in UML type) definition entry for the data dictionary.
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public abstract class TypeDefinitionEntry extends DataDictionaryEntry {

    private final String uof;
    private final String stereotype;
    private final String type;
    private final List<String> notes;
    private final List<String> examples;

    /**
     * Constructor
     * 
     * @param name The type name
     * @param definition The type definition
     * @param type The type meta-type name
     * @param stereotype The type stereotype
     * @param uof The type main UoF
     * @param notes Any notes
     * @param examples Any examples
     */
    public TypeDefinitionEntry(String name,
                               String definition,
                               String type,
                               String stereotype,
                               String uof,
                               List<String> notes,
                               List<String> examples) {
        super(name, definition);

        this.uof = uof;
        this.type = type;
        this.stereotype = stereotype;
        this.notes = List.copyOf(notes);
        this.examples = List.copyOf(examples);
    }

    @Override
    public final boolean isAttribute() {
        return false;
    }

    @Override
    public final List<String> getNotes() {
        return notes;
    }

    @Override
    public final List<String> getExamples() {
        return examples;
    }

    /**
     * Get the meta-type
     * 
     * @return The meta-type
     */
    public final String getType() {
        return type;
    }

    /**
     * Get the stereotype
     * 
     * @return The stereotype
     */
    public final String getStereotype() {
        return stereotype;
    }

    /**
     * Get the main UoF
     * 
     * @return The main UoF
     */
    public final String getUof() {
        return uof;
    }
}
