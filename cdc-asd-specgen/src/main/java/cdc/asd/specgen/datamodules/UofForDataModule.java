package cdc.asd.specgen.datamodules;

import java.time.LocalDate;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.specgen.EaModelIoUtils;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfType;

/**
 * Class for helping with translating a UoF into a specific
 * data module.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public final class UofForDataModule extends DataModule implements PublicationModuleEntryElement {

    private static final Logger LOGGER = LogManager.getLogger(UofForDataModule.class);
    private final String uofName;

    private final MfPackage pack;
    private final Map<String, MfType> packageMembersMap;

    /**
     * Create a {@code UofForDataModule} with all the required
     * parameters for a new data module.
     *
     * @param logoIcn The ICN that will be used as the data module's logo.
     * @param pack The UoF package for this data module.
     * @param position The position of this UoF. This allows proper
     *            calculation of the subSystem/subSubSystem codes, which are mapped
     *            on the chapter/subchapter structure of the spec.
     * @param modelIdentCode The spec name (ex. "S6000T")
     * @param systemCode The data model chapter number (ex. "07",
     *            "19", ...)
     * @param assyCode The assy code. Here, provides a hint on what
     *            UoF this Data module is about.
     * @param issueDate The issue date of the data model.
     * @param issueNumber The issue number of the data model (ex. "001")
     * @param inWork The inWork value
     * @param dmTitle The title of the data module.
     */
    public UofForDataModule(String logoIcn,
                            MfPackage pack,
                            int position,
                            String modelIdentCode,
                            String systemCode,
                            String assyCode,
                            LocalDate issueDate,
                            String issueNumber,
                            String inWork,
                            String dmTitle) {
        super(logoIcn, modelIdentCode, systemCode,
              calculateSubSystemCode(position),
              calculateSubSubSystemCode(position),
              assyCode, "040", issueDate, issueNumber, inWork, dmTitle, position);

        if (position < 1) {
            throw new IllegalArgumentException(pack.getName() + ": a UoF's subchapter "
                    + "number cannot be negative or equal to zero");
        }

        if (position > 99) {
            throw new IllegalArgumentException(pack.getName() + ": A UoF's subchapter "
                    + "number cannot exceed 99, as specified by S1000D 5.0");
        }

        if (!issueNumber.matches("\\d{3,5}")) {
            throw new IllegalArgumentException(pack.getName() + ": This UoF's issue number is invalid: " + issueNumber);
        }

        if (!systemCode.matches("[A-Z0-9]{2,3}")) {
            throw new IllegalArgumentException(pack.getName() + ": This UoF's system code is invalid: " + systemCode);
        }

        if (!inWork.matches("^\\d{2}$")) {
            throw new IllegalArgumentException(pack.getName() + ": This UoF's inWork value is invalid: " + inWork);
        }

        this.uofName = pack.getName();
        this.pack = pack;

        this.packageMembersMap = pack.getChildren()
                                     .stream()
                                     .filter(UofForDataModule::isClassOrInterface)
                                     .map(MfType.class::cast)
                                     .collect(Collectors.toMap(MfNameItem::getName,
                                                               Function.identity(),
                                                               UofForDataModule::keepFirstAndReport));
    }

    public MfPackage getPackage() {
        return pack;
    }

    public Set<String> getClassAndInterfaceNames() {
        return pack.getChildren()
                   .stream()
                   .filter(UofForDataModule::isClassOrInterface)
                   .map(MfType.class::cast)
                   .map(MfNameItem::getName)
                   .collect(Collectors.toSet());
    }

    private static boolean isClassOrInterface(MfElement object) {
        return object instanceof MfClass || object instanceof MfInterface;
    }

    /**
     * Return the UoF name exactly as spelt in the package name.
     *
     * @return The UoF name.
     */
    public String getUofName() {
        return uofName;
    }

    /**
     * Return the UoF name, reformatted for inline mention. For example,
     * "CDM UoF Breakdown Structure" becomes "Breakdown Structure UoF".
     *
     * @return The inline UoF name
     */
    public String getInlineUofName() {
        return uofName.replaceFirst(DataModules.UOF_PATTERN.toString(), "$1 UoF");
    }

    /**
     * Return the normalized UoF name. For example, "CDM UoF Breakdown Structure"
     * becomes "UoF Breakdown Structure".
     *
     * @return The normalized UoF name
     */
    public String getNormalizedUofName() {
        return uofName.replaceFirst(DataModules.UOF_PATTERN.toString(), "UoF $1");
    }

    /**
     * Return the simple name of this UoF.
     * If not possible, do the same as {@code getUofName}.
     * For example, "CDM UoF Breakdown Structure" becomes
     * "Breakdown Structure".
     *
     * @return The simple, or if not possible, the
     *         UoF name.
     */
    public String getSimpleUofName() {
        final String simpleUofName = EaModelIoUtils.getSimpleUofName(uofName);

        if (simpleUofName == uofName) {
            return getUofName();
        }

        return simpleUofName;
    }

    /**
     * Tell if a UoF member is an interface.
     *
     * @param uofMemberName The member to look up.
     * @return True if member is actually an interface.
     * @throws NoSuchElementException if member is not in this UoF.
     */
    public boolean uofMemberIsInterface(String uofMemberName) {
        return findFirstUofMember(uofMemberName) instanceof MfInterface;
    }

    /**
     * Tell if a UoF member is a class.
     *
     * @param uofMemberName The member to look up.
     * @return True if member is actually a class.
     * @throws NoSuchElementException if member is not in this UoF.
     */
    public boolean uofMemberIsClass(String uofMemberName) {
        return findFirstUofMember(uofMemberName) instanceof MfClass;
    }

    /**
     * Find the first item in this UoF's related EaPackage.
     *
     * @param uofMemberName The name to look up.
     * @return The found EaObject.
     * @throws NoSuchElementException for a lookup that does
     *             not yield any result.
     */
    private MfType findFirstUofMember(String uofMemberName) {
        final MfType obj = packageMembersMap.get(uofMemberName);

        if (obj == null) {
            throw new NoSuchElementException("UoF " + pack.getName()
                    + " has no such member named: " + uofMemberName);
        }

        return obj;
    }

    /**
     * Combine two {@link MfType} objects with the same key into a single one.
     * The logic is that there is no merging done; the first object
     * always wins and the second object is discarded.
     *
     * @param object1 The first object
     * @param object2 The second object
     * @return The first object.
     */
    private static MfType keepFirstAndReport(MfType object1,
                                             MfType object2) {
        LOGGER.warn("keepFirstAndReport({}, {}) discarded an EaObject "
                + "with duplicate key",
                    object1,
                    object2);

        return object1;
    }

    /**
     * Get the XML ID of the paragraph for the supplied class name. The class
     * is assumed to exist in this {@link UofForDataModule}, or the method will
     * fail and throw an exception as dictated in the implementation of
     * {@link Optional#get()}
     *
     * @param name The name of the class name for which to get the paragraph
     *            XML ID.
     * @return The identifier of the paragraph for identification or reference
     *         to the class.
     */
    public String getClassParagraphIdByClassName(String name) {
        return EaModelIoUtils.getXmlParaIdFromClass(pack.getAllClasses()
                                                        .stream()
                                                        .filter(c -> c.getName().equals(name))
                                                        .findFirst()
                                                        .orElseThrow());
    }

    /**
     * Get the XML ID of the paragraph for the supplied interface name. The interface
     * is assumed to exist in this {@link UofForDataModule}, or the method will fail
     * and throw an exception as dictated in the implementation of {@link Optional#get()}.
     *
     * @param name The name of the interface for which to get the paragraph XML ID.
     * @return The identifier of the paragraph for identification or reference to the
     *         interface.
     */
    public String getInterfaceParagraphIdByInterfaceName(String name) {
        return EaModelIoUtils.getXmlParaIdFromInterface(pack.getAllInterfaces()
                                                            .stream()
                                                            .filter(c -> c.getName().equals(name))
                                                            .findFirst()
                                                            .orElseThrow());
    }
}