package cdc.asd.specgen.datamodules;

import java.util.List;

/**
 * An interface for something that describes and UML object like
 * a {@link GlossaryMember} or a {@link DataDictionaryEntry}.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public interface UmlObjectDefinition {

    /**
     * This method must provide the name of the implemented UML object.
     *
     * @return The name.
     */
    public String getName();

    /**
     * This method must provide the full definition of the implemented
     * UML object.
     *
     * @return The definition.
     */
    public String getDefinition();

    /**
     * This method must return <code>true</code> if the implemented UML
     * object is a UML class.
     *
     * @return <code>true</code> or <code>false</code>
     */
    public boolean isClass();

    /**
     * This method must return <code>true</code> if the implemented UML
     * object is a UML interface.
     *
     * @return <code>true</code> or <code>false</code>
     */
    public boolean isInterface();

    /**
     * This method must return <code>true</code> if the implemented UML
     * object is a UML attribute.
     *
     * @return <code>true</code> or <code>false</code>
     */
    public boolean isAttribute();

    /**
     * This method must return <code>true</code> if the implemented UML
     * object is a package for a Unit of Functionality (UoF).
     *
     * @return <code>true</code> or <code>false</code>
     */
    public boolean isUof();

    /**
     * This method must return the notes assigned to the implemented UML
     * object. The returned {@link List} may be empty if there are none.
     *
     * @return A {@link List} of strings with any notes
     */
    public List<String> getNotes();

    /**
     * This method must return the examples assigned to the implemented
     * UML object. The returned {@link List} may be empty if there are
     * none.
     *
     * @return A {@link List} of strings with any examples.
     */
    public List<String> getExamples();
}
