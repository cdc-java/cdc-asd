package cdc.asd.specgen.datamodules;

import java.time.LocalDate;
import java.util.List;

/**
 * Class for helping with wrapping Data Module as
 * well as Publication Modules into a top-level
 * Publication Module.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public final class PublicationModuleEntryForPublicationModule implements PublicationModuleEntryElement {
    private static final String PMC_FORMAT = "PMC-%s-%s-%s-%s_%s-%s_%s-%s";
    private final String modelIdentCode;
    private final String pmIssuer = "B6865";
    private final String pmNumber;
    private final String pmVolume;
    private final String issueNumber = "000";
    private final String inWork = "01";

    private final String countryIsoCode = "US";
    private final String languageIsoCode = "EN";
    private final LocalDate issueDate;
    private final String pmTitle;
    private final String systemCode;
    private final int chapterNumber;

    private final String logoIcn;

    private final List<? extends PublicationModuleEntryElement> publicationModuleEntries;

    /**
     * Constructor.
     *
     * @param logoIcn The ICN of the logo to use for this Publication Module.
     * @param modelIdentCode The Model Ident Code, typically the spec
     *            name (e.g. S3000L).
     * @param issueDate The issue date of the data model.
     * @param pmTitle The title of the Publication Module.
     * @param pmVolume The volume of the Publication Module. S1000D allows
     *            as many as 2 digits.
     * @param chapterNumber The data model chapter number. S1000D only
     *            allows 2 or 3 digits.
     * @param publicationModuleEntries All the items to include in the
     *            resulting publication module.
     * @throws IllegalArgumentException {@code chapterNumber} or {@code
     * pmVolume} have illegal values.
     */
    public PublicationModuleEntryForPublicationModule(String logoIcn,
                                                      String modelIdentCode,
                                                      LocalDate issueDate,
                                                      String pmTitle,
                                                      int pmVolume,
                                                      int chapterNumber,
                                                      List<? extends PublicationModuleEntryElement> publicationModuleEntries) {
        if (chapterNumber > 999) {
            throw new IllegalArgumentException("This UoF PM's chapter number "
                    + "is invalid: " + chapterNumber);
        }

        if (pmVolume > 99) {
            throw new IllegalArgumentException("This PM letter can only be a character "
                    + "among A to Z. The following was provided: " + pmVolume);
        }

        this.chapterNumber = chapterNumber;
        this.systemCode = String.format("%02d", chapterNumber);
        if (this.systemCode.length() <= 2) {
            this.pmNumber = this.systemCode + "000";
        } else {
            this.pmNumber = this.systemCode + "00";
        }
        this.pmVolume = String.format("%02d", pmVolume);
        this.modelIdentCode = modelIdentCode;
        this.issueDate = issueDate;
        this.pmTitle = pmTitle;
        this.publicationModuleEntries = publicationModuleEntries;
        this.logoIcn = logoIcn;
    }

    public String getModelIdentCode() {
        return modelIdentCode;
    }

    public String getPmIssuer() {
        return pmIssuer;
    }

    public String getPmNumber() {
        return pmNumber;
    }

    public String getPmVolume() {
        return pmVolume;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public String getInWork() {
        return inWork;
    }

    public String getCountryIsoCode() {
        return countryIsoCode;
    }

    public String getLanguageIsoCode() {
        return languageIsoCode;
    }

    public String getPmTitle() {
        return pmTitle;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public String getPublicationModuleCode() {
        return String.format(PMC_FORMAT,
                             modelIdentCode,
                             pmIssuer,
                             pmNumber,
                             pmVolume,
                             issueNumber,
                             inWork,
                             languageIsoCode,
                             countryIsoCode);
    }

    public int getChapterNumber() {
        return chapterNumber;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public List<? extends PublicationModuleEntryElement> getPublicationModuleEntries() {
        return publicationModuleEntries;
    }

    public String getLogoIcn() {
        return logoIcn;
    }
}
