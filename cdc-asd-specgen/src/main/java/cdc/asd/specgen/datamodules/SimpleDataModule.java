package cdc.asd.specgen.datamodules;

import java.time.LocalDate;

/**
 * Simple Data Module for data modules that do not contain
 * actual content (technical data modules, e.g. table of contents, external
 * data modules not written by this program but that still should be referenced, etc.).
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class SimpleDataModule extends DataModule implements PublicationModuleEntryElement {

    /**
     * Create a {@code SimpleDataModule} with supplied characteristics
     *
     * @param logoIcn The ICN that will be used as the data module's logo.
     * @param modelIdentCode The model identification code.
     * @param systemCode The system code.
     * @param assyCode The assembly code.
     * @param infoCode The info code.
     * @param issueDate The issue date.
     * @param issueNumber The issue number.
     * @param inWork The in work text.
     * @param dmTitle The title.
     */
    public SimpleDataModule(String logoIcn,
                            String modelIdentCode,
                            String systemCode,
                            String assyCode,
                            String infoCode,
                            LocalDate issueDate,
                            String issueNumber,
                            String inWork,
                            String dmTitle) {

        super(logoIcn, modelIdentCode, systemCode, '0', '0',
              assyCode, infoCode, issueDate,
              issueNumber, inWork, dmTitle, 0);
    }

    /**
     * Create a {@code SimpleDataModule} with supplied characteristics
     *
     * @param logoIcn The ICN that will be used as the data module's logo.
     * @param modelIdentCode The model identification code.
     * @param systemCode The system code.
     * @param subSystemCode The subsystemcode.
     * @param subSubSystemCode The subsubsystemcode.
     * @param assyCode The assembly code.
     * @param infoCode The info code.
     * @param issueDate The issue date.
     * @param issueNumber The issue number.
     * @param inWork The in work text.
     * @param dmTitle The title.
     */
    public SimpleDataModule(String logoIcn,
                            String modelIdentCode,
                            String systemCode,
                            char subSystemCode,
                            char subSubSystemCode,
                            String assyCode,
                            String infoCode,
                            LocalDate issueDate,
                            String issueNumber,
                            String inWork,
                            String dmTitle) {

        super(logoIcn, modelIdentCode, systemCode, subSystemCode, subSubSystemCode,
              assyCode, infoCode, issueDate,
              issueNumber, inWork, dmTitle, 0);
    }
}
