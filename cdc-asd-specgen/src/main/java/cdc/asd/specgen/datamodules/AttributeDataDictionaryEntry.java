package cdc.asd.specgen.datamodules;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * An entry in the data dictionary for an attribute
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class AttributeDataDictionaryEntry extends DataDictionaryEntry {

    /**
     * The valid-value library name. May be <code>null</code>.
     */
    private final String validValueLibaryName;

    /**
     * A map relating parent classes (as map keys) and their
     * relating UoFs (as map values).
     */
    private final Map<String, String> parentClassesAndUof;

    /**
     * The type name of this attribute.
     */
    private final String type;

    /**
     * The "note" tag values
     */
    private final List<String> notes;

    /**
     * The "example" tag values
     */
    private final List<String> examples;

    /**
     * The "validValue" tag values
     */
    private final List<ValidValueDataDictionaryEntry> validValues;

    /**
     * Constructor
     * 
     * @param name The name of the attribute
     * @param definition The definition of the attribute
     * @param parentClassName The attribute's parent class name
     * @param type The attribute's type
     * @param uof The UoF of the attribute's parent UoF
     * @param validValueLibraryName The valid-value library name. May be <code>null</code>
     * @param validValues The valid-values, if any
     * @param notes The notes, if any
     * @param examples The examples, if any
     */
    public AttributeDataDictionaryEntry(String name,
                                                  String definition,
                                                  String parentClassName,
                                                  String type,
                                                  String uof,
                                                  String validValueLibraryName,
                                                  Collection<ValidValueDataDictionaryEntry> validValues,
                                                  List<String> notes,
                                                  List<String> examples) {

        super(name, definition);

        this.validValueLibaryName = validValueLibraryName;
        this.parentClassesAndUof = Map.of(parentClassName, uof);
        this.type = type;
        this.validValues = validValues.stream()
                                      .sorted(Comparator.comparing(ValidValueDataDictionaryEntry::getName))
                                      .toList();
        this.notes = List.copyOf(notes);
        this.examples = List.copyOf(examples);
    }

    private AttributeDataDictionaryEntry(Map<String, String> parentClassesAndUof,
                                                   AttributeDataDictionaryEntry original) {
        super(original.getName(), original.getDefinition());

        this.validValueLibaryName = original.validValueLibaryName;

        this.parentClassesAndUof = Stream.concat(original.parentClassesAndUof.entrySet().stream(),
                                                 parentClassesAndUof.entrySet().stream())
                                         .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
        this.type = original.type;
        this.validValues = original.validValues;
        this.notes = original.notes;
        this.examples = original.examples;
    }

    @Override
    public boolean isClass() {
        return false;
    }

    @Override
    public boolean isInterface() {
        return false;
    }

    @Override
    public boolean isAttribute() {
        return true;
    }

    @Override
    public List<String> getNotes() {
        return notes;
    }

    @Override
    public List<String> getExamples() {
        return examples;
    }

    /**
     * Get this object's type name
     * 
     * @return The type name
     */
    public String getType() {
        return type;
    }

    /**
     * Get this object's valid-values
     * 
     * @return The valid-values if any or an empty collection
     */
    public final List<ValidValueDataDictionaryEntry> getValidValues() {
        return validValues;
    }

    /**
     * Get this object's valid-values library names
     * 
     * @return The valid-values library name or <code>null</code>
     */
    public final String getValidValueLibraryName() {
        return validValueLibaryName;
    }

    /**
     * Get the parent classes and their relating UoF.
     * 
     * @return A {@link Map} with parent classes names as keys
     *         and parent classes' UoF as values
     */
    public final Map<String, String> getParentClassesAndUof() {
        return parentClassesAndUof;
    }

    /**
     * Add a parent class name and UoF
     * 
     * @param className The class name
     * @param uof The UoF
     * @return A new {@link AttributeDataDictionaryEntry} with the
     *         supplied class name and UoF
     */
    public final AttributeDataDictionaryEntry addParent(String className,
                                                                  String uof) {
        return addParents(Map.of(className, uof));
    }

    /**
     * Add parent class names and UoFs
     * 
     * @param parentClassesAndUof A {@link Map} with parent class names as keys and
     *            parent class UoFs as values
     * @return A new {@link AttributeDataDictionaryEntry} with the supplied
     *         class names and UoFs
     */
    public final AttributeDataDictionaryEntry addParents(Map<String, String> parentClassesAndUof) {
        return new AttributeDataDictionaryEntry(parentClassesAndUof, this);
    }

    /**
     * Tests whether this object has a valid-value or a valid-value library tag.
     * 
     * @return <code>true</code> if there is at least one valid-value tag
     *         or one valid-value library tag on this property, <code>false</code>
     *         otherwise
     */
    public final boolean hasValidValue() {
        return validValueLibaryName != null || validValues.size() > 0;
    }
}
