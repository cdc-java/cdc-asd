package cdc.asd.specgen.datamodules;

/**
 * An entry for the data dictionary.
 * A definition has at least a name and a definition
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public abstract class DataDictionaryEntry implements UmlObjectDefinition {

    /**
     * The entry name
     */
    private final String name;

    /**
     * The entry definition
     */
    private final String definition;

    /**
     * Constructor
     * 
     * @param name The entry name
     * @param definition The entry definition
     */
    public DataDictionaryEntry(String name,
                                         String definition) {
        this.name = name;
        this.definition = definition;
    }

    @Override
    public final String getName() {
        return name;
    }

    @Override
    public final String getDefinition() {
        return definition;
    }

    @Override
    public final String toString() {
        return String.format("%s [name=%s]", getClass().getSimpleName(), name);
    }

    @Override
    public final boolean isUof() {
        return false;
    }
}
