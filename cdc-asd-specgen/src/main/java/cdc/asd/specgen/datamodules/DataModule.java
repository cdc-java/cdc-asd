package cdc.asd.specgen.datamodules;

import java.time.LocalDate;

public abstract class DataModule {

    protected static final String DMC_FORMAT = "DMC-%s-%s-%s-%s%s-%s-%s%s-%s%s-%s-%s-%s_EN-US";

    protected final int position;

    protected final String modelIdentCode;
    protected final String systemCode;
    protected final String systemDifferenceCode = "A";
    protected final char subSystemCode;
    protected final char subSubSystemCode;

    protected final LocalDate issueDate;
    protected final String issueNumber;
    protected final String inWork;
    protected final String dmTitle;
    protected final String assyCode;
    protected final String disassyCode = "00";
    protected final String disassyCodeVariant = "A";
    protected final String infoCodeVariant = "A";
    protected final String itemLocationCode = "D";
    protected final String infoCode;

    private final String logoIcn;

    protected DataModule(String logoIcn,
                         String modelIdentCode,
                         String systemCode,
                         char subSystemCode,
                         char subSubSystemCode,
                         String assyCode,
                         String infoCode,
                         LocalDate issueDate,
                         String issueNumber,
                         String inWork,
                         String dmTitle,
                         int position) {

        this.modelIdentCode = modelIdentCode;

        this.systemCode = systemCode;
        this.subSystemCode = subSystemCode;
        this.subSubSystemCode = subSubSystemCode;
        this.position = position;
        this.issueDate = issueDate;
        this.issueNumber = issueNumber;
        this.inWork = inWork;
        this.dmTitle = dmTitle;
        this.assyCode = assyCode;
        this.infoCode = infoCode;
        this.logoIcn = logoIcn;
    }

    public final String getModelIdentCode() {
        return modelIdentCode;
    }

    public final String getSystemCode() {
        return systemCode;
    }

    public final char getSubSystemCode() {
        return subSystemCode;
    }

    public final char getSubSubSystemCode() {
        return subSubSystemCode;
    }

    public final int getPosition() {
        return position;
    }

    public final LocalDate getIssueDate() {
        return issueDate;
    }

    public final String getIssueNumber() {
        return issueNumber;
    }

    public final String getInWork() {
        return inWork;
    }

    public final String getDmTitle() {
        return dmTitle;
    }

    public final String getAssyCode() {
        return assyCode;
    }

    public final String getDisassyCode() {
        return disassyCode;
    }

    public final String getDisassyCodeVariant() {
        return disassyCodeVariant;
    }

    public final String getInfoCode() {
        return infoCode;
    }

    public final String getInfoCodeVariant() {
        return infoCodeVariant;
    }

    public final String getItemLocationCode() {
        return itemLocationCode;
    }

    public final String getSystemDifferenceCode() {
        return systemDifferenceCode;
    }

    static char calculateSubSubSystemCode(int position) {
        final char[] subChapter = String.valueOf(position).toCharArray();

        if (subChapter.length == 1) {
            return subChapter[0];
        } else {
            return subChapter[1];
        }
    }

    static char calculateSubSystemCode(int position) {
        final char[] subChapter = String.valueOf(position).toCharArray();

        if (subChapter.length == 1) {
            return '0';
        } else {
            return subChapter[0];
        }
    }

    /**
     * Get a formatted data module code (DMC) for this data module.
     *
     * @return The formatted DMC.
     */
    public final String getDataModuleCode() {
        return String.format(DMC_FORMAT,
                             modelIdentCode,
                             systemDifferenceCode,
                             systemCode,
                             subSystemCode,
                             subSubSystemCode,
                             assyCode,
                             disassyCode,
                             disassyCodeVariant,
                             infoCode,
                             infoCodeVariant,
                             itemLocationCode,
                             issueNumber,
                             inWork);
    }

    public String getLogoIcn() {
        return logoIcn;
    }
}
