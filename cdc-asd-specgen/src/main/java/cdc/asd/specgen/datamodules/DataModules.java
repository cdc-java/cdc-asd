package cdc.asd.specgen.datamodules;

import java.util.List;
import java.util.regex.Pattern;

import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.io.data.Attribute;
import cdc.io.data.Element;

/**
 * Utility class for data modules. Includes functions to convert this
 * Java model to S1000D constructs using {@link cdc.io.data.Element}.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public final class DataModules {
    /**
     * A regex pattern for detecting and capturing the name of a UoF.
     */
    public static final Pattern UOF_PATTERN = Pattern.compile("^[^\s]+ (.*)$");

    /**
     * The legacy regex pattern for {@link DataModules#UOF_PATTERN}.
     */
    public static final Pattern LEGACY_UOF_PATTERN = Pattern.compile("^[^\s]+ [^\s]+ (.*)$");

    private DataModules() {
    }

    /**
     * Create an &lt;internalRef&gt; element with the supplied reference.
     *
     * @param reference The reference to place in the internalRef
     * @return A new element.
     */
    public static Element createInternalRef(String reference) {
        final Element internalRefElement =
                new Element(S1000DNode.INTERNALREF);

        internalRefElement.addAttributes(new Attribute(S1000DNode.INTERNALREFTARGETTYPE,
                                                       S1000DNode.INTERNALREFTARGETTYPEVALUE),
                                         new Attribute(S1000DNode.INTERNALREFID,
                                                       reference));
        return internalRefElement;
    }

    /**
     * Create a &lt;dmRef&gt; element with the supplied reference
     * and its relating data module.
     *
     * @param reference The reference to place in the dmRef. May be
     *            <code>null</code>.
     * @param dataModule The data module for that reference.
     * @return A new element.
     * @apiNote The <code>reference</code> attribute is not used as per issue #63.
     *          Indeed, the stylesheet used for processing the data modules does not
     *          utilize that attribute and we want to avoid pushing unnecessary data
     *          into the data modules.
     */
    public static Element createDmRef(String reference,
                                      DataModule dataModule) {
        final Element dmRefElement = createDmRef(dataModule);

        // Issue #63
        // In the future we may want to reactivate the below line
        // dmRefElement.addAttribute(S1000DNode.REFERREDFRAGMENT, reference);

        return dmRefElement;
    }

    /**
     * Create a &lt;dmRef&gt; element with the supplied reference
     * and its relating data module.
     *
     * @param dataModule The data module for that reference.
     * @return A new element.
     */
    public static Element createDmRef(DataModule dataModule) {
        final Element dmRefElement =
                new Element(S1000DNode.DMREF);

        dmRefElement.addElement(S1000DNode.DMREFIDENT)
                    .addElement(S1000DNode.DMCODE)
                    .addAttributes(List.of(new Attribute(S1000DNode.MODELIDENTCODE,
                                                         dataModule.getModelIdentCode()),
                                           new Attribute(S1000DNode.SYSTEMDIFFCODE,
                                                         dataModule.getSystemDifferenceCode()),
                                           new Attribute(S1000DNode.SYSTEMCODE,
                                                         dataModule.getSystemCode()),
                                           new Attribute(S1000DNode.SUBSYSTEMCODE,
                                                         String.valueOf(dataModule.getSubSystemCode())),
                                           new Attribute(S1000DNode.SUBSUBSYSTEMCODE,
                                                         String.valueOf(dataModule.getSubSubSystemCode())),
                                           new Attribute(S1000DNode.ASSYCODE,
                                                         dataModule.getAssyCode()),
                                           new Attribute(S1000DNode.DISASSYCODE,
                                                         dataModule.getDisassyCode()),
                                           new Attribute(S1000DNode.DISASSYCODEVARIANT,
                                                         dataModule.getDisassyCodeVariant()),
                                           new Attribute(S1000DNode.INFOCODE,
                                                         dataModule.getInfoCode()),
                                           new Attribute(S1000DNode.INFOCODEVARIANT,
                                                         dataModule.getInfoCodeVariant()),
                                           new Attribute(S1000DNode.ITEMLOCATIONCODE,
                                                         dataModule.getItemLocationCode())));

        dmRefElement.addElement(S1000DNode.DMREFADDRESSITEMS)
                    .addElement(S1000DNode.DMTITLE)
                    .addElement(S1000DNode.TECHNAME)
                    .addText(dataModule.getDmTitle());

        return dmRefElement;
    }

    /**
     * Create a &lt;pmRef&gt; element with the supplied reference
     * and its relating data module.
     *
     * @param pm The publication module for that reference.
     * @return A new element.
     */
    public static Element createPmRef(PublicationModuleEntryForPublicationModule pm) {
        final Element pmRefElement =
                new Element(S1000DNode.PMREF);

        final Element pmRefIdentElement =
                pmRefElement.addElement(S1000DNode.PMREFIDENT);

        pmRefIdentElement.addElement(S1000DNode.PMCODE)
                         .addAttributes(List.of(new Attribute(S1000DNode.MODELIDENTCODE,
                                                              pm.getModelIdentCode()),
                                                new Attribute(S1000DNode.PMISSUER,
                                                              pm.getPmIssuer()),
                                                new Attribute(S1000DNode.PMNUMBER,
                                                              pm.getPmNumber()),
                                                new Attribute(S1000DNode.PMVOLUME,
                                                              pm.getPmVolume())));

        pmRefIdentElement.addElement(S1000DNode.ISSUEINFO)
                         .addAttributes(List.of(new Attribute(S1000DNode.ISSUENUMBER,
                                                              pm.getIssueNumber()),
                                                new Attribute(S1000DNode.INWORK,
                                                              pm.getInWork())));

        return pmRefElement;
    }

}
