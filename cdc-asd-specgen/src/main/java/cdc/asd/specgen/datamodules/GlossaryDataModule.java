package cdc.asd.specgen.datamodules;

import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * A Data Module, specifically designed to hold glossary data.
 * Glossary data encompasses members (the terms that make up the glossary) that begin
 * with a given character (a latin character) and a chapter number for proper identification
 * of the Data Module.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class GlossaryDataModule
        extends DataModule
        implements PublicationModuleEntryElement,
        Comparable<GlossaryDataModule> {

    /**
     * The sorted list of members.
     */
    private final Map<String, GlossaryMember> members;

    /**
     * The letter for this glossary data module.
     */
    private final char glossaryLetter;

    /**
     * The Data Module title for this glossary.
     */
    public static final String GLOSSARY_DATA_MODULE_TITLE = "Glossary - %c";

    /**
     * The chapter number for this glossary. Required to properly
     * identify its Data Module for future reference.
     */
    private final String chapterNumber;

    /**
     * Constructor.
     *
     * @param logoIcn The spec's logo ICN to use.
     * @param modelIdentCode The spec name (e.g. "SX001G")
     * @param chapterNumber The chapter number for this glossary section.
     * @param assyCode The assy code. Should suggest the letter for this
     *            glossary section.
     * @param issueDate The issue date of the glossary spec
     * @param issueNumber The issue number of this data module (e.g. "001")
     * @param inWork The inWork number (e.g. "00")
     * @param glossaryLetter The letter associated with this glossary Data Module
     * @param members The terms to include into this glossary section
     */
    public GlossaryDataModule(String logoIcn,
                              String modelIdentCode,
                              int chapterNumber,
                              String assyCode,
                              LocalDate issueDate,
                              String issueNumber,
                              String inWork,
                              char glossaryLetter,
                              Collection<GlossaryMember> members) {
        super(logoIcn, modelIdentCode, chapterToSystemCode(chapterNumber),
              calculateSubSystemCode(glossaryLetterToPosition(glossaryLetter)),
              calculateSubSubSystemCode(glossaryLetterToPosition(glossaryLetter)),
              assyCode, "040", issueDate, issueNumber, inWork,
              String.format(String.format(GLOSSARY_DATA_MODULE_TITLE, glossaryLetter)),
              glossaryLetterToPosition(glossaryLetter));

        this.glossaryLetter = glossaryLetter;

        this.members = members.parallelStream()
                              .sorted()
                              .collect(Collectors.toMap(GlossaryMember::getName,
                                                        Function.identity(),
                                                        GlossaryDataModule::mergeGlossaryMembers));

        this.chapterNumber = String.valueOf(chapterNumber);
    }

    /**
     * A merging function for several instances of {@link GlossaryMember} which are equal to
     * each other. Currently, this case should only ever happen with attributes that have
     * the same name, definition, valid-values, etc., but do not have the same parent class.
     * The merging function will simply merge the references together and issue a new
     * resulting {@link GlossaryMember}.
     *
     * @param m1 The first {@link GlossaryMember}
     * @param m2 The second {@link GlossaryMember}
     * @return A new {@link GlossaryMember}
     */
    private static GlossaryMember mergeGlossaryMembers(GlossaryMember m1,
                                                       GlossaryMember m2) {
        return m1.references(m2.getReferences());
    }

    /**
     * Get the letter associated with this glossary Data Module.
     *
     * @return The glossary letter.
     */
    public char getGlossaryLetter() {
        return glossaryLetter;
    }

    @Override
    public int compareTo(GlossaryDataModule other) {
        return Character.compare(glossaryLetter, other.glossaryLetter);
    }

    /**
     * Convert the glossary letter to a number. This in turn helps generate the
     * sub/subsubsystem codes for proper identification of this Data Module.
     *
     * @param letter The glossary letter
     * @return The positional number of this glossary Data Module.
     */
    private static int glossaryLetterToPosition(char letter) {
        return 1 + letter - 'A';
    }

    /**
     * Convert chapter number into systemCode for proper identification
     * of this Data Module.
     *
     * @param chapter The chapter number.
     * @return The formatted systemCode
     */
    private static String chapterToSystemCode(int chapter) {
        return String.format("%02d", chapter);
    }

    /**
     * Get the chapter number associated with this glossary Data Module.
     *
     * @return The chapter number.
     */
    public String getChapterNumber() {
        return chapterNumber;
    }

    /**
     * Get a member with its textual representation (or name). Before it is
     * returned, the member is modified to trace back to this object.
     *
     * @param term The name of the term.
     * @return The {@code GlossaryMember} object or {@code null}.
     */
    public GlossaryMember getMember(String term) {
        return members.get(term).dataModule(this);
    }

    /**
     * Get the members of this glossary Data Module.
     * Before they are returned, the members are modified to trace back to
     * this object.
     *
     * @return the members.
     */
    public List<GlossaryMember> getMembers() {
        return members.values()
                      .parallelStream()
                      .map(member -> member.dataModule(this))
                      .sorted()
                      .toList();
    }
}