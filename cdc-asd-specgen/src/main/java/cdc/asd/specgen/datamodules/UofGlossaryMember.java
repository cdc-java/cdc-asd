package cdc.asd.specgen.datamodules;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import cdc.asd.specgen.EaModelIoUtils;
import cdc.asd.specgen.s1000d5.ChangeMark;

/**
 * A glossary member, specifically one that is a UoF.
 * A {@code UofGlossaryMember} has no specific characteristic beyond
 * the {@code GenericGlossaryMember} but has specific functions to
 * address minor layout issues.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class UofGlossaryMember extends GlossaryMember {

    /**
     * The full UoF name (e.g. CDM UoF Breakdown Structure)
     */
    private final String fullUofName;

    public UofGlossaryMember(String name,
                             String definition,
                             UofGlossaryMember previousIssue) {
        super(moveUofKeyword(name), definition, ObjectType.UOF, previousIssue);
        fullUofName = name;
    }

    /**
     * Copy constructor with notes, references, examples.
     *
     * @param notes The notes to add. Can be an empty list if nothing to add.
     * @param references The references to add. Can be an empty list if nothing to add.
     * @param examples The examples to add. Can be an empty list if nothing to add.
     * @param dataModule The data module to set for this new object.
     * @param changeMark The {@link ChangeMark} associated to this object.
     * @param previousIssue The previous issue of the same {@link UofGlossaryMember}.
     * @param original The original object. Each attribute will be copied over.
     */
    private UofGlossaryMember(List<String> notes,
                              Set<String> references,
                              List<String> examples,
                              GlossaryDataModule dataModule,
                              ChangeMark changeMark,
                              GlossaryMember previousIssue,
                              UofGlossaryMember original) {
        super(notes, references, examples, dataModule,
              changeMark, previousIssue, original);
        fullUofName = original.fullUofName;
    }

    /**
     * Move the UoF keyword to the end. Example: UoF Breakdown Structure becomes
     * Breakdown Structure UoF.
     *
     * @param uofName The UoF name, with "UoF" followed with a name.
     * @return The UoF name, with the "UoF" keyword moved to the tail.
     * @implNote Since CDM 2024, the UoF keyword has been removed from the source package
     *           name. This implementation accepts both types of UoF naming for legacy support.
     */
    private static String moveUofKeyword(String uofName) {
        if (!uofName.contains("UoF ")) {
            return uofName.replaceFirst(DataModules.UOF_PATTERN.pattern(),
                                        "$1 UoF");
        }

        return uofName.replaceFirst(DataModules.LEGACY_UOF_PATTERN.pattern(), "$1 UoF");

    }

    /**
     * Return the normalized UoF name. For example, "CDM UoF Breakdown Structure"
     * becomes "UoF Breakdown Structure".
     *
     * @return The normalized UoF name
     */
    public String getNormalizedUofName() {
        return fullUofName.replaceFirst(DataModules.UOF_PATTERN.pattern(),
                                        "UoF $1");
    }

    @Override
    public UofGlossaryMember note(String note) {
        return new UofGlossaryMember(List.of(note), Set.of(),
                                     List.of(), this.dataModule,
                                     this.changeMark, this.previousIssue,
                                     this);
    }

    @Override
    public UofGlossaryMember notes(List<String> notes) {
        if (notes.isEmpty()) {
            return this;
        }

        return new UofGlossaryMember(notes, Set.of(),
                                     List.of(), this.dataModule,
                                     this.changeMark, this.previousIssue,
                                     this);
    }

    @Override
    public UofGlossaryMember references(Set<String> references) {
        if (references.isEmpty()) {
            return this;
        }

        return new UofGlossaryMember(List.of(), references,
                                     List.of(), this.dataModule,
                                     this.changeMark, this.previousIssue,
                                     this);
    }

    @Override
    public UofGlossaryMember reference(String reference) {
        return new UofGlossaryMember(List.of(), Set.of(reference),
                                     List.of(), this.dataModule,
                                     this.changeMark, this.previousIssue,
                                     this);
    }

    @Override
    public UofGlossaryMember examples(List<String> examples) {
        if (examples.isEmpty()) {
            return this;
        }

        return new UofGlossaryMember(List.of(), Set.of(),
                                     examples, this.dataModule,
                                     this.changeMark, this.previousIssue,
                                     this);
    }

    @Override
    public GlossaryMember dataModule(GlossaryDataModule dataModule) {
        return new UofGlossaryMember(List.of(), Set.of(), List.of(),
                                     dataModule, this.changeMark,
                                     this.previousIssue, this);
    }

    @Override
    public UofGlossaryMember changeMark(ChangeMark changeMark) {
        return new UofGlossaryMember(List.of(), Set.of(), List.of(), this.dataModule,
                                     changeMark, this.previousIssue, this);
    }

    @Override
    public UofGlossaryMember getPreviousIssue() {
        return (UofGlossaryMember) previousIssue;
    }

    @Override
    public UofGlossaryMember previousIssue(GlossaryMember previousIssue) {
        if (previousIssue == this.previousIssue) {
            return this;
        }

        if (!(previousIssue instanceof UofGlossaryMember)) {
            throw new IllegalArgumentException("previousIssue must be of type "
                    + UofGlossaryMember.class.getName());
        }

        return new UofGlossaryMember(List.of(), Set.of(), List.of(), this.dataModule,
                                     this.changeMark, previousIssue, this);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof final UofGlossaryMember g) {
            return super.equals(g) || EaModelIoUtils.getSimpleUofName(name)
                                                    .equals(EaModelIoUtils.getSimpleUofName(g.name));
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, getClass().getCanonicalName());
    }
}
