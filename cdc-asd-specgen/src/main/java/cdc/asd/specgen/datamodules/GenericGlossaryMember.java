package cdc.asd.specgen.datamodules;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import cdc.asd.specgen.s1000d5.ChangeMark;

/**
 * A generic glossary member that has no specific characteristic
 * beyond the abstract {@link GlossaryMember}.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class GenericGlossaryMember extends GlossaryMember {

    /**
     * The previous name of this entity.
     */
    private final String replacesField;

    /**
     * Copy constructor.
     *
     * @param notes The notes to add to the original object
     * @param references The references to add to the original object
     * @param examples The examples to add to the original object
     * @param dataModule The data module to set to this new object.
     * @param replaces The previous name of this entity. May be <code>null</code>.
     * @param changeMark The {@link ChangeMark} associated with this object.
     * @param previousIssue The previous issue of the same {@link GenericGlossaryMember}.
     * @param original The original object
     */
    private GenericGlossaryMember(List<String> notes,
                                  Set<String> references,
                                  List<String> examples,
                                  GlossaryDataModule dataModule,
                                  String replaces,
                                  ChangeMark changeMark,
                                  GlossaryMember previousIssue,
                                  GenericGlossaryMember original) {
        super(notes, references, examples, dataModule,
              changeMark, previousIssue, original);
        this.replacesField = replaces;
    }

    /**
     * Constructor.
     *
     * @param name Name of this {@link GenericGlossaryMember}
     * @param definition Definition of this {@link GenericGlossaryMember}
     * @param replaces The previous name of this entity. May be <code>null</code>.
     * @param type Type of {@link GenericGlossaryMember}
     * @param previousIssue The previous issue of this {@link GenericGlossaryMember}.
     *            May be <code>null</code>.
     */
    public GenericGlossaryMember(String name,
                                 String definition,
                                 String replaces,
                                 ObjectType type,
                                 GenericGlossaryMember previousIssue) {
        super(name, definition, type, previousIssue);
        this.replacesField = replaces;
    }

    @Override
    public GenericGlossaryMember note(String note) {
        return new GenericGlossaryMember(List.of(note), Set.of(),
                                         List.of(), this.dataModule,
                                         this.replacesField,
                                         this.changeMark,
                                         this.previousIssue,
                                         this);
    }

    @Override
    public GenericGlossaryMember notes(List<String> notes) {
        if (notes.isEmpty()) {
            return this;
        }

        return new GenericGlossaryMember(notes, Set.of(),
                                         List.of(), this.dataModule,
                                         this.replacesField,
                                         this.changeMark,
                                         this.previousIssue,
                                         this);
    }

    @Override
    public GenericGlossaryMember references(Set<String> references) {
        if (references.isEmpty()) {
            return this;
        }

        return new GenericGlossaryMember(List.of(), references,
                                         List.of(), this.dataModule,
                                         this.replacesField,
                                         this.changeMark,
                                         this.previousIssue,
                                         this);
    }

    @Override
    public GenericGlossaryMember reference(String reference) {
        return new GenericGlossaryMember(List.of(), Set.of(reference),
                                         List.of(), this.dataModule,
                                         this.replacesField,
                                         this.changeMark,
                                         this.previousIssue,
                                         this);
    }

    @Override
    public GenericGlossaryMember examples(List<String> examples) {
        if (examples.isEmpty()) {
            return this;
        }

        return new GenericGlossaryMember(List.of(), Set.of(),
                                         examples, this.dataModule,
                                         this.replacesField,
                                         this.changeMark,
                                         this.previousIssue, this);
    }

    @Override
    public GenericGlossaryMember dataModule(GlossaryDataModule dataModule) {
        return new GenericGlossaryMember(List.of(), Set.of(), List.of(),
                                         dataModule, this.replacesField,
                                         this.changeMark,
                                         this.previousIssue,
                                         this);
    }

    /**
     * Get the "replaces" field.
     *
     * @return The replaces field or <code>null</code>.
     */
    public String getReplaces() {
        return replacesField;
    }

    @Override
    public GenericGlossaryMember changeMark(ChangeMark changeMark) {
        if (changeMark == this.changeMark || changeMark == null) {
            return this;
        }

        return new GenericGlossaryMember(List.of(), Set.of(), List.of(),
                                         this.dataModule, this.replacesField,
                                         changeMark, this.previousIssue, this);
    }

    @Override
    public GenericGlossaryMember getPreviousIssue() {
        return (GenericGlossaryMember) previousIssue;
    }

    @Override
    public GenericGlossaryMember previousIssue(GlossaryMember previousIssue) {
        if (this.previousIssue == previousIssue) {
            return this;
        }

        if (!(previousIssue instanceof GenericGlossaryMember)) {
            throw new IllegalArgumentException("previousIssue must be of type "
                    + GenericGlossaryMember.class.getName());
        }

        return new GenericGlossaryMember(List.of(), Set.of(), List.of(), this.dataModule, this.replacesField,
                                         this.changeMark, previousIssue, this);
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof final GenericGlossaryMember g) {
            return super.equals(g) ||
                    name.equals(g.name);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, getClass().getCanonicalName());
    }

}
