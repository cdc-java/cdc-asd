package cdc.asd.specgen.datamodules;

import java.util.List;

/**
 * An entry in the data dictionary for an interface.
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class InterfaceDataDictionaryEntry extends TypeDefinitionEntry {

    /**
     * Constructor
     * 
     * @param name The interface name
     * @param definition The interface definition
     * @param type The interface type name
     * @param stereotype The interface stereotype
     * @param uof The interface's main UoF
     * @param notes Any notes
     * @param examples Any examples
     */
    public InterfaceDataDictionaryEntry(String name,
                                                  String definition,
                                                  String type,
                                                  String stereotype,
                                                  String uof,
                                                  List<String> notes,
                                                  List<String> examples) {
        super(name, definition, type, stereotype, uof, notes, examples);
    }

    @Override
    public boolean isClass() {
        return false;
    }

    @Override
    public boolean isInterface() {
        return true;
    }
}
