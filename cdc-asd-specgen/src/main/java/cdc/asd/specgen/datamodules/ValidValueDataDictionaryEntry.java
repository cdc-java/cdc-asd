package cdc.asd.specgen.datamodules;

import java.util.List;

/**
 * An entry in the data dictionary for an attribute's valid value
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class ValidValueDataDictionaryEntry extends DataDictionaryEntry {

    /**
     * Constructor
     * 
     * @param name The valid-value code (e.g. R)
     * @param definition The valid-value full definition (e.g.
     *            SX001G:reactiveObsolescenceManagement)
     */
    public ValidValueDataDictionaryEntry(String name,
                                                   String definition) {
        super(name, definition);
    }

    @Override
    public boolean isClass() {
        return false;
    }

    @Override
    public boolean isInterface() {
        return false;
    }

    @Override
    public boolean isAttribute() {
        return false;
    }

    @Override
    public List<String> getNotes() {
        return List.of();
    }

    @Override
    public List<String> getExamples() {
        return List.of();
    }

}
