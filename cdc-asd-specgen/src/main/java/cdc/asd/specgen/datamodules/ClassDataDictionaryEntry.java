package cdc.asd.specgen.datamodules;

import java.util.List;

/**
 * An entry in the data dictionary for a class
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class ClassDataDictionaryEntry extends TypeDefinitionEntry {

    /**
     * Constructor
     * 
     * @param name The class name
     * @param definition The class definition
     * @param type The class type name
     * @param stereotype The class stereotype
     * @param uof The class main UoF
     * @param notes Any notes
     * @param examples Any examples
     */
    public ClassDataDictionaryEntry(String name,
                                              String definition,
                                              String type,
                                              String stereotype,
                                              String uof,
                                              List<String> notes,
                                              List<String> examples) {
        super(name, definition, type, stereotype, uof, notes, examples);
    }

    @Override
    public boolean isClass() {
        return true;
    }

    @Override
    public boolean isInterface() {
        return false;
    }
}
