package cdc.asd.specgen.datamodules;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.specgen.s1000d5.ChangeMark;
import cdc.asd.specgen.s1000d5.ChangeMark.ChangeType;

/**
 * A glossary entry.
 * A glossary entry has the following characteristics:
 * <ul>
 * <li>A name, which appears as the glossary entry section title;</li>
 * <li>A definition, which is displayed at the top of the glossary entry, right
 * after the title;</li>
 * <li>A list of notes;</li>
 * <li>A list of references;</li>
 * <li>A list of examples;</li>
 * <li>A type.</li>
 * </ul>
 * All those details will allow generating an entry with all appropriate information.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public abstract class GlossaryMember implements Comparable<GlossaryMember>, UmlObjectDefinition {

    /**
     * An enum for the object type of a glossary member.
     * This gives the possibility to change the rendering/layout depending on type.
     * For example, attributes may have an extra section for valid-values.
     */
    public enum ObjectType {
        UOF,
        GENERIC_TERM,
        CLASS,
        SELECT_INTERFACE,
        EXTEND_INTERFACE,
        ATTRIBUTE
    }

    private static final String TYPEFIELD_UOF = "Unit of Functionality";
    private static final String TYPEFIELD_CLASS = "UML class";
    private static final String TYPEFIELD_SELECT_INTERFACE = "UML <<select>> interface";
    private static final String TYPEFIELD_EXTEND_INTERFACE = "UML <<extend>> interface";
    private static final String TYPEFIELD_GENERIC_TERM = "Generic term";
    private static final String TYPEFIELD_ATTRIBUTE = "Attribute";
    public static final String GLOSSARY_MEMBER_ID_TEMPLATE = "glossary-member-%s";
    public static final String GLOSSARY_MEMBER_RFU_TEMPLATE = "DMEWG-CDM2024-%s";

    /**
     * The previous issue of this glossary member.
     */
    protected final GlossaryMember previousIssue;

    /**
     * The definition of this glossary member.
     */
    private final String definition;

    /**
     * The name of this glossary member (i.e. the word being defined).
     */
    protected final String name;

    /**
     * The notes of this glossary member.
     */
    protected List<String> notesField = List.of();

    /**
     * The references of this glossary member.
     */
    protected Set<String> referencesField = Set.of();

    /**
     * The examples of this glossary member.
     */
    protected List<String> examplesField = List.of();

    /**
     * The type of this glossary member.
     */
    protected final ObjectType type;

    /**
     * The glossary data module this glossary member belongs to.
     */
    protected GlossaryDataModule dataModule = null;

    /**
     * The entire glossary member change type.
     */
    protected final ChangeMark changeMark;

    /**
     * Constructor.
     *
     * @param name The entry in the glossary.
     * @param definition The definition for this entry.
     * @param type The object type.
     * @param previousIssue The previous issue of this entry. May be <code>null</code>.
     */
    protected GlossaryMember(String name,
                             String definition,
                             ObjectType type,
                             GlossaryMember previousIssue) {
        this.definition = definition;
        this.name = name;
        this.type = type;
        this.previousIssue = previousIssue;

        this.changeMark = new ChangeMark(ChangeType.UNDETERMINED);
    }

    /**
     * Copy constructor.
     *
     * @param original Original object.
     */
    protected GlossaryMember(GlossaryMember original) {
        this(original.changeMark, original.previousIssue, original);
    }

    protected GlossaryMember(ChangeMark changeMark,
                             GlossaryMember previousIssue,
                             GlossaryMember original) {
        this.name = original.name;
        this.definition = original.definition;
        this.type = original.type;
        this.previousIssue = previousIssue;

        this.dataModule = original.dataModule;

        this.notesField = original.notesField;
        this.referencesField = original.referencesField;
        this.examplesField = original.examplesField;
        if (!Set.of(ChangeType.NONE, ChangeType.UNDETERMINED)
                .contains(changeMark.getChangeType())) {

            if (shouldGetPreviousIssueRfuId(changeMark.getChangeType(), previousIssue)) {
                this.changeMark = new ChangeMark(changeMark.getChangeType(),
                                                 previousIssue.getRfuId());
            } else {
                this.changeMark = new ChangeMark(changeMark.getChangeType(), original.getRfuId());
            }
        } else {
            this.changeMark = new ChangeMark(changeMark.getChangeType());
        }
    }

    protected GlossaryMember(List<String> notes,
                             Set<String> references,
                             List<String> examples,
                             GlossaryDataModule dataModule,
                             ChangeMark changeMark,
                             GlossaryMember previousIssue,
                             GlossaryMember original) {
        this(changeMark, previousIssue, original);
        this.dataModule = dataModule;

        this.referencesField = Stream.concat(this.referencesField.stream(),
                                             references.stream())
                                     .collect(Collectors.toUnmodifiableSet());

        this.notesField = Stream.concat(this.notesField.stream(),
                                        notes.stream())
                                .toList();

        this.examplesField = Stream.concat(this.examplesField.stream(),
                                           examples.stream())
                                   .toList();
    }

    /**
     * Add one note.
     *
     * @param note The note to add.
     * @return A new {@code GlossaryMember}.
     */
    public abstract GlossaryMember note(String note);

    /**
     * Add notes.
     *
     * @param notes The notes to add. May be empty.
     * @return A new {@code GlossaryMember} or this object if
     *         supplied input is empty.
     */
    public abstract GlossaryMember notes(List<String> notes);

    /**
     * Add references.
     *
     * @param references The references to add. May be empty.
     * @return A new {@code GlossaryMember} or this object
     *         if supplied input is empty.
     */
    public abstract GlossaryMember references(Set<String> references);

    /**
     * Add one reference.
     *
     * @param reference The reference to add.
     * @return A new {@code GlossaryMember}.
     */
    public abstract GlossaryMember reference(String reference);

    /**
     * Add examples.
     *
     * @param examples The examples to add. May be empty.
     * @return A new {@code GlossaryMember} or this object if
     *         supplied input is empty.
     */
    public abstract GlossaryMember examples(List<String> examples);

    /**
     * Set the {@link ChangeMark} for this {@link GlossaryMember}.
     *
     * @param changeMark The {@link ChangeMark}. May be <code>null</code>.
     * @return A new {@link GlossaryMember} or this object if supplied
     *         input is <code>null</code> or identical to this object's
     *         current value.
     */
    public abstract GlossaryMember changeMark(ChangeMark changeMark);

    /**
     * Predicate for a glossary member whose name's first character is the one supplied
     *
     * @param character The character looked up.
     * @return The predicate.
     */
    public static final Predicate<GlossaryMember> hasFirstLetter(char character) {
        return member -> member.name.toUpperCase().charAt(0) == Character.toUpperCase(character);
    }

    /**
     * Get the previous issue of this object.
     *
     * @return The previous issue or <code>null</code>.
     */
    public abstract GlossaryMember getPreviousIssue();

    /**
     * Create a copy of this object with the supplied previous issue.
     *
     * @param previousIssue The previous issue. May be <code>null</code>.
     * @return A new object on the basis of this one, with the supplied
     *         previous issue.
     */
    public abstract GlossaryMember previousIssue(GlossaryMember previousIssue);

    /**
     * Get an identifier suitable for the Reason For Update
     * (RFU) field (reasonForUpdateRefIds). Only one RFU ID
     * can be assigned to a glossary member.
     *
     * @return A value suitable for reasonForUpdateRefId.
     * @throws IllegalStateException If this method is called before
     *             this object has been fully initialized
     */
    public final String getRfuId() {
        if (getChangeMark() == null) {
            throw new IllegalStateException("This method must be called after full "
                    + "initialization of " + GlossaryMember.class.getCanonicalName());
        }

        if (shouldGetPreviousIssueRfuId(getChangeMark().getChangeType(), previousIssue)) {
            return previousIssue.getRfuId();
        }

        return String.format(GLOSSARY_MEMBER_RFU_TEMPLATE,
                             getName().toUpperCase()
                                      .replace(' ', '_'),
                             StandardCharsets.UTF_8);
    }

    private static boolean shouldGetPreviousIssueRfuId(ChangeType changeType,
                                                       GlossaryMember previousIssue) {
        return changeType == ChangeType.ADDED && previousIssue != null;
    }

    /**
     * Get an identifier suitable for identifying or referring
     * to a glossary member.
     *
     * @param member The member for which to get the identifier
     * @return The identifier
     */
    public static String getId(GlossaryMember member) {
        return String.format(GLOSSARY_MEMBER_ID_TEMPLATE,
                             URLEncoder.encode(member.getName()
                                                     .replace(' ', '_'),
                                               StandardCharsets.UTF_8));
    }

    @Override
    public final int compareTo(GlossaryMember other) {
        return name.toUpperCase()
                   .compareTo(other.name.toUpperCase());
    }

    /**
     * Get the definition of this glossary member.
     *
     * @return The definition
     */
    @Override
    public final String getDefinition() {
        return definition;
    }

    /**
     * Get the {@link ChangeMark} of the definition of this object, unless this object's
     * global {@link ChangeType} is {@link ChangeType#ADDED} or {@link ChangeType#DELETED}, in which
     * case an empty {@link ChangeMark} is returned.
     *
     * @return The {@link ChangeMark} of the definition this object.
     */
    public final ChangeMark getDefinitionChangeMark() {
        if (notAddedNorDeleted()) {
            return getChanges(definition,
                              previousIssue.definition);
        }

        return new ChangeMark(ChangeType.NONE);
    }

    /**
     * Get the name of this glossary member.
     *
     * @return The name
     */
    @Override
    public final String getName() {
        return name;
    }

    /**
     * Get whether or not this glossary member is a UoF.
     *
     * @return {@code true} if this glossary member is a UoF
     */
    @Override
    public final boolean isUof() {
        return type == ObjectType.UOF;
    }

    /**
     * Get whether or not this glossary member is a class.
     *
     * @return {@code true} if this glossary member is a class
     */
    @Override
    public final boolean isClass() {
        return type == ObjectType.CLASS;
    }

    /**
     * Get whether or not this glossary member is an interface.
     *
     * @return {@code true} if this glossary member is an interface
     */
    @Override
    public final boolean isInterface() {
        return type == ObjectType.EXTEND_INTERFACE || type == ObjectType.SELECT_INTERFACE;
    }

    /**
     * Get whether or not this glossary member is an attribute.
     *
     * @return {@code true} if this glossary member is an attribute
     */
    @Override
    public final boolean isAttribute() {
        return type == ObjectType.ATTRIBUTE;
    }

    /**
     * Get the printable UML/ASD type of this definition
     *
     * @return The type.
     * @throws IllegalStateException Type not set
     */
    public final String getUmlType() {
        if (type == ObjectType.UOF) {
            return TYPEFIELD_UOF;
        } else if (type == ObjectType.CLASS) {
            return TYPEFIELD_CLASS;
        } else if (type == ObjectType.ATTRIBUTE) {
            return TYPEFIELD_ATTRIBUTE;
        } else if (type == ObjectType.SELECT_INTERFACE) {
            return TYPEFIELD_SELECT_INTERFACE;
        } else if (type == ObjectType.EXTEND_INTERFACE) {
            return TYPEFIELD_EXTEND_INTERFACE;
        } else if (type == ObjectType.GENERIC_TERM) {
            return TYPEFIELD_GENERIC_TERM;
        }

        throw new IllegalStateException("Glossary term has no suitable type");
    }

    /**
     * Get this glossary member's notes.
     *
     * @return A list of notes
     */
    @Override
    public final List<String> getNotes() {
        return notesField;
    }

    /**
     * Get the {@link ChangeMark} between object 1 and object 2.
     *
     * @param currentObject Object 1. May be <code>null</code>.
     * @param previousObject Object 2. May be <code>null</code>.
     * @return The {@link ChangeMark}.
     */
    protected final ChangeMark getChanges(Object currentObject,
                                          Object previousObject) {
        if (previousObject == null) {
            return new ChangeMark(ChangeType.ADDED, getRfuId());
        }

        if (currentObject == null) {
            return new ChangeMark(ChangeType.DELETED, getRfuId());
        }

        if (currentObject.equals(previousObject)) {
            return new ChangeMark(ChangeType.NONE);
        }

        return new ChangeMark(ChangeType.MODIFIED, getRfuId());
    }

    /**
     * Get the {@link ChangeMark} of the notes section of this object, unless this object's
     * global {@link ChangeType} is {@link ChangeType#ADDED} or {@link ChangeType#DELETED}, in which
     * case an empty {@link ChangeMark} is returned.
     *
     * @return The {@link ChangeMark} of the notes section of this object.
     */
    public final ChangeMark getNotesChangeMark() {
        if (notAddedNorDeleted()) {
            return getChanges(notesField,
                              previousIssue.notesField);
        }

        return new ChangeMark(ChangeType.NONE);
    }

    /**
     * Get this glossary member's references.
     *
     * @return A set of references
     */
    public final Set<String> getReferences() {
        return referencesField;
    }

    /**
     * Get the {@link ChangeMark} of the references section of this object.
     *
     * @return The {@link ChangeMark}. If no previous issue is provided,
     *         then an empty {@link ChangeMark} is returned.
     */
    public final ChangeMark getReferencesChangeMark() {
        if (notAddedNorDeleted()) {
            return getChanges(referencesField,
                              previousIssue.referencesField);
        }

        return new ChangeMark(ChangeType.NONE);
    }

    /**
     * Returns true if this object's global {@link ChangeType} is
     * neither {@link ChangeType#ADDED} nor {@link ChangeType#DELETED}.
     *
     * @return <code>true</code> if this object's global {@link ChangeType} is
     *         neither {@link ChangeType#ADDED} nor {@link ChangeType#DELETED}.
     *
     */
    protected final boolean notAddedNorDeleted() {
        return previousIssue != null &&
                !Set.of(ChangeType.DELETED, ChangeType.ADDED)
                    .contains(changeMark.getChangeType());
    }

    /**
     * Get this glossary member's examples.
     *
     * @return A list of examples
     */
    @Override
    public final List<String> getExamples() {
        return examplesField;
    }

    /**
     * Get the {@link ChangeMark} of the examples section of this object, unless this object's
     * global {@link ChangeType} is {@link ChangeType#ADDED} or {@link ChangeType#DELETED}, in which
     * case an empty {@link ChangeMark} is returned.
     *
     * @return The {@link ChangeMark} of the examples section of this object.
     */
    public final ChangeMark getExamplesChangeMark() {
        if (notAddedNorDeleted()) {
            return getChanges(Set.copyOf(examplesField),
                              Set.copyOf(previousIssue.examplesField));
        }

        return new ChangeMark(ChangeType.NONE);
    }

    /**
     * Get this glossary member's {@link ChangeMark}.
     *
     * @return The {@link ChangeMark}.
     */
    public final ChangeMark getChangeMark() {
        return changeMark;
    }

    @Override
    public boolean equals(Object object) {
        return super.equals(object);
    }

    @Override
    public abstract int hashCode();

    /**
     * Attempt to get the data module this glossary member belongs to.
     *
     * @return The data module or {@code null}.
     */
    public GlossaryDataModule getDataModule() {
        return dataModule;
    }

    /**
     * Set the data module of this glossary member.
     *
     * @param dataModule The data module
     * @return A new {@code GlossaryMember}.
     */
    public abstract GlossaryMember dataModule(GlossaryDataModule dataModule);

    @Override
    public String toString() {
        return String.format("%s %s [%s]",
                             getClass().getSimpleName(),
                             name,
                             super.toString());
    }

    /**
     * Determine whether this object has any change mark to report.
     * If this object was deleted and replaced by another {@link GlossaryMember}
     * (with the {@link ChangeType#ADDED} {@link ChangeMark}), then there is
     * no {@link ChangeMark} to report
     *
     * @return <code>true</code> if there is at least one change mark
     *         on this object, <code>false</code> otherwise.
     */
    public boolean hasAnyChangeMark() {
        return Stream.of(getChangeMark().getChangeType(),
                         getDefinitionChangeMark().getChangeType(),
                         getExamplesChangeMark().getChangeType(),
                         getNotesChangeMark().getChangeType(),
                         getReferencesChangeMark().getChangeType())
                     .anyMatch(Predicate.not(Set.of(ChangeType.NONE, ChangeType.UNDETERMINED)::contains));
    }
}