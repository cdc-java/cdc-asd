package cdc.asd.specgen.s1000d5;

import java.util.List;

/**
 * An S1000D {@code randomList} element.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class RandomList extends S1000DGenericElementNode {
    /**
     * Create a {@code RandomList} with a title.
     *
     * @param randomListTitle The title.
     */
    public RandomList(String randomListTitle) {
        this();

        this.children =
                concatLists(this.children,
                            List.of(new S1000DGenericElementNode(TITLE).child(new S1000DTextNode(randomListTitle))));
    }

    /**
     * Create a {@code RandomList} without a title.
     */
    public RandomList() {
        super(RANDOMLIST);
    }
}