package cdc.asd.specgen.s1000d5;

import java.util.List;
import java.util.Map;

/**
 * An S1000D brDoc element.
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class BrDoc extends S1000DElementNode {
    private List<BrLevelledPara> para = List.of();

    public List<BrLevelledPara> getBrLevelledParas() {
        return para;
    }

    /**
     * BrDoc constructor.
     */
    public BrDoc() {
        super();
    }

    private BrDoc(BrLevelledPara paras,
                  BrDoc original) {
        this(original);

        this.para = concatLists(this.para, List.of(paras));
    }

    /**
     * Copy constructor.
     *
     * @param original Original object to copy over.
     */
    public BrDoc(BrDoc original) {
        this(original.para, original.changeMark, original);
    }

    private BrDoc(List<BrLevelledPara> paras,
                  ChangeMark changeMark,
                  BrDoc original) {
        super(changeMark, original);
        this.para = concatLists(this.para, paras);
    }

    /**
     * Add a brLevelledPara element to this element.
     *
     * @param para The para to add.
     * @return A new BrDoc element with the supplied
     *         brLevelledPara.
     */
    public BrDoc brLevelledPara(BrLevelledPara para) {
        return new BrDoc(para, this);
    }

    /**
     * Append a list of brLevelledParas to this object.
     *
     * @param paras The list of paras. May be empty.
     * @return A new {@code BrDoc} or this object if the supplied
     *         input is empty.
     */
    public BrDoc brLevelledParas(List<BrLevelledPara> paras) {
        if (paras.isEmpty()) {
            return this;
        }

        return new BrDoc(paras, this.changeMark, this);
    }

    @Override
    public String getElementName() {
        return BRDOC;
    }

    @Override
    public Map<String, String> getAttributes() {
        return super.getAttributes();
    }

    @Override
    public List<S1000DNode> getChildren() {
        return para.parallelStream()
                   .map(S1000DNode.class::cast)
                   .toList();
    }

    @Override
    public BrDoc changeMark(ChangeMark changeMark) {
        if (this.changeMark.equals(changeMark)) {
            return this;
        }

        return new BrDoc(this.para, changeMark, this);
    }
}