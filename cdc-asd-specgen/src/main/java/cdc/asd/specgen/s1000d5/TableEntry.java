package cdc.asd.specgen.s1000d5;

import java.util.List;

/**
 * An S1000D {@value S1000DNode#ENTRY} table element
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class TableEntry extends S1000DGenericElementNode {

    public TableEntry() {
        super(ENTRY);
    }

    private TableEntry(int moreRows,
                       TableEntry original) {
        super(MOREROWS, String.valueOf(moreRows), original);
    }

    /**
     * Set the morerow attribute.
     * 
     * @param value The morerow attribute to set.
     * @return A new {@link TableEntry} with the morerow attribute set
     *         to the supplied value.
     */
    public TableEntry moreRows(int value) {
        return new TableEntry(value, this);
    }

    private TableEntry(S1000DNode child,
                       TableEntry original) {
        super(child, original);
    }

    private TableEntry(List<? extends S1000DNode> children,
                       TableEntry original) {
        super(children, original);
    }

    @Override
    public TableEntry child(S1000DNode child) {
        return new TableEntry(child, this);
    }

    @Override
    public TableEntry child(List<? extends S1000DNode> children) {
        return new TableEntry(children, this);
    }
}
