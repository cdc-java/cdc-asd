package cdc.asd.specgen.s1000d5;

import java.util.List;

/**
 * And S1000D {@value S1000DNode#TBODY} element
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class TableBody extends S1000DGenericElementNode {

    public TableBody() {
        super(TBODY);
    }

    private TableBody(S1000DNode child,
                      TableBody original) {
        super(child, original);
    }

    private TableBody(List<? extends S1000DNode> children,
                      TableBody original) {
        super(children, original);
    }

    @Override
    public TableBody child(S1000DNode child) {
        return new TableBody(child, this);
    }

    @Override
    public TableBody child(List<? extends S1000DNode> children) {
        return new TableBody(children, this);
    }
}
