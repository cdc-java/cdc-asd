package cdc.asd.specgen.s1000d5;

import java.util.Set;

import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;

/**
 * Abstract class for elements in the S1000D XML schema
 * group "normalBrParaElemGroup"
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public abstract class NormalBrParaElem extends S1000DElementNode {

    protected Set<FormattingPolicy> formattingPolicies = Set.of();

    public NormalBrParaElem() {
        super();
    }

    public NormalBrParaElem(ChangeMark changeMark,
                            NormalBrParaElem original) {
        super(changeMark, original);
        this.formattingPolicies = original.formattingPolicies;
        this.id = original.id;
    }

    public NormalBrParaElem(NormalBrParaElem original) {
        this(original.changeMark, original);
    }

    /**
     * An identifier that can be used for future reference
     * within the same S1000D XML document.
     */
    protected String id;

    /**
     * Get the identifier of this element.
     *
     * @return The identifier.
     */
    public final String getId() {
        return id;
    }

    /**
     * This method must return a new object with the {@code id}
     * attribute set.
     *
     * @param id The identifier of that element, for future
     *            reference somewhere else in the S1000D XML document.
     * @return A new object with the supplied identifier.
     */
    public abstract NormalBrParaElem id(String id);

    /**
     * This method must return a new object and apply to it
     * the supplied formatting policies.
     * 
     * @param formattingPolicies The formatting policies to apply
     * @return A new object with the formatting policies specified.
     */
    public abstract NormalBrParaElem formattingPolicies(Set<FormattingPolicy> formattingPolicies);

    @Override
    public abstract NormalBrParaElem changeMark(ChangeMark changeMark);
}