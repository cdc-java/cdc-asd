package cdc.asd.specgen.s1000d5;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An S1000D generic element.
 *
 * This class is meant for plain elements that do not
 * require a specific class constructs, typically low-level
 * formatting elements.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class S1000DGenericElementNode extends S1000DElementNode {

    /**
     * The name of the element.
     */
    private final String name;

    /**
     * The attributes of the element.
     */
    protected Map<String, String> attributes = Map.of();

    /**
     * The list of children of the element. The order
     * will be preserved in the {@code getChildren()}
     * method.
     */
    protected List<S1000DNode> children = List.of();

    /**
     * Generic element constructor.
     *
     * @param elementName The name of the element.
     */
    public S1000DGenericElementNode(String elementName) {
        super();
        this.name = elementName;
    }

    protected S1000DGenericElementNode(S1000DGenericElementNode original) {
        this(original.changeMark, original);
    }

    protected S1000DGenericElementNode(ChangeMark changeMark,
                                       S1000DGenericElementNode original) {
        super(changeMark, original);
        this.name = original.name;
        this.attributes = original.attributes;
        this.children = original.children;
    }

    protected S1000DGenericElementNode(String key,
                                       String value,
                                       S1000DGenericElementNode original) {
        this(original);
        this.attributes = concatMaps(this.attributes, Map.of(key, value == null ? "" : value));
    }

    protected S1000DGenericElementNode(S1000DNode child,
                                       S1000DGenericElementNode original) {
        this(original);
        this.children = concatLists(this.children, List.of(child));
    }

    protected S1000DGenericElementNode(List<? extends S1000DNode> children,
                                       S1000DGenericElementNode original) {
        this(original);
        this.children = concatLists(this.children, children);
    }

    /**
     * Add new attribute named with key and valued with value.
     *
     * @param key The key.
     * @param value The value.
     * @return A new object with added attribute.
     */
    public S1000DGenericElementNode attribute(String key,
                                              String value) {
        return new S1000DGenericElementNode(key, value, this);
    }

    /**
     * Add new child to this node.
     *
     * @param child The child.
     * @return A new object with added child.
     */
    public S1000DGenericElementNode child(S1000DNode child) {
        return new S1000DGenericElementNode(child, this);
    }

    /**
     * Add new child to this node.
     *
     * @param children The child.
     * @return New object with added child in the order
     *         they are provided.
     */
    public S1000DGenericElementNode child(List<? extends S1000DNode> children) {
        return new S1000DGenericElementNode(children, this);
    }

    @Override
    public String getElementName() {
        return name;
    }

    @Override
    public Map<String, String> getAttributes() {
        final Map<String, String> attributes = new HashMap<>(super.getAttributes());

        attributes.putAll(this.attributes);

        return attributes;
    }

    @Override
    public List<S1000DNode> getChildren() {
        return children;
    }

    @Override
    public S1000DGenericElementNode changeMark(ChangeMark changeMark) {
        if (this.changeMark.equals(changeMark)) {
            return this;
        }

        return new S1000DGenericElementNode(changeMark, this);
    }
}