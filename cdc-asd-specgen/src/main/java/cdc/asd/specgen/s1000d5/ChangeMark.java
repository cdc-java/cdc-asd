package cdc.asd.specgen.s1000d5;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * A change mark in S1000D.
 * A change mark can be assigned onto a number of S1000D objects and
 * consists of three S1000D attributes: <code>changeMark</code>,
 * <code>changeType</code> and <code>reasonForUpdateRefIds</code>.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class ChangeMark {
    /**
     * The S1000D "changeType" field that documents the type
     * of change for an object.
     */
    private final ChangeType changeType;

    /**
     * The S1000D "reasonForUpdateRefIds" field that documents the
     * "reasonForUpdate" field.
     */
    private final String reasonForUpdateRefIds;

    /**
     * The S1000D "reasonForUpdate" field.
     */
    private final List<String> reasonForUpdateText;

    /**
     * Create a new {@link ChangeMark} with a reason for update field.
     *
     * @param changeType The S1000D changeType field.
     * @param reasonForUpdateRefIds The S1000D reasonForUpdate field. It must not
     *            be supplied if the {@link ChangeType} is {@link ChangeType#NONE} or
     *            {@link ChangeType#UNDETERMINED}. In such case, it must be <code>null</code>.
     * @throws IllegalArgumentException A reason for update value was supplied, while
     *             the supplied change type was {@link ChangeType#NONE} or {@link ChangeType#UNDETERMINED}.
     */
    public ChangeMark(ChangeType changeType,
                      String reasonForUpdateRefIds) {
        this(changeType, reasonForUpdateRefIds, List.of());
    }

    public ChangeMark(ChangeType changeType,
                      String reasonForUpdateRefIds,
                      List<String> reasonForUpdateText) {
        if ((changeType == ChangeType.NONE || changeType == ChangeType.UNDETERMINED) &&
                reasonForUpdateRefIds != null) {
            throw new IllegalArgumentException("A reasonForUpdateRefIds value was supplied, "
                    + "but ChangeType.NONE or ChangeType.UNDETERMINED was used.");
        }

        this.changeType = changeType;
        this.reasonForUpdateRefIds = reasonForUpdateRefIds;
        this.reasonForUpdateText = Collections.unmodifiableList(reasonForUpdateText);
    }

    private ChangeMark(ChangeType changeType,
                       String reasonForUpdateRefIds,
                       List<String> reasonForUpdateText,
                       ChangeMark original) {
        this(changeType,
             reasonForUpdateRefIds,
             Stream.concat(original.reasonForUpdateText.stream(),
                           reasonForUpdateText.stream())
                   .toList());
    }

    /**
     * Create a new {@link ChangeMark}.
     *
     * @param changeType The S1000D changeType field.
     */
    public ChangeMark(ChangeType changeType) {
        this(changeType, null);
    }

    /**
     * Create an empty {@link ChangeMark} set up with
     * {@link ChangeType#NONE} (effectively referring to no
     * change is to report for the given object).
     */
    public ChangeMark() {
        this(ChangeType.NONE);
    }

    /**
     * Get the {@link ChangeType}.
     *
     * @return The {@link ChangeType}.
     */
    public ChangeType getChangeType() {
        return changeType;
    }

    /**
     * Get the reasonForUpdateRefIds field.
     *
     * @return The reasonForUpdateRefIds field or
     *         <code>null</code>.
     */
    public String getReasonForUpdateRefIds() {
        return reasonForUpdateRefIds;
    }

    public List<String> getReasonForUpdateText() {
        return reasonForUpdateText;
    }

    public ChangeMark changeType(ChangeType changeType) {
        return new ChangeMark(changeType, this.reasonForUpdateRefIds, List.of(), this);
    }

    public ChangeMark reasonForUpdateText(List<String> reasonForUpdateText) {
        return new ChangeMark(this.changeType, this.reasonForUpdateRefIds, reasonForUpdateText, this);
    }

    public ChangeMark reasonForUpdateText(String reasonForUpdateText) {
        return new ChangeMark(this.changeType, this.reasonForUpdateRefIds, List.of(reasonForUpdateText), this);
    }

    public ChangeMark reasonForUpdateRefIds(String reasonForUpdateRefIds) {
        return new ChangeMark(this.changeType, reasonForUpdateRefIds, List.of(), this);
    }

    /**
     * An enum for the type of change, based on S1000D terminology.
     *
     */
    public enum ChangeType {
        /**
         * Content was added.
         */
        ADDED("add", "Added"),
        /**
         * Content was modified.
         */
        MODIFIED("modify", "Modified"),
        /**
         * Content was deleted.
         */
        DELETED("delete", "Deleted"),
        /**
         * Content was deleted but replaced by new content.
         */
        REPLACED("delete", "Deleted"),
        /**
         * No change.
         */
        NONE("none", "Kept"),
        /**
         * Indeterminate.
         */
        UNDETERMINED("undetermined", "Was not able to determine changes to");

        private final String s1000dCode;
        private final String introductorySentence;

        ChangeType(String s1000dCode,
                   String introductorySentence) {
            this.s1000dCode = s1000dCode;
            this.introductorySentence = introductorySentence;
        }

        /**
         * Get an introductory sentence for the change type in the following format:
         * "Added", "Modified", etc.
         *
         * @return The introductory sentence.
         */
        public String getIntroductorySentence() {
            return introductorySentence;
        }

        @Override
        public String toString() {
            return s1000dCode;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(changeType, reasonForUpdateRefIds, reasonForUpdateText);
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj)) {
            return true;
        }

        if (obj instanceof final ChangeMark m) {
            return changeType.equals(m.changeType) &&
                    (Optional.ofNullable(m.reasonForUpdateRefIds)
                             .filter(Predicate.isEqual(reasonForUpdateRefIds))
                             .isPresent()
                            || reasonForUpdateRefIds == m.reasonForUpdateRefIds
                                    && Optional.ofNullable(m.reasonForUpdateText)
                                               .filter(Predicate.isEqual(reasonForUpdateText))
                                               .isPresent());
        }

        return false;
    }

    @Override
    public String toString() {
        return String.format("%s [%s %s]",
                             getClass().getName(),
                             getChangeType(),
                             Optional.ofNullable(getReasonForUpdateRefIds())
                                     .orElse("(no reason for update supplied)"));
    }
}
