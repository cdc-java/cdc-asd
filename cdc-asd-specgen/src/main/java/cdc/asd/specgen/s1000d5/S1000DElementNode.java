package cdc.asd.specgen.s1000d5;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import cdc.asd.specgen.s1000d5.ChangeMark.ChangeType;

/**
 * Top-level class for S1000D assets.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public abstract class S1000DElementNode extends S1000DNode {
    /**
     * The {@link ChangeMark} of the node.
     */
    protected final ChangeMark changeMark;

    protected S1000DElementNode() {
        this.changeMark = new ChangeMark(ChangeType.NONE);
    }

    protected S1000DElementNode(S1000DElementNode original) {
        this(original.changeMark, original);
    }

    protected S1000DElementNode(ChangeMark changeMark,
                                S1000DElementNode original) {
        Objects.requireNonNull(changeMark);
        this.changeMark = changeMark;
    }

    /**
     * This method must return a new object with the specified {@link ChangeMark}.
     *
     * @param changeMark The {@link ChangeMark} to use.
     * @return A new object with the specified change type or this same object
     *         if the supplied argument is equal to the one currently assigned to
     *         this object.
     */
    public abstract S1000DElementNode changeMark(ChangeMark changeMark);

    /**
     * Get this S1000D node's change type
     *
     * @return The change type of this S1000D node.
     */
    public final ChangeMark getChangeMark() {
        return changeMark;
    }

    /**
     * This method must return the name of the element.
     * It must be compatible with XML.
     *
     * @return The element name.
     */
    public abstract String getElementName();

    /**
     * This method must return the list of attributes
     * to assign to the XML element to serialize.
     *
     * @return The entire list of attributes. If none,
     *         then return an empty map.
     */
    public Map<String, String> getAttributes() {
        return getChangeMarksMap();
    }

    /**
     * Get a {@link Map} with the keys {@link S1000DNode#CHANGEMARK},
     * {@link S1000DNode#CHANGETYPE} and {@link S1000DNode#REASONFORUPDATEREFIDS}.
     * 
     * @return A {@link Map} with the above keys or an empty {@link Map}.
     */
    public final Map<String, String> getChangeMarksMap() {
        if (!Set.of(ChangeType.NONE, ChangeType.UNDETERMINED)
                .contains(changeMark.getChangeType())) {
            final HashMap<String, String> changeMarkAttributes =
                    new HashMap<>();

            changeMarkAttributes.put(S1000DNode.CHANGEMARK, "1");
            changeMarkAttributes.put(S1000DNode.CHANGETYPE,
                                     changeMark.getChangeType()
                                               .toString());

            if (changeMark.getReasonForUpdateRefIds() != null) {
                changeMarkAttributes.put(S1000DNode.REASONFORUPDATEREFIDS,
                                         changeMark.getReasonForUpdateRefIds());
            }

            return Collections.unmodifiableMap(changeMarkAttributes);
        }

        return Map.of();
    }

    /**
     * This method must return a list of children
     * which are serializable as well.
     *
     * @return The list of children.
     */
    public abstract List<S1000DNode> getChildren();

    @Override
    public String toString() {

        return String.format("%s %s [%s]",
                             getClass().getSimpleName(),
                             getElementName(),
                             super.toString());
    }

    @Override
    public final int hashCode() {
        return Objects.hash(getElementName(), getChildren(), getAttributes());
    }

    @Override
    public final boolean equals(Object obj) {

        if (obj instanceof final S1000DElementNode other) {

            return getElementName().equals(other.getElementName())
                    && getChildren().equals(other.getChildren())
                    && getAttributes().equals(other.getAttributes());
        }

        return super.equals(obj);
    }
}