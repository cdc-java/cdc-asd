package cdc.asd.specgen.s1000d5;

import java.util.List;

/**
 * An S1000D {@value S1000DNode#ROW} element.
 * 
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public class TableRow extends S1000DGenericElementNode {

    public TableRow() {
        super(ROW);
    }

    private TableRow(S1000DNode child,
                     TableRow original) {
        super(child, original);
    }

    private TableRow(List<? extends S1000DNode> children,
                     TableRow original) {
        super(children, original);
    }

    @Override
    public TableRow child(S1000DNode child) {
        return new TableRow(child, this);
    }

    @Override
    public TableRow child(List<? extends S1000DNode> children) {
        return new TableRow(children, this);
    }
}
