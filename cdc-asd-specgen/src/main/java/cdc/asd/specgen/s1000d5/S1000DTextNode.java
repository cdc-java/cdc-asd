package cdc.asd.specgen.s1000d5;

import java.util.Objects;
import java.util.Set;

/**
 * An S1000D text node.
 * A text node consists of a simple string and may specify a
 * formatting policy to allow post-processing operations to
 * occur.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public final class S1000DTextNode extends S1000DNode {
    /**
     * Formatting policy for text nodes.
     * The order in which the elements are listed is important,
     * as it dictates the sequence in the executions of formatters.
     * Therefore, a formatting policy listed before another one
     * will be executed before that other one.
     *
     * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
     *
     */
    public enum FormattingPolicy {

        /**
         * Applies to a chunk of text.
         * Scan the text node and find any word that
         * matches a class name. Then place that word
         * into verbatimText elements.
         */
        CLASS_VERBATIM,

        /**
         * Applies to a chunk of text.
         * Whenever a class name is present, put "Refer to..."
         * and point to the paragraph that discusses that class,
         * except if this is the same paragraph. Do this only
         * once per class name.
         *
         */
        CLASS_REFERTO,

        /**
         * Applies only to an interface name. Will provide garbled results
         * if used on an entire chunk of text.
         * Put a "Refer to..." link for the provided interface. The link is based
         * on the interface name and whether or not it belongs to the same UoF
         * as that provided in the context.
         */
        INTERFACE_SINGLE_REFERTO,

        /**
         * Applies only to an interface name. Will not yield any result
         * if used on a non-interface. Scan a text, find the first item that
         * matches the name of an interface. Then place a "Refer to..."
         * link at the end of the text.
         */
        INTERFACE_REFERTO_ENDOFLINE,

        /**
         * For a glossary reference. Makes a reference to that term in the glossary,
         * whether it is on the same data module or a different one. Only works on
         * a text node that contains the term only; otherwise it will not yield any
         * result.
         */
        GLOSSARY_TERM_REFERENCE,

        /**
         * Scans text and formats all terms that are in the context's terms map.
         * Format according to term nature (class, attribute, etc.).
         */
        GLOSSARY_KEYWORD
    }

    private Set<FormattingPolicy> formattingPolicies = Set.of();

    private final String content;

    public S1000DTextNode(String content) {
        this.content = content;
    }

    public S1000DTextNode(String content,
                          Set<FormattingPolicy> formattingPolicies) {
        this.content = content;
        this.formattingPolicies = Set.copyOf(formattingPolicies);
    }

    private S1000DTextNode(Set<FormattingPolicy> policies,
                           S1000DTextNode original) {
        this(original.content, concatSets(original.formattingPolicies, policies));

    }

    public String getContent() {
        return content;
    }

    public Set<FormattingPolicy> getFormattingPolicies() {
        return formattingPolicies;
    }

    @Override
    public String toString() {
        return String.format("%s %s [FormattingPolicies: %s] [%s]",
                             getClass().getSimpleName(),
                             content,
                             formattingPolicies.stream()
                                               .map(FormattingPolicy::name)
                                               .reduce(S1000DTextNode::commaJoin)
                                               .orElse("none"),
                             super.toString());
    }

    /**
     * Join two strings with a comma
     *
     * @param s1 The first string.
     * @param s2 The second string.
     * @return Both strings joined with a comma and a whitespace.
     */
    private static String commaJoin(String s1,
                                    String s2) {
        return String.join(", ", s1, s2);
    }

    /**
     * Add formatting policies.
     *
     * @param policies The policies to add. May be empty.
     * @return A new {@code S1000DTextNode} or this object if supplied
     *         input is empty.
     */
    public S1000DTextNode formattingPolicies(Set<FormattingPolicy> policies) {
        if (policies.isEmpty()) {
            return this;
        }
        return new S1000DTextNode(policies, this);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getContent(), getFormattingPolicies());
    }

    @Override
    public boolean equals(Object obj) {

        if (super.equals(obj)) {
            return true;
        }

        if (obj instanceof final S1000DTextNode other) {

            if (getContent().equals(other.getContent()) &&
                    getFormattingPolicies().equals(other.getFormattingPolicies())) {
                return true;
            }
        }

        return false;
    }
}