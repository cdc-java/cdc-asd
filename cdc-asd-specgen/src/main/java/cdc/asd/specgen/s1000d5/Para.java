package cdc.asd.specgen.s1000d5;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;

import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;

/**
 * An S1000D para element.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class Para extends NormalBrParaElem {

    private List<S1000DNode> content;

    /**
     * Para constructor.
     *
     * @param content Content of element.
     */
    public Para(String content) {
        this(new S1000DTextNode(content));
    }

    /**
     * Para constructor
     *
     * @param content Content of element.
     */
    public Para(S1000DNode content) {
        super();

        this.content = List.of(content);
    }

    /**
     * Para constructor.
     *
     * @param content The content of the element.
     * @param formattingPolicies The policy for formatting the text of the paragraph.
     */
    public Para(String content,
                Set<FormattingPolicy> formattingPolicies) {
        super();

        this.content = List.of(new S1000DTextNode(content, formattingPolicies));
        this.formattingPolicies = Set.copyOf(formattingPolicies);
    }

    private Para(String id,
                 Para original) {
        this(original);
    }

    /**
     * Copy constructor.
     *
     * @param original The original object.
     */
    public Para(Para original) {
        this(original.formattingPolicies, original.changeMark, original);
    }

    private Para(S1000DNode node,
                 Para original) {
        this(original);

        this.content = concatLists(this.content, List.of(node));
    }

    private Para(List<S1000DNode> node,
                 Para original) {
        this(original);

        this.content = concatLists(this.content, node);
    }

    private Para(Set<FormattingPolicy> policies,
                 ChangeMark changeMark,
                 Para original) {
        super(changeMark, original);
        this.content = original.content;

        if (!policies.equals(this.formattingPolicies)) {
            this.formattingPolicies = concatSets(this.formattingPolicies, policies);

            final List<S1000DNode> contentToReformat =
                    content.stream().collect(Collectors.toList());

            for (int i = 0; i < contentToReformat.size(); i++) {
                if (contentToReformat.get(i) instanceof final S1000DTextNode textNode) {
                    contentToReformat.set(i, textNode.formattingPolicies(policies));
                }
            }

            this.content = Collections.unmodifiableList(contentToReformat);
        }
    }

    /**
     * Append a new item to this paragraph.
     *
     * @param node The item to append as last element.
     * @return A new {@code Para}.
     */
    public Para content(S1000DNode node) {
        return new Para(node, this);
    }

    /**
     * Append new items to this paragraph.
     *
     * @param node The items to append as last elements,
     *            in the order they are provided.
     * @return A new {@code Para}.
     */
    public Para content(List<S1000DNode> node) {
        return new Para(node, this);
    }

    /**
     * Add new formatting policies. All the text node elements
     * of this paragraph will be re-created with this formatting
     * policy added on top of theirs.
     *
     * @param policies The policies to add. May be empty.
     * @return A new {@code Para} or this object if supplied
     *         input is empty.
     */
    @Override
    public Para formattingPolicies(Set<FormattingPolicy> policies) {
        if (policies.isEmpty()) {
            return this;
        }

        return new Para(policies, this.changeMark, this);
    }

    /**
     * Allows more elegant lambda expressions for setting formatting policies.
     *
     * @param policy The policy to set
     * @return A {@code Function} whose input is a {@code Para}, and the output
     *         is a new {@code Para} with the supplied policy.
     */
    public static UnaryOperator<Para> formatWith(Set<FormattingPolicy> policy) {
        return p -> p.formattingPolicies(policy);
    }

    @Override
    public String getElementName() {
        return PARA;
    }

    @Override
    public Map<String, String> getAttributes() {
        return super.getAttributes();
    }

    @Override
    public List<S1000DNode> getChildren() {
        return content;
    }

    @Override
    public NormalBrParaElem id(String id) {
        return new Para(id, this);
    }

    @Override
    public Para changeMark(ChangeMark changeMark) {
        if (this.changeMark.equals(changeMark)) {
            return this;
        }

        return new Para(this.formattingPolicies, changeMark, this);
    }
}