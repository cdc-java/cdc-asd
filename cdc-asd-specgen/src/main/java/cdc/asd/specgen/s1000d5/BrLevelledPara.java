package cdc.asd.specgen.s1000d5;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;

public class BrLevelledPara extends NormalBrParaElem {
    /**
     * XML ID pattern:
     * <ul>
     * <li>case-sensitive
     * <li>must start with a letter or underscore
     * <li>cannot start with the letters xml (or XML, or Xml, etc)
     * <li>can contain letters, digits, hyphens, underscores, and periods
     * <li>cannot contain spaces
     * </ul>
     */
    private static final Pattern XML_ID_PATTERN = Pattern.compile("^[_A-Za-z][A-Za-z0-9-_\\.]*$");

    /**
     * The title element
     */
    private final Title title;

    /**
     * The contents of the BrLevelledPara
     */
    private List<NormalBrParaElem> contents = List.of();

    /**
     * The paragraph identifier for future reference
     */
    private String paragraphIdentifier;

    /**
     * The level of this paragraph.
     */
    private final int level;

    /**
     * Get the level of this paragraph. The lowest
     * value is 0.
     *
     * @return The level of this paragraph.
     */
    public int getLevel() {
        return level;
    }

    /**
     * Create a new brLevelledPara element.
     *
     * @param level The level of this paragraph
     */
    public BrLevelledPara(int level) {
        this.level = level;
        this.title = null;
    }

    private BrLevelledPara(ChangeMark changeMark,
                           Title title,
                           BrLevelledPara original) {
        super(changeMark, original);
        this.title = title;
        this.paragraphIdentifier = original.paragraphIdentifier;
        this.contents = original.contents;
        this.paragraphIdentifier = original.paragraphIdentifier;
        this.level = original.level;
    }

    private BrLevelledPara(BrLevelledPara original) {
        this(original.changeMark, original.title, original);
    }

    private BrLevelledPara(String title,
                           BrLevelledPara original) {
        this(original.changeMark,
             new Title(List.of(new S1000DTextNode(title))),
             original);
    }

    private BrLevelledPara(NormalBrParaElem para,
                           BrLevelledPara original) {
        this(original);
        this.contents = concatLists(this.contents, List.of(para));
    }

    private BrLevelledPara(Figure figure,
                           BrLevelledPara original) {
        this(original);
        this.contents = concatLists(this.contents, List.of(figure));
    }

    private BrLevelledPara(BrLevelledPara para,
                           BrLevelledPara original) {
        this(original);
        this.contents = concatLists(this.contents, List.of(para));
    }

    private BrLevelledPara(List<? extends NormalBrParaElem> paras,
                           BrLevelledPara original) {
        this(original);
        this.contents = concatLists(this.contents, paras);
    }

    private BrLevelledPara(String identifier,
                           Title title,
                           Set<FormattingPolicy> formattingPolicies,
                           ChangeMark changeMark,
                           BrLevelledPara original) {
        this(changeMark, title, original);
        this.paragraphIdentifier = identifier;

        if (!formattingPolicies.equals(this.formattingPolicies)) {
            this.formattingPolicies = concatSets(this.formattingPolicies,
                                                 formattingPolicies);

            final List<NormalBrParaElem> contentToReformat =
                    contents.stream().collect(Collectors.toList());

            for (int i = 0; i < contents.size(); i++) {
                contentToReformat.set(i,
                                      contentToReformat.get(i)
                                                       .formattingPolicies(formattingPolicies));
            }

            contents = Collections.unmodifiableList(contentToReformat);
        }
    }

    private BrLevelledPara(List<Figure> figures,
                           Title title,
                           BrLevelledPara original) {
        this(original.changeMark, title, original);
        this.contents = concatLists(this.contents, figures);
    }

    private BrLevelledPara(Title title,
                           BrLevelledPara original) {
        this(original.changeMark, title, original);
    }

    public BrLevelledPara title(String title) {
        return new BrLevelledPara(title, this);
    }

    public BrLevelledPara title(Title title) {
        return new BrLevelledPara(title, this);
    }

    /**
     * Add new para element.
     *
     * @param para Para to add.
     * @return New object with added para.
     */
    public BrLevelledPara para(NormalBrParaElem para) {
        return new BrLevelledPara(para, this);
    }

    public BrLevelledPara para(List<NormalBrParaElem> paras) {
        return new BrLevelledPara(paras, this);
    }

    /**
     * Add a figure for this levelled para.
     *
     * @param figure The figure to add.
     * @return New object with the supplied figure.
     */
    public BrLevelledPara figure(Figure figure) {
        return new BrLevelledPara(figure, this);
    }

    /**
     * Add figures for this levelled para, in the order
     * they are provided.
     *
     * @param figure The figures to add.
     * @return New object with the supplied figures.
     */
    public BrLevelledPara figure(List<Figure> figure) {
        return new BrLevelledPara(figure, this.title, this);
    }

    @Override
    public BrLevelledPara id(String identifier) {
        final String trimmedIdentifier = identifier.trim();

        if (XML_ID_PATTERN.matcher(trimmedIdentifier).matches()) {
            return new BrLevelledPara(trimmedIdentifier,
                                      this.title,
                                      this.formattingPolicies,
                                      this.changeMark,
                                      this);
        }

        throw new IllegalArgumentException("'" + identifier + "' is not a valid XML identifier.");
    }

    /**
     * Append a new brLevelledPara element to this one.
     *
     * @param para The brLevelledPara element to add.
     * @return A new object with the appended brLevelledPara element.
     */
    public BrLevelledPara brLevelledPara(BrLevelledPara para) {
        return new BrLevelledPara(para, this);
    }

    /**
     * Append new brLevelledPara elements to this one.
     *
     * @param paras The brLevelledPara elements to add.
     * @return A new object with the appended brLevelledPara elements.
     */
    public BrLevelledPara brLevelledPara(List<BrLevelledPara> paras) {
        return new BrLevelledPara(paras, this);
    }

    /**
     * Get the title element text of this item.
     *
     * @return The title or <code>null</code>.
     */
    public String getTitle() {
        if (title == null) {
            return null;
        }

        return title.getChildren()
                    .parallelStream()
                    .filter(S1000DTextNode.class::isInstance)
                    .map(S1000DTextNode.class::cast)
                    .map(S1000DTextNode::getContent)
                    .collect(Collectors.joining());
    }

    @Override
    public String getElementName() {
        return BRLEVELLEDPARA;
    }

    @Override
    public Map<String, String> getAttributes() {
        if (paragraphIdentifier == null) {
            return super.getAttributes();
        }

        final Map<String, String> attributes =
                new HashMap<>(super.getAttributes());

        attributes.put("id", paragraphIdentifier);

        return Collections.unmodifiableMap(attributes);
    }

    @Override
    public List<S1000DNode> getChildren() {
        if (title == null) {
            return contents.stream()
                           .map(S1000DNode.class::cast)
                           .toList();
        }

        return Stream.concat(List.of(title).stream(),
                             contents.stream())
                     .map(S1000DNode.class::cast)
                     .toList();
    }

    @Override
    public BrLevelledPara formattingPolicies(Set<FormattingPolicy> formattingPolicies) {
        if (formattingPolicies.isEmpty()) {
            return this;
        }

        return new BrLevelledPara(this.paragraphIdentifier, this.title,
                                  formattingPolicies, this.changeMark, this);

    }

    @Override
    public BrLevelledPara changeMark(ChangeMark changeMark) {
        if (this.changeMark.equals(changeMark)) {
            return this;
        }

        return new BrLevelledPara(this.paragraphIdentifier, this.title,
                                  this.formattingPolicies, changeMark, this);
    }
}