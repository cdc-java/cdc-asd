package cdc.asd.specgen.s1000d5;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.asd.specgen.formatter.VerbatimClassFormatter;
import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;
import cdc.io.xml.XmlUtils;
import cdc.io.xml.XmlUtils.Context;
import cdc.io.xml.XmlUtils.EscapingPolicy;

/**
 * An S1000D Note element.
 * Currently, this implementation only allows adding Strings
 * as members of the Note elements. The {@link VerbatimClassFormatter}
 * is applied to the members.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class Note extends NormalBrParaElem {
    private List<S1000DNode> noteParaTexts;
    private final String noteType;

    /**
     * Constructor.
     */
    public Note() {
        this("Note");
    }

    /**
     * Constructor.
     *
     * @param noteType The type of note, e.g. "Example", "Note", etc.
     */
    public Note(String noteType) {
        super();
        noteParaTexts = List.of();
        this.noteType = XmlUtils.escape(noteType,
                                        Context.ATTRIBUTE_DOUBLE_QUOTE,
                                        EscapingPolicy.ESCAPE_ALWAYS);
    }

    /**
     * Copy constructor
     *
     * @param original The original {@code Note}.
     */
    public Note(Note original) {
        this(original.changeMark, original);

    }

    private Note(ChangeMark changeMark,
                 Note original) {
        super(changeMark, original);
        this.noteType = original.noteType;
        this.noteParaTexts = original.noteParaTexts;
    }

    private Note(String id,
                 Note original) {
        this(original);
    }

    private Note(List<? extends S1000DNode> paraText,
                 Set<FormattingPolicy> formattingPolicies,
                 ChangeMark changeMark,
                 Note original) {
        this(changeMark, original);

        if (paraText != this.noteParaTexts) {
            this.noteParaTexts = concatLists(this.noteParaTexts, paraText);
        }

        if (!formattingPolicies.equals(this.formattingPolicies)) {
            final List<S1000DNode> noteParaTextsToReformat =
                    this.noteParaTexts.stream().collect(Collectors.toList());

            for (int i = 0; i < noteParaTexts.size(); i++) {
                if (noteParaTextsToReformat.get(i) instanceof final S1000DTextNode textNode) {
                    noteParaTextsToReformat.set(i, textNode.formattingPolicies(formattingPolicies));
                }
            }

            this.formattingPolicies = Collections.unmodifiableSet(formattingPolicies);
            this.noteParaTexts = Collections.unmodifiableList(noteParaTextsToReformat);
        }
    }

    /**
     * Add notePara elements.
     *
     * @param contents The strings to add as notePara elements.
     * @return A new {@code Note} with appended notePara elements.
     */
    public Note noteParaString(List<String> contents) {
        if (contents.isEmpty()) {
            return this;
        }

        return new Note(contents.stream()
                                .map(S1000DTextNode::new)
                                .toList(),
                        this.formattingPolicies,
                        this.changeMark,
                        this);
    }

    /**
     * Add notePara elements.
     *
     * @param contents The strings to add as notePara elements.
     * @return A new {@code Note} with appended notePara elements.
     */
    public Note noteParaNode(List<S1000DNode> contents) {
        if (contents.isEmpty()) {
            return this;
        }

        return new Note(contents, this.formattingPolicies, this.changeMark, this);
    }

    @Override
    public NormalBrParaElem id(String id) {
        return new Note(id, this);
    }

    @Override
    public String getElementName() {
        return NOTE;
    }

    @Override
    public Map<String, String> getAttributes() {
        final Map<String, String> attributes = new HashMap<>(super.getAttributes());

        attributes.put(NOTETYPE, noteType);

        return Collections.unmodifiableMap(attributes);
    }

    @Override
    public List<S1000DNode> getChildren() {
        return noteParaTexts.stream()
                            .map(Note::notePara)
                            .map(S1000DNode.class::cast)
                            .toList();
    }

    private static S1000DGenericElementNode notePara(S1000DNode para) {
        return new S1000DGenericElementNode(NOTEPARA).child(para);
    }

    @Override
    public Note formattingPolicies(Set<FormattingPolicy> formattingPolicies) {
        if (formattingPolicies.isEmpty() ||
                formattingPolicies.equals(this.formattingPolicies)) {
            return this;
        }

        return new Note(this.noteParaTexts,
                        formattingPolicies,
                        this.changeMark,
                        this);
    }

    @Override
    public Note changeMark(ChangeMark changeMark) {
        if (this.changeMark.equals(changeMark)) {
            return this;
        }

        return new Note(this.noteParaTexts, this.formattingPolicies,
                        changeMark, this);
    }
}