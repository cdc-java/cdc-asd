package cdc.asd.specgen.s1000d5;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Top-level class for all S1000D nodes.
 *
 * All the members of this class are immutable.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public abstract class S1000DNode {
    protected S1000DNode() {
    }

    /*
     * S1000D element names
     */
    public static final String ASSYCODE = "assyCode";
    public static final String BRDOC = "brDoc";
    public static final String BRLEVELLEDPARA = "brLevelledPara";
    public static final String CHANGEMARK = "changeMark";
    public static final String CHANGETYPE = "changeType";
    public static final String CONTENT = "content";
    public static final String DEFINITIONLIST = "definitionList";
    public static final String DEFINITIONLISTITEM = "definitionListItem";
    public static final String DISASSYCODE = "disassyCode";
    public static final String DISASSYCODEVARIANT = "disassyCodeVariant";
    public static final String DMADDRESS = "dmAddress";
    public static final String DMADDRESSITEMS = "dmAddressItems";
    public static final String DMCODE = "dmCode";
    public static final String DMIDENT = "dmIdent";
    public static final String DMODULE = "dmodule";
    public static final String DMREF = "dmRef";
    public static final String DMREFADDRESSITEMS = "dmRefAddressItems";
    public static final String DMREFIDENT = "dmRefIdent";
    public static final String DMSTATUS = "dmStatus";
    public static final String DMTITLE = "dmTitle";
    public static final String ENTRY = "entry";
    public static final String FIGURE = "figure";
    public static final String GRAPHIC = "graphic";
    public static final String ID = "id";
    public static final String IDENTANDSTATUSSECTION = "identAndStatusSection";
    public static final String INFOCODE = "infoCode";
    public static final String INFOCODEVARIANT = "infoCodeVariant";
    public static final String INFOENTITYIDENT = "infoEntityIdent";
    public static final String INFONAME = "infoName";
    public static final String INLINESIGNIFICANTDATA = "inlineSignificantData";
    public static final String INTERNALREF = "internalRef";
    public static final String INTERNALREFID = "internalRefId";
    public static final String INTERNALREFTARGETTYPE = "internalRefTargetType";
    public static final String INTERNALREFTARGETTYPEVALUE = "irtt01";
    public static final String INWORK = "inWork";
    public static final String ITEMLOCATIONCODE = "itemLocationCode";
    public static final String ISSUEDATE = "issueDate";
    public static final String ISSUEDATE_DAY = "day";
    public static final String ISSUEDATE_MONTH = "month";
    public static final String ISSUEDATE_YEAR = "year";
    public static final String ISSUEINFO = "issueInfo";
    public static final String ISSUENUMBER = "issueNumber";
    public static final String ISSUETYPE = "issueType";
    public static final String ISSUETYPE_GLOSSARY_VALUE = "revised";
    public static final String LISTITEM = "listItem";
    public static final String LISTITEMDEFINITION = "listItemDefinition";
    public static final String LISTITEMTERM = "listItemTerm";
    public static final String LOGO = "logo";
    public static final String MODELIDENTCODE = "modelIdentCode";
    public static final String MOREROWS = "morerows";
    public static final String NOTE = "note";
    public static final String NOTEPARA = "notePara";
    public static final String NOTETYPE = "noteType";
    public static final String PARA = "para";
    public static final String PM = "pm";
    public static final String PMADDRESS = "pmAddress";
    public static final String PMADDRESSITEMS = "pmAddressItems";
    public static final String PMCODE = "pmCode";
    public static final String PMENTRY = "pmEntry";
    public static final String PMENTRYTITLE = "pmEntryTitle";
    public static final String PMENTRYTYPE = "pmEntryType";
    public static final String PMIDENT = "pmIdent";
    public static final String PMISSUER = "pmIssuer";
    public static final String PMNUMBER = "pmNumber";
    public static final String PMREF = "pmRef";
    public static final String PMREFIDENT = "pmRefIdent";
    public static final String PMSTATUS = "pmStatus";
    public static final String PMTITLE = "pmTitle";
    public static final String PMVOLUME = "pmVolume";
    public static final String RANDOMLIST = "randomList";
    public static final String REASONFORUPDATE = "reasonForUpdate";
    public static final String REASONFORUPDATEREFIDS = "reasonForUpdateRefIds";
    public static final String REFERREDFRAGMENT = "referredFragment";
    public static final String ROW = "row";
    public static final String SIGNIFICANTPARADATATYPE = "significantParaDataType";
    public static final String SIGNIFICANTPARADATATYPE_VALUE = "psd51";
    public static final String SIMPLEPARA = "simplePara";
    public static final String SUBSUBSYSTEMCODE = "subSubSystemCode";
    public static final String SUBSYSTEMCODE = "subSystemCode";
    public static final String SYMBOL = "symbol";
    public static final String SYSTEMCODE = "systemCode";
    public static final String SYSTEMDIFFCODE = "systemDiffCode";
    public static final String TABLE = "table";
    public static final String TBODY = "tbody";
    public static final String TECHNAME = "techName";
    public static final String TGROUP = "tgroup";
    public static final String TITLE = "title";
    public static final String UPDATEHIGHLIGHT = "updateHighlight";
    public static final String UPDATEREASONTYPE = "updateReasonType";
    public static final String UPDATEREASONTYPE_VALUE = "urt01";
    public static final String VERBATIMSTYLE = "verbatimStyle";
    public static final String VERBATIMSTYLE_ATTRIBUTENAME = "vs13";
    public static final String VERBATIMSTYLE_CLASSNAME = "vs28";
    public static final String VERBATIMTEXT = "verbatimText";

    /**
     * Concat two lists into a single one.
     *
     * @param <T> The type of elements in the lists.
     * @param list1 The first list
     * @param list2 The second list
     * @return A concatenated, unmodifiable list made up from {@code list1}
     *         and {@code list2} (in this order).
     */
    static final <T> List<T> concatLists(List<? extends T> list1,
                                         List<? extends T> list2) {
        return Stream.concat(list1.stream(), list2.stream()).toList();
    }

    /**
     * Concat two maps into a single one.
     *
     * @param <T> The type of elements in the maps.
     * @param map1 The first map
     * @param map2 The second map
     * @return A concatenated, unmodifiable map made up from the supplied
     *         maps.
     */
    static final <T, U> Map<T, U> concatMaps(Map<? extends T, ? extends U> map1,
                                             Map<? extends T, ? extends U> map2) {
        return Stream.concat(map1.entrySet().stream(), map2.entrySet().stream())
                     .collect(Collectors.toUnmodifiableMap(Map.Entry::getKey,
                                                           Map.Entry::getValue));
    }

    /**
     * Concat two sets into a single one.
     *
     * @param <T> The type of elements in the sets.
     * @param set1 The first set
     * @param set2 The second set
     * @return A concatenated, unmodifiable set made up from
     *         the supplied sets.
     */
    static final <T> Set<T> concatSets(Set<? extends T> set1,
                                       Set<? extends T> set2) {
        return Stream.concat(set1.stream(), set2.stream())
                     .collect(Collectors.toUnmodifiableSet());
    }
}