package cdc.asd.specgen.s1000d5;

import java.util.List;

public class DefinitionList extends S1000DGenericElementNode {

    /**
     * Create a {@code DefinitionList} with a title.
     *
     * @param definitionListTitle The title.
     */
    public DefinitionList(String definitionListTitle) {
        this();

        this.children =
                concatLists(this.children,
                            List.of(new S1000DGenericElementNode(TITLE).child(new S1000DTextNode(definitionListTitle))));
    }

    /**
     * Create a {@code RandomList} without a title.
     */
    public DefinitionList() {
        super(DEFINITIONLIST);
    }
}