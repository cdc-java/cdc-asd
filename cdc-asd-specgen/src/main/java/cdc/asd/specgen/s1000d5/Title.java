package cdc.asd.specgen.s1000d5;

import java.util.List;

/**
 * An S1000D {@code title} element.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class Title extends S1000DGenericElementNode {

    /**
     * Create a {@code Title} with its contents.
     *
     * @param titleContents The contents of the title element.
     */
    public Title(List<S1000DNode> titleContents) {
        this();

        this.children = concatLists(this.children, titleContents);
    }

    private Title(ChangeMark changeMark,
                  Title original) {
        super(changeMark, original);
    }

    /**
     * Create a {@code Title} object
     */
    public Title() {
        super(TITLE);
    }

    @Override
    public Title changeMark(ChangeMark changeMark) {
        if (this.changeMark.equals(changeMark)) {
            return this;
        }

        return new Title(changeMark, this);
    }

}