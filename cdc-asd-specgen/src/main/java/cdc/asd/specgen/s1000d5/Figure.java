package cdc.asd.specgen.s1000d5;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;

/**
 * An S1000D figure element.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 *
 */
public class Figure extends NormalBrParaElem {

    private final String title;
    private final String infoEntityIdent;

    /**
     * Create a new {@code Figure} element with supplied
     * {@code title} and {@code infoEntityIdent}.
     *
     * @param title The title of this figure.
     * @param infoEntityIdent The ICN of this figure.
     */
    public Figure(String title,
                  String infoEntityIdent) {
        super();
        this.title = title;
        this.infoEntityIdent = infoEntityIdent;
        this.id = referTo(infoEntityIdent);
    }

    /**
     * Copy constructor
     *
     * @param original The original object.
     */
    public Figure(Figure original) {
        this(original.id, original.formattingPolicies,
             original.changeMark, original);
    }

    private Figure(String id,
                   Set<FormattingPolicy> formattingPolicies,
                   ChangeMark changeMark,
                   Figure original) {
        super(changeMark, original);

        this.title = original.title;
        this.infoEntityIdent = original.infoEntityIdent;
        this.id = id;

        if (!formattingPolicies.equals(this.formattingPolicies)) {
            // No formatting done at this time on Figure elements
            // but if required we might do that in the future

            this.formattingPolicies = concatSets(formattingPolicies,
                                                 this.formattingPolicies);
        }
    }

    @Override
    public String getElementName() {
        return FIGURE;
    }

    @Override
    public Map<String, String> getAttributes() {
        final Map<String, String> attributes = new HashMap<>(super.getAttributes());

        attributes.put("id", id);

        // Specific for figures- Remove the change marks (we are putting
        // them in the title element for better rendering) #62
        attributes.remove(S1000DNode.CHANGEMARK);
        attributes.remove(S1000DNode.CHANGETYPE);
        attributes.remove(S1000DNode.REASONFORUPDATEREFIDS);

        return Collections.unmodifiableMap(attributes);
    }

    @Override
    public List<S1000DNode> getChildren() {
        final Map<String, String> changeMarks = getChangeMarksMap();

        // Specific behavior - put change marks on title not on figure
        // element #62
        S1000DGenericElementNode titleElement =
                new S1000DGenericElementNode(TITLE).child(new S1000DTextNode(this.title));

        if (!changeMarks.isEmpty()) {
            titleElement = titleElement.attribute(S1000DNode.CHANGEMARK, changeMarks.get(S1000DNode.CHANGEMARK))
                                       .attribute(S1000DNode.CHANGETYPE, changeMarks.get(S1000DNode.CHANGETYPE))
                                       .attribute(S1000DNode.REASONFORUPDATEREFIDS,
                                                  changeMarks.get(S1000DNode.REASONFORUPDATEREFIDS));
        }

        return List.of(titleElement,
                       new S1000DGenericElementNode(GRAPHIC).attribute(INFOENTITYIDENT, this.infoEntityIdent));
    }

    /**
     * Return a reference to a {@code Figure}, that is suitable for
     * internalRef calls.
     *
     * @param icnValue The ICN of the {@code Figure}.
     * @return The formatted string suitable for internalRef calls.
     */
    public static String referTo(String icnValue) {
        return "fig-" + (icnValue == null ? "???" : stripIcnIssue(icnValue.toLowerCase()));
    }

    /**
     * Strip the Issue/InWork numbers of an ICN.
     *
     * @param icn The ICN with its IssueNumber/InWorkNumber.
     * @return The ICN without these two elements.
     */
    private static String stripIcnIssue(String icn) {
        return icn.replaceFirst("-\\d{3}-\\d{2}$", "");
    }

    @Override
    public Figure id(String id) {
        return new Figure(referTo(id), this.formattingPolicies, this.changeMark, this);
    }

    @Override
    public Figure formattingPolicies(Set<FormattingPolicy> formattingPolicies) {
        if (formattingPolicies.isEmpty()) {
            return this;
        }

        return new Figure(this.id, formattingPolicies, this.changeMark, this);
    }

    @Override
    public Figure changeMark(ChangeMark changeMark) {
        if (changeMark.equals(this.changeMark)) {
            return this;
        }

        return new Figure(this.id, this.formattingPolicies, changeMark, this);
    }

    /**
     * Returns if a {@link Figure} is the same as this one (but possibly in a different revision,
     * which would not allow it to satisfy the requirements of {@link Object#equals(Object)}).
     *
     * @param other The other figure to compare. May be <code>null</code>.
     * @return <code>true</code> if the supplied {@link Figure} is the same as this one.
     */
    public boolean isSameFigure(Figure other) {
        if (other == null) {
            return false;
        }

        return id.equals(other.id);
    }
}