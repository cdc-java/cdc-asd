package cdc.asd.specgen;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdClass;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.asd.model.wrappers.AsdTag;
import cdc.asd.model.wrappers.AsdType;
import cdc.asd.specgen.datamodules.AttributeDataDictionaryEntry;
import cdc.asd.specgen.datamodules.ClassDataDictionaryEntry;
import cdc.asd.specgen.datamodules.DataDictionaryEntry;
import cdc.asd.specgen.datamodules.InterfaceDataDictionaryEntry;
import cdc.asd.specgen.datamodules.TypeDefinitionEntry;
import cdc.asd.specgen.datamodules.ValidValueDataDictionaryEntry;
import cdc.asd.specgen.formatter.DataDictionaryFormatterContext;
import cdc.asd.specgen.formatter.Formatter;
import cdc.asd.specgen.formatter.FormatterContext;
import cdc.asd.specgen.formatter.FormattingBoundary;
import cdc.asd.specgen.s1000d5.Para;
import cdc.asd.specgen.s1000d5.S1000DElementNode;
import cdc.asd.specgen.s1000d5.S1000DGenericElementNode;
import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;
import cdc.asd.specgen.s1000d5.TableBody;
import cdc.asd.specgen.s1000d5.TableEntry;
import cdc.asd.specgen.s1000d5.TableRow;
import cdc.io.data.Document;
import cdc.io.data.Element;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;

/**
 * Export the data dictionary definitions of an {@link MfModel} to
 * an S1000D table. Currently, only S3000L requires this kind of export.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class S1000DDataDictionaryXmlIo {
    private static final String BRDOC_STATIC_XML = "brdoc_coredefinitions_static.xml";
    private static final String OUTPUT_FILE_NAME = "%s-definitions.xml";
    private static final String INTERNAL_REF_TARGET_TYPE_TABLE = "irtt02";

    /*
     * Table IDs
     */
    private static final String TABLE_CLASSES_AND_INTERFACES = "table-001";
    private static final String TABLE_ATTRIBUTES = "table-002";
    private static final String TABLE_VALID_VALUES = "table-003";
    private static final String TABLE_VALID_VALUE_LIBRARY = "table-004";

    /*
     * Default text
     */
    private static final String TEXT_REFER_TO_LIBRARY = "Refer to %s valid value library, ";
    private static final String TEXT_NOCONTENT = "(None)";

    private S1000DDataDictionaryXmlIo() {
    }

    /**
     * Save into file the MfModel serialized in S1000D files.
     *
     * @param pathToSave The path for saving the
     *            generated files.
     * @param baseName The base name of the
     *            generated files. May be blank.
     * @param model The MfModel to serialize.
     * @throws IOException When an IO error occurs.
     */
    public static void save(File pathToSave,
                            String baseName,
                            MfModel model) throws IOException {
        final MfPackage rootDataModelPackage = EaModelIoUtils.getRootDataModelPackage(model);

        if (rootDataModelPackage == null) {
            throw new IOException("No suitable root package was found");
        }

        final MfPackage dataModelPackage = EaModelIoUtils.getDataModelPackage(rootDataModelPackage);

        if (dataModelPackage == null) {
            throw new IOException("No suitable data model package was found inside "
                    + "of root package " + rootDataModelPackage.getName());
        }

        final Collection<TypeDefinitionEntry> allTypes =
                Stream.concat(dataModelPackage.getAllPackages()
                                              .parallelStream()
                                              .map(MfPackage::getAllClasses)
                                              .flatMap(Collection::stream)
                                              .map(S1000DDataDictionaryXmlIo::createDataDictionaryDefinitionEntry),
                              dataModelPackage.getAllPackages()
                                              .parallelStream()
                                              .map(MfPackage::getAllInterfaces)
                                              .flatMap(Collection::stream)
                                              .map(S1000DDataDictionaryXmlIo::createDataDictionaryDefinitionEntry))
                      .toList();

        final Collection<AttributeDataDictionaryEntry> allProperties =
                dataModelPackage.getAllPackages()
                                .parallelStream()
                                .map(MfPackage::getAllClasses)
                                .flatMap(Collection::stream)
                                .map(MfClass::getProperties)
                                .flatMap(Collection::stream)
                                .map(S1000DDataDictionaryXmlIo::createDataDictionaryDefinitionEntry)
                                .collect(Collectors.groupingBy(AttributeDataDictionaryEntry::getName,
                                                               Collectors.reducing(S1000DDataDictionaryXmlIo::mergeAttributeDefinitions)))
                                .values()
                                .stream()
                                .map(Optional::get)
                                .toList();

        final Collection<AttributeDataDictionaryEntry> allValidValuesProperties =
                dataModelPackage.getAllPackages()
                                .parallelStream()
                                .map(MfPackage::getAllClasses)
                                .flatMap(Collection::stream)
                                .map(MfClass::getProperties)
                                .flatMap(Collection::stream)
                                .map(S1000DDataDictionaryXmlIo::createDataDictionaryDefinitionEntry)
                                .collect(Collectors.groupingBy(AttributeDataDictionaryEntry::getName,
                                                               Collectors.reducing(S1000DDataDictionaryXmlIo::mergeAttributeDefinitions)))
                                .values()
                                .stream()
                                .map(Optional::get)
                                .filter(AttributeDataDictionaryEntry::hasValidValue)
                                .toList();

        final Document document = EaModelIoUtils.loadBrdocFromStaticDocument(BRDOC_STATIC_XML);

        EaModelIoUtils.createDtd(document);

        try (final XmlWriter writer =
                new XmlWriter(new File(pathToSave,
                                       String.format(OUTPUT_FILE_NAME, baseName)))) {
            writer.setIndentString("  ");
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);

            final Map<String, DataDictionaryEntry> termsMap =
                    Stream.of(allTypes, allProperties)
                          .flatMap(Collection::stream)
                          .map(definition -> Map.entry(definition.getName(), definition))
                          .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

            populateAllTypesIntoDocument(document, new DataDictionaryFormatterContext(termsMap, Set.of()), allTypes);
            populateAllPropertiesIntoDocument(document, new DataDictionaryFormatterContext(termsMap, Set.of()), allProperties);
            populateAllValidValuesTagsIntoDocument(document,
                                                   new DataDictionaryFormatterContext(termsMap, Set.of()),
                                                   allValidValuesProperties);

            XmlDataWriter.write(writer, document);

            writer.flush();
        }
    }

    /**
     * Merge two attribute definitions into a single one.
     * The first attribute definition gets the second one's
     * {@link AttributeDataDictionaryEntry#getParentClassesAndUof()}.
     *
     * @param att1 The first attribute definition
     * @param att2 The second attribute definition.
     * @return A new {@link AttributeDataDictionaryEntry}
     */
    private static AttributeDataDictionaryEntry mergeAttributeDefinitions(AttributeDataDictionaryEntry att1,
                                                                          AttributeDataDictionaryEntry att2) {
        return att1.addParents(att2.getParentClassesAndUof());
    }

    /**
     * Create a {@link ClassDataDictionaryEntry} on the basis of
     * an {@link MfClass}.
     *
     * @param oneClass The input {@link MfClass}
     * @return A new {@link ClassDataDictionaryEntry}
     */
    private static ClassDataDictionaryEntry createDataDictionaryDefinitionEntry(MfClass oneClass) {
        return new ClassDataDictionaryEntry(oneClass.getName(), oneClass.wrap(AsdClass.class).getNotes(),
                                            oneClass.getKind(),
                                            oneClass.wrap(AsdClass.class).getStereotype(),
                                            oneClass.getOwningPackage().getName(),
                                            convertTagsToStrings(oneClass.getTags(AsdTagName.NOTE)),
                                            convertTagsToStrings(oneClass.getTags(AsdTagName.EXAMPLE)));
    }

    /**
     * Create an {@link InterfaceDataDictionaryEntry} on the basis of
     * an {@link MfInterface}.
     *
     * @param oneInterface The input {@link MfInterface}
     * @return A new {@link InterfaceDataDictionaryEntry}
     */
    private static InterfaceDataDictionaryEntry createDataDictionaryDefinitionEntry(MfInterface oneInterface) {
        return new InterfaceDataDictionaryEntry(oneInterface.getName(), oneInterface.wrap(AsdType.class).getNotes(),
                                                oneInterface.getKind(),
                                                oneInterface.wrap(AsdType.class).getStereotype(),
                                                oneInterface.getOwningPackage().getName(),
                                                convertTagsToStrings(oneInterface.getTags(AsdTagName.NOTE)),
                                                convertTagsToStrings(oneInterface.getTags(AsdTagName.EXAMPLE)));
    }

    /**
     * Create an {@link AttributeDataDictionaryEntry} on the basis
     * of an {@link MfProperty}
     *
     * @param property The input {@link MfProperty}
     * @return A new {@link AttributeDataDictionaryEntry}
     */
    private static AttributeDataDictionaryEntry createDataDictionaryDefinitionEntry(MfProperty property) {
        return new AttributeDataDictionaryEntry(property.getName(),
                                                property.wrap(AsdProperty.class).getNotes(),
                                                property.getParent().getName(),
                                                property.getType().getName(),
                                                Optional.ofNullable(property.getParent(MfClass.class)
                                                                            .getOwningPackage())
                                                        .map(MfPackage::getName)
                                                        .orElse(TEXT_NOCONTENT),
                                                property.getTag(AsdTagName.VALID_VALUE_LIBRARY)
                                                        .map(MfTag::getValue)
                                                        .orElse(null),
                                                getValidValues(property),
                                                convertTagsToStrings(property.getTags(AsdTagName.NOTE)),
                                                convertTagsToStrings(property.getTags(AsdTagName.EXAMPLE)));
    }

    /**
     * Create a list of {@link ValidValueDataDictionaryEntry} on the basis
     * of an {@link MfProperty}.
     *
     * @param property The input {@link MfProperty}
     * @return A new {@link List} of {@link ValidValueDataDictionaryEntry}
     */
    private static List<ValidValueDataDictionaryEntry> getValidValues(MfProperty property) {

        return property.getTags(AsdTagName.VALID_VALUE)
                       .stream()
                       .map(S1000DDataDictionaryXmlIo::createDataDictionaryDefinitionEntry)
                       .toList();
    }

    /**
     * Create a {@link ValidValueDataDictionaryEntry} on the basis of an
     * {@link MfTag}
     *
     * @param validValueTag The input {@link MfTag}
     * @return A new {@link ValidValueDataDictionaryEntry}
     */
    private static ValidValueDataDictionaryEntry createDataDictionaryDefinitionEntry(MfTag validValueTag) {
        return new ValidValueDataDictionaryEntry(validValueTag.getValue(), validValueTag.wrap(AsdTag.class).getNotes());
    }

    /**
     * Convenience method to map a {@link List} of {@link MfTag}s to
     * a {@link List} of {@link String}s.
     *
     * @param tags The input {@link MfTag}s
     * @return The outpout {@link String}s
     */
    private static List<String> convertTagsToStrings(List<MfTag> tags) {
        return tags.stream().map(MfTag::getValue).toList();
    }

    /**
     * Populate valid-values definitions into a document, using a given
     * {@link DataDictionaryFormatterContext}.
     *
     * @param document The target document. This argument will be modified
     * @param context The formatter context
     * @param definitions The definitions to populate into the document
     * @throws IOException In case of an I/O error
     */
    private static void populateAllValidValuesTagsIntoDocument(Document document,
                                                               DataDictionaryFormatterContext context,
                                                               Collection<AttributeDataDictionaryEntry> definitions) throws IOException {
        final Element parentElement = selectTableElement(document, TABLE_VALID_VALUES);

        populateWithElements(parentElement,
                             createTableBodyForValidValues(definitions),
                             context);
    }

    /**
     * Populate properties definitions into a document, using a given
     * {@link DataDictionaryFormatterContext}.
     *
     * @param document The target document. This argument will be modified
     * @param context The formatter context
     * @param definitions The definitions to populate into the document
     * @throws IOException In case of an I/O error
     */
    private static void populateAllPropertiesIntoDocument(Document document,
                                                          DataDictionaryFormatterContext context,
                                                          Collection<AttributeDataDictionaryEntry> definitions) throws IOException {
        final Element parentElement = selectTableElement(document, TABLE_ATTRIBUTES);

        populateWithElements(parentElement,
                             createTableBodyForAttributes(definitions),
                             context);
    }

    /**
     * Populate classes/interfaces definitions into a document, using a
     * given {@link DataDictionaryFormatterContext}.
     *
     * @param document The target document. This argument will be modified
     * @param context The formatter context
     * @param definitions The definitions to populate into the document
     * @throws IOException In case of an I/O error
     */
    private static void populateAllTypesIntoDocument(Document document,
                                                     DataDictionaryFormatterContext context,
                                                     Collection<TypeDefinitionEntry> definitions) throws IOException {
        final Element parentElement = selectTableElement(document, TABLE_CLASSES_AND_INTERFACES);

        populateWithElements(parentElement,
                             createTableBodyForClassesAndInterfaces(definitions),
                             context);
    }

    /**
     * Create a {@link TableBody} on the basis of a collection of
     * {@link AttributeDataDictionaryEntry} objects.
     *
     * @param definitions The definitions
     * @return A new {@link TableBody}
     */
    private static TableBody createTableBodyForAttributes(Collection<AttributeDataDictionaryEntry> definitions) {
        return new TableBody().child(definitions.stream()
                                                .map(S1000DDataDictionaryXmlIo::createRowsFromProperty)
                                                .flatMap(Collection::stream)
                                                .toList());
    }

    /**
     * Create a {@link TableBody} on the basis of a collection of
     * {@link TypeDefinitionEntry} objects.
     *
     * @param definitions The definitions
     * @return A new {@link TableBody}
     */
    private static TableBody createTableBodyForClassesAndInterfaces(Collection<TypeDefinitionEntry> definitions) {
        return new TableBody().child(definitions.stream().map(S1000DDataDictionaryXmlIo::createRowFromType).toList());

    }

    /**
     * Create a {@link TableBody} on the basis of a collection of
     * {@link AttributeDataDictionaryEntry} objects and
     * extracts the valid-values definitions from the property.
     *
     * @param definitions The definitions
     * @return A new {@link TableBody}
     */
    private static TableBody createTableBodyForValidValues(Collection<AttributeDataDictionaryEntry> definitions) {
        return new TableBody().child(definitions.stream()
                                                .map(S1000DDataDictionaryXmlIo::createValidValueRowsFromProperty)
                                                .flatMap(Collection::stream)
                                                .toList());
    }

    /**
     * Populate an element with child elements based on the provided
     * {@link S1000DElementNode}, using the {@link FormatterContext}.
     * This function recursively descends the {@code S1000DElementNode}
     * elements and attempts to convert them into {@link Element} items.
     *
     * @param parent The parent element to populate.
     * @param s1000DItem The {@code S1000DElementNode} to descend, recursively,
     *            and append at the end of {@code parent}.
     * @param context The formatting context, required essentially for
     *            styling purposes.
     * @throws IOException When an IO error occurs.
     */
    public static void populateWithElements(Element parent,
                                            S1000DElementNode s1000DItem,
                                            DataDictionaryFormatterContext context) throws IOException {
        final Element element = new Element(parent, s1000DItem.getElementName());

        for (final Map.Entry<String, String> attribute : s1000DItem.getAttributes().entrySet()) {
            element.addAttribute(attribute.getKey(), attribute.getValue());
        }

        for (final S1000DNode node : s1000DItem.getChildren()) {
            if (node instanceof final S1000DElementNode elementNode) {
                populateWithElements(element, elementNode, context);
            } else if (node instanceof final S1000DTextNode textNode) {
                final String description = textNode.getContent();

                if (description == null) {
                    continue;
                }

                final List<FormattingBoundary> formattingBoundaries = new ArrayList<>();

                for (final FormattingPolicy policy : textNode.getFormattingPolicies()) {
                    final Formatter formatter = Formatter.createFormatter(policy, context);

                    formattingBoundaries.addAll(formatter.getTextFormattingBoundaries(description));
                }

                Formatter.formatElement(element, description, formattingBoundaries);
            }
        }
    }

    /**
     * Look up the {@code
     *
    <table>
     * } XML element in the supplied document and
     * with the supplied identifier. The element may be positioned anywhere
     * in the document.
     *
     * @param document The document where to look up
     * @param identifier The identifier of the table looked up
     * @return The {@link Element} for this table or <code>null</code>
     */
    private static Element selectTableElement(Document document,
                                              String identifier) {
        return document.getChildren()
                       .parallelStream()
                       .filter(Element.class::isInstance)
                       .map(Element.class::cast)
                       .mapMulti(S1000DDataDictionaryXmlIo::getAllChildren)
                       .filter(Element.named(S1000DNode.TABLE))
                       .filter(Element.hasAttribute("id", identifier))
                       .findFirst()
                       .get()
                       .getElementNamed(S1000DNode.TGROUP);

    }

    /**
     * Get all the children of an {@link Element}, recursively.
     * This function is meant for use within
     * {@link Stream#mapMulti(java.util.function.BiConsumer)} only.
     *
     * @param element The input {@link Element}
     * @param allElements The output mapper with all {@link Element}s
     */
    private static void getAllChildren(Element element,
                                       Consumer<Element> allElements) {
        allElements.accept(element);

        for (final Element e : element.getChildren()
                                      .stream()
                                      .filter(Element.class::isInstance)
                                      .map(Element.class::cast)
                                      .mapMulti(S1000DDataDictionaryXmlIo::getAllChildren)
                                      .toList()) {
            allElements.accept(e);
        }

    }

    /**
     * Create a {@link List} of {@link TableRow}s on the basis of an
     * {@link AttributeDataDictionaryEntry} with one {@link TableRow}
     * per extracted valid-value.
     *
     * @param entry The entry
     * @return The resulting {@link TableRow}s
     */
    private static List<TableRow> createValidValueRowsFromProperty(AttributeDataDictionaryEntry entry) {

        final List<ValidValueDataDictionaryEntry> validValues = entry.getValidValues();
        final int moreRows = validValues.size() - 1;
        final List<TableRow> rows = new ArrayList<>(validValues.size() + 1);

        if (validValues.size() > 0) {
            for (int i = 0; i < validValues.size(); i++) {
                TableEntry entryForAttributeName = new TableEntry();
                final TableEntry entryForValidValue =
                        new TableEntry().child(new Para(EaModelIoUtils.verbatimTextClass(validValues.get(i).getName())));
                final TableEntry entryForValidValueName =
                        new TableEntry().child(new Para(validValues.get(i).getDefinition()));

                if (i == 0) {
                    entryForAttributeName =
                            entryForAttributeName.child(new Para(EaModelIoUtils.verbatimTextAttribute(entry.getName())));

                    if (moreRows > 0) {
                        entryForAttributeName = entryForAttributeName.moreRows(moreRows);
                    }
                }

                rows.add(i,
                         new TableRow().child(List.of(entryForAttributeName,
                                                      entryForValidValue,
                                                      entryForValidValueName)));
            }
        } else if (entry.getValidValueLibraryName() != null) {
            final TableEntry entryForAttributeName =
                    new TableEntry().child(new Para(EaModelIoUtils.verbatimTextAttribute(entry.getName())));
            final TableEntry entryForValidValue = new TableEntry();
            final TableEntry entryForValidValueName =
                    new TableEntry().child(new Para(String.format(TEXT_REFER_TO_LIBRARY,
                                                                  entry.getValidValueLibraryName())).content(createInternalRefToValidValuesLibrary()));
            rows.add(0,
                     new TableRow().child(List.of(entryForAttributeName,
                                                  entryForValidValue,
                                                  entryForValidValueName)));
        } else {
            final TableEntry entryForAttributeName =
                    new TableEntry().child(new Para(EaModelIoUtils.verbatimTextAttribute(entry.getName())));

            return List.of(new TableRow().child(List.of(entryForAttributeName,
                                                        new TableEntry().child(new Para(TEXT_NOCONTENT)),
                                                        new TableEntry())));
        }

        return rows;
    }

    /**
     * Create an {@value S1000DNode#INTERNALREF} element with a static reference
     * to table id {@value #TABLE_VALID_VALUE_LIBRARY}.
     *
     * @return A new {@value S1000DNode#INTERNALREF} element
     */
    private static S1000DGenericElementNode createInternalRefToValidValuesLibrary() {
        return new S1000DGenericElementNode(S1000DNode.INTERNALREF).attribute(S1000DNode.INTERNALREFID,
                                                                              TABLE_VALID_VALUE_LIBRARY)
                                                                   .attribute(S1000DNode.INTERNALREFTARGETTYPE,
                                                                              INTERNAL_REF_TARGET_TYPE_TABLE);
    }

    /**
     * Create {@link TableRow}s on the basis of an
     * {@link AttributeDataDictionaryEntry} with one {@link TableRow} per
     * parent UoF/parent class.
     *
     * @param entry The entry
     * @return The resulting {@link TableRow}s.
     */
    private static List<TableRow> createRowsFromProperty(AttributeDataDictionaryEntry entry) {
        final List<Entry<String, String>> parentClassesAndUof = entry.getParentClassesAndUof()
                                                                     .entrySet()
                                                                     .stream()
                                                                     .toList();

        final int moreRows = parentClassesAndUof.size() - 1;
        final ArrayList<TableRow> rows = new ArrayList<>(parentClassesAndUof.size());

        for (int i = 0; i < parentClassesAndUof.size(); i++) {
            TableEntry entryForAttributeName = new TableEntry();
            TableEntry entryForAttributeType = new TableEntry();
            TableEntry entryForDefinition = new TableEntry();
            final TableEntry entryForClassName =
                    new TableEntry().child(new Para(EaModelIoUtils.verbatimTextClass(parentClassesAndUof.get(i)
                                                                                                        .getKey())));
            final TableEntry entryForUof = new TableEntry().child(new Para(parentClassesAndUof.get(i)
                                                                                              .getValue()));

            if (i == 0) {
                entryForAttributeName =
                        entryForAttributeName.child(new Para(EaModelIoUtils.verbatimTextAttribute(entry.getName())));
                entryForAttributeType = entryForAttributeType.child(new Para(entry.getType()));
                entryForDefinition = entryForDefinition.child(new Para(entry.getDefinition()));

                if (moreRows > 0) {
                    entryForAttributeName = entryForAttributeName.moreRows(moreRows);
                    entryForAttributeType = entryForAttributeType.moreRows(moreRows);
                    entryForDefinition = entryForDefinition.moreRows(moreRows);
                }
            }
            rows.add(new TableRow().child(List.of(entryForAttributeName,
                                                  entryForAttributeType,
                                                  entryForDefinition,
                                                  entryForClassName,
                                                  entryForUof)));
        }

        return rows;
    }

    /**
     * Create a {@link TableRow} on the basis of a {@link TypeDefinitionEntry}.
     *
     * @param entry The entry
     * @return The resulting {@link TableRow}
     */
    private static TableRow createRowFromType(TypeDefinitionEntry entry) {
        final TableEntry entryForClassName = new TableEntry().child(new Para(EaModelIoUtils.verbatimTextClass(entry.getName())));
        final TableEntry entryForType =
                new TableEntry().child(new Para(entry.getType()
                                                     .substring(0, 1)
                                                     .toUpperCase()
                        + entry.getType()
                               .substring(1)));
        final TableEntry entryForStereotype = new TableEntry().child(new Para(entry.getStereotype()));
        final TableEntry entryForDefinition = new TableEntry().child(new Para(entry.getDefinition()));
        final TableEntry entryForUof = new TableEntry().child(new Para(entry.getUof()));

        return new TableRow().child(List.of(entryForClassName,
                                            entryForType,
                                            entryForStereotype,
                                            entryForDefinition,
                                            entryForUof));
    }
}