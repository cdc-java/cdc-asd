/**
 *
 */
package cdc.asd.specgen;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.asd.model.wrappers.AsdTag;
import cdc.asd.specgen.datamodules.DataModules;
import cdc.asd.specgen.datamodules.PublicationModuleEntryForPublicationModule;
import cdc.asd.specgen.datamodules.SimpleDataModule;
import cdc.asd.specgen.datamodules.UofForDataModule;
import cdc.asd.specgen.diffhelpers.ClassOrInterfaceDiffHelper;
import cdc.asd.specgen.diffhelpers.ConnectorDiffHelper;
import cdc.asd.specgen.diffhelpers.ExampleTagDiffHelper;
import cdc.asd.specgen.diffhelpers.IllustrationTagDiffHelper;
import cdc.asd.specgen.diffhelpers.ImplementedInterfaceDiffHelper;
import cdc.asd.specgen.diffhelpers.ImplementingClassDiffHelper;
import cdc.asd.specgen.diffhelpers.NoteTagDiffHelper;
import cdc.asd.specgen.diffhelpers.PackageDiffHelper;
import cdc.asd.specgen.diffhelpers.PropertyDiffHelper;
import cdc.asd.specgen.diffhelpers.SourceTagDiffHelper;
import cdc.asd.specgen.diffhelpers.ValidValueTagDiffHelper;
import cdc.asd.specgen.formatter.DataModelChapterFormatterContext;
import cdc.asd.specgen.formatter.Formatter;
import cdc.asd.specgen.formatter.FormatterContext;
import cdc.asd.specgen.formatter.FormattingBoundary;
import cdc.asd.specgen.s1000d5.BrDoc;
import cdc.asd.specgen.s1000d5.BrLevelledPara;
import cdc.asd.specgen.s1000d5.ChangeMark;
import cdc.asd.specgen.s1000d5.ChangeMark.ChangeType;
import cdc.asd.specgen.s1000d5.DefinitionList;
import cdc.asd.specgen.s1000d5.Figure;
import cdc.asd.specgen.s1000d5.NormalBrParaElem;
import cdc.asd.specgen.s1000d5.Note;
import cdc.asd.specgen.s1000d5.Para;
import cdc.asd.specgen.s1000d5.RandomList;
import cdc.asd.specgen.s1000d5.S1000DElementNode;
import cdc.asd.specgen.s1000d5.S1000DGenericElementNode;
import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;
import cdc.asd.specgen.s1000d5.Title;
import cdc.asd.specgen.tags.Tags;
import cdc.io.data.Document;
import cdc.io.data.Element;
import cdc.io.data.Text;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfCardinality;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfType;

/**
 * Utility for exporting an {@link MfModel} to S1000D by using
 * the brdoc XML schema.
 *
 * @author Félix Mitjans {@literal <f.mitjans@nexter-group.fr>}
 */
public final class S1000DXmlIo {
    private static final String UOF_OVERVIEW_INDEX = "%s - Chapter %d - UoF Index";
    /*
     * To externalize properly (Issue #7)
     * TODO
     */
    private static final String SPECIFICATION_CODE = "SX002D";
    private static final int SPECIFICATION_CHAPTER_NUMBER = 3;
    private static final int SPECIFICATION_CHAPTER_SECTION_SHIFT = 1;
    private static final LocalDate SPECIFICATION_ISSUE_DATE = LocalDate.of(2024, 4, 1);
    private static final String SPECIFICATION_LOGO_ICN = "ICN-B6865-SX002D0001-001-01";

    /*
     * Labels for the data model chapter. Should probably be put in a resource file.
     */
    private static final String UOF_CHAPTER_TEXT = "%s UoF";
    private static final String UOF_DESCRIPTION_SECTION = "Description";
    private static final String UOF_CLASS_DEFINITION_SECTION = "Class definition";
    private static final String UOF_CLASS_DEFINITION_EXAMPLE = "Example";
    private static final String UOF_CLASS_DEFINITION_EXAMPLE_PLURAL = "Examples";
    private static final String UOF_INTERFACE_DEFINITION_MEMBERS_TITLE = "Class members";
    private static final String UOF_ASSOCIATION_DEFINITION_SECTION = "Associations";
    private static final String UOF_INTERFACE_DEFINITION_MEMBERS_SECTION = "This interface includes the following class member";
    private static final String UOF_INTERFACE_DEFINITION_MEMBERS_SECTION_PLURAL =
            "This interface includes the following class members";
    private static final String UOF_ATTRIBUTE_DEFINITION_SECTION = "Attributes";
    private static final String UOF_IMPLEMENTATION_DEFINITION_SECTION = "Implementations";
    private static final String UOF_IMPLEMENTATIONS_SECTION = "This class implements the following <<extend>> interface";
    private static final String UOF_IMPLEMENTATIONS_SECTION_PLURAL = "This class implements the following <<extend>> interfaces";
    private static final String NUMBER_WORD_REPRESENTATION[] = { "zero", "one", "two", "three", "four", "five",
            "six", "seven", "eight", "nine", "ten" };
    private static final String UOF_OVERVIEW_TEXT = "Units of functionality";
    private static final String UOF_OVERVIEW_ASSYCODE = "UNIT";
    private static final String ASSOCIATION_AGGREGATE_INTRO = "An aggregate";
    private static final String ASSOCIATION_DIRECTED_INTRO = "A directed";
    private static final String ASSOCIATION_GENERIC_INTRO_A = "A";
    private static final String ASSOCIATION_GENERIC_INTRO_AN = "An";
    private static final String ASSOCIATION_CARDINALITY_SINGULAR = " association with %s instance of %s";
    private static final String ASSOCIATION_CARDINALITY_PLURAL = " association with %s instances of %s";
    private static final String UOF_ASSOCIATION_SECTION_SINGULAR = "This %s has the following association:";
    private static final String UOF_ASSOCIATION_SECTION_PLURAL = "This %s has the following associations:";
    private static final String VALID_VALUES_CLASS_VALUES_TEXT = "Valid identifier class values";
    private static final String VALID_VALUES_TEXT = "Valid values";
    private static final String SOURCES_TEXT = "Sources";
    private static final String CARDINALITYTEXT_ERROR = "(unable to retrieve cardinality)";
    private static final String CARDINALITYTEXT_VALUERANGE_ONE = "%s or many";
    private static final String CARDINALITYTEXT_INHERITED = "Inherited from %s.";
    private static final String CARDINALITYTEXT_ZERO_MANY = "zero, one or many";
    private static final String CARDINALITYTEXT_VALUEPAIR = "or";
    private static final String CARDINALITYTEXT_VALUERANGE = "to";

    private static final int PARA_LEVEL_FOR_CLASSES_AND_INTERFACES = 3;
    private static final String RFU_PREFIX = "DMEWG-CDM2024-";

    /*
     * Assumptions
     */
    private static final int TYPICAL_TOTAL_UOF_PER_SPEC = 100;

    /*
     * RFU statements
     */
    private static final String RFU_FIGURE_ADD = "Added new figure";
    private static final String RFU_FIGURE_DELETE = "Deleted figure";
    private static final String RFU_FIGURE_MODIFY = "Modified figure";

    private static final String RFU_CLASS_CONNECTOR_SECTION_ADD = "Added first outbound connectors to class";
    private static final String RFU_CLASS_CONNECTOR_SECTION_DELETE = "Deleted all outbound connectors from class";
    private static final String RFU_CLASS_OR_INTERFACE_ADD = "Added new class or interface";
    private static final String RFU_CLASS_OR_INTERFACE_DELETE = "Deleted class or interface";

    private static final String RFU_CLASS_DEFINITION_MODIFY = "Modified definition of class";
    private static final String RFU_CLASS_TITLE_MODIFY = "Modified name of class from %s to %s";

    private static final String RFU_CONNECTOR_MODIFY_CARDINALITY = "Modified cardinality of connector from %s to %s";
    private static final String RFU_CONNECTOR_ADD = "Added connector";
    private static final String RFU_CONNECTOR_DELETE = "Removed connector";

    private static final String RFU_INTERFACE_DEFINITION_MODIFY = "Modified definition of interface";
    private static final String RFU_INTERFACE_TITLE_MODIFY = "Modified name of interface from %s to %s";
    private static final String RFU_INTERFACE_IMPLEMENTER_DELETE = "Removed implementing class from interface";
    private static final String RFU_INTERFACE_IMPLEMENTER_ADD = "Added implementing class to interface";
    private static final String RFU_INTERFACE_CONNECTOR_SECTION_ADD = "Added first outbound connectors to interface";
    private static final String RFU_INTERFACE_CONNECTOR_SECTION_DELETE = "Deleted all outbound connector from interface";

    private static final String RFU_EXAMPLE_ADD = "Added example";
    private static final String RFU_EXAMPLE_DELETE = "Deleted example";
    private static final String RFU_EXAMPLE_MODIFY = "Modified example";
    private static final String RFU_INTERFACE_IMPLEMENTED_ADD = "Added implemented interface to class";
    private static final String RFU_INTERFACE_IMPLEMENTED_DELETE = "Removed implemented interface from class";
    private static final String RFU_INTERFACE_IMPLEMENTED_SECTION_ADD = "Added first implemented interfaces to class";
    private static final String RFU_INTERFACE_IMPLEMENTED_SECTION_DELETE = "Removed all implemented interfaces from class";
    private static final String RFU_NOTE_ADD = "Added note";
    private static final String RFU_NOTE_DELETE = "Deleted note";
    private static final String RFU_NOTE_MODIFY = "Modified note";
    private static final String RFU_PROPERTY_ADD = "Added property";
    private static final String RFU_PROPERTY_CARDINALITY_MODIFY = "Modified cardinality of property from %s to %s";
    private static final String RFU_PROPERTY_DELETE = "Deleted property";
    private static final String RFU_PROPERTY_DEMOTED = "Moved property from generalization to class";

    private static final String RFU_PROPERTY_NAME_MODIFY = "Modified name of property";

    private static final String RFU_PROPERTY_PROMOTED = "Moved property from class to generalization";
    private static final String RFU_PROPERTY_SECTION_ADD = "Added first attributes";
    private static final String RFU_PROPERTY_SECTION_DELETE = "Deleted all attributes";
    private static final String RFU_PROPERTY_VALIDVALUE_ADD = "Added valid values";
    private static final String RFU_PROPERTY_VALIDVALUE_ADD_ITEM = "Added valid value";
    private static final String RFU_PROPERTY_VALIDVALUE_DELETE = "Deleted all valid values";
    private static final String RFU_PROPERTY_VALIDVALUE_DELETE_ITEM = "Deleted valid value";
    private static final String RFU_PROPERTY_VALIDVALUE_MODIFIED_ITEM = "Modified valid-value XML code";
    private static final String RFU_PROPERTY_VALIDVALUE_SOURCE_ADD = "Added source(s) to valid value";
    private static final String RFU_PROPERTY_VALIDVALUE_SOURCE_DELETE = "Deleted all source(s) from valid value";
    private static final String RFU_PROPERTY_VALIDVALUE_SOURCE_MODIFY = "Modified source(s) of valid value";

    private static final String RFU_UOF_DEFINITION_MODIFY = "Modified definition of UoF";

    private S1000DXmlIo() {
    }

    /**
     * Save into file the MfModel serialized in S1000D files.
     *
     * @param pathToSave The path for saving the
     *            generated files.
     * @param baseName The base name of the
     *            generated files. May be blank.
     * @param model The MfModel to serialize.
     * @param previousModel The previous model.
     * @throws IOException When an IO error occurs.
     */
    public static void save(File pathToSave,
                            String baseName,
                            MfModel model,
                            MfModel previousModel) throws IOException {
        final MfPackage rootDataModelPackage = EaModelIoUtils.getRootDataModelPackage(model);

        if (rootDataModelPackage == null) {
            throw new IOException("No suitable root package was found");
        }

        final MfPackage dataModelPackage = EaModelIoUtils.getDataModelPackage(rootDataModelPackage);

        if (dataModelPackage == null) {
            throw new IOException("No suitable data model package was found inside "
                    + "of root package " + rootDataModelPackage.getName());
        }

        final List<MfPackage> uofList = EaModelIoUtils.getUofList(dataModelPackage);

        final Map<MfPackage, String> assyCodeUofList = uofList.stream()
                                                              .collect(Collectors.groupingBy(S1000DXmlIo::toInitials))
                                                              .entrySet()
                                                              .stream()
                                                              .flatMap(S1000DXmlIo::toUniquelyIdentifiedEaPackage)
                                                              .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

        final MfPackage previousDataModelPackage =
                EaModelIoUtils.getDataModelPackage(EaModelIoUtils.getRootDataModelPackage(previousModel));

        final PackageDiffHelper packageDiff =
                new PackageDiffHelper(dataModelPackage, previousDataModelPackage);

        final List<UofForDataModule> uofDms = new ArrayList<>(TYPICAL_TOTAL_UOF_PER_SPEC);

        for (int i = 0; i < uofList.size(); i++) {
            final MfPackage uof = uofList.get(i);

            uofDms.add(new UofForDataModule(SPECIFICATION_LOGO_ICN,
                                            uof,
                                            i + 1 + SPECIFICATION_CHAPTER_SECTION_SHIFT,
                                            SPECIFICATION_CODE,
                                            String.format("%02d", SPECIFICATION_CHAPTER_NUMBER),
                                            assyCodeUofList.get(uofList.get(i)),
                                            SPECIFICATION_ISSUE_DATE,
                                            packageDiff.getAddedElements().contains(uof) ? "000" : "001",
                                            "01",
                                            uofList.get(i).getName()));
        }

        final Map<String, UofForDataModule> classesAndInterfacesWithUof =
                uofDms.stream()
                      .flatMap(uofDm -> uofDm.getClassAndInterfaceNames()
                                             .stream()
                                             .map(clName -> Map.entry(uofDm,
                                                                      clName)))
                      .collect(Collectors.toMap(Map.Entry::getValue,
                                                Map.Entry::getKey));

        final Set<String> allUofNames =
                Stream.concat(uofDms.stream()
                                    .map(UofForDataModule::getInlineUofName),
                              uofDms.stream()
                                    .map(UofForDataModule::getNormalizedUofName))
                      .collect(Collectors.toSet());

        for (final UofForDataModule uof : uofDms) {
            final String dmc = uof.getDataModuleCode();

            final Document document = EaModelIoUtils.loadBrdocFromStaticDocument();

            populateStaticBrdocDocumentAttributes(document, uof);

            final BrDoc brDoc =
                    new BrDoc().brLevelledPara(createBrLevelledParaFromPackage(uof.getPackage(),
                                                                               packageDiff));

            final Map<S1000DElementNode, String> rfuMap = createRfuMap(brDoc);

            populateWithElements(document.getElementNamed(S1000DNode.DMODULE)
                                         .getElementNamed(S1000DNode.CONTENT),
                                 brDoc,
                                 new DataModelChapterFormatterContext(classesAndInterfacesWithUof,
                                                                      allUofNames),
                                 rfuMap);

            exportReasonForUpdateToDocument(document, rfuMap);

            EaModelIoUtils.createDtd(document);

            try (final XmlWriter writer = new XmlWriter(new File(pathToSave,
                                                                 baseName + dmc + ".xml"))) {
                writer.setIndentString("  ");
                writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);

                XmlDataWriter.write(writer, document);

                writer.flush();
            }
        }

        final Map<Character, List<UofForDataModule>> uofLeadingLetterMap =
                uofDms.stream()
                      .collect(Collectors.groupingBy(S1000DXmlIo::uofFirstLetter));

        final SimpleDataModule overviewDm =
                new SimpleDataModule(SPECIFICATION_LOGO_ICN,
                                     SPECIFICATION_CODE,
                                     String.format("%02d", SPECIFICATION_CHAPTER_NUMBER),
                                     UOF_OVERVIEW_ASSYCODE,
                                     "009", SPECIFICATION_ISSUE_DATE,
                                     "000", "01", UOF_OVERVIEW_TEXT);

        final List<PublicationModuleEntryForPublicationModule> uofPms = new ArrayList<>(1 + 'Z' - 'A');

        for (char i = 'A'; i <= 'Z'; i++) {
            if (!uofLeadingLetterMap.containsKey(i)) {
                uofPms.add(createPublicationModule(Map.entry(i, List.of()),
                                                   overviewDm,
                                                   pathToSave,
                                                   baseName));
            } else {
                uofPms.add(createPublicationModule(Map.entry(i, uofLeadingLetterMap.get(i)),
                                                   overviewDm,
                                                   pathToSave,
                                                   baseName));
            }
        }

        createOverarchingPublicationModule(uofPms, overviewDm, pathToSave, baseName);
    }

    /**
     * Export the ReasonForUpdate (RFU) section into a target {@link Document}.
     *
     * @param document The document that will be modified to include the RFU section.
     * @param rfuMap A {@link Map} that relates element nodes with an RFU statement.
     */
    private static void exportReasonForUpdateToDocument(Document document,
                                                        Map<S1000DElementNode, String> rfuMap) {

        final Element dmStatus = document.getElementNamed(S1000DNode.DMODULE)
                                         .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                                         .getElementNamed(S1000DNode.DMSTATUS);

        for (final Element element : createReasonForUpdateSections(rfuMap)) {
            element.setParent(dmStatus);
        }
    }

    /**
     * Create a list of ReasonForUpdate (RFU) {@link Element}s.
     *
     * @param rfuMap The {@link Map} that associates {@link S1000DElementNode}s and RFU identifier.
     * @return A {@link List} of {@link Element}s to append at the end of the dmStatus element
     *         of the S1000D document.
     */
    private static List<Element> createReasonForUpdateSections(Map<S1000DElementNode, String> rfuMap) {
        final List<Element> rfuSections = new ArrayList<>(rfuMap.size());

        for (final Entry<S1000DElementNode, String> rfuEntry : rfuMap.entrySet()) {
            if (!Set.of(ChangeType.NONE, ChangeType.UNDETERMINED)
                    .contains(rfuEntry.getKey().getChangeMark().getChangeType())) {

                final Element rfuSection = new Element(S1000DNode.REASONFORUPDATE);

                rfuSection.addAttribute(S1000DNode.ID, rfuEntry.getValue());
                rfuSection.addAttribute(S1000DNode.UPDATEREASONTYPE, S1000DNode.UPDATEREASONTYPE_VALUE);
                rfuSection.addAttribute(S1000DNode.UPDATEHIGHLIGHT, "1");

                for (final Text rfuText : rfuEntry.getKey()
                                                  .getChangeMark()
                                                  .getReasonForUpdateText()
                                                  .stream()
                                                  .map(Text::new)
                                                  .toList()) {
                    rfuText.setParent(rfuSection.addElement(S1000DNode.SIMPLEPARA)
                                                .setParent(rfuSection));
                }

                rfuSections.add(rfuSection);
            }
        }

        return rfuSections;
    }

    /**
     * Scan an entire {@link BrDoc} and search for any elements with
     * a {@link ChangeMark} and with a ReasonForUpdate (RFU) statement.
     *
     * @param brDocNode The BrDoc to scan and for which to extract all the
     *            RFU identifiers.
     * @return A {@link Map} that relates each {@link S1000DNode} with an RFU
     *         identifier.
     */
    private static Map<S1000DElementNode, String> createRfuMap(BrDoc brDocNode) {

        final List<S1000DElementNode> nodes =
                brDocNode.getChildren()
                         .stream()
                         .filter(S1000DElementNode.class::isInstance)
                         .map(S1000DElementNode.class::cast)
                         .mapMulti(S1000DXmlIo::getGrandChildren)
                         .toList();

        final Map<S1000DElementNode, String> map = new HashMap<>(nodes.size());

        for (int i = 0; i < nodes.size(); i++) {
            final S1000DElementNode node = nodes.get(i);

            if (!Set.of(ChangeType.NONE, ChangeType.UNDETERMINED)
                    .contains(node.getChangeMark().getChangeType())) {
                map.put(nodes.get(i), RFU_PREFIX + String.format("%05d", i));
            }
        }

        return map;
    }

    /**
     * A function suitable only for {@link Stream#mapMulti(java.util.function.BiConsumer)}
     *
     * @param inputNode TODO
     * @param streamInput TODO
     */
    private static void getGrandChildren(S1000DElementNode inputNode,
                                         Consumer<S1000DElementNode> streamInput) {
        streamInput.accept(inputNode);

        for (final S1000DElementNode node : inputNode.getChildren()
                                                     .stream()
                                                     .filter(S1000DElementNode.class::isInstance)
                                                     .map(S1000DElementNode.class::cast)
                                                     .mapMulti(S1000DXmlIo::getGrandChildren)
                                                     .toList()) {
            streamInput.accept(node);
        }
    }

    /**
     * Get this {@code pack}'s name, and return its first two initials.
     *
     * @param pack The input package. It must have a name.
     * @return The initials after "...UoF ".
     * @throw IllegalArgumentException If supplied {@code pack} has no name.
     */
    private static String toInitials(MfPackage pack) {
        if (pack.getName() == null) {
            throw new IllegalArgumentException("toInitials() was called with a "
                    + "package that has no name. Package ID " + pack.getId());
        }

        final char[] initials = { '0', '0' };
        final String[] words = pack.getName()
                                   .trim()
                                   .replaceFirst(DataModules.UOF_PATTERN.pattern(), "$1")
                                   .split(" ", initials.length);

        for (int i = 0; i < initials.length && i < words.length; i++) {
            initials[i] = words[i].charAt(0);
        }

        return String.valueOf(initials);
    }

    /**
     * From an entry associating a {@code String} with a {@code List} of {@code EaPackage}
     * objects, return a {@code Stream} associating each {@code EaPackage} with its
     * {@code String}, concatenated with its index in the supplied {@code List}.
     *
     * @param input The input map entry. None of its members will be modified.
     * @return The stream with the associated (unchanged) {@code EaPackage} and new
     *         {@code String}.
     */
    private static Stream<Entry<MfPackage, String>> toUniquelyIdentifiedEaPackage(Entry<String, List<MfPackage>> input) {
        final MfPackage[] packs = input.getValue()
                                       .toArray(new MfPackage[input.getValue().size()]);
        final Stream.Builder<Entry<MfPackage, String>> stream = Stream.builder();

        for (int i = 0; i < packs.length; i++) {
            stream.add(Map.entry(packs[i],
                                 input.getKey()
                                      .toUpperCase() +
                                         String.format("%02d", i)));
        }

        return stream.build();
    }

    /**
     * Create overarching publication module, along with "Overview" data module.
     *
     * @param pmUofs All the UoFs/PMs to include in this PM.
     * @param overviewDm The overview DM, which is going to be linked from the resulting PM.
     * @param pathToSave Path where overarching publication module will be written on disk.
     * @param baseName The base name of the publication module file name.
     * @throws IOException In case of failure to write onto disk.
     */
    private static void createOverarchingPublicationModule(List<PublicationModuleEntryForPublicationModule> pmUofs,
                                                           SimpleDataModule overviewDm,
                                                           File pathToSave,
                                                           String baseName) throws IOException {

        final String pmTitle = String.format(UOF_OVERVIEW_INDEX, SPECIFICATION_CODE, SPECIFICATION_CHAPTER_NUMBER);
        final PublicationModuleEntryForPublicationModule pm =
                new PublicationModuleEntryForPublicationModule(SPECIFICATION_LOGO_ICN,
                                                               SPECIFICATION_CODE,
                                                               SPECIFICATION_ISSUE_DATE,
                                                               pmTitle,
                                                               0, Integer.valueOf(SPECIFICATION_CHAPTER_NUMBER),
                                                               pmUofs);

        try (final XmlWriter writer = new XmlWriter(new File(pathToSave,
                                                             baseName + overviewDm.getDataModuleCode() + ".xml"))) {
            final Document document = EaModelIoUtils.loadBrdocFromStaticDocument();

            writer.setIndentString("  ");
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);

            populateStaticBrdocDocumentAttributes(document, overviewDm);
            EaModelIoUtils.createDtd(document);

            populateStaticOverviewDm(document);

            XmlDataWriter.write(writer, document);

            writer.flush();
        }

        final String pmc = pm.getPublicationModuleCode();

        try (final XmlWriter writer = new XmlWriter(new File(pathToSave,
                                                             baseName + pmc + ".xml"))) {
            final Document document = EaModelIoUtils.loadPmFromStaticDocument();

            writer.setIndentString("  ");
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);

            EaModelIoUtils.populateStaticPmDocumentAttributes(document, pm);
            populateStaticPmDocumentOverviewDm(document, overviewDm);

            for (final PublicationModuleEntryForPublicationModule pmUof : pmUofs) {
                createPmEntries(document, pmUof);
            }

            EaModelIoUtils.createDtd(document);

            XmlDataWriter.write(writer, document);

            writer.flush();
        }
    }

    /**
     * Populate the supplied document with static values for
     * the overview data module.
     *
     * @param document The document corresponding to the overview
     *            data module.
     */
    private static void populateStaticOverviewDm(Document document) {
        final Element contentNode =
                document.getElementNamed(S1000DNode.DMODULE)
                        .getElementNamed(S1000DNode.CONTENT);

        final Element topBrLevelledPara =
                contentNode.addElement(S1000DNode.BRDOC)
                           .addElement(S1000DNode.BRLEVELLEDPARA);

        topBrLevelledPara.addElement(S1000DNode.TITLE)
                         .addText(UOF_OVERVIEW_TEXT);
        topBrLevelledPara.addElement(S1000DNode.PARA)
                         .addText("This is an overview Data module to complete by hand.");

    }

    /**
     * Create a publication module overarching data modules and saves
     * it as a file onto the disk.
     *
     * @param uofDmGroup The data modules inside of this new publication module.
     * @param overviewDm The overview DM, which is going to be linked from the resulting PM.
     * @param pathToSave Path to folder where the publication module will be
     *            saved as a file.
     * @param baseName The base name. May be blank (""). Must not be null.
     * @return A new {@code PublicationModuleEntryForPublicationModule}
     */
    private static PublicationModuleEntryForPublicationModule
            createPublicationModule(Entry<Character, List<UofForDataModule>> uofDmGroup,
                                    SimpleDataModule overviewDm,
                                    File pathToSave,
                                    String baseName) {
        final String pmTitle =
                String.format(UOF_OVERVIEW_INDEX + " - %s",
                              SPECIFICATION_CODE,
                              SPECIFICATION_CHAPTER_NUMBER,
                              uofDmGroup.getKey());

        final PublicationModuleEntryForPublicationModule pm =
                new PublicationModuleEntryForPublicationModule(SPECIFICATION_LOGO_ICN,
                                                               SPECIFICATION_CODE,
                                                               SPECIFICATION_ISSUE_DATE,
                                                               pmTitle,
                                                               Integer.valueOf(uofDmGroup.getKey()
                                                                                         .charValue()
                                                                       - 64),
                                                               Integer.valueOf(SPECIFICATION_CHAPTER_NUMBER),
                                                               uofDmGroup.getValue());

        final String pmc = pm.getPublicationModuleCode();

        try (final XmlWriter writer =
                new XmlWriter(new File(pathToSave, baseName + pmc + ".xml"))) {

            writer.setIndentString("  ");
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);

            final Document document = EaModelIoUtils.loadPmFromStaticDocument();

            EaModelIoUtils.populateStaticPmDocumentAttributes(document, pm);
            populateStaticPmDocumentOverviewDm(document, overviewDm);

            for (final UofForDataModule uof : uofDmGroup.getValue()) {
                createPmEntries(document, uof);
            }

            EaModelIoUtils.createDtd(document);

            XmlDataWriter.write(writer, document);

            writer.flush();
        } catch (final IOException e) {
            // TODO log error, maybe skip item?
            return null;
        }

        return pm;
    }

    private static void createPmEntries(Document document,
                                        UofForDataModule uof) {
        final Element pmEntry =
                document.getElementNamed(S1000DNode.PM)
                        .getElementNamed(S1000DNode.CONTENT)
                        .getElementNamed(S1000DNode.PMENTRY)
                        .getElementNamed(S1000DNode.PMENTRY)
                        .addElement(S1000DNode.PMENTRY);

        pmEntry.addElement(S1000DNode.PMENTRYTITLE)
               .addText(String.format("Chapter %d.%d - %s Unit of Functionality - %s",
                                      Integer.valueOf(uof.getSystemCode()),
                                      uof.getPosition(),
                                      uof.getModelIdentCode(),
                                      uof.getSimpleUofName()));

        pmEntry.addChild(DataModules.createDmRef(uof));
    }

    private static void createPmEntries(Document document,
                                        PublicationModuleEntryForPublicationModule pm) {
        final Element pmEntry =
                document.getElementNamed(S1000DNode.PM)
                        .getElementNamed(S1000DNode.CONTENT)
                        .getElementNamed(S1000DNode.PMENTRY)
                        .getElementNamed(S1000DNode.PMENTRY)
                        .addElement(S1000DNode.PMENTRY);

        pmEntry.addElement(S1000DNode.PMENTRYTITLE)
               .addText(pm.getPmTitle());

        pmEntry.addChild(DataModules.createPmRef(pm));
    }

    /**
     * Return the first letter for this UoF (without the
     * chatter in prefix).
     *
     * @param uofDm The data module for this UoF.
     * @return The first letter.
     */
    private static char uofFirstLetter(UofForDataModule uofDm) {
        return uofDm.getSimpleUofName().charAt(0);
    }

    /**
     * Populate an element with child elements based on the provided
     * {@link S1000DElementNode}, using the {@link FormatterContext}.
     * This function recursively descends the {@code S1000DElementNode}
     * elements and attempts to convert them into {@link Element} items.
     *
     * @param parent The parent element to populate.
     * @param s1000DItem The {@code S1000DElementNode} to descend, recursively,
     *            and append at the end of {@code parent}.
     * @param context The formatting context, required essentially for
     *            styling purposes.
     * @param rfuMap A map that relates the {@link S1000DElementNode}s with
     *            ReasonForUpdate (RFU) identifiers. They are pre-processed into
     *            the nodes right before being converted to {@link Element}s.
     * @throws IOException When an IO error occurs.
     */
    public static void populateWithElements(Element parent,
                                            S1000DElementNode s1000DItem,
                                            DataModelChapterFormatterContext context,
                                            Map<S1000DElementNode, String> rfuMap) throws IOException {
        final Element element = new Element(parent, s1000DItem.getElementName());

        for (final Map.Entry<String, String> attribute : s1000DItem.getAttributes().entrySet()) {
            element.addAttribute(attribute.getKey(), attribute.getValue());
        }

        for (final S1000DNode node : s1000DItem.getChildren()) {
            if (node instanceof S1000DElementNode elementNode) {
                if (node instanceof final BrLevelledPara brLevelledPara) {
                    if (brLevelledPara.getLevel() == PARA_LEVEL_FOR_CLASSES_AND_INTERFACES) {
                        context = context.currentClass(brLevelledPara.getTitle());
                    }

                    if (brLevelledPara.getLevel() == PARA_LEVEL_FOR_CLASSES_AND_INTERFACES - 2) {
                        context = context.currentUof(brLevelledPara.getTitle());
                    }
                }

                if (!Set.of(ChangeType.NONE, ChangeType.UNDETERMINED)
                        .contains(elementNode.getChangeMark().getChangeType())) {
                    final String rfuId = Optional.ofNullable(rfuMap.get(elementNode))
                                                 .orElse("%missing%");

                    elementNode = elementNode.changeMark(elementNode.getChangeMark().reasonForUpdateRefIds(rfuId));
                }

                populateWithElements(element,
                                     elementNode,
                                     context,
                                     rfuMap);
            } else {
                if (node instanceof final S1000DTextNode textNode) {
                    final String description = textNode.getContent();
                    final List<FormattingBoundary> formattingBoundaries = new ArrayList<>();

                    if (description == null) {
                        continue;
                    }

                    for (final FormattingPolicy policy : textNode.getFormattingPolicies()) {
                        final Formatter formatter = Formatter.createFormatter(policy, context);

                        formattingBoundaries.addAll(formatter.getTextFormattingBoundaries(description));
                    }

                    Formatter.formatElement(element, description, formattingBoundaries);
                }
            }
        }
    }

    private static BrLevelledPara createBrLevelledParaFromPackage(MfPackage uof,
                                                                  PackageDiffHelper diff) {
        final IllustrationTagDiffHelper illustrationDiff =
                new IllustrationTagDiffHelper(uof, diff.getMap().get(uof));

        final List<Figure> figures = createFigures(illustrationDiff);

        final List<String> referTos = illustrationDiff.getNetElements()
                                                      .stream()
                                                      .map(x -> x.wrap(AsdElement.class).getNotes())
                                                      .map(Figure::referTo)
                                                      .toList();

        final ClassOrInterfaceDiffHelper classOrInterfaceDiff =
                new ClassOrInterfaceDiffHelper(uof, diff.getMap()
                                                        .get(uof));

        final List<BrLevelledPara> classAndInterfacesList =
                Stream.concat(classOrInterfaceDiff.getNetElements()
                                                  .stream()
                                                  .filter(MfClass.class::isInstance)
                                                  .map(MfClass.class::cast),
                              classOrInterfaceDiff.getNetElements()
                                                  .stream()
                                                  .filter(MfInterface.class::isInstance)
                                                  .map(MfInterface.class::cast))
                      .map(computeChangeMark(classOrInterfaceDiff))
                      .collect(Collectors.toList());

        final BrLevelledPara brLevelledPara =
                new BrLevelledPara(1).title(uof.getName())
                                     .id(EaModelIoUtils.getXmlParaIdFromUofPackage(uof))
                                     .brLevelledPara(new BrLevelledPara(2).title(UOF_DESCRIPTION_SECTION)
                                                                          .para(uofNotesToS1000D(uof, diff))
                                                                          .para(uofLongDescriptionToS1000D(uof, referTos))
                                                                          .figure(figures)
                                                                          .formattingPolicies(Set.of(FormattingPolicy.CLASS_VERBATIM)))
                                     .brLevelledPara(new BrLevelledPara(2).title(UOF_CLASS_DEFINITION_SECTION)
                                                                          .brLevelledPara(classAndInterfacesList));
        return brLevelledPara;
    }

    private static Function<MfType, BrLevelledPara> computeChangeMark(ClassOrInterfaceDiffHelper classesAndInterfaceDiff) {
        return classOrInterface -> {
            BrLevelledPara classOrInterfacePara =
                    createBrLevelledParaFromClassOrInterface(classOrInterface,
                                                             classesAndInterfaceDiff).changeMark(new ChangeMark());

            if (classesAndInterfaceDiff.getDeletedElements().contains(classOrInterface)) {
                classOrInterfacePara =
                        classOrInterfacePara.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_CLASS_OR_INTERFACE_DELETE));
            } else if (classesAndInterfaceDiff.getAddedElements().contains(classOrInterface)) {
                classOrInterfacePara =
                        classOrInterfacePara.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_CLASS_OR_INTERFACE_ADD));
            }
            return classOrInterfacePara;
        };
    }

    /**
     * Create a list of {@link Figure}s from a list of {@link MfTag} wrapped in an
     * {@link IllustrationTagDiffHelper}.
     *
     * @param illustrationsDiff The diffset of {@link MfTag}s for which to create {@link Figure}s.
     * @return A list of {@link Figure}s.
     */
    private static List<Figure> createFigures(IllustrationTagDiffHelper illustrationsDiff) {

        return illustrationsDiff.getNetElements()
                                .stream()
                                .map(computeChangeMark(illustrationsDiff))
                                .toList();
    }

    private static Function<MfTag, Figure> computeChangeMark(IllustrationTagDiffHelper diff) {
        return tag -> {
            final Figure figure = EaModelIoUtils.createFigureFromTag(tag).changeMark(new ChangeMark());

            if (diff.onlyAddedElements() || diff.onlyDeletedElements()) {
                return figure;
            }

            if (diff.getAddedElements().contains(tag)) {
                return figure.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_FIGURE_ADD));
            } else if (diff.getSameElements().contains(tag)) {
                if (!Objects.equals(diff.getMap()
                                        .get(tag)
                                        .getValue(),
                                    tag.getValue())) {
                    return figure.changeMark(new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_FIGURE_MODIFY));
                }
            } else if (diff.getDeletedElements().contains(tag)) {
                return figure.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_FIGURE_DELETE));
            }

            return figure;
        };

    }

    private static NormalBrParaElem uofNotesToS1000D(MfPackage uof,
                                                     PackageDiffHelper packageDiff) {
        if (uof.wrap(AsdElement.class).getNotes() == null) {
            // TODO log error - we should have some content here. If we do
            // not, most likely the text was skipped by the EapDumper.
            return new Para("");
        }

        final Optional<String> previousDefinition =
                Optional.ofNullable(packageDiff.getMap().get(uof))
                        .map(p -> p.wrap(AsdElement.class))
                        .map(AsdElement::getNotes);

        Para introParagraph = new Para(uof.wrap(AsdElement.class).getNotes()).changeMark(new ChangeMark());

        if (previousDefinition.isPresent()) {
            if (!Objects.equals(previousDefinition.get(),
                                uof.wrap(AsdElement.class).getNotes())) {
                introParagraph =
                        introParagraph.changeMark(new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_UOF_DEFINITION_MODIFY));
            }
        }

        return introParagraph;
    }

    private static List<NormalBrParaElem> uofLongDescriptionToS1000D(MfPackage uof,
                                                                     List<String> referTos) {
        if (uof.getTag(AsdTagName.LONG_DESCRIPTION).isEmpty()) {
            // no long description. Return an empty para.
            return List.of();
        }

        return new Tags(uof.getTag(AsdTagName.LONG_DESCRIPTION)
                           .get()
                           .wrap(AsdTag.class)
                           .getNotes(),
                        referTos,
                        Set.of(FormattingPolicy.CLASS_VERBATIM, FormattingPolicy.CLASS_REFERTO)).getS1000DNodes();

    }

    /**
     * Create a brLevelledPara on either an {@link MfType}, which must correspond
     * to a class or an interface.
     *
     * @param mfClassOrInterface Either an {@link MfClass} or an {@link MfInterface},
     *            to transform into a {@link BrLevelledPara}.
     * @param classOrInterfaceDiff The helper object that registers all the changes to report
     *            (if any)
     * @return The resulting {@link BrLevelledPara}.
     * @implNote Since the paragraph structure is different for classes and
     *           for interfaces, this function calls either {@code
     * createBrLevelledParaFromClass} or {@code
     * createBrLevelledParaFromInterface} depending on the input. These two
     *           functions will then actually create the {@code BrLevelledPara} structure.
     * @throws IllegalArgumentException Neither an {@link MfClass} nor an {@link MfInterface}
     *             was supplied as input.
     */
    private static BrLevelledPara createBrLevelledParaFromClassOrInterface(MfType mfClassOrInterface,
                                                                           ClassOrInterfaceDiffHelper classOrInterfaceDiff) {

        if (mfClassOrInterface instanceof MfClass) {
            return createBrLevelledParaFromClass(MfClass.class.cast(mfClassOrInterface), classOrInterfaceDiff);
        }

        if (mfClassOrInterface instanceof MfInterface) {
            return createBrLevelledParaFromInterface(MfInterface.class.cast(mfClassOrInterface),
                                                     classOrInterfaceDiff);
        }

        throw new IllegalArgumentException("Attempted to create a <brLevelledPara> element for something that"
                + " is neither a class or an interface: " + mfClassOrInterface.getName());
    }

    /**
     * Create a {@link BrLevelledPara} from an interface.
     *
     * @param mfInterface The interface.
     * @param interfaceDiff The helper object that registers all the changes to report (if any)
     * @return The resulting {@link BrLevelledPara}.
     */
    private static BrLevelledPara createBrLevelledParaFromInterface(MfInterface mfInterface,
                                                                    ClassOrInterfaceDiffHelper interfaceDiff) {
        final Set<MfClass> implementingClasses = mfInterface.getDirectImplementations(MfClass.class);
        final ConnectorDiffHelper connectorDiff =
                new ConnectorDiffHelper(mfInterface, interfaceDiff.getMap()
                                                                  .get(mfInterface));
        final ImplementingClassDiffHelper implementerDiff =
                new ImplementingClassDiffHelper(mfInterface, (MfInterface) interfaceDiff.getMap().get(mfInterface));

        final ChangeMark titleChangeMark = getTitleChangeMark(mfInterface, interfaceDiff);
        final ChangeMark definitionChangeMark = getDefinitionChangeMark(mfInterface, interfaceDiff);

        BrLevelledPara brLevelledPara =
                new BrLevelledPara(PARA_LEVEL_FOR_CLASSES_AND_INTERFACES).id(EaModelIoUtils.getXmlParaIdFromInterface(mfInterface))
                                                                         .title(new Title(List.of(new S1000DTextNode(mfInterface.getName()))).changeMark(titleChangeMark))
                                                                         .para(new Para(mfInterface.wrap(AsdElement.class)
                                                                                                   .getNotes()).changeMark(definitionChangeMark));

        if (!implementerDiff.getNetElements().isEmpty()) {

            final S1000DNode classMemberList =
                    new RandomList().child(implementerDiff.getNetElements()
                                                          .stream()
                                                          .sorted(Comparator.comparing(MfClass::getName))
                                                          .map(computeChangeMark(implementerDiff))
                                                          .toList());

            final Para classMemberNode =
                    new Para(plural(implementingClasses.size(),
                                    UOF_INTERFACE_DEFINITION_MEMBERS_SECTION,
                                    UOF_INTERFACE_DEFINITION_MEMBERS_SECTION_PLURAL)
                            + ":").content(classMemberList);

            brLevelledPara =
                    brLevelledPara.brLevelledPara(new BrLevelledPara(4).title(UOF_INTERFACE_DEFINITION_MEMBERS_TITLE)
                                                                       .para(classMemberNode));

        }

        if (!connectorDiff.getNetElements().isEmpty()) {
            final BrLevelledPara associationsSection = new BrLevelledPara(4).title(UOF_ASSOCIATION_DEFINITION_SECTION)
                                                                            .para(createParaForAssociationSection(mfInterface,
                                                                                                                  connectorDiff));

            if (connectorDiff.onlyAddedElements()) {
                associationsSection.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_INTERFACE_CONNECTOR_SECTION_ADD));
            } else if (connectorDiff.onlyDeletedElements()) {
                associationsSection.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_INTERFACE_CONNECTOR_SECTION_DELETE));
            }
        }

        return brLevelledPara;
    }

    private static ChangeMark getDefinitionChangeMark(MfInterface mfInterface,
                                                      ClassOrInterfaceDiffHelper interfaceDiff) {
        if (Objects.equals(mfInterface.wrap(AsdElement.class).getNotes(),
                           Optional.ofNullable(interfaceDiff.getMap().get(mfInterface))
                                   .map(t -> t.wrap(AsdElement.class))
                                   .map(AsdElement::getNotes)
                                   .orElse(mfInterface.wrap(AsdElement.class).getNotes()))) {
            return new ChangeMark();
        }

        return new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_INTERFACE_DEFINITION_MODIFY);

    }

    private static ChangeMark getTitleChangeMark(MfInterface mfInterface,
                                                 ClassOrInterfaceDiffHelper interfaceDiff) {
        if (Objects.equals(mfInterface.getName(),
                           Optional.ofNullable(interfaceDiff.getMap().get(mfInterface))
                                   .map(MfType::getName)
                                   .orElse(mfInterface.getName()))) {
            return new ChangeMark();
        }

        return new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_INTERFACE_TITLE_MODIFY);
    }

    private static Function<MfClass, S1000DGenericElementNode> computeChangeMark(ImplementingClassDiffHelper implementerDiff) {
        return currentClass -> {
            S1000DGenericElementNode outputNode = formatInterfaceMember(currentClass.getName());

            if (implementerDiff.getDeletedElements().contains(currentClass)) {
                outputNode =
                        outputNode.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_INTERFACE_IMPLEMENTER_DELETE));
            } else if (implementerDiff.getAddedElements().contains(currentClass)) {
                outputNode =
                        outputNode.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_INTERFACE_IMPLEMENTER_ADD));
            } else {
                outputNode = outputNode.changeMark(new ChangeMark());
            }

            return outputNode;
        };
    }

    /**
     * Format interface member name, by placing it into a verbatimText
     * element, itself placed inside a listItem element.
     *
     * @param eaClassName The interface member name.
     * @return The resulting, formatted, {@link S1000DNode}.
     */
    private static S1000DGenericElementNode formatInterfaceMember(String eaClassName) {
        return new S1000DGenericElementNode(S1000DNode.LISTITEM).child(new Para(EaModelIoUtils.verbatimTextClass(eaClassName)));
    }

    private static List<S1000DGenericElementNode> getExamples(ExampleTagDiffHelper exampleDiff) {
        return exampleDiff.getNetElements()
                          .stream()
                          .map(computeExampleChangeMark(exampleDiff))
                          .toList();
    }

    private static Function<MfTag, S1000DGenericElementNode> computeExampleChangeMark(ExampleTagDiffHelper exampleDiff) {
        return current -> {
            final S1000DGenericElementNode node =
                    EaModelIoUtils.listItem(new Para(current.getValue()));

            if (exampleDiff.onlyAddedElements() || exampleDiff.onlyDeletedElements()) {
                return node;
            }

            if (exampleDiff.getDeletedElements().contains(current)) {
                return node.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_EXAMPLE_DELETE));
            }

            if (exampleDiff.getAddedElements().contains(current)) {
                return node.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_EXAMPLE_ADD));
            }

            if (exampleDiff.getSameElements().contains(current)) {
                if (Objects.equals(exampleDiff.getMap().get(current).getValue(),
                                   current.getValue())) {
                    return node.changeMark(new ChangeMark());
                }

                return node.changeMark(new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_EXAMPLE_MODIFY));
            }

            return node;
        };
    }

    private static S1000DGenericElementNode createExamplesFromTaggedValues(ExampleTagDiffHelper exampleDiff) {
        return new RandomList(plural(exampleDiff.getCurrentElements().size(),
                                     UOF_CLASS_DEFINITION_EXAMPLE,
                                     UOF_CLASS_DEFINITION_EXAMPLE_PLURAL)).child(getExamples(exampleDiff));
    }

    private static Note createNotesFromTaggedValues(NoteTagDiffHelper noteDiff) {
        return new Note().noteParaString(noteDiff.getCurrentElements().stream().map(MfTag::getValue).toList());
    }

    private static BrLevelledPara createBrLevelledParaFromClass(MfClass mfClass,
                                                                ClassOrInterfaceDiffHelper classDiff) {
        final MfClass previousClass = (MfClass) classDiff.getMap().get(mfClass);
        // Examples
        final ExampleTagDiffHelper exampleDiff =
                new ExampleTagDiffHelper(mfClass,
                                         previousClass);

        // Notes
        final NoteTagDiffHelper noteDiff =
                new NoteTagDiffHelper(mfClass,
                                      previousClass);

        // Title (name)
        final Title title =
                new Title(List.of(new S1000DTextNode(mfClass.getName()))).changeMark(getTitleChangeMark(mfClass, previousClass));

        // Definition change mark
        final ChangeMark definitionChangeMark = getDefinitionChangeMark(mfClass, classDiff);

        // Attributes
        final PropertyDiffHelper attributeDiff =
                new PropertyDiffHelper(mfClass, previousClass);

        // Implemented Interfaces
        final ImplementedInterfaceDiffHelper interfaceDiff =
                new ImplementedInterfaceDiffHelper(mfClass, previousClass);

        // Connectors
        final ConnectorDiffHelper connectorDiff = new ConnectorDiffHelper(mfClass, previousClass);

        BrLevelledPara brLevelledPara =
                new BrLevelledPara(PARA_LEVEL_FOR_CLASSES_AND_INTERFACES).id(EaModelIoUtils.getXmlParaIdFromClass(mfClass))
                                                                         .title(title);

        Para classIntroPara = new Para(mfClass.wrap(AsdElement.class).getNotes(),
                                       Set.of(FormattingPolicy.CLASS_VERBATIM,
                                              FormattingPolicy.CLASS_REFERTO)).changeMark(definitionChangeMark);

        if (!exampleDiff.getNetElements().isEmpty()) {
            S1000DGenericElementNode examplesRandomList = createExamplesFromTaggedValues(exampleDiff);

            if (exampleDiff.onlyAddedElements()) {
                examplesRandomList =
                        examplesRandomList.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_EXAMPLE_ADD));
            }

            if (exampleDiff.onlyDeletedElements()) {
                examplesRandomList =
                        examplesRandomList.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_EXAMPLE_DELETE));
            }

            classIntroPara = classIntroPara.content(examplesRandomList);
        }

        brLevelledPara = brLevelledPara.para(classIntroPara);

        if (!noteDiff.getNetElements().isEmpty()) {
            Note notes = createNotesFromTaggedValues(noteDiff);

            if (noteDiff.onlyAddedElements()) {
                notes = notes.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_NOTE_ADD));
            } else if (noteDiff.onlyDeletedElements()) {
                notes = notes.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_NOTE_DELETE));
            } else if (noteDiff.getSameElements()
                               .stream()
                               .anyMatch(tag -> !Objects.equals(tag.getValue(),
                                                                noteDiff.getMap().get(tag).getValue()))) {
                notes = notes.changeMark(new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_NOTE_MODIFY));
            }

            brLevelledPara = brLevelledPara.para(notes);
        }

        if (!attributeDiff.getNetElements().isEmpty()) {
            BrLevelledPara attributeSection =
                    new BrLevelledPara(4).title(UOF_ATTRIBUTE_DEFINITION_SECTION)
                                         .brLevelledPara(createParaForAttributeSection(attributeDiff))
                                         .changeMark(new ChangeMark());

            if (attributeDiff.onlyAddedElements()) {
                attributeSection =
                        attributeSection.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_PROPERTY_SECTION_ADD));
            } else if (attributeDiff.onlyDeletedElements()) {
                attributeSection =
                        attributeSection.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_PROPERTY_SECTION_DELETE));
            }

            brLevelledPara = brLevelledPara.brLevelledPara(attributeSection);
        }

        if (!interfaceDiff.getNetElements().isEmpty()) {
            BrLevelledPara interfacesSection = new BrLevelledPara(4).title(UOF_IMPLEMENTATION_DEFINITION_SECTION)
                                                                    .para(createParaForInterfaceSection(interfaceDiff))
                                                                    .changeMark(new ChangeMark());

            if (interfaceDiff.onlyAddedElements()) {
                interfacesSection =
                        interfacesSection.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_INTERFACE_IMPLEMENTED_SECTION_ADD));
            } else if (interfaceDiff.onlyDeletedElements()) {
                interfacesSection =
                        interfacesSection.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_INTERFACE_IMPLEMENTED_SECTION_DELETE));
            }

            brLevelledPara = brLevelledPara.brLevelledPara(interfacesSection);
        }

        if (!connectorDiff.getNetElements().isEmpty()) {
            BrLevelledPara connectorsSection = new BrLevelledPara(4).title(UOF_ASSOCIATION_DEFINITION_SECTION)
                                                                    .para(createParaForAssociationSection(mfClass,
                                                                                                          connectorDiff))
                                                                    .changeMark(new ChangeMark());

            if (connectorDiff.onlyAddedElements()) {
                connectorsSection =
                        connectorsSection.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_CLASS_CONNECTOR_SECTION_ADD));
            } else if (connectorDiff.onlyDeletedElements()) {
                connectorsSection =
                        connectorsSection.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_CLASS_CONNECTOR_SECTION_DELETE));
            }
            brLevelledPara = brLevelledPara.brLevelledPara(connectorsSection);

        }

        return brLevelledPara;
    }

    private static ChangeMark getDefinitionChangeMark(MfClass mfClass,
                                                      ClassOrInterfaceDiffHelper classDiff) {
        if (Objects.equals(mfClass.wrap(AsdElement.class).getNotes(),
                           Optional.ofNullable(classDiff.getMap().get(mfClass))
                                   .map(c -> c.wrap(AsdElement.class))
                                   .map(AsdElement::getNotes)
                                   .orElse(mfClass.wrap(AsdElement.class).getNotes()))) {
            return new ChangeMark();
        }
        return new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(String.format(RFU_CLASS_DEFINITION_MODIFY,
                                                                                     mfClass.getName(),
                                                                                     classDiff.getMap()
                                                                                              .get(mfClass)
                                                                                              .getName()));
    }

    private static ChangeMark getTitleChangeMark(MfClass mfClass,
                                                 MfClass previousClass) {
        if (Objects.equals(mfClass.getName(),
                           Optional.ofNullable(previousClass)
                                   .map(MfType::getName)
                                   .orElse(mfClass.getName()))) {
            return new ChangeMark();
        }

        return new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(String.format(RFU_CLASS_TITLE_MODIFY,
                                                                                     previousClass.getName(),
                                                                                     mfClass.getName()));
    }

    /**
     * Create the "associations" paragraph.
     *
     * @param mfType The object for which we are going to scan for associations.
     *            Aggregations and regular associations are included.
     * @param connectorDiff The helper that registers the changes to report (if any)
     * @return A paragraph with a list ready for integration in a document.
     */
    private static Para createParaForAssociationSection(MfType mfType,
                                                        ConnectorDiffHelper connectorDiff) {
        final String objectType;

        if (mfType instanceof MfInterface) {
            objectType = "interface";
        } else if (mfType instanceof MfClass) {
            objectType = "class";
        } else {
            throw new IllegalArgumentException("An illegal kind of association was found. An association"
                    + " must only be related to MfClass or MfInterface objects. The source object encountered "
                    + "was: " + mfType.getName() + " of type " + mfType.getKind());
        }

        final List<S1000DGenericElementNode> associations =
                connectorDiff.getNetElements()
                             .stream()
                             .sorted(Comparator.comparing(S1000DXmlIo::compareConnectors))
                             .map(computeChangeMark(connectorDiff))
                             .toList();

        final S1000DGenericElementNode randomList =
                new RandomList().child(associations);

        return new Para(String.format(plural(mfType.getAllConnectors()
                                                   .stream()
                                                   .filter(EaModelIoUtils.belongsTo(mfType))
                                                   .toList()
                                                   .size(),
                                             UOF_ASSOCIATION_SECTION_SINGULAR,
                                             UOF_ASSOCIATION_SECTION_PLURAL),
                                      objectType)).content(randomList);
    }

    private static String compareConnectors(MfConnector connector) {
        return connector.getTargetTip().getType().getName();
    }

    // Compute change marks depending on changes. Do not put change marks if
    // parent itself has a change mark.
    private static Function<MfConnector, S1000DGenericElementNode> computeChangeMark(ConnectorDiffHelper connectorDiff) {
        return connector -> {
            ChangeMark associationChangeMark = new ChangeMark();
            final Para associationPara = formatAssociation(connector, connectorDiff);

            if (!(connectorDiff.onlyAddedElements() && connectorDiff.onlyDeletedElements())) {

                if (connectorDiff.getSameElements().contains(connector)) {
                    final MfCardinality previousCardinality =
                            connectorDiff.getMap()
                                         .get(connector)
                                         .getTargetTip()
                                         .getEffectiveCardinality();

                    if (!Objects.equals(previousCardinality, connector.getTargetTip().getEffectiveCardinality())) {
                        associationChangeMark = associationChangeMark.changeType(ChangeType.MODIFIED)
                                                                     .reasonForUpdateText(String.format(RFU_CONNECTOR_MODIFY_CARDINALITY,
                                                                                                        toString(previousCardinality),
                                                                                                        toString(connector.getTargetTip()
                                                                                                                          .getEffectiveCardinality())));
                    }
                } else if (connectorDiff.getAddedElements().contains(connector)) {
                    associationChangeMark = associationChangeMark.changeType(ChangeType.ADDED)
                                                                 .reasonForUpdateText(RFU_CONNECTOR_ADD);
                } else if (connectorDiff.getDeletedElements().contains(connector)) {
                    associationChangeMark = associationChangeMark.changeType(ChangeType.DELETED)
                                                                 .reasonForUpdateText(RFU_CONNECTOR_DELETE);
                }
            }

            return EaModelIoUtils.listItem(associationPara).changeMark(associationChangeMark);
        };

    }

    /**
     * Create a paragraph for an association.
     *
     * @param connector The connector materializing the association.
     * @param connectorDiff The helper that registers the changes to report (if any).
     * @return A new {@code Para}.
     */
    private static Para formatAssociation(MfConnector connector,
                                          ConnectorDiffHelper connectorDiff) {
        String associationIntroText;
        String associationCardinalityText;
        final String role = connector.getTargetTip()
                                     .getRole();
        final MfCardinality cardinality = connector.getTargetTip()
                                                   .getEffectiveCardinality();
        final MfType connectorSourceType = connector.getSourceTip()
                                                    .getType();

        if (connector instanceof MfComposition || connector instanceof MfAggregation) {
            associationIntroText = ASSOCIATION_AGGREGATE_INTRO;
        } else if (connector instanceof MfAssociation /*
                                                       * &&
                                                       * connector.getDirection() == EaConnectorDirection.FORWARD
                                                       */) {
            associationIntroText = ASSOCIATION_DIRECTED_INTRO;
        } else {
            if (role == null) {
                associationIntroText = ASSOCIATION_GENERIC_INTRO_AN;
            } else {
                associationIntroText = ASSOCIATION_GENERIC_INTRO_A;
            }
        }

        Para para = new Para(associationIntroText,
                             Set.of(FormattingPolicy.CLASS_VERBATIM));

        if (role != null) {
            para = para.content(List.of(new S1000DTextNode(" "),
                                        EaModelIoUtils.verbatimTextAttribute(role)));
        }

        associationCardinalityText =
                MfCardinality.isUnbounded(cardinality.getMax()) ? ASSOCIATION_CARDINALITY_PLURAL
                        : ASSOCIATION_CARDINALITY_SINGULAR;

        para = para.content(new S1000DTextNode(String.format(associationCardinalityText,
                                                             toString(cardinality),
                                                             connector.getTargetTip()
                                                                      .getType()
                                                                      .getName()),
                                               Set.of(FormattingPolicy.CLASS_VERBATIM,
                                                      FormattingPolicy.CLASS_REFERTO)));

        if (connectorSourceType != connectorDiff.getParentMap().get(connector)) {
            para = para.content(new S1000DTextNode(String.format(" (" +
                    CARDINALITYTEXT_INHERITED + ")", connectorSourceType.getName()),
                                                   Set.of(FormattingPolicy.CLASS_VERBATIM)));
        }

        return para;
    }

    /**
     * Create the "implementations" paragraph with the following logic: first,
     * print the inherited implementations and sort them according to the name
     * of the relating interface. Then print the direct imp
     *
     * @param implementedInterfacesDiff The helper that reports all the changes to
     *            report (if any).
     * @return A paragraph with a list ready for integration in a document.
     */
    private static Para createParaForInterfaceSection(ImplementedInterfaceDiffHelper implementedInterfacesDiff) {

        final S1000DGenericElementNode randomList =
                new RandomList().child(implementedInterfacesDiff.getNetElements()
                                                                .stream()
                                                                .sorted(Comparator.comparing(MfInterface::getName))
                                                                .map(computeChangeMark(implementedInterfacesDiff))
                                                                .toList());

        return new Para(plural(implementedInterfacesDiff.getCurrentElements().size(),
                               UOF_IMPLEMENTATIONS_SECTION,
                               UOF_IMPLEMENTATIONS_SECTION_PLURAL)
                + ":").content(randomList);
    }

    private static Function<MfInterface, S1000DNode> computeChangeMark(ImplementedInterfaceDiffHelper implementedInterfacesDiff) {
        return current -> {
            final MfInterface previousInterface = implementedInterfacesDiff.getMap().get(current);
            S1000DGenericElementNode output;

            if (implementedInterfacesDiff.isDirectlyImplemented(current)) {
                output = formatDirectInterfaceImplementation(current);
            } else {
                output = formatInheritedInterfaceImplementation(current,
                                                                implementedInterfacesDiff.getImplementingAncestorClass(current));
            }

            if (implementedInterfacesDiff.onlyAddedElements() || implementedInterfacesDiff.onlyDeletedElements()) {
                return output;
            }

            if (implementedInterfacesDiff.getAddedElements().contains(current)) {
                return output.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_INTERFACE_IMPLEMENTED_ADD));
            }

            if (implementedInterfacesDiff.getDeletedElements().contains(current)) {
                return output.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_INTERFACE_IMPLEMENTED_DELETE));
            }

            // Put "Modified" if the inheritance changed, otherwise do not put
            // any change mark (the element is considered deleted/re-inserted)
            final Optional<MfClass> currentImplementingAncestor =
                    Optional.ofNullable(implementedInterfacesDiff.getImplementingAncestorClass(current));
            final Optional<MfClass> previousImplementingAncestor =
                    Optional.ofNullable(implementedInterfacesDiff.getImplementingAncestorClass(previousInterface));

            if (Objects.equals(currentImplementingAncestor.map(MfClass::getId),
                               previousImplementingAncestor.map(MfClass::getId))
                    || Objects.equals(currentImplementingAncestor.map(MfClass::getName),
                                      previousImplementingAncestor.map(MfClass::getName))) {
                return output;
            }

            return output;

        };
    }

    /**
     * Convert an interface into a formatted {@link S1000DNode} along
     * with the string representation of that interface (for sorting
     * purposes).
     *
     * @param iface The interface.
     * @return A formatted {@code S1000DNode}.
     */
    private static S1000DGenericElementNode formatDirectInterfaceImplementation(MfInterface iface) {
        final String ifaceName = iface.getName();

        return EaModelIoUtils.listItem(new Para(ifaceName,
                                                Set.of(FormattingPolicy.CLASS_VERBATIM,
                                                       FormattingPolicy.INTERFACE_SINGLE_REFERTO)).content(new S1000DTextNode(".")));
    }

    /**
     * Convert an interface into a formatted {@link S1000DNode} along
     * with the string representation of that interface (for sorting
     * purposes).
     *
     * @param inheritedInterface The interface that was implemented by inheritance.
     * @param originatingClass The class from which this interface
     *            implementation was inherited.
     * @return A formatted {@code S1000DNode}.
     */
    private static S1000DGenericElementNode formatInheritedInterfaceImplementation(MfInterface inheritedInterface,
                                                                                   MfClass originatingClass) {
        final String ifaceName = inheritedInterface.getName();

        final S1000DNode paraContent =
                new S1000DTextNode(ifaceName
                        + " " + String.format(CARDINALITYTEXT_INHERITED,
                                              originatingClass.getName()),
                                   Set.of(FormattingPolicy.CLASS_VERBATIM,
                                          FormattingPolicy.INTERFACE_REFERTO_ENDOFLINE));

        return EaModelIoUtils.listItem(new Para(paraContent));
    }

    private static List<BrLevelledPara> createParaForAttributeSection(PropertyDiffHelper propertyDiff) {

        return propertyDiff.getNetElements()
                           .stream()
                           .map(computeChangeMark(propertyDiff))
                           .toList();
    }

    private static Function<MfProperty, BrLevelledPara> computeChangeMark(PropertyDiffHelper propertyDiff) {
        return current -> {
            BrLevelledPara para = createParaForAttribute(current, propertyDiff);

            if (propertyDiff.getDeletedElements().contains(current)) {
                para = para.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_PROPERTY_DELETE));
            } else if (propertyDiff.getAddedElements().contains(current)) {
                para = para.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_PROPERTY_ADD));
            } else {
                para = para.changeMark(new ChangeMark());
            }

            return para;
        };
    }

    /**
     * Create one {@code BrLevelledPara} object for the provided attribute.
     * Title will contain attribute's name as well as its cardinality.
     * If this attribute does not belong to the supplied {@code currentClass},
     * it is assumed to be inherited from another class or interface, in such case
     * the description is not provided, and a link to the belonging class is
     * provided instead.
     *
     * @param currentProperty TODO
     * @param propertyDiff TODO
     * @return A new {@code BrLevelledPara} to insert into the {@code currentClass}'s
     *         corresponding data module.
     */
    private static BrLevelledPara createParaForAttribute(MfProperty currentProperty,
                                                         PropertyDiffHelper propertyDiff) {
        Para attributeDescription;
        final Optional<MfProperty> previousProperty =
                Optional.ofNullable(propertyDiff.getMap().get(currentProperty));

        // Title
        final Title title = formatAttribute(currentProperty).changeMark(new ChangeMark());
        ChangeMark titleChangeMark = new ChangeMark();

        if (previousProperty.isPresent()) {
            if (!Objects.equals(Optional.of(currentProperty.getName()),
                                previousProperty.map(MfProperty::getName))) {
                titleChangeMark = titleChangeMark.changeType(ChangeType.MODIFIED)
                                                 .reasonForUpdateText(RFU_PROPERTY_NAME_MODIFY);
            }

            if (!Objects.equals(Optional.of(currentProperty.getEffectiveCardinality()),
                                previousProperty.map(MfProperty::getEffectiveCardinality))) {
                titleChangeMark = titleChangeMark.changeType(ChangeType.MODIFIED)
                                                 .reasonForUpdateText(String.format(RFU_PROPERTY_CARDINALITY_MODIFY,
                                                                                    toString(previousProperty.get()
                                                                                                             .getEffectiveCardinality()),
                                                                                    toString(currentProperty.getEffectiveCardinality())));
            }

        }

        BrLevelledPara brLevelledPara =
                new BrLevelledPara(5).title(title.changeMark(titleChangeMark));

        if (!Objects.equals(currentProperty.getParent(), propertyDiff.getParentClass())) {
            attributeDescription = new Para(String.format(CARDINALITYTEXT_INHERITED,
                                                          currentProperty.getParent()
                                                                         .getName()),
                                            Set.of(FormattingPolicy.CLASS_VERBATIM,
                                                   FormattingPolicy.CLASS_REFERTO)).changeMark(new ChangeMark());

            if (previousProperty.isPresent()
                    && Objects.equals(previousProperty.map(MfProperty::getParent),
                                      Optional.ofNullable(propertyDiff.getPreviousParentClass()))) {
                attributeDescription =
                        attributeDescription.changeMark(new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_PROPERTY_PROMOTED));
            }

            return brLevelledPara.para(attributeDescription);
        }

        attributeDescription = new Para(currentProperty.wrap(AsdElement.class).getNotes(),
                                        Set.of(FormattingPolicy.CLASS_VERBATIM));

        if (previousProperty.isPresent()
                && !Objects.equals(previousProperty.map(MfProperty::getParent),
                                   Optional.ofNullable(propertyDiff.getPreviousParentClass()))) {
            attributeDescription =
                    attributeDescription.changeMark(new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_PROPERTY_DEMOTED));
        }

        final ValidValueTagDiffHelper validValueDiff =
                new ValidValueTagDiffHelper(currentProperty, previousProperty.orElse(null));

        if (!validValueDiff.getNetElements().isEmpty()) {
            ChangeMark validValueChangeMark = new ChangeMark();
            final SourceTagDiffHelper sourcesDiff =
                    new SourceTagDiffHelper(currentProperty,
                                            previousProperty.orElse(null));

            if (validValueDiff.onlyAddedElements()) {
                validValueChangeMark =
                        validValueChangeMark.changeType(ChangeType.ADDED).reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_ADD);
            } else if (validValueDiff.onlyDeletedElements()) {
                validValueChangeMark =
                        validValueChangeMark.changeType(ChangeType.DELETED).reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_DELETE);
            }

            brLevelledPara =
                    brLevelledPara.para(new Para(createValidValuesSection(validValueDiff)).changeMark(validValueChangeMark));

            if (!sourcesDiff.getNetElements().isEmpty()) {
                ChangeMark sourcesChangeMark = new ChangeMark();

                // No need to be specific for this aspect of the attribute
                if (sourcesDiff.onlyAddedElements()) {
                    sourcesChangeMark =
                            sourcesChangeMark.changeType(ChangeType.ADDED).reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_SOURCE_ADD);
                } else if (sourcesDiff.onlyDeletedElements()) {
                    sourcesChangeMark = sourcesChangeMark.changeType(ChangeType.DELETED)
                                                         .reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_SOURCE_DELETE);
                } else if (sourcesDiff.getMap()
                                      .entrySet()
                                      .stream()
                                      .anyMatch(e -> !Objects.equals(e.getKey().getValue(),
                                                                     e.getValue().getValue()))) {
                    sourcesChangeMark = sourcesChangeMark.changeType(ChangeType.MODIFIED)
                                                         .reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_SOURCE_MODIFY);
                }
                brLevelledPara =
                        brLevelledPara.para(new Para(createSourcesSection(sourcesDiff.getCurrentElements()).changeMark(sourcesChangeMark)));
            }
        }

        return brLevelledPara;
    }

    private static S1000DGenericElementNode createValidValuesSection(ValidValueTagDiffHelper validValueDiff) {
        final List<S1000DGenericElementNode> validValueNodes =
                validValueDiff.getNetElements()
                              .stream()
                              .map(computeChangeMark(validValueDiff))
                              .toList();

        if (validValueDiff.getCurrentProperty()
                          .wrap(AsdProperty.class)
                          .hasType(AsdPrimitiveTypeName.IDENTIFIER)) {
            return new DefinitionList(VALID_VALUES_CLASS_VALUES_TEXT).child(validValueNodes);
        }

        return new DefinitionList(VALID_VALUES_TEXT).child(validValueNodes);
    }

    private static Function<MfTag, S1000DGenericElementNode> computeChangeMark(ValidValueTagDiffHelper validValueDiff) {
        return current -> {
            S1000DGenericElementNode node =
                    EaModelIoUtils.formatValidValues(current.getValue(),
                                                     current.wrap(AsdTag.class).getNotes())
                                  .changeMark(new ChangeMark());

            if (validValueDiff.onlyAddedElements() || validValueDiff.onlyDeletedElements()) {
                return node;
            }
            if (validValueDiff.getDeletedElements().contains(current)) {
                node = node.changeMark(new ChangeMark(ChangeType.DELETED).reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_DELETE_ITEM));
            } else if (validValueDiff.getAddedElements().contains(current)) {
                node = node.changeMark(new ChangeMark(ChangeType.ADDED).reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_ADD_ITEM));
            } else if (validValueDiff.getSameElements().contains(current)) {
                if (!Objects.equals(validValueDiff.getMap().get(current).getValue(),
                                    current.getValue())) {
                    node = node.changeMark(new ChangeMark(ChangeType.MODIFIED).reasonForUpdateText(RFU_PROPERTY_VALIDVALUE_MODIFIED_ITEM));
                }
            }

            return node;
        };
    }

    private static S1000DGenericElementNode createSourcesSection(List<? extends MfTag> sourcesList) {
        return new RandomList(SOURCES_TEXT).child(sourcesList.stream()
                                                             .map(MfTag::getValue)
                                                             .map(Para::new)
                                                             .map(EaModelIoUtils::listItem)
                                                             .collect(Collectors.toList()));
    }

    /**
     * Transforms an MfCardinality into a string. The value is meant to be placed
     * directly in the output of the S1000D XML, so it may be concise and lack
     * precision.
     * For example, 1..* will yield "one or many".
     *
     * @param cardinality The cardinality.
     * @return The textual representation.
     */
    private static String toString(MfCardinality cardinality) {
        // TODO incorporate directly in MfCardinality??
        final int min = cardinality.getMin();
        final int max = cardinality.getMax();

        if (MfCardinality.isUnbounded(min) ||
                MfCardinality.isError(min) ||
                MfCardinality.isError(max)) {
            return " " + CARDINALITYTEXT_ERROR;
        }

        if (MfCardinality.isUnbounded(max)) {
            if (min == 0) {
                return CARDINALITYTEXT_ZERO_MANY;
            }

            return String.format(CARDINALITYTEXT_VALUERANGE_ONE, numberToWord(min));
        }

        if (min == max) {
            return numberToWord(min);
        }

        return String.join(" ",
                           numberToWord(min),
                           connectingWord(min, max),
                           numberToWord(max));
    }

    /**
     * Format attribute name into a nice sentence depending
     * on cardinality.
     *
     * @param attribute The attribute
     * @return A {@code Para} with the attribute and its sugar coating
     */
    private static Title formatAttribute(MfProperty attribute) {
        final MfCardinality cardinality =
                attribute.getEffectiveCardinality();
        final StringBuilder attributeText =
                new StringBuilder(" (").append(toString(cardinality))
                                       .append(")");

        return new Title(List.of(EaModelIoUtils.verbatimTextAttribute(attribute.getName()),
                                 new S1000DTextNode(attributeText.toString())));
    }

    /**
     * Connect two numbers for cardinalities with
     * a syntactically appropriate word.
     *
     * @param min The minimal cardinality
     * @param max The maximal cardinality
     * @return {@code CARDINALITYTEXT_VALUEPAIR} or {@code CARDINALITYTEXT_VALUERANGE}
     */
    private static String connectingWord(int min,
                                         int max) {
        return max - min == 1 ? CARDINALITYTEXT_VALUEPAIR : CARDINALITYTEXT_VALUERANGE;
    }

    /**
     * Returns a word from a number. Up to 10.
     *
     * @param number The integer to convert into a word.
     * @return The resulting word, or the literal integer converted
     *         into a {@code String}.
     */
    private static String numberToWord(int number) {
        if (number < 0 || number > NUMBER_WORD_REPRESENTATION.length) {
            return String.valueOf(number);
        }

        return NUMBER_WORD_REPRESENTATION[number];
    }

    /**
     * Returns one of the supplied inputs depending on the
     * supplied {@code value}.
     *
     * @param value The value. May not be negative.
     * @param singular The singular variant.
     * @param plural The plural variant.
     * @return Returns {@code singular} if value is inferior or
     *         equal to 1, or {@code plural} otherwise.
     * @throws IllegalArgumentException If an invalid {@code value}
     *             is supplied.
     */
    private static String plural(int value,
                                 String singular,
                                 String plural) {
        if (value < 0) {
            throw new IllegalArgumentException("This function can only be used on non negative integers.");
        }

        return value <= 1 ? singular : plural;
    }

    /**
     * Populate a Data Module template with "top-level" values.
     *
     * @param document The document for the Data Module, that will be modified
     *            to include the static/top-level values.
     * @param uof The UoF that contains the top-level values (e.g.
     *            Spec title, spec issue date, etc.)
     */
    private static void populateStaticBrdocDocumentAttributes(Document document,
                                                              UofForDataModule uof) {
        final Element dmAddress = document.getElementNamed(S1000DNode.DMODULE)
                                          .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                                          .getElementNamed(S1000DNode.DMADDRESS);

        final Element dmCode = dmAddress.getElementNamed(S1000DNode.DMIDENT)
                                        .getElementNamed(S1000DNode.DMCODE);

        final Element issueDate = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                           .getElementNamed(S1000DNode.ISSUEDATE);

        final Element issueInfo = dmAddress.getElementNamed(S1000DNode.DMIDENT)
                                           .getElementNamed(S1000DNode.ISSUEINFO);

        final Element techName = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                          .getElementNamed(S1000DNode.DMTITLE)
                                          .getElementNamed(S1000DNode.TECHNAME);

        final Element infoName = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                          .getElementNamed(S1000DNode.DMTITLE)
                                          .getElementNamed(S1000DNode.INFONAME);

        final Element dmStatus = document.getElementNamed(S1000DNode.DMODULE)
                                         .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                                         .getElementNamed(S1000DNode.DMSTATUS);

        dmStatus.getAttribute(S1000DNode.ISSUETYPE)
                .setValue("000".equals(uof.getIssueNumber()) ? "new" : "revised");

        dmStatus.getElementNamed(S1000DNode.LOGO)
                .getElementNamed(S1000DNode.SYMBOL)
                .getAttribute(S1000DNode.INFOENTITYIDENT)
                .setValue(uof.getLogoIcn());

        dmCode.getAttribute(S1000DNode.MODELIDENTCODE).setValue(uof.getModelIdentCode());
        dmCode.getAttribute(S1000DNode.SYSTEMCODE).setValue(uof.getSystemCode());
        dmCode.getAttribute(S1000DNode.SUBSYSTEMCODE).setValue(String.valueOf(uof.getSubSystemCode()));
        dmCode.getAttribute(S1000DNode.SUBSUBSYSTEMCODE).setValue(String.valueOf(uof.getSubSubSystemCode()));
        dmCode.getAttribute(S1000DNode.ASSYCODE).setValue(String.valueOf(uof.getAssyCode()));

        issueDate.getAttribute(S1000DNode.ISSUEDATE_YEAR)
                 .setValue(String.valueOf(uof.getIssueDate()
                                             .getYear()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_MONTH)
                 .setValue(String.format("%02d",
                                         uof.getIssueDate()
                                            .getMonthValue()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_DAY)
                 .setValue(String.format("%02d",
                                         uof.getIssueDate()
                                            .getDayOfMonth()));
        issueInfo.getAttribute(S1000DNode.ISSUENUMBER).setValue(uof.getIssueNumber());
        issueInfo.getAttribute(S1000DNode.INWORK).setValue(uof.getInWork());
        techName.addText(String.format(UOF_CHAPTER_TEXT,
                                       uof.getModelIdentCode()));
        infoName.addText(uof.getSimpleUofName());
    }

    /**
     * Populate a Data Module template with "top-level" values.
     *
     * @param document The document for the Data Module, that will be modified
     *            to include the static/top-level values.
     * @param dataModule The data module.
     * @implNote Nearly identical to populateStaticBrdocDocumentAttributes(Document, UofForDataModule),
     *           except for the attributes about UoF. Maybe creating a superclass for Data modules (independent of
     *           whether they are for UoFs or not) would be better.
     */
    private static void populateStaticBrdocDocumentAttributes(Document document,
                                                              SimpleDataModule dataModule) {
        final Element dmAddress = document.getElementNamed(S1000DNode.DMODULE)
                                          .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                                          .getElementNamed(S1000DNode.DMADDRESS);

        final Element dmCode = dmAddress.getElementNamed(S1000DNode.DMIDENT)
                                        .getElementNamed(S1000DNode.DMCODE);

        final Element issueDate = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                           .getElementNamed(S1000DNode.ISSUEDATE);

        final Element issueInfo = dmAddress.getElementNamed(S1000DNode.DMIDENT)
                                           .getElementNamed(S1000DNode.ISSUEINFO);

        final Element techName = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                          .getElementNamed(S1000DNode.DMTITLE)
                                          .getElementNamed(S1000DNode.TECHNAME);

        final Element infoName = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                          .getElementNamed(S1000DNode.DMTITLE)
                                          .getElementNamed(S1000DNode.INFONAME);

        dmCode.getAttribute(S1000DNode.MODELIDENTCODE).setValue(dataModule.getModelIdentCode());
        dmCode.getAttribute(S1000DNode.SYSTEMCODE).setValue(dataModule.getSystemCode());
        dmCode.getAttribute(S1000DNode.SUBSYSTEMCODE).setValue(String.valueOf(dataModule.getSubSystemCode()));
        dmCode.getAttribute(S1000DNode.SUBSUBSYSTEMCODE).setValue(String.valueOf(dataModule.getSubSubSystemCode()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_YEAR)
                 .setValue(String.valueOf(dataModule.getIssueDate()
                                                    .getYear()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_MONTH)
                 .setValue(String.format("%02d",
                                         dataModule.getIssueDate()
                                                   .getMonthValue()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_DAY)
                 .setValue(String.format("%02d",
                                         dataModule.getIssueDate()
                                                   .getDayOfMonth()));
        issueInfo.getAttribute(S1000DNode.ISSUENUMBER).setValue(dataModule.getIssueNumber());
        issueInfo.getAttribute(S1000DNode.INWORK).setValue(dataModule.getInWork());
        techName.addText(String.format(UOF_CHAPTER_TEXT,
                                       dataModule.getModelIdentCode()));
        infoName.addText(UOF_OVERVIEW_TEXT);

    }

    /**
     * Supply the DM ref to the overview data module. There can only be a single
     * one per publication module.
     *
     * @param document The document to modify.
     * @param overviewDm The overview document on data model.
     */
    private static void populateStaticPmDocumentOverviewDm(Document document,
                                                           SimpleDataModule overviewDm) {
        final Element dmRef = document.getElementNamed(S1000DNode.PM)
                                      .getElementNamed(S1000DNode.CONTENT)
                                      .getElementNamed(S1000DNode.PMENTRY)
                                      .getElementNamed(S1000DNode.DMREF);

        final Element dmCode = dmRef.getElementNamed(S1000DNode.DMREFIDENT)
                                    .getElementNamed(S1000DNode.DMCODE);

        document.getElementNamed(S1000DNode.PM)
                .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                .getElementNamed(S1000DNode.PMSTATUS)
                .getElementNamed(S1000DNode.LOGO)
                .getElementNamed(S1000DNode.SYMBOL)
                .getAttribute(S1000DNode.INFOENTITYIDENT)
                .setValue(overviewDm.getLogoIcn());

        dmCode.getAttribute(S1000DNode.MODELIDENTCODE)
              .setValue(overviewDm.getModelIdentCode());
        dmCode.getAttribute(S1000DNode.SYSTEMCODE)
              .setValue(overviewDm.getSystemCode());
        dmCode.getAttribute(S1000DNode.SUBSYSTEMCODE)
              .setValue(String.valueOf(overviewDm.getSubSystemCode()));
        dmCode.getAttribute(S1000DNode.SUBSUBSYSTEMCODE)
              .setValue(String.valueOf(overviewDm.getSubSubSystemCode()));
        dmCode.getAttribute(S1000DNode.ASSYCODE)
              .setValue(overviewDm.getAssyCode());
        dmCode.getAttribute(S1000DNode.DISASSYCODE)
              .setValue(overviewDm.getDisassyCode());
        dmCode.getAttribute(S1000DNode.DISASSYCODEVARIANT)
              .setValue(overviewDm.getDisassyCodeVariant());
        dmCode.getAttribute(S1000DNode.INFOCODE)
              .setValue(overviewDm.getInfoCode());
        dmCode.getAttribute(S1000DNode.INFOCODEVARIANT)
              .setValue(overviewDm.getInfoCodeVariant());
        dmCode.getAttribute(S1000DNode.ITEMLOCATIONCODE)
              .setValue(overviewDm.getItemLocationCode());

        dmRef.getElementNamed(S1000DNode.DMREFADDRESSITEMS)
             .getElementNamed(S1000DNode.DMTITLE)
             .getElementNamed(S1000DNode.TECHNAME)
             .addText(overviewDm.getDmTitle(), false);
    }
}