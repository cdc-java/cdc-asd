package cdc.asd.specgen.diffhelpers;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.args.Strictness;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfType;

public class PropertyDiffHelper extends DiffHelper<MfProperty, MfClass> {

    private final MfClass currentParentClass;
    private final MfClass previousParentClass;

    public PropertyDiffHelper(MfClass currentClass,
                                     MfClass previousClass) {
        super(currentClass, previousClass, MfProperty.class);

        this.currentParentClass = currentClass;
        this.previousParentClass = previousClass;

    }

    public final MfClass getParentClass() {
        return currentParentClass;
    }

    public final MfClass getPreviousParentClass() {
        return previousParentClass;
    }

    @Override
    protected List<? extends MfProperty> extractChildrenFromParent(Optional<MfClass> current,
                                                                   Class<? extends MfProperty> type) {
        return Stream.concat(current.stream()
                                    .map(c -> c.getAllAncestors(Strictness.STRICT))
                                    .flatMap(Collection::stream)
                                    .map(MfType::getProperties)
                                    .flatMap(List::stream),
                             current.stream()
                                    .map(MfClass::getProperties)
                                    .flatMap(Collection::stream))
                     .sorted(Comparator.comparing(PropertyDiffHelper::propertyKeyExtractor))
                     .toList();
    }

    private static String propertyKeyExtractor(MfProperty property) {
        // group by stereotype, and prepend stereotype with virtual
        // key so that some stereotypes (see below) pop up at first.
        String sortKey = "1";

        if (property.getStereotypes()
                    .stream()
                    .anyMatch(Set.of("key", "compositeKey", "relationshipKey")::contains)) {
            sortKey = "0";
        }

        return sortKey
                + property.getStereotypes().stream().collect(Collectors.joining())
                + property.getName();
    }
}
