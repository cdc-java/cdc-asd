package cdc.asd.specgen.diffhelpers;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import cdc.asd.specgen.EaModelIoUtils;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfTag;

public class IllustrationTagDiffHelper extends DiffHelper<MfTag, MfPackage> {

    public IllustrationTagDiffHelper(MfPackage currentUof,
                                            MfPackage previousUof) {
        super(currentUof, previousUof, MfTag.class);
    }

    @Override
    protected List<? extends MfTag> extractChildrenFromParent(Optional<MfPackage> pack,
                                                              Class<? extends MfTag> type) {
        return pack.stream()
                   .map(MfPackage::getTags)
                   .flatMap(Collection::stream)
                   .filter(EaModelIoUtils::isIcnTag)
                   .toList();
    }

    @Override
    protected Predicate<MfTag> itemMatches(MfTag other) {
        return current -> EaModelIoUtils.createFigureFromTag(current)
                                        .isSameFigure(EaModelIoUtils.createFigureFromTag(other));
    }
}
