package cdc.asd.specgen.diffhelpers;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.args.Strictness;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfInterface;

public final class ImplementedInterfaceDiffHelper extends DiffHelper<MfInterface, MfClass> {

    private final Map<MfInterface, MfClass> currentAncestorMap;
    private final Map<MfInterface, MfClass> previousAncestorMap;

    public ImplementedInterfaceDiffHelper(MfClass currentClass,
                                                 MfClass previousClass) {
        super(currentClass, previousClass, MfInterface.class);

        currentAncestorMap = getAncestorMap(currentClass);
        previousAncestorMap = getAncestorMap(previousClass);
    }

    private static void mapAncestorsToInterfaces(MfClass ancestor,
                                                 Consumer<Entry<MfInterface, MfClass>> ancestorMapper) {
        for (final MfInterface implementedInterface : ancestor.getDirectlyImplementedInterfaces()) {
            ancestorMapper.accept(Map.entry(implementedInterface, ancestor));
        }
    }

    @Override
    protected List<? extends MfInterface> extractChildrenFromParent(Optional<MfClass> someClass,
                                                                    Class<? extends MfInterface> type) {
        return Stream.concat(someClass.stream()
                                      .map(c -> c.getAllAncestors(Strictness.STRICT, MfClass.class))
                                      .flatMap(Collection::stream)
                                      .map(MfClass::getDirectlyImplementedInterfaces)
                                      .flatMap(Collection::stream)
                                      .sorted(Comparator.comparing(MfInterface::getName)),
                             someClass.stream()
                                      .map(MfClass::getDirectlyImplementedInterfaces)
                                      .flatMap(Collection::stream)
                                      .sorted(Comparator.comparing(MfInterface::getName)))
                     .toList();
    }

    private static Map<MfInterface, MfClass> getAncestorMap(MfClass someClass) {
        return Optional.ofNullable(someClass)
                       .map(c -> c.getAllAncestors(Strictness.STRICT, MfClass.class))
                       .stream()
                       .flatMap(Collection::stream)
                       .mapMulti(ImplementedInterfaceDiffHelper::mapAncestorsToInterfaces)
                       .collect(Collectors.toUnmodifiableMap(Entry::getKey, Entry::getValue));
    }

    public final boolean isDirectlyImplemented(MfInterface someInterface) {
        return !(currentAncestorMap.containsKey(someInterface) && previousAncestorMap.containsKey(someInterface));
    }

    public final MfClass getImplementingAncestorClass(MfInterface someInterface) {
        return Optional.ofNullable(someInterface).map(currentAncestorMap::get).orElse(null);
    }
}
