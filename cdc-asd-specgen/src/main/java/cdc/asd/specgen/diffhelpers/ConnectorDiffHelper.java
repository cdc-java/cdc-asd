package cdc.asd.specgen.diffhelpers;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.specgen.EaModelIoUtils;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfType;

public final class ConnectorDiffHelper extends DiffHelper<MfConnector, MfType> {

    private final Map<MfConnector, MfType> parentMap;

    public ConnectorDiffHelper(MfType currentClassOrInterface,
                                      MfType previousClassOrInterface) {
        super(currentClassOrInterface, previousClassOrInterface, MfConnector.class);

        parentMap = Stream.concat(getCurrentElements().stream().map(e -> Map.entry(e, currentClassOrInterface)),
                                  getPreviousElements().stream().map(e -> Map.entry(e, previousClassOrInterface)))
                          .collect(Collectors.toUnmodifiableMap(Entry::getKey, Entry::getValue));
    }

    private static Predicate<MfConnector> hasSameRoleAsCurrentConnector(MfConnector other) {
        return current -> Optional.ofNullable(current.getTargetTip().getRole())
                                  .equals(Optional.ofNullable(other.getTargetTip().getRole()));
    }

    private static Predicate<MfConnector> hasSameTargetTypeAsCurrentConnector(MfConnector other) {
        return current -> {
            final MfType currentTargetType = current.getTargetTip().getType();
            final MfType otherTargetType = other.getTargetTip().getType();

            // Use name or ID to compare both targets
            return currentTargetType.getName().equals(otherTargetType.getName()) ||
                    currentTargetType.getId().equals(otherTargetType.getId());
        };
    }

    @Override
    protected List<? extends MfConnector> extractChildrenFromParent(Optional<MfType> parent,
                                                                    Class<? extends MfConnector> type) {
        return parent.map(MfType::getAllConnectors)
                     .stream()
                     .flatMap(Collection::stream)
                     .filter(EaModelIoUtils.belongsTo(parent.orElse(null)))
                     .toList();
    }

    @Override
    protected Predicate<MfConnector> itemMatches(MfConnector other) {
        return hasSameRoleAsCurrentConnector(other).and(hasSameTargetTypeAsCurrentConnector(other));
    }

    public final Map<MfConnector, MfType> getParentMap() {
        return parentMap;
    }

}
