package cdc.asd.specgen.diffhelpers;

import java.util.function.Predicate;

import cdc.asd.specgen.datamodules.DataModules;
import cdc.mf.model.MfPackage;

public class PackageDiffHelper extends DiffHelper<MfPackage, MfPackage> {

    public PackageDiffHelper(MfPackage currentPackage,
                                       MfPackage previousPackage) {
        super(currentPackage, previousPackage, MfPackage.class);
    }

    private static Predicate<MfPackage> hasSameNormalizedNameAs(MfPackage other) {
        return current -> getNormalizeUofName(current).equals(getNormalizeUofName(other));
    }

    private static String getNormalizeUofName(MfPackage pack) {
        final String uofName = pack.getName();

        if (uofName.contains(" UoF ")) {
            return DataModules.LEGACY_UOF_PATTERN.matcher(uofName).replaceFirst("$1");
        }

        return DataModules.UOF_PATTERN.matcher(uofName).replaceFirst("$1");
    }

    @Override
    protected Predicate<MfPackage> itemMatches(MfPackage other) {
        return hasSameIdAs(other).or(hasSameNormalizedNameAs(other));
    }

}
