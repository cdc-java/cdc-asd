package cdc.asd.specgen.diffhelpers;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import cdc.mf.model.MfClass;
import cdc.mf.model.MfInterface;

public class ImplementingClassDiffHelper extends DiffHelper<MfClass, MfInterface> {

    public ImplementingClassDiffHelper(MfInterface currentParent,
                                              MfInterface previousParent) {
        super(currentParent, previousParent, MfClass.class);
    }

    @Override
    protected List<? extends MfClass> extractChildrenFromParent(Optional<MfInterface> someInterface,
                                                                Class<? extends MfClass> type) {
        return someInterface.stream()
                            .map(MfInterface::getDirectImplementations)
                            .flatMap(Collection::stream)
                            .filter(MfClass.class::isInstance)
                            .map(MfClass.class::cast)
                            .toList();
    }

}
