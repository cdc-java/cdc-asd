package cdc.asd.specgen.diffhelpers;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.mf.model.MfElement;
import cdc.mf.model.MfNameItem;

/**
 * A helper for processing differences between two sets of objects of {@link MfElement}
 * types. Both sets are assumed to relate to the same entity but in two different
 * issues, they are therefore called the previous set and the current set
 * and their relating elements are respectively called the previous elements and the current
 * elements.
 *
 * The helper extracts the objects of the supplied sets and attempts to relate each
 * object of one of the supplied sets, to another object of the other supplied set, if
 * possible. Once the helper is initialized, it provides methods for accessing both sets,
 * either in a distinguished or a unified way, as well as provide an overview of what
 * was added, deleted or was already there between the two versions of the supplied
 * sets of objects.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 * @param <T> The type of objects to compare.
 * @param <U> The set of objects that contain the objects to compare.
 */
abstract public class DiffHelper<T extends MfElement, U extends MfElement> {

    /**
     * The type of objects to compare.
     */
    private final Class<? extends T> type;

    /**
     * An immutable map that relates objects from the current set, to similar objects
     * in the previous set.
     */
    private final Map<T, T> map;

    /**
     * An immutable list of objects that are in the previous set.
     */
    private final List<? extends T> previousElements;

    /**
     * An immutable list of objects that are in the current set.
     */
    private final List<? extends T> currentElements;

    /**
     * An immutable list of objects that were in the previous set, but were not found
     * in the current set.
     */
    private final List<T> deletedElements;

    /**
     * An immutable list of objects that were in the previous set as well as the current
     * set, regardless of whether or not their non-key characteristics changed.
     */
    private final List<T> sameElements;

    /**
     * An immutable list of objects that were not found in the previous set, but were in the
     * current set.
     */
    private final List<T> addedElements;

    /**
     * A boolean that tells if we are comparing or not. We will not be comparing if a single
     * set of objects was supplied at initialization, although it does not preclude the
     * utilization of the helper (which will not report any differences).
     */
    private final boolean comparing;

    /**
     * An immutable list of objects, that is the union of both {@link #previousElements} and
     * {@link #currentElements}.
     */
    private final List<T> netElements;

    /**
     * Constructor to call from all subclasses. After initialization, this object MUST be
     * suitable for utilization (without any further initialization) and MUST be immutable.
     *
     * @param currentSet The current set of objects.
     * @param previousSet The previous set of objects. May be <code>null</code>.
     * @param type The type of objects, inside both supplied sets, to compare.
     */
    protected DiffHelper(U currentSet,
                         U previousSet,
                         Class<? extends T> type) {

        if (previousSet == null) {
            comparing = false;
        } else {
            comparing = true;
        }

        previousElements = removeDuplicates(extractChildrenFromParent(Optional.ofNullable(previousSet), type));
        currentElements = removeDuplicates(extractChildrenFromParent(Optional.of(currentSet), type));

        deletedElements = previousElements.parallelStream()
                                          .filter(Predicate.not(isContainedBy(currentElements)))
                                          .collect(Collectors.toUnmodifiableList());

        sameElements = currentElements.parallelStream()
                                      .filter(isContainedBy(previousElements))
                                      .collect(Collectors.toUnmodifiableList());

        addedElements = currentElements.parallelStream()
                                       .filter(x -> comparing)
                                       .filter(Predicate.not(isContainedBy(previousElements)))
                                       .collect(Collectors.toUnmodifiableList());

        map = sameElements.parallelStream()
                          .map(e -> Map.entry(e, previousElements.parallelStream().filter(itemMatches(e)).findFirst().get()))
                          .collect(Collectors.toUnmodifiableMap(Entry::getKey, Entry::getValue));

        netElements = Stream.concat(deletedElements.stream(),
                                    currentElements.stream())
                            .toList();

        this.type = type;
    }

    /**
     * Remove duplicate {@link MfElement} included in a {@link List}.
     * 
     * @param <T> The type for which we are removing duplicates
     * @param container The container to scan for duplicates.
     * @return A new container with duplicates removed.
     * @implNote Implemented because of issue #58
     */
    private static <T> List<T> removeDuplicates(List<T> container) {
        return container.stream()
                        .collect(Collectors.groupingBy(Function.identity()))
                        .values()
                        .stream()
                        .map(l -> l.get(0))
                        .toList();
    }

    /**
     * Extract the children from a set of objects.
     *
     * @param set The set that contains the objects to extract.
     * @param type The type of the objects to extract.
     * @return A list of the extracted objects. In the default method, the order
     *         is that supplied by {@link MfElement#getChildren()}.
     *
     * @implNote The default behavior is to use {@link MfElement#getChildren()}. If this
     *           behavior is inappropriate, then this method must be overridden.
     *
     * @apiNote Overridden methods MUST NOT access this object's internal state.
     *          This method is called upon this object's initialization and
     *          thus cannot access any of its internal state or that of any
     *          subclass. When overriding this method, use as though it was
     *          a static function.
     */
    protected List<? extends T> extractChildrenFromParent(Optional<U> set,
                                                          Class<? extends T> type) {
        return set.map(U::getChildren)
                  .stream()
                  .flatMap(Collection::stream)
                  .filter(type::isInstance)
                  .map(type::cast)
                  .toList();
    }

    /**
     * Provide a {@link Predicate} that returns <code>true</code> if an object's functional
     * properties match those of the other object or <code>false</code> otherwise. This
     * method is required to establish the identity of an object and relate its instances
     * in the previous set and the current set.
     *
     * @param other The other object for which to match functional properties.
     * @return A {@link Predicate} that returns <code>true</code> if the other object's
     *         functional properties match the {@link Predicate}'s input or <code>false</code>
     *         otherwise.
     *
     * @implNote The default behavior is to use {@link #hasSameIdAs(MfElement)} or
     *           {@link #hasSameNameAs(MfElement)}. If this behavior is inappropriate,
     *           then this method must be overridden.
     *
     * @apiNote Overridden methods MUST NOT access this object's internal state.
     *          This method is called upon this object's initialization and
     *          thus cannot access any of its internal state or that of any
     *          subclass. When overriding this method, use as though it was
     *          a static function.
     */
    protected Predicate<T> itemMatches(T other) {
        return hasSameIdAs(other).or(hasSameNameAs(other));
    }

    /**
     * Provide a {@link Predicate} that returns <code>true</code> if the {@link Predicate}'s
     * input has any match in the supplied collection. The match is tested with
     * {@link #itemMatches(MfElement)}.
     *
     * @param other The other object to match with the {@link Predicate}'s input.
     * @return A {@link Predicate} that returns <code>true</code> if there is match
     *         or <code>false</code> otherwise.
     */
    private final Predicate<T> isContainedBy(Collection<? extends T> other) {
        return current -> other.stream().anyMatch(itemMatches(current));
    }

    /**
     * Provide a {@link Predicate} that tests whether its input has the same name
     * (obtained with {@link MfNameItem#getName()}) as the other object.
     *
     * @param <T> The type of {@link Predicate}'s input as well as that of the other object.
     * @param other The other object to compare with the {@link Predicate}'s input.
     * @return A {@link Predicate} that returns <code>true</code> if both object's
     *         names are equal or <code>false</code> in all other cases.
     */
    protected final static <T extends MfElement> Predicate<T> hasSameNameAs(T other) {
        return current -> {
            if (other instanceof final MfNameItem o
                    && current instanceof final MfNameItem c) {
                return c.getName().equals(o.getName());
            }

            return false;
        };
    }

    /**
     * Provide a {@link Predicate} that tests whether its input has the same identifier
     * (obtained with {@link MfElement#getId()}) as of the other object.
     *
     * @param <T> The type of {@link Predicate}'s input as well as that of the other object.
     * @param other The other object to compare with the {@link Predicate}'s input.
     * @return A {@link Predicate} that returns <code>true</code> if both object's identifiers
     *         are equal or <code>false</code> otherwise.
     */
    protected final static <T extends MfElement> Predicate<T> hasSameIdAs(T other) {
        return current -> current.getId().equals(other.getId());
    }

    /**
     * Get the objects that were in the previous set.
     *
     * @return The {@link #previousElements}.
     */
    public final List<? extends T> getPreviousElements() {
        return previousElements;
    }

    /**
     * Get the objects that were in the current set.
     *
     * @return The {@link #currentElements}.
     */
    public final List<? extends T> getCurrentElements() {
        return currentElements;
    }

    /**
     * Get the objects that were deleted.
     *
     * @return The {@link #deletedElements}.
     */
    public final List<T> getDeletedElements() {
        return deletedElements;
    }

    /**
     * Get the objects that were neither deleted nor added.
     *
     * @return The {@link #sameElements}.
     */
    public final List<T> getSameElements() {
        return sameElements;
    }

    /**
     * Get the objects that were added.
     *
     * @return The {@link #addedElements}.
     */
    public final List<T> getAddedElements() {
        return addedElements;
    }

    /**
     * Get the map that relates current objects with their previous
     * issue, if any.
     *
     * @return A map with the object
     */
    public final Map<T, T> getMap() {
        return map;
    }

    /**
     * Get a union of {@link #getDeletedElements()}, {@link #getSameElements()}
     * and {@link #getAddedElements()}.
     *
     * @return The {@link #netElements}.
     */
    public final List<T> getNetElements() {
        return netElements;
    }

    /**
     * Tests whether this helper only has deleted elements.
     *
     * @return <code>true</code> if the helper only has deleted elements and
     *         is {@link #comparing}, <code>false</code> otherwise.
     */
    public final boolean onlyDeletedElements() {
        return comparing && currentElements.isEmpty() && !deletedElements.isEmpty();
    }

    /**
     * Tests whether this helper only has added elements.
     *
     * @return <code>true</code> if the helper only has added elements and
     *         is {@link #comparing}, <code>false</code> otherwise.
     */
    public final boolean onlyAddedElements() {
        return comparing &&
                deletedElements.isEmpty() &&
                sameElements.isEmpty() &&
                !addedElements.isEmpty();
    }

    @Override
    public final String toString() {
        return String.format("%s [%s]", type.getSimpleName(), super.toString());
    }
}
