package cdc.asd.specgen.diffhelpers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import cdc.asd.model.AsdTagName;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;

public class ExampleTagDiffHelper extends DiffHelper<MfTag, MfTagOwner> {

    public ExampleTagDiffHelper(MfTagOwner currentClass,
                                           MfTagOwner previousClass) {
        super(currentClass, previousClass, MfTag.class);

    }

    @Override
    protected List<? extends MfTag> extractChildrenFromParent(Optional<MfTagOwner> parent,
                                                              Class<? extends MfTag> type) {
        return parent.map(p -> p.getTags(AsdTagName.EXAMPLE)).orElseGet(Collections::emptyList);
    }

    @Override
    protected Predicate<MfTag> itemMatches(MfTag other) {
        return current -> current.getValue().equals(other.getValue()) || current.getId().equals(other.getId());
    }

}
