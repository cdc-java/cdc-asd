package cdc.asd.specgen.diffhelpers;

import cdc.mf.model.MfPackage;
import cdc.mf.model.MfType;

public class ClassOrInterfaceDiffHelper extends DiffHelper<MfType, MfPackage> {

    public ClassOrInterfaceDiffHelper(MfPackage currentParent,
                                      MfPackage previousParent) {
        super(currentParent, previousParent, MfType.class);
    }

    public <T extends MfType> ClassOrInterfaceDiffHelper(MfPackage currentParent,
                                                         MfPackage previousParent,
                                                         Class<? extends T> type) {
        super(currentParent, previousParent, type);
    }
}
