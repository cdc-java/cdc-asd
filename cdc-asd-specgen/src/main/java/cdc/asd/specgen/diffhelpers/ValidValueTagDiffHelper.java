package cdc.asd.specgen.diffhelpers;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdTag;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;

public final class ValidValueTagDiffHelper extends DiffHelper<MfTag, MfProperty> {

    private final MfProperty currentProperty;

    public ValidValueTagDiffHelper(MfProperty currentProperty,
                                          MfProperty previousProperty) {
        super(currentProperty, previousProperty, MfTag.class);

        this.currentProperty = currentProperty;

    }

    @Override
    protected List<? extends MfTag> extractChildrenFromParent(Optional<MfProperty> parent,
                                                              Class<? extends MfTag> type) {
        return parent.map(p -> p.getTags(AsdTagName.VALID_VALUE))
                     .orElseGet(Collections::emptyList);
    }

    @Override
    protected Predicate<MfTag> itemMatches(MfTag other) {
        return current -> Optional.ofNullable(current.wrap(AsdTag.class)
                                                     .getNotes())
                                  .equals(Optional.ofNullable(other.wrap(AsdTag.class).getNotes()));
    }

    public final MfProperty getCurrentProperty() {
        return currentProperty;
    }

}
