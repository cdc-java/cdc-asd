package cdc.asd.specgen;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.asd.model.wrappers.AsdTag;
import cdc.asd.specgen.datamodules.AttributeGlossaryMember;
import cdc.asd.specgen.datamodules.AttributeGlossaryMember.ValidValue;
import cdc.asd.specgen.datamodules.DataModules;
import cdc.asd.specgen.datamodules.GenericGlossaryMember;
import cdc.asd.specgen.datamodules.GlossaryDataModule;
import cdc.asd.specgen.datamodules.GlossaryMember;
import cdc.asd.specgen.datamodules.GlossaryMember.ObjectType;
import cdc.asd.specgen.datamodules.PublicationModuleEntryForPublicationModule;
import cdc.asd.specgen.datamodules.SimpleDataModule;
import cdc.asd.specgen.datamodules.UofGlossaryMember;
import cdc.asd.specgen.formatter.Formatter;
import cdc.asd.specgen.formatter.FormatterContext;
import cdc.asd.specgen.formatter.FormattingBoundary;
import cdc.asd.specgen.formatter.GlossaryFormatterContext;
import cdc.asd.specgen.s1000d5.BrDoc;
import cdc.asd.specgen.s1000d5.BrLevelledPara;
import cdc.asd.specgen.s1000d5.ChangeMark;
import cdc.asd.specgen.s1000d5.ChangeMark.ChangeType;
import cdc.asd.specgen.s1000d5.DefinitionList;
import cdc.asd.specgen.s1000d5.NormalBrParaElem;
import cdc.asd.specgen.s1000d5.Note;
import cdc.asd.specgen.s1000d5.Para;
import cdc.asd.specgen.s1000d5.RandomList;
import cdc.asd.specgen.s1000d5.S1000DElementNode;
import cdc.asd.specgen.s1000d5.S1000DGenericElementNode;
import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;
import cdc.io.data.Document;
import cdc.io.data.Element;
import cdc.io.data.Text;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;
import cdc.mf.model.MfAbstractChildTaggedNamedElement;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;

/**
 * Utility for exporting an {@link MfModel} to S1000D by using
 * the brdoc XML schema for the purpose of SX001G (glossary spec).
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class S1000DGlossaryXmlIo {
    private S1000DGlossaryXmlIo() {
    }

    private static final String SECTION_TITLE_REFERENCES = "References";
    private static final String SECTION_TITLE_VV = "Valid values";
    private static final String SECTION_TITLE_EXAMPLES = "Examples";
    private static final String SECTION_TITLE_TYPE = "Type";
    private static final String SECTION_TITLE_REPLACES = "Replaces";
    private static final String REPLACES_CONTENT = "This term replaces the former term ";
    private static final int PARA_LEVEL_FOR_TERM = 0;

    private static final String HIGHLIGHT_TEXT_TERM = "term";
    private static final String HIGHLIGHT_TEXT_DEFINITION_OF_TERM = "definition of term";
    private static final String HIGHLIGHT_TEXT_LIST_OF_EXAMPLES = "list of examples";
    private static final String HIGHLIGHT_TEXT_LIST_OF_REFERENCES = "list of references";
    private static final String HIGHLIGHT_TEXT_NOTES = "notes";
    private static final String HIGHLIGHT_TEXT_LIST_OF_VALID_VALUES = "list of valid values";

    /*
     * TODO - Put in data model or in a properties file
     */
    private static final String GLOSSARY_SPEC_NAME = "SX001G";
    private static final LocalDate GLOSSARY_SPEC_ISSUE_DATE =
            LocalDate.of(2024, 4, 1);
    private static final int GLOSSARY_SPEC_DYNAMIC_CHAPTER_NUMBER = 2;
    private static final String GLOSSARY_SPEC_ISSUE_NUMBER = "001";
    private static final String GLOSSARY_SPEC_INWORK_NUMBER = "01";
    private static final String GLOSSARY_SPEC_LOGO_ICN = "ICN-B6865-SX001G0001-001-01";
    private static final String GLOSSARY_SPEC_PM_ENTRY_TITLE = "Chapter %s.%d";
    private static final String GLOSSARY_SPEC_FULL_TITLE = "Glossary for the S-Series IPS specifications";
    private static final String GLOSSARY_SPEC_INFONAME = "Glossary - %c";

    /*
     * Chapter 1 is static. DM have been written already, but just need to be referenced
     * by the publication module. TODO push that into application properties file or data model?
     */
    private static final List<TocEntry> staticDataModules =
            List.of(new TocEntry("Introduction", new SimpleDataModule(GLOSSARY_SPEC_LOGO_ICN,
                                                                      GLOSSARY_SPEC_NAME,
                                                                      "01", '0', '0', "INTR", "009",
                                                                      GLOSSARY_SPEC_ISSUE_DATE,
                                                                      "000", "01", "Introduction"),
                                 List.of(new TocEntry("Purpose", new SimpleDataModule(GLOSSARY_SPEC_LOGO_ICN,
                                                                                      GLOSSARY_SPEC_NAME,
                                                                                      "01", '0', '1', "PURP", "040",
                                                                                      GLOSSARY_SPEC_ISSUE_DATE,
                                                                                      "000", "01", "Purpose"),
                                                      List.of()),
                                         new TocEntry("Scope", new SimpleDataModule(GLOSSARY_SPEC_LOGO_ICN,
                                                                                    GLOSSARY_SPEC_NAME,
                                                                                    "01", '0', '2', "SCOP", "040",
                                                                                    GLOSSARY_SPEC_ISSUE_DATE,
                                                                                    "000", "01", "Scope"),
                                                      List.of()),
                                         new TocEntry("How to use the specification", new SimpleDataModule(GLOSSARY_SPEC_LOGO_ICN,
                                                                                                           GLOSSARY_SPEC_NAME,
                                                                                                           "01", '0', '3', "HOWT",
                                                                                                           "040",
                                                                                                           GLOSSARY_SPEC_ISSUE_DATE,
                                                                                                           "000", "01",
                                                                                                           "How to use the specification"),
                                                      List.of()),
                                         new TocEntry("Maintenance of the specification",
                                                      new SimpleDataModule(GLOSSARY_SPEC_LOGO_ICN,
                                                                           GLOSSARY_SPEC_NAME,
                                                                           "01", '0', '4',
                                                                           "MAIN", "040",
                                                                           GLOSSARY_SPEC_ISSUE_DATE,
                                                                           "000", "01",
                                                                           "Maintenance of the specification"),
                                                      List.of()))),
                    new TocEntry("Glossary", new SimpleDataModule(GLOSSARY_SPEC_LOGO_ICN,
                                                                  GLOSSARY_SPEC_NAME,
                                                                  "02", '0', '0', "GLOS", "009",
                                                                  GLOSSARY_SPEC_ISSUE_DATE,
                                                                  "000", "01", "Glossary"),
                                 List.of()));

    /**
     * A main Table of Content entry (utility class to export into
     * publication module)
     *
     * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
     *
     */
    static final class TocEntry {
        /**
         * The title of the ToC entry
         */
        public final String entryTitle;

        /**
         * The data module related to this ToC entry
         */
        public final SimpleDataModule relatedDataModule;

        /**
         * Child ToC entries (subchapters)
         */
        public final List<TocEntry> tocEntries;

        /**
         * Constructor
         *
         * @param entryTitle The title of the ToC entry
         * @param relatedDataModule The data module related to this ToC entry
         * @param childEntries Child ToC entries (subchapters). May be empty.
         */
        public TocEntry(String entryTitle,
                        SimpleDataModule relatedDataModule,
                        List<TocEntry> childEntries) {
            this.entryTitle = entryTitle;
            this.relatedDataModule = relatedDataModule;
            this.tocEntries = List.copyOf(childEntries);
        }
    }

    /**
     * Save into file the {@link MfModel} serialized in S1000D files.
     *
     * @param pathToSave The path for saving the
     *            generated files.
     * @param baseName The base name of the
     *            generated files. May be blank.
     * @param currentModel The {@link MfModel} to serialize.
     * @param previousModel The previous {@link MfModel} so we can compare and insert
     *            change marks. May be <code>null</code>.
     * @throws IOException When an IO error occurs.
     */
    public static void save(File pathToSave,
                            String baseName,
                            MfModel currentModel,
                            MfModel previousModel) throws IOException {

        final MfPackage rootDataModelPackage = EaModelIoUtils.getRootDataModelPackage(currentModel);

        if (rootDataModelPackage == null) {
            throw new IOException("No suitable root package was found");
        }

        final MfPackage previousRootDataModelPackage = EaModelIoUtils.getRootDataModelPackage(previousModel);
        final MfPackage dataModelPackage = EaModelIoUtils.getDataModelPackage(rootDataModelPackage);

        if (dataModelPackage == null) {
            throw new IOException("No suitable data model package was found inside "
                    + "of root package " + rootDataModelPackage.getName());
        }

        final List<MfPackage> uofList =
                EaModelIoUtils.getUofList(dataModelPackage,
                                          EaModelIoUtils.getPrimitivesPackage(rootDataModelPackage),
                                          EaModelIoUtils.getCompoundAttributesPackage(rootDataModelPackage));

        final List<MfPackage> previousUofList =
                EaModelIoUtils.getUofList(EaModelIoUtils.getDataModelPackage(previousRootDataModelPackage),
                                          EaModelIoUtils.getCompoundAttributesPackage(previousRootDataModelPackage),
                                          EaModelIoUtils.getPrimitivesPackage(previousRootDataModelPackage));

        final Set<GlossaryMember> previousGlossaryMembers = getGlossaryMembers(previousUofList, null);

        final Set<GlossaryMember> currentGlossaryMembers =
                getGlossaryMembers(uofList,
                                   previousModel).parallelStream()
                                                 .map(populatePreviousIssueReferences(previousGlossaryMembers))
                                                 .map(setChangeTypeIfPreviousModelPresent(previousModel))
                                                 .map(populateReferences(getGlossaryMembers(uofList,
                                                                                            previousModel).parallelStream()
                                                                                                          .map(populatePreviousIssueReferences(previousGlossaryMembers))
                                                                                                          .collect(Collectors.toSet())))
                                                 .collect(Collectors.toSet());

        final Set<GlossaryMember> allGlossaryMembers =
                Stream.concat(currentGlossaryMembers.stream(),
                              Stream.concat(previousGlossaryMembers.parallelStream()
                                                                   .filter(Predicate.not(currentGlossaryMembers::contains))
                                                                   .map(populateReferences(previousGlossaryMembers))
                                                                   .map(member -> member.changeMark(new ChangeMark(ChangeType.DELETED))),
                                            previousGlossaryMembers.parallelStream()
                                                                   .filter(p -> currentGlossaryMembers.parallelStream()
                                                                                                      .filter(p::equals)
                                                                                                      .findFirst()
                                                                                                      .filter(current -> current.getChangeMark()
                                                                                                                                .getChangeType() == ChangeType.ADDED)
                                                                                                      .isPresent())
                                                                   .map(populateReferences(previousGlossaryMembers))
                                                                   .map(member -> member.changeMark(new ChangeMark(ChangeType.REPLACED)))))
                      .collect(Collectors.toSet());

        final List<GlossaryDataModule> dataModules = new ArrayList<>(1 + 'Z' - 'A');

        for (char i = 'A'; i <= 'Z'; i++) {
            final GlossaryDataModule glossaryDataModule =
                    new GlossaryDataModule(GLOSSARY_SPEC_LOGO_ICN,
                                           GLOSSARY_SPEC_NAME,
                                           GLOSSARY_SPEC_DYNAMIC_CHAPTER_NUMBER,
                                           String.valueOf(i) + "000",
                                           GLOSSARY_SPEC_ISSUE_DATE,
                                           GLOSSARY_SPEC_ISSUE_NUMBER,
                                           GLOSSARY_SPEC_INWORK_NUMBER, i,
                                           allGlossaryMembers.parallelStream()
                                                             .filter(GlossaryMember.hasFirstLetter(i))
                                                             .collect(Collectors.toSet()));
            dataModules.add(glossaryDataModule);
        }

        writeDataModulesToDisk(dataModules, allGlossaryMembers, pathToSave, baseName);

        writePublicationModuleToDisk(dataModules, pathToSave, baseName);
    }

    /**
     * Get a {@link UnaryOperator} for a function that takes as input a {@link GlossaryMember}, and that
     * outputs a new {@link GlossaryMember} with its previous issues' references section populated.
     * If the input {@link GlossaryMember} has no previous issue, then the same {@link GlossaryMember} member
     * will be returned.
     *
     * @param previousGlossaryMembers A collection of previous {@link GlossaryMember}s that will be used to populate
     *            the references section.
     * @return A {@link UnaryOperator} for a function implementing the above behavior
     */
    private static UnaryOperator<GlossaryMember>
            populatePreviousIssueReferences(Collection<GlossaryMember> previousGlossaryMembers) {
        return member -> {
            if (member.getPreviousIssue() == null) {
                return member;
            }

            return member.previousIssue(Stream.of(member.getPreviousIssue())
                                              .map(populateReferences(previousGlossaryMembers))
                                              .findFirst()
                                              .get());
        };
    }

    /**
     * Recursively get all the relevant children of an {@link MfElement}.
     *
     * @param element The element for which to retrieve the children.
     * @param mapper The mapper function (intended for use as part of
     *            {@link Stream#mapMulti(BiConsumer)}).
     */
    private static void getAllRelevantChildren(MfElement element,
                                               Consumer<MfElement> mapper) {
        for (final MfElement child : element.getChildren()) {
            getAllRelevantChildren(child, mapper);

            if (Set.of(MfClass.class,
                       MfInterface.class,
                       MfProperty.class,
                       MfPackage.class)
                   .contains(child.getClass())) {
                mapper.accept(child);
            }
        }
    }

    /**
     * Extract a list of UoF from a data model package.
     *
     * @param uofList The package that contains all the UoFs of the data model. May be <code>null</code>.
     * @param previousModel The previous version of the data model for comparison. May be <code>null</code>.
     * @return A set of glossary members.
     */
    private static Set<GlossaryMember> getGlossaryMembers(List<MfPackage> uofList,
                                                          MfModel previousModel) {
        if (uofList == null) {
            return Set.of();
        }

        final Set<MfElement> previousModelElements =
                Optional.ofNullable(previousModel)
                        .map(MfModel::getChildren)
                        .filter(Predicate.not(Collection::isEmpty))
                        .stream()
                        .flatMap(List::stream)
                        .mapMulti(S1000DGlossaryXmlIo::getAllRelevantChildren)
                        .collect(Collectors.toSet());

        return Stream.concat(uofList.parallelStream()
                                    .filter(pack -> pack.wrap(AsdPackage.class).isUof())
                                    .map(p -> uofPackageToGlossaryMember(p,
                                                                         (MfPackage) getByNameAndKindOrId(previousModelElements,
                                                                                                          p.getId(),
                                                                                                          EaModelIoUtils.getSimpleUofName(p.getName()),
                                                                                                          p.getKind()))),
                             Stream.concat(uofList.parallelStream()
                                                  .map(MfPackage::getChildren)
                                                  .flatMap(List::parallelStream)
                                                  .filter(S1000DGlossaryXmlIo::isGlossaryItem)
                                                  .map(MfAbstractChildTaggedNamedElement.class::cast)
                                                  .map(m -> uofMemberToGlossaryMember(m,
                                                                                      (MfAbstractChildTaggedNamedElement<?>) getByNameAndKindOrId(previousModelElements,
                                                                                                                                       m.getId(),
                                                                                                                                       m.getName(),
                                                                                                                                       m.getKind()))),
                                           uofList.parallelStream()
                                                  .map(MfPackage::getChildren)
                                                  .flatMap(List::parallelStream)
                                                  .filter(MfClass.class::isInstance)
                                                  .map(MfClass.class::cast)
                                                  .map(MfClass::getProperties)
                                                  .flatMap(List::parallelStream)
                                                  .map(a -> attributeToGlossaryMember(a,
                                                                                      (MfProperty) getByNameAndKindOrId(previousModelElements,
                                                                                                                        a.getId(),
                                                                                                                        a.getName(),
                                                                                                                        a.getKind())))))
                     .collect(Collectors.toSet());
    }

    /**
     * Find, within a given {@link Set}, any MfElement whose name and kind match
     * the ones provided. If there is no match, attempt to match with model ID.
     *
     * @param modelElements A {@link Set} that contains {@link MfElement} objects. May
     *            be <code>null</code>.
     * @param id The id to look up. May be <code>null</code>.
     * @param name The name to look up. May be <code>null</code>.
     * @param kind The kind to look up in conjunction with the name. May be <code>null</code>.
     * @return The {@link MfElement} found or <code>null</code>.
     */
    private static MfElement getByNameAndKindOrId(Set<MfElement> modelElements,
                                                  String id,
                                                  String name,
                                                  String kind) {
        if (Optional.ofNullable(modelElements)
                    .filter(Predicate.not(Set::isEmpty))
                    .isEmpty()) {
            return null;
        }

        try {
            return modelElements.parallelStream()
                                .map(MfNameItem.class::cast)
                                .filter(element -> element.getName().equals(name) &&
                                        element.getKind().equals(kind))
                                .findFirst()
                                .get();
        } catch (final NoSuchElementException e) {
            return getById(modelElements, id);
        }
    }

    /**
     * Find, within a given {@link Set}, any MfElement whose name and kind match
     * the ones provided.
     *
     * @param modelElements A {@link Set} that contains {@link MfElement} objects.
     * @param id The id to look up.
     * @return The {@link MfElement} found or <code>null</code>.
     */
    private static MfElement getById(Set<MfElement> modelElements,
                                     String id) {
        return modelElements.parallelStream()
                            .filter(member -> member.getId().equals(id))
                            .findFirst()
                            .orElse(null);
    }

    /**
     * A unary operator that takes a {@link GlossaryMember} as input and returns a new {@link GlossaryMember}
     * with its references populated.
     *
     * @param referenceableItems Glossary members that can be referenced. Only the glossary members
     *            whose name matches in {@link #nameMatchesReferenceableItem(GlossaryMember, Collection)} will
     *            be referenced.
     * @return A {@link UnaryOperator} that returns a {@link GlossaryMember} with its
     *         references populated.
     */
    private static UnaryOperator<GlossaryMember> populateReferences(Collection<GlossaryMember> referenceableItems) {
        return glossaryMember -> glossaryMember.references(referenceableItems.parallelStream()
                                                                             .filter(g -> g.isClass() || g.isInterface())
                                                                             .map(GlossaryMember::getName)
                                                                             .filter(nameMatchesReferenceableItem(glossaryMember,
                                                                                                                  referenceableItems))
                                                                             .collect(Collectors.toSet()))
                                               .references(glossaryMember.isAttribute()
                                                       ? Set.of(((AttributeGlossaryMember) glossaryMember).getParentClassName())
                                                       : Set.of());
    }

    /**
     * Take a {@link String} and return <code>true</code> if it matches anything in the supplied
     * {@link GlossaryMember}'s definition.
     * Before attempting to match, remove from the string any name of a UoF, to avoid garbage referencing.
     * The name of the UoFs are determined with {@link UofGlossaryMember#getName()} and
     * {@link UofGlossaryMember#getNormalizedUofName()}.
     *
     * @param glossaryMember The {@link GlossaryMember} whose definition will be scanned for matches.
     * @param allGlossaryMembers All the {@link GlossaryMember}s.
     * @return A {@link Predicate} that returns <code>true</code> in case of a match or
     *         <code>false</code> otherwise.
     */
    private static Predicate<String> nameMatchesReferenceableItem(GlossaryMember glossaryMember,
                                                                  Collection<GlossaryMember> allGlossaryMembers) {
        return name -> {
            if (glossaryMember.getDefinition() == null) {
                return false;
            }

            if (glossaryMember.getName().equals(name)) {
                return false;
            }

            if (glossaryMember.getDefinition().contains(name)) {
                String definitionCleaned = glossaryMember.getDefinition();

                for (final UofGlossaryMember uofGlossaryMember : allGlossaryMembers.parallelStream()
                                                                                   .filter(UofGlossaryMember.class::isInstance)
                                                                                   .map(UofGlossaryMember.class::cast)
                                                                                   .toList()) {

                    definitionCleaned =
                            definitionCleaned.replace(uofGlossaryMember.getName(), "")
                                             .replace(uofGlossaryMember.getNormalizedUofName(), "");

                }

                if (definitionCleaned.matches(".*\\b" + name + "\\b.*")) {
                    return true;
                }
            }

            return false;
        };
    }

    /**
     * Write into disk a Publication Module, on the basis of the supplied
     * Data Modules, to the specified file path. Name file according to the
     * specified base name.
     *
     * @param dataModules The Data Modules that will be listed in target Publication Module
     * @param pathToSave The target file path
     * @param baseName A base name for the new file
     * @throws IOException When an IO error occurs
     */
    private static void writePublicationModuleToDisk(List<GlossaryDataModule> dataModules,
                                                     File pathToSave,
                                                     String baseName) throws IOException {

        final PublicationModuleEntryForPublicationModule pm =
                new PublicationModuleEntryForPublicationModule(GLOSSARY_SPEC_LOGO_ICN,
                                                               GLOSSARY_SPEC_NAME,
                                                               GLOSSARY_SPEC_ISSUE_DATE,
                                                               GLOSSARY_SPEC_FULL_TITLE,
                                                               0, 0, dataModules);

        try (final XmlWriter writer = new XmlWriter(new File(pathToSave,
                                                             baseName +
                                                                     pm.getPublicationModuleCode() +
                                                                     ".xml"))) {

            writer.setIndentString("  ");
            writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);

            final Document document = EaModelIoUtils.loadFmPmFromStaticDocument();

            populateStaticPmDocumentAttributes(document, pm);

            for (final GlossaryDataModule dataModule : dataModules) {
                createPmEntries(document, dataModule);
            }

            EaModelIoUtils.createDtd(document);

            XmlDataWriter.write(writer, document);

            writer.flush();
        }

    }

    /**
     * Populate static elements/attributes from static Publication Module
     * document.
     *
     * @param document The target document
     * @param pm The source Publication Module
     */
    private static void populateStaticPmDocumentAttributes(Document document,
                                                           PublicationModuleEntryForPublicationModule pm) {
        EaModelIoUtils.populateStaticPmDocumentAttributes(document, pm);

        final Element pmEntry =
                document.getElementNamed(S1000DNode.PM)
                        .getElementNamed(S1000DNode.CONTENT)
                        .getElementNamed(S1000DNode.PMENTRY);

        pmEntry.removeChild(pmEntry.getElementNamed(S1000DNode.PMENTRY));
        pmEntry.removeChild(pmEntry.getElementNamed(S1000DNode.DMREF));

        pmEntry.getElementNamed(S1000DNode.PMENTRYTITLE)
               .removeTexts();
        pmEntry.getElementNamed(S1000DNode.PMENTRYTITLE)
               .addText(GLOSSARY_SPEC_FULL_TITLE);

        pmEntry.addAttribute(S1000DNode.PMENTRYTYPE, "pmt13");

        populatePmEntries(pmEntry, staticDataModules);

        document.getElementNamed(S1000DNode.PM)
                .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                .getElementNamed(S1000DNode.PMSTATUS)
                .getElementNamed(S1000DNode.LOGO)
                .getElementNamed(S1000DNode.SYMBOL)
                .getAttribute(S1000DNode.INFOENTITYIDENT)
                .setValue(GLOSSARY_SPEC_LOGO_ICN);
    }

    /**
     * Recursively populate a PM entry {@link Element} on the basis of a
     * supplied list of {@link TocEntry}.
     *
     * @param pmEntry An S1000D {@code pmEntry} element. It will be appended
     *            with one or several additional nodes.
     * @param entries A list of ToC entries to add into the supplied {@code pmEntry}.
     */
    private static void populatePmEntries(Element pmEntry,
                                          List<TocEntry> entries) {
        for (final TocEntry entry : entries) {
            final Element pmEntryChapt = new Element(pmEntry, S1000DNode.PMENTRY);
            final Element pmEntryChaptTitle = new Element(pmEntryChapt, S1000DNode.PMENTRYTITLE);
            pmEntryChaptTitle.addText(entry.entryTitle);
            pmEntryChapt.addChild(DataModules.createDmRef(entry.relatedDataModule));

            if (!entry.tocEntries.isEmpty()) {
                populatePmEntries(pmEntryChapt, entry.tocEntries);
            }
        }
    }

    /**
     * Create Publication Module entries into the target document,
     * based on the supplied glossary data module.
     *
     * @param document The target document
     * @param dm The source glossary data module
     */
    private static void createPmEntries(Document document,
                                        GlossaryDataModule dm) {
        final Element pmEntry =
                document.getElementNamed(S1000DNode.PM)
                        .getElementNamed(S1000DNode.CONTENT)
                        .getElementNamed(S1000DNode.PMENTRY)
                        .getElementNamedAt(S1000DNode.PMENTRY, 1)
                        .addElement(S1000DNode.PMENTRY);

        pmEntry.addElement(S1000DNode.PMENTRYTITLE)
               .addText(dm.getDmTitle());

        pmEntry.addChild(DataModules.createDmRef(dm));
    }

    /**
     * Write glossary data modules to disk.
     *
     * @param dataModules The list of input data modules.
     * @param glossaryMembers The glossary members so we can apply formatting rules on them
     * @param pathToSave The target file path
     * @param baseName A base name for the output files
     * @throws IOException Write error
     */
    private static void writeDataModulesToDisk(List<GlossaryDataModule> dataModules,
                                               Set<GlossaryMember> glossaryMembers,
                                               File pathToSave,
                                               String baseName) throws IOException {
        final Map<String, GlossaryMember> termsMap =
                dataModules.parallelStream()
                           .map(dm -> dm.getMembers()
                                        .parallelStream()
                                        .collect(HashMap::new, combineIfAbsent(dm), Map::putAll))
                           .collect(HashMap::new, Map::putAll, Map::putAll);

        final List<String> uofList =
                Stream.concat(glossaryMembers.parallelStream()
                                             .filter(UofGlossaryMember.class::isInstance)
                                             .map(UofGlossaryMember.class::cast)
                                             .map(UofGlossaryMember::getNormalizedUofName),
                              glossaryMembers.parallelStream()
                                             .filter(UofGlossaryMember.class::isInstance)
                                             .map(UofGlossaryMember.class::cast)
                                             .map(UofGlossaryMember::getName))
                      .toList();

        for (final GlossaryDataModule dataModule : dataModules) {

            try (final XmlWriter writer = new XmlWriter(new File(pathToSave,
                                                                 baseName +
                                                                         dataModule.getDataModuleCode() +
                                                                         ".xml"))) {
                writer.setIndentString("  ");
                writer.setEnabled(XmlWriter.Feature.PRETTY_PRINT);

                final Document document = EaModelIoUtils.loadBrdocFromStaticDocument();

                populateStaticBrdocDocumentAttributes(document, dataModule);

                exportDataModuleToDocument(document, dataModule, uofList, termsMap);

                exportReasonForUpdateToDocument(document, dataModule);

                EaModelIoUtils.createDtd(document);

                XmlDataWriter.write(writer, document);

                writer.flush();
            }
        }
    }

    /**
     * Export the ReasonForUpdate (RFU) section into a target {@link Document}.
     *
     * @param document The target {@link Document} which will be modified to include
     *            the RFU section.
     * @param dataModule The data module whose {@link GlossaryMember}s' {@link ChangeMark}s
     *            will be appended to the RFU section.
     */
    private static void exportReasonForUpdateToDocument(Document document,
                                                        GlossaryDataModule dataModule) {

        final Element dmStatus = document.getElementNamed(S1000DNode.DMODULE)
                                         .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                                         .getElementNamed(S1000DNode.DMSTATUS);

        for (final Map.Entry<String, List<GlossaryMember>> m : dataModule.getMembers()
                                                                         .stream()
                                                                         .filter(GlossaryMember::hasAnyChangeMark)
                                                                         .collect(Collectors.groupingBy(GlossaryMember::getRfuId))
                                                                         .entrySet()) {
            createReasonForUpdateSection(m.getValue()).setParent(dmStatus);
        }
    }

    /**
     * Create a ReasonForUpdate (RFU) element, on the basis of a list of {@link GlossaryMember}s
     * with {@link ChangeMark} object.
     *
     * @param members The {@link GlossaryMember} objects. They must all have the same ReasonForUpdate
     *            ID.
     * @return A RFU section, or an empty RFU section if the supplied {@link GlossaryMember}s do
     *         not have any {@link ChangeMark} (or the {@link ChangeMark}'s {@link ChangeType} is
     *         {@link ChangeType#NONE} or {@link ChangeType#UNDETERMINED}).
     * @throws IllegalArgumentException If the supplied list of {@link GlossaryMember} have several
     *             RFU IDs.
     */
    private static Element createReasonForUpdateSection(List<GlossaryMember> members) {

        if (members.stream().collect(Collectors.groupingBy(GlossaryMember::getRfuId)).size() > 1) {
            throw new IllegalArgumentException("This function cannot be used with several "
                    + GlossaryMember.class.getCanonicalName() + " objects with different RFU IDs.");
        }

        final Element rfuSection = new Element(S1000DNode.REASONFORUPDATE);

        rfuSection.addAttribute(S1000DNode.ID, members.get(0).getRfuId());
        rfuSection.addAttribute(S1000DNode.UPDATEREASONTYPE, S1000DNode.UPDATEREASONTYPE_VALUE);
        rfuSection.addAttribute(S1000DNode.UPDATEHIGHLIGHT, "1");

        for (final GlossaryMember member : members) {
            Stream<Map.Entry<String, ChangeMark>> changeTypes =
                    Stream.of(Map.entry(HIGHLIGHT_TEXT_TERM, member.getChangeMark()),
                              Map.entry(HIGHLIGHT_TEXT_DEFINITION_OF_TERM, member.getDefinitionChangeMark()),
                              Map.entry(HIGHLIGHT_TEXT_LIST_OF_EXAMPLES, member.getExamplesChangeMark()),
                              Map.entry(HIGHLIGHT_TEXT_LIST_OF_REFERENCES, member.getReferencesChangeMark()),
                              Map.entry(HIGHLIGHT_TEXT_NOTES, member.getNotesChangeMark()));

            if (member instanceof final AttributeGlossaryMember attributeMember) {
                changeTypes = Stream.concat(changeTypes,
                                            Stream.of(Map.entry(HIGHLIGHT_TEXT_LIST_OF_VALID_VALUES,
                                                                attributeMember.getValidValuesChangeMark())));
            }

            for (final Element simplePara : changeTypes.filter(c -> !Set.of(ChangeType.NONE,
                                                                            ChangeType.UNDETERMINED)
                                                                        .contains(c.getValue()
                                                                                   .getChangeType()))
                                                       .map(S1000DGlossaryXmlIo::changeTypeToString)
                                                       .map(Text::new)
                                                       .map(text -> text.setParent(new Element(S1000DNode.SIMPLEPARA))
                                                                        .getParent(Element.class))
                                                       .toList()) {
                simplePara.setParent(rfuSection);
            }
        }

        return rfuSection;
    }

    /**
     * Convert a pair of {@code String} and {@link ChangeMark} into a nice sentence.
     * The string is the sentence's body and designates the element that underwent changes;
     * the {@link ChangeMark}'s {@link ChangeType} contains the action ("Added", "Deleted",
     * etc.).
     *
     * @param pair A pair with a string and a {@link ChangeMark}. The {@link ChangeMark}
     *            <i>MUST NOT</i> have the {@link ChangeType}s {@link ChangeType#NONE} or
     *            {@link ChangeType#UNDETERMINED}.
     * @return A nice sentence for the change type.
     * @throws IllegalArgumentException If the {@link ChangeMark} does not have a suitable
     *             {@link ChangeType}.
     */
    private static String changeTypeToString(Map.Entry<String, ChangeMark> pair) {
        if (Set.of(ChangeType.NONE, ChangeType.UNDETERMINED).contains(pair.getValue().getChangeType())) {
            throw new IllegalArgumentException("This function must not be used with "
                    + ChangeType.NONE.name() + " or "
                    + ChangeType.UNDETERMINED.name());
        }

        return String.format("%s %s",
                             pair.getValue()
                                 .getChangeType()
                                 .getIntroductorySentence(),
                             pair.getKey());
    }

    /**
     * Return a {@link UnaryOperator} that turns a {@link GlossaryMember} into a new
     * {@link GlossaryMember} with a given {@link ChangeType}.
     * If the supplied <code>previousModel</code> is <code>null</code>, the new
     * {@link GlossaryMember}'s {@link ChangeType} will always be {@link
     * ChangeType#NONE} type to report because no previous model was supplied)
     * Otherwise, the new {@link GlossaryMember} will have its {@link ChangeType}
     * set to either {@link ChangeType#ADDED} or {@link ChangeType#NONE}.
     *
     * @param previousModel The previous model. May be <code>null</code>.
     * @return A {@link UnaryOperator}
     */
    private static UnaryOperator<GlossaryMember> setChangeTypeIfPreviousModelPresent(MfModel previousModel) {
        if (previousModel == null) {
            return member -> member.changeMark(new ChangeMark());
        }

        return member -> member.changeMark(Optional.ofNullable(member.getPreviousIssue())
                                                   .filter(member::equals)
                                                   .map(p -> new ChangeMark())
                                                   .orElse(new ChangeMark(ChangeType.ADDED)));
    }

    /**
     * Shorthand expression for {@code (map, member) -> map.putIfAbsent(member.getName(),
     * dataModule)}
     *
     * @param dataModule The data module.
     * @return A function that puts into the supplied map a new entry. Does nothing if
     *         the entry is already there.
     */
    private static BiConsumer<Map<String, GlossaryMember>, GlossaryMember> combineIfAbsent(GlossaryDataModule dataModule) {
        return (map,
                member) -> map.putIfAbsent(member.getName(), member);
    }

    /**
     * Export a {@link GlossaryDataModule} into a {@link Document}.
     *
     * @param document The {@code Document} where to export the glossary data module.
     * @param glossaryDataModule The input {@code GlossaryDataModule}.
     * @param uofList The list of UoFs for formatting. Can be empty.
     * @param termsMap A map linking terms to glossary data modules. This allows us to link them
     *            properly across data modules (i.e. by using S1000D internalRef or dmRef).
     * @throws IOException Write error.
     */
    private static void exportDataModuleToDocument(Document document,
                                                   GlossaryDataModule glossaryDataModule,
                                                   List<String> uofList,
                                                   Map<String, GlossaryMember> termsMap) throws IOException {

        final BrDoc brDoc =
                new BrDoc().brLevelledParas(glossaryDataModule.getMembers()
                                                              .stream()
                                                              .map(S1000DGlossaryXmlIo::glossaryMemberToS1000D)
                                                              .toList());

        populateWithElements(document.getElementNamed(S1000DNode.DMODULE)
                                     .getElementNamed(S1000DNode.CONTENT),
                             brDoc,
                             new GlossaryFormatterContext(termsMap, uofList));
    }

    /**
     * Convert a {@link GlossaryMember} into a well-groomed {@link BrLevelledPara}.
     *
     * @param member The member.
     * @return A new {@code BrLevelledPara}.
     */
    private static BrLevelledPara glossaryMemberToS1000D(GlossaryMember member) {
        final GlossaryMember previousIssue = member.getPreviousIssue();

        BrLevelledPara definitionParagraph =
                new BrLevelledPara(PARA_LEVEL_FOR_TERM).id(GlossaryMember.getId(member))
                                                       .title(member.getName())
                                                       .changeMark(member.getChangeMark())
                                                       .para(new Para(member.getDefinition(),
                                                                      Set.of(FormattingPolicy.GLOSSARY_KEYWORD)).changeMark(member.getDefinitionChangeMark()));

        if (!member.getNotes().isEmpty()) {
            final ChangeMark c = Optional.ofNullable(previousIssue)
                                         .map(GlossaryMember::getNotes)
                                         .map(l -> new ChangeMark(l.isEmpty() ? ChangeType.ADDED : ChangeType.MODIFIED,
                                                                  member.getChangeMark().getReasonForUpdateRefIds()))
                                         .orElse(new ChangeMark());

            for (final String note : member.getNotes()) {
                definitionParagraph = definitionParagraph.para(new Note().noteParaString(List.of(note))
                                                                         .formattingPolicies(Set.of(FormattingPolicy.GLOSSARY_KEYWORD))
                                                                         .changeMark(c));
            }
        }

        if (!member.getReferences().isEmpty()) {
            definitionParagraph =
                    definitionParagraph.para(createReferencesSection(member).changeMark(member.getReferencesChangeMark()));
        } else if (Optional.ofNullable(previousIssue)
                           .map(GlossaryMember::getReferences)
                           .filter(Predicate.not(Set::isEmpty))
                           .isPresent()) {
            definitionParagraph =
                    definitionParagraph.para(createReferencesSection(previousIssue).changeMark(new ChangeMark(ChangeType.DELETED,
                                                                                                              member.getChangeMark()
                                                                                                                    .getReasonForUpdateRefIds())));
        }

        if (member instanceof final AttributeGlossaryMember attributeMember) {
            if (!attributeMember.getValidValues().isEmpty()) {
                definitionParagraph =
                        definitionParagraph.para(createValidValuesSection(attributeMember).changeMark(attributeMember.getValidValuesChangeMark()));
            } else if (Optional.ofNullable(previousIssue)
                               .map(AttributeGlossaryMember.class::cast)
                               .map(AttributeGlossaryMember::getValidValues)
                               .filter(Predicate.not(List::isEmpty))
                               .isPresent()) {
                definitionParagraph =
                        definitionParagraph.para(createValidValuesSection(attributeMember.getPreviousIssue()
                                                                                         .changeMark(new ChangeMark(ChangeType.DELETED,
                                                                                                                    member.getChangeMark()
                                                                                                                          .getReasonForUpdateRefIds()))));
            }
        }

        if (!member.getExamples().isEmpty()) {
            definitionParagraph =
                    definitionParagraph.para(createExamplesSection(member).changeMark(member.getExamplesChangeMark()));
        } else if (Optional.ofNullable(previousIssue)
                           .map(GlossaryMember::getExamples)
                           .filter(Predicate.not(List::isEmpty))
                           .isPresent()) {
            definitionParagraph =
                    definitionParagraph.para(createExamplesSection(previousIssue).changeMark(new ChangeMark(ChangeType.DELETED,
                                                                                                            member.getChangeMark()
                                                                                                                  .getReasonForUpdateRefIds())));
        }

        if (member.getUmlType() != null) {
            NormalBrParaElem type;
            if (member instanceof final AttributeGlossaryMember agm) {

                type = randomList(List.of("CDM " + agm.getTypeName()),
                                  SECTION_TITLE_TYPE,
                                  Set.of(FormattingPolicy.CLASS_VERBATIM));
            } else {
                type = randomList(List.of(member.getUmlType()), SECTION_TITLE_TYPE, Set.of());
            }

            definitionParagraph =
                    definitionParagraph.para(type);
        }

        if (member instanceof final GenericGlossaryMember g) {
            if (g.getReplaces() != null) {
                final ChangeMark c = Optional.ofNullable(previousIssue)
                                             .map(GenericGlossaryMember.class::cast)
                                             .map(previous -> {
                                                 final ChangeMark cm = g.getChangeMark();

                                                 if (Set.of(ChangeType.NONE, ChangeType.UNDETERMINED)
                                                        .contains(cm.getChangeType())) {

                                                     if (previous.getReplaces() == null) {
                                                         return new ChangeMark(ChangeType.ADDED,
                                                                               g.getChangeMark()
                                                                                .getReasonForUpdateRefIds());
                                                     } else if (previous.getReplaces().equals(g.getReplaces())) {
                                                         return new ChangeMark();
                                                     } else {
                                                         return new ChangeMark(ChangeType.MODIFIED, cm.getReasonForUpdateRefIds());
                                                     }
                                                 } else {
                                                     return new ChangeMark();
                                                 }
                                             })
                                             .orElse(new ChangeMark());

                definitionParagraph =
                        definitionParagraph.para(createReplacesSection(g).stream().map(e -> e.changeMark(c)).toList());
            } else if (previousIssue != null && g.getPreviousIssue().getReplaces() != null) {
                definitionParagraph =
                        definitionParagraph.para(createReplacesSection(g.getPreviousIssue()).stream()
                                                                                            .map(e -> e.changeMark(new ChangeMark(ChangeType.DELETED,
                                                                                                                                  g.getChangeMark()
                                                                                                                                   .getReasonForUpdateRefIds())))
                                                                                            .toList());
            }
        }

        return definitionParagraph;
    }

    private static List<NormalBrParaElem> createReplacesSection(GenericGlossaryMember g) {
        return List.of(new Para(new S1000DGenericElementNode(S1000DNode.INLINESIGNIFICANTDATA).attribute(S1000DNode.SIGNIFICANTPARADATATYPE,
                                                                                                         S1000DNode.SIGNIFICANTPARADATATYPE_VALUE)
                                                                                              .child(new S1000DTextNode(SECTION_TITLE_REPLACES))),
                       new Para(new S1000DTextNode(REPLACES_CONTENT)).content(EaModelIoUtils.verbatimTextClass(g.getReplaces()))
                                                                     .content(new S1000DTextNode(".")));
    }

    private static Para createExamplesSection(GlossaryMember member) {
        return randomList(member.getExamples(),
                          SECTION_TITLE_EXAMPLES,
                          Set.of());
    }

    private static Para createValidValuesSection(AttributeGlossaryMember attributeMember) {
        return validValuesRandomList(attributeMember.getValidValues());
    }

    private static Para createReferencesSection(GlossaryMember member) {
        return randomList(member.getReferences()
                                .stream()
                                .sorted()
                                .toList(),
                          SECTION_TITLE_REFERENCES,
                          Set.of(FormattingPolicy.GLOSSARY_TERM_REFERENCE));
    }

    /**
     * Combines strings into an S1000D random list.
     *
     * @param elements The strings.
     * @param title The title of the random list.
     * @param formattingPolicies The formatting policies to apply to each string.
     * @return A {@link Para} with a random list and one list item per supplied string.
     */
    private static Para randomList(List<String> elements,
                                   String title,
                                   Set<FormattingPolicy> formattingPolicies) {
        return new Para(new RandomList(title).child(toListItem(elements, formattingPolicies)));
    }

    /**
     * Convert valid-values to a paragraph suitable for S1000D
     *
     * @param validValues The list of valid-values to convert.
     * @return A new {@code Para} with all valid-values.
     */
    private static Para validValuesRandomList(List<ValidValue> validValues) {
        return new Para(new DefinitionList(SECTION_TITLE_VV).child(validValues.stream()
                                                                              .map(vv -> EaModelIoUtils.formatValidValues(vv.code,
                                                                                                                          vv.value))
                                                                              .collect(Collectors.toList())));
    }

    /**
     * Shorthand to put a list of strings into an S1000D list item.
     *
     * @param element The input data.
     * @param formattingPolicies The formatting policies to apply.
     * @return A list of {@link S1000DNode}s.
     */
    private static List<S1000DNode> toListItem(List<String> element,
                                               Set<FormattingPolicy> formattingPolicies) {
        return element.parallelStream()
                      .map(Para::new)
                      .map(Para.formatWith(formattingPolicies))
                      .map(EaModelIoUtils::listItem)
                      .map(S1000DNode.class::cast)
                      .toList();
    }

    /**
     * Takes an {@link MfPackage} corresponding to a UoF as input and returns a
     * new {@link GlossaryMember}.
     *
     * @param uofPackage The input package. It should be a UoF package.
     * @param previousIssue The previous issue of the same package. May be <code>null</code>.
     *
     * @return A new {@code GlossaryMember}.
     */
    private static UofGlossaryMember uofPackageToGlossaryMember(MfPackage uofPackage,
                                                                MfPackage previousIssue) {
        return new UofGlossaryMember(uofPackage.getName(),
                                     uofPackage.wrap(AsdElement.class).getNotes(),
                                     Optional.ofNullable(previousIssue)
                                             .map(p -> new UofGlossaryMember(p.getName(),
                                                                             p.wrap(AsdElement.class)
                                                                              .getNotes(),
                                                                             null))
                                             .orElse(null));
    }

    /**
     * Takes an {@link MfAbstractChildTaggedNamedElement} as input and returns a
     * new {@link GlossaryMember} instance.
     *
     * @param uofMember A {@link MfAbstractChildTaggedNamedElement} UoF member (should be a class
     *            or an interface). May be <code>null</code>.
     * @param previousIssue The previous issue of that {@link MfAbstractChildTaggedNamedElement} UoF
     *            member. May be <code>null</code>.
     * @return A new {@code GlossaryMember} or <code>null</code> if input is <code>null</code>.
     */
    private static GenericGlossaryMember uofMemberToGlossaryMember(MfAbstractChildTaggedNamedElement<?> uofMember,
                                                                   MfAbstractChildTaggedNamedElement<?> previousIssue) {
        ObjectType objectType;
        String replaces = null;

        if (uofMember == null) {
            return null;
        }

        if (uofMember instanceof MfClass) {
            objectType = ObjectType.CLASS;
            replaces = uofMember.getTagValue("replaces");
        } else if (uofMember.wrap(AsdElement.class).isExtendInterface()) {
            objectType = ObjectType.EXTEND_INTERFACE;
            replaces = uofMember.getTagValue("replaces");
        } else if (uofMember.wrap(AsdElement.class).isSelectInterface()) {
            objectType = ObjectType.SELECT_INTERFACE;
            replaces = uofMember.getTagValue("replaces");
        } else {
            objectType = ObjectType.GENERIC_TERM;
        }

        return new GenericGlossaryMember(uofMember.getName(),
                                         uofMember.wrap(AsdElement.class)
                                                  .getNotes(),
                                         replaces, objectType,
                                         uofMemberToGlossaryMember(previousIssue,
                                                                   null)).notes(getTagsFor(uofMember,
                                                                                           AsdTagName.NOTE));
    }

    /**
     * Takes an {@link MfProperty} as input, and returns a {@link GlossaryMember}.
     *
     * @param attribute The attribute. May be <code>null</code>.
     * @param previousIssue The previous issue of that attribute. May be <code>null</code>.
     * @return A new {@code GlossaryMember} or <code>null</code> if the input is <code>null</code>.
     */
    private static AttributeGlossaryMember attributeToGlossaryMember(MfProperty attribute,
                                                                     MfProperty previousIssue) {
        if (attribute == null) {
            return null;
        }

        return new AttributeGlossaryMember(attribute.getName(),
                                           attribute.wrap(AsdElement.class).getNotes(),
                                           attribute.getParent().getName(),
                                           attribute.hasValidType()
                                                   ? attribute.getType().getName()
                                                   : "???",
                                           attributeToGlossaryMember(previousIssue, null))
                                                                                          .notes(getTagsFor(attribute,
                                                                                                            AsdTagName.NOTE))
                                                                                          .validValues(getValidValueTagsFor(attribute))
                                                                                          .examples(getTagsFor(attribute,
                                                                                                               AsdTagName.EXAMPLE));
    }

    /**
     * Create a list of valid-values for an {@link MfProperty}.
     *
     * @param attribute The attribute for which to get valid-values
     *            (they should have the {@link AsdTagName#VALID_VALUE} tag name).
     * @return A list of {@link ValidValue} objects.
     */
    private static List<ValidValue> getValidValueTagsFor(MfProperty attribute) {
        return attribute.getTags(AsdTagName.VALID_VALUE)
                        .stream()
                        .filter(t -> t.getValue() != null && t.wrap(AsdTag.class).getNotes() != null)
                        .map(t -> new ValidValue(t.getValue(),
                                                 t.wrap(AsdTag.class).getNotes()))
                        .toList();
    }

    /**
     * Get a list of tagged values of an {@link MfTagOwner}.
     *
     * @param taggedObject The {@link MfTagOwner}, from which to get the tagged values.
     * @param tagName The {@link AsdTagName} to extract.
     * @return The list of tagged values as strings.
     */
    private static List<String> getTagsFor(MfTagOwner taggedObject,
                                           AsdTagName tagName) {
        return taggedObject.getTags(tagName)
                           .stream()
                           .map(MfTag::getValue)
                           .toList();
    }

    /**
     * Populate a Data Module template with "top-level" values.
     *
     * @param document The document for the Data Module, that will be modified
     *            to include the static/top-level values.
     * @param dm The UoF that contains the top-level values (e.g.
     *            Spec title, spec issue date, etc.).
     */
    private static void populateStaticBrdocDocumentAttributes(Document document,
                                                              GlossaryDataModule dm) {
        final Element dmAddress = document.getElementNamed(S1000DNode.DMODULE)
                                          .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                                          .getElementNamed(S1000DNode.DMADDRESS);

        final Element dmCode = dmAddress.getElementNamed(S1000DNode.DMIDENT)
                                        .getElementNamed(S1000DNode.DMCODE);

        final Element issueDate = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                           .getElementNamed(S1000DNode.ISSUEDATE);

        final Element issueInfo = dmAddress.getElementNamed(S1000DNode.DMIDENT)
                                           .getElementNamed(S1000DNode.ISSUEINFO);

        final Element dmTitle = dmAddress.getElementNamed(S1000DNode.DMADDRESSITEMS)
                                         .getElementNamed(S1000DNode.DMTITLE);

        final Element dmStatus = document.getElementNamed(S1000DNode.DMODULE)
                                         .getElementNamed(S1000DNode.IDENTANDSTATUSSECTION)
                                         .getElementNamed(S1000DNode.DMSTATUS);

        dmCode.getAttribute(S1000DNode.MODELIDENTCODE).setValue(dm.getModelIdentCode());
        dmCode.getAttribute(S1000DNode.SYSTEMCODE).setValue(dm.getSystemCode());
        dmCode.getAttribute(S1000DNode.SUBSYSTEMCODE).setValue(String.valueOf(dm.getSubSystemCode()));
        dmCode.getAttribute(S1000DNode.SUBSUBSYSTEMCODE).setValue(String.valueOf(dm.getSubSubSystemCode()));
        dmCode.getAttribute(S1000DNode.ASSYCODE).setValue(dm.getAssyCode());

        dmTitle.getElementNamed(S1000DNode.TECHNAME)
               .addText(String.format(GLOSSARY_SPEC_PM_ENTRY_TITLE,
                                      dm.getChapterNumber(),
                                      dm.getPosition()));
        dmTitle.getElementNamed(S1000DNode.INFONAME)
               .addText(String.format(GLOSSARY_SPEC_INFONAME,
                                      dm.getGlossaryLetter()));

        issueDate.getAttribute(S1000DNode.ISSUEDATE_YEAR)
                 .setValue(String.valueOf(dm.getIssueDate()
                                            .getYear()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_MONTH)
                 .setValue(String.format("%02d",
                                         dm.getIssueDate()
                                           .getMonthValue()));
        issueDate.getAttribute(S1000DNode.ISSUEDATE_DAY)
                 .setValue(String.format("%02d",
                                         dm.getIssueDate()
                                           .getDayOfMonth()));
        issueInfo.getAttribute(S1000DNode.ISSUENUMBER).setValue(dm.getIssueNumber());
        issueInfo.getAttribute(S1000DNode.INWORK).setValue(dm.getInWork());

        dmStatus.getElementNamed(S1000DNode.LOGO)
                .getElementNamed(S1000DNode.SYMBOL)
                .getAttribute(S1000DNode.INFOENTITYIDENT)
                .setValue("ICN-B6865-SX001G0001-001-01"); // TODO hardcorded. Set ICN of logo
                                                          // in Data model/properties file

        dmStatus.getAttribute(S1000DNode.ISSUETYPE)
                .setValue(S1000DNode.ISSUETYPE_GLOSSARY_VALUE);

    }

    /**
     * Returns true for {@link MfElement} objects suitable for a glossary entry.
     *
     * @param obj The {@link MfElement} object.
     * @return {@code true} if object is suitable or {@code false}.
     */
    private static boolean isGlossaryItem(MfElement obj) {
        return obj.wrap(AsdElement.class).isClass()
                || obj.wrap(AsdElement.class).isExtendInterface()
                || obj.wrap(AsdElement.class).isSelectInterface();
    }

    /**
     * Populate an element with child elements based on the provided
     * {@link S1000DElementNode}, using the {@link FormatterContext}.
     * This function recursively descends the {@code S1000DElementNode}
     * elements and attempts to convert them into {@link Element} items.
     *
     * @param parent The parent element to populate.
     * @param s1000DItem The {@code S1000DElementNode} to descend, recursively,
     *            and append at the end of {@code parent}.
     * @param context The formatting context, required essentially for
     *            styling purposes.
     * @throws IOException When an IO error occurs.
     */
    public static void populateWithElements(Element parent,
                                            S1000DElementNode s1000DItem,
                                            GlossaryFormatterContext context) throws IOException {
        final Element element = new Element(parent, s1000DItem.getElementName());

        for (final Map.Entry<String, String> attribute : s1000DItem.getAttributes().entrySet()) {
            element.addAttribute(attribute.getKey(), attribute.getValue());
        }

        for (final S1000DNode node : s1000DItem.getChildren()) {
            if (node instanceof final S1000DElementNode elementNode) {
                if (node instanceof final BrLevelledPara brLevelledPara
                        && brLevelledPara.getLevel() == PARA_LEVEL_FOR_TERM) {
                    context = context.currentTerm(brLevelledPara.getTitle());
                }
                populateWithElements(element, elementNode, context);
            } else if (node instanceof final S1000DTextNode textNode) {
                final String description = textNode.getContent();

                if (description == null) {
                    continue;
                }

                final List<FormattingBoundary> formattingBoundaries = new ArrayList<>();

                for (final FormattingPolicy policy : textNode.getFormattingPolicies()) {
                    final Formatter formatter = Formatter.createFormatter(policy, context);

                    formattingBoundaries.addAll(formatter.getTextFormattingBoundaries(description));
                }

                Formatter.formatElement(element, description, formattingBoundaries);
            }
        }
    }
}