package cdc.asd.specgen.tags;

final class ClosingTag extends Tag {
    public ClosingTag(String name,
                      String fullTag,
                      int startPosition,
                      int endPosition) {
        super(name, fullTag, startPosition, endPosition);
    }
}