package cdc.asd.specgen.tags;

import java.util.Set;

/**
 * An {@link OpeningTag} that closes immediately, much like in XML.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
final class OpeningClosingTag extends OpeningTag {

    /**
     * Constructor.
     *
     * @param name Name of this tag, without the brackets and slashes.
     * @param fullTag The full name of this tag, with brackets/slashes.
     * @param startPosition The leading position of this tag in the source text.
     * @param endPosition The trailing position of this tag in the source text.
     */
    public OpeningClosingTag(String name,
                             String fullTag,
                             int startPosition,
                             int endPosition) {
        super(name, fullTag, startPosition, endPosition, Set.of());
        this.closingTag = this;
    }
}