package cdc.asd.specgen.tags;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;

/**
 * An Opening Tag is a tag that opens, and that optionally may close immediately
 * afterwards (which makes it an {@link OpeningClosingTag}).
 *
 * For convenience, some more data is assigned to this class, such as:
 * <ul>
 * <li>the relating closing tag;</li>
 * <li>its indenture;</li>
 * <li>the related S1000D node generated from it;</li>
 * <li>the child opening tags.</li>
 * </ul>
 *
 * All instances are mutable and not synchronized.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
class OpeningTag extends Tag {

    /**
     * The child opening tags inside this opening tag.
     */
    private final List<OpeningTag> children = new ArrayList<>();

    /**
     * The corresponding closing tag.
     */
    protected Tag closingTag = null;

    /**
     * The indenture of this tag.
     */
    private int indenture = -1;

    /**
     * The related S1000D node generated from this tag.
     */
    private S1000DNode relatedNode = null;

    /**
     * The formatting policies to enforce on all text nodes.
     */
    private final Set<FormattingPolicy> formattingPolicies;

    /**
     * Constructor.
     *
     * @param name Name of this tag, without the brackets and slashes.
     * @param fullTag The full name of this tag, with brackets/slashes.
     * @param startPosition The leading position of this tag in the source text.
     * @param endPosition The trailing position of this tag in the source text.
     * @param formattingPolicies The formatting policies to enforce on all text nodes.
     */
    public OpeningTag(String name,
                      String fullTag,
                      int startPosition,
                      int endPosition,
                      Set<FormattingPolicy> formattingPolicies) {
        super(name, fullTag, startPosition, endPosition);

        this.formattingPolicies = formattingPolicies;
    }

    /**
     * Get the related closing tag.
     *
     * @return The closing tag. May be {@code null} if not set yet.
     */
    public Tag getClosingTag() {
        return closingTag;
    }

    /**
     * Set the related closing tag.
     *
     * @param closingTag The closing tag.
     */
    public void setClosingTag(ClosingTag closingTag) {
        this.closingTag = closingTag;
    }

    /**
     * Get the indenture code.
     *
     * @return The indenture code or -1 if not set yet.
     */
    public int getIndenture() {
        return indenture;
    }

    /**
     * Set the indenture value.
     *
     * @param indenture The indenture value.
     */
    public void setIndenture(int indenture) {
        this.indenture = indenture;
    }

    /**
     * Add a child opening tag to this one.
     *
     * @param tag The tag.
     */
    public void addChild(OpeningTag tag) {
        children.add(tag);
    }

    /**
     * Set a related S1000D node, created from this tag.
     *
     * @param node The related S1000D node.
     */
    public void setRelatedNode(S1000DNode node) {
        this.relatedNode = node;
    }

    /**
     * Get the related S1000D node, created from this tag.
     *
     * @return The related S1000D node or {@code null} if not created yet.
     */
    public S1000DNode getRelatedNode() {
        return this.relatedNode;
    }

    /**
     * Get the child S1000D nodes. If it has no children, then the source text
     * inside the tag is wrapped inside an {@link S1000DTextNode}.
     *
     * @return The child S1000D nodes.
     */
    public List<S1000DNode> getChildren() {
        final ArrayList<S1000DNode> childNodes = new ArrayList<>();

        if (children.isEmpty()) {
            return List.of(new S1000DTextNode(stripTags(sourceText.substring(endPosition,
                                                                             closingTag.startPosition)),
                                              formattingPolicies));
        }

        int currentPosition = endPosition;
        final Tag lastChildClosingTag =
                children.get(children.size() - 1).closingTag;

        for (final OpeningTag child : children) {
            if (child.startPosition > currentPosition) {
                childNodes.add(new S1000DTextNode(sourceText.substring(currentPosition, child.startPosition)
                                                            .trim(),
                                                  formattingPolicies));
            }

            childNodes.add(child.relatedNode);

            currentPosition = child.closingTag.endPosition;
        }

        if (lastChildClosingTag.endPosition < closingTag.endPosition) {
            childNodes.add(new S1000DTextNode(sourceText.substring(lastChildClosingTag.endPosition,
                                                                   closingTag.startPosition)
                                                        .trim(),
                                              formattingPolicies));
        }

        return childNodes;
    }
}