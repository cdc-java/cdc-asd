package cdc.asd.specgen.tags;

import java.util.regex.Pattern;

/**
 * Abstract, top-level class for tags.
 * A Tag can be an {@link OpeningTag}, a {@link ClosingTag}, or a
 * {@link OpeningClosingTag} (this very specific case is a tag that opens
 * and immediately closes, much like empty tags in XML.
 *
 * What constitutes a tag is determined by {@code Tag.MARKUP_PATTERN}.
 * A tag has a name (e.g. [note]'s name is "note") and a position within
 * the text it was extracted from.
 *
 * All instances are mutable and not synchronized.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
abstract class Tag {

    /**
     * The pattern for determining what a tag is.
     */
    public static final Pattern MARKUP_PATTERN =
            Pattern.compile("(\\[\\/?([^]/]+)\\/?\\])");

    /**
     * The name of the tag, without the syntactic coating
     */
    private final String name;

    /**
     * The full tag with its syntactic coating.
     */
    private final String fullTag;

    /**
     * The leading position in the text the tag has been extracted from.
     */
    protected final int startPosition;

    /**
     * The trailing position in the text the tag has been extracted from.
     */
    protected final int endPosition;

    /**
     * The text the tag has been extracted from.
     */
    protected String sourceText = null;

    /**
     * Constructor.
     *
     * @param name Name of this tag, without the brackets and slashes.
     * @param fullTag The full name of this tag, with brackets/slashes.
     * @param startPosition The leading position of this tag in the source text.
     * @param endPosition The trailing position of this tag in the source text.
     */
    protected Tag(String name,
                  String fullTag,
                  int startPosition,
                  int endPosition) {
        this.name = name;
        this.fullTag = fullTag;
        this.startPosition = startPosition;
        this.endPosition = endPosition;
    }

    /**
     * Strip tags from an input. For example, {@code [li]hello[/li]} becomes
     * {@code hello}.
     *
     * @param input The input from which to strip tags.
     * @return A new string, based on the input, with tags stripped out.
     */
    protected static String stripTags(String input) {
        return input.replaceAll(MARKUP_PATTERN.pattern(), "");
    }

    /**
     * Get the name of this tag.
     *
     * @return Name of this tag, without the brackets and slashes.
     */
    public String getName() {
        return name;
    }

    /**
     * Get the full tag.
     *
     * @return The full name of this tag, with brackets/slashes.
     */
    public String getFullTag() {
        return fullTag;
    }

    /**
     * Get the start position of this tag.
     *
     * @return The leading position of this tag in the source text.
     */
    public int getStartPosition() {
        return startPosition;
    }

    /**
     * Get the end position of this tag.
     *
     * @return The trailing position of this tag in the source text.
     */
    public int getEndPosition() {
        return endPosition;
    }

    /**
     * Set the source text, this tag was extracted from.
     *
     * @param sourceText The source text.
     */
    public void setSourceText(String sourceText) {
        this.sourceText = sourceText;
    }

    /**
     * Get the source text, this tag was extracted from.
     *
     * @return The source text. May be {@code null} if not set yet.
     */
    public String getSourceText() {
        return this.sourceText;
    }

}
