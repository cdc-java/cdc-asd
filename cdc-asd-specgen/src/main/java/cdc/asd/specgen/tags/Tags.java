package cdc.asd.specgen.tags;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import cdc.asd.specgen.s1000d5.NormalBrParaElem;
import cdc.asd.specgen.s1000d5.Note;
import cdc.asd.specgen.s1000d5.Para;
import cdc.asd.specgen.s1000d5.RandomList;
import cdc.asd.specgen.s1000d5.S1000DElementNode;
import cdc.asd.specgen.s1000d5.S1000DGenericElementNode;
import cdc.asd.specgen.s1000d5.S1000DNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode;
import cdc.asd.specgen.s1000d5.S1000DTextNode.FormattingPolicy;

/**
 * Convert a source text that contains simple markups, in the form of
 * [li], [note], ..., into {@link NormalBrParaElem}.
 *
 * The tags are stripped out before being transformed into
 * {@code NormalBrParaElem}.
 *
 * If the source text does not contain any tags, then the function will
 * simply wrap its input inside a {@link Para}.
 *
 * Support for additional tags can be implemented in the constructor.
 *
 * @author Félix Mitjans {@literal <felix.mitjans@knds.fr>}
 *
 */
public final class Tags {
    /**
     * All the tags found in the source text, in the order in which
     * they were found.
     */
    private final List<Tag> tags;

    private final List<NormalBrParaElem> outputNodes;

    private static final int GROUP_FULL_TAG = 1;
    private static final int GROUP_TAG_NAME = 2;

    private static final String SYNTAX_ERROR_CLOSING_TAG_MISPLACED =
            "Syntax error in package description. This tag is misplaced: %s.";
    private static final String SYNTAX_ERROR_WRONG_CLOSING_TAG =
            "Syntax error in package description. This closing tag: %s was found after %s.";
    private static final String SYNTAX_ERROR_OPENING_TAG_NOT_CLOSED =
            "Syntax error in package description. This tag is never closed: %s.";
    private static final String SYNTAX_ERROR_ILLEGAL_SYNTAX = "Illegal syntax: %s";

    private final String sourceText;

    private final Set<FormattingPolicy> formattingPolicies;

    private final class ReferTo {
        public final List<String> referToList;

        public ReferTo(List<String> referToList) {
            this.referToList = referToList;
        }
    }

    private final ReferTo referTo;

    private static final Pattern ERROR_CONTEXT_NEWLINE_PATTERN = Pattern.compile("\\n");
    private static final String ERROR_CONTEXT_ENCLOSING_TEXT = "...";
    private static final int ERROR_CONTEXT_PRINT_BEFORE = 25;
    private static final int ERROR_CONTEXT_PRINT_AFTER = 25;

    /**
     * Constructor.
     *
     * @param sourceText The source text.
     * @param referTo A list of ICNs that will be referred to whenever
     *            the [referTo/] tag is used. May be null.
     * @param formattingPolicies The formatting policies to enforce, recursively, on all
     *            created text element nodes.
     * @exception IllegalArgumentException Bad syntax
     */
    public Tags(String sourceText,
                List<String> referTo,
                Set<FormattingPolicy> formattingPolicies) {
        this.sourceText = sourceText;
        this.referTo = new ReferTo(referTo);

        final Map<String, Function<OpeningTag, S1000DElementNode>> tmpTagTypes = new HashMap<>();

        this.formattingPolicies = Set.copyOf(formattingPolicies);

        tmpTagTypes.put("randomList", tag -> new RandomList().child(tag.getChildren()));
        tmpTagTypes.put("li",
                        tag -> new S1000DGenericElementNode(S1000DNode.LISTITEM).child(new S1000DGenericElementNode(S1000DNode.PARA).child(tag.getChildren())));
        tmpTagTypes.put("note", tag -> new Note().noteParaNode(tag.getChildren()));

        if (referTo != null) {
            tmpTagTypes.put("referTo", this::createReferTo);
        }

        this.tagTypes = Map.copyOf(tmpTagTypes);

        tags = Tag.MARKUP_PATTERN.matcher(sourceText)
                                 .results()
                                 .filter(this::tagExists)
                                 .map(this::createFrom)
                                 .toList();

        for (final Tag tag : tags) {
            tag.setSourceText(sourceText);
        }

        if (!tags.isEmpty()) {
            final Tag[] tagsArray = tags.toArray(new Tag[tags.size()]);

            setIndenture(tagsArray, 0, 0);

            setParentChild(tags);
        }

        outputNodes = new ArrayList<>();

        generateNodes();
    }

    /**
     * Get a list of {@link NormalBrParaElem} from our tags.
     *
     * @return An unmodifiable list of NormalBrParaElem. If no tags are
     *         found, there will be a single element in this list.
     */
    public List<NormalBrParaElem> getS1000DNodes() {
        return List.copyOf(outputNodes);
    }

    /**
     * Generate S1000D nodes and place them in context.
     *
     */
    private void generateNodes() {
        if (!tags.isEmpty()) {
            streamOpeningTags(tags).sorted(Comparator.comparingInt(OpeningTag::getIndenture).reversed())
                                   .filter(tag -> tag.getIndenture() > 0)
                                   .forEach(this::setNodeForTag);

            final List<OpeningTag> topLevelOpeningTags =
                    streamOpeningTags(tags).filter(tag -> tag.getIndenture() == 0)
                                           .toList();

            int currentPosition = 0;
            OpeningTag latestTopLevelTag = null;
            Para latestPara = null;

            // final List<NormalBrParaElem> outputNodes = new ArrayList<>();

            for (final OpeningTag topLevelTag : topLevelOpeningTags) {
                latestTopLevelTag = topLevelTag;
                Note note = null;
                final String currentZone = sourceText.substring(currentPosition,
                                                                topLevelTag.startPosition);

                if (topLevelTag.getName().equals("note")) {
                    note = (Note) tagTypes.get(topLevelTag.getName()).apply(topLevelTag);
                }

                if (topLevelTag.startPosition > currentPosition && !currentZone.matches("^[\r\n]+$")) {
                    latestPara = new Para(currentZone.trim());
                    if (note != null) {
                        outputNodes.add(latestPara);
                        outputNodes.add(note);
                        topLevelTag.setRelatedNode(note);
                    } else {
                        final S1000DElementNode node = tagTypes.get(topLevelTag.getName())
                                                               .apply(topLevelTag);

                        if (node instanceof Para) {

                            if (outputNodes.size() > 0) {
                                if (outputNodes.get(outputNodes.size() - 1) instanceof final Para previousPara) {
                                    outputNodes.set(outputNodes.size() - 1,
                                                    previousPara.content(latestPara.getChildren())
                                                                .content(node.getChildren()));
                                } else {
                                    outputNodes.add(latestPara.content(node.getChildren()));
                                }

                            } else {
                                outputNodes.add(latestPara.content(node.getChildren()));

                            }
                        } else {
                            if (outputNodes.size() > 0) {
                                if (outputNodes.get(outputNodes.size() - 1) instanceof final Para previousPara) {
                                    outputNodes.set(outputNodes.size() - 1,
                                                    previousPara.content(latestPara.getChildren())
                                                                .content(node));
                                } else {
                                    outputNodes.add(latestPara.content(node));
                                }

                            } else {
                                outputNodes.add(latestPara.content(node));

                            }
                        }
                    }
                } else {
                    final S1000DElementNode node = tagTypes.get(topLevelTag.getName()).apply(topLevelTag);

                    if (node instanceof final Para p) {
                        latestPara = p;
                    } else {
                        latestPara = new Para(node);
                    }

                    outputNodes.add(note != null ? note : latestPara);
                }

                currentPosition = topLevelTag.closingTag.endPosition;
            }

            if (latestTopLevelTag.closingTag.endPosition < sourceText.length() - 1) {
                final Para trailingParagraph =
                        new Para(sourceText.substring(latestTopLevelTag.closingTag.endPosition,
                                                      sourceText.length() - 1));

                if (latestTopLevelTag.getName().equals("note")) {
                    outputNodes.add(trailingParagraph);
                } else {

                    // MErge into previous paragraph (if previous was a paragraph)
                    if (outputNodes.get(outputNodes.size() - 1) instanceof final Para previousPara) {
                        outputNodes.set(outputNodes.size() - 1,
                                        previousPara.content(trailingParagraph.getChildren()));
                    } else {
                        outputNodes.add(trailingParagraph);
                    }
                }
            }

            return;
        }

        outputNodes.add(new Para(sourceText));
    }

    /**
     * Create an {@link S1000DNode} from a tag and modify that tag to link
     * to the newly created S1000D node.
     *
     * @param tag The tag. It will be modified by this function.
     * @return The output node.
     */
    private S1000DNode setNodeForTag(OpeningTag tag) {

        tag.setRelatedNode(tagTypes.get(tag.getName()).apply(tag));

        return tag.getRelatedNode();
    }

    /**
     * Map of acceptable tags.
     * For each acceptable tag, there must be a function that generates
     * {@link S1000DNode}s.
     * The function {@code OpeningTag.getChildren()} can be used to correctly
     * incorporate the content inside the resulting S1000D nodes.
     */
    private final Map<String, Function<OpeningTag, S1000DElementNode>> tagTypes;

    private S1000DElementNode createReferTo(OpeningTag tag) {
        final List<S1000DNode> referToNodes =
                referTo.referToList.stream()
                                   .map(rt -> new S1000DGenericElementNode(S1000DNode.INTERNALREF).attribute(S1000DNode.INTERNALREFID,
                                                                                                             rt)
                                                                                                  .attribute(S1000DNode.INTERNALREFTARGETTYPE,
                                                                                                             S1000DNode.INTERNALREFTARGETTYPEVALUE))
                                   .map(S1000DNode.class::cast)
                                   .toList();

        final int referToNodesSize = referToNodes.size();
        final ArrayList<S1000DNode> finalNodes = new ArrayList<>();

        finalNodes.add(new S1000DTextNode("Refer to "));

        for (int i = 0; i < referToNodesSize; i++) {
            finalNodes.add(referToNodes.get(i));

            if (i + 1 < referToNodesSize
                    && i + 2 >= referToNodesSize) {
                finalNodes.add(new S1000DTextNode(" and "));
                continue;
            }

            if (i + 1 < referToNodesSize) {
                finalNodes.add(new S1000DTextNode(", "));
            }
        }

        return new Para("").content(finalNodes);
    }

    /**
     * Check if tag exists.
     *
     * @param matchResult The match result of the tag.
     * @return True if tag is known or false otherwise.
     */
    private boolean tagExists(MatchResult matchResult) {
        return tagTypes.containsKey(matchResult.group(GROUP_TAG_NAME));
    }

    /**
     * Transform a match result into a Tag.
     *
     * @param matchResult The match result.
     * @return The resulting {@link OpeningTag}, {@link OpeningClosingTag} or
     *         {@link ClosingTag}.
     */
    private Tag createFrom(MatchResult matchResult) {
        final Class<? extends Tag> tagOrientation = tagOrientation(matchResult.group(GROUP_FULL_TAG));

        if (tagOrientation == OpeningTag.class) {
            return new OpeningTag(matchResult.group(GROUP_TAG_NAME),
                                  matchResult.group(GROUP_FULL_TAG),
                                  matchResult.start(),
                                  matchResult.end(),
                                  formattingPolicies);
        }

        if (tagOrientation == OpeningClosingTag.class) {
            return new OpeningClosingTag(matchResult.group(GROUP_TAG_NAME),
                                         matchResult.group(GROUP_FULL_TAG),
                                         matchResult.start(),
                                         matchResult.end());
        }

        if (tagOrientation == ClosingTag.class) {
            return new ClosingTag(matchResult.group(GROUP_TAG_NAME),
                                  matchResult.group(GROUP_FULL_TAG),
                                  matchResult.start(),
                                  matchResult.end());
        }

        throw new IllegalArgumentException("Cannot create tag out of: " + matchResult.group());
    }

    /**
     * Detect tag orientation. Much like XML, a
     * tag like [this] is opening; a tag like
     * [/this] is closing; a tag like [this/] is
     * opening/closing.
     *
     * @param tag The tag whose orientation is to be provided.
     * @return The tag's {@code TagOrientation}.
     * @throws IllegalArgumentException Impossible to detect tag's
     *             orientation.
     */
    private static Class<? extends Tag> tagOrientation(String tag) {
        Class<? extends Tag> type = OpeningTag.class;

        if (tag.charAt(1) == '/') {
            type = ClosingTag.class;
        }

        if (tag.charAt(tag.length() - 2) == '/') {
            if (type == ClosingTag.class) {
                throw new IllegalArgumentException(String.format(SYNTAX_ERROR_ILLEGAL_SYNTAX, tag));
            }

            type = OpeningClosingTag.class;
        }
        return type;
    }

    /**
     * Browse an array of tags and set their indenture values.
     *
     * @param tags An array of tags. The members of this array will be modified by
     *            this function.
     * @param index The index where to start in the array.
     * @param indenture The current indenture value (0 if starting from the top).
     * @return The index of the last member that was processed.
     * @exception IllegalArgumentException Unexpected closing tag.
     */
    private static int setIndenture(Tag[] tags,
                                    int index,
                                    int indenture) {
        int i;

        for (i = index; i < tags.length; i++) {
            if (tags[i] instanceof final OpeningClosingTag oct) {
                oct.setIndenture(indenture);
            } else if (tags[i] instanceof final OpeningTag openingTag) {
                openingTag.setIndenture(indenture);

                i = findClosingTag((OpeningTag) tags[i],
                                   tags,
                                   i,
                                   indenture + 1);

                openingTag.setClosingTag((ClosingTag) tags[i]);
            } else if (tags[i] instanceof ClosingTag) {
                if (indenture > 0) {
                    // Treat again closing tag in outer stack level
                    return i - 1;
                }

                throw new IllegalArgumentException(String.format(SYNTAX_ERROR_CLOSING_TAG_MISPLACED,
                                                                 tags[i].getFullTag()));
            }
        }

        return i;
    }

    /**
     * For a given opening tag, browse an array of tags and find a matching closing tag.
     *
     * @param tag The opening tag for which to find the closing counterpart.
     * @param tags The array where to search for the closing counterpart.
     * @param index Where to begin in the array.
     * @param indenture The indenture value to set to the child tags.
     * @return The index of the last member that was processed.
     * @exception IllegalArgumentException Could not find closing counterpart
     *                for supplied opening tag.
     * @exception IllegalArgumentException Found unexpected closing tag.
     */
    private static int findClosingTag(OpeningTag tag,
                                      Tag[] tags,
                                      int index,
                                      int indenture) {
        for (int i = index + 1; i < tags.length; i++) {
            if (tags[i] instanceof ClosingTag) {
                if (tags[i].getName().equals(tag.getName())) {
                    return i;
                } else {
                    final String finalContext = getTagContext(tags[i]);

                    throw new IllegalArgumentException(String.format(SYNTAX_ERROR_WRONG_CLOSING_TAG + "\n" + finalContext,
                                                                     tags[i].getFullTag(),
                                                                     tag.getFullTag()));
                }
            }

            if (tags[i] instanceof OpeningTag) {
                i = setIndenture(tags, i, indenture);
            }
        }

        throw new IllegalArgumentException(String.format(SYNTAX_ERROR_OPENING_TAG_NOT_CLOSED, tag.getFullTag()));
    }

    /**
     * Get the context of a {@link Tag}. Print {@link #ERROR_CONTEXT_PRINT_BEFORE}
     * characters before the supplied tag, then the tag itself, then an additional
     * {@link #ERROR_CONTEXT_PRINT_AFTER} characters, then print another line with
     * "^" characters pointing to the exact position of the tag in the above line.
     * 
     * @param tag The {@link Tag} for which to get the context.
     * @return A string that contains the context in the format given above.
     */
    private static String getTagContext(Tag tag) {
        final int beginIndex =
                tag.getStartPosition() - ERROR_CONTEXT_PRINT_BEFORE + 1 > 0 ? tag.getStartPosition() - ERROR_CONTEXT_PRINT_BEFORE
                        : tag.getStartPosition();
        final int endIndex =
                tag.getSourceText().length() > tag.getEndPosition() + ERROR_CONTEXT_PRINT_AFTER + 1
                        ? tag.getEndPosition() + ERROR_CONTEXT_PRINT_AFTER
                        : tag.getEndPosition();

        final String rawContext = tag.getSourceText().substring(beginIndex, endIndex);

        final String context = ERROR_CONTEXT_ENCLOSING_TEXT
                + ERROR_CONTEXT_NEWLINE_PATTERN.matcher(rawContext).replaceAll("")
                + ERROR_CONTEXT_ENCLOSING_TEXT;

        final int strippedNewlines = ERROR_CONTEXT_NEWLINE_PATTERN.matcher(rawContext).results().mapToInt(e -> 1).sum();

        return context + "\n"
                + new String(" ").repeat(tag.getStartPosition() - beginIndex + ERROR_CONTEXT_ENCLOSING_TEXT.length()
                        - strippedNewlines)
                + "^".repeat(tag.getFullTag().length())
                + new String(" ").repeat(tag.getEndPosition() - beginIndex + ERROR_CONTEXT_ENCLOSING_TEXT.length());
    }

    /**
     * Set the parent-child relationship for a list of tags based on their indenture code.
     *
     * @param tags The list of tags. Each tag will be modified in order to refer to its children.
     * @throw IllegalStateException Indenture codes have not been calculated (with {@code setIndenture()})
     *        yet.
     */
    private static void setParentChild(List<Tag> tags) {
        final OpeningTag[] indentedTag = new OpeningTag[getMaxIndenture(tags) + 1];

        streamOpeningTags(tags).sequential()
                               .forEach(setCurrentIndent(indentedTag).andThen(setParentFor(indentedTag)));
    }

    private static Consumer<OpeningTag> setCurrentIndent(OpeningTag[] indentedTag) {
        return currentTag -> {
            if (currentTag.getIndenture() == -1) {
                throw new IllegalStateException("No indenture code for this tag. Have indenture codes"
                        + " been calculated yet?");
            }

            indentedTag[currentTag.getIndenture()] = currentTag;
        };
    }

    private static Consumer<OpeningTag> setParentFor(OpeningTag[] indentedTag) {
        return currentTag -> {
            if (currentTag.getIndenture() > 0) {
                indentedTag[currentTag.getIndenture() - 1].addChild(currentTag);
            }
        };
    }

    /**
     * Find out the highest indenture values among the supplied tags.
     *
     * @param tags The tags for which we want to find the highest indenture value.
     * @return The highest indenture value.
     */
    private static int getMaxIndenture(List<Tag> tags) {
        return streamOpeningTags(tags).max(Comparator.comparingInt(OpeningTag::getIndenture))
                                      .orElseThrow()
                                      .getIndenture();
    }

    /**
     * Opens a stream of OpeningTags, from a collection of Tags.
     *
     * @param tags The collection of Tags.
     * @return A new stream.
     */
    private static Stream<OpeningTag> streamOpeningTags(Collection<Tag> tags) {
        return tags.stream()
                   .filter(OpeningTag.class::isInstance)
                   .map(OpeningTag.class::cast);
    }
}