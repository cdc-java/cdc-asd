# AsdModelToS1000D
`AsdModelToS1000D` loads an `ASD MF XML model` to produce:
- Data model S1000D data modules
- Glossary data modules


## AsdModelToS1000D options
Options of `AsdModelToS1000D` are:

````
USAGE
AsdModelToS1000D [--args-file <arg>] [--args-file-charset <arg>] [--basename <arg>] [--clean]
                       [--definitions] [--glossary] [-h | -v] [--help-width <arg>] --model <arg>
                       --output-dir <arg> [--previous-model <arg>] [--spec]  [--verbose]

Utility that converts XML EA model to S1000D Spec (S2000M, S3000L, ...) and/or Glossary (SX001G)
Data Modules.
If no generation target is defined, data model DM are generated.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option
                                or value).
                                A line is ignored when it is empty or starts by any number of white
                                spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is
                                read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file
                                encoding.
    --basename <arg>            Optional base name of generated files.
    --clean                     Clean model before converting it to S1000D.
    --definitions               Generate the data dictionary definitions (S3000L).
    --glossary                  Generate glossary data modules (SX001G).
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --model <arg>               Mandatory name of the ASD MF XML model to transform.
    --output-dir <arg>          Mandatory name of the output directory. If this directory does not
                                exist, it is created.
    --previous-model <arg>      Optional name of the ASD MF XML model in a previous version for
                                comparison.
    --spec                      Generate data model spec data modules (S2000M, S3000L, ...).
 -v,--version                   Prints version and exits.
    --verbose                   Prints messages.
````