# AsdCdcMappingExporter
`AsdCdcMappingExporter` exports the mapping between ASD and CDC rules.


## AsdCdcMappingExporter options
Options of `AsdCdcMappingExporter` are:

````
USAGE
AsdCdcMappingExporter [--args-file <arg>] [--args-file-charset <arg>] [-h | -v] [--help-width <arg>]
                            --output <arg>  [--verbose]

Utility that can export the ASD/CDC mapping of rules to an Office file.
Supported formats are: xlsx, xls, xlsm, csv and ods.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option
                                or value).
                                A line is ignored when it is empty or starts by any number of white
                                spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is
                                read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file
                                encoding.
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --output <arg>              Name of the mapping file to generate. Its extension must be
                                recognized and supported.
 -v,--version                   Prints version and exits.
    --verbose                   Prints messages.
````