# AsdModelsComparator

`AsdModelsComparator` loads several `ASD MF XML models` and compares them.


## AsdModelsComparator options
Options of `AsdModelsComparator `are:

````
USAGE
AsdModelsComparator [--args-file <arg>] [--args-file-charset <arg>] [--best] [--diff-id |
                          --diff-name]  [--fastest] [-h | -v] [--help-width <arg>] --model <arg>
                          [--no-show-cardinalities | --show-cardinalities] [--no-show-ids |
                          --show-ids] [--no-show-notes | --show-notes] [--no-show-stereotypes |
                          --show-stereotypes] [--no-show-tags | --show-tags] --output <arg>
                          [--show-all]       [--verbose]

Utility that can compare several ASD MF XML models and save comparison into an Office file.
It generates:
 - 'Packages' sheet, showing characteristics of each package in each model
 - 'Classes' sheet, showing characteristics of each class in each model
 - 'Interfaces' sheet, showing characteristics of each interface in each model
 - 'Attributes' sheet, showing characteristics of each attribute in each model
 - 'Tips' sheet, showing characteristics of each tip in each model
 - 'Implementations' sheet, showing characteristics of each implementation in each model
 - 'Specializations' sheet, showing characteristics of each specialization in each model
 - 'Synthesis' sheet, showing presence of each element in each model
 - 1 sheet for each model, showing characteristics of each element of the model
 - 'All' sheet, showing characteristics of all elements of all models
 - 1 sheet for each pair of models (optional); In that case model names are compressed.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option
                                or value).
                                A line is ignored when it is empty or starts by any number of white
                                spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is
                                read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file
                                encoding.
    --best                      Use options that generate best output.
    --diff-id                   Compare pairs of models using (kind, id) as key.
                                This should be used when all compared models are revision of the
                                same specification.
    --diff-name                 Compare pairs of models using (kind, name, discriminator) as key.
                                The discriminator is used to differentiate elements with identical
                                names.
                                This should be used when compared models belong to different
                                specifications.
    --fastest                   Use options that are fast to generate output (default).
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --model <arg>               Mandatory name(s) of the ASD MF XML models to compare.
    --no-show-cardinalities     Do not show cardinality of attributes.
    --no-show-ids               Do not show ids of model elements. Use with --show-all.
    --no-show-notes             Do not show notes of model elements. Use with --show-all.
    --no-show-stereotypes       Do not show stereotypes of model elements. Use with --show-all.
    --no-show-tags              Do not show tags of model elements. Use with --show-all.
    --output <arg>              Mandatory name of the output file. It must end with an Office
                                extension (XLSX, CSV, ...).
    --show-all                  Show all details.
    --show-cardinalities        Show cardinality of attributes.
    --show-ids                  Show ids of model elements.
    --show-notes                Show notes of model elements.
    --show-stereotypes          Show stereotypes of model elements.
    --show-tags                 Show tags of model elements.
 -v,--version                   Prints version and exits.
    --verbose                   Prints messages.
````