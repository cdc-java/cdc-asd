package cdc.asd.checks;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import cdc.asd.checks.packages.PackageUtils;

class PackageUtilsTest {
    @Test
    void test() {
        assertEquals("Foo", PackageUtils.cleanName("CDM Foo"));
        assertEquals("Foo", PackageUtils.cleanName("CDM UoF Foo"));
        assertEquals("Foo", PackageUtils.cleanName("Foo 0-1_000-01"));
        assertEquals("Foo", PackageUtils.cleanName("CDM UoF Foo_0-1_000-01"));

        assertEquals("Foo", PackageUtils.cleanName("S1000D Foo"));
        assertEquals("Foo", PackageUtils.cleanName("S2000M Foo"));
        assertEquals("Foo", PackageUtils.cleanName("S3000L Foo"));
        assertEquals("Foo", PackageUtils.cleanName("S4000P Foo"));
        assertEquals("Foo", PackageUtils.cleanName("S5000F Foo"));
        assertEquals("Foo", PackageUtils.cleanName("S6000T Foo"));
    }
}