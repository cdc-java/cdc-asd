package cdc.asd.checks;

import java.util.EnumSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import cdc.asd.model.AsdModelUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.issues.rules.RuleDescription;
import cdc.mf.model.MfUtils;

public class AsdRuleDescription extends RuleDescription {
    private static final Pattern ASD_RULE_PATTERN =
            Pattern.compile("- R[A-Z][A-Z]\\d\\d [A-Z][A-Z0-9_\\(\\)]*: ");

    public static final String SOURCE_SX002D_2_1 = "[SX002D 2.1]";
    public static final String SOURCE_UMLWRSG_2_0 = "[UML Writing Rules and Style Guide 2.0]";
    public static final String SOURCE_XSDAR_2_0 = "[XML Schema Authoring Rules 2.0]";

    protected AsdRuleDescription(Builder builder) {
        super(builder);
    }

    public Set<AsdRule> getRelatedRules() {
        final Set<AsdRule> set = EnumSet.noneOf(AsdRule.class);
        final String description = getText();
        final Matcher matcher = ASD_RULE_PATTERN.matcher(description);
        while (matcher.find()) {
            final String match = matcher.group();
            final String name = match.substring(8, match.length() - 2);
            final AsdRule rel = AsdRule.valueOf(name);
            set.add(rel);
        }
        return set;
    }

    public static AsdRuleDescription.Builder builder() {
        return new AsdRuleDescription.Builder();
    }

    public static class Builder extends RuleDescription.Builder<Builder> {
        protected Builder() {
            super();
        }

        public Builder appliesTo(Set<AsdStereotypeName> stereotypeNames) {
            section(SECTION_APPLIES_TO);
            for (final AsdStereotypeName stereotypeName : stereotypeNames) {
                uItem("All " + AsdModelUtils.identify(stereotypeName) + " "
                        + stereotypeName.getScope().stream()
                                        .map(MfUtils::getKindPlural)
                                        .collect(Collectors.joining(", ")));
            }
            return self();
        }

        public Builder relatedTo(AsdRule... asdRules) {
            section(SECTION_RELATED_TO);
            for (final AsdRule rule : asdRules) {
                uItem(rule.getCode() + " " + rule.name() + ": " + rule.getDescription());
            }
            return self();
        }

        @Override
        public AsdRuleDescription build() {
            return new AsdRuleDescription(this);
        }
    }
}