package cdc.asd.checks.tags;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfTag;

public class TagWhenUidPatternValueShouldBeAtMost8Chars extends MfAbstractRuleChecker<MfTag> {
    public static final String NAME = "T10";
    public static final String TITLE = "TAG(UID_PATTERN)_VALUE_SHOULD_BE_AT_MOST_8_CHARS";
    public static final IssueSeverity SEVERITY = IssueSeverity.MINOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All defined {%wrap} {%wrap} should be at most 8 chars.",
                                                                AsdNames.T_UID_PATTERN + WS + N_TAG,
                                                                N_VALUE + S)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.3")
                                                        .relatedTo(AsdRule.UID_PATTERN_LENGTH),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION,
                                AsdLabels.DEPRECATED)
                        .build();

    public TagWhenUidPatternValueShouldBeAtMost8Chars(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected String getHeader(MfTag object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfTag object,
                             Location location) {
        final String value = object.getValue();
        if (value != null && value.length() > 8) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(value)
                       .violation("has more than 8 characters (" + value.length() + ")");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.UID_PATTERN;
    }
}