package cdc.asd.checks.tags;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagValueMustMatchPattern;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;

public class TagWhenXmlNameValueMustBeLowerCamelCase extends AbstractTagValueMustMatchPattern {
    public static final String NAME = "T11";
    public static final String TITLE = "TAG(XML_NAME)_VALUE_MUST_BE_LOWER_CAMEL_CASE";
    public static final String REGEX = LOWER_CAMEL_CASE_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap}" + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                AsdNames.T_XML_NAME + WS + N_TAG,
                                                                N_VALUE + S)
                                                        .text("\nThey must be lowerCamelCase.")
                                                        .appliesTo("All attribute " + AsdNames.T_XML_NAME + " tag values",
                                                                   "All class " + AsdNames.T_XML_NAME + " tag values",
                                                                   "All interface " + AsdNames.T_XML_NAME + " tag values")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + "  11.8.1")
                                                        .relatedTo(AsdRule.XML_NAME_CASE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public TagWhenXmlNameValueMustBeLowerCamelCase(SnapshotManager manager) {
        super(manager,
              RULE,
              REGEX);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.XML_NAME
                && (object.getParent() instanceof MfProperty
                        || object.getParent() instanceof MfClass
                        || object.getParent() instanceof MfInterface);
    }
}