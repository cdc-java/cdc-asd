package cdc.asd.checks.tags;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfTag;
import cdc.util.strings.StringUtils;

public class TagNameMustBeRecognized extends MfAbstractRuleChecker<MfTag> {
    public static final String NAME = "T02";
    public static final String TITLE = "TAG_NAME_MUST_BE_RECOGNIZED";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All defined {%wrap} {%wrap} must be recognized.",
                                                                N_TAG,
                                                                N_NAME + S)
                                                        .appliesTo("All attribute tag names",
                                                                   "All connector tag names",
                                                                   "All class tag names",
                                                                   "All interface tag names",
                                                                   "All package tag names")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8")
                                                        .relatedTo(AsdRule.AGGREGATION_TAGS,
                                                                   AsdRule.ASSOCIATION_TAGS,
                                                                   AsdRule.ATTRIBUTE_TAGS,
                                                                   AsdRule.ATTRIBUTE_GROUP_TAGS,
                                                                   AsdRule.CLASS_TAGS,
                                                                   AsdRule.COMPOSITION_TAGS,
                                                                   AsdRule.EXCHANGE_TAGS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public TagNameMustBeRecognized(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected final String getHeader(MfTag object) {
        return getTheNameOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfTag object,
                             Location location) {
        if (!StringUtils.isNullOrEmpty(object.getName())) {
            final AsdTagName tagname = AsdTagName.of(object.getName());
            if (tagname == null) {
                // Tag name is not recognized
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .violation("is not recognized");
                final AsdTagName ic = AsdTagName.ofIgnoreCase(object.getName());
                if (ic != null) {
                    description.uItem("ignoring case, it matches '" + ic.getLiteral() + "'");
                } else {
                    final AsdTagName similar = AsdTagName.ofMostSimilar(object.getName());
                    if (similar != null) {
                        description.uItem("possible misspelling of '" + similar.getLiteral() + "'");
                    }
                }

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }
}