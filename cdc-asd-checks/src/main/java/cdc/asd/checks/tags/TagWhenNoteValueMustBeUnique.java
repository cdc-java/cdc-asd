package cdc.asd.checks.tags;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagValuesMustBeUnique;
import cdc.mf.model.MfTagOwner;

public class TagWhenNoteValueMustBeUnique extends AbstractTagValuesMustBeUnique<MfTagOwner> {
    public static final String NAME = "T05";
    public static final String TITLE = "TAG(NOTE)_VALUE_MUST_BE_UNIQUE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AsdNames.N_ITEM, AsdTagName.NOTE))
                                                        .appliesTo("All attributes",
                                                                   "All connectors",
                                                                   "All classes",
                                                                   "All interfaces"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.17.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public TagWhenNoteValueMustBeUnique(SnapshotManager manager) {
        super(manager,
              MfTagOwner.class,
              RULE,
              AsdTagName.NOTE);
    }
}