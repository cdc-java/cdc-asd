package cdc.asd.checks.tags;

import java.util.List;

import cdc.issues.checks.AbstractChecker;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;

public class TagsChecker extends AbstractPartsChecker<MfTagOwner, MfTag, TagsChecker> {
    @SafeVarargs
    public TagsChecker(SnapshotManager manager,
                       AbstractChecker<? super MfTag>... partCheckers) {
        super(manager,
              MfTagOwner.class,
              MfTag.class,
              partCheckers);
    }

    @Override
    protected List<LocatedObject<? extends MfTag>> getParts(MfTagOwner object) {
        final List<MfTag> delegates = object.getTags()
                                            .stream()
                                            .sorted(MfNameItem.NAME_COMPARATOR)
                                            .toList();
        return LocatedObject.locate(delegates);
    }
}