package cdc.asd.checks.tags;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdModel;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.checks.atts.text.TextUtils;
import cdc.mf.model.MfTag;

public class TagWhenRefValueShouldContainOneConcept extends MfAbstractRuleChecker<MfTag> {
    public static final String NAME = "T06";
    public static final String TITLE = "TAG(REF)_VALUE_SHOULD_CONTAIN_ONE_CONCEPT";
    public static final IssueSeverity SEVERITY = IssueSeverity.MINOR;
    public static final AsdTagName TAG_NAME = AsdTagName.REF;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("Each {%wrap} {%wrap} that contains one token should be a recognized concept.",
                                                                TAG_NAME.getLiteral() + WS + N_TAG,
                                                                N_VALUE)

                                                        .appliesTo("All " + AsdNames.T_REF
                                                                + " tag values that contain one token.")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.6")
                                                        .relatedTo(AsdRule.ATTRIBUTE_REF_INCORRECT,
                                                                   AsdRule.CLASS_REF_INCORRECT,
                                                                   AsdRule.INTERFACE_REF_INCORRECT,
                                                                   AsdRule.ASSOCIATION_REF_INCORRECT)
                                                        .remarks("As ref tag can also refer to external sources of information, it can produce false positives.",
                                                                 "Classes, interfaces and attributes names are considered as concepts: should we exclude attributes?"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public TagWhenRefValueShouldContainOneConcept(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected final String getHeader(MfTag object) {
        return getTheValueOfHeader(object);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == TAG_NAME;
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfTag object,
                             Location location) {
        final String value = object.getValue();
        if (TextUtils.containsOneToken(value)
                && !object.getModel().wrap(AsdModel.class).getSpecificTypeNames().contains(value)
                && !object.getModel().wrap(AsdModel.class).getAttributeNames().contains(value)) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(value)
                       .violation("does not contain a recognized concept");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}