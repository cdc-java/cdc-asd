package cdc.asd.checks.tags;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.ftags.FTagsAnalyzer;
import cdc.asd.model.ftags.FTagsAnalyzer.Message;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdTag;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfTag;

public class TagWhenLongDescriptionNotesMustHaveValidMarkup extends MfAbstractRuleChecker<MfTag> {
    public static final String NAME = "T04";
    public static final String TITLE = "TAG(LONG_DESCRIPTION)_NOTES_MUST_HAVE_VALID_MARKUP";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("The {%wrap} {%wrap} must have valid markup.",
                                                                AsdNames.T_LONG_DESCRIPTION + WS + N_TAG,
                                                                N_NOTES)
                                                        .appliesTo("All " + AsdNames.T_LONG_DESCRIPTION + " notes"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.17.0")
                        .build();

    public TagWhenLongDescriptionNotesMustHaveValidMarkup(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected String getHeader(MfTag object) {
        return getTheNotesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfTag object,
                             Location location) {
        final String notes = object.wrap(AsdElement.class).getNotes();
        final FTagsAnalyzer analyzer = notes == null ? null : new FTagsAnalyzer(notes);

        if (analyzer != null && !analyzer.getMessages().isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(notes)
                       .violation("has invalid markup");

            description.uItems(analyzer.getMessages()
                                       .stream()
                                       .map(Message::toString)
                                       .toList());

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfTag object) {
        return object.wrap(AsdTag.class).is(AsdTagName.LONG_DESCRIPTION);
    }
}