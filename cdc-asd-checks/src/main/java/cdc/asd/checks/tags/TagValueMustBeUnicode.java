package cdc.asd.checks.tags;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.text.AbstractTextMustBeUnicode;
import cdc.mf.model.MfTag;

public class TagValueMustBeUnicode extends AbstractTextMustBeUnicode<MfTag> {
    public static final String NAME = "T03";
    public static final String TITLE = "TAG_VALUE_MUST_BE_UNICODE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap}" + MUST_BE_UNICODE,
                                                                N_TAG,
                                                                N_VALUE + S)
                                                        .appliesTo("All attribute tag values",
                                                                   "All connector tag values",
                                                                   "All class tag values",
                                                                   "All interface tag values",
                                                                   "All package tag values"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public TagValueMustBeUnicode(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected final String getText(MfTag object) {
        return object.getValue();
    }

    @Override
    protected final String getHeader(MfTag object) {
        return getTheValueOfHeader(object);
    }
}