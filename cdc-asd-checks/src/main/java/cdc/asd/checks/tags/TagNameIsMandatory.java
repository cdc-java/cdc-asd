package cdc.asd.checks.tags;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameIsMandatory;
import cdc.mf.model.MfTag;

public class TagNameIsMandatory extends AbstractNameIsMandatory<MfTag> {
    public static final String NAME = "T01";
    public static final String TITLE = "TAG_NAME_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_TAG + S))
                                                        .appliesTo("All attribute tags",
                                                                   "All connector tags",
                                                                   "All class tags",
                                                                   "All interface tags",
                                                                   "All package tags")
                                                        .relatedTo(AsdRule.AGGREGATION_TAGS,
                                                                   AsdRule.ASSOCIATION_TAGS,
                                                                   AsdRule.ATTRIBUTE_TAGS,
                                                                   AsdRule.ATTRIBUTE_GROUP_TAGS,
                                                                   AsdRule.CLASS_TAGS,
                                                                   AsdRule.COMPOSITION_TAGS,
                                                                   AsdRule.EXCHANGE_TAGS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public TagNameIsMandatory(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }
}