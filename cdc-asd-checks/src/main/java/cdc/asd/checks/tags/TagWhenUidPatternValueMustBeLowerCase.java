package cdc.asd.checks.tags;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagValueMustMatchPattern;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;

public class TagWhenUidPatternValueMustBeLowerCase extends AbstractTagValueMustMatchPattern {
    public static final String NAME = "T09";
    public static final String TITLE = "TAG(UID_PATTERN)_MUST_BE_LOWER_CASE";
    public static final String REGEX = LOWER_CASE_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap}" + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                AsdNames.T_UID_PATTERN + WS + N_TAG,
                                                                N_VALUE + S)
                                                        .text("\nThey must be lower case.")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.3")
                                                        .relatedTo(AsdRule.UID_PATTERN_CASE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION,
                                AsdLabels.DEPRECATED)
                        .build();

    public TagWhenUidPatternValueMustBeLowerCase(SnapshotManager context) {
        super(context,
              RULE,
              REGEX);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.UID_PATTERN
                && (object.getParent() instanceof MfProperty
                        || object.getParent() instanceof MfClass
                        || object.getParent() instanceof MfInterface);
    }
}