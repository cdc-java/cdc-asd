package cdc.asd.checks.tags;

import java.util.List;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.AsdSnapshotManager;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdModel;
import cdc.asd.model.wrappers.AsdTag;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;
import cdc.mf.model.MfType;

public class TagWhenReplacesValueMustExistInRefModel extends MfAbstractRuleChecker<MfTag> {
    public static final String NAME = "T07";
    public static final String TITLE = "TAG(REPLACES)_VALUE_MUST_EXIST_IN_REF_MODEL";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap} must exist in the {%wrap}.",
                                                                AsdNames.T_REPLACES + WS + N_TAG,
                                                                N_VALUE + S,
                                                                AsdNames.N_REF_MODEL)
                                                        .appliesTo("All " + AsdNames.T_REPLACES
                                                                + " tag values (if a reference model is passed)")
                                                        .remarks("Choice of reference model impacts results."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.15.0")
                        .build();

    public TagWhenReplacesValueMustExistInRefModel(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected String getHeader(MfTag object) {
        return getTheValueOfHeader(object);
    }

    private MfModel getRefModel() {
        return getManager(AsdSnapshotManager.class).getRefModel().orElseThrow();
    }

    private static String getOwnerKind(MfTag object) {
        final MfTagOwner owner = object.getParent();
        if (owner instanceof MfPackage) {
            return "package";
        } else if (owner instanceof MfType) {
            return "type";
        } else if (owner instanceof MfProperty) {
            return "property";
        } else {
            return "???";
        }
    }

    private boolean valueExistsInRef(MfTag object) {
        final String value = object.getValue();
        final MfTagOwner owner = object.getParent();
        if (owner instanceof MfPackage) {
            return getRefModel().wrap(AsdModel.class).getPackageNames().contains(value);
        } else if (owner instanceof MfType) {
            return getRefModel().wrap(AsdModel.class).getSpecificTypeNames().contains(value);
        } else if (owner instanceof MfProperty) {
            return getRefModel().wrap(AsdModel.class).getAttributeNames().contains(value);
        } else {
            return false;
        }
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfTag object,
                             Location location) {
        final List<MfTag> tagsReplaces = object.getParent().getTags(AsdTagName.REPLACES);

        if (tagsReplaces.size() == 1) {
            if (!valueExistsInRef(object)) {
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .value(object.getValue())
                           .violation("is not a " + getOwnerKind(object) + " of reference model '" + getRefModel().getName() + "'");

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfTag object) {
        return object.wrap(AsdTag.class).is(AsdTagName.REPLACES);
    }

    @Override
    public boolean isCorrectlyConfigured() {
        return getManager(AsdSnapshotManager.class).getRefModel().isPresent();
    }
}