package cdc.asd.checks.tags;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.text.AbstractTextMustContainOneToken;
import cdc.mf.model.MfTag;

public class TagWhenSomeValueMustContainOneToken extends AbstractTextMustContainOneToken<MfTag> {
    public static final String NAME = "T08";
    public static final String TITLE = "TAG(SOME)_VALUE_MUST_CONTAIN_ONE_TOKEN";
    private static final Set<AsdTagName> TAG_NAMES = EnumSet.of(AsdTagName.REF,
                                                                AsdTagName.UNIT,
                                                                AsdTagName.VALID_VALUE);
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("Each {%wrap}, {%wrap} or {%wrap} {%wrap}"
                                      + MUST_CONTAIN_ONE_TOKEN,
                                                                AsdNames.T_REF + WS + N_TAG,
                                                                AsdNames.T_UNIT + WS + N_TAG,
                                                                AsdNames.T_VALID_VALUE + WS + N_TAG,
                                                                N_VALUE)
                                                        .appliesTo("All " + AsdNames.T_REF + " tag values",
                                                                   "All " + AsdNames.T_UNIT + " tag values",
                                                                   "All " + AsdNames.T_VALID_VALUE + " tag values"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public TagWhenSomeValueMustContainOneToken(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected String getText(MfTag object) {
        return object.getValue();
    }

    @Override
    protected String getHeader(MfTag object) {
        return getTheValueOfHeader(object);
    }

    @Override
    public boolean accepts(MfTag object) {
        return TAG_NAMES.contains(AsdTagName.of(object.getName()));
    }
}