package cdc.asd.checks.tags;

import java.util.List;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfTag;

public class TagWhenXmlRefNameValueMustFollowAuthoring extends MfAbstractRuleChecker<MfTag> {
    public static final String NAME = "T12";
    public static final String TITLE = "TAG(XML_REF_NAME)_VALUE_MUST_FOLLOW_AUTHORING";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap} must be built by appending 'Ref' to the {%wrap} {%wrap}.",
                                                                AsdNames.T_XML_REF_NAME + WS + N_TAG,
                                                                N_VALUE + S,
                                                                AsdNames.T_XML_NAME + WS + N_TAG,
                                                                N_VALUE)
                                                        .appliesTo("All " + AsdNames.T_XML_REF_NAME + " tag values")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.2")
                                                        .relatedTo(AsdRule.XML_REF_NAME_DEF),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public TagWhenXmlRefNameValueMustFollowAuthoring(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    protected String getHeader(MfTag object) {
        return getTheValueOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfTag object,
                             Location location) {
        final List<MfTag> tagsXmlName = object.getParent().getTags(AsdTagName.XML_NAME);

        if (tagsXmlName.size() == 1) {
            final String xmlNameValue = tagsXmlName.get(0).getValue();
            final String xmlRefNameValue = object.getValue();
            if (!xmlRefNameValue.equals(xmlNameValue + "Ref")) {
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .value(xmlRefNameValue)
                           .violation("is not compliant with " + AsdTagName.XML_NAME.getLiteral()
                                   + " value '" + xmlNameValue + "'");

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.XML_REF_NAME;
    }
}