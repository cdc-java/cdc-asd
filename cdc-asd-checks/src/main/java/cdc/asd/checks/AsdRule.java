package cdc.asd.checks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public enum AsdRule {
    ATTRIBUTE_NAME_CASE(b().code("RAC02")
                           .name("attrNameCase")
                           .desc("The attribute name must exist, start with a letter, be in lowerCamelCase and may not include spaces.")),
    ATTRIBUTE_NAME_UNIQUE(b().code("RAC03")
                             .name("attrNameUnique")
                             .desc("The attribute name must be unique in the class (included inherited attributes)")),
    ATTRIBUTE_NAME_CONSISTENT(b().code("RAC04")
                                 .name("attrNameConsistent")
                                 .desc("If an attribute name is repeated in another class, then the type and the definition must be the same.")),
    ATTRIBUTE_CARDINALITY(b().code("RAC05")
                             .name("attrCardinality")
                             .desc("The attribute cardinality must be numeric or be a numeric range (including '*' for unlimited upper range), and not zero.")),
    ATTRIBUTE_KEY_LOWER_CARDINALITY(b().code("RAC06")
                                       .name("keyCardinality")
                                       .desc("  Key attributes (stereotype key, compositeKey and relationshipKey) must always have the lower multiplicity defined as ‘.")),
    ATTRIBUTE_UPPER_CARDINALITY(b().code("RAC07")
                                   .name("attrUpperCard")
                                   .desc("  Attributes of type DescriptorType and NameType must always have their upper cardinality defied as ‘*’ (ie, many)")),
    ATTRIBUTE_NAME_NO_CODE(b().code("RAC08")
                              .name("attrCodeInName")
                              .desc("'Code' must not be used as part of an attribute name. Use ‘Classification’ or ‘Identifier’ instead in order to distinguish between different uses of codes.")),
    ATTRIBUTE_VISIBLE(b().code("RAC09")
                         .name("attrVisible")
                         .desc("An attribute must be visible.")),
    ATTRIBUTE_NAME_NO_SPACE(b().code("RAC10")
                               .name("attrNameNoSpace")
                               .desc("The attribute name may not contain spaces and must start with a lowercase letter.")),
    ATTRIBUTE_TYPE(b().code("RAC11")
                      .name("attrType")
                      .desc("An attribute must exist and be either a primitive, compoundattribute, existing class or UML primitive.")),
    ATTRIBUTE_NAME_CLASS(b().code("RAC12")
                            .name("attrNameClass")
                            .desc("An attribute name must start with the class name.")),

    ATTRIBUTE_DEFINITION(b().code("RAD01")
                            .name("attrDef")
                            .desc("An attribute must have a definition")),
    ATTRIBUTE_DEFINITION_AUTHORING(b().code("RAD02")
                                      .name("attrDefAuth")
                                      .desc("An attribute definition must start with the attribute name")),

    ATTRIBUTE_STEREOTYPES(b().code("RAS01")
                             .name("attributeStereoTypes")
                             .desc("Attributes can only have the \"key\", \"compositeKey\", \"relationshipkey\", \"characteristic\" or \"metadata\" stereotypes. The stereotype is mandatory.")),
    ATTRIBUTE_KEYS(b().code("RAS02")
                      .name("attributeKeys")
                      .desc("Only IdentifierType and ClassificationType attributes can have the <<key>>, <<compositeKey>> or <<relationshipKey>> stereotypes.")),
    ATTRIBUTE_CLASS_KEYS(b().code("RAS03")
                            .name("attributeClassKeys")
                            .desc("Non-relationship classes must not use <<relationshipKey>> as stereotype.")),
    ATTRIBUTE_COMPOSITE_KEYS(b().code("RAS04")
                                .name("attributeCompositeKeys")
                                .desc("Key attributes in composition classes  must use <<compositeKey>> stereotype.")
                                .remarks("Ignore compositions whose whole is an <<exchange>>.")),
    ATTRIBUTE_NO_COMPOSITE_KEYS(b().code("RAS05")
                                   .name("attributeNoCompositeKeys")
                                   .desc("Non-composition classes must not use <<compositeKey>> as stereotype.")),
    ATTRIBUTE_RELATIONSHIP_KEYS(b().code("RAS06")
                                   .name("attributeRelationshipKeys")
                                   .desc("Key attributes in relationship classes must use the <<relationshipKey>> stereotype.")),

    ATTRIBUTE_TAGS(b().code("RAT01")
                      .name("attrTagTypes")
                      .desc("An attribute can only have the following tags: xmlName, validValue, validValueLibrary, unit, note, example, source and ref.")),
    ATTRIBUTE_XML_NAME(b().code("RAT02")
                          .name("attrXmlName")
                          .desc("An attribute must have exactly one xmlName.")),
    ATTRIBUTE_XML_NAME_UNIQUE(b().code("RAT03")
                                 .name("attrXmlNameUnique")
                                 .desc("The xmlName of an attribute must be unique within its class (including inherited attributes).")),
    IDENTIFER_VALID_VALUE_OR_LIBRARY(b().code("RAT04")
                                        .name("identiferValidValue")
                                        .desc("All attributes of identifier type must have at least one (non-blank) validValue/validValueLibrary.")),
    CLASSIFICATION_VALID_VALUE_OR_LIBRARY(b().code("RAT05")
                                             .name("classificationValidValue")
                                             .desc("All attributes of classification type must have at least one validValue/validValueLibrary. The validValue can be blank.")),
    DATED_CLASSIFICATION_VALID_VALUE_OR_LIBRARY(b().code("RAT06")
                                                   .name("datedClassifValidValue")
                                                   .desc("All attributes of DatedClassification type must have at least one validValue/validValueLibrary. The validValue can be blank.")),
    TIMED_CLASSIF_VALID_VALUE_OR_LIBRARY(b().code("RAT07")
                                            .name("timedClassifValidValue")
                                            .desc("All attributes of TimeStampedClassification type must have at least one validValue/validValueLibrary. The validValue can be blank.")),
    VALID_VALUE_AND_LIBRARY(b().code("RAT08")
                               .name("validValueAndLib")
                               .desc("It is not allowed to have simultaneously a validValue and validValueLibrary for the same attribute.")),
    VALID_VALUE_LIBRARY(b().code("RAT09")
                           .name("validValueLibrary")
                           .desc("An attribute can only have zero or one single validValueLibary.")),
    NO_VALID_VALUE(b().code("RAT10")
                      .name("noValidValue")
                      .desc(" validValue/validValueLibrary tags not allowed for DateType, DateTimeType, DescriptorType, NameType or UML primitives.")),
    NO_BLANK_VALID_VALUE(b().code("RAT11")
                            .name("noBlankValidValue")
                            .desc("If an attribute has more than one validValue, then none of them must be blank.")),
    UNIT(b().code("RAT12")
            .name("unit")
            .desc("All attributes of Property type (or its specializations) must have exactly one unit tag.")),
    NO_UNIT_ALLOWED(b().code("RAT13")
                       .name("noUnitAllowed")
                       .desc("The unit tag is not allowed for IdentifierType, DescriptorType, NameType, ClassificationType, DateType, DateTimeType or StateType primitives.")),
    ATTRIBUTE_REF_INCORRECT(b().code("RAT14")
                               .name("attributeRefIncorrect")
                               .desc("An interface reference must reference an existing class or interface.")
                               .remarks("See RCT07, RIT02 and RYT22",
                                        "Should be: An attribute reference ...")),
    SAME_ATTRIBUTE_VALID_VALUES(b().code("RAT15")
                                   .name("sameAttrValidValues")
                                   .desc("Two attributes with the same name must have the same validValues.")),
    STATE_VALID_VALUE_OR_LIBRARY(b().code("RAT20")
                                    .name("stateValidValue")
                                    .desc("All attributes of state type must have at least one validValue/validValueLibrary. The validValue can be blank.")),

    CLASS_NO_KEY_IF_SPECIALIZED(b().code("RCA01")
                                   .name("classNoKeyIfSpecialized")
                                   .desc("A Class which is a specialization of another Class must not add any key attributes (<<key>>, <<compositeKey>> and <<relationshipKey>>).")),

    CLASS_NAME_RULES(b().code("RCC01")
                        .name("classNameRules")
                        .desc("A class name name must exist, start with a letter, be in UpperCamelCase and may not include spaces.")),
    CLASS_NAME_SINGULAR(b().code("RCC02")
                           .name("classNameSingular")
                           .desc("A class name must be stated in singular (eg, Car, not Cars).")
                           .remarks("Difficult to implement")),
    CLASS_NAME_UNIQUE(b().code("RCC03")
                         .name("classNameUnique")
                         .desc("A class name must be unique, except if it is a specialization (extension) of a class of the CDM (same name).")),

    CLASS_DEFINITION(b().code("RCD01")
                        .name("classDef")
                        .desc("Classes must have a definition.")),
    CLASS_DEFINITION_AUTHORING(b().code("RCD02")
                                  .name("classDefAuth")
                                  .desc("A class definition must start with the class name.")),

    CLASS_SPECIALIZATION(b().code("RCH01")
                            .name("classSpecialization")
                            .desc("A class cannot be a specialization of more than one class (multiple inheritance not allowed).")),
    CLASS_COMPOSITION_SPECIALIZATION(b().code("RCH02")
                                        .name("classCompSpecial")
                                        .desc("A specialized class cannot be a composition of another class (risk of adding keys to specialization).")),

    CLASS_STEREOTYPES(b().code("RCM01")
                         .name("classStereo")
                         .desc("A class must have a stereotype, either <<class>>, <<relationship>>, <<attributeGroup>>, <<exchange>> or <<proxy>>.")
                         .remarks("Code should be RCS")),
    CLASS_PUBLIC(b().code("RCM02")
                    .name("classPublic")
                    .desc("The scope of a class must be Public.")),
    CLASS_AUTHOR(b().code("RCM03")
                    .name("classAuthor")
                    .desc("A class must have an author; the author name must be one of the specification numbers or \"DMEWG\".")),
    CLASS_VERSION(b().code("RCM04")
                     .name("classVersion")
                     .desc("A class must have a version number with a format NNN-NN, where NNN must be three digits and not null and NN must be \"00\".")),

    CLASS_TAGS(b().code("RCT01")
                  .name("classTags")
                  .desc("Allowed tags for classes and relationships are uidPattern, xmlName, xmlRefName, note, example, replaces, source, ref and changeNote.")),
    CLASS_XML_NAME(b().code("RCT02")
                      .name("classXmlName")
                      .desc("Each class must have exactly one xmlName.")),
    UID_PATTERN(b().code("RCT03")
                   .name("uidPattern")
                   .desc("There must be exactly one uidPattern for non-specialized classes, relationships and proxies.")),
    UID_PATTERN_EXCLUSION(b().code("RCT04")
                             .name("uidPatternExcl")
                             .desc("uidPattern must not be defined for specialized classes.")),
    XML_REF_NAME(b().code("RCT05")
                    .name("xmlRefName")
                    .desc("Classes, relationships & proxies must have either zero or 1 xmlRefName.")),
    XML_REF_NAME_DEF(b().code("RCT06")
                        .name("xmlRefNameDef")
                        .desc("xmlRefName must be equal to xmlName + 'Ref'.")),
    CLASS_REF_INCORRECT(b().code("RCT07")
                           .name("classRefIncorrect")
                           .desc("A class reference must reference an existing class or interface.")
                           .remarks("See RAT14, RIT02 and RYT22")),

    CLASS_USED(b().code("RCX01")
                  .name("classUsed")
                  .desc("Class is used in at least one diagram.")),
    CLASS_REVISION(b().code("RCX02")
                      .name("classRev")
                      .desc("Class revisions (class name ends with 'Revision'): It must include Rationale, Date, Status, in that order, at the end of the class.")),
    WRONG_CLASS_IN_EXCHANGE(b().code("RCX03")
                               .name("wrongClassInExchange")
                               .desc("A class that is part of a composition must not be part of an <<exchange>>.")),
    REDUNDANT_CLASS_EXCHANGE(b().code("RCX04")
                                .name("redundatClassExchange")
                                .desc("A class is included in the <<exchange>> that is a specialization of another class also included in the same exchange.")),
    CLASS_EXCHANGED(b().code("RCX05")
                       .name("classExchanged")
                       .desc("All classes with \"class\" stereotype should be included in at least one exchange.")),

    CLASS_COMPOSITION(b().code("RCY01")
                         .name("classComp")
                         .desc("A class cannot be a composition of more than one class or <<extend>> interface.")
                         .remarks("This only applies to classes that have a business id (local or inherited key(s)")),

    DIAGRAM_NAME(b().code("RDC01")
                    .name("diagramName")
                    .desc("A diagram name must start with the specification number or \"CDM\", must be written in capitalization style and must not include hyphens or underscores to separate words.")),

    DIAGRAM_STEREOTYPES(b().code("RDS01")
                           .name("diagramStereo")
                           .desc("Diagram stereotypes may be only <<NoPrint>> or null.")),

    EXCHANGE_NO_ATTRIBUTES(b().code("REC01")
                              .name("exchangeNoAttr")
                              .desc("<<exchange>> classes cannot have attributes.")),

    EXCHANGE_TAGS(b().code("RET01")
                     .name("exchangeTags")
                     .desc("Allowed <<exchange>> tags are xmlName, xmlSchemaName, note, example, replaces, source, ref and changeNote.")),
    XML_SCHEMA_NAME(b().code("RET02")
                       .name("xmlSchemaName")
                       .desc("xmlSchemaName is mandatory for <<exchange>> classes which are first indenture level specializations of the CDM MessageContent class.")),
    XML_SCHEMA_NAME_EXCL(b().code("RET03")
                            .name("xmlSchemaNameExcl")
                            .desc("xmlSchemaName must not be used for <<exchange>> classes which are NOT first indenture level specializations of the CDM MessageContent class.")),

    EXCHANGE_COMPOSITION(b().code("REY01")
                            .name("exchangeComp")
                            .desc("exchange classes can only be compositions or specializations of other exchange classes (except MessageContent, which must be a specialization of Message).")),
    EXCHANGE_CONNECTOR(b().code("REY02")
                          .name("exchangeConnect")
                          .desc("exchange classes cannot have any connectors (aggregations, associations, implementations, etc) except those listed in rule REY01 or a link to a Note.")),
    EXCHANGE_CONTENT(b().code("REY03")
                        .name("exchangeContent")
                        .desc("Only items with \"class\" or \"exchange\" stereotypes can be compositions of an exchange.")),
    EXCHANGE_SPECIALIZATION(b().code("REY04")
                               .name("exchangeSpecializations")
                               .desc("A specialization and its generalization must not appear in the same exchange")),

    ATTRIBUTE_GROUP_NOT_ABSTRACT(b().code("RGC01")
                                    .name("attrGrpNotAbstract")
                                    .desc("attributeGroups cannot be abstract.")),

    ATTRIBUTE_GROUP_TAGS(b().code("RGT01")
                            .name("attrGroupTags")
                            .desc("Allowed tags for attribute groups are xmlName, note, example, replaces, source, ref and changeNote.")),

    ATTRIBUTE_GROUP_NO_ASSOCIATIONS(b().code("RGY01")
                                       .name("attrGroupNoAssoc")
                                       .desc("An attributeGroup can have no incoming or outgoing associations.")),
    ATTRIBUTE_GROUP_COMPOSITION(b().code("RGY02")
                                   .name("attrgroupComp")
                                   .desc("An attributeGroup must always be a composition of a class.")),

    SELECT_NOT_ASSOCIATION_SOURCE(b().code("RIA01")
                                     .name("selectNotSource")
                                     .desc("<<select>> interfaces must not be used as the source for directed associations.")),
    SELECT_NOT_COMPOSITION_SOURCE(b().code("RIA02")
                                     .name("selectNotComp")
                                     .desc("<<select>> interfaces must not be used as the source for compositions.")),
    EXTEND_NOT_ASSOCIATION_TARGET(b().code("RIA03")
                                     .name("extendNotTarget")
                                     .desc("<<extend>> interfaces must not be used as the target for directed associations.")),
    INTERFACE_NO_ATTRIBUTES(b().code("RIA04")
                               .name("interfaceNoAttr")
                               .desc("Interfaces may not have any attributes.")),
    NO_PRIMITIVE_EXTEND(b().code("RIA06")
                           .name("noPrimitiveExtend")
                           .desc("<<primitives>> cannot implement <<extend>> interfaces (ie, only <<class>> and <<relationship>> allowed)."
                                   + "\nException is if that extend interface ONLY has one composition (eg, RemarkItem)")),
    NO_ATTRIBUTE_GROUP_EXTEND(b().code("RIA07")
                                 .name("noAttributeGroupExtend")
                                 .desc("<<attributeGroup>> cannot implement <<extend>> interfaces (ie, only <<class>> and <<relationship>> allowed).")),
    NO_COMPOUND_ATTRIBUTE_EXTEND(b().code("RIA08")
                                    .name("noCompAttrExtend")
                                    .desc("<<compoundAttribute>> cannot implement <<extend>> interfaces (ie, only <<class>> and <<relationship>> allowed).")),
    NO_INTERFACE_EXTEND(b().code("RIA09")
                           .name("noInterfaceExtend")
                           .desc("Interfaces cannot extend <<extend>> interfaces (ie, only <<class>> and <<relationship>> allowed).")),
    NO_INTERFACE_SELECT(b().code("RIA10")
                           .name("noInterfaceSelect")
                           .desc("Interfaces cannot extend <<select>> interfaces.")),

    INTERFACE_NAME_RULES(b().code("RIC01")
                            .name("interfaceNameRules")
                            .desc("An interface name name must exist, start with a letter, be in UpperCamelCase and may not include spaces.")),

    INTERFACE_XML_NAME_UNIQUE(b().code("RID01")
                                 .name("interfaceXmlNameUnique")
                                 .desc("An interface xmlName must be     unique in the context of an S-Series ILS specifications data model, including any imported data models.")
                                 .remarks("Can an interface xmlName be the same as a class xmlName?",
                                          "See RTD04")),

    // Should be RIS01?

    INTERFACE_STEREOTYPES(b().code("RIM01")
                             .name("interfaceStereo")
                             .desc("An interface must have a stereotype, either <<select>> or <<extend>>.")),

    SELECT_XML_NAME(b().code("RIT01")
                       .name("selectXmlName")
                       .desc("<<select>> interfaces must have exactly one xmlName tag.")),
    INTERFACE_REF_INCORRECT(b().code("RIT02")
                               .name("interfaceRefIncorrect")
                               .desc("An interface reference must reference an existing class or interface.")
                               .remarks("See RAT14, RCT07 and RYT22")),

    UML_PROFILE(b().code("RLX01")
                   .name("UMLProfile")
                   .desc("Wrong UML profile detected.")
                   .remarks("Code should be RXX01.")),

    CONSISTENT_RELATIONSHIP_CARDINALITY(b().code("RMC01")
                                           .name("consistentRelCardinality")
                                           .desc("Cardinalities of relationship associations must be compatible (eg, cannot have one incoming 1..* and outgoing 0..*)")),

    RELATIONSHIP_NO_KEY(b().code("RMT01")
                           .name("relNoKey")
                           .desc("A relationship  with more than two associations must have the additional ones tagged as \"NoKey\".")
                           .remarks("noKey is not declared as a possible tag name in " + AsdRuleDescription.SOURCE_UMLWRSG_2_0)),

    PACKAGE_NAME(b().code("RPC01")
                    .name("packageName")
                    .desc("A package name (except the root node) must start with the specification number or \"CDM\", must be written in capitalization style and must not include hyphens or underscores to separate words.")),

    PACKAGE_DEFINITION(b().code("RPD01")
                          .name("packageDef")
                          .desc("A package must have a definition.")),
    PACKAGE_DEFINITION_AUTHORING(b().code("RPD02")
                                    .name("packageDefAuth")
                                    .desc("A package definition must start with the package name.")),

    PACKAGE_STEREOTYPES(b().code("RPS01")
                           .name("packageStereo")
                           .desc("All package stereotypes (except the model itself) must be <<Domain>>, <<FunctionalArea>>, <<UoF>> or null.")),
    PACKAGE_STEREOTYPES_FULL(b().code("RPS02")
                                .name("packageStereoFull")
                                .desc("If <<Domain>> or <<FunctionalArea>> stereotypes are used, the use of <<UoF>> stereotypes is mandatory and all packages MUST have a stereotype.")),

    PACKAGE_NO_TAGS(b().code("RPT01")
                       .name("packageNoTags")
                       .desc("A package with \"Domain\" or \"FunctionalArea\" stereotypes should have no tags.")),
    UOF_TAGS(b().code("RPT02")
                .name("UoFTags")
                .desc("A  package with no stereotype or \"UoF\" one can have only the following tags: \"NoPrint\", \"ICN\", \"replaces\".")),
    UOF_ICNS(b().code("RPT03")
                .name("UoFICNs")
                .desc("A package must have one \"ICN\" tag for every single diagram that it contains.")),

    UID_PATTERN_UNIQUE(b().code("RTD01")
                          .name("uidPatternUnique")
                          .desc("uidPattern must be unique in the context of an S-Series ILS specifications data model, including any imported data models.")),
    UID_PATTERN_LENGTH(b().code("RTD02")
                          .name("uidPatternLen")
                          .desc("uidPattern should be as short as possible and should not exceed eight character.")),
    UID_PATTERN_CASE(b().code("RTD03")
                        .name("uidPatternCase")
                        .desc("uidPattern must be written using only lowercase characters.")),
    CLASS_XML_NAME_UNIQUE(b().code("RTD04")
                             .name("classXmlNameUnique")
                             .desc("A class xmlName must be unique in the context of an S-Series ILS specifications data model, including any imported data models.")
                             .remarks("Can a class xmlName be the same as an interface xmlName?",
                                      "See RID01")),
    XML_NAME_CASE(b().code("RTD05")
                     .name("xmlNameCase")
                     .desc("xmlName must be defined in lowerCamelCase for classes, interfaces and attributes.")),
    CLASS_XML_REF_NAME_CASE(b().code("RTD06")
                               .name("classXmlRefNameCase")
                               .desc("A class xmlRefName must be defined in lowerCamelCase.")),
    VALID_VALUE_CODE(b().code("RTD07")
                        .name("validValueCode")
                        .desc("A validValue code must be unique within a same attribute.")),
    VALID_VALUE_DEFINITION(b().code("RTD08")
                              .name("validValueDef")
                              .desc("A non-blank validValue must have a definition in the tagged value notes and the definition must start with \"SX001G:\"")),
    VALID_VALUE_DEFINITION_NAME(b().code("RTD09")
                                   .name("validValueDefName")
                                   .desc("The definition name of the validValue must be a single lowerCamelCase word.")),
    UNIT_TYPE(b().code("RTD10")
                 .name("unitType")
                 .desc("A \"unit\" tag must have a value \"unit\" or one of the approved/predefined unit types. It may not be blank.")),

    COMPOUND_ATTRIBUTE_NUMBER_OF_ATTRIBUTES(b().code("RUC01")
                                               .name("compAttrNumAttr")
                                               .desc("A compoundAttribute must have at least two attributes.")),

    COMPOUND_ATTRIBUTE_ASSOCIATIONS(b().code("RUY01")
                                       .name("compAttrAssoc")
                                       .desc("A compoundAttribute can have no associations, neither incoming nor outgoing.")),
    COMPOUND_ATTRIBUTE_AGGREGATIONS(b().code("RUY02")
                                       .name("compAttrAggr")
                                       .desc("A compoundAttribute cannot be part of an aggregation or a composition.")),

    CLASS_IN_UOF(b().code("RXX01")
                    .name("classInUoF")
                    .desc("A class may only be defined under an UoF, not under domain or functional area.")),

    ASSOCIATION_DIRECTION(b().code("RYC01")
                             .name("directAssocDirect")
                             .desc("All associations must have a direction \"Source->Destination\".")),
    ASSOCIATION_HAS_TARGET(b().code("RYC02")
                              .name("directAssocHasTarget")
                              .desc("All directed associations must have a target role.")),

    ASSOCIATION_TAGS(b().code("RYT01")
                        .name("assocTags")
                        .desc("An association can have the following tags: xmlName, note, example, ref, noKey.")),
    ASSOCIATION_XML_NAME(b().code("RYT02")
                            .name("assocXmlName")
                            .desc("An association must have  one xmlName if there are two or more associations defined between the same two classes. This includes inherited associations.")),
    ASSOCIATION_XML_NAME_UNIQUE(b().code("RYT03")
                                   .name("assocXmlNameSingle")
                                   .desc("An association cannot have more than one xmlName.")),
    AGGREGATION_TAGS(b().code("RYT04")
                        .name("aggregTags")
                        .desc("An aggregation can have only the following tags: xmlName, note, example, ref.")),
    AGGREGATION_XML_NAME(b().code("RYT05")
                            .name("aggregXmlName")
                            .desc("An aggregation must have  one xmlName if there are two or more associations defined between the same two classes. This includes inherited associations.")),
    AGGREGATION_XML_NAME_UNIQUE(b().code("RYT06")
                                   .name("aggregXmlNameSingle")
                                   .desc("An aggregation cannot have more than one xmlName")),
    COMPOSITION_TAGS(b().code("RYT07")
                        .name("compTags")
                        .desc("A composition can have only the following tags: xmlName, note, example, ref")),
    COMPOSITION_XML_NAME(b().code("RYT08")
                            .name("compXmlName")
                            .desc("A composition must have one xmlName if there are two or more associations defined between the same two classes. This includes inherited associations.")),
    COMPOSITION_XML_NAME_UNIQUE(b().code("RYT09")
                                   .name("compXmlNameSingle")
                                   .desc("A composition cannot have more than one xmlName")),
    RELATIONSHIP_COMPOSITION(b().code("RYT10")
                                .name("relationshipComposition")
                                .desc("A composite association must not use a relationship as its part.")),
    RELATIONSHIP_AGGREGATION(b().code("RYT11")
                                .name("relationshipAggregation")
                                .desc("An aggregation  must not use a relationship as its part.")),
    COMPOSITION_SOURCE_ROLE(b().code("RYT12")
                               .name("compSourceRole")
                               .desc("A composition must have  a Source Role if there are two or more associations defined between the same two classes. This includes inherited associations.")),
    AGGREGATION_SOURCE_ROLE(b().code("RYT13")
                               .name("aggregSourceRole")
                               .desc("An aggregation must have  a Source Role if there are two or more associations defined between the same two classes. This includes inherited associations.")),
    COMPOSITION_DEFINITION(b().code("RYT14")
                              .name("compDefinition")
                              .desc("A composition must have  a definition if there are two or more associations defined between the same two classes. This includes inherited associations.")),
    AGGREGATION_DEFINITION(b().code("RYT15")
                              .name("aggregDefinition")
                              .desc("An aggregation must have  a definition if there are two or more associations defined between the same two classes. This includes inherited associations.")),
    COMPOSITION_SOURCE_CARDINALITY(b().code("RYT16")
                                      .name("compMulti")
                                      .desc("A composition must have  a multiplIcity (cardinality) for the source class.")),
    COMPOSITION_TARGET(b().code("RYT17")
                          .name("compTarget")
                          .desc("A composition must not have a target role.")),
    COMPOSITION_TARGET_CARDINALITY(b().code("RYT18")
                                      .name("compTargetMulti")
                                      .desc("The target multiplicity (cardinality) of a composition must be either not filled in or 1.")),
    AGGREGATION_TARGET(b().code("RYT19")
                          .name("aggregTarget")
                          .desc("An aggregation must not have a target role.")),
    AGGREGATION_TARGET_CARDINALITY(b().code("RYT20")
                                      .name("aggregTargetMulti")
                                      .desc("The target multiplicity (cardinality) of an aggregation must be either not filled in or 1.")),
    AGGREGATION_SOURCE_CARDINALITY(b().code("RYT21")
                                      .name("aggregMulti")
                                      .desc("An aggregation must have a multiplicity (cardinality) for the source class.")),
    ASSOCIATION_REF_INCORRECT(b().code("RYT22")
                                 .name("assocRefIncorrect")
                                 .desc("An association reference must reference an existing class or interface.")
                                 .remarks("See RAT14, RCT07 and RIT02"));

    private final String code;
    private final String name;
    private final String description;
    private final List<String> remarks = new ArrayList<>();

    private static Builder b() {
        return new Builder();
    }

    private AsdRule(Builder builder) {
        this.code = builder.code;
        this.name = builder.name;
        this.description = builder.description;
        this.remarks.addAll(builder.remarks);
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public AsdRuleScope getScope() {
        return AsdRuleScope.of(code.charAt(1));
    }

    public AsdRuleCheckType getCheckType() {
        return AsdRuleCheckType.of(code.charAt(2));
    }

    public int getNumber() {
        return Integer.valueOf(code.substring(3));
    }

    public String getDescription() {
        return description;
    }

    public List<String> getRemarks() {
        return remarks;
    }

    private static class Builder {
        private String code;
        private String name;
        private String description;
        private final List<String> remarks = new ArrayList<>();

        Builder() {
        }

        Builder code(String code) {
            this.code = code;
            return this;
        }

        Builder name(String name) {
            this.name = name;
            return this;
        }

        Builder desc(String description) {
            this.description = description;
            return this;
        }

        Builder remarks(String... remarks) {
            Collections.addAll(this.remarks, remarks);
            return this;
        }
    }
}