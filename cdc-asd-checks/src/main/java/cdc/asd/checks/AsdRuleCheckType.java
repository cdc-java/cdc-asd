package cdc.asd.checks;

import java.util.HashMap;
import java.util.Map;

public enum AsdRuleCheckType {
    ATTRIBUTE('A', "Attribute"),
    CHARACTETISTICS_OR_CARDINALITY('C', "Characteristics/Cardinality"),
    DESCRIPTION('D', "Description"),
    INHERITANCE('H', "Inheritance"),
    INTERFACE('I', "Interface"),
    METADATA('M', "Metadata"),
    ICN('N', "ICN"),
    TAG_PATTERNS('P', "Tag patterns"),
    ROLE_OR_RESTRICTION('R', "Role/Restriction"),
    STEREOTYPE('S', "Stereotype"),
    TAG('T', "Tag"),
    OTHER('X', "Other"),
    CONNECTORS('Y', "Connectors");

    private final char code;
    private final String literal;

    private static final Map<Character, AsdRuleCheckType> MAP = new HashMap<>();

    static {
        for (final AsdRuleCheckType x : AsdRuleCheckType.values()) {
            MAP.put(x.getCode(), x);
        }
    }

    private AsdRuleCheckType(char code,
                             String literal) {
        this.code = code;
        this.literal = literal;
    }

    public char getCode() {
        return code;
    }

    public String getLiteral() {
        return literal;
    }

    public static AsdRuleCheckType of(char code) {
        return MAP.get(code);
    }
}