package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.misc.AbstractAuthorIsMandatory;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfInterface;

public class InterfaceAuthorIsMandatory extends AbstractAuthorIsMandatory<MfInterface> {
    public static final String NAME = "I02";
    public static final String TITLE = "INTERFACE_AUTHOR_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(ALL, N_INTERFACE + S))
                                                        .appliesTo("All interfaces")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for "
                                                                + AsdNames.S_SELECT,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.1 for "
                                                                         + AsdNames.S_EXTEND),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceAuthorIsMandatory(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}