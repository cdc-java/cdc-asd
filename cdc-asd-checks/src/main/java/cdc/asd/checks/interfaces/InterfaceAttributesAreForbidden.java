package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfInterface;

public class InterfaceAttributesAreForbidden extends MfAbstractRuleChecker<MfInterface> {
    public static final String NAME = "I01";
    public static final String TITLE = "INTERFACE_ATTRIBUTES_ARE_FORBIDDEN";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("An {%wrap} must not have any {%wrap}.",
                                                                N_INTERFACE,
                                                                N_ATTRIBUTE)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for "
                                                                + AsdNames.S_SELECT + " interfaces")
                                                        .relatedTo(AsdRule.INTERFACE_NO_ATTRIBUTES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    protected InterfaceAttributesAreForbidden(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }

    @Override
    protected String getHeader(MfInterface object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext manager,
                             MfInterface object,
                             Location location) {
        if (!object.getAllProperties().isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has attributes, which is not allowed");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}