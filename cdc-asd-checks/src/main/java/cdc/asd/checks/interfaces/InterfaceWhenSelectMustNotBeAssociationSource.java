package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.types.AbstractTypeMustNotPlayRole;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfTipRole;

public class InterfaceWhenSelectMustNotBeAssociationSource extends AbstractTypeMustNotPlayRole<MfInterface> {
    public static final String NAME = "I18";
    public static final String TITLE = "INTERFACE(SELECT)_MUST_NOT_BE_ASSOCIATION_SOURCE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} must not be an {%wrap} {%wrap}.",
                                                                AsdNames.S_SELECT + WS + N_INTERFACE,
                                                                N_ASSOCIATION,
                                                                N_SOURCE)
                                                        .relatedTo(AsdRule.SELECT_NOT_ASSOCIATION_SOURCE)
                                                        .remarks("Does this apply to compositions and aggregations?"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public InterfaceWhenSelectMustNotBeAssociationSource(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE,
              MfAssociation.class,
              MfTipRole.SOURCE);
    }

    @Override
    public boolean accepts(MfInterface object) {
        return object.wrap(AsdElement.class).getStereotypeName() == AsdStereotypeName.SELECT;
    }
}