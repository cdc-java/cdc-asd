package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameMustMatchPattern;
import cdc.mf.model.MfInterface;

public class InterfaceNameMustBeUpperCamelCase extends AbstractNameMustMatchPattern<MfInterface> {
    public static final String NAME = "I07";
    public static final String TITLE = "INTERFACE_NAME_MUST_BE_UPPER_CAMEL_CASE";
    public static final String REGEX = UPPER_CAMEL_CASE_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap}" + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                N_INTERFACE,
                                                                N_NAME + S)
                                                        .text("\nThey must be UpperCamelCase.")
                                                        .appliesTo("The name of all interfaces")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.3.1")
                                                        .relatedTo(AsdRule.INTERFACE_NAME_RULES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceNameMustBeUpperCamelCase(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE,
              REGEX);
    }
}