package cdc.asd.checks.interfaces;

import java.util.List;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesMustStartWithName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfInterface;

public class InterfaceNotesMustStartWithName extends AbstractNotesMustStartWithName<MfInterface> {
    public static final String NAME = "I09";
    public static final String TITLE = "INTERFACE_NOTES_MUST_START_WITH_NAME";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INTERFACE)),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public InterfaceNotesMustStartWithName(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }

    @Override
    protected List<String> cleanName(String name) {
        return List.of(name);
    }
}