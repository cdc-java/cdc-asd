package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.stereotypes.AbstractStereotypeIsMandatory;
import cdc.mf.model.MfInterface;

public class InterfaceStereotypeIsMandatory extends AbstractStereotypeIsMandatory<MfInterface> {
    public static final String NAME = "I10";
    public static final String TITLE = "INTERFACE_STEREOTYPE_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(ALL, N_INTERFACE + S))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for <<select>>",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.1 for <<extend>>")
                                                        .relatedTo(AsdRule.INTERFACE_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public InterfaceStereotypeIsMandatory(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}