package cdc.asd.checks.interfaces;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.stereotypes.AbstractStereotypesMustBeAllowed;
import cdc.asd.model.AsdStereotypeName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfInterface;

public class InterfaceStereotypesMustBeAllowed extends AbstractStereotypesMustBeAllowed<MfInterface> {
    public static final String NAME = "I11";
    public static final String TITLE = "INTERFACE_STEREOTYPES_MUST_BE_ALLOWED";

    /** Stereotypes that can be used with interfaces (11.5.1). */
    private static final Set<AsdStereotypeName> ALLOWED_STEREOTYPES =
            EnumSet.of(AsdStereotypeName.EXTEND,
                       AsdStereotypeName.SELECT);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INTERFACE))
                                                        .text(oneOf(ALLOWED_STEREOTYPES))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.1")
                                                        .relatedTo(AsdRule.INTERFACE_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceStereotypesMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }

    @Override
    protected boolean isAllowed(MfInterface object,
                                AsdStereotypeName stereotypeName) {
        return ALLOWED_STEREOTYPES.contains(stereotypeName);
    }
}