package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.misc.AbstractElementMustBeGraphicallyRepresented;
import cdc.mf.model.MfInterface;

public class InterfaceMustBeGraphicallyRepresented extends AbstractElementMustBeGraphicallyRepresented<MfInterface> {
    public static final String NAME = "I21";
    public static final String TITLE = "INTERFACE_MUST_BE_GRAPHICALLY_REPRESENTED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INTERFACE + S))
                                                        .appliesTo("All interfaces")
                                                        .relatedTo(AsdRule.CLASS_USED),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.23.0")
                        .build();

    public InterfaceMustBeGraphicallyRepresented(SnapshotManager context) {
        super(context,
              MfInterface.class,
              RULE);
    }
}