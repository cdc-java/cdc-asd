package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.types.AbstractTypeMustNotDeclareUselessInheritance;
import cdc.mf.model.MfInterface;

public class InterfaceMustNotDeclareUselessInheritance
        extends AbstractTypeMustNotDeclareUselessInheritance<MfInterface> {
    public static final String NAME = "I05";
    public static final String TITLE = "INTERFACE_MUST_NOT_DECLARE_USELESS_INHERITANCE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define(describe(A, N_INTERFACE)),
                              SEVERITY)
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .meta(Metas.SINCE, "0.19.0")
                        .build();

    protected InterfaceMustNotDeclareUselessInheritance(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}