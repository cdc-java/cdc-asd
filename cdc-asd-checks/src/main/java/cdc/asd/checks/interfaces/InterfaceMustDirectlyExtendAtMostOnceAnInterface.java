package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.types.AbstractTypeMustDirectlyExtendAtMostOnceAType;
import cdc.mf.model.MfInterface;

public class InterfaceMustDirectlyExtendAtMostOnceAnInterface
        extends AbstractTypeMustDirectlyExtendAtMostOnceAType<MfInterface> {
    public static final String NAME = "I04";
    public static final String TITLE = "INTERFACE_MUST_DIRECTLY_EXTEND_AT_MOST_ONCE_AN_INTERFACE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define(describe(A, N_INTERFACE, A, N_INTERFACE)),
                              SEVERITY)
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .meta(Metas.SINCE, "0.5.0")
                        .build();

    protected InterfaceMustDirectlyExtendAtMostOnceAnInterface(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}