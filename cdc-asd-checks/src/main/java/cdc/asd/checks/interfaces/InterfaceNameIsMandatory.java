package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameIsMandatory;
import cdc.mf.model.MfInterface;

public class InterfaceNameIsMandatory extends AbstractNameIsMandatory<MfInterface> {
    public static final String NAME = "I06";
    public static final String TITLE = "INTERFACE_NAME_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INTERFACE + S))
                                                        .appliesTo("All interfaces")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for <<select>>",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.1 for <<extend>>")
                                                        .relatedTo(AsdRule.INTERFACE_NAME_RULES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceNameIsMandatory(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}