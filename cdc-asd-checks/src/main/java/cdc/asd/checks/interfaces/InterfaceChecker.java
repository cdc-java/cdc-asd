package cdc.asd.checks.interfaces;

import cdc.asd.checks.connectors.ConnectorsChecker;
import cdc.asd.checks.misc.InheritanceMustBeGraphicallyRepresented;
import cdc.asd.checks.misc.InheritancesChecker;
import cdc.asd.checks.misc.SiblingsMustHaveDifferentNames;
import cdc.asd.checks.notes.NotesChecker;
import cdc.asd.checks.notes.NotesMustBeUnicode;
import cdc.asd.checks.notes.NotesMustNotContainHtml;
import cdc.asd.checks.stereotypes.StereotypeMustBeRecognized;
import cdc.asd.checks.tags.TagNameIsMandatory;
import cdc.asd.checks.tags.TagNameMustBeRecognized;
import cdc.asd.checks.tags.TagValueMustBeUnicode;
import cdc.asd.checks.tags.TagWhenNoteValueMustBeUnique;
import cdc.asd.checks.tags.TagWhenRefValueShouldContainOneConcept;
import cdc.asd.checks.tags.TagWhenReplacesValueMustExistInRefModel;
import cdc.asd.checks.tags.TagWhenSomeValueMustContainOneToken;
import cdc.asd.checks.tags.TagWhenUidPatternValueMustBeLowerCase;
import cdc.asd.checks.tags.TagWhenUidPatternValueShouldBeAtMost8Chars;
import cdc.asd.checks.tags.TagWhenXmlNameValueMustBeLowerCamelCase;
import cdc.asd.checks.tags.TagWhenXmlRefNameValueMustFollowAuthoring;
import cdc.asd.checks.tags.TagsChecker;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.mf.model.MfInterface;

public class InterfaceChecker extends CompositeChecker<MfInterface> {
    public InterfaceChecker(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              new InterfaceAttributesAreForbidden(manager),
              new InterfaceAuthorIsMandatory(manager),
              new InterfaceInheritanceIsForbidden(manager),
              new InterfaceMustBeGraphicallyRepresented(manager),
              new InterfaceMustDirectlyExtendAtMostOnceAnInterface(manager),
              new InterfaceMustNotDeclareUselessInheritance(manager),
              new InterfaceNameIsMandatory(manager),
              new InterfaceNameMustBeUpperCamelCase(manager),
              new InterfaceNotesAreMandatory(manager),
              new InterfaceNotesMustStartWithName(manager),
              new InterfaceStereotypeIsMandatory(manager),
              new InterfaceStereotypesMustBeAllowed(manager),
              new InterfaceTagWhenXmlRefNameCountMustBe0(manager),
              new InterfaceVersionIsMandatory(manager),
              new InterfaceVersionMustBeAllowed(manager),
              new InterfaceWhenExtendMustNotBeAssociationTarget(manager),
              new InterfaceWhenExtendTagWhenXmlNameCountMustBe1(manager),
              new InterfaceWhenSelectMustNotBeAssociationSource(manager),
              new InterfaceWhenSelectMustNotBeCompositionPart(manager),
              new InterfaceWhenSelectTagWhenXmlNameCountMustBe1(manager),
              new ConnectorsChecker(manager),
              new NotesChecker(manager,
                               new NotesMustBeUnicode(manager),
                               new NotesMustNotContainHtml(manager)),
              new SiblingsMustHaveDifferentNames(manager),
              new StereotypeMustBeRecognized(manager),
              new TagWhenNoteValueMustBeUnique(manager),
              new InheritancesChecker<MfInterface>(manager,
                                                   MfInterface.class,
                                                   new InheritanceMustBeGraphicallyRepresented(manager)),
              new TagsChecker(manager,
                              new InterfaceTagNameMustBeAllowed(manager),
                              new TagNameIsMandatory(manager),
                              new TagNameMustBeRecognized(manager),
                              new TagValueMustBeUnicode(manager),
                              new TagWhenRefValueShouldContainOneConcept(manager),
                              new TagWhenReplacesValueMustExistInRefModel(manager),
                              new TagWhenSomeValueMustContainOneToken(manager),
                              new TagWhenUidPatternValueMustBeLowerCase(manager),
                              new TagWhenUidPatternValueShouldBeAtMost8Chars(manager),
                              new TagWhenXmlNameValueMustBeLowerCamelCase(manager),
                              new TagWhenXmlRefNameValueMustFollowAuthoring(manager),
                              new NotesChecker(manager,
                                               new NotesMustBeUnicode(manager),
                                               new NotesMustNotContainHtml(manager))));
    }
}