package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.misc.AbstractVersionMustBeAllowed;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfInterface;

public class InterfaceVersionMustBeAllowed extends AbstractVersionMustBeAllowed<MfInterface> {
    public static final String NAME = "I15";
    public static final String TITLE = "INTERFACE_VERSION_MUST_BE_ALLOWED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INTERFACE))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 4.1.3",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for "
                                                                         + AsdNames.S_EXTEND
                                                                         + " interfaces",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.1 for "
                                                                         + AsdNames.S_SELECT
                                                                         + " interfaces"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceVersionMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}