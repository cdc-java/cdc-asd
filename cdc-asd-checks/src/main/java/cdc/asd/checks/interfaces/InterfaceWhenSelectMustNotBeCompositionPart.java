package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.types.AbstractTypeMustNotPlayRole;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfTipRole;

public class InterfaceWhenSelectMustNotBeCompositionPart extends AbstractTypeMustNotPlayRole<MfInterface> {
    public static final String NAME = "I19";
    public static final String TITLE = "INTERFACE(SELECT)_MUST_NOT_BE_COMPOSITION_PART";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} must not be a {%wrap}.",
                                                                AsdNames.S_SELECT + WS + N_INTERFACE,
                                                                N_COMPOSITION + WS + N_PART)
                                                        .relatedTo(AsdRule.SELECT_NOT_COMPOSITION_SOURCE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public InterfaceWhenSelectMustNotBeCompositionPart(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE,
              MfComposition.class,
              MfTipRole.PART);
    }

    @Override
    public boolean accepts(MfInterface object) {
        return object.wrap(AsdElement.class).is(AsdStereotypeName.SELECT);
    }
}