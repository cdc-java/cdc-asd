package cdc.asd.checks.interfaces;

import java.util.List;

import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfQNameItem;
import cdc.mf.model.MfTypeOwner;

public class InterfacesChecker extends AbstractPartsChecker<MfTypeOwner, MfInterface, InterfacesChecker> {
    public InterfacesChecker(SnapshotManager manager) {
        super(manager,
              MfTypeOwner.class,
              MfInterface.class,
              new InterfaceChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends MfInterface>> getParts(MfTypeOwner object) {
        final List<MfInterface> delegates = object.getInterfaces()
                                                .stream()
                                                .sorted(MfQNameItem.QNAME_COMPARATOR)
                                                .toList();
        return LocatedObject.locate(delegates);
    }
}