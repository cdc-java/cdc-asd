package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesAreMandatory;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfInterface;

public class InterfaceNotesAreMandatory extends AbstractNotesAreMandatory<MfInterface> {
    public static final String NAME = "I08";
    public static final String TITLE = "INTERFACE_NOTES_ARE_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INTERFACE + S))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for "
                                                                + AsdNames.S_SELECT,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.1 for "
                                                                         + AsdNames.S_EXTEND),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceNotesAreMandatory(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}