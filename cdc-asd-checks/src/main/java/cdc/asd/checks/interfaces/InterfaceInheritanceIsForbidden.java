package cdc.asd.checks.interfaces;

import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfInterface;

public class InterfaceInheritanceIsForbidden extends MfAbstractRuleChecker<MfInterface> {
    public static final String NAME = "I03";
    public static final String TITLE = "INTERFACE_INHERITANCE_IS_FORBIDDEN";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("An {%wrap} must not extend another {%wrap}.",
                                                                N_INTERFACE,
                                                                N_INTERFACE)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for "
                                                                + AsdNames.S_SELECT + " interfaces",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.1 for "
                                                                         + AsdNames.S_EXTEND + " interfaces")
                                                        .relatedTo(AsdRule.NO_INTERFACE_EXTEND,
                                                                   AsdRule.NO_INTERFACE_SELECT),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    protected InterfaceInheritanceIsForbidden(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }

    @Override
    protected String getHeader(MfInterface object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfInterface object,
                             Location location) {
        final Set<MfInterface> set = object.getDirectAncestors(MfInterface.class);

        if (!set.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("cannot extend another interface")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}