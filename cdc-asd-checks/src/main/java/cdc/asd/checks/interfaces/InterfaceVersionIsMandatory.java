package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.misc.AbstractVersionIsMandatory;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfInterface;

public class InterfaceVersionIsMandatory extends AbstractVersionIsMandatory<MfInterface> {
    public static final String NAME = "I14";
    public static final String TITLE = "INTERFACE_VERSION_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(ALL, N_INTERFACE + S))
                                                        .appliesTo("All interfaces")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.1 for "
                                                                + AsdNames.S_SELECT + " interfaces",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.1 for "
                                                                         + AsdNames.S_EXTEND
                                                                         + " interfaces"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceVersionIsMandatory(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE);
    }
}