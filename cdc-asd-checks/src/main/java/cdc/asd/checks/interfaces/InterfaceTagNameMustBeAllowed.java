package cdc.asd.checks.interfaces;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameMustBeAllowed;
import cdc.mf.model.MfTag;

public class InterfaceTagNameMustBeAllowed extends AbstractTagNameMustBeAllowed {
    public static final String NAME = "I12";
    public static final String TITLE = "INTERFACE_TAG_NAME_MUST_BE_ALLOWED";

    /** Tags that can be associated to an interface. */
    private static final Set<AsdTagName> ALLOWED_TAGS =
            EnumSet.of(AsdTagName.XML_NAME,
                       AsdTagName.NOTE,
                       AsdTagName.EXAMPLE,
                       AsdTagName.REPLACES,
                       AsdTagName.REF);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INTERFACE))
                                                        .text(oneOf(AsdTagName::getLiteral, ALLOWED_TAGS))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.2 for <<select>>",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.3.2 for <<extend>>",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.4",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.1 for xmlName",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.6 for ref",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.8 for note",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.9 for example",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.10 for replaces"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public InterfaceTagNameMustBeAllowed(SnapshotManager manager) {
        super(manager,
              RULE);
    }

    @Override
    protected boolean isRecognizedAndNotAllowed(String tagName) {
        final AsdTagName tn = AsdTagName.of(tagName);
        return tn != null && !ALLOWED_TAGS.contains(tn);
    }

    @Override
    public boolean accepts(MfTag object) {
        return object.getParent().wrap(AsdElement.class).isInterface();
    }
}