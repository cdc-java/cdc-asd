package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.types.AbstractTypeMustNotPlayRole;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfTipRole;

public class InterfaceWhenExtendMustNotBeAssociationTarget extends AbstractTypeMustNotPlayRole<MfInterface> {
    public static final String NAME = "I16";
    public static final String TITLE = "INTERFACE(EXTEND)_MUST_NOT_BE_ASSOCIATION_TARGET";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("An {%wrap} must not be an {%wrap} {%wrap}.",
                                                                AsdNames.S_EXTEND + WS + N_INTERFACE,
                                                                N_ASSOCIATION,
                                                                N_TARGET)
                                                        .relatedTo(AsdRule.EXTEND_NOT_ASSOCIATION_TARGET)
                                                        .remarks("Does this apply to compositions and aggregations?"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public InterfaceWhenExtendMustNotBeAssociationTarget(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE,
              MfAssociation.class,
              MfTipRole.TARGET);
    }

    @Override
    public boolean accepts(MfInterface item) {
        return item.wrap(AsdElement.class).getStereotypeName() == AsdStereotypeName.EXTEND;
    }
}