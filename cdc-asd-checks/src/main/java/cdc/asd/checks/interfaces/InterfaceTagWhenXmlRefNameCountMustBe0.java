package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfInterface;

public class InterfaceTagWhenXmlRefNameCountMustBe0 extends AbstractTagNameCountMustMatch<MfInterface> {
    public static final String NAME = "I13";
    public static final String TITLE = "INTERFACE_TAG(XML_REF_NAME)_COUNT_MUST_BE_0";
    private static final AsdTagName TAG_NAME = AsdTagName.XML_REF_NAME;
    private static final int MINMAX = 0;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AN, N_INTERFACE, TAG_NAME, MINMAX)),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected InterfaceTagWhenXmlRefNameCountMustBe0(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }
}