package cdc.asd.checks.interfaces;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfInterface;

public class InterfaceWhenSelectTagWhenXmlNameCountMustBe1 extends AbstractTagNameCountMustMatch<MfInterface> {
    public static final String NAME = "I20";
    public static final String TITLE = "INTERFACE(SELECT)_TAG(XML_NAME)_COUNT_MUST_BE_1";
    private static final AsdTagName TAG_NAME = AsdTagName.XML_NAME;
    private static final int MINMAX = 1;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(A,
                                                                       AsdNames.S_SELECT + " " + N_INTERFACE,
                                                                       TAG_NAME,
                                                                       MINMAX))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.5.2.2")
                                                        .relatedTo(AsdRule.SELECT_XML_NAME),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    protected InterfaceWhenSelectTagWhenXmlNameCountMustBe1(SnapshotManager manager) {
        super(manager,
              MfInterface.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfInterface object) {
        return object.wrap(AsdElement.class).getStereotypeName() == AsdStereotypeName.SELECT;
    }
}