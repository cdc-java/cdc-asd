package cdc.asd.checks.packages;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameMustBeAllowed;
import cdc.mf.model.MfTag;

public class PackageWhenSomeMustNotHaveAnyTag extends AbstractTagNameMustBeAllowed {
    public static final String NAME = "P05";
    public static final String TITLE = "PACKAGE(SOME)_MUST_NOT_HAVE_ANY_TAG";

    /** Tags that can be associated to a package. */
    private static final Set<AsdTagName> ALLOWED_TAGS =
            EnumSet.noneOf(AsdTagName.class);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} and {%wrap} cannot have any {%wrap}.",
                                                                AsdNames.S_DOMAIN + WS + N_PACKAGE + S,
                                                                AsdNames.S_FUNCTIONAL_AREA + WS + N_PACKAGE + S,
                                                                N_TAG)
                                                        .appliesTo("All " + AsdNames.S_DOMAIN + " packages",
                                                                   "All " + AsdNames.S_FUNCTIONAL_AREA + " packages.")
                                                        .relatedTo(AsdRule.PACKAGE_NO_TAGS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public PackageWhenSomeMustNotHaveAnyTag(SnapshotManager context) {
        super(context,
              RULE);
    }

    @Override
    protected boolean isRecognizedAndNotAllowed(String tagName) {
        final AsdTagName tn = AsdTagName.of(tagName);
        return tn != null && !ALLOWED_TAGS.contains(tn);
    }

    @Override
    public boolean accepts(MfTag object) {
        final AsdStereotypeName stereotypeName = object.getParent().wrap(AsdElement.class).getStereotypeName();

        return stereotypeName == AsdStereotypeName.DOMAIN
                || stereotypeName == AsdStereotypeName.FUNCTIONAL_AREA;
    }
}