package cdc.asd.checks.packages;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameMustMatchPattern;
import cdc.mf.model.MfPackage;

public class PackageWhenSomeNameMustBeCapitalCase extends AbstractNameMustMatchPattern<MfPackage> {
    public static final String NAME = "P06";
    public static final String TITLE = "PACKAGE(SOME)_NAME_MUST_BE_CAPITAL_CASE";
    /** Practically, it seems names of packages are quite free. */
    public static final String REGEX = "^[A-Z][A-Za-z0-9]*( (and|or|[A-Z][A-Za-z0-9]*))*$";

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("Some {%wrap} {%wrap}, when defined," + MUST_MATCH_THIS_PATTERN
                                      + REGEX,
                                                                N_PACKAGE,
                                                                N_NAME + S)
                                                        .text("\nThey must be Capital Case.")
                                                        .appliesTo("All " + AsdNames.S_UOF + WS + N_PACKAGE + S,
                                                                   "All packages that have no stereotype but are UoF, excluding packages whose name contains 'Primitives', 'Compound_Attributes' or 'Base_Object_Definition',",
                                                                   "All " + AsdNames.S_DOMAIN + WS + N_PACKAGE + S,
                                                                   "All " + AsdNames.S_FUNCTIONAL_AREA + WS + N_PACKAGE + S)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 6.3.1 for UoF",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 7.2 for Functional Area",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 7.3 for Domain")
                                                        .relatedTo(AsdRule.PACKAGE_NAME)
                                                        .remarks("'Product and Package' is not strictly Capital Case, but the pattern accepts it."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public PackageWhenSomeNameMustBeCapitalCase(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE,
              REGEX);
    }

    private static boolean isBasic(String name) {
        return name.contains("Primitives")
                || name.contains("Compound_Attributes")
                || name.contains("Base_Object_Definition");
    }

    @Override
    public boolean accepts(MfPackage object) {
        final String name = object.getName() == null ? "" : object.getName();
        final AsdPackage wrapper = object.wrap(AsdPackage.class);
        return (wrapper.isUof() && !isBasic(name))
                || wrapper.isDomain()
                || wrapper.isFunctionalArea();
    }
}