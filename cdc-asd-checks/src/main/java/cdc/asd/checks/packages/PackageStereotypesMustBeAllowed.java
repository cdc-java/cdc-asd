package cdc.asd.checks.packages;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.stereotypes.AbstractStereotypesMustBeAllowed;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfPackage;

public class PackageStereotypesMustBeAllowed extends AbstractStereotypesMustBeAllowed<MfPackage> {
    public static final String NAME = "P03";
    public static final String TITLE = "PACKAGE_STEREOTYPES_MUST_BE_ALLOWED";

    private static final Set<AsdStereotypeName> ALLOWED_STEREOTYPES =
            EnumSet.of(AsdStereotypeName.DOMAIN,
                       AsdStereotypeName.FUNCTIONAL_AREA,
                       AsdStereotypeName.UOF);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_PACKAGE))
                                                        .text(oneOf(ALLOWED_STEREOTYPES))
                                                        .appliesTo("All packages except <<builtin>> package")
                                                        .relatedTo(AsdRule.PACKAGE_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public PackageStereotypesMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE);
    }

    @Override
    protected boolean isAllowed(MfPackage object,
                                AsdStereotypeName stereotypeName) {
        return ALLOWED_STEREOTYPES.contains(stereotypeName);
    }

    @Override
    public boolean accepts(MfPackage object) {
        return !object.wrap(AsdPackage.class).isBuiltin();
    }
}