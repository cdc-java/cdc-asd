package cdc.asd.checks.packages;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.stereotypes.AbstractStereotypesAreForbidden;
import cdc.mf.model.MfPackage;

public class PackageWhenSomeStereotypesAreForbidden extends AbstractStereotypesAreForbidden<MfPackage> {
    public static final String NAME = "P08";
    public static final String TITLE = "PACKAGE(SOME)_STEREOTYPE_IS_FORBIDDEN";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_PACKAGE + S))
                                                        .appliesTo("All root packages, except the <<builtin>> package")
                                                        .relatedTo(AsdRule.PACKAGE_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public PackageWhenSomeStereotypesAreForbidden(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE);
    }

    @Override
    public boolean accepts(MfPackage object) {
        return object.wrap(AsdPackage.class).isRoot()
                && !object.wrap(AsdPackage.class).isBuiltin();
    }
}