package cdc.asd.checks.packages;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.stereotypes.AbstractStereotypeIsMandatory;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfPackageOwner;

public class PackageWhenSomeStereotypeIsMandatory extends AbstractStereotypeIsMandatory<MfPackage> {
    public static final String NAME = "P07";
    public static final String TITLE = "PACKAGE(SOME)_STEREOTYPE_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_PACKAGE + "s"))
                                                        .appliesTo("All packages whose parent package has a stereotype")
                                                        .relatedTo(AsdRule.PACKAGE_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public PackageWhenSomeStereotypeIsMandatory(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE);
    }

    @Override
    public boolean accepts(MfPackage object) {
        final MfPackageOwner parent = object.getParent();
        if (parent instanceof final MfPackage p) {
            return p.wrap(AsdElement.class).getStereotype() != null;
        } else {
            return false;
        }
    }
}