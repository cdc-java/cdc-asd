package cdc.asd.checks.packages;

import java.util.List;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesMustStartWithName;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfPackage;

/**
 * Check that the notes of a package start with its name.
 *
 * @author Damien Carbonne
 */
public class PackageWhenUofNotesMustStartWithName extends AbstractNotesMustStartWithName<MfPackage> {
    public static final String NAME = "P09";
    public static final String TITLE = "PACKAGE(UOF)_NOTES_MUST_START_WITH_NAME";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_PACKAGE))
                                                        .text("\nIt should start with 'The xxx UoF' or 'xxx UoF'.")
                                                        .appliesTo("All " + AsdNames.S_UOF + " packages",
                                                                   "All packages without any stereotype but are however UoF.")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 6.7")
                                                        .relatedTo(AsdRule.PACKAGE_DEFINITION_AUTHORING)
                                                        .remarks("Rule was relaxed when compared to "
                                                                + AsdNames.SOURCE_UMLWRSG_2_0),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public PackageWhenUofNotesMustStartWithName(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE);
    }

    @Override
    protected List<String> cleanName(String name) {
        return List.of("The " + PackageUtils.cleanName(name) + " UoF",
                       PackageUtils.cleanName(name) + " UoF");
    }

    @Override
    public boolean accepts(MfPackage object) {
        return object.wrap(AsdPackage.class).isUof();
    }
}