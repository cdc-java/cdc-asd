package cdc.asd.checks.packages;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class PackageUtils {
    private PackageUtils() {
    }

    private static final Pattern PATTERN_SPEC = Pattern.compile("^(CDM|S1000D|S2000M|S3000L|S4000P|S5000F|S6000T|SX000i) (UoF )?");
    private static final Pattern PATTERN_VERSION = Pattern.compile("[ _]*\\d[-]\\d_\\d{3}[-]\\d{2}");

    public static String cleanName(String name) {
        String s = name;
        final Matcher m1 = PATTERN_SPEC.matcher(name);
        s = m1.replaceAll("");
        final Matcher m2 = PATTERN_VERSION.matcher(s);
        s = m2.replaceAll("");
        return s;
    }
}