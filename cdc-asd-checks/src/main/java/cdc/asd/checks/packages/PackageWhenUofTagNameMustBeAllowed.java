package cdc.asd.checks.packages;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameMustBeAllowed;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfTag;

/**
 * Check that the name of a package tag is allowed.
 *
 * @author Damien Carbonne
 */
public class PackageWhenUofTagNameMustBeAllowed extends AbstractTagNameMustBeAllowed {
    public static final String NAME = "P10";
    public static final String TITLE = "PACKAGE(UOF)_TAG_NAME_MUST_BE_ALLOWED";

    /** Tags that can be associated to a package. */
    private static final Set<AsdTagName> ALLOWED_TAGS =
            EnumSet.of(AsdTagName.LONG_DESCRIPTION,
                       AsdTagName.ICN,
                       AsdTagName.REPLACES);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AsdNames.S_UOF + " or no stereotype " + N_PACKAGE))
                                                        .text(oneOf(AsdTagName::getLiteral, ALLOWED_TAGS))
                                                        .appliesTo("All " + AsdNames.S_UOF + " packages",
                                                                   "All package without any stereotype but are however UoF.")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 6.3.1 (ICN)",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 6.7 (long description)")
                                                        .relatedTo(AsdRule.UOF_TAGS)
                                                        .remarks("NoPrint is ignored. As a tag it is not described in "
                                                                + AsdNames.SOURCE_UMLWRSG_2_0 + ".",
                                                                 "There is a <<noPrint>> stereotype.",
                                                                 "replaces seems to be usable with classes and interfaces according to "
                                                                         + AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.10."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public PackageWhenUofTagNameMustBeAllowed(SnapshotManager manager) {
        super(manager,
              RULE);
    }

    @Override
    protected boolean isRecognizedAndNotAllowed(String tagName) {
        final AsdTagName tn = AsdTagName.of(tagName);
        return tn != null && !ALLOWED_TAGS.contains(tn);
    }

    @Override
    public boolean accepts(MfTag object) {
        return ((MfPackage) object.getParent()).wrap(AsdPackage.class).isUof();
    }
}