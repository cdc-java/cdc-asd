package cdc.asd.checks.packages;

import cdc.asd.checks.classes.ClassesChecker;
import cdc.asd.checks.diagrams.DiagramsChecker;
import cdc.asd.checks.interfaces.InterfacesChecker;
import cdc.asd.checks.misc.SiblingsMustHaveDifferentNames;
import cdc.asd.checks.notes.NotesChecker;
import cdc.asd.checks.notes.NotesMustBeUnicode;
import cdc.asd.checks.notes.NotesMustNotContainHtml;
import cdc.asd.checks.stereotypes.StereotypeMustBeRecognized;
import cdc.asd.checks.tags.TagNameIsMandatory;
import cdc.asd.checks.tags.TagNameMustBeRecognized;
import cdc.asd.checks.tags.TagValueMustBeUnicode;
import cdc.asd.checks.tags.TagWhenLongDescriptionNotesMustHaveValidMarkup;
import cdc.asd.checks.tags.TagWhenReplacesValueMustExistInRefModel;
import cdc.asd.checks.tags.TagsChecker;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.LazyChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.mf.model.MfPackage;

public class PackageChecker extends CompositeChecker<MfPackage> {
    public PackageChecker(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              new PackageNameIsMandatory(manager),
              new PackageNotesAreMandatory(manager),
              new PackageStereotypesMustBeAllowed(manager),
              new PackageWhenUofNotesMustStartWithName(manager),
              new PackageWhenSomeMustNotContainTypes(manager),
              new PackageWhenSomeNameMustBeCapitalCase(manager),
              new PackageWhenSomeStereotypesAreForbidden(manager),
              new PackageWhenSomeStereotypeIsMandatory(manager),
              new NotesChecker(manager,
                               new NotesMustBeUnicode(manager),
                               new NotesMustNotContainHtml(manager)),
              new StereotypeMustBeRecognized(manager),
              new ClassesChecker(manager),
              new DiagramsChecker(manager),
              new InterfacesChecker(manager),
              new LazyChecker<>(manager, MfPackage.class, PackagesChecker.KEY),
              new SiblingsMustHaveDifferentNames(manager),
              new TagsChecker(manager,
                              new PackageWhenSomeMustNotHaveAnyTag(manager),
                              new PackageWhenUofTagNameMustBeAllowed(manager),
                              new TagNameIsMandatory(manager),
                              new TagNameMustBeRecognized(manager),
                              new TagValueMustBeUnicode(manager),
                              new TagWhenLongDescriptionNotesMustHaveValidMarkup(manager),
                              new TagWhenReplacesValueMustExistInRefModel(manager),
                              new NotesChecker(manager,
                                               new NotesMustBeUnicode(manager),
                                               new NotesMustNotContainHtml(manager))));
    }
}