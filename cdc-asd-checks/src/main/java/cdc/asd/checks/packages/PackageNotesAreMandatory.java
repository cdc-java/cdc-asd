package cdc.asd.checks.packages;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesAreMandatory;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfPackage;

public class PackageNotesAreMandatory extends AbstractNotesAreMandatory<MfPackage> {
    public static final String NAME = "P02";
    public static final String TITLE = "PACKAGE_NOTES_ARE_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_PACKAGE + S))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 6.1")
                                                        .relatedTo(AsdRule.PACKAGE_DEFINITION),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public PackageNotesAreMandatory(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE);
    }

    @Override
    public boolean accepts(MfPackage object) {
        return !object.wrap(AsdPackage.class).isRoot();
    }
}