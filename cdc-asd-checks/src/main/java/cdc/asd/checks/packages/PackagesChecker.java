package cdc.asd.checks.packages;

import java.util.List;

import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfPackageOwner;
import cdc.mf.model.MfQNameItem;

public class PackagesChecker extends AbstractPartsChecker<MfPackageOwner, MfPackage, PackagesChecker> {
    public static final String KEY = "PackagesChecker";

    public PackagesChecker(SnapshotManager manager) {
        super(manager,
              MfPackageOwner.class,
              MfPackage.class,
              new PackageChecker(manager));
        manager.register(KEY, this);
    }

    @Override
    protected List<LocatedObject<? extends MfPackage>> getParts(MfPackageOwner object) {
        final List<MfPackage> delegates = object.getPackages()
                                              .stream()
                                              .sorted(MfQNameItem.QNAME_COMPARATOR)
                                              .toList();
        return LocatedObject.locate(delegates);
    }
}