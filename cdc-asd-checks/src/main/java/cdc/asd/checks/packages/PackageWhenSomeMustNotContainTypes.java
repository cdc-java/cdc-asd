package cdc.asd.checks.packages;

import java.util.List;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfType;

public class PackageWhenSomeMustNotContainTypes extends MfAbstractRuleChecker<MfPackage> {
    public static final String NAME = "P04";
    public static final String TITLE = "PACKAGE(SOME)_MUST_NOT_CONTAIN_TYPES";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} or {%wrap} cannot contain any {%wrap} or {%wrap} declarations.",
                                                                AsdNames.S_DOMAIN + WS + N_PACKAGE,
                                                                AsdNames.S_FUNCTIONAL_AREA + WS + N_PACKAGE,
                                                                N_CLASS,
                                                                N_INTERFACE)
                                                        .relatedTo(AsdRule.CLASS_IN_UOF),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected PackageWhenSomeMustNotContainTypes(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE);
    }

    @Override
    protected String getHeader(MfPackage object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfPackage object,
                             Location location) {
        final List<MfType> types = object.getTypes();

        if (!types.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("cannot contain classes or interfaces.");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfPackage object) {
        final AsdStereotypeName stereotypeName = object.wrap(AsdElement.class).getStereotypeName();
        return stereotypeName == AsdStereotypeName.DOMAIN
                || stereotypeName == AsdStereotypeName.FUNCTIONAL_AREA;
    }
}