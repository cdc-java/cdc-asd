package cdc.asd.checks.packages;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameIsMandatory;
import cdc.mf.model.MfPackage;

public class PackageNameIsMandatory extends AbstractNameIsMandatory<MfPackage> {
    public static final String NAME = "P01";
    public static final String TITLE = "PACKAGE_NAME_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_PACKAGE + S))
                                                        .appliesTo("All packages"),

                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public PackageNameIsMandatory(SnapshotManager manager) {
        super(manager,
              MfPackage.class,
              RULE);
    }
}