package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfType;

public class AttributeTypeMustBeAllowed extends MfAbstractRuleChecker<MfProperty> {
    public static final String NAME = "A17";
    public static final String TITLE = "ATTRIBUTE_TYPE_MUST_BE_ALLOWED";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("The {%wrap} of an {%wrap} must be a {%wrap}, {%wrap}, {%wrap}, {%wrap} or {%wrap}.",
                                                                N_TYPE,
                                                                N_ATTRIBUTE,
                                                                AsdNames.S_PRIMITIVE + WS + N_CLASS,
                                                                AsdNames.S_UML_PRIMITIVE + WS + N_CLASS,
                                                                AsdNames.S_BUILTIN + WS + N_CLASS,
                                                                AsdNames.S_CLASS + WS + N_CLASS,
                                                                AsdNames.S_COMPOUND_ATTRIBUTE + WS + N_CLASS)
                                                        .appliesTo("Any valid type of an attribute")
                                                        .relatedTo(AsdRule.ATTRIBUTE_TYPE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public AttributeTypeMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty object) {
        return getTheTypeOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfProperty object,
                             Location location) {
        final MfType type = object.getType();
        final AsdStereotypeName stereotypeName = type.wrap(AsdElement.class).getStereotypeName();
        // We need to allow builtin, till one does not need to create them
        if (!(stereotypeName == AsdStereotypeName.PRIMITIVE
                || stereotypeName == AsdStereotypeName.COMPOUND_ATTRIBUTE
                || stereotypeName == AsdStereotypeName.UML_PRIMITIVE
                || stereotypeName == AsdStereotypeName.CLASS
                || stereotypeName == AsdStereotypeName.BUILTIN)) {

            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(type.getName())
                       .violation("is not allowed");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfProperty object) {
        return object.hasValidType();
    }
}