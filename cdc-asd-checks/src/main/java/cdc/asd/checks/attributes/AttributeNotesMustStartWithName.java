package cdc.asd.checks.attributes;

import java.util.List;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesMustStartWithName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfProperty;

public class AttributeNotesMustStartWithName extends AbstractNotesMustStartWithName<MfProperty> {
    public static final String TITLE = "ATTRIBUTE_NOTES_MUST_START_WITH_NAME";
    public static final String NAME = "A07";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE))
                                                        .appliesTo("All attributes notes")
                                                        .relatedTo(AsdRule.ATTRIBUTE_DEFINITION_AUTHORING),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public AttributeNotesMustStartWithName(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected List<String> cleanName(String name) {
        return List.of(name);
    }
}