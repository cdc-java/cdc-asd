package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.visibility.AbstractVisibilityMustBePublic;
import cdc.mf.model.MfProperty;

public class AttributeVisibilityMustBePublic extends AbstractVisibilityMustBePublic<MfProperty> {
    public static final String NAME = "A18";
    public static final String TITLE = "ATTRIBUTE_VISIBILITY_MUST_BE_PUBLIC";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(THE, N_ATTRIBUTE))
                                                        .appliesTo("All attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_VISIBLE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeVisibilityMustBePublic(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty object) {
        return getTheItemHeader(object);
    }
}