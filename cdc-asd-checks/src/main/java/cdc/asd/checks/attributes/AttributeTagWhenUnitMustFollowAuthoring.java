package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagValueMustMatchPattern;
import cdc.mf.model.MfTag;

public class AttributeTagWhenUnitMustFollowAuthoring extends AbstractTagValueMustMatchPattern {
    public static final String NAME = "A11";
    public static final String TITLE = "ATTRIBUTE_TAG(UNIT)_MUST_FOLLOW_AUTHORING";
    public static final String REGEX = NO_SPACES_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap} {%wrap}"
                                      + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                N_ATTRIBUTE,
                                                                AsdNames.T_UNIT + WS + N_TAG,
                                                                N_VALUE + S)
                                                        .text("\nThey must neither contain spaces nor be empty.")
                                                        .appliesTo("The value of all " + AsdNames.T_UNIT
                                                                + " tags of all attributes")
                                                        .relatedTo(AsdRule.UNIT_TYPE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public AttributeTagWhenUnitMustFollowAuthoring(SnapshotManager manager) {
        super(manager,
              RULE,
              REGEX);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.UNIT;
    }
}