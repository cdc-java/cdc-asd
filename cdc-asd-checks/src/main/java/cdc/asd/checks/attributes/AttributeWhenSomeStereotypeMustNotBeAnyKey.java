package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.stereotypes.AbstractStereotypeMustNotBeAnyKey;
import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfProperty;

public class AttributeWhenSomeStereotypeMustNotBeAnyKey extends AbstractStereotypeMustNotBeAnyKey<MfProperty> {
    public static final String NAME = "A22";
    public static final String TITLE = "ATTRIBUTE(SOME)_STEREOTYPE_MUST_NOT_BE_ANY_KEY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ATTRIBUTE + "s"))
                                                        .text("\nOnly IdentifierType, ClassificationType and NameType attributes can have a <<key>>, <<compositeKey>> or <<relationshipKey>> stereotype.")
                                                        .appliesTo("All attributes that are neither IdentifierType, ClassificationType nor NameType")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0
                                                                + " 11.2.2.1 excludes DescriptorType with <<key>> or <<compositeKey>>.",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0
                                                                         + " 11.2.2.2 excludes DescriptorType with <<relationshipKey>>.")
                                                        .relatedTo(AsdRule.ATTRIBUTE_KEYS)
                                                        .remarks("Added NameType to ASD rule.",
                                                                 "umlString, Organization, DateTimeType are key attributes in GlobalPosition, StreetAddress, ...: are they valid exceptions?"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public AttributeWhenSomeStereotypeMustNotBeAnyKey(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    public boolean accepts(MfProperty object) {
        final AsdPrimitiveTypeName primitive = object.wrap(AsdProperty.class).getPrimitiveTypeName();
        return primitive != AsdPrimitiveTypeName.IDENTIFIER
                && primitive != AsdPrimitiveTypeName.CLASSIFICATION
                && primitive != AsdPrimitiveTypeName.NAME; // Added
    }
}