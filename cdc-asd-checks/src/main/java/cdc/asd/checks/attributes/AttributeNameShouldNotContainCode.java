package cdc.asd.checks.attributes;

import java.util.List;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameMustNotMatchPattern;
import cdc.mf.model.MfProperty;

public class AttributeNameShouldNotContainCode extends AbstractNameMustNotMatchPattern<MfProperty> {
    public static final String NAME = "A04";
    public static final String TITLE = "ATTRIBUTE_NAME_SHOULD_NOT_CONTAIN_CODE";
    public static final IssueSeverity SEVERITY = IssueSeverity.MINOR;
    public static final String REGEX = "^.*Code.*$";
    public static final List<String> EXCEPTIONS =
            List.of("disassemblyCode",
                    "disassemblyCodeVariant",
                    "exportControlPartyClearanceCode",
                    "exportControlRegulationLegalCode",
                    "extensionCode",
                    "informationCode",
                    "informationCodeVariant",
                    "itemLocationCode",
                    "learnCode",
                    "learnEventCode",
                    "materialItemCategoryCode",
                    "modelIdentificationCode",
                    "postalCode",
                    "skillCode",
                    "subtaskInformationCode",
                    "systemDifferenceCode",
                    "taskInformationCode",
                    "taskRequirementInformationCode");
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("No defined {%wrap} {%wrap} should match this pattern: " + REGEX,
                                                                N_ATTRIBUTE,
                                                                N_NAME)
                                                        .text("\nThey should not contain 'Code', except if they come from another standard or specification.")
                                                        .appliesTo("All defined attribute names")
                                                        .exceptions(EXCEPTIONS)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_NAME_NO_CODE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeNameShouldNotContainCode(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              REGEX,
              EXCEPTIONS);
    }
}