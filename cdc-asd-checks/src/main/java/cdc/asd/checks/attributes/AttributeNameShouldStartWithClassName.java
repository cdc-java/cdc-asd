package cdc.asd.checks.attributes;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfMemberOwner;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTipRole;
import cdc.mf.model.MfType;

public class AttributeNameShouldStartWithClassName extends MfAbstractRuleChecker<MfProperty> {
    public static final String NAME = "A05";
    public static final String TITLE = "ATTRIBUTE_NAME_SHOULD_START_WITH_CLASS_NAME";
    public static final IssueSeverity SEVERITY = IssueSeverity.INFO;

    public static final Set<AsdStereotypeName> EXCLUDED_STEREOTYPES =
            Set.of(AsdStereotypeName.UML_PRIMITIVE,
                   AsdStereotypeName.PRIMITIVE,
                   AsdStereotypeName.COMPOUND_ATTRIBUTE,
                   AsdStereotypeName.ATTRIBUTE_GROUP);

    public static final Set<String> EXCLUDED_CLASSES =
            Set.of(
                   "BaseObject",
                   "BreakdownElement",
                   "BreakdownElementRevision",
                   "BreakdownElementStructure",
                   "BreakdownElementUsageInBreakdown",
                   "DegradationMechanismAcceptanceCriteria",
                   "FailureModeSymptomsSignature",
                   "GlobalPosition",
                   "HardwareElementAttachingConnector",
                   "InstalledPart",
                   "ParameterThresholdDefinition",
                   "PartDefinitionPartsListEntry",
                   "PartDefinitionPartsListRevision",
                   "S1000DDataModule",
                   "S1000DDataModuleIssue",
                   "S1000DPublicationModule",
                   "S1000DPublicationModuleIssue",
                   "SerializedProductVariantConfigurationConformance",
                   "StreetAddress",
                   "Task",
                   "TaskRevision");

    public static final Set<String> EXCLUDED_ATTRIBUTES =
            Set.of("allowedProductConfigurationIdentifier",
                   "applicabilityDateRange",
                   "applicableSerialNumberRange",
                   "circuitBreakerState",
                   "evaluationByAssertionRole",
                   "eventThresholdNumberOfEventOccurrences",
                   "packagedTask",
                   "partDefinitionIdentifier",
                   "partIdentifier",
                   "partName",
                   "partsListType",
                   "productDefinitionIdentifier",
                   "productVariantDefinitionIdentifier",
                   "sourceDocumentPortion",
                   "specialEventOccurenceRatio",
                   "workItemTimelineEvent",
                   "workItemTimelineLag");

    private static final List<String> EXCEPTIONS =
            Stream.concat(Stream.concat(EXCLUDED_STEREOTYPES.stream()
                                                            .sorted()
                                                            .map(s -> "attributes of <<" + s.getLiteral() + ">> classes"),
                                        EXCLUDED_CLASSES.stream().sorted().map(s -> "attributes of " + s)),
                          EXCLUDED_ATTRIBUTES.stream().sorted())
                  .toList();

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("The {%wrap} of an {%wrap} should start with the {%wrap} of its {%wrap}, in lowerCamelCase."
                                      + "\nIt can also start with the name of another closely related class (ancestor, composition owner, ...) ",
                                                                N_NAME,
                                                                N_ATTRIBUTE,
                                                                N_NAME,
                                                                N_CLASS)
                                                        .appliesTo("All class attributes")
                                                        .exceptions(EXCEPTIONS)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1 (recommendation).")
                                                        .relatedTo(AsdRule.ATTRIBUTE_NAME_CLASS)
                                                        .remarks(AsdNames.SOURCE_UMLWRSG_2_0
                                                                + " recommends this naming, whilst "
                                                                + AsdRule.ATTRIBUTE_NAME_CLASS + " requires it.",
                                                                 "There are many cases of non-compliance with this rule.",
                                                                 "Should this be applied to attributes of all classes?"),
                              SEVERITY)
                        .labels(AsdLabels.CONTRADICTION)
                        .meta(Metas.SINCE, "0.12.0")
                        .build();

    public AttributeNameShouldStartWithClassName(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty object) {
        return getTheNameOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfProperty object,
                             Location location) {
        final String attributeName = object.getName();
        final String ownerName = object.getParent().getName();
        final String ccOwnerName = toCamelCase(ownerName);

        if (attributeName != null
                && !startsWithParentName(object)
                && !startsWithDirectAncestorName(object)
                && !startsWithCompositionWholeName(object)) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(attributeName)
                       .violation("does not start with '" + ccOwnerName + "' or the name of another closely related class.");
            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfProperty object) {
        return !(EXCLUDED_CLASSES.contains(object.getParent().getName())
                || EXCLUDED_STEREOTYPES.contains(object.getParent().wrap(AsdElement.class).getStereotypeName())
                || EXCLUDED_ATTRIBUTES.contains(object.getName()));
    }

    private static boolean startsWithParentName(MfProperty property) {
        final String attributeName = property.getName();
        final String ownerName = property.getParent().getName();
        return ownerName != null && attributeName.startsWith(toCamelCase(ownerName));
    }

    private static boolean startsWithDirectAncestorName(MfProperty property) {
        final String attributeName = property.getName();
        if (property.getParent() instanceof final MfClass c) {
            for (final MfClass ancestor : c.getDirectAncestors(MfClass.class)) {
                if (ancestor.hasName() && attributeName.startsWith(toCamelCase(ancestor.getName()))) {
                    return true;
                }
            }
        }
        return false;
    }

    private static boolean startsWithCompositionWholeName(MfProperty property) {
        final String attributeName = property.getName();
        final MfMemberOwner owner = property.getParent();
        if (owner instanceof final MfType type) {
            for (final MfComposition composition : type.getAllReversedConnectors(MfComposition.class)) {
                final MfType whole = composition.getTip(MfTipRole.WHOLE).getType();
                if (whole.hasName() && attributeName.startsWith(toCamelCase(whole.getName()))) {
                    return true;
                }
            }
        }
        return false;
    }

    private static String toCamelCase(String s) {
        if (s == null || s.isEmpty()) {
            return s;
        } else {
            return Character.toLowerCase(s.charAt(0)) + s.substring(1);
        }
    }
}