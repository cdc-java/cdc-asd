package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameMustMatchPattern;
import cdc.mf.model.MfProperty;

public class AttributeNameMustBeLowerCamelCase extends AbstractNameMustMatchPattern<MfProperty> {
    public static final String NAME = "A03";
    public static final String TITLE = "ATTRIBUTE_NAME_MUST_BE_LOWER_CAMEL_CASE";
    public static final String REGEX = LOWER_CAMEL_CASE_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All defined {%wrap} {%wrap}" + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                N_ATTRIBUTE,
                                                                N_NAME + S)
                                                        .text("\nThey must be lowerCamelCase.")
                                                        .appliesTo("All defined attribute names")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 13.1.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_NAME_CASE,
                                                                   AsdRule.ATTRIBUTE_NAME_NO_SPACE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeNameMustBeLowerCamelCase(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              REGEX);
    }
}