package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameIsMandatory;
import cdc.mf.model.MfProperty;

public class AttributeNameIsMandatory extends AbstractNameIsMandatory<MfProperty> {
    public static final String NAME = "A02";
    public static final String TITLE = "ATTRIBUTE_NAME_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE + S))
                                                        .appliesTo("All attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_NAME_CASE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeNameIsMandatory(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }
}