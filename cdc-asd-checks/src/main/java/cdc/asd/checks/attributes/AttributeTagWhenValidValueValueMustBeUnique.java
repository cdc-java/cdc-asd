package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagValuesMustBeUnique;
import cdc.mf.model.MfProperty;

public class AttributeTagWhenValidValueValueMustBeUnique extends AbstractTagValuesMustBeUnique<MfProperty> {
    public static final String NAME = "A14";
    public static final String TITLE = "ATTRIBUTE_TAG(VALID_VALUE)_VALUE_MUST_BE_UNIQUE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE, AsdTagName.VALID_VALUE))
                                                        .relatedTo(AsdRule.VALID_VALUE_CODE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public AttributeTagWhenValidValueValueMustBeUnique(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              AsdTagName.VALID_VALUE);
    }
}