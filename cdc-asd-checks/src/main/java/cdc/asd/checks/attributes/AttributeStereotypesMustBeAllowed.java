package cdc.asd.checks.attributes;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.stereotypes.AbstractStereotypesMustBeAllowed;
import cdc.asd.model.AsdStereotypeName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfProperty;

public class AttributeStereotypesMustBeAllowed extends AbstractStereotypesMustBeAllowed<MfProperty> {
    public static final String NAME = "A09";
    public static final String TITLE = "ATTRIBUTE_STEREOTYPES_MUST_BE_ALLOWED";

    /** Stereotypes that can be used with attributes (11.2.2). */
    private static final Set<AsdStereotypeName> ALLOWED_STEREOTYPES =
            EnumSet.of(AsdStereotypeName.CHARACTERISTIC,
                       AsdStereotypeName.COMPOSITE_KEY,
                       AsdStereotypeName.KEY,
                       AsdStereotypeName.METADATA,
                       AsdStereotypeName.RELATIONSHIP_KEY);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE))
                                                        .text(oneOf(ALLOWED_STEREOTYPES))
                                                        .appliesTo("All recognized stereotypes of all attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.2")
                                                        .relatedTo(AsdRule.ATTRIBUTE_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeStereotypesMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected boolean isAllowed(MfProperty object,
                                AsdStereotypeName stereotypeName) {
        return ALLOWED_STEREOTYPES.contains(stereotypeName);
    }
}