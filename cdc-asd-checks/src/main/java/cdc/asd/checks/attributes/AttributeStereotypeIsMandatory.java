package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.stereotypes.AbstractStereotypeIsMandatory;
import cdc.mf.model.MfProperty;

public class AttributeStereotypeIsMandatory extends AbstractStereotypeIsMandatory<MfProperty> {
    public static final String NAME = "A08";
    public static final String TITLE = "ATTRIBUTE_STEREOTYPE_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(ALL, N_ATTRIBUTE + S))
                                                        .appliesTo("All attributes of classes that are neither BaseObject, <<primitive>>, <<umlPrimitive>> or <<compoundAttribute>> classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_STEREOTYPES)
                                                        .remarks(AsdNames.SOURCE_UMLWRSG_2_0 + " should be fixed.",
                                                                 AsdRule.ATTRIBUTE_STEREOTYPES + " should be fixed."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeStereotypeIsMandatory(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    public boolean accepts(MfProperty object) {
        final AsdElement parent = object.getParent().wrap(AsdElement.class);
        return !(parent.isBaseObject()
                || parent.is(AsdStereotypeName.PRIMITIVE)
                || parent.is(AsdStereotypeName.UML_PRIMITIVE)
                || parent.is(AsdStereotypeName.COMPOUND_ATTRIBUTE));
    }
}