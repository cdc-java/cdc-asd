package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityIsMandatory;
import cdc.mf.model.MfProperty;

public class AttributeCardinalityIsMandatory extends AbstractCardinalityIsMandatory<MfProperty> {
    public static final String NAME = "A01";
    public static final String TITLE = "ATTRIBUTE_CARDINALITY_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE + S))
                                                        .appliesTo("All attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_CARDINALITY)
                                                        .remarks("Should we consider that a missing cardinality is equivalent to 1?",
                                                                 "In that case, " + AsdNames.SOURCE_UMLWRSG_2_0
                                                                         + " should be modified."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeCardinalityIsMandatory(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }
}