package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesAreMandatory;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfTag;
import cdc.util.strings.StringUtils;

public class AttributeTagWhenValidValueNotesAreMandatory extends AbstractNotesAreMandatory<MfTag> {
    public static final String NAME = "A12";
    public static final String TITLE = "ATTRIBUTE_TAG(VALID_VALUE)_NOTES_ARE_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE, AsdNames.T_VALID_VALUE + WS + N_TAG + S))
                                                        .appliesTo("validValue attribute tags that are non-empty")
                                                        .relatedTo(AsdRule.VALID_VALUE_DEFINITION),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public AttributeTagWhenValidValueNotesAreMandatory(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.VALID_VALUE
                && !StringUtils.isNullOrEmpty(object.getValue());
    }
}