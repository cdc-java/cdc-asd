package cdc.asd.checks.attributes;

import java.util.List;

import cdc.issues.checks.AbstractChecker;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfMemberOwner;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfProperty;

public class AttributesChecker<O extends MfMemberOwner> extends AbstractPartsChecker<O, MfProperty, AttributesChecker<O>> {
    @SafeVarargs
    public AttributesChecker(SnapshotManager manager,
                             Class<O> objectClass,
                             AbstractChecker<? super MfProperty>... partsCheckers) {
        super(manager,
              objectClass,
              MfProperty.class,
              partsCheckers);
    }

    @Override
    protected List<LocatedObject<? extends MfProperty>> getParts(O object) {
        final List<MfProperty> delegates = object.getProperties()
                                                 .stream()
                                                 .sorted(MfNameItem.NAME_COMPARATOR)
                                                 .toList();
        return LocatedObject.locate(delegates);
    }
}