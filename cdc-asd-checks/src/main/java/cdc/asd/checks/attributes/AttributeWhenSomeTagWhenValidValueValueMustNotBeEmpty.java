package cdc.asd.checks.attributes;

import java.util.List;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdTag;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.util.strings.StringUtils;

public class AttributeWhenSomeTagWhenValidValueValueMustNotBeEmpty extends MfAbstractRuleChecker<MfProperty> {
    public static final String NAME = "A29";
    public static final String TITLE = "ATTRIBUTE(SOME)_TAG(VALID_VALUE)_VALUE_MUST_NOT_BE_EMPTY";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("If an {%wrap} has only one {%wrap}, it cannot have an empty {%wrap}.",
                                                                N_ATTRIBUTE,
                                                                AsdNames.T_VALID_VALUE + WS + N_TAG,
                                                                N_VALUE)
                                                        .appliesTo("All validValue tag values of attributes that have 2 or more validValue tags.")
                                                        .relatedTo(AsdRule.NO_BLANK_VALID_VALUE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.15.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public AttributeWhenSomeTagWhenValidValueValueMustNotBeEmpty(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty object) {
        return getTheNameOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfProperty object,
                             Location location) {
        final List<MfTag> emptyValidValues =
                object.collect(MfTag.class,
                               t -> t.wrap(AsdTag.class).is(AsdTagName.VALID_VALUE))
                      .stream()
                      .filter(x -> StringUtils.isNullOrEmpty(x.getValue()))
                      .toList();

        if (!emptyValidValues.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            final int validValueSize = object.collect(MfTag.class,
                                                      t -> t.wrap(AsdTag.class).is(AsdTagName.VALID_VALUE))
                                             .size();

            description.header(getHeader(object))
                       .violation("has " + validValueSize + " validValue tags and " + emptyValidValues.size()
                               + " of them are empty.");
            description.elements(emptyValidValues);

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfProperty object) {
        return object.collect(MfTag.class, t -> t.wrap(AsdTag.class).is(AsdTagName.VALID_VALUE)).size() > 1;
    }
}