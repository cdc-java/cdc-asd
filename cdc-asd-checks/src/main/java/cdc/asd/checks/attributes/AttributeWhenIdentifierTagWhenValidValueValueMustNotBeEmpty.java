package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagValueMustMatchPattern;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;

public class AttributeWhenIdentifierTagWhenValidValueValueMustNotBeEmpty extends AbstractTagValueMustMatchPattern {
    public static final String NAME = "A19";
    public static final String TITLE = "ATTRIBUTE(IDENTIFIER)_TAG(VALID_VALUE)_VALUE_MUST_NOT_BE_EMPTY";
    public static final String REGEX = NO_SPACES_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("The {%wrap} {%wrap} of some {%wrap}"
                                      + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                AsdNames.T_VALID_VALUE + WS + N_TAG,
                                                                N_VALUE + S,
                                                                N_ATTRIBUTE + S)
                                                        .text("\nThey must neither contain spaces nor be empty.")
                                                        .appliesTo("All " + AsdNames.P_IDENTIFIER + " attributes")
                                                        .relatedTo(AsdRule.IDENTIFER_VALID_VALUE_OR_LIBRARY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public AttributeWhenIdentifierTagWhenValidValueValueMustNotBeEmpty(SnapshotManager manager) {
        super(manager,
              RULE,
              REGEX);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.VALID_VALUE
                && object.getParent() instanceof final MfProperty p
                && p.wrap(AsdProperty.class).getPrimitiveTypeName() == AsdPrimitiveTypeName.IDENTIFIER;
    }
}