package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesAreMandatory;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfMemberOwner;
import cdc.mf.model.MfProperty;

/**
 * Check that an attribute has notes.
 *
 * @author Damien Carbonne
 */
public class AttributeNotesAreMandatory extends AbstractNotesAreMandatory<MfProperty> {
    public static final String NAME = "A06";
    public static final String TITLE = "ATTRIBUTE_NOTES_ARE_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE + S))
                                                        .appliesTo("All attributes, except those of " + AsdNames.S_BUILTIN
                                                                + " classes and of BaseObject")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_DEFINITION)
                                                        .remarks("Scope is reduced."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeNotesAreMandatory(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    public boolean accepts(MfProperty object) {
        final MfMemberOwner parent = object.getParent();
        return !parent.wrap(AsdElement.class).isBuiltin()
                && !parent.wrap(AsdElement.class).isBaseObject();
    }
}