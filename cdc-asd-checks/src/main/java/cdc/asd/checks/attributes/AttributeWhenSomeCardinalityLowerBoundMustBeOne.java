package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityLowerBoundMustBeOne;
import cdc.mf.model.MfProperty;

public class AttributeWhenSomeCardinalityLowerBoundMustBeOne extends AbstractCardinalityLowerBoundMustBeOne<MfProperty> {
    public static final String NAME = "A20";
    public static final String TITLE = "ATTRIBUTE(SOME)_CARDINALITY_LOWER_BOUND_MUST_BE_ONE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ATTRIBUTE + S))
                                                        .appliesTo("All " + AsdNames.S_KEY + " attributes",
                                                                   "All " + AsdNames.S_COMPOSITE_KEY + " attributes",
                                                                   "All " + AsdNames.S_RELATIONSHIP_KEY + " attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_KEY_LOWER_CARDINALITY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeWhenSomeCardinalityLowerBoundMustBeOne(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    public boolean accepts(MfProperty object) {
        return AttributeUtils.expectsOneMin(object);
    }
}