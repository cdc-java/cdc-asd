package cdc.asd.checks.attributes;

import cdc.asd.model.AsdCompoundAttributeTypeName;
import cdc.asd.model.AsdConstants;
import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.mf.model.MfProperty;

public final class AttributeUtils {
    private AttributeUtils() {
    }

    /**
     * Returns {@code true} if an attribute must have a {@link AsdTagName#VALID_VALUE VALID_VALUE}
     * or {@link AsdTagName#VALID_VALUE_LIBRARY VALID_VALUE_LIBRARY} tag.
     * <ul>
     * <li>IdentifierType primitive
     * <li>ClassificationType primitive
     * <li>StateType primitive
     * <li>DatedClassification compound attribute
     * <li>TimeStampedClassification compound attribute
     * </ul>
     *
     * @param object The attribute to check.
     * @return {@code true} if {@code item} must have a {@link AsdTagName#VALID_VALUE VALID_VALUE}
     *         or {@link AsdTagName#VALID_VALUE_LIBRARY VALID_VALUE_LIBRARY} tag.
     */
    public static boolean expectsValidValueOrValidValueLibrary(MfProperty object) {
        // 11.2.5
        final AsdStereotypeName stereotypeName;
        if (object.hasValidType()) {
            stereotypeName = object.getType().wrap(AsdElement.class).getStereotypeName();
        } else {
            stereotypeName = null;
        }
        if (stereotypeName == AsdStereotypeName.PRIMITIVE) {
            final AsdPrimitiveTypeName primitive = object.wrap(AsdProperty.class).getPrimitiveTypeName();
            return primitive == AsdPrimitiveTypeName.IDENTIFIER
                    || primitive == AsdPrimitiveTypeName.CLASSIFICATION
                    || primitive == AsdPrimitiveTypeName.STATE;
        } else if (stereotypeName == AsdStereotypeName.COMPOUND_ATTRIBUTE) {
            final AsdCompoundAttributeTypeName compound = object.wrap(AsdProperty.class).getCompoundAttributeTypeName();
            return compound == AsdCompoundAttributeTypeName.DATED_CLASSIFICATION
                    || compound == AsdCompoundAttributeTypeName.TIME_STAMPED_CLASSIFICATION;
        } else {
            return false;
        }
    }

    /**
     * Returns {@code true} if a {@link AsdTagName#VALID_VALUE VALID_VALUE} tag value can be blank.
     * <ul>
     * <li>ClassificationType primitive
     * <li>StateType primitive TODO sure?
     * <li>DatedClassification compound attribute
     * <li>TimeStampedClassification compound attribute
     * </ul>
     *
     * @param object The attribute to check.
     * @return {@code true} if {@code item} accepts a blank {@link AsdTagName#VALID_VALUE}
     *         tag value.
     */
    public static boolean acceptsBlankValidValue(MfProperty object) {
        final AsdStereotypeName stereotypeName = object.getType().wrap(AsdElement.class).getStereotypeName();
        if (stereotypeName == AsdStereotypeName.PRIMITIVE) {
            final AsdPrimitiveTypeName primitive = object.wrap(AsdProperty.class).getPrimitiveTypeName();
            return primitive == AsdPrimitiveTypeName.CLASSIFICATION
                    || primitive == AsdPrimitiveTypeName.STATE;
        } else if (stereotypeName == AsdStereotypeName.COMPOUND_ATTRIBUTE) {
            // Special treatment of compound attributes
            final String name = object.getType().getName();
            return AsdConstants.DATED_CLASSIFICATION.equals(name)
                    || AsdConstants.TIME_STAMPED_CLASSIFICATION.equals(name);
        } else {
            return false;
        }
    }

    /**
     * Returns {@code true} if an attribute must have a {@link AsdTagName#UNIT UNIT} tag.
     * This is the case of attributes that have the following {@code <<primitive>>} type:
     * <ul>
     * <li>properties {@link AsdPrimitiveTypeName#isPropertyType()}
     * <li>numerical properties {@link AsdPrimitiveTypeName#isNumericalPropertyType()}
     * </ul>
     *
     * @param object The attribute to check.
     * @return {@code true} if {@code item} must have a {@link AsdTagName#UNIT UNIT} tag.
     */
    public static boolean expectsUnit(MfProperty object) {
        // 11.2.5
        final AsdPrimitiveTypeName primitive = object.wrap(AsdProperty.class).getPrimitiveTypeName();
        return primitive != null
                && (primitive.isNumericalPropertyType() || primitive.isPropertyType());
    }

    /**
     * Returns {@code true} if an attribute must have its cardinality lower bound equal to 1.
     * This is the case of attributes that have the following stereotypes:
     * <ul>
     * <li>{@link AsdStereotypeName#KEY KEY}
     * <li>{@link AsdStereotypeName#RELATIONSHIP_KEY RELATIONSHIP_KEY}
     * <li>{@link AsdStereotypeName#COMPOSITE_KEY COMPOSITE_KEY}
     * </ul>
     *
     * @param object The attribute to check.
     * @return {@code true} if {@code item} must have a min cardinality of 1.
     */
    public static boolean expectsOneMin(MfProperty object) {
        return object.hasStereotype(AsdStereotypeName.KEY,
                                    AsdStereotypeName.RELATIONSHIP_KEY,
                                    AsdStereotypeName.COMPOSITE_KEY);
    }

    /**
     * Returns {@code true} if an attribute must have its cardinality upper bound equal to infinity.
     * This is the case of attributes whose type is:
     * <ul>
     * <li>{@link AsdPrimitiveTypeName#DESCRIPTOR}
     * <li>{@link AsdPrimitiveTypeName#NAME}
     * </ul>
     *
     * @param object The attribute to check.
     * @return {@code true} if {@code item} must have a max cardinality of *.
     */
    public static boolean expectsUnboundedMax(MfProperty object) {
        final AsdPrimitiveTypeName primitive = object.wrap(AsdProperty.class).getPrimitiveTypeName();
        return primitive == AsdPrimitiveTypeName.DESCRIPTOR
                || primitive == AsdPrimitiveTypeName.NAME;
    }
}