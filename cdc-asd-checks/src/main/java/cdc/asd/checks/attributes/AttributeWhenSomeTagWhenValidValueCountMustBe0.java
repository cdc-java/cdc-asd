package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfProperty;

public class AttributeWhenSomeTagWhenValidValueCountMustBe0 extends AbstractTagNameCountMustMatch<MfProperty> {
    public static final String NAME = "A26";
    public static final String TITLE = "ATTRIBUTE(SOME)_TAG(VALID_VALUE)_COUNT_MUST_BE_0";
    private static final AsdTagName TAG_NAME = AsdTagName.VALID_VALUE;
    private static final int MINMAX = 0;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ATTRIBUTE + S, TAG_NAME, MINMAX))
                                                        .appliesTo("All attributes that are neither IdentifierType, ClassificationType, StateType, DatedClassification nor TimeStampedClassification")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.5")
                                                        .relatedTo(AsdRule.NO_VALID_VALUE,
                                                                   AsdRule.VALID_VALUE_LIBRARY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeWhenSomeTagWhenValidValueCountMustBe0(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              TAG_NAME.getLiteral(),
              MINMAX);
    }

    @Override
    public boolean accepts(MfProperty object) {
        return !AttributeUtils.expectsValidValueOrValidValueLibrary(object);
    }
}