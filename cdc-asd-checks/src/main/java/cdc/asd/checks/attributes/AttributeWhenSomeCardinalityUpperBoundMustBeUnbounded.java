package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityUpperBoundMustBeUnbounded;
import cdc.mf.model.MfProperty;

public class AttributeWhenSomeCardinalityUpperBoundMustBeUnbounded
        extends AbstractCardinalityUpperBoundMustBeUnbounded<MfProperty> {
    public static final String NAME = "A21";
    public static final String TITLE = "ATTRIBUTE(SOME)_CARDINALITY_UPPER_BOUND_MUST_BE_UNBOUNDED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ATTRIBUTE + S))
                                                        .appliesTo("All " + AsdNames.P_DESCRIPTOR + " attributes",
                                                                   "All " + AsdNames.P_NAME + " attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.1")
                                                        .relatedTo(AsdRule.ATTRIBUTE_UPPER_CARDINALITY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeWhenSomeCardinalityUpperBoundMustBeUnbounded(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    public boolean accepts(MfProperty object) {
        return AttributeUtils.expectsUnboundedMax(object);
    }
}