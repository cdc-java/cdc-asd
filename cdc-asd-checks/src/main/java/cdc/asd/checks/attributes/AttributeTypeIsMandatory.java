package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.properties.AbstractPropertyTypeIsMandatory;

public class AttributeTypeIsMandatory extends AbstractPropertyTypeIsMandatory {
    public static final String NAME = "A16";
    public static final String TITLE = "ATTRIBUTE_TYPE_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe())
                                                        .appliesTo("All attributes"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.9.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public AttributeTypeIsMandatory(SnapshotManager manager) {
        super(manager,
              RULE);
    }
}