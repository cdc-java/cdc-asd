package cdc.asd.checks.attributes;

import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagMustBeChosen;
import cdc.mf.model.MfProperty;

public class AttributeWhenSomeTagMustHaveValidValueOrValidValueLibrary extends AbstractTagMustBeChosen<MfProperty> {
    public static final String NAME = "A23";
    public static final String TITLE = "ATTRIBUTE(SOME)_TAG_MUST_HAVE_VALID_VALUE_OR_VALID_VALUE_LIBRARY";
    protected static final Set<String> CHOICE =
            Set.of(AsdTagName.VALID_VALUE.getLiteral(), AsdTagName.VALID_VALUE_LIBRARY.getLiteral());
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ATTRIBUTE + S, CHOICE))
                                                        .appliesTo("All " + AsdNames.P_IDENTIFIER + " attributes",
                                                                   "All " + AsdNames.P_CLASSIFICATION + " attributes",
                                                                   "All " + AsdNames.P_STATE + " attributes",
                                                                   "All " + AsdNames.CA_DATED_CLASSIFICATION + " attributes",
                                                                   "All " + AsdNames.CA_TIME_STAMPED_CLASSIFICATION
                                                                           + " attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.11",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.12")
                                                        .relatedTo(AsdRule.IDENTIFER_VALID_VALUE_OR_LIBRARY,
                                                                   AsdRule.CLASSIFICATION_VALID_VALUE_OR_LIBRARY,
                                                                   AsdRule.DATED_CLASSIFICATION_VALID_VALUE_OR_LIBRARY,
                                                                   AsdRule.TIMED_CLASSIF_VALID_VALUE_OR_LIBRARY,
                                                                   AsdRule.VALID_VALUE_AND_LIBRARY,
                                                                   AsdRule.STATE_VALID_VALUE_OR_LIBRARY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public AttributeWhenSomeTagMustHaveValidValueOrValidValueLibrary(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              CHOICE);
    }

    @Override
    public boolean accepts(MfProperty object) {
        return AttributeUtils.expectsValidValueOrValidValueLibrary(object);
    }
}