package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesMustMatchPattern;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfTag;
import cdc.util.strings.StringUtils;

public class AttributeTagWhenValidValueNotesMustFollowAuthoring extends AbstractNotesMustMatchPattern<MfTag> {
    public static final String NAME = "A13";
    public static final String TITLE = "ATTRIBUTE_TAG(VALID_VALUE)_NOTES_MUST_FOLLOW_AUTHORING";
    public static final String REGEX = "^[a-zA-Z0-9]+:[a-zA-Z0-9]+$";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All defined {%wrap} {%wrap} {%wrap}" + MUST_MATCH_THIS_PATTERN
                                      + REGEX,
                                                                N_ATTRIBUTE,
                                                                AsdNames.T_VALID_VALUE + WS + N_TAG,
                                                                N_NOTES)
                                                        .text("\nThe notes must contain the source and the term separated by a colon.")
                                                        .appliesTo("validValue attribute tags that are non-empty")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.11")
                                                        .relatedTo(AsdRule.VALID_VALUE_DEFINITION,
                                                                   AsdRule.VALID_VALUE_DEFINITION_NAME)
                                                        .remarks("RTD08/RTD09 are stricter than " + AsdNames.SOURCE_UMLWRSG_2_0,
                                                                 "Source can be different from SX001G.",
                                                                 "Term can be anything."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeTagWhenValidValueNotesMustFollowAuthoring(SnapshotManager manager) {
        super(manager,
              MfTag.class,
              RULE,
              REGEX);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.VALID_VALUE
                && !StringUtils.isNullOrEmpty(object.getValue());
    }
}