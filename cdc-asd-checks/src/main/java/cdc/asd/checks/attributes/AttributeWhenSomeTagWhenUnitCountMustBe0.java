package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfProperty;

public class AttributeWhenSomeTagWhenUnitCountMustBe0 extends AbstractTagNameCountMustMatch<MfProperty> {
    public static final String NAME = "A24";
    public static final String TITLE = "ATTRIBUTE(SOME)_TAG(UNIT)_COUNT_MUST_BE_0";
    private static final AsdTagName TAG_NAME = AsdTagName.UNIT;
    private static final int MINMAX = 0;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ATTRIBUTE + S, TAG_NAME, MINMAX))
                                                        .appliesTo("All attributes that are neither PropertyType, nor NumericalPropertyType or any of its descendants.")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.5",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.4")
                                                        .relatedTo(AsdRule.UNIT,
                                                                   AsdRule.NO_UNIT_ALLOWED),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeWhenSomeTagWhenUnitCountMustBe0(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfProperty object) {
        return !AttributeUtils.expectsUnit(object);
    }
}