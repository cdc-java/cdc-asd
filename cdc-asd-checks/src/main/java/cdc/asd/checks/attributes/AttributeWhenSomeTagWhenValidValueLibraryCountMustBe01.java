package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfProperty;

public class AttributeWhenSomeTagWhenValidValueLibraryCountMustBe01 extends AbstractTagNameCountMustMatch<MfProperty> {
    public static final String NAME = "A28";
    public static final String TITLE = "ATTRIBUTE(SOME)_TAG(VALID_VALUE_LIBRARY)_COUNT_MUST_BE_0_1";
    private static final AsdTagName TAG_NAME = AsdTagName.VALID_VALUE_LIBRARY;
    private static final int MIN = 0;
    private static final int MAX = 1;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ATTRIBUTE + S, TAG_NAME, MIN, MAX))
                                                        .appliesTo("All IdentifierType attributes",
                                                                   "All ClassificationType attributes",
                                                                   "All StateType attributes",
                                                                   "All DatedClassification attributes",
                                                                   "All TimeStampedClassification attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.5",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.12")
                                                        .relatedTo(AsdRule.VALID_VALUE_LIBRARY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public AttributeWhenSomeTagWhenValidValueLibraryCountMustBe01(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              TAG_NAME,
              MIN,
              MAX);
    }

    @Override
    public boolean accepts(MfProperty object) {
        return AttributeUtils.expectsValidValueOrValidValueLibrary(object);
    }
}