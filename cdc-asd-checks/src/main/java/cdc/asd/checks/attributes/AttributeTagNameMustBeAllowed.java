package cdc.asd.checks.attributes;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameMustBeAllowed;
import cdc.mf.model.MfTag;

public class AttributeTagNameMustBeAllowed extends AbstractTagNameMustBeAllowed {
    public static final String NAME = "A10";
    public static final String TITLE = "ATTRIBUTE_TAG_NAME_MUST_BE_ALLOWED";

    /** Tags that can be associated to an attribute. */
    private static final Set<AsdTagName> ALLOWED_TAGS =
            EnumSet.of(AsdTagName.XML_NAME,
                       AsdTagName.VALID_VALUE,
                       AsdTagName.VALID_VALUE_LIBRARY,
                       AsdTagName.UNIT,
                       AsdTagName.NOTE,
                       AsdTagName.EXAMPLE,
                       AsdTagName.SOURCE,
                       AsdTagName.REF);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_ATTRIBUTE))
                                                        .text(oneOf(AsdTagName::getLiteral, ALLOWED_TAGS))
                                                        .appliesTo("All recognized tag names of all attributes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.5",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.1 for xmlName",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.4 for unit",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.6 for ref",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.7 for source",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.8 for note",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.9 for example")
                                                        .relatedTo(AsdRule.ATTRIBUTE_TAGS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AttributeTagNameMustBeAllowed(SnapshotManager manager) {
        super(manager,
              RULE);
    }

    @Override
    protected boolean isRecognizedAndNotAllowed(String tagName) {
        final AsdTagName tn = AsdTagName.of(tagName);
        return tn != null && !ALLOWED_TAGS.contains(tn);
    }

    @Override
    public boolean accepts(MfTag object) {
        return object.getParent().wrap(AsdElement.class).isAttribute();
    }
}