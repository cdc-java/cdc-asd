package cdc.asd.checks.attributes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfProperty;

public class AttributeTagWhenXmlNameCountMustBe1 extends AbstractTagNameCountMustMatch<MfProperty> {
    public static final String NAME = "A15";
    public static final String TITLE = "ATTRIBUTE_TAG(XML_NAME)_COUNT_MUST_BE_1";
    private static final AsdTagName TAG_NAME = AsdTagName.XML_NAME;
    private static final int MINMAX = 1;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(ALL, N_ATTRIBUTE + S, TAG_NAME, MINMAX))
                                                        .appliesTo("All attributes")
                                                        .exceptions("uri")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.2.5")
                                                        .relatedTo(AsdRule.ATTRIBUTE_XML_NAME),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public AttributeTagWhenXmlNameCountMustBe1(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfProperty object) {
        return !"uri".equals(object.getName());
    }
}