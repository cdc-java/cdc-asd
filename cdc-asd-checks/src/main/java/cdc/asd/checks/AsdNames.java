package cdc.asd.checks;

import cdc.asd.model.AsdCompoundAttributeTypeName;
import cdc.asd.model.AsdModelUtils;
import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.issues.rules.RuleDescription;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfUtils;

public final class AsdNames {
    private AsdNames() {
    }

    public static final String THE = RuleDescription.THE;
    public static final String SOURCE_UMLWRSG_2_0 = AsdRuleDescription.SOURCE_UMLWRSG_2_0;
    public static final String SOURCE_XSDAR_2_0 = AsdRuleDescription.SOURCE_XSDAR_2_0;

    public static final String N_AUTHOR = "author";
    public static final String N_CONCEPT = "concept";
    public static final String N_ITEM = "item";
    public static final String N_NON = "non";
    public static final String N_REF_MODEL = "ref model";
    public static final String N_REVISION = "revision";
    public static final String N_UOF = "UoF";
    public static final String N_VERSION = "version";

    public static final String S_ATTRIBUTE_GROUP = AsdModelUtils.identify(AsdStereotypeName.ATTRIBUTE_GROUP);
    public static final String S_BUILTIN = AsdModelUtils.identify(AsdStereotypeName.BUILTIN);
    public static final String S_CHARACTERISTIC = AsdModelUtils.identify(AsdStereotypeName.CHARACTERISTIC);
    public static final String S_CLASS = AsdModelUtils.identify(AsdStereotypeName.CLASS);
    public static final String S_COMPOSITE_KEY = AsdModelUtils.identify(AsdStereotypeName.COMPOSITE_KEY);
    public static final String S_COMPOUND_ATTRIBUTE = AsdModelUtils.identify(AsdStereotypeName.COMPOUND_ATTRIBUTE);
    public static final String S_DOMAIN = AsdModelUtils.identify(AsdStereotypeName.DOMAIN);
    public static final String S_EXCHANGE = AsdModelUtils.identify(AsdStereotypeName.EXCHANGE);
    public static final String S_EXTEND = AsdModelUtils.identify(AsdStereotypeName.EXTEND);
    public static final String S_FUNCTIONAL_AREA = AsdModelUtils.identify(AsdStereotypeName.FUNCTIONAL_AREA);
    public static final String S_KEY = AsdModelUtils.identify(AsdStereotypeName.KEY);
    public static final String S_METACLASS = AsdModelUtils.identify(AsdStereotypeName.METACLASS);
    public static final String S_METADATA = AsdModelUtils.identify(AsdStereotypeName.METADATA);
    public static final String S_PRIMITIVE = AsdModelUtils.identify(AsdStereotypeName.PRIMITIVE);
    public static final String S_PROXY = AsdModelUtils.identify(AsdStereotypeName.PROXY);
    public static final String S_RELATIONSHIP = AsdModelUtils.identify(AsdStereotypeName.RELATIONSHIP);
    public static final String S_RELATIONSHIP_KEY = AsdModelUtils.identify(AsdStereotypeName.RELATIONSHIP_KEY);
    public static final String S_SELECT = AsdModelUtils.identify(AsdStereotypeName.SELECT);
    public static final String S_UML_PRIMITIVE = AsdModelUtils.identify(AsdStereotypeName.UML_PRIMITIVE);
    public static final String S_UOF = AsdModelUtils.identify(AsdStereotypeName.UOF);

    public static final String T_CHANGE_NOTE = AsdModelUtils.identify(AsdTagName.CHANGE_NOTE);
    public static final String T_EXAMPLE = AsdModelUtils.identify(AsdTagName.EXAMPLE);
    public static final String T_ICN = AsdModelUtils.identify(AsdTagName.ICN);
    public static final String T_LONG_DESCRIPTION = AsdModelUtils.identify(AsdTagName.LONG_DESCRIPTION);
    public static final String T_NOTE = AsdModelUtils.identify(AsdTagName.NOTE);
    public static final String T_NO_KEY = AsdModelUtils.identify(AsdTagName.NO_KEY);
    public static final String T_REF = AsdModelUtils.identify(AsdTagName.REF);
    public static final String T_REPLACES = AsdModelUtils.identify(AsdTagName.REPLACES);
    public static final String T_SOURCE = AsdModelUtils.identify(AsdTagName.SOURCE);
    public static final String T_UID_PATTERN = AsdModelUtils.identify(AsdTagName.UID_PATTERN);
    public static final String T_UNIT = AsdModelUtils.identify(AsdTagName.UNIT);
    public static final String T_VALID_VALUE = AsdModelUtils.identify(AsdTagName.VALID_VALUE);
    public static final String T_VALID_VALUE_LIBRARY = AsdModelUtils.identify(AsdTagName.VALID_VALUE_LIBRARY);
    public static final String T_XML_NAME = AsdModelUtils.identify(AsdTagName.XML_NAME);
    public static final String T_XML_REF_NAME = AsdModelUtils.identify(AsdTagName.XML_REF_NAME);
    public static final String T_XML_SCHEMA_NAME = AsdModelUtils.identify(AsdTagName.XML_SCHEMA_NAME);

    public static final String P_CLASSIFICATION = AsdModelUtils.identify(AsdPrimitiveTypeName.CLASSIFICATION);
    public static final String P_DATE = AsdModelUtils.identify(AsdPrimitiveTypeName.DATE);
    public static final String P_DATE_TIME = AsdModelUtils.identify(AsdPrimitiveTypeName.DATE_TIME);
    public static final String P_DESCRIPTOR = AsdModelUtils.identify(AsdPrimitiveTypeName.DESCRIPTOR);
    public static final String P_IDENTIFIER = AsdModelUtils.identify(AsdPrimitiveTypeName.IDENTIFIER);
    public static final String P_NAME = AsdModelUtils.identify(AsdPrimitiveTypeName.NAME);
    public static final String P_NUMERICAL_PROPERTY = AsdModelUtils.identify(AsdPrimitiveTypeName.NUMERICAL_PROPERTY);
    public static final String P_PROPERTY = AsdModelUtils.identify(AsdPrimitiveTypeName.PROPERTY);
    public static final String P_SINGLE_VALUE_PROPERTY = AsdModelUtils.identify(AsdPrimitiveTypeName.SINGLE_VALUE_PROPERTY);
    public static final String P_STATE = AsdModelUtils.identify(AsdPrimitiveTypeName.STATE);
    public static final String P_TEXT_PROPERTY = AsdModelUtils.identify(AsdPrimitiveTypeName.TEXT_PROPERTY);
    public static final String P_TIME = AsdModelUtils.identify(AsdPrimitiveTypeName.TIME);
    public static final String P_VALUE_RANGE_PROPERTY = AsdModelUtils.identify(AsdPrimitiveTypeName.VALUE_RANGE_PROPERTY);
    public static final String P_VALUE_WITH_TOLERANCE_PROPERTY =
            AsdModelUtils.identify(AsdPrimitiveTypeName.VALUE_WITH_TOLERANCE_PROPERTY);

    public static final String CA_AREA = AsdModelUtils.identify(AsdCompoundAttributeTypeName.AREA);
    public static final String CA_AUTHORIZED_LIFE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.AUTHORIZED_LIFE);
    public static final String CA_CIRCLE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.CIRCLE);
    public static final String CA_CUBOID = AsdModelUtils.identify(AsdCompoundAttributeTypeName.CUBOID);
    public static final String CA_CYLINDER = AsdModelUtils.identify(AsdCompoundAttributeTypeName.CYLINDER);
    public static final String CA_DATE_RANGE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.DATE_RANGE);
    public static final String CA_DATE_TIME_RANGE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.DATE_TIME_RANGE);
    public static final String CA_DATED_CLASSIFICATION = AsdModelUtils.identify(AsdCompoundAttributeTypeName.DATED_CLASSIFICATION);
    public static final String CA_DIMENSIONS = AsdModelUtils.identify(AsdCompoundAttributeTypeName.DIMENSIONS);
    public static final String CA_RECTANGLE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.RECTANGLE);
    public static final String CA_SERIAL_NUMBER_RANGE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.SERIAL_NUMBER_RANGE);
    public static final String CA_SPHERE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.SPHERE);
    public static final String CA_THREE_DIMENSIONAL = AsdModelUtils.identify(AsdCompoundAttributeTypeName.THREE_DIMENSIONAL);
    public static final String CA_TIME_RANGE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.TIME_RANGE);
    public static final String CA_TIME_STAMPED_CLASSIFICATION =
            AsdModelUtils.identify(AsdCompoundAttributeTypeName.TIME_STAMPED_CLASSIFICATION);
    public static final String CA_TRIANGLE = AsdModelUtils.identify(AsdCompoundAttributeTypeName.TRIANGLE);

    public static final String THE_AUTHOR_OF = RuleDescription.wrap(THE, true, N_AUTHOR) + " of ";
    public static final String THE_VERSION_OF = RuleDescription.wrap(THE, true, N_VERSION) + " of ";

    private static String getHeader(String prefix,
                                    MfElement item) {
        return prefix + MfUtils.identify(item);
    }

    public static String getTheAuthorOfHeader(MfElement item) {
        return getHeader(THE_AUTHOR_OF, item);
    }

    public static String getTheVersionOfHeader(MfElement item) {
        return getHeader(THE_VERSION_OF, item);
    }
}