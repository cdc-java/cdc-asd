package cdc.asd.checks;

import java.util.Optional;

import cdc.asd.model.AsdEaNaming;
import cdc.issues.checks.SnapshotManager;
import cdc.mf.model.MfModel;
import cdc.util.lang.Checks;

/**
 * Specialization of {@link SnapshotManager}.
 *
 * @author Damien Carbonne
 */
public final class AsdSnapshotManager extends SnapshotManager {
    private final Optional<MfModel> refModel;
    private final AsdEaNaming naming;

    protected AsdSnapshotManager(Builder builder) {
        super(builder);
        this.refModel = Optional.ofNullable(builder.refModel);
        this.naming = Checks.isNotNull(builder.naming, "naming");
    }

    /**
     * @return The reference model.
     */
    public Optional<MfModel> getRefModel() {
        return refModel;
    }

    public AsdEaNaming getNaming() {
        return naming;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends SnapshotManager.Builder<Builder> {
        private MfModel refModel = null;
        private AsdEaNaming naming;

        protected Builder() {
        }

        public Builder refModel(MfModel refModel) {
            this.refModel = refModel;
            return this;
        }

        public Builder naming(AsdEaNaming naming) {
            this.naming = naming;
            return this;
        }

        @Override
        public AsdSnapshotManager build() {
            return new AsdSnapshotManager(this);
        }
    }
}