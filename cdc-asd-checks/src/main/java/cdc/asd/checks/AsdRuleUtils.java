package cdc.asd.checks;

import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

import cdc.issues.IssueSeverity;
import cdc.issues.rules.Rule;

public final class AsdRuleUtils {
    private AsdRuleUtils() {
    }

    public static Set<AsdRule> getRelatedRules(Rule rule) {
        final AsdRuleDescription description = AsdRuleDescription.builder()
                                                                 .text(rule.getDescription())
                                                                 .build();
        return description.getRelatedRules();
    }

    public static String getHeader(Rule rule) {
        final AsdRuleDescription description = AsdRuleDescription.builder()
                                                                 .text(rule.getDescription())
                                                                 .build();
        return description.getHeader();
    }

    public static List<String> getSectionItems(Rule rule,
                                               String section) {
        final AsdRuleDescription description = AsdRuleDescription.builder()
                                                                 .text(rule.getDescription())
                                                                 .build();
        return description.getSectionItems(section);
    }

    public static Rule.Builder rule(String name,
                                    String title,
                                    Consumer<AsdRuleDescription.Builder> descriptionBuilder,
                                    IssueSeverity severity) {
        final AsdRuleDescription.Builder description = AsdRuleDescription.builder();
        descriptionBuilder.accept(description);
        return Rule.builder()
                   .domain(AsdProfile.DOMAIN)
                   .name(name)
                   .title(title)
                   .description(description.build().toString())
                   .severity(severity);
    }
}