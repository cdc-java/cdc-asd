package cdc.asd.checks.connectors;

import java.util.List;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityMustBeAmong;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfCardinality;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfTipRole;

public class AssociationToRelationshipTargetCardinalityMustBeZeroToManyOrOneToMany
        extends AbstractCardinalityMustBeAmong<MfTip> {
    public static final String NAME = "X13";
    public static final String TITLE = "ASSOCIATION(TO_RELATIONSHIP)_TARGET_CARDINALITY_MUST_BE_ZERO_TO_MANY_OR_ONE_TO_MANY";
    public static final List<MfCardinality> CARDINALITIES =
            List.of(MfCardinality.ZERO_UNBOUNDED,
                    MfCardinality.ONE_UNBOUNDED);
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(THE,
                                                                       N_TARGET + WS + N_TIP,
                                                                       SOME,
                                                                       N_ASSOCIATION + S,
                                                                       CARDINALITIES))
                                                        .appliesTo("The target tip of all associations targeting a <<relationship>> class")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.2.2.1"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AssociationToRelationshipTargetCardinalityMustBeZeroToManyOrOneToMany(SnapshotManager manager) {
        super(manager,
              MfTip.class,
              RULE,
              CARDINALITIES);
    }

    @Override
    public boolean accepts(MfTip object) {
        return object.getParent() instanceof MfAssociation
                && object.getSide() == MfTipRole.TARGET.getSide()
                && object.getParent().getTargetTip().getType().wrap(AsdElement.class)
                         .getStereotypeName() == AsdStereotypeName.RELATIONSHIP;
    }
}