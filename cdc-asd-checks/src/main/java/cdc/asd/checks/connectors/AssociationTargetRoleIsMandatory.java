package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTipRole;

public class AssociationTargetRoleIsMandatory extends AbstractConnectorTipRoleIsMandatory {
    public static final String NAME = "X10";
    public static final String TITLE = "ASSOCIATION_TARGET_ROLE_IS_MANDATORY";
    public static final MfTipRole ROLE = MfTipRole.TARGET;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AN, N_ASSOCIATION, ROLE))
                                                        .appliesTo("The target tip of all associations")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.2.2.1",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.4.1.1")
                                                        .relatedTo(AsdRule.ASSOCIATION_HAS_TARGET),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    protected AssociationTargetRoleIsMandatory(SnapshotManager manager) {
        super(manager,
              RULE,
              ROLE);
    }

    @Override
    public boolean accepts(MfConnector object) {
        return object instanceof MfAssociation;
    }
}