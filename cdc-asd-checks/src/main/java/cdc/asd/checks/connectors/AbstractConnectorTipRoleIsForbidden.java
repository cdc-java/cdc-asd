package cdc.asd.checks.connectors;

import cdc.asd.model.AsdModelUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfTipRole;
import cdc.util.strings.StringUtils;

public abstract class AbstractConnectorTipRoleIsForbidden extends MfAbstractRuleChecker<MfConnector> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    private final MfTipRole role;

    protected static String describe(String article,
                                     String item,
                                     MfTipRole role) {
        return RuleDescription.wrap(THE, true, AsdModelUtils.identify(role) + WS + N_TIP)
                + " of " + RuleDescription.wrap(article, false, item)
                + " must not have " + RuleDescription.wrap(ANY, false, N_ROLE) + ".";
    }

    protected AbstractConnectorTipRoleIsForbidden(SnapshotManager context,
                                                  Rule rule,
                                                  MfTipRole role) {
        super(context,
              MfConnector.class,
              rule);
        this.role = role;
    }

    @Override
    protected String getHeader(MfConnector object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfConnector object,
                             Location location) {
        final MfTip tip = object.getTip(role);
        if (!StringUtils.isNullOrEmpty(tip.getRole())) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has a role, it should not");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}