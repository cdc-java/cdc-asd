package cdc.asd.checks.connectors;

import java.util.List;

import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfConnectorOwner;
import cdc.mf.model.MfElement;

public class ConnectorsChecker extends AbstractPartsChecker<MfConnectorOwner, MfConnector, ConnectorsChecker> {
    public ConnectorsChecker(SnapshotManager manager) {
        super(manager,
              MfConnectorOwner.class,
              MfConnector.class,
              new ConnectorChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends MfConnector>> getParts(MfConnectorOwner object) {
        final List<MfConnector> delegates = object.getConnectors()
                                                  .stream()
                                                  .sorted(MfElement.ID_COMPARATOR)
                                                  .toList();
        return LocatedObject.locate(delegates);
    }
}