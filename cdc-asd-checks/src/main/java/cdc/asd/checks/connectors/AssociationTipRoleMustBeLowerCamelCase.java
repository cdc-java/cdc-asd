package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.text.AbstractTextMustMatchPattern;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfTip;

public class AssociationTipRoleMustBeLowerCamelCase extends AbstractTextMustMatchPattern<MfTip> {
    public static final String NAME = "X11";
    public static final String TITLE = "ASSOCIATION_TIP_ROLE_MUST_BE_LOWER_CAMEL_CASE";
    public static final String REGEX = LOWER_CAMEL_CASE_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All defined {%wrap} {%wrap} {%wrap}" + MUST_MATCH_THIS_PATTERN
                                      + REGEX,
                                                                N_ASSOCIATION,
                                                                N_TIP,
                                                                N_ROLE + S)
                                                        .text("\nThey must be lowerCamelCase.")
                                                        .appliesTo("All defined association tip roles")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.13.1"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AssociationTipRoleMustBeLowerCamelCase(SnapshotManager manager) {
        super(manager,
              MfTip.class,
              RULE,
              REGEX);
    }

    @Override
    protected String getText(MfTip object) {
        return object.getRole();
    }

    @Override
    protected final String getHeader(MfTip object) {
        return getTheRoleOfHeader(object);
    }

    @Override
    public boolean accepts(MfTip object) {
        return object.getParent() instanceof MfAssociation;
    }
}