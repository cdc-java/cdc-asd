package cdc.asd.checks.connectors;

import java.util.List;

import cdc.asd.checks.cardinalities.CardinalityMustBeValid;
import cdc.asd.checks.cardinalities.CardinalityMustBeWellFormed;
import cdc.asd.checks.notes.NotesChecker;
import cdc.asd.checks.notes.NotesMustBeUnicode;
import cdc.asd.checks.notes.NotesMustNotContainHtml;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTip;

public class ConnectorTipsChecker extends AbstractPartsChecker<MfConnector, MfTip, ConnectorTipsChecker> {
    public ConnectorTipsChecker(SnapshotManager manager) {
        super(manager,
              MfConnector.class,
              MfTip.class,
              new AggregationPartCardinalityIsMandatory(manager),
              new AggregationWholeCardinalityMustBeOne(manager),
              new AssociationToRelationshipSourceCardinalityMustBeOne(manager),
              new AssociationToRelationshipTargetCardinalityMustBeZeroToManyOrOneToMany(manager),
              new AssociationTipRoleMustBeLowerCamelCase(manager),
              new CompositionPartCardinalityIsMandatory(manager),
              new CompositionWholeCardinalityMustBeOne(manager),
              new CardinalityMustBeValid(manager),
              new CardinalityMustBeWellFormed(manager),
              new NotesChecker(manager,
                               new NotesMustBeUnicode(manager),
                               new NotesMustNotContainHtml(manager)));
    }

    @Override
    protected List<LocatedObject<? extends MfTip>> getParts(MfConnector object) {
        return LocatedObject.locate(object.getTips());
    }
}