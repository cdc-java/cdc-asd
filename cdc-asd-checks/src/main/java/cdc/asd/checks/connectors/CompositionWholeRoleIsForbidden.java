package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTipRole;

public class CompositionWholeRoleIsForbidden extends AbstractConnectorTipRoleIsForbidden {
    public static final String NAME = "X19";
    public static final String TITLE = "COMPOSITION_WHOLE_ROLE_IS_FORBIDDEN";
    public static final MfTipRole ROLE = MfTipRole.WHOLE;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(A, N_COMPOSITION, ROLE))
                                                        .appliesTo("The whole tip of all compositions")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.4.2.1")
                                                        .relatedTo(AsdRule.COMPOSITION_TARGET),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    protected CompositionWholeRoleIsForbidden(SnapshotManager manager) {
        super(manager,
              RULE,
              ROLE);
    }

    @Override
    public boolean accepts(MfConnector object) {
        return object instanceof MfComposition;
    }
}