package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfInterface;

/**
 * Check that an association has 0..1 {@link AsdTagName#NO_KEY NO_KEY} tag.
 *
 * @author Damien Carbonne
 */
public class AssociationFromExtendTagWhenNoKeyCountMustBe1 extends AbstractTagNameCountMustMatch<MfConnector> {
    public static final String NAME = "X07";
    public static final String TITLE = "ASSOCIATION(FROM_EXTEND)_TAG(NO_KEY)_COUNT_MUST_BE_1";
    private static final AsdTagName TAG_NAME = AsdTagName.NO_KEY;
    private static final int MIN = 1;
    private static final int MAX = 1;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_ASSOCIATION + S, TAG_NAME, MIN, MAX))
                                                        .appliesTo("All directed associations whose source is an <<extend>> interface")
                                                        .remarks("[#32] Since 2024-02-17. DMEWG 35.01: All directed associations of <<extend>> interface MUST be tagged noKey.",
                                                                 "An association is directed when its target is navigable and its source is not navigable."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.11.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    protected AssociationFromExtendTagWhenNoKeyCountMustBe1(SnapshotManager manager) {
        super(manager,
              MfConnector.class,
              RULE,
              TAG_NAME,
              MIN,
              MAX);
    }

    @Override
    public boolean accepts(MfConnector object) {
        return object instanceof final MfAssociation ass
                && ass.getSourceTip().getType() instanceof final MfInterface xfc
                && xfc.wrap(AsdElement.class).is(AsdStereotypeName.EXTEND)
                && ass.getTargetTip().isNavigable()
                && !ass.getSourceTip().isNavigable();
    }
}