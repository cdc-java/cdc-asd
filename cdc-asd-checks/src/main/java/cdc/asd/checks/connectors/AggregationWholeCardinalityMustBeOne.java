package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityMustBeOne;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfTipRole;

public class AggregationWholeCardinalityMustBeOne extends AbstractCardinalityMustBeOne<MfTip> {
    public static final String NAME = "X05";
    public static final String TITLE = "AGGREGATION_WHOLE_CARDINALITY_MUST_BE_ONE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(THE, N_WHOLE + WS + N_TIP, AN, N_AGGREGATION))
                                                        .appliesTo("The cardinality of the whole tip of all aggregations")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.4.2.1")
                                                        .relatedTo(AsdRule.AGGREGATION_TARGET_CARDINALITY)
                                                        .remarks("This rule contradicts 11.4.2.1 that say that this can be any cardinality for aggregations."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.CONTRADICTION,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public AggregationWholeCardinalityMustBeOne(SnapshotManager manager) {
        super(manager,
              MfTip.class,
              RULE);
    }

    @Override
    public boolean accepts(MfTip object) {
        return object.getParent() instanceof MfAggregation
                && object.getSide() == MfTipRole.WHOLE.getSide();
    }
}