package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTipRole;

public class AggregationWholeRoleIsForbidden extends AbstractConnectorTipRoleIsForbidden {
    public static final String NAME = "X06";
    public static final String TITLE = "AGGREGATION_WHOLE_ROLE_IS_FORBIDDEN";
    public static final MfTipRole ROLE = MfTipRole.WHOLE;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AN, N_AGGREGATION, ROLE))
                                                        .appliesTo("The whole tip of all aggregations")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.4.2.1")
                                                        .relatedTo(AsdRule.AGGREGATION_TARGET),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    protected AggregationWholeRoleIsForbidden(SnapshotManager manager) {
        super(manager,
              RULE,
              ROLE);
    }

    @Override
    public boolean accepts(MfConnector object) {
        return object instanceof MfAggregation;
    }
}