package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityMustBeOne;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfTipRole;

public class AssociationToRelationshipSourceCardinalityMustBeOne extends AbstractCardinalityMustBeOne<MfTip> {
    public static final String NAME = "X12";
    public static final String TITLE = "ASSOCIATION(TO_RELATIONSHIP)_SOURCE_CARDINALITY_MUST_BE_ONE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(THE, N_SOURCE + WS + N_TIP, SOME, N_ASSOCIATION + S))
                                                        .appliesTo("The source tip of all associations targeting a <<relationship>> class")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.2.2.1"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AssociationToRelationshipSourceCardinalityMustBeOne(SnapshotManager manager) {
        super(manager,
              MfTip.class,
              RULE);
    }

    @Override
    public boolean accepts(MfTip object) {
        return object.getParent() instanceof MfAssociation
                && object.getSide() == MfTipRole.SOURCE.getSide()
                && object.getParent().getTargetTip().getType().wrap(AsdElement.class)
                         .getStereotypeName() == AsdStereotypeName.RELATIONSHIP;
    }
}