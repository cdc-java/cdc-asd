package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTipRole;
import cdc.mf.model.MfType;

public class AggregationPartTypeWhenSomeIsForbidden extends AbstractConnectorTipTypeIsForbidden {
    public static final String NAME = "X02";
    public static final String TITLE = "AGGREGATION_PART_TYPE(SOME)_IS_FORBIDDEN";
    public static final MfTipRole ROLE = MfTipRole.PART;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AN,
                                                                       N_AGGREGATION,
                                                                       ROLE,
                                                                       "a " + AsdNames.S_RELATIONSHIP + " or "
                                                                               + AsdNames.S_COMPOUND_ATTRIBUTE))
                                                        .appliesTo("The part tip of all aggregations")
                                                        .relatedTo(AsdRule.RELATIONSHIP_AGGREGATION,
                                                                   AsdRule.COMPOUND_ATTRIBUTE_AGGREGATIONS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public AggregationPartTypeWhenSomeIsForbidden(SnapshotManager manager) {
        super(manager,
              RULE,
              ROLE);
    }

    @Override
    protected boolean isAllowed(MfType type) {
        final AsdStereotypeName stereotypeName = type.wrap(AsdElement.class).getStereotypeName();
        return stereotypeName != AsdStereotypeName.RELATIONSHIP
                && stereotypeName != AsdStereotypeName.COMPOUND_ATTRIBUTE;
    }

    @Override
    public boolean accepts(MfConnector object) {
        return object instanceof MfAggregation;
    }
}