package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.misc.AbstractElementMustBeGraphicallyRepresented;
import cdc.mf.model.MfConnector;

public class ConnectorMustBeGraphicallyRepresented extends AbstractElementMustBeGraphicallyRepresented<MfConnector> {
    public static final String NAME = "X20";
    public static final String TITLE = "CONNECTOR_MUST_BE_GRAPHICALLY_REPRESENTED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_CONNECTOR + S))
                                                        .appliesTo("All connectors")
                                                        .relatedTo(AsdRule.CLASS_USED),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.23.0")
                        .build();

    public ConnectorMustBeGraphicallyRepresented(SnapshotManager context) {
        super(context,
              MfConnector.class,
              RULE);
    }
}