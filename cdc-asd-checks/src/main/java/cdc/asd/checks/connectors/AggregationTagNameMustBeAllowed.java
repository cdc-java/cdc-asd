package cdc.asd.checks.connectors;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameMustBeAllowed;
import cdc.mf.model.MfTag;

public class AggregationTagNameMustBeAllowed extends AbstractTagNameMustBeAllowed {
    public static final String NAME = "X03";
    public static final String TITLE = "AGGREGATION_TAG_NAME_MUST_BE_ALLOWED";

    /** Tags that can be associated to an association. */
    private static final Set<AsdTagName> ALLOWED_TAGS =
            EnumSet.of(AsdTagName.XML_NAME,
                       AsdTagName.NOTE,
                       AsdTagName.EXAMPLE,
                       AsdTagName.REF);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_AGGREGATION))
                                                        .text(oneOf(AsdTagName::getLiteral, ALLOWED_TAGS))
                                                        .appliesTo("All recognized tag names of all aggregations")
                                                        .relatedTo(AsdRule.AGGREGATION_TAGS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public AggregationTagNameMustBeAllowed(SnapshotManager manager) {
        super(manager,
              RULE);
    }

    @Override
    protected boolean isRecognizedAndNotAllowed(String tagName) {
        final AsdTagName tn = AsdTagName.of(tagName);
        return tn != null && !ALLOWED_TAGS.contains(tn);
    }

    @Override
    public boolean accepts(MfTag object) {
        return object.getParent().wrap(AsdElement.class).isAggregation();
    }
}