package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityMustBeOne;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfTipRole;

public class CompositionWholeCardinalityMustBeOne extends AbstractCardinalityMustBeOne<MfTip> {
    public static final String NAME = "X18";
    public static final String TITLE = "COMPOSITION_WHOLE_CARDINALITY_MUST_BE_ONE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(THE, N_WHOLE + WS + N_TIP, A, N_COMPOSITION))
                                                        .appliesTo("The cardinality of the whole tip of all compositions")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.4.2.1")
                                                        .relatedTo(AsdRule.COMPOSITION_TARGET_CARDINALITY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public CompositionWholeCardinalityMustBeOne(SnapshotManager context) {
        super(context,
              MfTip.class,
              RULE);
    }

    @Override
    public boolean accepts(MfTip object) {
        return object.getParent() instanceof MfComposition
                && object.getSide() == MfTipRole.WHOLE.getSide();
    }
}