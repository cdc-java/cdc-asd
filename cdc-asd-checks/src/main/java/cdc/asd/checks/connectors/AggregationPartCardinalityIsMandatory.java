package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityIsMandatory;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfTipRole;

public class AggregationPartCardinalityIsMandatory extends AbstractCardinalityIsMandatory<MfTip> {
    public static final String NAME = "X01";
    public static final String TITLE = "AGGREGATION_PART_CARDINALITY_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_AGGREGATION, N_PART + WS + N_TIP + S))
                                                        .appliesTo("The part tip of all aggregations")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.4.2.1")
                                                        .relatedTo(AsdRule.AGGREGATION_SOURCE_CARDINALITY)
                                                        .remarks("Should we consider that a missing cardinality is equivalent to 1?",
                                                                 "In that case, " + AsdNames.SOURCE_UMLWRSG_2_0
                                                                         + " should be modified."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public AggregationPartCardinalityIsMandatory(SnapshotManager manager) {
        super(manager,
              MfTip.class,
              RULE);
    }

    @Override
    public boolean accepts(MfTip object) {
        return object.getParent() instanceof MfAggregation
                && object.getSide() == MfTipRole.PART.getSide();
    }
}