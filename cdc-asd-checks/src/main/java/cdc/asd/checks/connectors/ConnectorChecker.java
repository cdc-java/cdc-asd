package cdc.asd.checks.connectors;

import cdc.asd.checks.notes.NotesChecker;
import cdc.asd.checks.notes.NotesMustBeUnicode;
import cdc.asd.checks.notes.NotesMustNotContainHtml;
import cdc.asd.checks.tags.TagNameIsMandatory;
import cdc.asd.checks.tags.TagNameMustBeRecognized;
import cdc.asd.checks.tags.TagValueMustBeUnicode;
import cdc.asd.checks.tags.TagWhenNoteValueMustBeUnique;
import cdc.asd.checks.tags.TagsChecker;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.mf.model.MfConnector;

public class ConnectorChecker extends CompositeChecker<MfConnector> {
    public ConnectorChecker(SnapshotManager manager) {
        super(manager,
              MfConnector.class,
              new ConnectorTipsChecker(manager),
              new AggregationPartTypeWhenSomeIsForbidden(manager),
              new AggregationTagWhenXmlNameCountMustBe01(manager),
              new AggregationWholeRoleIsForbidden(manager),
              // new ConnectorWhenAssociationMustBeForward(manager),
              new AssociationTargetRoleIsMandatory(manager),
              new AssociationFromExtendTagWhenNoKeyCountMustBe1(manager),
              new AssociationTagWhenXmlNameCountMustBe01(manager),
              new CompositionPartTypeIsForbidden(manager),
              new CompositionTagWhenXmlNameCountMustBe01(manager),
              new CompositionWholeRoleIsForbidden(manager),
              new ConnectorMustBeGraphicallyRepresented(manager),
              new NotesChecker(manager,
                               new NotesMustBeUnicode(manager),
                               new NotesMustNotContainHtml(manager)),
              new TagWhenNoteValueMustBeUnique(manager),
              new TagsChecker(manager,
                              new AggregationTagNameMustBeAllowed(manager),
                              new AssociationTagNameMustBeAllowed(manager),
                              new CompositionTagNameMustBeAllowed(manager),
                              new TagNameIsMandatory(manager),
                              new TagNameMustBeRecognized(manager),
                              new TagValueMustBeUnicode(manager),
                              new NotesChecker(manager,
                                               new NotesMustBeUnicode(manager),
                                               new NotesMustNotContainHtml(manager))));
    }
}