package cdc.asd.checks.connectors;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfConnector;

/**
 * Check that an association has 0..1 {@link AsdTagName#XML_NAME XML_NAME} tag.
 *
 * @author Damien Carbonne
 */
public class AssociationTagWhenXmlNameCountMustBe01 extends AbstractTagNameCountMustMatch<MfConnector> {
    public static final String NAME = "X09";
    public static final String TITLE = "ASSOCIATION_TAG(XML_NAME)_COUNT_MUST_BE_0_1";
    private static final AsdTagName TAG_NAME = AsdTagName.XML_NAME;
    private static final int MIN = 0;
    private static final int MAX = 1;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AN, N_ASSOCIATION, TAG_NAME, MIN, MAX))
                                                        .appliesTo("All associations")
                                                        .relatedTo(AsdRule.ASSOCIATION_XML_NAME_UNIQUE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected AssociationTagWhenXmlNameCountMustBe01(SnapshotManager manager) {
        super(manager,
              MfConnector.class,
              RULE,
              TAG_NAME,
              MIN,
              MAX);
    }

    @Override
    public boolean accepts(MfConnector object) {
        return object instanceof MfAssociation;
    }
}