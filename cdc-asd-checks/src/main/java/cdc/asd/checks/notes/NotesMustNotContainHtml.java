package cdc.asd.checks.notes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.documentations.AbstractDocumentationTextMustNotContainHtml;
import cdc.mf.model.MfDocumentation;
import cdc.mf.model.MfPackage;

public class NotesMustNotContainHtml extends AbstractDocumentationTextMustNotContainHtml {
    public static final String NAME = "G06";
    public static final String TITLE = "NOTES(SOME)_MUST_NOT_CONTAIN_HTML";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap}" + MUST_NOT_CONTAIN_HTML,
                                                                N_NOTES)
                                                        .appliesTo("All notes that are used in schema or doc generation",
                                                                   "All attribute notes",
                                                                   "All class notes",
                                                                   "All connector notes",
                                                                   "All connector tip notes",
                                                                   "All interface notes",
                                                                   "All tag notes")
                                                        .remarks("Entities are detected.",
                                                                 "All tags are detected."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public NotesMustNotContainHtml(SnapshotManager manager) {
        super(manager,
              RULE);
    }

    @Override
    public boolean accepts(MfDocumentation object) {
        return !(object.getParent() instanceof MfPackage);
    }
}