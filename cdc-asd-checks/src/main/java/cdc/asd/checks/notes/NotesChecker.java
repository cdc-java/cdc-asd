package cdc.asd.checks.notes;

import java.util.List;

import cdc.issues.checks.AbstractChecker;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfDocumentation;
import cdc.mf.model.MfElement;

public class NotesChecker extends AbstractPartsChecker<MfElement, MfDocumentation, NotesChecker> {
    @SafeVarargs
    public NotesChecker(SnapshotManager manager,
                        AbstractChecker<? super MfDocumentation>... partsCheckers) {
        super(manager,
              MfElement.class,
              MfDocumentation.class,
              partsCheckers);
    }

    @Override
    protected List<LocatedObject<? extends MfDocumentation>> getParts(MfElement object) {
        final List<MfDocumentation> delegates = object.getDocumentations()
                                                      .stream()
                                                      .toList();
        return LocatedObject.locate(delegates);
    }
}