package cdc.asd.checks.notes;

import java.util.List;

import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfNameItem;
import cdc.util.strings.StringUtils;

public abstract class AbstractNotesMustStartWithName<O extends MfElement> extends MfAbstractRuleChecker<O> {
    private static final int MAX_LENGTH = 80;
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;

    protected static String describe(String item) {
        return RuleDescription.format("All defined {%wrap} {%wrap} must start with the {%wrap} {%wrap}.",
                                      item,
                                      N_NOTES,
                                      item,
                                      N_NAME);
    }

    protected AbstractNotesMustStartWithName(SnapshotManager manager,
                                             Class<O> objectClass,
                                             Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O object) {
        return getTheNotesOfHeader(object);
    }

    protected abstract List<String> cleanName(String name);

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final String notes = object.wrap(AsdElement.class).getNotes();
        if (!StringUtils.isNullOrEmpty(notes)) {
            boolean found = false;
            final List<String> names = cleanName(((MfNameItem) object).getName());
            for (final String name : names) {
                if (notes.startsWith(name)) {
                    found = true;
                    break;
                }
            }

            if (!found) {
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .value(StringUtils.extract(notes, MAX_LENGTH))
                           .violation("do not follow authoring rules");
                int index = 0;
                for (final String name : names) {
                    index++;
                    description.justification(index, "do not start with: '" + name + "'");
                }

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }
}