package cdc.asd.checks.notes;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdModel;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;
import cdc.util.strings.StringUtils;

/**
 * The checker is now useless.
 * References section is now automatically generated in glossary.
 */
public class NotesConceptsMustBeDefinedAsRefTags extends MfAbstractRuleChecker<MfTagOwner> {
    public static final String NAME = "G04";
    public static final String TITLE = "NOTES_CONCEPTS_MUST_BE_DEFINED_AS_REF_TAGS";
    public static final IssueSeverity SEVERITY = IssueSeverity.MINOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("The {%wrap} used in {%wrap} must be defined as {%wrap}.",
                                                                AsdNames.N_CONCEPT + S,
                                                                N_NOTES,
                                                                AsdNames.T_REF + WS + N_TAG + S)
                                                        .appliesTo("All classes",
                                                                   "All attributes",
                                                                   "All interfaces",
                                                                   "All packages")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.6"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    private static final Pattern BOUNDARIES = Pattern.compile("\\b+");

    public NotesConceptsMustBeDefinedAsRefTags(SnapshotManager manager) {
        super(manager,
              MfTagOwner.class,
              RULE);
    }

    @Override
    protected String getHeader(MfTagOwner object) {
        return getTheNotesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfTagOwner object,
                             Location location) {
        final String notes = object.wrap(AsdElement.class).getNotes();
        final Set<String> usedConcepts = getConcepts(notes, object.getModel());
        final List<MfTag> refTags = object.getTags(AsdTagName.REF);
        final Set<String> refValues = new HashSet<>();
        for (final MfTag tag : refTags) {
            if (tag.getValue() != null) {
                final String[] values = BOUNDARIES.split(tag.getValue());
                Collections.addAll(refValues, values);
            }
        }
        final Set<String> missingConcepts = new HashSet<>(usedConcepts);
        missingConcepts.removeAll(refValues);
        missingConcepts.remove(((MfNameItem) object).getName());

        if (!missingConcepts.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(notes)
                       .violation("might have concepts that are not declared as ref tags:")
                       .uItems(missingConcepts)
                       .elements(refTags);

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfTagOwner object) {
        return object instanceof MfNameItem;
    }

    private static Set<String> getConcepts(String text,
                                           MfModel model) {
        final Set<String> set = new HashSet<>();
        if (!StringUtils.isNullOrEmpty(text)) {
            final String[] words = BOUNDARIES.split(text);
            for (final String word : words) {
                if (model.wrap(AsdModel.class).getSpecificTypeNames().contains(word)) {
                    set.add(word);
                }
            }
        }
        return set;
    }
}