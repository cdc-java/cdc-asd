package cdc.asd.checks.notes;

import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfElement;
import cdc.util.strings.StringUtils;

public abstract class AbstractNotesAreMandatory<O extends MfElement> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MINOR;

    protected static String describe(String item) {
        return RuleDescription.format("All {%wrap} must have {%wrap}.",
                                      item,
                                      N_NOTES);
    }

    protected static String describe(String item1,
                                     String item2) {
        return RuleDescription.format("All {%wrap} {%wrap} must have {%wrap}.",
                                      item1,
                                      item2,
                                      N_NOTES);
    }

    protected AbstractNotesAreMandatory(SnapshotManager manager,
                                        Class<O> objectClass,
                                        Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final String notes = object.wrap(AsdElement.class).getNotes();
        if (StringUtils.isNullOrEmpty(notes)) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has no notes");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}