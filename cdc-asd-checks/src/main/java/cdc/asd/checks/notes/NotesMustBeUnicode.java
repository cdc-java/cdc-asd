package cdc.asd.checks.notes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.documentations.AbstractDocumentationTextMustBeUnicode;

public class NotesMustBeUnicode extends AbstractDocumentationTextMustBeUnicode {
    public static final String NAME = "G05";
    public static final String TITLE = "NOTES_MUST_BE_UNICODE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap}" + MUST_BE_UNICODE,
                                                                N_NOTES)
                                                        .appliesTo("The notes of all attributes",
                                                                   "The notes of all classes",
                                                                   "The notes of all connectors",
                                                                   "The notes of all connector tips",
                                                                   "The notes of all interfaces",
                                                                   "The notes of all packages",
                                                                   "The notes of all tags"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public NotesMustBeUnicode(SnapshotManager manager) {
        super(manager,
              RULE);
    }
}