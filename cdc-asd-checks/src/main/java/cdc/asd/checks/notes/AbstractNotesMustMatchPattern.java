package cdc.asd.checks.notes;

import java.util.regex.Pattern;

import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfElement;
import cdc.util.strings.StringUtils;

public abstract class AbstractNotesMustMatchPattern<O extends MfElement> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    protected static final String MUST_MATCH_THIS_PATTERN = " must match this pattern: ";

    private final Pattern pattern;

    protected AbstractNotesMustMatchPattern(SnapshotManager manager,
                                            Class<O> objectClass,
                                            Rule rule,
                                            String regex) {
        super(manager,
              objectClass,
              rule);
        this.pattern = Pattern.compile(regex);
    }

    @Override
    protected final String getHeader(O object) {
        return getTheNotesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final String notes = object.wrap(AsdElement.class).getNotes();
        if (!StringUtils.isNullOrEmpty(notes)) {
            final boolean matches = pattern.matcher(notes).matches();
            if (!matches) {
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .value(notes)
                           .violation("does not match " + pattern.pattern());

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }
}