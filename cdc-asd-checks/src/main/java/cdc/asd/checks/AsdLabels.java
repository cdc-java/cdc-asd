package cdc.asd.checks;

/**
 * Standard labels that can be attached to an ASD rule.
 */
public final class AsdLabels {
    private AsdLabels() {
    }

    /** The rule contradicts another rule. */
    public static final String CONTRADICTION = "Contradiction";

    /** The rule is deprecated. */
    public static final String DEPRECATED = "Deprecated";

    /** The rule is a DRAFT. */
    public static final String DRAFT = "Draft";

    /** There are no UML Writing Rules and Style Guide entry for that rule. */
    public static final String UWRSG_SOURCE_MISSING = "No_UWRSG_source";

    /** The rule has a direct impact on XSD generation. */
    public static final String XSD_GENERATION = "XSD_generation";
}