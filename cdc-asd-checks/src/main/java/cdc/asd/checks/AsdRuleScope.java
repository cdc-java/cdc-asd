package cdc.asd.checks;

import java.util.HashMap;
import java.util.Map;

public enum AsdRuleScope {
    ATTRIBUTE('A', "Attribute"),
    CLASS('C', "Class"),
    DIAGRAM('D', "Diagram"),
    EXCHANGE_CLASS('E', "Exchange class"),
    ATTRIBUTE_GROUP('G', "Attribute group"),
    INTERFACE('I', "Interface"),
    MODEL_LEVEL('L', "Model-level"),
    MISCELLANEOUS('M', "Miscellaneous"),
    NOTES('N', "Notes"),
    OTHER('O', "Other"),
    PACKAGE('P', "Package"),
    CONSTRAINT('R', "Constraint"),
    METHOD('S', "Method"),
    TAG('T', "Tag"),
    COMPOUND_ATTRIBUTE('U', "Compound attribute"),
    MODEL('X', "Model structure"),
    CONNECTOR('Y', "Connector");

    private final char code;
    private final String literal;

    private static final Map<Character, AsdRuleScope> MAP = new HashMap<>();

    static {
        for (final AsdRuleScope x : AsdRuleScope.values()) {
            MAP.put(x.getCode(), x);
        }
    }

    private AsdRuleScope(char code,
                         String literal) {
        this.code = code;
        this.literal = literal;
    }

    public char getCode() {
        return code;
    }

    public String getLiteral() {
        return literal;
    }

    public static AsdRuleScope of(char code) {
        return MAP.get(code);
    }
}