package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfClass;

public class ClassWhenSomeTagWhenUidPatternCountMustBe0 extends AbstractTagNameCountMustMatch<MfClass> {
    public static final String NAME = "C47";
    public static final String TITLE = "CLASS(SOME)_TAG(UID_PATTERN)_COUNT_MUST_BE_0";
    private static final AsdTagName TAG_NAME = AsdTagName.UID_PATTERN;
    private static final int MINMAX = 0;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_CLASS + ES, TAG_NAME, MINMAX))
                                                        .appliesTo("All " + N_GENERALIZED + WS + AsdNames.S_CLASS + " classes",
                                                                   "All " + N_GENERALIZED + WS + AsdNames.S_RELATIONSHIP
                                                                           + " classes",
                                                                   "All " + N_GENERALIZED + WS + AsdNames.S_PROXY + " classes",
                                                                   "All " + AsdNames.S_ATTRIBUTE_GROUP + " classes",
                                                                   "All " + AsdNames.S_BUILTIN + " classes",
                                                                   "All " + AsdNames.S_COMPOUND_ATTRIBUTE + " classes",
                                                                   "All " + AsdNames.S_EXCHANGE + " classes",
                                                                   "All " + AsdNames.S_METACLASS + " classes",
                                                                   "All " + AsdNames.S_PRIMITIVE + " classes",
                                                                   "All " + AsdNames.S_UML_PRIMITIVE + " classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.3")
                                                        .relatedTo(AsdRule.UID_PATTERN_EXCLUSION)
                                                        .remarks("11.1.3 does not require uidPattern for <<proxy>> classes.",
                                                                 "specialized was replaced by generalized.",
                                                                 "A generalized class is the specialization of another class: it extends another class.",
                                                                 "Wording of " + AsdNames.SOURCE_UMLWRSG_2_0 + " and "
                                                                         + AsdRule.UID_PATTERN_EXCLUSION + " should be fixed."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.DEPRECATED)
                        .build();

    protected ClassWhenSomeTagWhenUidPatternCountMustBe0(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfClass object) {
        return !ClassUtils.expectsUidPattern(object);
    }
}