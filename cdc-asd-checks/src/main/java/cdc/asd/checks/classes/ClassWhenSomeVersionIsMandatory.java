package cdc.asd.checks.classes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.misc.AbstractVersionIsMandatory;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfClass;

public class ClassWhenSomeVersionIsMandatory extends AbstractVersionIsMandatory<MfClass> {
    public static final String NAME = "C55";
    public static final String TITLE = "CLASS(SOME)_VERSION_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_CLASS + ES))
                                                        .appliesTo("All classes except <<builtin>> classes and BaseObject")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.1")
                                                        .relatedTo(AsdRule.CLASS_VERSION),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public ClassWhenSomeVersionIsMandatory(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    public boolean accepts(MfClass object) {
        return !object.wrap(AsdElement.class).isBuiltin()
                && !object.wrap(AsdElement.class).isBaseObject();
    }
}