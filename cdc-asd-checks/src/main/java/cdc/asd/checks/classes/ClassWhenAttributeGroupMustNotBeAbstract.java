package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;

/**
 * Check that an {@link AsdStereotypeName#ATTRIBUTE_GROUP ATTRIBUTE_GROUP} class is not abstract.
 *
 * @author Damien Carbonne
 */
public class ClassWhenAttributeGroupMustNotBeAbstract extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C18";
    public static final String TITLE = "CLASS(ATTRIBUTE_GROUP)_MUST_NOT_BE_ABSTRACT";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("An {%wrap} cannot be abstract.",
                                                                AsdNames.S_ATTRIBUTE_GROUP + WS + N_CLASS)
                                                        .relatedTo(AsdRule.ATTRIBUTE_GROUP_NOT_ABSTRACT),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected ClassWhenAttributeGroupMustNotBeAbstract(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        if (object.isAbstract()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("cannot be abtract");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdElement.class).is(AsdStereotypeName.ATTRIBUTE_GROUP);
    }
}