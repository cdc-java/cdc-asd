package cdc.asd.checks.classes;

import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfType;

public class ClassMultipleInheritanceIsForbidden extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C03";
    public static final String TITLE = "CLASS_MULTIPLE_INHERITANCE_IS_FORBIDDEN";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} cannot inherit from several {%wrap}.",
                                                                N_CLASS,
                                                                N_CLASS + ES)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.4.3")
                                                        .relatedTo(AsdRule.CLASS_SPECIALIZATION),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    protected ClassMultipleInheritanceIsForbidden(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass item) {
        return getTheItemHeader(item);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        final Set<? extends MfType> set = object.getDirectGenitors();
        if (set.size() > 1) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has multiple inheritance, which is not allowed")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}