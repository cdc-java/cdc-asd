package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.misc.AbstractElementMustNotOwnAny;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfClass;

public class ClassWhenExchangeMustNotOwnAnyAssociation extends AbstractElementMustNotOwnAny<MfClass> {
    public static final String NAME = "C26";
    public static final String TITLE = "CLASS(EXCHANGE)_MUST_NOT_OWN_ANY_ASSOCIATION";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define(describe(AsdNames.S_EXCHANGE + WS + N_CLASS, N_ASSOCIATION))
                                                        .appliesTo("All " + AsdNames.S_EXCHANGE + " classes")
                                                        .relatedTo(AsdRule.EXCHANGE_CONNECTOR),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.12.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    protected ClassWhenExchangeMustNotOwnAnyAssociation(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              MfAssociation.class,
              RULE);
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdElement.class).is(AsdStereotypeName.EXCHANGE);
    }
}