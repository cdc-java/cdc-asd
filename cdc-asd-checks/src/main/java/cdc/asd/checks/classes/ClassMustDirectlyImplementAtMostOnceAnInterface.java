package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.types.AbstractTypeMustDirectlyImplementAtMostOnceAnInterface;
import cdc.mf.model.MfClass;

public class ClassMustDirectlyImplementAtMostOnceAnInterface
        extends AbstractTypeMustDirectlyImplementAtMostOnceAnInterface<MfClass> {
    public static final String NAME = "C05";
    public static final String TITLE = "CLASS_MUST_DIRECTLY_IMPLEMENT_AT_MOST_ONCE_AN_INTERFACE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define(describe(A, N_CLASS)),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.5.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    protected ClassMustDirectlyImplementAtMostOnceAnInterface(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }
}