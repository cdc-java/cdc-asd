package cdc.asd.checks.classes;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.types.AbstractTypeMustNotPlayRole;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfTipRole;

public class ClassWhenSomeMustNotBeAggregationPart extends AbstractTypeMustNotPlayRole<MfClass> {
    public static final String NAME = "C38";
    public static final String TITLE = "CLASS(SOME)_MUST_NOT_BE_AGGREGATION_PART";
    private static final Set<AsdStereotypeName> STEREOTYPES = EnumSet.of(AsdStereotypeName.ATTRIBUTE_GROUP,
                                                                         AsdStereotypeName.COMPOUND_ATTRIBUTE,
                                                                         AsdStereotypeName.PRIMITIVE,
                                                                         AsdStereotypeName.UML_PRIMITIVE);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("Some {%wrap} must not be an {%wrap} {%wrap} (they have no identity).",
                                                                N_CLASS + ES,
                                                                N_AGGREGATION,
                                                                N_PART)
                                                        .appliesTo(STEREOTYPES)
                                                        .relatedTo(AsdRule.ATTRIBUTE_GROUP_NO_ASSOCIATIONS,
                                                                   AsdRule.COMPOUND_ATTRIBUTE_ASSOCIATIONS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.20.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public ClassWhenSomeMustNotBeAggregationPart(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE,
              MfAggregation.class,
              MfTipRole.PART);
    }

    @Override
    public boolean accepts(MfClass object) {
        return STEREOTYPES.contains(object.wrap(AsdElement.class).getStereotypeName());
    }
}