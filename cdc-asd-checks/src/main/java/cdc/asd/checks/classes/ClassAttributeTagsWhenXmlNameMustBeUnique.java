package cdc.asd.checks.classes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.util.strings.StringUtils;

public class ClassAttributeTagsWhenXmlNameMustBeUnique extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C02";
    public static final String TITLE = "CLASS_ATTRIBUTE_TAGS(XML_NAME)_MUST_BE_UNIQUE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap} {%wrap} {%wrap}, including inherited {%wrap}, must be unique.",
                                                                N_CLASS,
                                                                N_ATTRIBUTE,
                                                                AsdNames.T_XML_NAME + WS + N_TAG,
                                                                N_VALUE + S,
                                                                N_ATTRIBUTE + S)
                                                        .relatedTo(AsdRule.ATTRIBUTE_XML_NAME_UNIQUE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected ClassAttributeTagsWhenXmlNameMustBeUnique(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass item,
                             Location location) {
        final Set<MfProperty> attributes = item.getAllProperties();
        final Map<String, Set<MfProperty>> counts = new HashMap<>();
        boolean violation = false;
        for (final MfProperty attribute : attributes) {
            final List<MfTag> tags = attribute.getTags(AsdTagName.XML_NAME.getLiteral());
            // Ignore tags if there are many of them
            if (tags.size() == 1) {
                final String value = tags.get(0).getValue();
                // Ignore value if it is not significant
                if (!StringUtils.isNullOrEmpty(value)) {
                    final Set<MfProperty> set = counts.computeIfAbsent(value, k -> new HashSet<>());
                    set.add(attribute);
                    if (set.size() > 1) {
                        violation = true;
                    }
                }
            }
        }

        if (violation) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(item))
                       .violation("has duplicate attribute xmlName tags");
            for (final Map.Entry<String, Set<MfProperty>> entry : counts.entrySet()) {
                final Set<MfProperty> set = entry.getValue();
                if (set.size() > 1) {
                    description.uItems(entry.getKey())
                               .elements(1, set);
                }
            }
            add(issue().description(description)
                       .location(item)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}