package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.misc.AbstractAuthorIsMandatory;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfClass;

/**
 * Check that a class has an author.
 *
 * @author Damien Carbonne
 */
public class ClassWhenSomeAuthorIsMandatory extends AbstractAuthorIsMandatory<MfClass> {
    public static final String NAME = "C36";
    public static final String TITLE = "CLASS(SOME)_AUTHOR_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(ALL, N_CLASS + ES))
                                                        .appliesTo("All classes except <<builtin>> ones")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.1")
                                                        .relatedTo(AsdRule.CLASS_AUTHOR),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public ClassWhenSomeAuthorIsMandatory(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdElement.class).getStereotypeName() != AsdStereotypeName.BUILTIN;
    }
}