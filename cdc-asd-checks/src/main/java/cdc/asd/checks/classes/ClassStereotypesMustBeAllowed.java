package cdc.asd.checks.classes;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.stereotypes.AbstractStereotypesMustBeAllowed;
import cdc.asd.model.AsdStereotypeName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfClass;

public class ClassStereotypesMustBeAllowed extends AbstractStereotypesMustBeAllowed<MfClass> {
    public static final String NAME = "C13";
    public static final String TITLE = "CLASS_STEREOTYPE_MUST_BE_ALLOWED"; // FIXME plural

    /** Stereotypes that can be used with classes (11.1.2). */
    private static final Set<AsdStereotypeName> ALLOWED_STEREOTYPES =
            EnumSet.of(AsdStereotypeName.ATTRIBUTE_GROUP,
                       AsdStereotypeName.BUILTIN, // Local
                       AsdStereotypeName.CLASS,
                       AsdStereotypeName.COMPOUND_ATTRIBUTE, // Can not be used in spec models
                       AsdStereotypeName.EXCHANGE,
                       AsdStereotypeName.METACLASS,  // Can not be used in spec models
                       AsdStereotypeName.PRIMITIVE,  // Can not be used in spec models
                       AsdStereotypeName.RELATIONSHIP,
                       AsdStereotypeName.UML_PRIMITIVE);  // Can not be used in spec models

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_CLASS))
                                                        .text(oneOf(ALLOWED_STEREOTYPES))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.2")
                                                        .relatedTo(AsdRule.CLASS_STEREOTYPES)
                                                        .remarks("11.1.2 lists only 4 stereotypes."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public ClassStereotypesMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected boolean isAllowed(MfClass object,
                                AsdStereotypeName stereotypeName) {
        return ALLOWED_STEREOTYPES.contains(stereotypeName);
    }
}