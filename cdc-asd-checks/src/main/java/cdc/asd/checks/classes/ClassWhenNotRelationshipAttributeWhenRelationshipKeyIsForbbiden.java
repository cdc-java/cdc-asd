package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfProperty;

public class ClassWhenNotRelationshipAttributeWhenRelationshipKeyIsForbbiden extends MfAbstractRuleChecker<MfProperty> {
    public static final String NAME = "C31";
    public static final String TITLE = "CLASS(NOT_RELATIONSHIP)_ATTRIBUTE(RELATIONSHIP_KEY)_IS_FORBIDDEN";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} that are not {%wrap} cannot have {%wrap}.",
                                                                N_CLASS + ES,
                                                                AsdNames.S_RELATIONSHIP + WS + N_CLASS + ES,
                                                                AsdNames.S_RELATIONSHIP_KEY + WS + N_ATTRIBUTE + S)
                                                        .appliesTo("All attributes of classes that are not relationships")
                                                        .relatedTo(AsdRule.ATTRIBUTE_CLASS_KEYS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public ClassWhenNotRelationshipAttributeWhenRelationshipKeyIsForbbiden(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty object) {
        return getTheStereotypesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfProperty item,
                             Location location) {
        final AsdStereotypeName stereotypeName = item.wrap(AsdElement.class).getStereotypeName();
        if (stereotypeName == AsdStereotypeName.RELATIONSHIP_KEY) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(item))
                       .violation("is forbidden")
                       .justifications("This attribute belongs to a class that is not a realtionship");

            add(issue().description(description)
                       .location(item)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfProperty object) {
        return object.getParent().wrap(AsdElement.class).getStereotypeName() != AsdStereotypeName.RELATIONSHIP;
    }
}