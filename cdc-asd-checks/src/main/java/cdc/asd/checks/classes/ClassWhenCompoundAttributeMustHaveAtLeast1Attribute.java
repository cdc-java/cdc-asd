package cdc.asd.checks.classes;

import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfProperty;

public class ClassWhenCompoundAttributeMustHaveAtLeast1Attribute extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C22";
    public static final String TITLE = "CLASS(COMPOUND_ATTRIBUTE)_MUST_HAVE_AT_LEAST_1_ATTRIBUTE";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} must have at least 1 {%wrap}.",
                                                                N_CONCRETE + WS + AsdNames.S_COMPOUND_ATTRIBUTE
                                                                        + WS + N_CLASS + ES,
                                                                N_ATTRIBUTE)
                                                        .relatedTo(AsdRule.COMPOUND_ATTRIBUTE_NUMBER_OF_ATTRIBUTES)
                                                        .remarks("This rule is an adaptation of ASD Rule.",
                                                                 "This could be applied to other concrete classes."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public ClassWhenCompoundAttributeMustHaveAtLeast1Attribute(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        final Set<MfProperty> attributes = object.getAllProperties();
        if (attributes.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has not enough attributes")
                       .justifications("expecting at least 1",
                                       "found " + attributes.size());
            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdElement.class).is(AsdStereotypeName.COMPOUND_ATTRIBUTE)
                && !object.isAbstract();
    }
}