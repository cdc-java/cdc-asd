package cdc.asd.checks.classes;

import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdType;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfType;

public class ClassWhenCompositionPartAttributeWhenKeyStereotypeMustBeCompositeKey extends MfAbstractRuleChecker<MfProperty> {
    public static final String NAME = "C21";
    public static final String TITLE = "CLASS(COMPOSITION_PART)_ATTRIBUTE(KEY)_STEREOTYPE_MUST_BE_COMPOSITE_KEY";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} that are {%wrap} {%wrap} of {%wrap} {%wrap} must use {%wrap} for their key {%wrap}."
                                      + "\n{%wrap} and {%wrap} are forbidden.",
                                                                N_CLASS + ES,
                                                                N_COMPOSITION,
                                                                N_PART + S,
                                                                "non-" + AsdNames.S_EXCHANGE,
                                                                N_CLASS + ES,
                                                                AsdNames.S_COMPOSITE_KEY + WS + N_STEREOTYPE,
                                                                N_ATTRIBUTE + S,
                                                                AsdNames.S_KEY + WS + N_STEREOTYPE,
                                                                AsdNames.S_RELATIONSHIP_KEY + WS + N_STEREOTYPE)
                                                        .appliesTo("All attributes of classes that are composition parts of non-<<exhange>> classes")
                                                        .relatedTo(AsdRule.ATTRIBUTE_COMPOSITE_KEYS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public ClassWhenCompositionPartAttributeWhenKeyStereotypeMustBeCompositeKey(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty object) {
        return getTheStereotypesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfProperty object,
                             Location location) {
        final AsdStereotypeName stereotypeName = object.wrap(AsdElement.class).getStereotypeName();
        if (stereotypeName == AsdStereotypeName.KEY || stereotypeName == AsdStereotypeName.RELATIONSHIP_KEY) {
            final IssueDescription.Builder description = IssueDescription.builder();

            final Set<MfComposition> set = ((MfType) object.getParent()).getAllReversedConnectors(MfComposition.class);

            description.header(getHeader(object))
                       .value(stereotypeName.getLiteral())
                       .violation("is forbidden")
                       .justifications("This attribute belongs to a class that is part of composition(s) of non-<<exchange>> classes")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfProperty object) {
        // Is this class a composition part of a non-<<exchange>> class ?
        final MfType type = (MfType) object.getParent();
        return type.getAllReversedConnectors(MfComposition.class)
                   .stream()
                   .anyMatch(c -> !c.getSourceTip().getType().wrap(AsdType.class).is(AsdStereotypeName.EXCHANGE));
    }
}