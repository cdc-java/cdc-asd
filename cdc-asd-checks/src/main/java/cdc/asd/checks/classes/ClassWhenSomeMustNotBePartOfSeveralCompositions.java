package cdc.asd.checks.classes;

import java.util.Set;
import java.util.stream.Collectors;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.wrappers.AsdClass;
import cdc.asd.model.wrappers.AsdConnector;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;

public class ClassWhenSomeMustNotBePartOfSeveralCompositions extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C40";
    public static final String TITLE = "CLASS(SOME)_MUST_NOT_BE_PART_OF_SEVERAL_COMPOSITIONS";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("Some {%wrap} cannot be part of several {%wrap}.",
                                                                N_CLASS + ES,
                                                                N_COMPOSITION + S)
                                                        .appliesTo("Classes that have a business id (one or more local or inherited key).")
                                                        .relatedTo(AsdRule.CLASS_COMPOSITION),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public ClassWhenSomeMustNotBePartOfSeveralCompositions(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected final String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public final CheckResult check(CheckContext context,
                                   MfClass object,
                                   Location location) {
        final Set<MfComposition> set =
                object.getAllReversedConnectors(MfComposition.class)
                      .stream()
                      // Ignore local compositons
                      .filter(x -> !x.wrap(AsdConnector.class).hasLocalConstraint())
                      .collect(Collectors.toSet());
        if (set.size() > 1) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getTheItemHeader(object))
                       .violation("is part of " + set.size() + " compositions")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdClass.class).hasKeyBusinessId();
    }
}