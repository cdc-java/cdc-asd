package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfClass;

public class ClassWhenSomeTagWhenXmlNameCountMustBe1 extends AbstractTagNameCountMustMatch<MfClass> {
    public static final String NAME = "C50";
    public static final String TITLE = "CLASS(SOME)_TAG(XML_NAME)_COUNT_MUST_BE_1";
    private static final AsdTagName TAG_NAME = AsdTagName.XML_NAME;
    private static final int MINMAX = 1;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_CLASS + ES, TAG_NAME, MINMAX))
                                                        .appliesTo("All classes except " + AsdNames.S_BUILTIN + " and "
                                                                + AsdNames.S_METACLASS
                                                                + " classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.3",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.1")
                                                        .relatedTo(AsdRule.CLASS_XML_NAME)
                                                        .remarks("11.1.3 requires xmlName to be defined for all classes."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    protected ClassWhenSomeTagWhenXmlNameCountMustBe1(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfClass object) {
        return ClassUtils.expectsXmlName(object);
    }
}