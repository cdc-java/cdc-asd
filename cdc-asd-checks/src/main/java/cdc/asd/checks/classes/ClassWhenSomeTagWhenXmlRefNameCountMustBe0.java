package cdc.asd.checks.classes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfClass;

public class ClassWhenSomeTagWhenXmlRefNameCountMustBe0 extends AbstractTagNameCountMustMatch<MfClass> {
    public static final String NAME = "C51";
    public static final String TITLE = "CLASS(SOME)_TAG(XML_REF_NAME)_COUNT_MUST_BE_0";
    private static final AsdTagName TAG_NAME = AsdTagName.XML_REF_NAME;
    private static final int MINMAX = 0;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_CLASS + ES, TAG_NAME, MINMAX))
                                                        .appliesTo("All classes except " + AsdNames.S_CLASS + ", "
                                                                + AsdNames.S_RELATIONSHIP
                                                                + " and " + AsdNames.S_PROXY + WS + N_CLASS + ES)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.3")
                                                        .relatedTo(AsdRule.XML_REF_NAME),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    protected ClassWhenSomeTagWhenXmlRefNameCountMustBe0(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfClass object) {
        final AsdStereotypeName sn = object.wrap(AsdElement.class).getStereotypeName();
        return !(sn == AsdStereotypeName.CLASS
                || sn == AsdStereotypeName.RELATIONSHIP
                || sn == AsdStereotypeName.PROXY);
    }
}