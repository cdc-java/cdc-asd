package cdc.asd.checks.classes;

import java.util.List;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;

public class ClassWhenAttributeGroupMustBeCompositionPart extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C17";
    public static final String TITLE = "CLASS(ATTRIBUTE_GROUP)_MUST_BE_COMPOSITION_PART";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("An {%wrap} must be a {%wrap} {%wrap} of a {%wrap}, {%wrap} or {%wrap}.",
                                                                AsdNames.S_ATTRIBUTE_GROUP + WS + N_CLASS,
                                                                N_COMPOSITION,
                                                                N_PART,
                                                                AsdNames.S_CLASS + WS + N_CLASS,
                                                                AsdNames.S_RELATIONSHIP + WS + N_CLASS,
                                                                AsdNames.S_EXTEND + WS + N_CLASS)
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.2.3")
                                                        .relatedTo(AsdRule.ATTRIBUTE_GROUP_COMPOSITION)
                                                        .remarks("Rule needs to be refined."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public ClassWhenAttributeGroupMustBeCompositionPart(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        final List<MfComposition> set = object.getAllReversedConnectors(MfComposition.class)
                                              .stream()
                                              .filter(x -> !x.getWholeTip().getType().hasStereotype(AsdStereotypeName.CLASS,
                                                                                                    AsdStereotypeName.RELATIONSHIP,
                                                                                                    AsdStereotypeName.EXTEND))
                                              .toList();
        if (!set.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("is composition part of a class that is neither a <<class>>, <<relationship>> nor <<extend>>")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdElement.class).getStereotypeName() == AsdStereotypeName.ATTRIBUTE_GROUP;
    }
}