package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.types.AbstractTypeMustNotDeclareUselessInheritance;
import cdc.mf.model.MfClass;

public class ClassMustNotDeclareUselessInheritance
        extends AbstractTypeMustNotDeclareUselessInheritance<MfClass> {
    public static final String NAME = "C07";
    public static final String TITLE = "CLASS_MUST_NOT_DECLARE_USELESS_INHERITANCE";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define(describe(A, N_CLASS)),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.19.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    protected ClassMustNotDeclareUselessInheritance(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }
}