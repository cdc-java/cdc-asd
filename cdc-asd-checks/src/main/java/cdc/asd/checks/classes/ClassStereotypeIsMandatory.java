package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.stereotypes.AbstractStereotypeIsMandatory;
import cdc.mf.model.MfClass;

public class ClassStereotypeIsMandatory extends AbstractStereotypeIsMandatory<MfClass> {
    public static final String NAME = "C12";
    public static final String TITLE = "CLASS_STEREOTYPE_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(ALL, N_CLASS + ES))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.1")
                                                        .relatedTo(AsdRule.CLASS_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION)
                        .build();

    public ClassStereotypeIsMandatory(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }
}