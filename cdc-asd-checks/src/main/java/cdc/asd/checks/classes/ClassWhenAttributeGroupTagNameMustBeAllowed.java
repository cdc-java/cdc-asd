package cdc.asd.checks.classes;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdModelUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameMustBeAllowed;
import cdc.mf.model.MfEnum;
import cdc.mf.model.MfTag;

/**
 * Check that the name of a {@link AsdStereotypeName#ATTRIBUTE_GROUP ATTRIBUTE_GROUP} class tag is allowed.
 *
 * @author Damien Carbonne
 */
public class ClassWhenAttributeGroupTagNameMustBeAllowed extends AbstractTagNameMustBeAllowed {
    public static final String NAME = "C19";
    public static final String TITLE = "CLASS(ATTRIBUTE_GROUP)_TAG_NAME_MUST_BE_ALLOWED";

    private static final AsdStereotypeName STEREOTYPE = AsdStereotypeName.ATTRIBUTE_GROUP;

    /** Tags that can be associated to {@link AsdStereotypeName#ATTRIBUTE_GROUP ATTRIBUTE_GROUP} classes. */
    private static final Set<AsdTagName> ALLOWED_TAGS =
            EnumSet.of(AsdTagName.XML_NAME,
                       AsdTagName.NOTE,
                       AsdTagName.EXAMPLE,
                       AsdTagName.REPLACES,
                       AsdTagName.SOURCE,
                       AsdTagName.REF,
                       AsdTagName.CHANGE_NOTE);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(AsdModelUtils.identify(STEREOTYPE) + " " + N_CLASS))
                                                        .text(oneOf(MfEnum.toSet(ALLOWED_TAGS)))
                                                        .appliesTo("All recognized tag names of all "
                                                                + AsdModelUtils.identify(STEREOTYPE) + " classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.1 for "
                                                                + AsdNames.T_XML_NAME,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.5 for "
                                                                         + AsdNames.T_CHANGE_NOTE,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.6 for " + AsdNames.T_REF,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.7 for " + AsdNames.T_SOURCE,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.8 for " + AsdNames.T_NOTE,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.9 for "
                                                                         + AsdNames.T_EXAMPLE,
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.10 for "
                                                                         + AsdNames.T_REPLACES)
                                                        .relatedTo(AsdRule.ATTRIBUTE_GROUP_TAGS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public ClassWhenAttributeGroupTagNameMustBeAllowed(SnapshotManager manager) {
        super(manager,
              RULE);
    }

    @Override
    protected boolean isRecognizedAndNotAllowed(String tagName) {
        final AsdTagName tn = AsdTagName.of(tagName);
        return tn != null && !ALLOWED_TAGS.contains(tn);
    }

    @Override
    public boolean accepts(MfTag object) {
        return object.getParent().wrap(AsdElement.class).getStereotypeName() == STEREOTYPE;
    }
}