package cdc.asd.checks.classes;

import java.util.Set;
import java.util.function.Predicate;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;

public class ClassMustNotBeCompositionPartOfExchangeAndNonExchangeClasses extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C06";
    public static final String TITLE = "CLASS_MUST_NOT_BE_COMPOSITION_PART_OF_EXCHANGE_AND_NON_EXCHANGE_CLASSES";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} cannot simultaneously be {%wrap} {%wrap} of an {%wrap} and of a {%wrap}.",
                                                                N_CLASS,
                                                                N_COMPOSITION,
                                                                N_PART,
                                                                AsdNames.S_EXCHANGE + WS + N_CLASS,
                                                                "non " + AsdNames.S_EXCHANGE + WS + N_CLASS)
                                                        .relatedTo(AsdRule.WRONG_CLASS_IN_EXCHANGE)
                                                        .remarks("Definition may need to be refined"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public ClassMustNotBeCompositionPartOfExchangeAndNonExchangeClasses(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected final String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    private static boolean isPartOfExchange(MfComposition composition) {
        return composition.getSourceTip().getType().wrap(AsdElement.class).getStereotypeName() == AsdStereotypeName.EXCHANGE;

    }

    @Override
    public final CheckResult check(CheckContext context,
                                   MfClass object,
                                   Location location) {
        final Set<MfComposition> set = object.getAllReversedConnectors(MfComposition.class);
        boolean partOfExchange = false;
        boolean partOfNormal = false;
        for (final MfComposition connector : set) {
            // if (connector.getTip(EaConnectionRole.WHOLE).getObject().getStereotypeName() == EaStereotypeName.EXCHANGE) {
            if (isPartOfExchange(connector)) {
                partOfExchange = true;
            } else {
                partOfNormal = true;
            }
        }
        if (partOfExchange && partOfNormal) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("is composition part of <<exchange>> and non-<<echange>> classes")
                       .uItem("<<exchange>>")
                       .elements(1,
                                 sortUsingId(set.stream()
                                                .filter(ClassMustNotBeCompositionPartOfExchangeAndNonExchangeClasses::isPartOfExchange)
                                                .toList()))
                       .uItem("non-<<exchange>>")
                       .elements(1,
                                 sortUsingId(set.stream()
                                                .filter(Predicate.not(ClassMustNotBeCompositionPartOfExchangeAndNonExchangeClasses::isPartOfExchange))
                                                .toList()));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}