package cdc.asd.checks.classes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.misc.AbstractVersionMustBeAllowed;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfClass;

public class ClassVersionMustBeAllowed extends AbstractVersionMustBeAllowed<MfClass> {
    public static final String NAME = "C15";
    public static final String TITLE = "CLASS_VERSION_MUST_BE_ALLOWED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_CLASS))
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 4.1.3")
                                                        .relatedTo(AsdRule.CLASS_VERSION),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public ClassVersionMustBeAllowed(SnapshotManager context) {
        super(context,
              MfClass.class,
              RULE);
    }
}