package cdc.asd.checks.classes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameMustMatchPattern;
import cdc.mf.model.MfClass;

public class ClassWhenSomeNameMustBeUpperCamelCase extends AbstractNameMustMatchPattern<MfClass> {
    public static final String NAME = "C45";
    public static final String TITLE = "CLASS(SOME)_NAME_MUST_BE_UPPER_CAMEL_CASE";
    public static final String REGEX = UPPER_CAMEL_CASE_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap}" + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                N_CLASS,
                                                                N_NAME + S)
                                                        .text("\nThey must be UpperCamelCase.")
                                                        .appliesTo("Name of all classes, except <<umlPrimitive>> and <<builtin>> classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 13",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 13.1.1 for Camel Case",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 13.1.2 for valid characters")
                                                        .relatedTo(AsdRule.CLASS_NAME_RULES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public ClassWhenSomeNameMustBeUpperCamelCase(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE,
              REGEX);
    }

    @Override
    public boolean accepts(MfClass object) {
        final AsdStereotypeName stereotypeName = object.wrap(AsdElement.class).getStereotypeName();
        return stereotypeName != AsdStereotypeName.BUILTIN
                && stereotypeName != AsdStereotypeName.UML_PRIMITIVE;
    }
}