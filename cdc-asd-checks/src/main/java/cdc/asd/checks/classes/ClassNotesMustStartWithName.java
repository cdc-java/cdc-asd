package cdc.asd.checks.classes;

import java.util.List;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.notes.AbstractNotesMustStartWithName;
import cdc.asd.model.wrappers.AsdClass;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfClass;

public class ClassNotesMustStartWithName extends AbstractNotesMustStartWithName<MfClass> {
    public static final String NAME = "C10";
    public static final String TITLE = "CLASS_NOTES_MUST_START_WITH_NAME";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_CLASS))
                                                        .relatedTo(AsdRule.CLASS_DEFINITION_AUTHORING)
                                                        .appliesTo("All classes notes"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public ClassNotesMustStartWithName(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected List<String> cleanName(String name) {
        return List.of(name);
    }

    @Override
    public boolean accepts(MfClass object) {
        return !object.wrap(AsdClass.class).isBuiltin()
                && !object.wrap(AsdClass.class).isBaseObject();
    }
}