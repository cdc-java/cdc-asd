package cdc.asd.checks.classes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfClass;

public class ClassWhenSomeTagWhenXmlNameCountMustBe0 extends AbstractTagNameCountMustMatch<MfClass> {
    public static final String NAME = "C49";
    public static final String TITLE = "CLASS(SOME)_TAG(XML_NAME)_COUNT_MUST_BE_0";
    private static final AsdTagName TAG_NAME = AsdTagName.XML_NAME;
    private static final int MINMAX = 0;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME, N_CLASS + ES, TAG_NAME, MINMAX))
                                                        .appliesTo("All " + AsdNames.S_BUILTIN + " classes",
                                                                   "All " + AsdNames.S_METACLASS + " classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.3")
                                                        .remarks("11.1.3 requires xmlName to be defined for all classes."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    protected ClassWhenSomeTagWhenXmlNameCountMustBe0(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfClass object) {
        return !ClassUtils.expectsXmlName(object);
    }
}