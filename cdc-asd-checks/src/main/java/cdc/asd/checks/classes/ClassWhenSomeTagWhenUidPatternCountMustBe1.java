package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagNameCountMustMatch;
import cdc.mf.model.MfClass;

public class ClassWhenSomeTagWhenUidPatternCountMustBe1 extends AbstractTagNameCountMustMatch<MfClass> {
    public static final String NAME = "C48";
    public static final String TITLE = "CLASS(SOME)_TAG(UID_PATTERN)_COUNT_MUST_BE_1";
    private static final AsdTagName TAG_NAME = AsdTagName.UID_PATTERN;
    private static final int MINMAX = 1;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(SOME,
                                                                       AsdNames.N_NON + DASH + N_GENERALIZED + WS + N_CLASS + ES,
                                                                       TAG_NAME,
                                                                       MINMAX))
                                                        .appliesTo("All non-" + N_GENERALIZED + WS + AsdNames.S_CLASS
                                                                + " classes",
                                                                   "All non-" + N_GENERALIZED + WS + AsdNames.S_RELATIONSHIP
                                                                           + " classes",
                                                                   "All non-" + N_GENERALIZED + WS + AsdNames.S_PROXY
                                                                           + " classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.3",
                                                                 AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.3")
                                                        .relatedTo(AsdRule.UID_PATTERN)
                                                        .remarks("11.1.3 does not require uidPattern for <<proxy>> classes.",
                                                                 "non-specialized was replaced by non-generalized.",
                                                                 "A non-generalized class is not the specialization of any other class: it does not extend any other class.",
                                                                 "Wording of " + AsdNames.SOURCE_UMLWRSG_2_0 + " and "
                                                                         + AsdRule.UID_PATTERN + " should be fixed."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.XSD_GENERATION,
                                AsdLabels.DEPRECATED)
                        .build();

    protected ClassWhenSomeTagWhenUidPatternCountMustBe1(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE,
              TAG_NAME,
              MINMAX);
    }

    @Override
    public boolean accepts(MfClass object) {
        return ClassUtils.expectsUidPattern(object);
    }
}