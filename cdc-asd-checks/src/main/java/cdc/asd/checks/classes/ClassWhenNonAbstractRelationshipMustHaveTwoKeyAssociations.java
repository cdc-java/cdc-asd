package cdc.asd.checks.classes;

import java.util.Set;
import java.util.stream.Collectors;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdClass;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfClass;

public class ClassWhenNonAbstractRelationshipMustHaveTwoKeyAssociations extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C29";
    public static final String TITLE = "CLASS(NON_ABSTRACT_RELATIONSHIP)_MUST_HAVE_TWO_KEY_ASSOCIATIONS";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap} must have 2 {%wrap} without {%wrap} {%wrap}.",
                                                                AsdNames.N_NON + DASH + N_ABSTRACT + WS + AsdNames.S_RELATIONSHIP,
                                                                N_CLASS + ES,
                                                                N_ASSOCIATION + S,
                                                                AsdNames.T_NO_KEY,
                                                                N_TAG)
                                                        .appliesTo("All non-abstract associations")
                                                        .relatedTo(AsdRule.RELATIONSHIP_NO_KEY)
                                                        .remarks("To be confirmed"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.21.0")
                        .labels(AsdLabels.DRAFT,
                                AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public ClassWhenNonAbstractRelationshipMustHaveTwoKeyAssociations(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        // Incomings without noKey
        final Set<MfAssociation> kincomings =
                object.getAllReversedConnectors(MfAssociation.class)
                      .stream()
                      .filter(x -> !x.hasTags(AsdTagName.NO_KEY))
                      .collect(Collectors.toSet());
        // Outgoings without noKey
        final Set<MfAssociation> koutgoings =
                object.getAllConnectors(MfAssociation.class)
                      .stream()
                      .filter(x -> !x.hasTags(AsdTagName.NO_KEY))
                      .collect(Collectors.toSet());
        final int in = kincomings.size();
        final int out = koutgoings.size();
        final int ktotal = in + out;

        if (ktotal != 2) {
            final IssueDescription.Builder description = IssueDescription.builder();

            // Incomings with noKey
            final Set<MfAssociation> nkincomings =
                    object.getAllReversedConnectors(MfAssociation.class)
                          .stream()
                          .filter(x -> x.hasTags(AsdTagName.NO_KEY))
                          .collect(Collectors.toSet());
            // Outgoings with noKey
            final Set<MfAssociation> nkoutgoings =
                    object.getAllConnectors(MfAssociation.class)
                          .stream()
                          .filter(x -> x.hasTags(AsdTagName.NO_KEY))
                          .collect(Collectors.toSet());
            final int nktotal = nkincomings.size() + nkoutgoings.size();

            description.header(getHeader(object))
                       .violation("has " + ktotal + " no-noKey (local or inherited) association(s), when it should have 2.")
                       .elements(kincomings)
                       .elements(koutgoings)
                       .violation("\nIt also has " + nktotal + " noKey (local or inherited) association(s).")
                       .elements(nkincomings)
                       .elements(nkoutgoings);

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdClass.class).is(AsdStereotypeName.RELATIONSHIP)
                && !object.isAbstract();
    }
}