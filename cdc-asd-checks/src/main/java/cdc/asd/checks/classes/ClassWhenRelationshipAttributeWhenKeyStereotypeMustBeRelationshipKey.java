package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfProperty;

public class ClassWhenRelationshipAttributeWhenKeyStereotypeMustBeRelationshipKey extends MfAbstractRuleChecker<MfProperty> {
    public static final String NAME = "C33";
    public static final String TITLE = "CLASS(RELATIONSHIP)_ATTRIBUTE(KEY)_STEREOTYPE_MUST_BE_RELATIONSHIP_KEY";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} must use {%wrap} for their key {%wrap}."
                                      + "\n{%wrap} and {%wrap} are forbidden.",
                                                                AsdNames.S_RELATIONSHIP + WS + N_CLASS + ES,
                                                                AsdNames.S_RELATIONSHIP_KEY + WS + N_STEREOTYPE,
                                                                N_ATTRIBUTE + S,
                                                                AsdNames.S_KEY + WS + N_STEREOTYPE,
                                                                AsdNames.S_COMPOSITE_KEY + WS + N_STEREOTYPE)
                                                        .appliesTo("All attributes of " + AsdNames.S_RELATIONSHIP + " classes")
                                                        .relatedTo(AsdRule.ATTRIBUTE_RELATIONSHIP_KEYS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public ClassWhenRelationshipAttributeWhenKeyStereotypeMustBeRelationshipKey(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty object) {
        return getTheStereotypesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfProperty object,
                             Location location) {
        final AsdStereotypeName stereotypeName = object.wrap(AsdElement.class).getStereotypeName();
        if (stereotypeName == AsdStereotypeName.KEY || stereotypeName == AsdStereotypeName.COMPOSITE_KEY) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("is forbidden")
                       .justifications("This attribute belongs to a " + AsdNames.S_RELATIONSHIP + " class");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfProperty object) {
        return object.getParent().wrap(AsdElement.class).getStereotypeName() == AsdStereotypeName.RELATIONSHIP;
    }
}