package cdc.asd.checks.classes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfProperty;

public class ClassAttributeNamesMustBeUnique extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C01";
    public static final String TITLE = "CLASS_ATTRIBUTE_NAMES_MUST_BE_UNIQUE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap} {%wrap}, including inherited ones, must be unique.",
                                                                N_CLASS,
                                                                N_ATTRIBUTE,
                                                                N_NAME + S)
                                                        .relatedTo(AsdRule.ATTRIBUTE_NAME_UNIQUE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected ClassAttributeNamesMustBeUnique(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        final Set<MfProperty> attributes = object.getAllProperties();
        final Map<String, Set<MfProperty>> counts = new HashMap<>();
        boolean violation = false;
        for (final MfProperty attribute : attributes) {
            final String name = attribute.getName();
            final Set<MfProperty> set = counts.computeIfAbsent(name, k -> new HashSet<>());
            set.add(attribute);
            if (set.size() > 1) {
                violation = true;
            }
        }

        if (violation) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate attribute names");
            for (final Map.Entry<String, Set<MfProperty>> entry : counts.entrySet()) {
                final Set<MfProperty> set = entry.getValue();
                if (set.size() > 1) {
                    description.uItem(entry.getKey())
                               .elements(1, set);
                }
            }
            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}