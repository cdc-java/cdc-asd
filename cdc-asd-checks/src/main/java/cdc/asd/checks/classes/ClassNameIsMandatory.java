package cdc.asd.checks.classes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameIsMandatory;
import cdc.mf.model.MfClass;

public class ClassNameIsMandatory extends AbstractNameIsMandatory<MfClass> {
    public static final String NAME = "C09";
    public static final String TITLE = "CLASS_NAME_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_CLASS + ES))
                                                        .appliesTo("All classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.1.1")
                                                        .relatedTo(AsdRule.CLASS_NAME_RULES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public ClassNameIsMandatory(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }
}