package cdc.asd.checks.classes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdConstants;
import cdc.asd.model.wrappers.AsdClass;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfProperty;

public class ClassWhenRevisionMustHaveRevisionAttributes extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C35";
    public static final String TITLE = "CLASS(REVISION)_MUST_HAVE_REVISION_ATTRIBUTES";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} must have Rationale, Date and Status {%wrap}, in that order, in last position.",
                                                                N_CLASS + WS + AsdNames.N_REVISION,
                                                                N_ATTRIBUTE + S)
                                                        .appliesTo("All root " + AsdNames.N_REVISION + " classes")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 15.1.2")
                                                        .relatedTo(AsdRule.CLASS_REVISION),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();
    /** A pattern to recognize rationale revision attributes. Add spaces at the end. */
    private static final Pattern RATIONALE_PATTERN = Pattern.compile("^.*RevisionRationale\\s*$");
    /** A pattern to recognize date revision attributes. Add spaces at the end. */
    private static final Pattern DATE_PATTERN = Pattern.compile("^.*RevisionDate\\s*$");
    /** A pattern to recognize status revision attributes. Add spaces at the end. */
    private static final Pattern STATUS_PATTERN = Pattern.compile("^.*RevisionStatus\\s*$");

    private static final List<String> REVISION_ATTRIBUTES =
            List.of(AsdConstants.RATIONALE,
                    AsdConstants.DATE,
                    AsdConstants.STATUS);

    public ClassWhenRevisionMustHaveRevisionAttributes(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        if (!ClassUtils.extendsRevision(object)) {
            final List<MfProperty> attributes = object.getProperties();
            final Set<String> namesSet = new HashSet<>();
            final List<String> namesList = new ArrayList<>();
            for (final MfProperty attribute : attributes) {
                final String name;
                if (RATIONALE_PATTERN.matcher(attribute.getName()).matches()) {
                    name = AsdConstants.RATIONALE;
                } else if (DATE_PATTERN.matcher(attribute.getName()).matches()) {
                    name = AsdConstants.DATE;
                } else if (STATUS_PATTERN.matcher(attribute.getName()).matches()) {
                    name = AsdConstants.STATUS;
                } else {
                    name = attribute.getName();
                }

                namesSet.add(name);
                namesList.add(name);
            }
            final List<String> missings = new ArrayList<>();
            for (final String name : REVISION_ATTRIBUTES) {
                if (!namesSet.contains(name)) {
                    missings.add(name);
                }
            }
            if (missings.isEmpty()) {
                // Expected attributes are there
                // Check their position
                final List<String> last3 = namesList.subList(namesList.size() - 3, namesList.size());
                if (!last3.equals(REVISION_ATTRIBUTES)) {
                    final IssueDescription.Builder description = IssueDescription.builder();

                    description.header(getHeader(object))
                               .violation("does not correctly declare Revision attributes")
                               .justifications(AsdConstants.RATIONALE + ", " + AsdConstants.DATE + ", " + AsdConstants.STATUS
                                       + " are not declared last, in that order")
                               .elements(attributes);

                    add(issue().description(description)
                               .location(object)
                               .build());
                    return CheckResult.FAILURE;
                } else {
                    return CheckResult.SUCCESS;
                }
            } else {
                // Some expected attributes are missing
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .violation("has missing Revision attributes");
                int index = 0;
                for (final String missing : missings) {
                    index++;
                    description.justification(index, missing + " is missing");
                }
                description.elements(attributes);

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdClass.class).isRevision()
                && !ClassUtils.extendsRevision(object);
    }
}