package cdc.asd.checks.classes;

import java.util.EnumSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdType;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfImplementation;
import cdc.mf.model.MfInterface;

public class ClassWhenSomeMustNotImplementExtendInterface extends MfAbstractRuleChecker<MfClass> {
    private static final Logger LOGGER = LogManager.getLogger(ClassWhenSomeMustNotImplementExtendInterface.class);
    // FIXME This rule is wrong
    public static final String NAME = "C44";
    public static final String TITLE = "CLASS(SOME)_MUST_NOT_IMPLEMENT_EXTEND_INTERFACE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    private static final Set<AsdStereotypeName> STEREOTYPES = EnumSet.of(AsdStereotypeName.ATTRIBUTE_GROUP,
                                                                         AsdStereotypeName.BUILTIN,
                                                                         AsdStereotypeName.COMPOUND_ATTRIBUTE,
                                                                         AsdStereotypeName.EXCHANGE,
                                                                         AsdStereotypeName.METACLASS,
                                                                         AsdStereotypeName.PRIMITIVE,
                                                                         AsdStereotypeName.UML_PRIMITIVE);
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("Some {%wrap} cannot implement an {%wrap}."
                                      + "\nOnly {%wrap} and {%wrap} can."
                                      + "\n{%wrap} that have 1 {%wrap} are ignored.",
                                                                N_CLASS + ES,
                                                                AsdNames.S_EXTEND + WS + N_INTERFACE,
                                                                AsdNames.S_CLASS + WS + N_CLASS + ES,
                                                                AsdNames.S_RELATIONSHIP + WS + N_CLASS + ES,
                                                                AsdNames.S_EXTEND + WS + N_INTERFACE + S,
                                                                N_COMPOSITION)
                                                        .appliesTo(STEREOTYPES)
                                                        .relatedTo(AsdRule.NO_PRIMITIVE_EXTEND,
                                                                   AsdRule.NO_ATTRIBUTE_GROUP_EXTEND,
                                                                   AsdRule.NO_COMPOUND_ATTRIBUTE_EXTEND)
                                                        .remarks("This rule is wrong and should be ignored at the moment.",
                                                                 "Should this rule be applied to all those classes?",
                                                                 "What about <<compoundAttribute>>, <<exchange>>, <<metaclass>> and <<umlPrimitive>>?",
                                                                 "There are many <<primitive>>, ... that implement an <<extend>> interface that does not own 1 single composition.",
                                                                 "As stated, ASD rules seem contradictory.",
                                                                 "How should we understand composition?",
                                                                 "ApplicabilityStatementItem owns 1 association.",
                                                                 "OrganizationReferencingItem owns 1 association.",
                                                                 "SecurityClassificationItem owns 1 association."),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    protected ClassWhenSomeMustNotImplementExtendInterface(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    private static boolean hasOneComposition(MfInterface object) {
        LOGGER.debug("hasOneComposition({})", object);
        // meaningful connectors
        final Set<MfConnector> connectors = object.getAllConnectors();
        LOGGER.debug("   meaingful connectors: {}", connectors.size());
        for (final MfConnector connector : connectors) {
            LOGGER.debug("      - {}", connector);
        }

        if (connectors.size() == 1) {
            final MfConnector connector = connectors.iterator().next();
            if (connector instanceof MfComposition) {
                // The interface has 1 meaningful connector
                // that is a composition and the interface plays the whole role
                return connector.getSourceTip().getType() == object;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        final Set<MfImplementation> set = object.getImplementations()
                                                .stream()
                                                .filter(x -> x.getGeneralType().wrap(AsdType.class).isExtendInterface()
                                                        && !hasOneComposition(x.getGeneralType()))
                                                .collect(Collectors.toSet());
        if (!set.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("cannot implement an " + AsdNames.S_EXTEND + " interface that has not one composition")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfClass object) {
        return STEREOTYPES.contains(object.wrap(AsdElement.class).getStereotypeName());
    }
}