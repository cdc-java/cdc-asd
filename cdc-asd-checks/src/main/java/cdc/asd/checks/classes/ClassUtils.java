package cdc.asd.checks.classes;

import cdc.asd.model.AsdConstants;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.mf.model.MfClass;

public final class ClassUtils {
    private ClassUtils() {
    }

    /**
     * @param object The class.
     * @return {@code true} if {@code object} has {@link MfClass} descendants.
     */
    public static boolean isSpecialized(MfClass object) {
        return !object.getDirectDescendants(MfClass.class).isEmpty();
    }

    /**
     * @param object The class.
     * @return {@code true} if {@code object} has {@link MfClass} ancestors.
     */
    public static boolean isGeneralized(MfClass object) {
        return !object.getDirectAncestors(MfClass.class).isEmpty();
    }

    public static boolean isRevision(MfClass object) {
        final String name = object.getName();
        if (name == null) {
            return false;
        } else {
            final int index = name.lastIndexOf(AsdConstants.REVISION);
            return index >= 0
                    && index == name.length() - AsdConstants.REVISION.length();
        }
    }

    /**
     * @param object The object to test.
     * @return {@code true} if {@code item} is a first level {@code <<exchange>>} specialization of MessageContent.
     */
    public static boolean isFirstLevelMessageContentSpecialization(MfClass object) {
        // The class is an <<exchange>>
        // It is a specialization of MessageContent
        // TODO How can we know it is first level indenture?
        return object.wrap(AsdElement.class).getStereotypeName() == AsdStereotypeName.EXCHANGE
                && object.getSpecializations().stream()
                         .anyMatch(x -> AsdConstants.MESSAGE_CONTENT.equals(x.getGeneralType().getName()));
        // && object.hasLocalConnectors(x -> x.isGeneralization()
        // && item == x.getSource().getObject()
        // && AsdConstants.MESSAGE_CONTENT.equals(x.getTarget().getObject().getName()));
    }

    /**
     * @param object The class.
     * @return {@code true} if {@code item} extends a Revision class.
     */
    public static boolean extendsRevision(MfClass object) {
        for (final MfClass ancestor : object.getDirectAncestors(MfClass.class)) {
            if (isRevision(ancestor)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param object The class.
     * @return {@code true} if {@code item} expects a {@link AsdTagName#UID_PATTERN UID_PATTERN} tag.
     */
    public static boolean expectsUidPattern(MfClass object) {
        final AsdStereotypeName sn = object.wrap(AsdElement.class).getStereotypeName();
        return !isGeneralized(object)
                && (sn == AsdStereotypeName.CLASS
                        || sn == AsdStereotypeName.RELATIONSHIP
                        || sn == AsdStereotypeName.PROXY);
    }

    public static boolean expectsXmlName(MfClass object) {
        final AsdStereotypeName sn = object.wrap(AsdElement.class).getStereotypeName();
        return sn != AsdStereotypeName.BUILTIN
                && sn != AsdStereotypeName.METACLASS;
    }
}