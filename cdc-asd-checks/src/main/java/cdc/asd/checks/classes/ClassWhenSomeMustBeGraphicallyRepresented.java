package cdc.asd.checks.classes;

import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.misc.AbstractElementMustBeGraphicallyRepresented;
import cdc.mf.model.MfClass;

public class ClassWhenSomeMustBeGraphicallyRepresented extends AbstractElementMustBeGraphicallyRepresented<MfClass> {
    public static final String NAME = "C57";
    public static final String TITLE = "CLASS(SOME)_MUST_BE_GRAPHICALLY_REPRESENTED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_CLASS + ES))
                                                        .appliesTo("All non-<<builtin>> classes")
                                                        .relatedTo(AsdRule.CLASS_USED),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.23.0")
                        .build();

    public ClassWhenSomeMustBeGraphicallyRepresented(SnapshotManager context) {
        super(context,
              MfClass.class,
              RULE);
    }

    @Override
    public boolean accepts(MfClass object) {
        return !object.wrap(AsdElement.class).isBuiltin();
    }
}