package cdc.asd.checks.classes;

import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.tags.AbstractTagValueMustMatchPattern;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;

public class ClassTagWhenXmlRefNameMustBeLowerCamelCase extends AbstractTagValueMustMatchPattern {
    public static final String NAME = "C14";
    public static final String TITLE = "CLASS_TAG(XML_REF_NAME)_MUST_BE_LOWER_CAMEL_CASE";
    public static final String REGEX = LOWER_CAMEL_CASE_REGEX;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} {%wrap} {%wrap}"
                                      + MUST_MATCH_THIS_PATTERN + REGEX,
                                                                N_CLASS,
                                                                AsdNames.T_XML_REF_NAME + WS + N_TAG,
                                                                N_VALUE + S)
                                                        .text("\nThey must be lowerCamelCase.")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 11.8.2")
                                                        .relatedTo(AsdRule.CLASS_XML_REF_NAME_CASE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .build();

    public ClassTagWhenXmlRefNameMustBeLowerCamelCase(SnapshotManager manager) {
        super(manager,
              RULE,
              REGEX);
    }

    @Override
    public boolean accepts(MfTag object) {
        return AsdTagName.of(object.getName()) == AsdTagName.XML_REF_NAME
                && (object.getParent() instanceof MfProperty
                        || object.getParent() instanceof MfClass
                        || object.getParent() instanceof MfInterface);
    }
}