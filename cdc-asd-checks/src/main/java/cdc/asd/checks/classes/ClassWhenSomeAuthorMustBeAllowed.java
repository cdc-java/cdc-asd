package cdc.asd.checks.classes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.misc.AbstractAuthorMustBeAllowed;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfClass;

/**
 * Check that a class author is allowed.
 *
 * @author Damien Carbonne
 */
public class ClassWhenSomeAuthorMustBeAllowed extends AbstractAuthorMustBeAllowed<MfClass> {
    public static final String NAME = "C37";
    public static final String TITLE = "CLASS(SOME)_AUTHOR_MUST_BE_ALLOWED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(A, N_CLASS))
                                                        .appliesTo("All classes except <<builtin>> ones")
                                                        .relatedTo(AsdRule.CLASS_AUTHOR),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public ClassWhenSomeAuthorMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    public boolean accepts(MfClass object) {
        return object.wrap(AsdElement.class).getStereotypeName() != AsdStereotypeName.BUILTIN;
    }
}