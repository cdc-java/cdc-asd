package cdc.asd.checks.classes;

import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfType;

public class ClassWhenNotCompositionPartAttributeWhenCompositeKeyIsForbbiden extends MfAbstractRuleChecker<MfProperty> {
    public static final String NAME = "C30";
    public static final String TITLE = "CLASS(NOT_COMPOSITION_PART)_ATTRIBUTE(COMPOSITE_KEY)_IS_FORBIDDEN";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All {%wrap} that are not {%wrap} {%wrap} cannot have {%wrap}.",
                                                                N_CLASS + ES,
                                                                N_COMPOSITION,
                                                                N_PART + S,
                                                                AsdNames.S_COMPOSITE_KEY + WS + N_ATTRIBUTE + S)
                                                        .appliesTo("All attributes of classes that are not composition parts")
                                                        .relatedTo(AsdRule.ATTRIBUTE_NO_COMPOSITE_KEYS),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public ClassWhenNotCompositionPartAttributeWhenCompositeKeyIsForbbiden(SnapshotManager manager) {
        super(manager,
              MfProperty.class,
              RULE);
    }

    @Override
    protected String getHeader(MfProperty item) {
        return getTheStereotypesOfHeader(item);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfProperty object,
                             Location location) {
        final AsdStereotypeName stereotypeName = object.wrap(AsdElement.class).getStereotypeName();
        if (stereotypeName == AsdStereotypeName.COMPOSITE_KEY) {
            final IssueDescription.Builder description = IssueDescription.builder();

            final Set<MfComposition> set = ((MfType) object.getParent()).getAllReversedConnectors(MfComposition.class);

            description.header(getHeader(object))
                       .violation("is forbidden")
                       .justifications("This attribute belongs to a class that is not part of composition(s)")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }

    @Override
    public boolean accepts(MfProperty object) {
        return !((MfType) object.getParent()).isCompositionPart();
    }
}