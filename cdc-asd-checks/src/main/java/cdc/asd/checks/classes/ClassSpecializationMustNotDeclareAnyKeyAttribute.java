package cdc.asd.checks.classes;

import java.util.List;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfProperty;

/**
 * Checks that a class does not declare any {@link AsdStereotypeName#KEY KEY},
 * {@link AsdStereotypeName#COMPOSITE_KEY COMPOSITE_KEY}
 * or {@link AsdStereotypeName#RELATIONSHIP_KEY RELATIONSHIP_KEY} attribute
 * when it is a specialization.
 * <b>Note:</b> only local attributes are checked.
 *
 * @author Damien Carbonne
 */
public class ClassSpecializationMustNotDeclareAnyKeyAttribute extends MfAbstractRuleChecker<MfClass> {
    public static final String NAME = "C11";
    public static final String TITLE = "CLASS_SPECIALIZATION_MUST_NOT_DECLARE_ANY_KEY_ATTRIBUTE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} must not declare any {%wrap}, {%wrap} or {%wrap} {%wrap}.",
                                                                N_CLASS + " specialization",
                                                                AsdNames.S_KEY,
                                                                AsdNames.S_COMPOSITE_KEY,
                                                                AsdNames.S_RELATIONSHIP_KEY,
                                                                N_ATTRIBUTE)
                                                        .appliesTo("All classes that are a specialization of a class that is not BaseObject")
                                                        .relatedTo(AsdRule.CLASS_NO_KEY_IF_SPECIALIZED),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    protected ClassSpecializationMustNotDeclareAnyKeyAttribute(SnapshotManager manager) {
        super(manager,
              MfClass.class,
              RULE);
    }

    @Override
    protected String getHeader(MfClass object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfClass object,
                             Location location) {
        // The ancestors, excluding base object.
        // There should be 0 or 1, but this does not matter
        final List<MfClass> ancestors = object.getDirectAncestors(MfClass.class)
                                              .stream()
                                              .filter(x -> !x.wrap(AsdElement.class).isBaseObject())
                                              .toList();

        if (!ancestors.isEmpty()) {
            // There are ancestor(s)
            final List<MfProperty> attributes =
                    object.getProperties()
                          .stream()
                          .filter(x -> x.hasStereotype(AsdStereotypeName.KEY,
                                                       AsdStereotypeName.COMPOSITE_KEY,
                                                       AsdStereotypeName.RELATIONSHIP_KEY))
                          .toList();
            if (!attributes.isEmpty()) {
                // There are illegal attributes
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .justifications("is a specialization",
                                           "has (" + AsdNames.S_KEY + ", " + AsdNames.S_COMPOSITE_KEY + ", "
                                                   + AsdNames.S_RELATIONSHIP_KEY
                                                   + ") attributes, which is not allowed")
                           .elements(ancestors)
                           .elements(attributes);

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }
}