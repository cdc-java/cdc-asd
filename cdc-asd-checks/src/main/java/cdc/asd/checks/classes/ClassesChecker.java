package cdc.asd.checks.classes;

import java.util.List;

import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfQNameItem;
import cdc.mf.model.MfTypeOwner;

public class ClassesChecker extends AbstractPartsChecker<MfTypeOwner, MfClass, ClassesChecker> {
    public ClassesChecker(SnapshotManager manager) {
        super(manager,
              MfTypeOwner.class,
              MfClass.class,
              new ClassChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends MfClass>> getParts(MfTypeOwner object) {
        final List<MfClass> delegates = object.getClasses()
                                              .stream()
                                              .sorted(MfQNameItem.QNAME_COMPARATOR)
                                              .toList();
        return LocatedObject.locate(delegates);
    }
}