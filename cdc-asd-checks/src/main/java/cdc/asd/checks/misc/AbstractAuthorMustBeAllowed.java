package cdc.asd.checks.misc;

import java.util.Set;

import cdc.asd.checks.AsdNames;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfElement;
import cdc.util.strings.StringUtils;

public abstract class AbstractAuthorMustBeAllowed<O extends MfElement> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    public static final Set<String> ALLOWED_AUTHORS = Set.of("DMEWG",
                                                             "S1000D",
                                                             "S2000M",
                                                             "S3000L",
                                                             "S4000P",
                                                             "S5000F",
                                                             "S6000T",
                                                             "SX000i");

    protected static String describe(String article,
                                     String item) {
        return RuleDescription.format("{%capital}defined {%wrap} {%wrap} must be allowed.",
                                      article,
                                      item,
                                      AsdNames.N_AUTHOR)
                + oneOf(ALLOWED_AUTHORS);
    }

    protected static String oneOf(Set<String> allowed) {
        final StringBuilder builder = new StringBuilder();
        builder.append("\nIt must be one of:");
        for (final String name : allowed.stream()
                                        .sorted()
                                        .toList()) {
            builder.append("\n- ")
                   .append(name);
        }
        return builder.toString();
    }

    protected AbstractAuthorMustBeAllowed(SnapshotManager manager,
                                          Class<O> objectClass,
                                          Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O object) {
        return AsdNames.getTheAuthorOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final String author = object.wrap(AsdElement.class).getAuthor();
        if (!StringUtils.isNullOrEmpty(author)
                && !ALLOWED_AUTHORS.contains(author)) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(author)
                       .violation("is not allowed");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}