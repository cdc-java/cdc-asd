package cdc.asd.checks.misc;

import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.misc.AbstractElementMustBeGraphicallyRepresented;
import cdc.mf.model.MfInheritance;

public class InheritanceMustBeGraphicallyRepresented extends AbstractElementMustBeGraphicallyRepresented<MfInheritance> {
    public static final String NAME = "G04";
    public static final String TITLE = "INHERITANCE_MUST_BE_GRAPHICALLY_REPRESENTED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_INHERITANCE + S))
                                                        .appliesTo("All inheritances")
                                                        .relatedTo(AsdRule.CLASS_USED),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.23.0")
                        .build();

    public InheritanceMustBeGraphicallyRepresented(SnapshotManager context) {
        super(context,
              MfInheritance.class,
              RULE);
    }

    @Override
    public boolean accepts(MfInheritance object) {
        return !object.wrap(AsdElement.class).isExtension();
    }
}