package cdc.asd.checks.misc;

import cdc.asd.checks.AsdNames;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfElement;
import cdc.util.strings.StringUtils;

public abstract class AbstractAuthorIsMandatory<O extends MfElement> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    protected static String describe(String article,
                                     String item) {
        return RuleDescription.wrap(article, true, item) + " must have " + RuleDescription.wrap(AN, false, AsdNames.N_AUTHOR)
                + ".";
    }

    protected AbstractAuthorIsMandatory(SnapshotManager manager,
                                        Class<O> objectClass,
                                        Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final String author = object.wrap(AsdElement.class).getAuthor();
        if (StringUtils.isNullOrEmpty(author)) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has no author");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}