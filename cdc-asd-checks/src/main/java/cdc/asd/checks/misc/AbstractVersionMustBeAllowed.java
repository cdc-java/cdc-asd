package cdc.asd.checks.misc;

import java.util.regex.Pattern;

import cdc.asd.checks.AsdNames;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfElement;
import cdc.util.strings.StringUtils;

public abstract class AbstractVersionMustBeAllowed<O extends MfElement> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;

    private static final String REGEX = "^[\\d][\\d][\\d]-00$";
    private static final Pattern PATTERN = Pattern.compile(REGEX);

    protected static String describe(String item) {
        return RuleDescription.format("All defined {%wrap} {%wrap} must match this pattern: " + REGEX,
                                      item,
                                      AsdNames.N_VERSION + S);
    }

    protected AbstractVersionMustBeAllowed(SnapshotManager manager,
                                           Class<O> objectClass,
                                           Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O object) {
        return AsdNames.getTheVersionOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final String version = object.wrap(AsdElement.class).getVersion();
        if (!StringUtils.isNullOrEmpty(version)) {
            final boolean matches = PATTERN.matcher(version).matches();
            if (!matches) {
                final IssueDescription.Builder description = IssueDescription.builder();

                description.header(getHeader(object))
                           .value(version)
                           .violation("does not match " + PATTERN.pattern());

                add(issue().description(description)
                           .location(object)
                           .build());
                return CheckResult.FAILURE;
            } else {
                return CheckResult.SUCCESS;
            }
        } else {
            return CheckResult.SUCCESS;
        }
    }
}