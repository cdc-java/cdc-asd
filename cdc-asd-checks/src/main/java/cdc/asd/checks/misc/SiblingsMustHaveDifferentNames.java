package cdc.asd.checks.misc;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractSiblingsMustHaveDifferentNames;

public class SiblingsMustHaveDifferentNames extends AbstractSiblingsMustHaveDifferentNames {
    public static final String NAME = "G03";
    public static final String TITLE = "SIBLINGS_MUST_HAVE_DIFFERENT_NAMES";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe())
                                                        .appliesTo("All sibling classes, interfaces, packages, attributes"),
                              SEVERITY)
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .meta(Metas.SINCE, "0.5.0")
                        .build();

    public SiblingsMustHaveDifferentNames(SnapshotManager manager) {
        super(manager,
              RULE);
    }
}