package cdc.asd.checks.misc;

import java.util.List;

import cdc.issues.checks.AbstractChecker;
import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInheritance;
import cdc.mf.model.MfType;

public class InheritancesChecker<O extends MfType> extends AbstractPartsChecker<O, MfInheritance, InheritancesChecker<O>> {
    @SafeVarargs
    public InheritancesChecker(SnapshotManager manager,
                               Class<O> objectClass,
                               AbstractChecker<? super MfInheritance>... partsCheckers) {
        super(manager,
              objectClass,
              MfInheritance.class,
              partsCheckers);
    }

    @Override
    protected List<LocatedObject<? extends MfInheritance>> getParts(O object) {
        final List<MfInheritance> delegates = object.getInheritances()
                                                    .stream()
                                                    .sorted(MfElement.ID_COMPARATOR)
                                                    .toList();
        return LocatedObject.locate(delegates);
    }
}