package cdc.asd.checks.stereotypes;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfTagOwner;

public class StereotypeMustBeRecognized extends AbstractStereotypeMustBeRecognized<MfTagOwner> {
    public static final String NAME = "G07";
    public static final String TITLE = "STEREOTYPE_MUST_BE_RECOGNIZED";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All defined {%wrap} must be recognized.",
                                                                N_STEREOTYPE + S)
                                                        .relatedTo(AsdRule.ATTRIBUTE_STEREOTYPES,
                                                                   AsdRule.CLASS_STEREOTYPES,
                                                                   AsdRule.DIAGRAM_STEREOTYPES,
                                                                   AsdRule.INTERFACE_STEREOTYPES,
                                                                   AsdRule.PACKAGE_STEREOTYPES),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public StereotypeMustBeRecognized(SnapshotManager manager) {
        super(manager,
              MfTagOwner.class,
              RULE);
    }
}