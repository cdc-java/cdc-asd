package cdc.asd.checks.stereotypes;

import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfTagOwner;

public abstract class AbstractStereotypeMustNotBeAnyKey<O extends MfTagOwner> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    protected static String describe(String article,
                                     String item) {
        return THE_STEREOTYPES_OF + RuleDescription.wrap(article, false, item)
                + " must not be <<key>>, <<relationshipKey>> or <<compositeKey>>.";
    }

    protected AbstractStereotypeMustNotBeAnyKey(SnapshotManager manager,
                                                Class<O> objectClass,
                                                Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O object) {
        return getSomeStereotypesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        if (object.hasStereotype(AsdStereotypeName.KEY,
                                 AsdStereotypeName.COMPOSITE_KEY,
                                 AsdStereotypeName.RELATIONSHIP_KEY)) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .value(object.wrap(AsdElement.class).getStereotype())
                       .violation("are key stereotypes");

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}