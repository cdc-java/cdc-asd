package cdc.asd.checks.stereotypes;

import java.util.List;

import cdc.asd.model.AsdStereotypeName;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfTagOwner;

public abstract class AbstractStereotypeMustBeRecognized<O extends MfTagOwner> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    protected static String describe(String item) {
        return RuleDescription.format("All defined {%wrap} {%wrap} must be recognized.",
                                      item,
                                      N_STEREOTYPE + S);
    }

    protected AbstractStereotypeMustBeRecognized(SnapshotManager manager,
                                                 Class<O> objectClass,
                                                 Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O object) {
        return getSomeStereotypesOfHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O item,
                             Location location) {
        final List<String> invalidStereotypes = item.getStereotypes().stream()
                                                    .filter(s -> !AsdStereotypeName.isRecognized(s))
                                                    .sorted()
                                                    .toList();
        if (!invalidStereotypes.isEmpty()) {
            // There are some unrecognized stereotypes
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(item))
                       .violation("are not recognized");

            for (final String stereotype : invalidStereotypes) {
                final AsdStereotypeName ic = AsdStereotypeName.ofIgnoreCase(stereotype);
                if (ic != null) {
                    description.uItem("ignoring case, '" + stereotype + "' matches '" + ic.getLiteral() + "'");
                } else {
                    final AsdStereotypeName similar = AsdStereotypeName.ofMostSimilar(stereotype);
                    if (similar != null) {
                        description.uItem("'" + stereotype + "' is a possible misspelling of '" + similar.getLiteral() + "'");
                    }
                }
            }

            add(issue().description(description)
                       .location(item)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}