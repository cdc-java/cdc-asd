package cdc.asd.checks.stereotypes;

import java.util.List;
import java.util.Set;

import cdc.asd.model.AsdStereotypeName;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfTagOwner;

public abstract class AbstractStereotypesMustBeAllowed<O extends MfTagOwner> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    protected static String describe(String item) {
        return RuleDescription.format("All defined {%wrap} {%wrap} must be allowed.",
                                      item,
                                      N_STEREOTYPE + S);
    }

    protected static String oneOf(Set<AsdStereotypeName> allowed) {
        final StringBuilder builder = new StringBuilder();
        builder.append("\nThey must be one of:");
        for (final AsdStereotypeName name : allowed) {
            builder.append("\n- ")
                   .append("<<")
                   .append(name.getLiteral())
                   .append(">>");
        }
        return builder.toString();
    }

    protected AbstractStereotypesMustBeAllowed(SnapshotManager manager,
                                               Class<O> objectClass,
                                               Rule rule) {
        super(manager,
              objectClass,
              rule);
    }

    @Override
    protected final String getHeader(O item) {
        return getTheItemHeader(item);
    }

    protected abstract boolean isAllowed(O object,
                                         AsdStereotypeName stereotypeName);

    private boolean isAllowed(O object,
                              String stereotype) {
        final AsdStereotypeName stereotypeName = AsdStereotypeName.of(stereotype);
        return stereotypeName != null && !isAllowed(object, stereotypeName);
    }

    @Override
    public CheckResult check(CheckContext context,
                             O object,
                             Location location) {
        final List<String> unexpectedStereotypes = object.getStereotypes()
                                                         .stream()
                                                         .filter(s -> isAllowed(object, s))
                                                         .sorted()
                                                         .toList();
        if (!unexpectedStereotypes.isEmpty()) {
            // There are some disallowed stereotype
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has disallowed stereotypes");

            description.uItems(unexpectedStereotypes);

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}