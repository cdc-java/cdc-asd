package cdc.asd.checks;

import cdc.asd.checks.attributes.AttributeCardinalityIsMandatory;
import cdc.asd.checks.attributes.AttributeNameIsMandatory;
import cdc.asd.checks.attributes.AttributeNameMustBeLowerCamelCase;
import cdc.asd.checks.attributes.AttributeNameShouldNotContainCode;
import cdc.asd.checks.attributes.AttributeNameShouldStartWithClassName;
import cdc.asd.checks.attributes.AttributeNotesAreMandatory;
import cdc.asd.checks.attributes.AttributeNotesMustStartWithName;
import cdc.asd.checks.attributes.AttributeStereotypeIsMandatory;
import cdc.asd.checks.attributes.AttributeStereotypesMustBeAllowed;
import cdc.asd.checks.attributes.AttributeTagNameMustBeAllowed;
import cdc.asd.checks.attributes.AttributeTagWhenUnitMustFollowAuthoring;
import cdc.asd.checks.attributes.AttributeTagWhenValidValueNotesAreMandatory;
import cdc.asd.checks.attributes.AttributeTagWhenValidValueNotesMustFollowAuthoring;
import cdc.asd.checks.attributes.AttributeTagWhenValidValueValueMustBeUnique;
import cdc.asd.checks.attributes.AttributeTagWhenXmlNameCountMustBe1;
import cdc.asd.checks.attributes.AttributeTypeIsMandatory;
import cdc.asd.checks.attributes.AttributeTypeMustBeAllowed;
import cdc.asd.checks.attributes.AttributeVisibilityMustBePublic;
import cdc.asd.checks.attributes.AttributeWhenIdentifierTagWhenValidValueValueMustNotBeEmpty;
import cdc.asd.checks.attributes.AttributeWhenSomeCardinalityLowerBoundMustBeOne;
import cdc.asd.checks.attributes.AttributeWhenSomeCardinalityUpperBoundMustBeUnbounded;
import cdc.asd.checks.attributes.AttributeWhenSomeStereotypeMustNotBeAnyKey;
import cdc.asd.checks.attributes.AttributeWhenSomeTagMustHaveValidValueOrValidValueLibrary;
import cdc.asd.checks.attributes.AttributeWhenSomeTagWhenUnitCountMustBe0;
import cdc.asd.checks.attributes.AttributeWhenSomeTagWhenUnitCountMustBe1;
import cdc.asd.checks.attributes.AttributeWhenSomeTagWhenValidValueCountMustBe0;
import cdc.asd.checks.attributes.AttributeWhenSomeTagWhenValidValueLibraryCountMustBe0;
import cdc.asd.checks.attributes.AttributeWhenSomeTagWhenValidValueLibraryCountMustBe01;
import cdc.asd.checks.attributes.AttributeWhenSomeTagWhenValidValueValueMustNotBeEmpty;
import cdc.asd.checks.cardinalities.CardinalityMustBeValid;
import cdc.asd.checks.cardinalities.CardinalityMustBeWellFormed;
import cdc.asd.checks.classes.ClassAttributeNamesMustBeUnique;
import cdc.asd.checks.classes.ClassAttributeTagsWhenXmlNameMustBeUnique;
import cdc.asd.checks.classes.ClassMultipleInheritanceIsForbidden;
import cdc.asd.checks.classes.ClassMustDirectlyExtendAtMostOnceAClass;
import cdc.asd.checks.classes.ClassMustDirectlyImplementAtMostOnceAnInterface;
import cdc.asd.checks.classes.ClassMustNotBeCompositionPartOfExchangeAndNonExchangeClasses;
import cdc.asd.checks.classes.ClassMustNotDeclareUselessInheritance;
import cdc.asd.checks.classes.ClassNameIsMandatory;
import cdc.asd.checks.classes.ClassNotesMustStartWithName;
import cdc.asd.checks.classes.ClassSpecializationMustNotDeclareAnyKeyAttribute;
import cdc.asd.checks.classes.ClassStereotypeIsMandatory;
import cdc.asd.checks.classes.ClassStereotypesMustBeAllowed;
import cdc.asd.checks.classes.ClassTagWhenXmlRefNameMustBeLowerCamelCase;
import cdc.asd.checks.classes.ClassVersionMustBeAllowed;
import cdc.asd.checks.classes.ClassVisibilityMustBePublic;
import cdc.asd.checks.classes.ClassWhenAttributeGroupMustBeCompositionPart;
import cdc.asd.checks.classes.ClassWhenAttributeGroupMustNotBeAbstract;
import cdc.asd.checks.classes.ClassWhenAttributeGroupTagNameMustBeAllowed;
import cdc.asd.checks.classes.ClassWhenClassTagNameMustBeAllowed;
import cdc.asd.checks.classes.ClassWhenCompositionPartAttributeWhenKeyStereotypeMustBeCompositeKey;
import cdc.asd.checks.classes.ClassWhenCompoundAttributeMustHaveAtLeast1Attribute;
import cdc.asd.checks.classes.ClassWhenCompoundAttributeTagNameMustBeAllowed;
import cdc.asd.checks.classes.ClassWhenExchangeAttributesAreForbidden;
import cdc.asd.checks.classes.ClassWhenExchangeMustNotOwnAnyAggregation;
import cdc.asd.checks.classes.ClassWhenExchangeMustNotOwnAnyAssociation;
import cdc.asd.checks.classes.ClassWhenExchangeMustNotOwnAnyImplementation;
import cdc.asd.checks.classes.ClassWhenExchangeTagNameMustBeAllowed;
import cdc.asd.checks.classes.ClassWhenNonAbstractRelationshipMustHaveTwoKeyAssociations;
import cdc.asd.checks.classes.ClassWhenNotCompositionPartAttributeWhenCompositeKeyIsForbbiden;
import cdc.asd.checks.classes.ClassWhenNotRelationshipAttributeWhenRelationshipKeyIsForbbiden;
import cdc.asd.checks.classes.ClassWhenPrimitiveTagNameMustBeAllowed;
import cdc.asd.checks.classes.ClassWhenRelationshipAttributeWhenKeyStereotypeMustBeRelationshipKey;
import cdc.asd.checks.classes.ClassWhenRelationshipTagNameMustBeAllowed;
import cdc.asd.checks.classes.ClassWhenRevisionMustHaveRevisionAttributes;
import cdc.asd.checks.classes.ClassWhenSomeAuthorIsMandatory;
import cdc.asd.checks.classes.ClassWhenSomeAuthorMustBeAllowed;
import cdc.asd.checks.classes.ClassWhenSomeMustBeGraphicallyRepresented;
import cdc.asd.checks.classes.ClassWhenSomeMustNotBeAggregationPart;
import cdc.asd.checks.classes.ClassWhenSomeMustNotBeAssociationTarget;
import cdc.asd.checks.classes.ClassWhenSomeMustNotBePartOfSeveralCompositions;
import cdc.asd.checks.classes.ClassWhenSomeMustNotDirectlyBeAggregationWhole;
import cdc.asd.checks.classes.ClassWhenSomeMustNotDirectlyBeAssociationSource;
import cdc.asd.checks.classes.ClassWhenSomeMustNotDirectlyBeCompositionWhole;
import cdc.asd.checks.classes.ClassWhenSomeMustNotImplementExtendInterface;
import cdc.asd.checks.classes.ClassWhenSomeNameMustBeUpperCamelCase;
import cdc.asd.checks.classes.ClassWhenSomeNotesAreMandatory;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenUidPatternCountMustBe0;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenUidPatternCountMustBe1;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenXmlNameCountMustBe0;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenXmlNameCountMustBe1;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenXmlRefNameCountMustBe0;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenXmlRefNameCountMustBe01;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenXmlSchemaNameCountMustBe0;
import cdc.asd.checks.classes.ClassWhenSomeTagWhenXmlSchemaNameCountMustBe1;
import cdc.asd.checks.classes.ClassWhenSomeVersionIsMandatory;
import cdc.asd.checks.classes.ClassWhenUmlPrimitiveTagNameMustBeAllowed;
import cdc.asd.checks.connectors.AggregationPartCardinalityIsMandatory;
import cdc.asd.checks.connectors.AggregationPartTypeWhenSomeIsForbidden;
import cdc.asd.checks.connectors.AggregationTagNameMustBeAllowed;
import cdc.asd.checks.connectors.AggregationTagWhenXmlNameCountMustBe01;
import cdc.asd.checks.connectors.AggregationWholeCardinalityMustBeOne;
import cdc.asd.checks.connectors.AggregationWholeRoleIsForbidden;
import cdc.asd.checks.connectors.AssociationFromExtendTagWhenNoKeyCountMustBe1;
import cdc.asd.checks.connectors.AssociationTagNameMustBeAllowed;
import cdc.asd.checks.connectors.AssociationTagWhenXmlNameCountMustBe01;
import cdc.asd.checks.connectors.AssociationTargetRoleIsMandatory;
import cdc.asd.checks.connectors.AssociationTipRoleMustBeLowerCamelCase;
import cdc.asd.checks.connectors.AssociationToRelationshipSourceCardinalityMustBeOne;
import cdc.asd.checks.connectors.AssociationToRelationshipTargetCardinalityMustBeZeroToManyOrOneToMany;
import cdc.asd.checks.connectors.CompositionPartCardinalityIsMandatory;
import cdc.asd.checks.connectors.CompositionPartTypeIsForbidden;
import cdc.asd.checks.connectors.CompositionTagNameMustBeAllowed;
import cdc.asd.checks.connectors.CompositionTagWhenXmlNameCountMustBe01;
import cdc.asd.checks.connectors.CompositionWholeCardinalityMustBeOne;
import cdc.asd.checks.connectors.CompositionWholeRoleIsForbidden;
import cdc.asd.checks.connectors.ConnectorMustBeGraphicallyRepresented;
import cdc.asd.checks.diagrams.DiagramNameIsMandatory;
import cdc.asd.checks.diagrams.DiagramNameMustFollowAuthoring;
import cdc.asd.checks.diagrams.DiagramShouldContainAtMostOneShapePerElement;
import cdc.asd.checks.diagrams.DiagramStereotypesMustBeAllowed;
import cdc.asd.checks.interfaces.InterfaceAttributesAreForbidden;
import cdc.asd.checks.interfaces.InterfaceAuthorIsMandatory;
import cdc.asd.checks.interfaces.InterfaceInheritanceIsForbidden;
import cdc.asd.checks.interfaces.InterfaceMustBeGraphicallyRepresented;
import cdc.asd.checks.interfaces.InterfaceMustDirectlyExtendAtMostOnceAnInterface;
import cdc.asd.checks.interfaces.InterfaceMustNotDeclareUselessInheritance;
import cdc.asd.checks.interfaces.InterfaceNameIsMandatory;
import cdc.asd.checks.interfaces.InterfaceNameMustBeUpperCamelCase;
import cdc.asd.checks.interfaces.InterfaceNotesAreMandatory;
import cdc.asd.checks.interfaces.InterfaceNotesMustStartWithName;
import cdc.asd.checks.interfaces.InterfaceStereotypeIsMandatory;
import cdc.asd.checks.interfaces.InterfaceStereotypesMustBeAllowed;
import cdc.asd.checks.interfaces.InterfaceTagNameMustBeAllowed;
import cdc.asd.checks.interfaces.InterfaceTagWhenXmlRefNameCountMustBe0;
import cdc.asd.checks.interfaces.InterfaceVersionIsMandatory;
import cdc.asd.checks.interfaces.InterfaceVersionMustBeAllowed;
import cdc.asd.checks.interfaces.InterfaceWhenExtendMustNotBeAssociationTarget;
import cdc.asd.checks.interfaces.InterfaceWhenExtendTagWhenXmlNameCountMustBe1;
import cdc.asd.checks.interfaces.InterfaceWhenSelectMustNotBeAssociationSource;
import cdc.asd.checks.interfaces.InterfaceWhenSelectMustNotBeCompositionPart;
import cdc.asd.checks.interfaces.InterfaceWhenSelectTagWhenXmlNameCountMustBe1;
import cdc.asd.checks.misc.InheritanceMustBeGraphicallyRepresented;
import cdc.asd.checks.misc.SiblingsMustHaveDifferentNames;
import cdc.asd.checks.models.ModelAttributesWithSameNameMustHaveSameDefinitions;
import cdc.asd.checks.models.ModelAttributesWithSameNameMustHaveSameTypes;
import cdc.asd.checks.models.ModelAttributesWithSameNameMustHaveSameValidValues;
import cdc.asd.checks.models.ModelClassNamesMustBeUnique;
import cdc.asd.checks.models.ModelElementGuidMustBeUnique;
import cdc.asd.checks.models.ModelTypeXmlNamesMustBeUnique;
import cdc.asd.checks.models.ModelUidPatternsMustBeUnique;
import cdc.asd.checks.notes.NotesMustBeUnicode;
import cdc.asd.checks.notes.NotesMustNotContainHtml;
import cdc.asd.checks.packages.PackageNameIsMandatory;
import cdc.asd.checks.packages.PackageNotesAreMandatory;
import cdc.asd.checks.packages.PackageStereotypesMustBeAllowed;
import cdc.asd.checks.packages.PackageWhenSomeMustNotContainTypes;
import cdc.asd.checks.packages.PackageWhenSomeMustNotHaveAnyTag;
import cdc.asd.checks.packages.PackageWhenSomeNameMustBeCapitalCase;
import cdc.asd.checks.packages.PackageWhenSomeStereotypeIsMandatory;
import cdc.asd.checks.packages.PackageWhenSomeStereotypesAreForbidden;
import cdc.asd.checks.packages.PackageWhenUofNotesMustStartWithName;
import cdc.asd.checks.packages.PackageWhenUofTagNameMustBeAllowed;
import cdc.asd.checks.stereotypes.StereotypeMustBeRecognized;
import cdc.asd.checks.tags.TagNameIsMandatory;
import cdc.asd.checks.tags.TagNameMustBeRecognized;
import cdc.asd.checks.tags.TagValueMustBeUnicode;
import cdc.asd.checks.tags.TagWhenLongDescriptionNotesMustHaveValidMarkup;
import cdc.asd.checks.tags.TagWhenNoteValueMustBeUnique;
import cdc.asd.checks.tags.TagWhenRefValueShouldContainOneConcept;
import cdc.asd.checks.tags.TagWhenReplacesValueMustExistInRefModel;
import cdc.asd.checks.tags.TagWhenSomeValueMustContainOneToken;
import cdc.asd.checks.tags.TagWhenUidPatternValueMustBeLowerCase;
import cdc.asd.checks.tags.TagWhenUidPatternValueShouldBeAtMost8Chars;
import cdc.asd.checks.tags.TagWhenXmlNameValueMustBeLowerCamelCase;
import cdc.asd.checks.tags.TagWhenXmlRefNameValueMustFollowAuthoring;
import cdc.asd.model.Config;
import cdc.issues.Metas;
import cdc.issues.impl.ProfileImpl;

/**
 * Description of checked rules.
 *
 * @author Damien Carbonne
 */
public final class AsdProfile {
    private AsdProfile() {
    }

    public static final String DOMAIN = "S-Series";

    public static final ProfileImpl PROFILE =
            ProfileImpl.builder()
                       .name("ASD Profile")
                       .metas(Metas.builder()
                                   .meta("Version", Config.VERSION)
                                   .build())
                       .description("Rules used to check an ASD model.")
                       .rule(AttributeCardinalityIsMandatory.RULE)
                       .rule(AttributeNameIsMandatory.RULE)
                       .rule(AttributeNameMustBeLowerCamelCase.RULE)
                       .rule(AttributeNameShouldNotContainCode.RULE)
                       .rule(AttributeNameShouldStartWithClassName.RULE)
                       .rule(AttributeNotesAreMandatory.RULE)
                       .rule(AttributeNotesMustStartWithName.RULE)
                       .rule(AttributeStereotypeIsMandatory.RULE)
                       .rule(AttributeStereotypesMustBeAllowed.RULE)
                       .rule(AttributeTagNameMustBeAllowed.RULE)
                       .rule(AttributeTagWhenUnitMustFollowAuthoring.RULE)
                       .rule(AttributeTagWhenValidValueNotesAreMandatory.RULE)
                       .rule(AttributeTagWhenValidValueNotesMustFollowAuthoring.RULE)
                       .rule(AttributeTagWhenValidValueValueMustBeUnique.RULE)
                       .rule(AttributeTagWhenXmlNameCountMustBe1.RULE)
                       .rule(AttributeTypeMustBeAllowed.RULE)
                       .rule(AttributeTypeIsMandatory.RULE)
                       .rule(AttributeVisibilityMustBePublic.RULE)
                       .rule(AttributeWhenIdentifierTagWhenValidValueValueMustNotBeEmpty.RULE)
                       .rule(AttributeWhenSomeCardinalityLowerBoundMustBeOne.RULE)
                       .rule(AttributeWhenSomeCardinalityUpperBoundMustBeUnbounded.RULE)
                       .rule(AttributeWhenSomeStereotypeMustNotBeAnyKey.RULE)
                       .rule(AttributeWhenSomeTagMustHaveValidValueOrValidValueLibrary.RULE)
                       .rule(AttributeWhenSomeTagWhenUnitCountMustBe0.RULE)
                       .rule(AttributeWhenSomeTagWhenUnitCountMustBe1.RULE)
                       .rule(AttributeWhenSomeTagWhenValidValueCountMustBe0.RULE)
                       .rule(AttributeWhenSomeTagWhenValidValueLibraryCountMustBe0.RULE)
                       .rule(AttributeWhenSomeTagWhenValidValueLibraryCountMustBe01.RULE)
                       .rule(AttributeWhenSomeTagWhenValidValueValueMustNotBeEmpty.RULE)

                       .rule(ClassAttributeNamesMustBeUnique.RULE)
                       .rule(ClassAttributeTagsWhenXmlNameMustBeUnique.RULE)
                       .rule(ClassMultipleInheritanceIsForbidden.RULE)
                       .rule(ClassMustDirectlyExtendAtMostOnceAClass.RULE)
                       .rule(ClassMustDirectlyImplementAtMostOnceAnInterface.RULE)
                       .rule(ClassMustNotBeCompositionPartOfExchangeAndNonExchangeClasses.RULE)
                       .rule(ClassMustNotDeclareUselessInheritance.RULE)
                       .rule(ClassNameIsMandatory.RULE)
                       .rule(ClassNotesMustStartWithName.RULE)
                       .rule(ClassSpecializationMustNotDeclareAnyKeyAttribute.RULE)
                       .rule(ClassStereotypeIsMandatory.RULE)
                       .rule(ClassStereotypesMustBeAllowed.RULE)
                       .rule(ClassTagWhenXmlRefNameMustBeLowerCamelCase.RULE)
                       .rule(ClassVersionMustBeAllowed.RULE)
                       .rule(ClassVisibilityMustBePublic.RULE)
                       .rule(ClassWhenAttributeGroupMustBeCompositionPart.RULE)
                       .rule(ClassWhenAttributeGroupMustNotBeAbstract.RULE)
                       .rule(ClassWhenAttributeGroupTagNameMustBeAllowed.RULE)
                       .rule(ClassWhenClassTagNameMustBeAllowed.RULE)
                       .rule(ClassWhenCompositionPartAttributeWhenKeyStereotypeMustBeCompositeKey.RULE)
                       .rule(ClassWhenCompoundAttributeMustHaveAtLeast1Attribute.RULE)
                       .rule(ClassWhenCompoundAttributeTagNameMustBeAllowed.RULE)
                       .rule(ClassWhenExchangeAttributesAreForbidden.RULE)
                       .rule(ClassWhenExchangeMustNotOwnAnyAggregation.RULE)
                       .rule(ClassWhenExchangeMustNotOwnAnyAssociation.RULE)
                       .rule(ClassWhenExchangeMustNotOwnAnyImplementation.RULE)
                       .rule(ClassWhenExchangeTagNameMustBeAllowed.RULE)
                       .rule(ClassWhenNonAbstractRelationshipMustHaveTwoKeyAssociations.RULE)
                       .rule(ClassWhenNotCompositionPartAttributeWhenCompositeKeyIsForbbiden.RULE)
                       .rule(ClassWhenNotRelationshipAttributeWhenRelationshipKeyIsForbbiden.RULE)
                       .rule(ClassWhenPrimitiveTagNameMustBeAllowed.RULE)
                       .rule(ClassWhenRelationshipAttributeWhenKeyStereotypeMustBeRelationshipKey.RULE)
                       .rule(ClassWhenRelationshipTagNameMustBeAllowed.RULE)
                       .rule(ClassWhenRevisionMustHaveRevisionAttributes.RULE)
                       .rule(ClassWhenSomeAuthorIsMandatory.RULE)
                       .rule(ClassWhenSomeAuthorMustBeAllowed.RULE)
                       .rule(ClassWhenSomeMustBeGraphicallyRepresented.RULE)
                       .rule(ClassWhenSomeMustNotBeAggregationPart.RULE)
                       .rule(ClassWhenSomeMustNotBeAssociationTarget.RULE)
                       .rule(ClassWhenSomeMustNotBePartOfSeveralCompositions.RULE)
                       .rule(ClassWhenSomeMustNotDirectlyBeAggregationWhole.RULE)
                       .rule(ClassWhenSomeMustNotDirectlyBeAssociationSource.RULE)
                       .rule(ClassWhenSomeMustNotDirectlyBeCompositionWhole.RULE)
                       .rule(ClassWhenSomeMustNotImplementExtendInterface.RULE)
                       .rule(ClassWhenSomeNameMustBeUpperCamelCase.RULE)
                       .rule(ClassWhenSomeNotesAreMandatory.RULE)
                       .rule(ClassWhenSomeTagWhenUidPatternCountMustBe0.RULE)
                       .rule(ClassWhenSomeTagWhenUidPatternCountMustBe1.RULE)
                       .rule(ClassWhenSomeTagWhenXmlNameCountMustBe0.RULE)
                       .rule(ClassWhenSomeTagWhenXmlNameCountMustBe1.RULE)
                       .rule(ClassWhenSomeTagWhenXmlRefNameCountMustBe0.RULE)
                       .rule(ClassWhenSomeTagWhenXmlRefNameCountMustBe01.RULE)
                       .rule(ClassWhenSomeTagWhenXmlSchemaNameCountMustBe0.RULE)
                       .rule(ClassWhenSomeTagWhenXmlSchemaNameCountMustBe1.RULE)
                       .rule(ClassWhenSomeVersionIsMandatory.RULE)
                       .rule(ClassWhenUmlPrimitiveTagNameMustBeAllowed.RULE)

                       .rule(DiagramNameIsMandatory.RULE)
                       .rule(DiagramNameMustFollowAuthoring.RULE)
                       .rule(DiagramShouldContainAtMostOneShapePerElement.RULE)
                       .rule(DiagramStereotypesMustBeAllowed.RULE)

                       .rule(InterfaceAttributesAreForbidden.RULE)
                       .rule(InterfaceAuthorIsMandatory.RULE)
                       .rule(InterfaceInheritanceIsForbidden.RULE)
                       .rule(InterfaceMustBeGraphicallyRepresented.RULE)
                       .rule(InterfaceMustDirectlyExtendAtMostOnceAnInterface.RULE)
                       .rule(InterfaceMustNotDeclareUselessInheritance.RULE)
                       .rule(InterfaceNameIsMandatory.RULE)
                       .rule(InterfaceNameMustBeUpperCamelCase.RULE)
                       .rule(InterfaceNotesAreMandatory.RULE)
                       .rule(InterfaceNotesMustStartWithName.RULE)
                       .rule(InterfaceStereotypeIsMandatory.RULE)
                       .rule(InterfaceStereotypesMustBeAllowed.RULE)
                       .rule(InterfaceTagNameMustBeAllowed.RULE)
                       .rule(InterfaceTagWhenXmlRefNameCountMustBe0.RULE)
                       .rule(InterfaceVersionIsMandatory.RULE)
                       .rule(InterfaceVersionMustBeAllowed.RULE)
                       .rule(InterfaceWhenExtendMustNotBeAssociationTarget.RULE)
                       .rule(InterfaceWhenExtendTagWhenXmlNameCountMustBe1.RULE)
                       .rule(InterfaceWhenSelectMustNotBeAssociationSource.RULE)
                       .rule(InterfaceWhenSelectMustNotBeCompositionPart.RULE)
                       .rule(InterfaceWhenSelectTagWhenXmlNameCountMustBe1.RULE)

                       .rule(PackageNameIsMandatory.RULE)
                       .rule(PackageNotesAreMandatory.RULE)
                       .rule(PackageStereotypesMustBeAllowed.RULE)
                       .rule(PackageWhenUofNotesMustStartWithName.RULE)
                       .rule(PackageWhenUofTagNameMustBeAllowed.RULE)
                       .rule(PackageWhenSomeMustNotContainTypes.RULE)
                       .rule(PackageWhenSomeMustNotHaveAnyTag.RULE)
                       .rule(PackageWhenSomeNameMustBeCapitalCase.RULE)
                       .rule(PackageWhenSomeStereotypesAreForbidden.RULE)
                       .rule(PackageWhenSomeStereotypeIsMandatory.RULE)

                       .rule(AggregationPartCardinalityIsMandatory.RULE)
                       .rule(AggregationPartTypeWhenSomeIsForbidden.RULE)
                       .rule(AggregationWholeCardinalityMustBeOne.RULE)
                       .rule(AggregationTagNameMustBeAllowed.RULE)
                       .rule(AggregationTagWhenXmlNameCountMustBe01.RULE)
                       .rule(AggregationWholeRoleIsForbidden.RULE)
                       // .rule(ConnectorWhenAssociationMustBeForward.RULE)
                       .rule(AssociationTipRoleMustBeLowerCamelCase.RULE)
                       .rule(AssociationToRelationshipSourceCardinalityMustBeOne.RULE)
                       .rule(AssociationTagNameMustBeAllowed.RULE)
                       .rule(AssociationFromExtendTagWhenNoKeyCountMustBe1.RULE)
                       .rule(AssociationTagWhenXmlNameCountMustBe01.RULE)
                       .rule(AssociationToRelationshipTargetCardinalityMustBeZeroToManyOrOneToMany.RULE)
                       .rule(AssociationTargetRoleIsMandatory.RULE)
                       .rule(CompositionPartCardinalityIsMandatory.RULE)
                       .rule(CompositionPartTypeIsForbidden.RULE)
                       .rule(CompositionWholeCardinalityMustBeOne.RULE)
                       .rule(CompositionTagNameMustBeAllowed.RULE)
                       .rule(CompositionTagWhenXmlNameCountMustBe01.RULE)
                       .rule(CompositionWholeRoleIsForbidden.RULE)
                       .rule(ConnectorMustBeGraphicallyRepresented.RULE)

                       .rule(CardinalityMustBeValid.RULE)
                       .rule(CardinalityMustBeWellFormed.RULE)
                       .rule(NotesMustBeUnicode.RULE)
                       .rule(NotesMustNotContainHtml.RULE)
                       .rule(StereotypeMustBeRecognized.RULE)
                       .rule(TagNameIsMandatory.RULE)
                       .rule(TagNameMustBeRecognized.RULE)
                       .rule(TagValueMustBeUnicode.RULE)
                       .rule(TagWhenLongDescriptionNotesMustHaveValidMarkup.RULE)
                       .rule(TagWhenNoteValueMustBeUnique.RULE)
                       .rule(TagWhenRefValueShouldContainOneConcept.RULE)
                       .rule(TagWhenReplacesValueMustExistInRefModel.RULE)
                       .rule(TagWhenSomeValueMustContainOneToken.RULE)
                       .rule(TagWhenUidPatternValueMustBeLowerCase.RULE)
                       .rule(TagWhenUidPatternValueShouldBeAtMost8Chars.RULE)
                       .enabled(TagWhenUidPatternValueShouldBeAtMost8Chars.RULE, false)
                       .rule(TagWhenXmlNameValueMustBeLowerCamelCase.RULE)
                       .rule(TagWhenXmlRefNameValueMustFollowAuthoring.RULE)

                       .rule(ModelAttributesWithSameNameMustHaveSameDefinitions.RULE)
                       .rule(ModelAttributesWithSameNameMustHaveSameTypes.RULE)
                       .rule(ModelAttributesWithSameNameMustHaveSameValidValues.RULE)
                       .rule(ModelClassNamesMustBeUnique.RULE)
                       .rule(ModelElementGuidMustBeUnique.RULE)
                       .rule(ModelTypeXmlNamesMustBeUnique.RULE)
                       .rule(ModelUidPatternsMustBeUnique.RULE)

                       .rule(InheritanceMustBeGraphicallyRepresented.RULE)
                       .rule(SiblingsMustHaveDifferentNames.RULE)
                       .build();
}