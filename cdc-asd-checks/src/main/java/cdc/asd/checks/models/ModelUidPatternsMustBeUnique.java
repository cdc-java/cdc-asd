package cdc.asd.checks.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfQNameItem;
import cdc.mf.model.MfTag;

public class ModelUidPatternsMustBeUnique extends MfAbstractRuleChecker<MfModel> {
    public static final String NAME = "M07";
    public static final String TITLE = "MODEL_TAGS(UID_PATTERN)_MUST_BE_UNIQUE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} cannot have duplicate {%wrap} {%wrap}.",
                                                                N_MODEL,
                                                                AsdNames.T_UID_PATTERN + WS + N_TAG,
                                                                N_VALUE + S)
                                                        .relatedTo(AsdRule.UID_PATTERN_UNIQUE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected ModelUidPatternsMustBeUnique(SnapshotManager manager) {
        super(manager,
              MfModel.class,
              RULE);
    }

    @Override
    protected String getHeader(MfModel object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfModel object,
                             Location location) {
        final List<MfQNameItem> objects = object.collect(MfQNameItem.class)
                                                .stream()
                                                .sorted(MfQNameItem.QNAME_COMPARATOR)
                                                .toList();

        final Map<String, Set<MfQNameItem>> map = new HashMap<>();
        boolean violation = false;

        for (final MfQNameItem o : objects) {
            if (o.wrap(AsdElement.class).isLocal()) {
                final List<MfTag> tags = o.getTags(AsdTagName.UID_PATTERN);
                for (final MfTag tag : tags) {
                    final String value = tag.getValue();
                    final Set<MfQNameItem> set = map.computeIfAbsent(value, k -> new HashSet<>());
                    set.add(o);
                    if (set.size() > 1) {
                        violation = true;
                    }
                }
            }
        }

        if (violation) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate uidPattern tags");
            for (final String name : map.keySet().stream().sorted().toList()) {
                final Set<MfQNameItem> set = map.get(name);
                if (set.size() > 1) {
                    description.uItems(name)
                               .elements(1, set);
                }
            }

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}