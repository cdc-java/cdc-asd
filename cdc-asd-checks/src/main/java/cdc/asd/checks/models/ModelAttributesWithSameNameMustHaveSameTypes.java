package cdc.asd.checks.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfQNameItem;
import cdc.mf.model.MfType;

public class ModelAttributesWithSameNameMustHaveSameTypes extends MfAbstractRuleChecker<MfModel> {
    public static final String NAME = "M02";
    public static final String TITLE = "MODEL_ATTRIBUTES_WITH_SAME_NAME_MUST_HAVE_SAME_TYPES";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("In a {%wrap}, 2 {%wrap} with the same {%wrap} must have the same {%wrap}.",
                                                                N_MODEL,
                                                                N_ATTRIBUTE + S,
                                                                N_NAME,
                                                                N_TYPE + S)
                                                        .relatedTo(AsdRule.ATTRIBUTE_NAME_CONSISTENT),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.15.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected ModelAttributesWithSameNameMustHaveSameTypes(SnapshotManager manager) {
        super(manager,
              MfModel.class,
              RULE);
    }

    @Override
    protected String getHeader(MfModel object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfModel object,
                             Location location) {
        final List<MfProperty> attributes = object.collect(MfProperty.class)
                                                  .stream()
                                                  .sorted(MfQNameItem.NAME_PARENT_PATH_COMPARATOR)
                                                  .toList();

        // Attribute name to set of types
        final Map<String, Set<MfType>> map = new HashMap<>();
        boolean violation = false;

        for (final MfProperty attribute : attributes) {
            final String name = attribute.getName();
            if (attribute.getTypeRef().isValid()) {
                final Set<MfType> set = map.computeIfAbsent(name, k -> new HashSet<>());
                set.add(attribute.getType());
                if (set.size() > 1) {
                    violation = true;
                }
            }
        }

        if (violation) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has attributes with same name and different types.");
            for (final String name : map.keySet().stream().sorted().toList()) {
                final Set<MfType> set = map.get(name);
                if (set.size() > 1) {
                    description.uItems(name);
                    final List<MfProperty> list = object.collect(MfProperty.class, x -> x.getName().equals(name));

                    for (final MfType type : set) {
                        description.element(1, type);
                        description.elements(2, list.stream().filter(x -> type == x.getType()).toList());
                    }
                }
            }

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}