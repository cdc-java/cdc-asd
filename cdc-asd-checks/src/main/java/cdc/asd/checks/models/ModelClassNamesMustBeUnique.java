package cdc.asd.checks.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.model.wrappers.AsdElement;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfQNameItem;

public class ModelClassNamesMustBeUnique extends MfAbstractRuleChecker<MfModel> {
    public static final String NAME = "M04";
    public static final String TITLE = "MODEL_CLASS_NAMES_MUST_BE_UNIQUE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("A {%wrap} cannot have duplicate {%wrap} {%wrap}.",
                                                                N_MODEL,
                                                                N_CLASS,
                                                                N_NAME + S)
                                                        .relatedTo(AsdRule.CLASS_NAME_UNIQUE),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected ModelClassNamesMustBeUnique(SnapshotManager manager) {
        super(manager,
              MfModel.class,
              RULE);
    }

    @Override
    protected String getHeader(MfModel object) {
        return getTheItemHeader(object);
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfModel object,
                             Location location) {
        final List<MfClass> classes = object.getAllClasses()
                                            .stream()
                                            .sorted(MfQNameItem.QNAME_COMPARATOR)
                                            .toList();

        final Map<String, Set<MfClass>> counts = new HashMap<>();
        boolean violation = false;

        for (final MfClass cls : classes) {
            if (cls.wrap(AsdElement.class).isLocal()) {
                final String name = cls.getName();
                final Set<MfClass> set = counts.computeIfAbsent(name, k -> new HashSet<>());
                set.add(cls);
                if (set.size() > 1) {
                    violation = true;
                }
            }
        }

        if (violation) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("has duplicate class names");
            for (final Map.Entry<String, Set<MfClass>> entry : counts.entrySet()) {
                final Set<MfClass> set = entry.getValue();
                if (set.size() > 1) {
                    description.uItems(entry.getKey())
                               .elements(1, set);
                }
            }

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}