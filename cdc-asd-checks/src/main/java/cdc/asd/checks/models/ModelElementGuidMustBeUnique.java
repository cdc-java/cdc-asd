package cdc.asd.checks.models;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.models.AbstractElementGuidMustBeUnique;

public class ModelElementGuidMustBeUnique extends AbstractElementGuidMustBeUnique {
    public static final String NAME = "M05";
    public static final String TITLE = "MODEL_ELEMENT_GUID_MUST_BE_UNIQUE";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> describe(),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.22.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    protected ModelElementGuidMustBeUnique(SnapshotManager manager) {
        super(manager,
              RULE);
    }
}