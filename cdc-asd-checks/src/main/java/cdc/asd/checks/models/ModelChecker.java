package cdc.asd.checks.models;

import cdc.asd.checks.AsdProfile;
import cdc.asd.checks.AsdSnapshotManager;
import cdc.asd.checks.classes.ClassesChecker;
import cdc.asd.checks.interfaces.InterfacesChecker;
import cdc.asd.checks.misc.SiblingsMustHaveDifferentNames;
import cdc.asd.checks.packages.PackagesChecker;
import cdc.asd.model.AsdEaNaming;
import cdc.issues.Issue;
import cdc.issues.IssuesCollector;
import cdc.issues.Metas;
import cdc.issues.checks.RootChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfModel;

public class ModelChecker extends RootChecker<MfModel> {
    public ModelChecker(String projectName,
                        Metas projectMetas,
                        String snasphotName,
                        Metas snapshotMetas,
                        MfModel model,
                        AsdEaNaming naming,
                        MfModel refModel,
                        IssuesCollector<Issue> issuesCollector) {
        super(AsdSnapshotManager.builder()
                                .projectName(projectName)
                                .projectMetas(projectMetas)
                                .snapshotName(snasphotName)
                                .snapshotMetas(snapshotMetas)
                                .profile(AsdProfile.PROFILE)
                                .issuesCollector(issuesCollector)
                                .stats(true)
                                .naming(naming)
                                .refModel(refModel)
                                .build(),
              MfModel.class,
              LocatedObject.of(model));

        add(new ModelAttributesWithSameNameMustHaveSameDefinitions(getManager()));
        add(new ModelAttributesWithSameNameMustHaveSameTypes(getManager()));
        add(new ModelAttributesWithSameNameMustHaveSameValidValues(getManager()));
        add(new ModelClassNamesMustBeUnique(getManager()));
        add(new ModelElementGuidMustBeUnique(getManager()));
        add(new ModelUidPatternsMustBeUnique(getManager()));
        add(new ModelTypeXmlNamesMustBeUnique(getManager()));
        add(new SiblingsMustHaveDifferentNames(getManager()));
        add(new PackagesChecker(getManager()));
        add(new ClassesChecker(getManager()));
        add(new InterfacesChecker(getManager()));
    }

    public static SnapshotManager check(String projectName,
                                        Metas projectMetas,
                                        String snasphotName,
                                        Metas snapshotMetas,
                                        MfModel model,
                                        AsdEaNaming naming,
                                        MfModel refModel,
                                        IssuesCollector<Issue> issuesCollector) {
        final ModelChecker checker =
                new ModelChecker(projectName,
                                 projectMetas,
                                 snasphotName,
                                 snapshotMetas,
                                 model,
                                 naming,
                                 refModel,
                                 issuesCollector);
        checker.check();
        return checker.getManager();
    }
}
