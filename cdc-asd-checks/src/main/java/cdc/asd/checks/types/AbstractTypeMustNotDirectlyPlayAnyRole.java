package cdc.asd.checks.types;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import cdc.asd.model.AsdModelUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfType;

public abstract class AbstractTypeMustNotDirectlyPlayAnyRole<O extends MfType> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    private final Class<? extends MfConnector> connectorClass;

    protected AbstractTypeMustNotDirectlyPlayAnyRole(SnapshotManager manager,
                                                     Class<O> objectClass,
                                                     Rule rule,
                                                     Class<? extends MfConnector> connectorClass) {
        super(manager,
              objectClass,
              rule);
        this.connectorClass = connectorClass;
    }

    @Override
    protected final String getHeader(O object) {
        return getTheItemHeader(object);
    }

    @Override
    public final CheckResult check(CheckContext context,
                                   O object,
                                   Location location) {
        final Set<MfConnector> set = Stream.concat(object.getConnectors().stream(),
                                                   object.getReversedConnectors().stream())
                                           .filter(x -> x.getClass().equals(connectorClass))
                                           .collect(Collectors.toSet());
        if (!set.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("directly plays a(n) " + AsdModelUtils.identify(connectorClass) + " role, it should not")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}