package cdc.asd.checks.types;

import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import cdc.asd.model.AsdModelUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTipRole;
import cdc.mf.model.MfTipSide;
import cdc.mf.model.MfType;
import cdc.util.lang.Checks;

public abstract class AbstractTypeMustNotPlayRole<O extends MfType> extends MfAbstractRuleChecker<O> {
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;

    private final Class<? extends MfConnector> connectorClass;
    private final MfTipRole role;

    protected AbstractTypeMustNotPlayRole(SnapshotManager manager,
                                          Class<O> objectClass,
                                          Rule rule,
                                          Class<? extends MfConnector> connectorClass,
                                          MfTipRole role) {
        super(manager,
              objectClass,
              rule);
        this.connectorClass = connectorClass;
        this.role = role;
        Checks.assertTrue(role.isCompliantWith(connectorClass), "{} and {} are not compliant.", role, connectorClass);
    }

    @Override
    protected final String getHeader(O object) {
        return getTheItemHeader(object);
    }

    @Override
    public final CheckResult check(CheckContext context,
                                   O object,
                                   Location location) {
        final Predicate<MfConnector> predicate =
                x -> x.getClass().equals(connectorClass)
                        && x.getTip(role).getType() == object;
        final Set<MfConnector> set;
        if (role.getSide() == MfTipSide.SOURCE) {
            set = object.getAllConnectors()
                        .stream()
                        .filter(predicate)
                        .collect(Collectors.toSet());
        } else {
            set = object.getAllReversedConnectors()
                        .stream()
                        .filter(predicate)
                        .collect(Collectors.toSet());
        }
        if (!set.isEmpty()) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation("is " + AsdModelUtils.identify(role)
                               + " of " + AsdModelUtils.identify(connectorClass) + "(s), it should not")
                       .elements(sortUsingId(set));

            add(issue().description(description)
                       .location(object)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}