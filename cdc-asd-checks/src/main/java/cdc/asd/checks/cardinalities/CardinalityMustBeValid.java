package cdc.asd.checks.cardinalities;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityMustBeValid;

public class CardinalityMustBeValid extends AbstractCardinalityMustBeValid {
    public static final String NAME = "G01";
    public static final String TITLE = "CARDINALITY_MUST_BE_VALID";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All well formed {%wrap} must be valid.",
                                                                N_CARDINALITIES)
                                                        .appliesTo("The cardinality of all attributes",
                                                                   "The cardinality of all connector tips")
                                                        .relatedTo(AsdRule.ATTRIBUTE_CARDINALITY,
                                                                   AsdRule.COMPOSITION_SOURCE_CARDINALITY,
                                                                   AsdRule.AGGREGATION_SOURCE_CARDINALITY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public CardinalityMustBeValid(SnapshotManager manager) {
        super(manager,
              RULE);
    }
}