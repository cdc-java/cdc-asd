package cdc.asd.checks.cardinalities;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.cardinality.AbstractCardinalityMustBeWellFormed;

public class CardinalityMustBeWellFormed extends AbstractCardinalityMustBeWellFormed {
    public static final String NAME = "G02";
    public static final String TITLE = "CARDINALITY_MUST_BE_WELL_FORMED";
    public static final IssueSeverity SEVERITY = IssueSeverity.CRITICAL;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("All defined {%wrap} must be well formed.",
                                                                N_CARDINALITIES)
                                                        .appliesTo("The cardinality of all attributes",
                                                                   "The cardinality of all connector tips")
                                                        .relatedTo(AsdRule.ATTRIBUTE_CARDINALITY,
                                                                   AsdRule.COMPOSITION_SOURCE_CARDINALITY,
                                                                   AsdRule.AGGREGATION_SOURCE_CARDINALITY),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.1.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING,
                                AsdLabels.XSD_GENERATION)
                        .build();

    public CardinalityMustBeWellFormed(SnapshotManager manager) {
        super(manager,
              RULE);
    }
}