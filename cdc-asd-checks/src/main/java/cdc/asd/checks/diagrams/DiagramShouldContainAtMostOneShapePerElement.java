package cdc.asd.checks.diagrams;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.nodes.misc.AbstractDiagramShouldContainAtMostOneShapePerElement;

public class DiagramShouldContainAtMostOneShapePerElement extends AbstractDiagramShouldContainAtMostOneShapePerElement {
    public static final String NAME = "D04";
    public static final String TITLE = "DIAGRAM_SHOULD_CONTAIN_AT_MOST_ONE_SHAPE_PER_ELEMENT";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe())
                                                        .appliesTo("All diagrams"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.24.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public DiagramShouldContainAtMostOneShapePerElement(SnapshotManager manager) {
        super(manager,
              RULE);
    }
}