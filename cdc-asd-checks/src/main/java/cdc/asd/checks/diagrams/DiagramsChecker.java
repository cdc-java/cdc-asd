package cdc.asd.checks.diagrams;

import java.util.List;

import cdc.issues.checks.AbstractPartsChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.LocatedObject;
import cdc.mf.model.MfDiagram;
import cdc.mf.model.MfDiagramOwner;
import cdc.mf.model.MfElement;

public class DiagramsChecker extends AbstractPartsChecker<MfDiagramOwner, MfDiagram, DiagramsChecker> {
    public DiagramsChecker(SnapshotManager manager) {
        super(manager,
              MfDiagramOwner.class,
              MfDiagram.class,
              new DiagramChecker(manager));
    }

    @Override
    protected List<LocatedObject<? extends MfDiagram>> getParts(MfDiagramOwner object) {
        final List<MfDiagram> delegates = object.getDiagrams()
                                                .stream()
                                                .sorted(MfElement.ID_COMPARATOR)
                                                .toList();
        return LocatedObject.locate(delegates);
    }
}