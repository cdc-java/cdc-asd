package cdc.asd.checks.diagrams;

import cdc.asd.checks.stereotypes.StereotypeMustBeRecognized;
import cdc.issues.checks.CompositeChecker;
import cdc.issues.checks.SnapshotManager;
import cdc.mf.model.MfDiagram;

public class DiagramChecker extends CompositeChecker<MfDiagram> {
    public DiagramChecker(SnapshotManager manager) {
        super(manager,
              MfDiagram.class,
              new DiagramNameIsMandatory(manager),
              new DiagramNameMustFollowAuthoring(manager),
              new DiagramShouldContainAtMostOneShapePerElement(manager),
              new DiagramStereotypesMustBeAllowed(manager),
              new StereotypeMustBeRecognized(manager));
    }
}