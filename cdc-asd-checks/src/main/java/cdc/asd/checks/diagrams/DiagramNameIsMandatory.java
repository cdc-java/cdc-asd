package cdc.asd.checks.diagrams;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.checks.atts.name.AbstractNameIsMandatory;
import cdc.mf.model.MfDiagram;

public class DiagramNameIsMandatory extends AbstractNameIsMandatory<MfDiagram> {
    public static final String NAME = "D01";
    public static final String TITLE = "DIAGRAM_NAME_IS_MANDATORY";
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_DIAGRAM + S))
                                                        .appliesTo("All diagrams")
                                                        .relatedTo(AsdRule.DIAGRAM_NAME),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.23.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public DiagramNameIsMandatory(SnapshotManager manager) {
        super(manager,
              MfDiagram.class,
              RULE);
    }
}