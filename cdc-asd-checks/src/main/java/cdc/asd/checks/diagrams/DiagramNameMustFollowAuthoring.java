package cdc.asd.checks.diagrams;

import java.util.regex.Pattern;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdRule;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.AsdSnapshotManager;
import cdc.asd.model.AsdEaNaming;
import cdc.issues.IssueSeverity;
import cdc.issues.Metas;
import cdc.issues.checks.CheckContext;
import cdc.issues.checks.CheckResult;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.locations.Location;
import cdc.issues.rules.Rule;
import cdc.mf.checks.IssueDescription;
import cdc.mf.checks.MfAbstractRuleChecker;
import cdc.mf.model.MfDiagram;
import cdc.util.strings.StringUtils;

public class DiagramNameMustFollowAuthoring extends MfAbstractRuleChecker<MfDiagram> {
    public static final String NAME = "D02";
    public static final String TITLE = "DIAGRAM_NAME_MUST_FOLLOW_AUTHORING";
    public static final IssueSeverity SEVERITY = IssueSeverity.MAJOR;
    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.define("The {%wrap} of a {%wrap} should start with CDM or the specification name.\n"
                                      + "It must be written in capitalization style and use space to separate words.",
                                                                N_NAME,
                                                                N_DIAGRAM)
                                                        .appliesTo("All diagrams")
                                                        .relatedTo(AsdRule.DIAGRAM_NAME),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.23.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    private static final Pattern TAIL_PATTERN = Pattern.compile("^( [A-Za-z0-9()]+)*$");

    public DiagramNameMustFollowAuthoring(SnapshotManager manager) {
        super(manager,
              MfDiagram.class,
              RULE);
    }

    @Override
    protected String getHeader(MfDiagram object) {
        return getTheNameOfHeader(object);
    }

    private boolean isCompliant(String name,
                                StringBuilder violations) {
        final AsdSnapshotManager manager = getManager(AsdSnapshotManager.class);
        final AsdEaNaming naming = manager.getNaming();

        boolean compliant = true;
        final String specName = naming.getSpecName();

        if (name.startsWith("CDM") || name.startsWith(specName)) {
            final String tail = name.replaceFirst("CDM", "")
                                    .replaceFirst(specName, "");
            if (!TAIL_PATTERN.matcher(tail).matches()) {
                compliant = false;
                violations.append("does match '" + TAIL_PATTERN.pattern() + "'");
            }
        } else {
            compliant = false;
            violations.append("does not start with CDM or '" + specName + "'");
        }

        return compliant;
    }

    @Override
    public CheckResult check(CheckContext context,
                             MfDiagram object,
                             Location location) {
        final String name = object.getName();
        final StringBuilder violations = new StringBuilder();

        if (!StringUtils.isNullOrEmpty(name) && !isCompliant(name, violations)) {
            final IssueDescription.Builder description = IssueDescription.builder();

            description.header(getHeader(object))
                       .violation(violations.toString());

            add(issue().description(description)
                       .location(location)
                       .build());
            return CheckResult.FAILURE;
        } else {
            return CheckResult.SUCCESS;
        }
    }
}