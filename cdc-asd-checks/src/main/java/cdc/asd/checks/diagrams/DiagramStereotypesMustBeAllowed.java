package cdc.asd.checks.diagrams;

import java.util.EnumSet;
import java.util.Set;

import cdc.asd.checks.AsdLabels;
import cdc.asd.checks.AsdNames;
import cdc.asd.checks.AsdRuleUtils;
import cdc.asd.checks.stereotypes.AbstractStereotypesMustBeAllowed;
import cdc.asd.model.AsdStereotypeName;
import cdc.issues.Metas;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.rules.Rule;
import cdc.mf.model.MfDiagram;

public class DiagramStereotypesMustBeAllowed extends AbstractStereotypesMustBeAllowed<MfDiagram> {
    public static final String NAME = "D03";
    public static final String TITLE = "DIAGRAM_STEREOTYPES_MUST_BE_ALLOWED";

    private static final Set<AsdStereotypeName> ALLOWED_STEREOTYPES =
            EnumSet.of(AsdStereotypeName.NO_PRINT);

    public static final Rule RULE =
            AsdRuleUtils.rule(NAME,
                              TITLE,
                              description -> description.text(describe(N_DIAGRAM))
                                                        .text(oneOf(ALLOWED_STEREOTYPES))
                                                        .appliesTo("All diagrams")
                                                        .sources(AsdNames.SOURCE_UMLWRSG_2_0 + " 6.3.1"),
                              SEVERITY)
                        .meta(Metas.SINCE, "0.24.0")
                        .labels(AsdLabels.UWRSG_SOURCE_MISSING)
                        .build();

    public DiagramStereotypesMustBeAllowed(SnapshotManager manager) {
        super(manager,
              MfDiagram.class,
              RULE);
    }

    @Override
    protected boolean isAllowed(MfDiagram object,
                                AsdStereotypeName stereotypeName) {
        return ALLOWED_STEREOTYPES.contains(stereotypeName);
    }
}