package cdc.asd.checks;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.issues.core.io.MdProfileIo;
import cdc.issues.io.ProfileIo;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.rules.Rule;
import cdc.issues.rules.RuleDescription;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.util.lang.CollectionUtils;

public final class AsdProfileUtils {
    private static final Logger LOGGER = LogManager.getLogger(AsdProfileUtils.class);

    private static final Pattern EM_PATTERN = Pattern.compile("(<<[a-zA-Z0-9_ ]*>>|\\[[<>a-zA-Z0-9_\\. ]*\\])");
    private static final Pattern ASD_RULE_PATTERN = Pattern.compile("(R[A-Z]{2}\\d{2})");
    private static final Pattern PATTERN_START = Pattern.compile("pattern: ");
    private static final Pattern PATTERN_END_NL = Pattern.compile("(pattern: .*)\\R");
    private static final Pattern PATTERN_END = Pattern.compile("(pattern: .*)$");

    private AsdProfileUtils() {
    }

    public static Set<Rule> getRules() {
        return AsdProfile.PROFILE.getRules();
    }

    public static Set<Rule> getRelatedRules(AsdRule rule) {
        final Set<Rule> set = new HashSet<>();
        for (final Rule cdc : AsdProfile.PROFILE.getRules()) {
            if (AsdRuleUtils.getRelatedRules(cdc).contains(rule)) {
                set.add(cdc);
            }
        }
        return set;
    }

    public static void showRules() {
        final Set<Rule> ngRules = AsdProfileUtils.getRules();
        final Set<Rule> orphanNgRules = new HashSet<>(ngRules);

        final Map<AsdRule, Set<Rule>> asdToNg = new EnumMap<>(AsdRule.class);
        for (final Rule rule : AsdProfile.PROFILE.getRules()) {
            for (final AsdRule asd : AsdRuleUtils.getRelatedRules(rule)) {
                final Set<Rule> set = asdToNg.computeIfAbsent(asd, k -> new HashSet<>());
                set.add(rule);
                orphanNgRules.remove(rule);
            }
        }

        final Set<AsdRule> unsupportedAsdRules = EnumSet.noneOf(AsdRule.class);
        Collections.addAll(unsupportedAsdRules, AsdRule.values());
        unsupportedAsdRules.removeAll(asdToNg.keySet());

        LOGGER.info("There are {} CDC Rules", ngRules.size());
        LOGGER.info("There are {} ASD Rules", AsdRule.values().length);
        LOGGER.info("There are {} ASD Rules supported by {} CDC Rules",
                    asdToNg.keySet().size(),
                    ngRules.size() - orphanNgRules.size());
        for (final AsdRule asd : asdToNg.keySet()
                                        .stream()
                                        .sorted(Comparator.comparing(AsdRule::getCode))
                                        .toList()) {
            LOGGER.info("   {} {}", asd.getCode(), asd);
            for (final Rule rule : asdToNg.get(asd)) {
                LOGGER.info("      {}", rule.getName());
            }
        }

        LOGGER.info("There are {} orphan CDC Rules", orphanNgRules.size());
        for (final Rule rule : orphanNgRules.stream()
                                            .sorted(Comparator.comparing(Rule::getName))
                                            .toList()) {
            LOGGER.info("   {}", rule.getName());
        }

        LOGGER.info("There are {} unsupported ASD Rules", unsupportedAsdRules.size());
        for (final AsdRule asd : unsupportedAsdRules) {
            LOGGER.info("   {} {}", asd.getCode(), asd);
        }
    }

    public static void saveMapping(File file) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
        factory.setEnabled(WorkbookWriterFactory.Hint.ZIP32, true);
        try (final WorkbookWriter<?> writer = factory.create(file, WorkbookWriterFeatures.STANDARD_BEST)) {
            writer.beginSheet("ASD");
            writer.addRow(TableSection.HEADER,
                          "Code",
                          "Scope",
                          "Type of Check",
                          "Rule",
                          "Name",
                          "Description",
                          "Remarks",
                          "CDC Rules");
            for (final AsdRule asd : List.of(AsdRule.values())
                                         .stream()
                                         .sorted(Comparator.comparing(AsdRule::getCode))
                                         .toList()) {
                writer.beginRow(TableSection.DATA);
                writer.addCell(asd.getCode());
                writer.addCell(asd.getScope() == null ? "???" : asd.getScope().getLiteral());
                writer.addCell(asd.getCheckType() == null ? "???" : asd.getCheckType().getLiteral());
                writer.addCell(asd.name());
                writer.addCell(asd.getName());
                writer.addCell(asd.getDescription());
                writer.addCell(asd.getRemarks()
                                  .stream()
                                  .collect(Collectors.joining("\n")));
                writer.addCell(getRelatedRules(asd).stream()
                                                   .map(x -> "- " + x.getName() + " " + x.getTitle())
                                                   .collect(Collectors.joining("\n")));
            }

            writer.beginSheet("CDC");
            writer.addRow(TableSection.HEADER,
                          "Name",
                          "Title",
                          "Description",
                          "Applies to",
                          "Remarks",
                          "Sources",
                          "Labels",
                          "ASD Rules");
            for (final Rule rule : CollectionUtils.toSortedList(getRules(),
                                                                Comparator.comparing(Rule::getName))) {
                writer.beginRow(TableSection.DATA);
                writer.addCell(rule.getName());
                writer.addCell(rule.getTitle());
                writer.addCell(rule.getDescription());
                writer.addCell(AsdRuleUtils.getSectionItems(rule, RuleDescription.SECTION_APPLIES_TO)
                                           .stream()
                                           .map(x -> "- " + x)
                                           .collect(Collectors.joining("\n")));
                writer.addCell(AsdRuleUtils.getSectionItems(rule, RuleDescription.SECTION_REMARKS)
                                           .stream()
                                           .map(x -> "- " + x)
                                           .collect(Collectors.joining("\n")));
                writer.addCell(AsdRuleUtils.getSectionItems(rule, RuleDescription.SECTION_SOURCES)
                                           .stream()
                                           .map(x -> "- " + x)
                                           .collect(Collectors.joining("\n")));
                writer.addCell(rule.getLabels()
                                   .getSorted()
                                   .stream()
                                   .map(x -> "- " + x)
                                   .collect(Collectors.joining("\n")));
                writer.addCell(AsdRuleUtils.getRelatedRules(rule)
                                           .stream()
                                           .map(x -> "- " + x.getCode() + " " + x.name())
                                           .collect(Collectors.joining("\n")));
            }
        }
    }

    public static void saveProfileToOffice(File file) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
        factory.setEnabled(WorkbookWriterFactory.Hint.ZIP32, true);
        try (final WorkbookWriter<?> writer = factory.create(file, WorkbookWriterFeatures.STANDARD_BEST)) {
            writer.beginSheet("CDC");
            writer.addRow(TableSection.HEADER,
                          "Name",
                          "Title",
                          "Description",
                          "Applies to",
                          "Remarks",
                          "Sources",
                          "Labels",
                          "ASD Rules");
            for (final Rule rule : CollectionUtils.toSortedList(getRules(),
                                                                Comparator.comparing(Rule::getName))) {
                writer.beginRow(TableSection.DATA);
                writer.addCell(rule.getName());
                writer.addCell(rule.getTitle());
                writer.addCell(AsdRuleUtils.getHeader(rule));
                writer.addCell(AsdRuleUtils.getSectionItems(rule, RuleDescription.SECTION_APPLIES_TO)
                                           .stream()
                                           .map(x -> "- " + x)
                                           .collect(Collectors.joining("\n")));
                writer.addCell(AsdRuleUtils.getSectionItems(rule, RuleDescription.SECTION_REMARKS)
                                           .stream()
                                           .map(x -> "- " + x)
                                           .collect(Collectors.joining("\n")));
                writer.addCell(AsdRuleUtils.getSectionItems(rule, RuleDescription.SECTION_SOURCES)
                                           .stream()
                                           .map(x -> "- " + x)
                                           .collect(Collectors.joining("\n")));
                writer.addCell(rule.getLabels()
                                   .getSorted()
                                   .stream()
                                   .map(x -> "- " + x)
                                   .collect(Collectors.joining("\n")));
                writer.addCell(AsdRuleUtils.getRelatedRules(rule)
                                           .stream()
                                           .map(x -> "- " + x.getCode() + " " + x.name())
                                           .collect(Collectors.joining("\n")));
            }
        }
    }

    public static void saveProfileToHtml(File file) throws IOException {
        final ProfileIoFeatures features =
                ProfileIoFeatures.builder()
                                 .sections(RuleDescription.SECTION_APPLIES_TO,
                                           RuleDescription.SECTION_SOURCES,
                                           RuleDescription.SECTION_REMARKS,
                                           RuleDescription.SECTION_RELATED_TO)
                                 .hint(ProfileIoFeatures.Hint.SHOW_EMPTY_SECTIONS)
                                 .build();

        ProfileIo.save(features, AsdProfile.PROFILE, file);
    }

    private static String convertMd(String s) {
        s = MdProfileIo.wrap(s, ASD_RULE_PATTERN, "**", "**");
        s = MdProfileIo.wrap(s, EM_PATTERN, "`", "`");
        s = MdProfileIo.wrap(s, PATTERN_END_NL, "", "`\n");
        s = MdProfileIo.wrap(s, PATTERN_END, "", "`");
        s = MdProfileIo.replace(s, PATTERN_START, "pattern: `");
        s = MdProfileIo.mdReplaceNL(s);
        return s;
    }

    public static void saveProfileToMd(File file) throws IOException {
        final ProfileIoFeatures features =
                ProfileIoFeatures.builder()
                                 .sections(RuleDescription.SECTION_APPLIES_TO,
                                           RuleDescription.SECTION_SOURCES,
                                           RuleDescription.SECTION_REMARKS,
                                           RuleDescription.SECTION_RELATED_TO)
                                 .itemConverter(AsdProfileUtils::convertMd)
                                 .build();
        ProfileIo.save(features, AsdProfile.PROFILE, file);
    }
}