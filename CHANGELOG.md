# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.24.0] - 2025-02-23
### Added
- Added `AsdStereotypeName.NO_PRINT`.
- Created rule `D03 DiagramStereotypesMustBeAllowed`.
- Added `since` meta to some rules.
- Added `AsdCreateEnumerations` tranformer, `--create-enumerations` and `--cleaner-create-enumerations` options.
  This can be used to generate enumerations from `validValue` tags and make model review easier. #78
- Added `AsdCreateBaseObjectInheritance` transformer, `--create-base-object-inheritance`
  and `--cleaner-create-base-object-inheritance` options.
  This can be used to make `BaseObject` inheritance explicit. It should not be used with checks. #74
- Added `AsdAddMissingXmlNameTags` tranformer, `--add-missing-xml-name-tags` and `--clean-add-missing-xml-name-tags` options.
  This can ne used to craate missing `xmlName` tags. #81 
- Added `AsdAddMissingXmlRefNameTags` tranformer, `--add-missing-xml-ref-name-tags` and `--clean-add-missing-xml-ref-name-tags` options.
  This can ne used to craate missing `xmlRefName` tags. #81
- Added `AsdSuite` options to show and hide meta data in diagrams.
- Added `--html-show-guids` and `--html-show-indices` options.
- Added `--html-img-show-ids`, `--html-img-show-guids` and `--html-img-show-indices` options.
- Created rule `D04 DiagramShouldContainAtMostOneShapePerElement`.
- Added recognition of `sterotypes` of `diagrams`. S5000F uses many non compliant sterotypes.  
  Should the naming of stereotypes be relaxed?

### Changed
- Updated dependencies:
    - cdc-issues-0.66.0
    - cdc-mf-0.45.0
    - cdc-office-0.60.1
    - org.junit-5.12.0


### Removed
- Removed `AsdRuleUtils.define()`. Use `AsdRuleUtils.rule()` instead.


## [0.23.0] - 2025-02-08
### Added
- Created rules to check that classes, interfaces, connectors and inheritances have
  a graphical representation.
- Created rules to check diagram name compliance.
   
### Changed
- Added rules title and labels to ASD/CDC rules mapping. #79
- Added more exceptions to rule `ATTRIBUTE_NAME_SHOULD_START_WITH_CLASS_NAME`.
- Relaxed rule P06 on package name.
- Fixed rule `CLASS(COMPOSITION_PART)_ATTRIBUTE(KEY)_STEREOTYPE_MUST_BE_COMPOSITE_KEY` by ignoring
  compositions whose whole is an `<<exchange>>`.
- Added more exceptions to `AttributeNameShouldStartWithClassName`. #66
- Reactivated `T10 TagWhenUidPatternValueShouldBeAtMost8Chars` and disabled it.  
  This shows that ASD rule `RTD02 UID_PATTERN_LENGTH` is implemented. #65
- Add `Deprecated` label to rules related to `uidPattern` tag.
- Updated dependencies:
    - cdc-issues-0.65.0
    - cdc-mf-0.44.0
    - cdc-office-0.60.0


## [0.22.0] - 2025-01-19
### Added
- Added `AsdSuite` options to select visible/hidden tags in images.
- Added `AsdSuite` options to truncate tag values and limit the number of visible tag values in images.
- Added detection of duplicated GUIDs.
- Added and used `AsdLabels.XSD_GENERATION` to highlight rules that have a particular impact on XSD generation.

### Changed
- `--xsd-namespace` is now optional for `AsdSuite`. #77
- Updated dependencies:
    - cdc-io-0.53.2
    - cdc-issues-0.64.0
    - cdc-mf-0.43.0
    - cdc-office-0.59.0
    - jackcess-4.0.8
- Better compliance with ASD naming convention for issue numbers.

### Fixed
- Fixed project name in `AsdModelChecker`. #76


## [0.21.0] - 2025-01-03
### Added
- Added new values to AsdCompoundAttributeTypeName (introduced by CDM-3.0).
- Added TimeType (CDM-3.0) and Organization to AsdPrimitiveTypeName.  
  Organization is at the same time a \<\<primitive\>\> and a \<\<class\>\>.
- Created `AsdBusinessId` and its hierarchy. #73
- Created a rule to check the number of associations of a \<\<relationship\>\>.
- Created `cdc-asd-xsdgen` module. Generator is incomplete. #68 

### Changed
- Updated dependencies:
    - cdc-graphs-0.71.3
    - cdc-gv-0.100.3
    - cdc-io-0.53.1
    - cdc-issues-0.63.0
    - cdc-kernel-0.51.3
    - cdc-mf-0.42.0
    - cdc-office-0.58.1
    - cdc-rdb-0.52.3
    - cdc-util-0.54.0
    - commons-text-1.13.0
    - hsqldb-2.7.4
    - org.apache.log4j-2.24.3
    - org.junit-5.11.4

### Fixed
- Fixed rules checking presence or absence of `unit tag`.  
  All attributes whose type is `PropertyType` or any of its descendant, excluding `TextPropertyType` are now included.  
  Previously, `unit tag` was expected only for attributes whose type was `NumericalPropertyType` or any of its descendants.
- Fixed `ClassMustNotBePartOfSeveralCompositions` that was renamed to `ClassWhenSomeMustNotBePartOfSeveralCompositions`. #72


## [0.20.0] - 2024-10-12
### Added
- Created `cdc-asd-distrib` module to copy distribution files (into target/distrib/lib).  
  Note that log4j2.xml and other distrib files are not copied.  
  PDF are generated on Linux with chromium in headless mode.
- Created `gendoc.sh`.

### Changed
- Updated dependencies:
    - cdc-mf-0.40.0
    - cdc-issues-0.62.0
    - org.junit-5.11.2
- Temporarily removed attribute `referredFragment` from the `dmRef` element. specgen #63
- Added exceptions to `AttributeNameShouldNotContainCode`.
- Added exceptions to `AttributeNameShouldStartWithClassName`. #66
- Improve message for `TagWhenUidPatternValueShouldBeAtMost8Chars`.  
  This rule will probably be removed.
- Remove`TagWhenUidPatternValueShouldBeAtMost8Chars` from ASD profile. #65
- Changed validity detection of association source/target. #67
- Exclude `uri` from `AttributeTagWhenXmlNameCountMustBe1`.
- Added validity check of some aggregation and composition whole/part.  
  This is an extension of rules related to associations. #67 

### Fixed
- Fixed a bug in conversion of EA weak aggregations (cdc-mf-0.39.0 update).


## [0.19.0] - 2024-09-28
### Changed
- Updated dependencies:
    - cdc-graphs-0.71.2
    - cdc-gv-0.100.2
    - cdc-io-0.52.1
    - cdc-issues-0.61.0
    - cdc-kernel-0.51.2
    - cdc-mf-0.38.0
    - cdc-office-0.57.2
    - cdc-rdb-0.52.2
    - cdc-util-0.53.0
    - commons-cli-1.9.0
    - commons-lang3-3.17.0
    - hsqldb-2.7.3
    - jackcess-4.0.7
    - org.junit-5.11.1
- Changed the way change marks are printed for figures - moving change marks to title. specgen #62

### Added
- When a model element is transformed, a meta is now generated.
- Added export of the S3000L data dictionary with `S1000DDataDictionaryXmlIo`. #59
- Created new checkers: 
    - `ClassMustNotDeclareUselessInheritance`
    - `InterfaceMustNotDeclareUselessInheritance`

### Fixed
- Fixed export of assyCode on specgen. #57
- Fixed coping with duplicate interface implementations on specgen. #58


## [0.18.0] - 2024-06-30
### Added
- Added comparison of attributes cardinalities to `AsdModelsComparator`.
- Added comparison of tips, implementations and specializations to `AsdModelsComparator`.
- Added a sheet containing all data of all models. It can be used to compute stats.

### Changed
- Updated dependencies:
    - cdc-mf-0.37.2

### Fixed
- Fixed detection of Capital Case in `PackageWhenSomeNameMustBeCapitalCase`.  
  This rule has exceptions and its definition should be made more precise. #55


## [0.17.0] - 2024-06-02
### Added
- Created a checker to detect duplicates of note tags. #54

### Changed
- Updated dependencies:
    - cdc-office-0.56.0
    - commons-cli-1.8.0
- Created a checker of validity of formatting tags. #51
- Added a sheet for each model in `AsdModelsCompararor`. #53
- Added options to campare pairs of models in `AsdModelsCompararor`. #53

### Fixed
- Fixed possible NPE in `S1000DXmlIo`. #49


## [0.16.0] - 2024-05-19
### Added
- Added a synthesis sheet to `AsdModelsComparator`. #43
- Created `longDescription` tag.
- Added comparison of packages to `AsdModelsComparator`.
- Added option to display ids to `AsdModelsComparator`.
- Added `--no-show-*` options to `AsdModelsComparator`.
- Added data model comparison and generation of reasonForUpdate statements to `AsdModelToS1000D`. #44
- Incorporated `longDescription` MF tagged value into each UoF generated by `AsdModelToS1000D`.

### Changed
- Modified some cardinality checkers.
- Allowed `longDescription` tag for `uof` packages.
- Updated dependencies:
    - cdc-graphs-0.71.1
    - cdc-gv-0.100.1
    - cdc-io-0.52.0
    - cdc-issues-0.59.1
    - cdc-kernel-0.51.1
    - cdc-mf-0.37.1
    - cdc-office-0.55.0
    - cdc-rdb-0.52.2
    - cdc-util-0.52.1
    - jackcess-4.0.6
    - xml-unit-2.10.0
- Changed `AsdModelsComparator` to display several 'X' when an item is present several times in a model.
- Updated maven plugins.

### Fixed
- Handle missing type in checker.
- Fixed timing in `AsdModelsComparator`.
- Fixed Javadoc maven section of pom.xml to support use of undocumented tags (`@apiNote`, `@implSpec` and `@implmNote`).  
  Those tags have not been officialy documented. It seems they are used only for JDK documentation.


## [0.15.0] - 2024-03-23
### Added
- Added `ChangeMark` and related classes for reporting change marks in `AsdModelToS1000D`
- Added support for `ChangeMark` into `S1000DGlossaryXmlIo`
- Added some labels to rules.
- Created `TagWhenReplacesValueMustExistInRefModel` checker that checks that a replaces tag value
  corresponds to a name of a reference model. #41
- Created new checkers: 
    - `ModelAttributesWithSameNameMustHaveSameTypes`
    - `ModelAttributesWithSameNameMustHaveSameDefinitions`
    - `ModelAttributesWithSameNameMustHaveSameValidValues`
    - `AttributeWhenSomeTagWhenValidValueValueMustNotBeEmpty`

### Changed
- Updated dependencies:
    - cdc-io-0.51.2
    - cdc-issues-0.58.0
    - cdc-mf-0.36.0
    - org.apache.log4j-2.23.1
- Reduced the number of false positives generated by `AttributeNameShouldStartWithClassName`:
    - Ignore attributes of `<<umlPrimitive>>` and `<<prmitive>>`.
- Renamed `AttributeWhenSomeTagWhenValidValueMustNotBeEmpty` to `AttributeWhenIdentifierTagWhenValidValueValueMustNotBeEmpty`.

### Fixed
- Fixed possible NPE in `AttributeNameShouldStartWithClassName`.


## [0.14.0] - 2024-03-10
### Added
- Adeed missing checker option to `AsdSuite`.
- Linked `TAG(REF)_VALUE_SHOULD_CONTAIN_ONE_CONCEPT` to `ATTRIBUTE_REF_INCORRECT`,
  `CLASS_REF_INCORRECT`, ` INTERFACE_REF_INCORRECT` and `ASSOCIATION_REF_INCORRECT`.

### Changed
- Updated dependencies:
    - cdc-io-0.51.1
    - cdc-issues-0.57.1
    - cdc-mf-0.35.0
    - cdc-office-0.54.0
- Renamed `AttributeNameMustNotContainCode` to `AttributeNameShouldNotContainCode`,
  and changed severity to `MINOR`. #39 
- Reduced the number of false positives generated by `AttributeNameShouldStartWithClassName`: #37
    - Ignore attributes of `BaseObject`, `<<attributeGroup>>` and `<<compoundAttribute>>`
    - Ignore names that match the name of an ancestor of the attribute owner
    - Ignore names that match the namle of whole tip of a composition directed to the attribute owner
- Renamed `AssociationToRelationshipTargetCardinalityUpperBoundMustBeUnbounded`
  to `AssociationToRelationshipTargetCardinalityUpperBoundMustBeOneOrUnbounded`.
- Do not check notes of BaseObjct attributes.

### Removed
- Removed use of `NotesConceptsMustBeDefinedAsRefTags`. #38

## [0.13.0] - 2024-03-03
### Added
- Added options to configure checkers in `AsdModelChecker` and `AsdSuite`. #36

### Changed
- Updated dependencies:
    - cdc-issues-0.57.0
    - cdc-mf-0.34.0


## [0.12.0] - 2024-02-25
### Added
- Added `AttributeNameMustStartWithClassName` checker.  
  Note that the `UML Writing Rules and Style Guide` recommends this naming convention,
  whilst `ATTRIBUTE_NAME_CLASS` rule imposes it.
- Added `ClassWhenExchangeMustNotOwnAnyAggregation`.
- Added `ClassWhenExchangeMustNotOwnAnyAssociation`.
- Added `ClassWhenExchangeMustNotOwnAnyImplementation`.
- Created `AsdTagNameFixer` that can fix case of tag names . #34
- Added clean options to `AsdSuite`.
- Created `AsdCdcMappingExporter` and added to to `AsdSuite`. #35

### Changed
- Renamed (simplified) rules related to connectors.
- Renamed `AttributeNameMustStartWithClassName` checker to `AttributeNameShouldStartWithClassName`.
- Changed options of `AsdModelCleaner` that now saves stats.
- Updated dependencies:
    - cdc-mf-0.33.0

### Fixed
- Forced deterministic order in some checkers.


## [0.11.0] - 2024-02-17
### Added
- Added `--profile-all` option to `AsdSuite`.
- Added `--append-basename` option to `AsdSuite`. #29
- Added `ConnectorWhenAssociationTagWhenNoKeyCountMustBe1` rule. #32 

### Changed
- Renamed `NOKEY` to `NO_KEY`.
- Changed glossary entry references logic so attributes' class names are autoatically added. #31
- Updated dependencies:
    - cdc-issues-0.56.1
    - cdc-mf-0.32.0
- Replaced `ClassWhenCompoundAttributeMustHaveAtLeast2Attributes`
  by `ClassWhenCompoundAttributeMustHaveAtLeast1Attribute`. #30

### Fixed
- Created output directory in `AsdSuite`.
- Generate several profile formats in `AsdOthers`.
- Fixed MD formatting in `AsdProfileExporter`. #28
- Created output directory in `AsdProfileExporter`.
- Fixed `ClassWhenSomeTagWhenUidPatternCountMustBe0` and `ClassWhenSomeTagWhenUidPatternCountMustBe1`.  
  Wording of ASD `UID_PATTERN` and `UID_PATTERN_EXCLUSION` rules probably needs a fix. #33


## [0.10.0] - 2024-02-11
### Added
- Created `AsdProfileExporter`.
- Integrated `EaProfileExporter` and `AsdProfileExporter` to `AsdSuite`.

### Changed
- Used patterns to exclude packages from model overview in `AsdSuite`.
- `AsdSuite` `--basename`, `--model-author` and `--model-name` options are now optional.  
  When omitted, they are deduced from EAP file name. #25
- Created `AsdProfileExporter`.
- Added `noKey` as a suitable `AsdTagName`. #27
- Changed layout produced by `S1000DGlossaryXmlIo`, to utilize other S1000D constructs, as per feedback.
- Updated dependencies:
    - cdc-issues-0.56.0
    - cdc-mf-0.31.0
    - org.junit-5.10.2


## [0.9.0] - 2024-01-29
### Added
- Created AttributeTypeIsMandatory checker.
- Created `AsdModelsComparator` utility. #24

### Changed
- Updated dependencies:
    - cdc-mf-0.30.0


## [0.8.1] - 2024-01-27
### Changed
- Added traces to `AsdSuite`.

### Fixed
- Fixed `AsdSuite`: cleaned model is correctly used now, and S1000D options are taken into account.


## [0.8.0] - 2024-01-27 [YANKED]
### Changed
- Updated dependencies:
    - cdc-mf-0.29.0
    - commons-lang3-3.14.0


## [0.7.0] - 2024-01-21
### Added
- Added primitive/compoundAttributes into the glossary generation
- Added automatic generation of the "references" section on the glossary
- Added support for the "replaces" tag on the glossary
- Included "techName"/"infoName" into files on the glossary
- Created `AsdSuite` that can launch several tools at once. #22 

### Changed
- Fixed rendering of attribute type name on the glossary
- Fixed ineffective sorting of the glossary entries
- Fixed unset/incorrect DM declarations or references on the glossary
- Updated dependencies:
    - cdc-mf-0.28.0

### Removed
- Removed large ASD files from generated jar. #21


## [0.6.0] - 2024-01-06
### Changed
- Updated dependencies:
    - cdc-issues-0.54.0
    - cdc-mf-0.26.0
- Removed empty sections in `RULES.md`.

### Fixed
- Fixed `AsdModelCleaner`.


## [0.5.0] - 2024-01-05
### Added
- Created `AsdModelCleaner`, based on MF, to replace `EaModelCleaner`. #15
- Added new capability for recursively setting formatting policies to all NormalBrParaElem subclasses
- Added detection of duplicate siblings. #11
- Added a deduplication pass to `AsdModelCleaner`. #13
- Added new checkers to detect duplicate direct inheritance. 

### Changed
- Updated dependencies:
    - cdc-issues-0.53.0
    - cdc-mf-0.25.0
    
### Removed
- Removed all EA model code that is now replaced by MF.


## [0.4.0] - 2024-01-01
### Changed
- Updated maven plugins.
- Updated dependencies:
    - cdc-graphs-0.71.0
    - cdc-gv-0.100.0
    - cdc-io-0.51.0
    - cdc-issues-0.52.0
    - cdc-kernel-0.51.0
    - cdc-mf-0.22.0
    - cdc-office-0.52.0
    - cdc-rdb-0.51.0
    - cdc-util-0.52.0

### Removed
- Moved `EapDumper` to MF project.


## [0.3.0] - 2023-12-22
### Added
- Created the `cdc-asd-specgen` module.
- Created `EaModelToS1000D` that can generate Data Model and Gloassary S1000D Data Modules.
- Recognize Dependency connectors. #9
- Created `cdc-asd-checks` module. #14

### Changed
- Updated dependencies:
    - cdc-issues-0.51.0
    - cdc-mf-0.21.0
    - cdc-office-0.51.0
    - cdc-util-0.51.0
- Added `exec()` and call `System.exit()` in `EaModelCleaner`, `EaModelOfficeExporter`,
  `EaModelTopS1000D`, `EapDumpAnalyzer`, `EapDumper`, `EaToMf` and `XsdAnalyzer`.
- Used meta data for author and version in MF model.


### Fixed
- Fixed glossary generation: fixing missing formatting when referenced item is an interface.
- Fixed issue #6 with more robust checks/protections to prevent runtime errors upon bad data
- Assume that an aggregation with no subtype is a weak aggregation. #8
- Fixed a NPE in EaClass when name is null.
- Ignore or fix connectors that are invalid. #10  
  For example:
    - An interface that implements a class is ignored.
    - An annotation connector is fixed, as its direction has no real meaning.


## [0.2.0] - 2023-11-25
### Changed
- Moved to Java 17
- Updated dependencies
    - cdc-graphs-0.70.0
    - cdc-gv-0.99.0
    - cdc-io-0.50.0
    - cdc-issues-0.50.0
    - cdc-kernel-0.50.0
    - cdc-mf-0.20.0
    - cdc-office-0.50.0
    - cdc-rdb-0.50.0
    - cdc-tuples-1.4.0
    - cdc-util-0.50.0


## [0.1.0] - 2023-11-18
### Added
- Created `XsdAnalyzer` that can do some checks on a ASD XSD file.
- Created `EapDumper` that dumps all or a selection of EAP tables to different office formats.
- Created `EapDumpAnalyzer` that can analyze dumps generated by `EapDumper` and generate an XML EA model.
- Created `EaModelOfficeExporter` that converts an XML EA model to an Office (XLSX) file.
- Created `EaXmiAnalyzer` which is a quick and dirty EA XMI analyzer used to produce an Excel file.  
  One should use `EaModelOfficeExporter`.


## [Before 0.1.0]
This ASD project was created in October 2022, one year before its first release (0.1.0).  
The first tool was an XML schema analyzer, with the aim of detecting errors.  
But developping checks on XSD was difficult and had limitations.  
So, directly analyzing the data model followed: using the XMI form of the data model seemed a good choice.  
It rapidly appeared that XMI contained many specificities and was not so standard.  
As EA uses a data base, it was used to create an EA data model.  
In December 2022 the MF project was also started. It contained a neutral data model.  
There were many redundancies, and basing ASD on MF project started in April 2023.  
The full merger took months.