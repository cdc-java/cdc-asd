package cdc.asd.tools;

import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import cdc.util.cli.MainResult;

class AsdProfileExporterTest {
    @ParameterizedTest
    @ValueSource(strings = { "md", "xml", "html", "xlsx", "csv" })
    void test(String ext) {
        final MainResult result = AsdProfileExporter.exec("--output", "target/asd-profile." + ext);
        assertSame(MainResult.SUCCESS, result);
    }
}