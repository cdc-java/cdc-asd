package cdc.asd.tools;

import static org.junit.jupiter.api.Assertions.assertSame;

import cdc.util.cli.MainResult;

class ToolsTest {
    private static void check(String spec) {
        final String dir = "src/main/resources/" + spec + "/";
        final MainResult result = AsdSuite.exec("--args-file", dir + spec + "-asd-suite-args.txt");

        assertSame(MainResult.SUCCESS, result);
    }

    // @Test
    // void testEaRdbMeta() throws Exception {
    // EaRdbMeta.main();
    // assertTrue(true);
    // }
    //
    // @Test
    // void testSX000I() {
    // check("sx000i-3.0");
    // }
    //
    // @Test
    // void testSX002D() {
    // check("sx002d-2.1");
    // }
    //
    // @Test
    // void testS2000M() {
    // check("s2000m-7.0");
    // }

    // @Test
    void testS3000L() {
        check("s3000l-2.0");
    }

    // @Test
    // void testS5000F() {
    // check("s5000f-3.0");
    // }
    //
    // @Test
    // void testS6000T() {
    // check("s6000t-2.0");
    // }
}