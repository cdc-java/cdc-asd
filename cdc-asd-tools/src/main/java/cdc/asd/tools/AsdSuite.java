package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.AsdEaNaming;
import cdc.asd.model.Config;
import cdc.mf.ea.EaDumpToMf;
import cdc.mf.ea.EaProfileExporter;
import cdc.mf.ea.EapDumper;
import cdc.mf.html.MfHtmlGenerationArgs;
import cdc.mf.tools.MfToHtml;
import cdc.mf.tools.MfToOffice;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.files.Resources;

public final class AsdSuite {
    private static final Logger LOGGER = LogManager.getLogger(AsdSuite.class);
    private final MainArgs margs;

    public static class MainArgs {
        /** The EAP database file. */
        public File eapFile;
        /** Output directory. basename can be appended if required. */
        public File outputDir;
        /** Base name of read / generated files. */
        public String basename;
        /** Model name. */
        public String modelName;
        /** Model author. */
        public String modelAuthor;

        /** Names of individually enabled rules. */
        public final Set<String> checkEnabledRules = new HashSet<>();
        /** Names of individually disabled rules. */
        public final Set<String> checkDisabledRules = new HashSet<>();
        /** Name of the profile config file to load. */
        public File checkProfileConfigFile;
        /** Input reference MF ASD XML model file. */
        public File checkRefModelFile;

        /** Paths patterns of packages that should be ignored in the overview image of the model. */
        public final List<Pattern> htmlModelOverviewIgnoredPackages = new ArrayList<>();
        /** Patterns of names parts that must be removed. */
        public final List<Pattern> htmlNameRemovedParts = new ArrayList<>();
        public int htmlLtorThreshold = MfHtmlGenerationArgs.DEFAULT_LTOR_THRESHOLD;
        public int htmlLayoutDepth = MfHtmlGenerationArgs.DEFAULT_LAYOUT_DEPTH;
        /** Names of individually shown tags in images. */
        public final Set<String> htmlImgVisibleTags = new HashSet<>();
        /** Names of individually hidden tags in images. */
        public final Set<String> htmlImgHiddenTags = new HashSet<>();
        /** Max length of tag values in images. */
        public int htmlImgTagValueMaxLength = -1;
        /** Max number of tag values in images. */
        public int htmlImgMaxTagValues = -1;
        /** Names of individually shown metas in images. */
        public final Set<String> htmlImgVisibleMetas = new HashSet<>();
        /** Names of individually hidden metas in images. */
        public final Set<String> htmlImgHiddenMetas = new HashSet<>();

        public String xsdNamespace;
        public String xsdSpecification;
        public String xsdSpecificationUrl;
        public String xsdIssueNumber;
        public String xsdIssueDate;
        public String xsdXmlSchemaReleaseNumber;
        public String xsdXmlSchemaReleaseDate;
        public String xsdCopyright;
        public URL xsdValidValuesUnits;
        public URL xsdValidValuesLibraries;
        public URL xsdValidValuesExternalLibraries;

        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public void htmlIgnoreModelOverviewPackage(String pattern) {
            htmlModelOverviewIgnoredPackages.add(Pattern.compile(pattern));
        }

        public void htmlRemoveNamePart(String pattern) {
            htmlNameRemovedParts.add(Pattern.compile(pattern));
        }

        public String getBasename() {
            if (basename == null) {
                final AsdEaNaming naming = new AsdEaNaming(eapFile);
                return naming.getBasename();
            } else {
                return basename;
            }
        }

        public String getModelName() {
            if (modelName == null) {
                final AsdEaNaming naming = new AsdEaNaming(eapFile);
                return naming.getModelName();
            } else {
                return modelName;
            }
        }

        public String getModelAuthor() {
            if (modelAuthor == null) {
                final AsdEaNaming naming = new AsdEaNaming(eapFile);
                return naming.getModelAuthor();
            } else {
                return modelAuthor;
            }
        }

        public File getOutputDir() {
            if (features.contains(Feature.APPEND_BASENAME)) {
                return new File(outputDir, getBasename());
            } else {
                return outputDir;
            }
        }

        public enum Feature implements OptionEnum {
            RUN_ALL("run-all",
                    "Run all tools, except the one listed with '--no-run option-*'"),

            RUN_ASD_CDC_MAPPING_EXPORTER("run-asd-cdc-mapping-exporter",
                                         "Export the ASD/CDC mapping of rules."),
            RUN_EA_PROFILE_EXPORTER("run-ea-profile-exporter",
                                    "Export the EA profile. See --profile-* options."),
            RUN_ASD_PROFILE_EXPORTER("run-asd-profile-exporter",
                                     "Export the ASD profile. See --profile-* options."),
            RUN_EAP_DUMPER("run-eap-dumper",
                           "Dump the EAP database. See --dump-* options."),
            RUN_MODEL_GENERATOR("run-model-generator",
                                "Generate the ASD model from the EA dumps. See --model-* options."),
            RUN_MODEL_CLEANER("run-model-cleaner",
                              "Clean the ASD model."),
            RUN_MODEL_CHECKER("run-model-checker",
                              "Check the ASD model.  See --model-* options."),
            RUN_OFFICE_GENERATOR("run-office-generator",
                                 "Generate Office files from the ASD model. See --office-* options."),
            RUN_HTML_GENERATOR("run-html-generator",
                               "Generate an HTML report. See --html-* options."),
            RUN_S1000D_GENERATOR("run-s1000d-generator",
                                 "Generate S1000D.  See --s1000d-* options."),
            RUN_XSD_GENERATOR("run-xsd-generator",
                              "Generate XSD.  See --xsd-* options."),

            NO_RUN_ASD_CDC_MAPPING_EXPORTER("no-run-asd-cdc-mapping-exporter",
                                            "Do not export the ASD/CDC mapping of rules."),
            NO_RUN_EA_PROFILE_EXPORTER("no-run-ea-profile-exporter",
                                       "Do not export the EA profile. Use with --run-all option."),
            NO_RUN_ASD_PROFILE_EXPORTER("no-run-asd-profile-exporter",
                                        "Do not export the ASD profile. Use with --run-all option."),
            NO_RUN_EAP_DUMPER("no-run-eap-dumper",
                              "Do not dump the EAP database. Use with --run-all option."),
            NO_RUN_MODEL_GENERATOR("no-run-model-generator",
                                   "Do not generate the ASD model from the EA dumps. Use with --run-all option."),
            NO_RUN_MODEL_CLEANER("no-run-model-cleaner",
                                 "Do not clean the ASD model. Use with --run-all option."),
            NO_RUN_MODEL_CHECKER("no-run-model-checker",
                                 "Do not check the ASD model. Use with --run-all option."),
            NO_RUN_OFFICE_GENERATOR("no-run-office-generator",
                                    "Do not generate Office files from the ASD model. Use with --run-all option."),
            NO_RUN_HTML_GENERATOR("no-run-html-generator",
                                  "Do not generate an HTML report. Use with --run-all option."),
            NO_RUN_S1000D_GENERATOR("no-run-s1000d-generator",
                                    "Do not generate S1000D. Use with --run-all option."),
            NO_RUN_XSD_GENERATOR("no-run-xsd-generator",
                                 "Do not generate XSD. Use with --run-all option."),

            PROFILE_XML("profile-xml",
                        "EA/ASD profiles are exported as XML."),
            PROFILE_MD("profile-md",
                       "EA/ASD profiles are exported as MD."),
            PROFILE_XLSX("profile-xlsx",
                         "EA/ASD profiles are exported as XLSX."),
            PROFILE_HTML("profile-html",
                         "EA/ASD profiles are exported as HTML."),
            PROFILE_CONFIG_XLSX("profile-config-xlsx",
                                "ASD profile config is exported as XLSX."),
            PROFILE_ALL("profile-all",
                        "EA/ASD profiles and ASD profile config are exported as XML, MD, XLSX and HTML."),
            PROFILE_SHOW_EMPTY_SECTIONS("profile-show-empty-sections",
                                        "If enabled, empty sections of EA/ASD profiles are exported."),
            PROFILE_SHOW_RULES_SEVERITIES("profile-show-rules-severities",
                                          "If enabled, rules severities of EA/ADS profiles are exported."),
            PROFILE_SHOW_RULES_ENABLING("profile-show-rules-enabling",
                                        "If enabled, rules enabling of EA/ASD profiles is exported."),
            PROFILE_SHOW_RULES_DOMAIN("profile-show-rules-domain",
                                      "If enabled, rules domain of EA/ASD profiles is exported."),

            DUMP_CSV("dump-csv", "Uses CSV format to generate/read EAP dumps."),
            DUMP_XLSX("dump-xlsx", "Uses XLSX format to generate/read EAP dumps (default)."),
            DUMP_ALL("dump-all", "Dumps all tables."),
            DUMP_MIN("dump-min", "Dumps only tables that are necessary for post processing (Default)."),
            DUMP_FORCE("dump-force", "Dump EAP database even if output is already generated (default)."),
            DUMP_NO_FORCE("dump-no-force", """
                                           Do not dump EAP database if output is already generated.
                                           Does not work with '--dump-min'."""),

            MODEL_FIX_DIRECTION("model-fix-direction",
                                "Fix direction and navigability of aggregations and compositions from the whole to the part."),
            MODEL_NO_FIX_DIRECTION("model-no-fix-direction",
                                   "Do not fix direction and navigability of aggregations and compositions."),

            CHECK_ENABLE_ALL("check-enable-all",
                             "Enable all rules checkers. Use with --check-disable to disable individual rule checkers."),
            CHECK_DISABLE_ALL("check-disable-all",
                              "Disable all rules checkers. Use with --check-enable to enable individual rule checkers."),

            CLEAN_ALL("clean-all", "Enable all cleaning options."),
            CLEAN_ADD_MISSING_XML_NAME_TAGS("clean-add-missing-xml-name-tags",
                                            "Add xmlName tags that are missing."),
            CLEAN_ADD_MISSING_XML_REF_NAME_TAGS("clean-add-missing-xml-ref-name-tags",
                                                "Add xmlRefName tags that are missing."),
            CLEAN_DEDUPLICATE_NAMES("clean-deduplicate-names",
                                    "When several sibling packages, classes, interfaces have the same name, they are renamed to make their name unique."),
            CLEAN_FIX_TAG_NAME_CASE("clean-fix-tag-name-case",
                                    "When a tag name case is invalid, fix it."),
            CLEAN_REMOVE_EXTRA_SPACES("clean-remove-extra-spaces",
                                      "All leading and trailing spaces are removed from notes and tag values."),
            CLEAN_REMOVE_HTML_TAGS("clean-remove-html-tags",
                                   "All html tags are removed from notes."),
            CLEAN_REPLACE_HTML_ENTITIES("clean-replace-html-entities",
                                        "Some html entities are replaced in notes."),
            CLEAN_SPLIT_REF_TAGS("clean-split-ref-tags",
                                 "All ref tags that contain several refs are split into tags that contain one ref."),

            CLEAN_CREATE_ENUMERATIONS("clean-create-enumerations",
                                      "Create enumerations for properties that have validValue tags."
                                              + "\nThis does not fix the data model but can help reviewing it."),
            CLEAN_CREATE_BASE_OBJECT_INHERITANCE("clean-create-base-object-inheritance",
                                                 "Make BaseObject inheritance explicit."
                                                         + "\nThis does not fix the data model but can help reviewing it."
                                                         + "\nDO NOT USE with checks."),

            HTML_FORCE("html-force", "Force image generation, even when images have not changed."),
            HTML_NO_FORCE("html-no-force", "Do not force image generation (default)."),
            HTML_PARALLEL("html-parallel", "Use parallel tasks for HTML generation (default)."),
            HTML_NO_PARALLEL("html-no-parallel", "Do not use parallel tasks for HTML generation."),
            HTML_FLAT_DIRS("html-flat-dirs",
                           "Use flat directories (hashcodes). Otherwise, use hierarchy of directories."
                                   + "\nUseful with frames."),
            HTML_SHOW_IDS("html-show-ids", "Show IDs."),
            HTML_SHOW_GUIDS("html-show-guids", "Show GUIDs."),
            HTML_SHOW_INDICES("html-show-indices", "Show indices."),
            HTML_SHOW_NON_NAVIGABLE_TIPS("html-show-non-navigable-tips", "Show non-navigable tips in usage."),
            HTML_SINGLE_PAGE("html-single-page", "Generate a single page document."),
            HTML_FRAMES("html-frames", "Generate a multi-frame document (default)."),
            HTML_SORT_TAGS("html-sort-tags", "Sort tags using their name and value."),
            HTML_IMG_COLORS("html-img-colors", "Generate colored images (default)."),
            HTML_IMG_NO_COLORS("html-img-no-colors", "Generate monochrome (grey) images."),
            HTML_IMG_SHADOWS("html-img-shadows", "Generate shadows in images."),
            HTML_IMG_NO_SHADOWS("html-img-no-shadows", "Do not generate shadows in images (default)."),
            HTML_IMG_PNG("html-img-png", "Generate PNG images (default)."),
            HTML_IMG_SVG("html-img-svg", "Generate SVG images."),
            HTML_IMG_SHOW_ALL_TAGS("html-img-show-all-tags",
                                   "Show all tags by default in images (default). Use with --html-img-hide-tag to hide individual tags."),
            HTML_IMG_HIDE_ALL_TAGS("html-img-hide-all-tags",
                                   "Hide all tags by default in images. Use with --html-img-show-tag to show individual tags."),
            HTML_IMG_SHOW_ALL_METAS("html-img-show-all-metas",
                                    "Show all metas by default in images (default). Use with --html-img-hide-meta to hide individual metas."),
            HTML_IMG_HIDE_ALL_METAS("html-img-hide-all-metas",
                                    "Hide all metas by default in images. Use with --html-img-show-meta to show individual metas."),
            HTML_IMG_SHOW_IDS("html-img-show-ids", "Show IDs in images."),
            HTML_IMG_SHOW_GUIDS("html-img-show-guids", "Show GUIDs in images."),
            HTML_IMG_SHOW_INDICES("html-img-show-indices", "Show indices in images."),
            HTML_FAIL("html-fail", "Fail in case of detected implementation error."),
            HTML_NO_FAIL("html-no-fail", "Do not fail in case of detected implementation error (default)."),

            S1000D_SPEC("s1000d-spec", "Generate data model spec data modules (S2000M, S3000L, ...)."),
            S1000D_GLOSSARY("s1000d-glossary", "Generate glossary data modules (SX001G)."),

            OFFICE_FASTEST("office-fastest", "Use options that are fast to generate office files (default)."),
            OFFICE_BEST("office-best", "Use options that generate best office files."),

            XSD_PARALLEL("xsd-parallel", "Use parallel tasks for XSD generation (default)."),
            XSD_NO_PARALLEL("xsd-no-parallel", "Do not use parallel tasks for XSD generation."),

            APPEND_BASENAME("append-basename", "Append basename to output directory."),

            VERBOSE("verbose", "Prints messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdSuite(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private void banner(String title) {
        final String line = "=============================================================";
        log(line);
        log("=== {}", title);
        log(line);
    }

    private <E extends Enum<E> & OptionEnum> void transfer(MainArgs.Feature sourceFeature,
                                                           E targetFeature,
                                                           FeatureMask<E> targetFeatures) {
        if (margs.features.contains(sourceFeature)) {
            targetFeatures.add(targetFeature);
        }
    }

    private void execute() throws IOException, SQLException, ExecutionException {
        final File eapDumpDir = new File(margs.getOutputDir(), "eap");

        margs.getOutputDir().mkdirs();

        runAsdCdcMappingExporter();
        runEaProfileExporter();
        runAsdProfileExporter();

        runEapDumper(eapDumpDir);
        runEaDumpToMf(eapDumpDir);

        // The model that will be used for tasks after clean
        final File actualModelFile = runAsdModelCleaner();

        runAsdModelChecker(actualModelFile);
        runMfToOffice(actualModelFile);
        runMfToHtml(actualModelFile);
        runAsdToS1000D(actualModelFile);
        runAsdToXsd(actualModelFile);
    }

    private void runAsdCdcMappingExporter() throws IOException {
        if (margs.features.contains(MainArgs.Feature.RUN_ASD_CDC_MAPPING_EXPORTER)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_ASD_CDC_MAPPING_EXPORTER))) {
            banner("ASD/CDC mapping");
            final AsdCdcMappingExporter.MainArgs args = new AsdCdcMappingExporter.MainArgs();

            transfer(MainArgs.Feature.VERBOSE, AsdCdcMappingExporter.MainArgs.Feature.VERBOSE, args.features);

            args.output = new File(margs.getOutputDir(), "asd-cdc-mapping.xlsx");
            AsdCdcMappingExporter.execute(args);
        } else {
            banner("Skipped ASD/CDC mapping");
        }
    }

    private void runEaProfileExporter() throws IOException {
        if (margs.features.contains(MainArgs.Feature.RUN_EA_PROFILE_EXPORTER)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_EA_PROFILE_EXPORTER))) {
            banner("EA Profile");
            final EaProfileExporter.MainArgs args = new EaProfileExporter.MainArgs();

            transfer(MainArgs.Feature.VERBOSE, EaProfileExporter.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_EMPTY_SECTIONS,
                     EaProfileExporter.MainArgs.Feature.SHOW_EMPTY_SECTIONS,
                     args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_RULES_DOMAIN,
                     EaProfileExporter.MainArgs.Feature.SHOW_RULES_DOMAIN,
                     args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_RULES_ENABLING,
                     EaProfileExporter.MainArgs.Feature.SHOW_RULES_ENABLING,
                     args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_RULES_SEVERITIES,
                     EaProfileExporter.MainArgs.Feature.SHOW_RULES_SEVERITY,
                     args.features);

            if (margs.features.contains(MainArgs.Feature.PROFILE_HTML)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "ea-profile.html");
                EaProfileExporter.execute(args);
            }
            if (margs.features.contains(MainArgs.Feature.PROFILE_MD)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "ea-profile.md");
                EaProfileExporter.execute(args);
            }
            if (margs.features.contains(MainArgs.Feature.PROFILE_XLSX)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "ea-profile.xlsx");
                EaProfileExporter.execute(args);
            }
            if (margs.features.contains(MainArgs.Feature.PROFILE_XML)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "ea-profile.xml");
                EaProfileExporter.execute(args);
            }
        } else {
            banner("Skipped EA Profile");
        }
    }

    private void runAsdProfileExporter() throws IOException {
        if (margs.features.contains(MainArgs.Feature.RUN_ASD_PROFILE_EXPORTER)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_ASD_PROFILE_EXPORTER))) {
            banner("ASD Profile");
            final AsdProfileExporter.MainArgs args = new AsdProfileExporter.MainArgs();

            transfer(MainArgs.Feature.VERBOSE, AsdProfileExporter.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_EMPTY_SECTIONS,
                     AsdProfileExporter.MainArgs.Feature.SHOW_EMPTY_SECTIONS,
                     args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_RULES_DOMAIN,
                     AsdProfileExporter.MainArgs.Feature.SHOW_RULES_DOMAIN,
                     args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_RULES_ENABLING,
                     AsdProfileExporter.MainArgs.Feature.SHOW_RULES_ENABLING,
                     args.features);
            transfer(MainArgs.Feature.PROFILE_SHOW_RULES_SEVERITIES,
                     AsdProfileExporter.MainArgs.Feature.SHOW_RULES_SEVERITY,
                     args.features);

            if (margs.features.contains(MainArgs.Feature.PROFILE_HTML)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "asd-profile.html");
                AsdProfileExporter.execute(args);
            }
            if (margs.features.contains(MainArgs.Feature.PROFILE_MD)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "asd-profile.md");
                AsdProfileExporter.execute(args);
            }
            if (margs.features.contains(MainArgs.Feature.PROFILE_XLSX)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "asd-profile.xlsx");
                AsdProfileExporter.execute(args);
            }
            if (margs.features.contains(MainArgs.Feature.PROFILE_CONFIG_XLSX)
                    || margs.features.contains(MainArgs.Feature.PROFILE_ALL)) {
                args.output = new File(margs.getOutputDir(), "asd-profile-config.xlsx");
                args.features.add(AsdProfileExporter.MainArgs.Feature.PROFILE_CONFIG);
                AsdProfileExporter.execute(args);
                args.features.remove(AsdProfileExporter.MainArgs.Feature.PROFILE_CONFIG);
            }

            // Always generate XML in that case
            // It is currently the only format that can be loaded (used by MfToHtml)
            args.output = new File(margs.getOutputDir(), "asd-profile.xml");
            AsdProfileExporter.execute(args);
        } else {
            banner("Skipped ASD Profile");
        }
    }

    private void runEapDumper(File eapDumpDir) throws IOException, SQLException {
        if (margs.features.contains(MainArgs.Feature.RUN_EAP_DUMPER)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_EAP_DUMPER))) {
            banner("Dump");
            final EapDumper.MainArgs args = new EapDumper.MainArgs();
            args.basename = margs.getBasename();
            args.eapFile = margs.eapFile;
            args.outputDir = eapDumpDir;

            transfer(MainArgs.Feature.VERBOSE, EapDumper.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.DUMP_ALL, EapDumper.MainArgs.Feature.DUMP_ALL, args.features);
            transfer(MainArgs.Feature.DUMP_MIN, EapDumper.MainArgs.Feature.DUMP_MIN, args.features);
            transfer(MainArgs.Feature.DUMP_XLSX, EapDumper.MainArgs.Feature.XLSX, args.features);
            transfer(MainArgs.Feature.DUMP_CSV, EapDumper.MainArgs.Feature.CSV, args.features);
            transfer(MainArgs.Feature.DUMP_FORCE, EapDumper.MainArgs.Feature.FORCE, args.features);
            transfer(MainArgs.Feature.DUMP_NO_FORCE, EapDumper.MainArgs.Feature.NO_FORCE, args.features);

            EapDumper.execute(args);
        } else {
            banner("Skipped Dump");
        }
    }

    private void runEaDumpToMf(File eapDumpDir) throws IOException {
        if (margs.features.contains(MainArgs.Feature.RUN_MODEL_GENERATOR)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_MODEL_GENERATOR))) {
            banner("Model");
            final AsdEaNaming naming = new AsdEaNaming(margs.eapFile);

            final EaDumpToMf.MainArgs args = new EaDumpToMf.MainArgs();
            args.basename = margs.getBasename();
            args.eapFile = margs.eapFile;
            args.inputDir = eapDumpDir;
            args.outputDir = margs.getOutputDir();
            args.author = margs.getModelAuthor();
            args.modelName = margs.getModelName();
            args.projectName = naming.getProjectName();
            args.snapshotName = naming.getSnapshotName();
            args.lang = "en";

            transfer(MainArgs.Feature.VERBOSE, EaDumpToMf.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.MODEL_FIX_DIRECTION, EaDumpToMf.MainArgs.Feature.FIX_DIRECTION, args.features);
            transfer(MainArgs.Feature.MODEL_NO_FIX_DIRECTION, EaDumpToMf.MainArgs.Feature.NO_FIX_DIRECTION, args.features);
            transfer(MainArgs.Feature.OFFICE_BEST, EaDumpToMf.MainArgs.Feature.BEST, args.features);
            transfer(MainArgs.Feature.OFFICE_FASTEST, EaDumpToMf.MainArgs.Feature.FASTEST, args.features);

            EaDumpToMf.execute(args);
        } else {
            banner("Skipped Model");
        }
    }

    private File runAsdModelCleaner() throws IOException {
        final File modelFile = new File(margs.getOutputDir(), margs.getBasename() + "-model.xml");
        final File cleanedModelFile = new File(margs.getOutputDir(), margs.getBasename() + AsdModelCleaner.CLEANED);

        // The model that will be used for tasks after clean
        final File actualModelFile;

        if (margs.features.contains(MainArgs.Feature.RUN_MODEL_CLEANER)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_MODEL_CLEANER))) {
            banner("Clean");
            final AsdModelCleaner.MainArgs args = new AsdModelCleaner.MainArgs();
            args.modelFile = modelFile;
            args.outputDir = margs.getOutputDir();
            args.basename = margs.getBasename();

            transfer(MainArgs.Feature.VERBOSE,
                     AsdModelCleaner.MainArgs.Feature.VERBOSE,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_ALL,
                     AsdModelCleaner.MainArgs.Feature.ALL,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_ADD_MISSING_XML_NAME_TAGS,
                     AsdModelCleaner.MainArgs.Feature.ADD_MISSING_XML_NAME_TAGS,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_ADD_MISSING_XML_REF_NAME_TAGS,
                     AsdModelCleaner.MainArgs.Feature.ADD_MISSING_XML_REF_NAME_TAGS,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_DEDUPLICATE_NAMES,
                     AsdModelCleaner.MainArgs.Feature.DEDUPLICATE_NAMES,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_FIX_TAG_NAME_CASE,
                     AsdModelCleaner.MainArgs.Feature.FIX_TAG_NAME_CASE,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_REMOVE_EXTRA_SPACES,
                     AsdModelCleaner.MainArgs.Feature.REMOVE_EXTRA_SPACES,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_REMOVE_HTML_TAGS,
                     AsdModelCleaner.MainArgs.Feature.REMOVE_HTML_TAGS,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_REPLACE_HTML_ENTITIES,
                     AsdModelCleaner.MainArgs.Feature.REPLACE_HTML_ENTITIES,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_SPLIT_REF_TAGS,
                     AsdModelCleaner.MainArgs.Feature.SPLIT_REF_TAGS,
                     args.features);

            transfer(MainArgs.Feature.CLEAN_CREATE_ENUMERATIONS,
                     AsdModelCleaner.MainArgs.Feature.CREATE_ENUMERATIONS,
                     args.features);
            transfer(MainArgs.Feature.CLEAN_CREATE_BASE_OBJECT_INHERITANCE,
                     AsdModelCleaner.MainArgs.Feature.CREATE_BASE_OBJECT_INHERITANCE,
                     args.features);

            AsdModelCleaner.execute(args);

            // Clean is enabled: use the cleaned model
            actualModelFile = cleanedModelFile;
        } else {
            banner("Skipped Clean");
            // Clean is disable: use the original model
            actualModelFile = modelFile;
        }
        return actualModelFile;
    }

    private void runAsdModelChecker(File actualModelFile) throws IOException {
        if (margs.features.contains(MainArgs.Feature.RUN_MODEL_CHECKER)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_MODEL_CHECKER))) {
            banner("Check");
            final AsdModelChecker.MainArgs args = new AsdModelChecker.MainArgs();
            args.basename = margs.getBasename();
            args.modelFile = actualModelFile;
            args.outputDir = margs.getOutputDir();

            transfer(MainArgs.Feature.VERBOSE, AsdModelChecker.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.OFFICE_BEST, AsdModelChecker.MainArgs.Feature.BEST, args.features);
            transfer(MainArgs.Feature.OFFICE_FASTEST, AsdModelChecker.MainArgs.Feature.FASTEST, args.features);
            transfer(MainArgs.Feature.CHECK_DISABLE_ALL, AsdModelChecker.MainArgs.Feature.DISABLE_ALL, args.features);
            transfer(MainArgs.Feature.CHECK_ENABLE_ALL, AsdModelChecker.MainArgs.Feature.ENABLE_ALL, args.features);
            args.disabledRules.addAll(margs.checkDisabledRules);
            args.enabledRules.addAll(margs.checkEnabledRules);
            args.profileConfigFile = margs.checkProfileConfigFile;
            args.refModelFile = margs.checkRefModelFile;

            AsdModelChecker.execute(args);
        } else {
            banner("Skipped Check");
        }
    }

    private void runMfToOffice(File actualModelFile) throws IOException {
        if (margs.features.contains(MainArgs.Feature.RUN_OFFICE_GENERATOR)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_OFFICE_GENERATOR))) {
            banner("Office");
            final MfToOffice.MainArgs args = new MfToOffice.MainArgs();
            args.basename = margs.getBasename();
            args.input = actualModelFile;
            args.outputDir = margs.getOutputDir();

            transfer(MainArgs.Feature.VERBOSE, MfToOffice.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.OFFICE_BEST, MfToOffice.MainArgs.Feature.BEST, args.features);
            transfer(MainArgs.Feature.OFFICE_FASTEST, MfToOffice.MainArgs.Feature.FASTEST, args.features);

            MfToOffice.execute(args);
        } else {
            banner("Skipped Office");
        }
    }

    private void runMfToHtml(File actualModelFile) throws IOException, ExecutionException {
        final File modelIssuesFile = new File(margs.getOutputDir(), margs.getBasename() + AsdModelChecker.ISSUES);
        final File checkerProfileFile = new File(margs.getOutputDir(), margs.getBasename() + AsdModelChecker.PROFILE);
        final File asdProfileFile = new File(margs.getOutputDir(), "asd-profile.xml");
        final File htmlDir = new File(margs.getOutputDir(), "html");

        if (margs.features.contains(MainArgs.Feature.RUN_HTML_GENERATOR)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_HTML_GENERATOR))) {
            banner("Html");
            final MfToHtml.MainArgs args = new MfToHtml.MainArgs();
            args.input = actualModelFile;
            if (margs.features.contains(MainArgs.Feature.RUN_MODEL_CHECKER)
                    || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                            && !margs.features.contains(MainArgs.Feature.NO_RUN_MODEL_CHECKER))) {
                args.issues = modelIssuesFile;
                args.profile = checkerProfileFile;
            }
            if (args.profile == null
                    && (margs.features.contains(MainArgs.Feature.RUN_ASD_PROFILE_EXPORTER)
                            || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                                    && !margs.features.contains(MainArgs.Feature.NO_RUN_ASD_PROFILE_EXPORTER)))) {
                args.profile = asdProfileFile;
            }

            args.outputDir = htmlDir;
            args.layoutDepth = margs.htmlLayoutDepth;
            args.ltorThreshold = margs.htmlLtorThreshold;
            args.modelOverviewIgnoredPackages.addAll(margs.htmlModelOverviewIgnoredPackages);
            args.nameRemovedParts.addAll(margs.htmlNameRemovedParts);
            args.imgHiddenTags.addAll(margs.htmlImgHiddenTags);
            args.imgVisibleTags.addAll(margs.htmlImgVisibleTags);
            args.imgMaxTagValues = margs.htmlImgMaxTagValues;
            args.imgTagValueMaxLength = margs.htmlImgTagValueMaxLength;
            args.imgHiddenMetas.addAll(margs.htmlImgHiddenMetas);
            args.imgVisibleMetas.addAll(margs.htmlImgVisibleMetas);

            transfer(MainArgs.Feature.VERBOSE, MfToHtml.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.HTML_FAIL, MfToHtml.MainArgs.Feature.FAIL, args.features);
            transfer(MainArgs.Feature.HTML_FLAT_DIRS, MfToHtml.MainArgs.Feature.FLAT_DIRS, args.features);
            transfer(MainArgs.Feature.HTML_FORCE, MfToHtml.MainArgs.Feature.FORCE, args.features);
            transfer(MainArgs.Feature.HTML_FRAMES, MfToHtml.MainArgs.Feature.FRAMES, args.features);
            transfer(MainArgs.Feature.HTML_IMG_COLORS, MfToHtml.MainArgs.Feature.IMG_COLORS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_NO_COLORS, MfToHtml.MainArgs.Feature.IMG_NO_COLORS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_NO_SHADOWS, MfToHtml.MainArgs.Feature.IMG_NO_SHADOWS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_PNG, MfToHtml.MainArgs.Feature.IMG_PNG, args.features);
            transfer(MainArgs.Feature.HTML_IMG_SHADOWS, MfToHtml.MainArgs.Feature.IMG_SHADOWS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_SVG, MfToHtml.MainArgs.Feature.IMG_SVG, args.features);
            transfer(MainArgs.Feature.HTML_IMG_HIDE_ALL_TAGS, MfToHtml.MainArgs.Feature.IMG_HIDE_ALL_TAGS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_HIDE_ALL_METAS, MfToHtml.MainArgs.Feature.IMG_HIDE_ALL_METAS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_SHOW_IDS, MfToHtml.MainArgs.Feature.IMG_SHOW_IDS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_SHOW_GUIDS, MfToHtml.MainArgs.Feature.IMG_SHOW_GUIDS, args.features);
            transfer(MainArgs.Feature.HTML_IMG_SHOW_INDICES, MfToHtml.MainArgs.Feature.IMG_SHOW_INDICES, args.features);
            transfer(MainArgs.Feature.HTML_NO_FAIL, MfToHtml.MainArgs.Feature.NO_FAIL, args.features);
            transfer(MainArgs.Feature.HTML_NO_FORCE, MfToHtml.MainArgs.Feature.NO_FORCE, args.features);
            transfer(MainArgs.Feature.HTML_NO_PARALLEL, MfToHtml.MainArgs.Feature.NO_PARALLEL, args.features);
            transfer(MainArgs.Feature.HTML_PARALLEL, MfToHtml.MainArgs.Feature.PARALLEL, args.features);
            transfer(MainArgs.Feature.HTML_SHOW_IDS, MfToHtml.MainArgs.Feature.SHOW_IDS, args.features);
            transfer(MainArgs.Feature.HTML_SHOW_GUIDS, MfToHtml.MainArgs.Feature.SHOW_GUIDS, args.features);
            transfer(MainArgs.Feature.HTML_SHOW_INDICES, MfToHtml.MainArgs.Feature.SHOW_INDICES, args.features);
            transfer(MainArgs.Feature.HTML_SHOW_NON_NAVIGABLE_TIPS,
                     MfToHtml.MainArgs.Feature.SHOW_NON_NAVIGABLE_TIPS,
                     args.features);
            transfer(MainArgs.Feature.HTML_SINGLE_PAGE, MfToHtml.MainArgs.Feature.SINGLE_PAGE, args.features);
            transfer(MainArgs.Feature.HTML_SORT_TAGS, MfToHtml.MainArgs.Feature.SORT_TAGS, args.features);

            MfToHtml.execute(args);
        } else {
            banner("Skipped Html");
        }
    }

    private void runAsdToS1000D(File actualModelFile) throws IOException {
        final File s1000dDir = new File(margs.getOutputDir(), "s1000d");

        if (margs.features.contains(MainArgs.Feature.RUN_S1000D_GENERATOR)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_S1000D_GENERATOR))) {
            banner("S1000D");
            final AsdModelToS1000D.MainArgs args = new AsdModelToS1000D.MainArgs();
            args.basename = "";
            args.modelFile = actualModelFile;
            args.outputDir = s1000dDir;

            transfer(MainArgs.Feature.VERBOSE, AsdModelToS1000D.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.S1000D_GLOSSARY, AsdModelToS1000D.MainArgs.Feature.GLOSSARY, args.features);
            transfer(MainArgs.Feature.S1000D_SPEC, AsdModelToS1000D.MainArgs.Feature.SPEC, args.features);

            AsdModelToS1000D.execute(args);
        } else {
            banner("Skipped S1000D");
        }
    }

    private void runAsdToXsd(File actualModelFile) throws IOException, ExecutionException {
        final File xsdDir = new File(margs.getOutputDir(), "xsd");
        if (margs.features.contains(MainArgs.Feature.RUN_XSD_GENERATOR)
                || (margs.features.contains(MainArgs.Feature.RUN_ALL)
                        && !margs.features.contains(MainArgs.Feature.NO_RUN_XSD_GENERATOR))) {
            banner("XSD");
            final AsdModelToXsd.MainArgs args = new AsdModelToXsd.MainArgs();

            args.outputDir = xsdDir;
            args.modelFile = actualModelFile;
            args.issueDate = margs.xsdIssueDate;
            args.issueNumber = margs.xsdIssueNumber;
            args.namespace = margs.xsdNamespace;
            args.specification = margs.xsdSpecification;
            args.specificationUrl = margs.xsdSpecificationUrl;
            args.validValuesExternalLibraries = margs.xsdValidValuesExternalLibraries;
            args.validValuesLibraries = margs.xsdValidValuesLibraries;
            args.validValuesUnits = margs.xsdValidValuesUnits;
            args.xmlSchemaReleaseDate = margs.xsdXmlSchemaReleaseDate;
            args.xmlSchemaReleaseNumber = margs.xsdXmlSchemaReleaseNumber;
            args.copyright = margs.xsdCopyright;

            transfer(MainArgs.Feature.VERBOSE, AsdModelToXsd.MainArgs.Feature.VERBOSE, args.features);
            transfer(MainArgs.Feature.XSD_NO_PARALLEL, AsdModelToXsd.MainArgs.Feature.NO_PARALLEL, args.features);
            transfer(MainArgs.Feature.XSD_PARALLEL, AsdModelToXsd.MainArgs.Feature.PARALLEL, args.features);

            AsdModelToXsd.execute(args);
        } else {
            banner("Skipped XSD");
        }

    }

    public static void execute(MainArgs margs) throws IOException, SQLException, ExecutionException {
        final AsdSuite instance = new AsdSuite(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String EAP_FILE = "eap-file";
        private static final String BASENAME = "basename";
        private static final String MODEL_NAME = "model-name";
        private static final String MODEL_AUTHOR = "model-author";
        private static final String CHECK_ENABLE = "check-enable";
        private static final String CHECK_DISABLE = "check-disable";
        private static final String CHECK_PROFILE_CONFIG_FILE = "check-profile-config";
        private static final String CHECK_REF_MODEL_FILE = "check-ref-model";
        private static final String HTML_MODEL_OVERVIEW_IGNORE_PACKAGE = "html-model-overview-ignore-package";
        private static final String HTML_NAME_REMOVE_PART = "html-name-remove-part";
        private static final String HTML_LTOR_THRESHOLD = "html-ltor-threshold";
        private static final String HTML_LAYOUT_DEPTH = "html-layout-depth";
        private static final String HTML_IMG_SHOW_TAG = "html-img-show-tag";
        private static final String HTML_IMG_HIDE_TAG = "html-img-hide-tag";
        private static final String HTML_IMG_TAG_VALUE_MAX_LENGTH = "html-img-tag-value-max-length";
        private static final String HTML_IMG_MAX_TAG_VALUES = "html-img-max-tag-values";
        private static final String HTML_IMG_SHOW_META = "html-img-show-meta";
        private static final String HTML_IMG_HIDE_META = "html-img-hide-meta";
        private static final String XSD_ISSUE_DATE = "xsd-issue-date";
        private static final String XSD_ISSUE_NUMBER = "xsd-issue-number";
        private static final String XSD_SPECIFICATION = "xsd-specification";
        private static final String XSD_SPECIFICATION_URL = "xsd-specification-url";
        private static final String XSD_VALID_VALUES_EXTERNAL_LIBRARIES = "xsd-valid-values-external-libraries";
        private static final String XSD_VALID_VALUES_LIBRARIES = "xsd-valid-values-libraries";
        private static final String XSD_VALID_VALUES_UNITS = "xsd-valid-values-units";
        private static final String XSD_XML_SCHEMA_RELEASE_NUMBER = "xsd-xml-schema-release-number";
        private static final String XSD_XML_SCHEMA_RELEASE_DATE = "xsd-xml-schema-release-date";
        private static final String XSD_COPYRIGHT = "xsd-copyright";
        private static final String XSD_NAMESPACE = "xsd-namespace";

        public MainSupport() {
            super(AsdSuite.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return """
                   Utility that can run several tools, in that order:
                     - EapDumper
                     - EaDumpToMf
                     - AsdModelCleaner
                     - AsdModelChecker
                     - MfToOffice
                     - MfToHtml
                     - AsdModelToS1000D
                     - AsdModelToXsd
                   A tool is executed if the corresponding '--run-xxx' option is enabled.

                   The following naming conventions are applied:
                     - The EAP dumps are located in ${outputDir}/eap
                     - The model file is ${outputDir}/${basename}-model.xml
                     - The cleaned model file is ${outputDir}/${basename}-cleaned-model.xml
                     - The model issues file is ${outputDir}/${basename}-model-issues.???
                     - The HTML report is located in ${outputDir}/html
                     - The S1000D data modules are located in ${outputDir}/s1000d
                     - The XSD files are located in ${outputDir}/xsd""";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(BASENAME)
                                    .desc("Optional base name of generated files. If missing, it is automatically deduced from the EAP file name.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(EAP_FILE)
                                    .desc("Mandatory name of the input EAP file.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Mandatory name of the output directory. If this directory does not exist, it is created.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(MODEL_NAME)
                                    .desc("Optional name of the model. If missing, it is automatically deduced from the EAP file name.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(MODEL_AUTHOR)
                                    .desc("Optional author of the  model. If missing, it is automatically deduced from the EAP file name.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(HTML_MODEL_OVERVIEW_IGNORE_PACKAGE)
                                    .desc("Optional paths patterns of packages that should be ignored in model overview image.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_NAME_REMOVE_PART)
                                    .desc("Optional patterns of name parts that must be removed in images.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_LTOR_THRESHOLD)
                                    .desc("Number of elements from which images are oriented left to right (default "
                                            + MfHtmlGenerationArgs.DEFAULT_LTOR_THRESHOLD + ")."
                                            + "\nIf the number of elements in the image is smaller, the the image is oriented top to bottom.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_LAYOUT_DEPTH)
                                    .desc("The maximum depth used to spread elements in images (default "
                                            + MfHtmlGenerationArgs.DEFAULT_LAYOUT_DEPTH + ").")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_IMG_SHOW_TAG)
                                    .desc("Name(s) of tags to show in images. Use with --html-img-hide-all-tags.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_IMG_HIDE_TAG)
                                    .desc("Name(s) of tags to hide in images. Use with --html-img-show-all-tags.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_IMG_MAX_TAG_VALUES)
                                    .desc("Maximum number of values to show for a tag in images (default: -1).")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_IMG_TAG_VALUE_MAX_LENGTH)
                                    .desc("Maximum length of values to show for a tag in images (default: -1).")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_IMG_SHOW_META)
                                    .desc("Name(s) of metas to show in images. Use with --html-img-hide-all-metas.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(HTML_IMG_HIDE_META)
                                    .desc("Name(s) of metas to hide in images. Use with --html-img-show-all-metas.")
                                    .hasArgs()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(CHECK_ENABLE)
                                    .desc("Name(s) of rule checkers to enable.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(CHECK_DISABLE)
                                    .desc("Name(s) of rule checkers to disable.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(CHECK_PROFILE_CONFIG_FILE)
                                    .desc("Optional name of the ASD Profile Config file to load.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(CHECK_REF_MODEL_FILE)
                                    .desc("Optional name of the reference XML ASD MF model to check against."
                                            + "\nUsed by some checkers as a reference to compare the checked model.")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(XSD_SPECIFICATION)
                                    .desc("Mandatory (If XSD generation is enabled) name of the specification.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_SPECIFICATION_URL)
                                    .desc("Mandatory (If XSD generation is enabled) name of the specification URL.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_ISSUE_NUMBER)
                                    .desc("Mandatory (If XSD generation is enabled) issue number of the specification.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_ISSUE_DATE)
                                    .desc("Mandatory (If XSD generation is enabled) issue date of the specification.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_XML_SCHEMA_RELEASE_NUMBER)
                                    .desc("Mandatory (If XSD generation is enabled) release number of the valid values.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_XML_SCHEMA_RELEASE_DATE)
                                    .desc("Mandatory (If XSD generation is enabled) release date of the valid values.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_VALID_VALUES_UNITS)
                                    .desc("Optional URL of the input valid values units (Office).")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(XSD_VALID_VALUES_LIBRARIES)
                                    .desc("Optional URL of the input valid values libraries (Office).")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_VALID_VALUES_EXTERNAL_LIBRARIES)
                                    .desc("Optional URL of the input valid values external libraries (Office).")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_COPYRIGHT)
                                    .desc("Optional copyright.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XSD_NAMESPACE)
                                    .desc("Mandatory namespace.")
                                    .hasArg()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
            createGroup(options,
                        MainArgs.Feature.RUN_EA_PROFILE_EXPORTER,
                        MainArgs.Feature.NO_RUN_EA_PROFILE_EXPORTER);
            createGroup(options,
                        MainArgs.Feature.RUN_ASD_PROFILE_EXPORTER,
                        MainArgs.Feature.NO_RUN_ASD_PROFILE_EXPORTER);
            createGroup(options,
                        MainArgs.Feature.RUN_EAP_DUMPER,
                        MainArgs.Feature.NO_RUN_EAP_DUMPER);
            createGroup(options,
                        MainArgs.Feature.RUN_HTML_GENERATOR,
                        MainArgs.Feature.NO_RUN_HTML_GENERATOR);
            createGroup(options,
                        MainArgs.Feature.RUN_MODEL_CHECKER,
                        MainArgs.Feature.NO_RUN_MODEL_CHECKER);
            createGroup(options,
                        MainArgs.Feature.RUN_MODEL_CLEANER,
                        MainArgs.Feature.NO_RUN_MODEL_CLEANER);
            createGroup(options,
                        MainArgs.Feature.RUN_MODEL_GENERATOR,
                        MainArgs.Feature.NO_RUN_MODEL_GENERATOR);
            createGroup(options,
                        MainArgs.Feature.RUN_OFFICE_GENERATOR,
                        MainArgs.Feature.NO_RUN_OFFICE_GENERATOR);
            createGroup(options,
                        MainArgs.Feature.RUN_S1000D_GENERATOR,
                        MainArgs.Feature.NO_RUN_S1000D_GENERATOR);
            createGroup(options,
                        MainArgs.Feature.RUN_XSD_GENERATOR,
                        MainArgs.Feature.NO_RUN_XSD_GENERATOR);

            createGroup(options,
                        MainArgs.Feature.OFFICE_BEST,
                        MainArgs.Feature.OFFICE_FASTEST);

            createGroup(options,
                        MainArgs.Feature.DUMP_CSV,
                        MainArgs.Feature.DUMP_XLSX);
            createGroup(options,
                        MainArgs.Feature.DUMP_ALL,
                        MainArgs.Feature.DUMP_MIN);
            createGroup(options,
                        MainArgs.Feature.DUMP_FORCE,
                        MainArgs.Feature.DUMP_NO_FORCE);

            createGroup(options,
                        MainArgs.Feature.CHECK_DISABLE_ALL,
                        MainArgs.Feature.CHECK_ENABLE_ALL);
            createGroup(options,
                        CHECK_DISABLE,
                        CHECK_ENABLE);

            createGroup(options,
                        MainArgs.Feature.HTML_SINGLE_PAGE,
                        MainArgs.Feature.HTML_FRAMES);
            createGroup(options,
                        MainArgs.Feature.HTML_NO_PARALLEL,
                        MainArgs.Feature.HTML_PARALLEL);
            createGroup(options,
                        MainArgs.Feature.HTML_NO_FORCE,
                        MainArgs.Feature.HTML_FORCE);
            createGroup(options,
                        MainArgs.Feature.HTML_NO_FAIL,
                        MainArgs.Feature.HTML_FAIL);
            createGroup(options,
                        MainArgs.Feature.HTML_IMG_COLORS,
                        MainArgs.Feature.HTML_IMG_NO_COLORS);
            createGroup(options,
                        MainArgs.Feature.HTML_IMG_SHADOWS,
                        MainArgs.Feature.HTML_IMG_NO_SHADOWS);
            createGroup(options,
                        MainArgs.Feature.HTML_IMG_PNG,
                        MainArgs.Feature.HTML_IMG_SVG);
            createGroup(options,
                        MainArgs.Feature.HTML_IMG_SHOW_ALL_TAGS,
                        MainArgs.Feature.HTML_IMG_HIDE_ALL_TAGS);
            createGroup(options,
                        HTML_IMG_SHOW_TAG,
                        HTML_IMG_HIDE_TAG);
            createGroup(options,
                        MainArgs.Feature.HTML_IMG_SHOW_ALL_METAS,
                        MainArgs.Feature.HTML_IMG_HIDE_ALL_METAS);
            createGroup(options,
                        HTML_IMG_SHOW_META,
                        HTML_IMG_HIDE_META);

            createGroup(options,
                        MainArgs.Feature.MODEL_FIX_DIRECTION,
                        MainArgs.Feature.MODEL_NO_FIX_DIRECTION);

            createGroup(options,
                        MainArgs.Feature.XSD_NO_PARALLEL,
                        MainArgs.Feature.XSD_PARALLEL);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.basename = getValueAsString(cl, BASENAME, null);
            margs.eapFile = getValueAsResolvedFile(cl, EAP_FILE, IS_FILE);
            margs.outputDir = getValueAsResolvedFile(cl, OUTPUT_DIR);
            margs.modelName = getValueAsString(cl, MODEL_NAME, null);
            margs.modelAuthor = getValueAsString(cl, MODEL_AUTHOR, null);
            margs.htmlLayoutDepth = getValueAsInt(cl, HTML_LAYOUT_DEPTH, MfHtmlGenerationArgs.DEFAULT_LAYOUT_DEPTH);
            margs.htmlLtorThreshold = getValueAsInt(cl, HTML_LTOR_THRESHOLD, MfHtmlGenerationArgs.DEFAULT_LTOR_THRESHOLD);
            margs.htmlImgMaxTagValues = getValueAsInt(cl, HTML_IMG_MAX_TAG_VALUES, -1);
            margs.htmlImgTagValueMaxLength = getValueAsInt(cl, HTML_IMG_TAG_VALUE_MAX_LENGTH, -1);
            margs.checkProfileConfigFile = getValueAsResolvedFile(cl, CHECK_PROFILE_CONFIG_FILE, IS_NULL_OR_FILE);
            margs.checkRefModelFile = getValueAsResolvedFile(cl, CHECK_REF_MODEL_FILE, IS_NULL_OR_FILE);

            margs.xsdNamespace = getValueAsString(cl, XSD_NAMESPACE, null);
            margs.xsdSpecification = getValueAsString(cl, XSD_SPECIFICATION, "???");
            margs.xsdSpecificationUrl = getValueAsString(cl, XSD_SPECIFICATION_URL, "???");
            margs.xsdIssueNumber = getValueAsString(cl, XSD_ISSUE_NUMBER, "???");
            margs.xsdIssueDate = getValueAsString(cl, XSD_ISSUE_DATE, "???");
            margs.xsdXmlSchemaReleaseNumber = getValueAsString(cl, XSD_XML_SCHEMA_RELEASE_NUMBER, "???");
            margs.xsdXmlSchemaReleaseDate = getValueAsString(cl, XSD_XML_SCHEMA_RELEASE_DATE, "???");

            margs.xsdCopyright = getValueAsString(cl, XSD_COPYRIGHT, AsdModelToXsd.DEFAULT_COPYRIGHT);

            margs.xsdValidValuesUnits =
                    getValueAsURL(cl, XSD_VALID_VALUES_UNITS, Resources.getResource(AsdModelToXsd.DEFAULT_VALID_VALUES_UNITS));
            margs.xsdValidValuesLibraries =
                    getValueAsURL(cl,
                                  XSD_VALID_VALUES_LIBRARIES,
                                  Resources.getResource(AsdModelToXsd.DEFAULT_VALID_VALUES_LIBRARIES));
            margs.xsdValidValuesExternalLibraries =
                    getValueAsURL(cl,
                                  XSD_VALID_VALUES_LIBRARIES,
                                  Resources.getResource(AsdModelToXsd.DEFAULT_VALID_VALUES_EXTERNAL_LIBRARIES));

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);
            fillValues(cl, HTML_MODEL_OVERVIEW_IGNORE_PACKAGE, margs.htmlModelOverviewIgnoredPackages, Pattern::compile);
            fillValues(cl, HTML_NAME_REMOVE_PART, margs.htmlNameRemovedParts, Pattern::compile);
            fillValues(cl, HTML_IMG_SHOW_TAG, margs.htmlImgVisibleTags);
            fillValues(cl, HTML_IMG_HIDE_TAG, margs.htmlImgHiddenTags);
            fillValues(cl, HTML_IMG_SHOW_META, margs.htmlImgVisibleMetas);
            fillValues(cl, HTML_IMG_HIDE_META, margs.htmlImgHiddenMetas);
            fillValues(cl, CHECK_ENABLE, margs.checkEnabledRules);
            fillValues(cl, CHECK_DISABLE, margs.checkDisabledRules);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException, SQLException, ExecutionException {
            AsdSuite.execute(margs);
            return null;
        }
    }
}