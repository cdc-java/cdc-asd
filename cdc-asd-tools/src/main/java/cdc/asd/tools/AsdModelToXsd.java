package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.wrappers.AsdUtils;
import cdc.asd.xsdgen.AsdXsdGenerationArgs;
import cdc.asd.xsdgen.AsdXsdGenerationHint;
import cdc.asd.xsdgen.AsdXsdGenerator;
import cdc.mf.Config;
import cdc.mf.ea.EaDumpToMfImpl;
import cdc.mf.model.MfModel;
import cdc.mf.model.io.MfModelXmlIo;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.files.Resources;
import cdc.util.time.Chronometer;

public final class AsdModelToXsd {
    private static final Logger LOGGER = LogManager.getLogger(AsdModelToXsd.class);
    public static final String DEFAULT_VALID_VALUES_UNITS = "cdc/asd/model/ext/valid_values_units.xlsx";
    public static final String DEFAULT_VALID_VALUES_LIBRARIES = "cdc/asd/model/ext/valid_values_libraries.xlsx";
    public static final String DEFAULT_VALID_VALUES_EXTERNAL_LIBRARIES = "cdc/asd/model/ext/valid_values_external_libraries.xlsx";
    public static final String DEFAULT_COPYRIGHT =
            "Copyright (c) 2016-2024 by AeroSpace, Security and Defence Industries Association of Europe (ASD).";

    private final MainArgs margs;

    public static class MainArgs {
        /** Output base directory. */
        public File outputDir;

        /** Input ASD MF XML model file. */
        public File modelFile;

        public String namespace;
        public String specification;
        public String specificationUrl;
        public String issueNumber;
        public String issueDate;
        public String xmlSchemaReleaseNumber;
        public String xmlSchemaReleaseDate;

        public String copyright;

        public URL validValuesUnits;
        public URL validValuesLibraries;
        public URL validValuesExternalLibraries;

        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            PARALLEL("parallel", "Use parallel tasks (default)."),
            NO_PARALLEL("no-parallel", "Do not use parallel tasks."),
            VERBOSE("verbose", "Print messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdModelToXsd(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private void execute() throws IOException, ExecutionException {
        final Chronometer chrono = new Chronometer();

        margs.outputDir.mkdirs();

        log("Loading model {}", margs.modelFile);
        chrono.start();
        final MfModel model = MfModelXmlIo.load(margs.modelFile, AsdUtils.FACTORY);
        chrono.suspend();
        log("Loaded model ({})", chrono);

        final AsdXsdGenerationArgs args =
                AsdXsdGenerationArgs.builder()
                                    .outputDir(margs.outputDir)
                                    .model(model)
                                    .eaFile(new File(model.getMetas().getValue(EaDumpToMfImpl.META_EA_FILE)))
                                    .namespace(margs.namespace)
                                    .specification(margs.specification)
                                    .specificationUrl(margs.specificationUrl)
                                    .issueNumber(margs.issueNumber)
                                    .issueDate(margs.issueDate)
                                    .xmlSchemaReleaseNumber(margs.xmlSchemaReleaseNumber)
                                    .xmlSchemaReleaseDate(margs.xmlSchemaReleaseDate)
                                    .copyright(margs.copyright)
                                    .validValuesUnits(margs.validValuesUnits)
                                    .validValuesLibraries(margs.validValuesLibraries)
                                    .validValuesExternalLibraries(margs.validValuesExternalLibraries)
                                    .hint(AsdXsdGenerationHint.VERBOSE,
                                          margs.features.isEnabled(MainArgs.Feature.VERBOSE))
                                    .hint(AsdXsdGenerationHint.PARALLEL,
                                          margs.features.isEnabled(MainArgs.Feature.PARALLEL))
                                    .build();

        log("Generating XSD into {}", margs.outputDir);
        chrono.start();
        final AsdXsdGenerator xsdGenerator = new AsdXsdGenerator(args);
        xsdGenerator.generate();
        chrono.suspend();
        log("Generated XSD into {} ({})", margs.outputDir.getCanonicalFile(), chrono);
    }

    public static void execute(MainArgs margs) throws IOException, ExecutionException {
        final AsdModelToXsd instance = new AsdModelToXsd(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String COPYRIGHT = "copyright";
        private static final String ISSUE_DATE = "issue-date";
        private static final String ISSUE_NUMBER = "issue-number";
        private static final String MODEL_FILE = "model";
        private static final String NAMESPACE = "namespace";
        private static final String SPECIFICATION = "specification";
        private static final String SPECIFICATION_URL = "specification-url";
        private static final String VALID_VALUES_EXTERNAL_LIBRARIES = "valid-values-external-libraries";
        private static final String VALID_VALUES_LIBRARIES = "valid-values-libraries";
        private static final String VALID_VALUES_UNITS = "valid-values-units";
        private static final String XML_SCHEMA_RELEASE_NUMBER = "xml-schema-release-number";
        private static final String XML_SCHEMA_RELEASE_DATE = "xml-schema-release-date";

        public MainSupport() {
            super(AsdModelToXsd.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return "Converts an ASD model to XSD.";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Mandatory name of the output directory.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(MODEL_FILE)
                                    .desc("Mandatory name of the ASD MF XML model to transform.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(NAMESPACE)
                                    .desc("Mandatory namespace.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SPECIFICATION)
                                    .desc("Mandatory name of the specification.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(SPECIFICATION_URL)
                                    .desc("Mandatory name of the specification URL.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(ISSUE_NUMBER)
                                    .desc("Mandatory issue number of the specification.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(ISSUE_DATE)
                                    .desc("Mandatory issue date of the specification.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(XML_SCHEMA_RELEASE_NUMBER)
                                    .desc("Mandatory release number of the valid values.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(XML_SCHEMA_RELEASE_DATE)
                                    .desc("Mandatory release date of the valid values.")
                                    .hasArg()
                                    .required()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(VALID_VALUES_UNITS)
                                    .desc("Optional URL of the input valid values units (Office).")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(VALID_VALUES_LIBRARIES)
                                    .desc("Optional URL of the input valid values libraries (Office).")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(VALID_VALUES_EXTERNAL_LIBRARIES)
                                    .desc("Optional URL of the input valid values external libraries (Office).")
                                    .hasArg()
                                    .build());

            options.addOption(Option.builder()
                                    .longOpt(COPYRIGHT)
                                    .desc("Optional copyright.")
                                    .hasArg()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);

            createGroup(options,
                        MainArgs.Feature.NO_PARALLEL,
                        MainArgs.Feature.PARALLEL);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.outputDir = getValueAsResolvedFile(cl, OUTPUT_DIR);
            margs.modelFile = getValueAsResolvedFile(cl, MODEL_FILE, IS_FILE);

            margs.namespace = getValueAsString(cl, NAMESPACE, null);
            margs.specification = getValueAsString(cl, SPECIFICATION, "???");
            margs.specificationUrl = getValueAsString(cl, SPECIFICATION_URL, "???");
            margs.issueNumber = getValueAsString(cl, ISSUE_NUMBER, "???");
            margs.issueDate = getValueAsString(cl, ISSUE_DATE, "???");
            margs.xmlSchemaReleaseNumber = getValueAsString(cl, XML_SCHEMA_RELEASE_NUMBER, "???");
            margs.xmlSchemaReleaseDate = getValueAsString(cl, XML_SCHEMA_RELEASE_DATE, "???");

            margs.copyright = getValueAsString(cl, COPYRIGHT, DEFAULT_COPYRIGHT);

            margs.validValuesUnits =
                    getValueAsURL(cl, VALID_VALUES_UNITS, Resources.getResource(DEFAULT_VALID_VALUES_UNITS));
            margs.validValuesLibraries =
                    getValueAsURL(cl, VALID_VALUES_LIBRARIES, Resources.getResource(DEFAULT_VALID_VALUES_LIBRARIES));
            margs.validValuesExternalLibraries =
                    getValueAsURL(cl, VALID_VALUES_LIBRARIES, Resources.getResource(DEFAULT_VALID_VALUES_EXTERNAL_LIBRARIES));

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException, ExecutionException {
            AsdModelToXsd.execute(margs);
            return null;
        }
    }
}