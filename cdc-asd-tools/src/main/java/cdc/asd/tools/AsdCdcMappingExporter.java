package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.checks.AsdProfileUtils;
import cdc.mf.Config;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.time.Chronometer;

public final class AsdCdcMappingExporter {
    private static final Logger LOGGER = LogManager.getLogger(AsdCdcMappingExporter.class);
    /** Pattern of sections that must be surrounded by ``. */

    private final MainArgs margs;

    public static class MainArgs {
        /** Name of the generated file. */
        public File output;
        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            VERBOSE("verbose", "Prints messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdCdcMappingExporter(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private void execute() throws IOException {
        final Chronometer chrono = new Chronometer();

        log("Exporting ASD/CDC mapping to {}", margs.output);
        chrono.start();

        // Save mapping
        margs.output.getParentFile().mkdirs();
        AsdProfileUtils.saveMapping(margs.output);
        chrono.suspend();
        log("Exported ASD/CDC mapping ({})", chrono);
    }

    public static void execute(MainArgs margs) throws IOException {
        final AsdCdcMappingExporter instance = new AsdCdcMappingExporter(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {

        public MainSupport() {
            super(AsdCdcMappingExporter.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return """
                   Utility that can export the ASD/CDC mapping of rules to an Office file.
                   Supported formats are: xlsx, xls, xlsm, csv and ods.""";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .desc("Name of the mapping file to generate. Its extension must be recognized and supported.")
                                    .hasArg()
                                    .required()
                                    .build());
            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.output = getValueAsFile(cl, OUTPUT);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException, SQLException {
            AsdCdcMappingExporter.execute(margs);
            return null;
        }
    }
}