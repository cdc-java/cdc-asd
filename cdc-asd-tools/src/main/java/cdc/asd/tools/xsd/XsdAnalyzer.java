package cdc.asd.tools.xsd;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.Config;
import cdc.asd.tools.xsd.XsdAnalyzer.MainArgs.Feature;
import cdc.graphs.EdgeDirection;
import cdc.gv.tools.GvEngine;
import cdc.gv.tools.GvFormat;
import cdc.gv.tools.GvToAny;
import cdc.issues.IssueSeverity;
import cdc.issues.IssuesCollector;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.locations.TextFileLocation;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.events.ProgressController;
import cdc.util.files.SearchPath;
import cdc.validation.checkers.defaults.MatchesPattern;

/**
 * Utility that loads a set of XSD and checks them:
 * <ul>
 * <li>global types and groups that are not used.
 * <li>non compliant names.
 * </ul>
 *
 * @author Damien Carbonne
 */
public final class XsdAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger(XsdAnalyzer.class);
    private static final MatchesPattern CASE_MATCHER = new MatchesPattern("[a-z].*");

    public static class MainArgs {
        /** Basename of generated files. */
        public String basename;
        /** List of schemas to read. Order matters. */
        public final List<File> schemas = new ArrayList<>();
        /** (namespace, prefix) map. */
        public final Map<String, String> namespaceToPrefix = new HashMap<>();
        /** Output directory that will contain generated files. */
        public File outputDir;
        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();
        /** Paths where Graphviz layout engines can be found. */
        public SearchPath paths = new SearchPath();
        /** Set of excluded QNames. */
        public final Set<String> excludedNodeQNames = new HashSet<>();

        public enum Feature implements OptionEnum {
            VERBOSE("verbose", "Prints messages."),
            COMPRESS("compress", "Generates a compressed graph that only contains first level nodes."),
            NO_XSD("no-xsd", "Removes XSD nodes."),
            DIRECTED("directed", "Generates directed graph. This may take a long time (several minutes)."),
            UNDIRECTED("undirected", "Generates undirected graph (default).");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }

        /**
         * @param node The node.
         * @return A QName with prefix instead of namespace.
         */
        public String getQName(XsdNode node) {
            if (!namespaceToPrefix.containsKey(node.getNamespace())) {
                LOGGER.warn("No prefix for {}", node.getNamespace());
            }
            return node.getQName(ns -> namespaceToPrefix.getOrDefault(node.getNamespace(), node.getNamespace()));
        }
    }

    private final MainArgs margs;
    /** The generated graph. */
    private final XsdGraph graph = new XsdGraph();

    private final IssuesCollector<XsdIssue> issues = new IssuesCollector<>();

    private XsdAnalyzer(MainArgs margs) {
        this.margs = margs;
    }

    private void execute() throws IOException {
        loadSchemas();
        checkUnusedGlobalItems();
        checkNamesCase();
        saveIssues();
        generateGv();
    }

    private void loadSchemas() throws IOException {
        // Load schemas into graph
        XsdGraphLoader.load(graph,
                            margs.schemas,
                            margs.features.isEnabled(MainArgs.Feature.VERBOSE));
    }

    private void checkUnusedGlobalItems() {
        LOGGER.info("Check unused global items");
        for (final XsdNode node : graph.getNodes()) {
            if ((graph.getEdgesCount(node, EdgeDirection.INGOING)) == 0 && node.getType() != XsdNodeType.ELEMENT) {
                issues.issue(XsdIssue.builder()
                                     .project(margs.basename)
                                     .name(XsdIssueType.UNUSED_GLOBAL_ITEM)
                                     .description("Global " + node.getType() + " '" + margs.getQName(node)
                                             + "' is declared but not used.")
                                     .severity(node.getType().isDeclaredType() ? IssueSeverity.MAJOR : IssueSeverity.MINOR)
                                     .addLocation(TextFileLocation.builder().systemId(node.getSystemId()).build())
                                     .build());
                LOGGER.warn("   Unused global item {}", node);
            }
        }
    }

    private void checkNamesCase() {
        LOGGER.info("Check names case");
        for (final XsdNode node : graph.getNodes()) {
            switch (node.getType()) {
            case ATTRIBUTE:
            case ELEMENT:
            case COMPLEX_TYPE:
            case SIMPLE_TYPE:
                final String name = node.getLocalName();
                final boolean matches = CASE_MATCHER.test(name);
                if (!matches) {
                    final String explanation = CASE_MATCHER.explain(false, name);
                    issues.issue(XsdIssue.builder()
                                         .project(margs.basename)
                                         .name(XsdIssueType.NON_COMPLIANT_NAME_CASE)
                                         .description(node.getType() + " '" + margs.getQName(node)
                                                 + "' name case is not compliant.\n" + explanation)
                                         .addLocation(TextFileLocation.builder().systemId(node.getSystemId()).build())
                                         .build());
                    LOGGER.warn("   Non compliant name case {}", node);
                }
                break;
            default:
                break;
            }
        }
    }

    private void saveIssues() throws IOException {
        final File file = new File(margs.outputDir, margs.basename.toLowerCase() + "-xsd-issues.xlsx");
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info("Save issues to {}", file);
        }
        IssuesWriter.save(issues.getIssues(),
                          file,
                          ProgressController.VOID,
                          IssuesIoFactoryFeatures.UTC_BEST);
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info("Done");
        }
    }

    private void generateGv() throws IOException {
        // Generate GraphViz file
        final File gv = new File(margs.outputDir, margs.basename.toLowerCase() + "-xsd.gv");
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info("Generate {}", gv);
        }

        final XsdGvGenerator generator = new XsdGvGenerator(margs);
        if (margs.features.isEnabled(MainArgs.Feature.COMPRESS)) {
            generator.generate(XsdGraphCompressor.compress(graph), gv);
        } else {
            generator.generate(graph, gv);
        }

        // Convert GraphViz file to images
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info("Done");

            LOGGER.info("Generate images");
        }
        final GvToAny.MainArgs imargs = new GvToAny.MainArgs();
        imargs.paths = margs.paths;
        if (margs.features.isEnabled(MainArgs.Feature.DIRECTED)) {
            imargs.engine = GvEngine.DOT;
        } else {
            imargs.engine = GvEngine.SFDP;
        }
        imargs.formats.add(GvFormat.SVG);
        imargs.formats.add(GvFormat.PDF);
        imargs.input = gv;
        imargs.outputDir = margs.outputDir;
        imargs.setEnabled(GvToAny.MainArgs.Feature.VERBOSE, margs.features.isEnabled(MainArgs.Feature.VERBOSE));
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            imargs.args.add("-v");
        }

        GvToAny.execute(imargs);
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info("Done");
        }
    }

    public static void execute(MainArgs margs) throws IOException {
        final XsdAnalyzer instance = new XsdAnalyzer(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String NAMESPACE_PREFIX_SEPARATOR = "::";
        private static final String BASENAME = "basename";
        private static final String SCHEMA = "schema";
        private static final String EXCLUDE_QNAME = "exclude-qname";
        private static final String NAMESPACE_PREFIX = "namespace-prefix";

        public MainSupport() {
            super(XsdAnalyzer.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return """
                    Utility that analyzes a set of S-Series schemas and checks them:
                    - global types and groups that are not used.
                    - non-compliant element and type names.
                    A graph (PDF and SVG) is generated using GraphViz.""";
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(BASENAME)
                                    .desc("Base name of generated files.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(SCHEMA)
                                    .desc("Name(s) of the input schema(s).")
                                    .hasArgs()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Name of the output directory.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PATH)
                                    .desc("Directory(ies) where external binaries (GraphViz) can be found.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(EXCLUDE_QNAME)
                                    .desc("QName(s) of the nodes that must be excluded. Use prefixes defined with '--"
                                            + NAMESPACE_PREFIX + "' option.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(NAMESPACE_PREFIX)
                                    .desc("Prefix(es) that should be used to represent namespace(s). Has the form: namespace"
                                            + NAMESPACE_PREFIX_SEPARATOR + "prefix")
                                    .hasArgs()
                                    .build());
            addNoArgOptions(options, MainArgs.Feature.class);
            createGroup(options, Feature.DIRECTED, Feature.UNDIRECTED);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.basename = getValueAsString(cl, BASENAME, "xxx");
            for (final String schema : cl.getOptionValues(SCHEMA)) {
                final File file = getReferencePath().resolve(new File(schema).toPath()).toFile();
                IS_FILE.check(cl, SCHEMA, file);
                margs.schemas.add(file);
            }
            margs.outputDir = getValueAsResolvedFile(cl, OUTPUT_DIR);
            if (cl.hasOption(PATH)) {
                margs.paths = new SearchPath(cl.getOptionValues(PATH));
            }
            fillValues(cl, EXCLUDE_QNAME, margs.excludedNodeQNames);

            if (cl.getOptionValues(NAMESPACE_PREFIX) != null) {
                for (final String s : cl.getOptionValues(NAMESPACE_PREFIX)) {
                    final String namespace = getPart(s, NAMESPACE_PREFIX_SEPARATOR, 0);
                    final String prefix = getPart(s, NAMESPACE_PREFIX_SEPARATOR, 1);
                    margs.namespaceToPrefix.put(namespace, prefix);
                }
            }

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException {
            XsdAnalyzer.execute(margs);
            return null;
        }
    }
}