package cdc.asd.tools.xsd;

import cdc.issues.Issue;

public class XsdIssue extends Issue {
    public static final String DOMAIN = "S-Series";

    protected XsdIssue(Builder builder) {
        super(builder);
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder extends Issue.Builder<Builder> {
        Builder() {
            super();
        }

        @Override
        public Builder self() {
            return this;
        }

        protected Builder fix() {
            domain(DOMAIN);
            return this;
        }

        @Override
        public XsdIssue build() {
            return new XsdIssue(fix());
        }
    }
}