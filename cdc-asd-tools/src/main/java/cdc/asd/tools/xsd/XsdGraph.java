package cdc.asd.tools.xsd;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.graphs.impl.BasicLightGraph;
import cdc.util.lang.Checks;

public class XsdGraph extends BasicLightGraph<XsdNode, XsdEdge> {
    private static final Logger LOGGER = LogManager.getLogger(XsdGraph.class);
    private static final String QNAME = "qname";

    private final Map<String, XsdNode> types = new HashMap<>();
    private final Map<String, XsdNode> groups = new HashMap<>();
    private final Map<String, XsdNode> elements = new HashMap<>();

    private final Map<XsdNode, Integer> nodesToId = new HashMap<>();
    private final Map<Integer, XsdNode> idsToNode = new HashMap<>();

    public XsdGraph() {
        super(true, CollectionKind.LIST);
    }

    @SuppressWarnings("unchecked")
    @Override
    public XsdNode addNode(XsdNode node) {
        if (node.getDepth() == 0) {
            // Register global nodes
            if (node.getType() == XsdNodeType.COMPLEX_TYPE
                    || node.getType() == XsdNodeType.SIMPLE_TYPE
                    || node.getType() == XsdNodeType.EXTERNAL_TYPE) {
                // Global type
                types.put(node.getQName(), node);
            } else if (node.getType() == XsdNodeType.GROUP) {
                // Global group
                groups.put(node.getQName(), node);
            } else if (node.getType() == XsdNodeType.ELEMENT
                    || node.getType() == XsdNodeType.EXTERNAL_ELEMENT) {
                // Global element
                elements.put(node.getQName(), node);
            }
        }

        super.addNode(node);
        final Integer id = nodesToId.size() + 1;
        nodesToId.put(node, id);
        idsToNode.put(id, node);

        return node;
    }

    @Override
    public void removeNode(XsdNode node) {
        types.remove(node.getQName());
        groups.remove(node.getQName());
        final Integer id = nodesToId.remove(node);
        idsToNode.remove(id);
        super.removeNode(node);
    }

    public XsdNode addNode(String systemId,
                           List<XsdNode> context,
                           XsdNodeType type,
                           String namespace,
                           String name) {
        final XsdNode node = new XsdNode(systemId, context, type, namespace, name);
        addNode(node);
        return node;
    }

    public XsdNode getType(String qname) {
        Checks.isNotNull(qname, QNAME);

        final XsdNode node = types.get(qname);
        if (node == null) {
            LOGGER.warn("Can not find type {}", qname);
        }
        return node;
    }

    public boolean hasType(String qname) {
        Checks.isNotNull(qname, QNAME);

        return types.containsKey(qname);
    }

    public XsdNode getGroup(String qname) {
        Checks.isNotNull(qname, QNAME);

        final XsdNode node = groups.get(qname);
        if (node == null) {
            LOGGER.warn("Can not find group {}", qname);
        }
        return node;
    }

    public boolean hasGroup(String qname) {
        Checks.isNotNull(qname, QNAME);

        return groups.containsKey(qname);
    }

    public XsdNode getElement(String qname) {
        Checks.isNotNull(qname, QNAME);

        final XsdNode node = elements.get(qname);
        if (node == null) {
            LOGGER.warn("Can not find element {}", qname);
        }
        return node;
    }

    public boolean hasElement(String qname) {
        Checks.isNotNull(qname, QNAME);

        return elements.containsKey(qname);
    }

    public XsdEdge addEdge(XsdEdgeType type,
                           XsdNode source,
                           XsdNode target) {
        final XsdEdge edge = new XsdEdge(type, source, target);
        return addEdge(edge);
    }

    public Integer getId(XsdNode node) {
        return nodesToId.get(node);
    }

    public XsdNode getNode(int id) {
        return idsToNode.get(id);
    }
}