package cdc.asd.tools.xsd;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.graphs.EdgeDirection;
import cdc.graphs.core.GraphTraverser;
import cdc.util.function.Evaluation;

final class XsdGraphCompressor {
    private XsdGraphCompressor() {
    }

    private static record NodeNode(XsdNode node0,
                                   XsdNode node1) {
    }

    public static XsdGraph compress(XsdGraph graph) {
        final XsdGraph g = new XsdGraph();

        final List<XsdNode> nodes = new ArrayList<>();
        for (final XsdNode node : graph.getNodes()) {
            if (node.getDepth() == 0) {
                g.addNode(node);
                nodes.add(node);
            }
        }

        final GraphTraverser<XsdNode, XsdEdge> traverser = new GraphTraverser<>(graph);
        final Set<NodeNode> set = new HashSet<>();
        for (final XsdNode node : nodes) {
            traverser.traverseDepthFirstPre(node,
                                            EdgeDirection.OUTGOING,
                                            n -> {
                                                if (n != node && n.getDepth() == 0) {
                                                    set.add(new NodeNode(node, n));
                                                }
                                            },
                                            n -> n != node && n.getDepth() == 0 ? Evaluation.PRUNE : Evaluation.CONTINUE);
        }

        for (final NodeNode t : set) {
            g.addEdge(XsdEdgeType.DEPENDS, t.node0(), t.node1());
        }

        return g;
    }
}