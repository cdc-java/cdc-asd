package cdc.asd.tools.xsd;

public enum XsdNodeType {
    ELEMENT,
    ATTRIBUTE,
    COMPLEX_TYPE,
    SIMPLE_TYPE,
    GROUP,
    EXTERNAL_TYPE,
    EXTERNAL_ELEMENT;

    public char getSeparator() {
        if (this == ATTRIBUTE) {
            return '@';
        } else {
            return '/';
        }
    }

    public boolean isDeclaredType() {
        return this == COMPLEX_TYPE || this == SIMPLE_TYPE;
    }
}