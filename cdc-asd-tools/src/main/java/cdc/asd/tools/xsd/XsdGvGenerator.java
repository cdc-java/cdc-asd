package cdc.asd.tools.xsd;

import java.io.File;
import java.io.IOException;

import cdc.graphs.EdgeDirection;
import cdc.gv.GvWriter;
import cdc.gv.atts.GvArrowType;
import cdc.gv.atts.GvDirType;
import cdc.gv.atts.GvEdgeAttributes;
import cdc.gv.atts.GvEdgeStyle;
import cdc.gv.atts.GvGraphAttributes;
import cdc.gv.atts.GvLabelLoc;
import cdc.gv.atts.GvNodeAttributes;
import cdc.gv.atts.GvNodeShape;
import cdc.gv.atts.GvNodeStyle;
import cdc.gv.atts.GvOverlap;
import cdc.gv.colors.GvColor;
import cdc.gv.colors.GvX11Colors;
import cdc.util.function.IterableUtils;
import cdc.util.lang.Checks;

/**
 * Class used to generate a GraphViz file from an {@link XsdGraph}.
 *
 * @author Damien Carbonne
 */
class XsdGvGenerator {
    private static final String XSD = "http://www.w3.org/2001/XMLSchema";
    private final XsdAnalyzer.MainArgs margs;

    XsdGvGenerator(XsdAnalyzer.MainArgs margs) {
        this.margs = margs;
    }

    private static String getId(XsdGraph graph,
                                XsdNode node) {
        return "N" + graph.getId(node);
    }

    private static boolean isRootNode(XsdNode node) {
        return node.getDepth() == 0;
    }

    private static boolean isDubious(XsdGraph graph,
                                     XsdNode node) {
        final int ingoings = graph.getEdgesCount(node, EdgeDirection.INGOING);
        return ingoings == 0 && node.getType() != XsdNodeType.ELEMENT;
    }

    private static boolean isRootElement(XsdGraph graph,
                                         XsdNode node) {
        return node.getType() == XsdNodeType.ELEMENT && isRootNode(node);
    }

    private boolean accept(XsdNode node) {
        return !margs.excludedNodeQNames.contains(margs.getQName(node))
                && (!margs.features.isEnabled(XsdAnalyzer.MainArgs.Feature.NO_XSD)
                        || !XSD.equals(node.getNamespace()));
    }

    private boolean accept(XsdEdge edge) {
        return accept(edge.getSource()) && accept(edge.getTarget());
    }

    public void generate(XsdGraph graph,
                         File file) throws IOException {
        try (final GvWriter w = new GvWriter(file)) {
            w.beginGraph("ASD", margs.features.isEnabled(XsdAnalyzer.MainArgs.Feature.DIRECTED), getAttributes());
            for (int id = 1; id <= graph.getNodesCount(); id++) {
                final XsdNode node = graph.getNode(id);
                Checks.assertTrue(node != null, "No node with id {}", id);
                if (accept(node)) {
                    w.addNode(getId(graph, node),
                              getAttributes(graph, node));
                }
            }
            for (final XsdEdge edge : IterableUtils.toSortedList(graph.getEdges(), XsdEdge.comparator(graph))) {
                if (accept(edge)) {
                    w.addEdge(getId(graph, edge.getSource()),
                              getId(graph, edge.getTarget()),
                              getAttributes(edge));
                }
            }
            w.endGraph();
            w.flush();
        }
    }

    private GvGraphAttributes getAttributes() {
        final GvGraphAttributes atts = new GvGraphAttributes();
        atts.setOverlap(GvOverlap.PRISM)
            .setLabel(margs.basename)
            .setLabelLoc(GvLabelLoc.TOP)
            .setFontSize(80.0)
            .setFontName("Arial bold");
        return atts;
    }

    private static GvColor getFillColor(XsdNodeType type) {
        switch (type) {
        case ATTRIBUTE:
            return new GvColor(200, 200, 255);
        case COMPLEX_TYPE:
            return new GvColor(255, 255, 100);
        case ELEMENT:
            return new GvColor(255, 170, 255);
        case GROUP:
            return new GvColor(200, 170, 255);
        case SIMPLE_TYPE:
            return new GvColor(255, 200, 100);
        case EXTERNAL_TYPE:
        case EXTERNAL_ELEMENT:
            return new GvColor(180, 220, 80);
        default:
            return GvX11Colors.WHITE;
        }
    }

    private static GvColor getColor(XsdGraph graph,
                                    XsdNode node) {
        if (isDubious(graph, node)) {
            return GvX11Colors.RED;
        } else {
            return GvX11Colors.BLACK;
        }
    }

    private static GvColor getFontColor(XsdGraph graph,
                                        XsdNode node) {
        return getColor(graph, node);
    }

    private static double getFontSize(XsdGraph graph,
                                      XsdNode node) {
        final double k = isDubious(graph, node) || isRootElement(graph, node) ? 2.5 : 1.0;
        switch (node.getType()) {
        case ATTRIBUTE:
            return 6.0 * k;
        case COMPLEX_TYPE:
        case SIMPLE_TYPE:
        case EXTERNAL_TYPE:
        case EXTERNAL_ELEMENT:
        case GROUP:
            return 14.0 * k;
        case ELEMENT:
            if (isRootNode(node)) {
                return 24.0 * k;
            } else {
                return 10.0 * k;
            }
        default:
            return 8.0 * k;
        }
    }

    private static double getPenWidth(XsdGraph graph,
                                      XsdNode node) {
        if (isRootNode(node)) {
            return 4.0;
        } else {
            return 1.0;
        }
    }

    private GvNodeAttributes getAttributes(XsdGraph graph,
                                           XsdNode node) {
        final GvNodeAttributes atts = new GvNodeAttributes();
        atts.setShape(GvNodeShape.RECTANGLE)
            .setStyle(GvNodeStyle.FILLED)
            .setFillColor(getFillColor(node.getType()))
            .setColor(getColor(graph, node))
            .setMargin(0.1, 0.05)
            .setHeight(0.1)
            .setWidth(0.1)
            .setFontSize(getFontSize(graph, node))
            .setFontColor(getFontColor(graph, node))
            .setPenWidth(getPenWidth(graph, node))
            .setLabel(margs.getQName(node));
        return atts;
    }

    private static GvColor getColor(XsdEdgeType type) {
        switch (type) {
        case COMPOSED:
            return new GvColor(0, 0, 0, 128);
        case TYPED:
            return new GvColor(100, 100, 100, 64);
        case DEPENDS:
            return new GvColor(100, 100, 100, 50);
        default:
            return GvX11Colors.RED;
        }
    }

    private static GvArrowType getArrowHead(XsdEdgeType type) {
        switch (type) {
        case COMPOSED:
            return GvArrowType.NONE;
        case TYPED:
        case DEPENDS:
            return GvArrowType.NORMAL;
        default:
            return GvArrowType.NONE;
        }
    }

    private static GvArrowType getArrowTail(XsdEdgeType type) {
        switch (type) {
        case COMPOSED:
            return GvArrowType.DIAMOND;
        case TYPED:
        case DEPENDS:
            return GvArrowType.NONE;
        default:
            return GvArrowType.NONE;
        }
    }

    private static GvEdgeStyle getStyle(XsdEdgeType type) {
        if (type == XsdEdgeType.DEPENDS) {
            return GvEdgeStyle.DASHED;
        } else {
            return GvEdgeStyle.SOLID;
        }
    }

    private static GvEdgeAttributes getAttributes(XsdEdge edge) {
        final GvEdgeAttributes atts = new GvEdgeAttributes();
        atts.setStyle(getStyle(edge.getType()))
            .setArrowHead(getArrowHead(edge.getType()))
            .setArrowTail(getArrowTail(edge.getType()))
            .setDir(GvDirType.BOTH)
            .setColor(getColor(edge.getType()));
        return atts;
    }
}