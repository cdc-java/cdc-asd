package cdc.asd.tools.xsd;

import cdc.issues.IssueSeverity;
import cdc.issues.IssueSeverityItem;

public enum XsdIssueType implements IssueSeverityItem {
    /** A global item is declared but is not used. */
    UNUSED_GLOBAL_ITEM(IssueSeverity.MAJOR),
    /** The name of an attribute, element, type is not compliant with adopted naming convention. */
    NON_COMPLIANT_NAME_CASE(IssueSeverity.MAJOR);

    private final IssueSeverity severity;

    private XsdIssueType(IssueSeverity severity) {
        this.severity = severity;
    }

    @Override
    public IssueSeverity getSeverity() {
        return severity;
    }
}