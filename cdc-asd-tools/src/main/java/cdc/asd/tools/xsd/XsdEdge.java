package cdc.asd.tools.xsd;

import java.util.Comparator;
import java.util.Objects;

import cdc.graphs.impl.BasicGraphEdge;

public class XsdEdge extends BasicGraphEdge<XsdNode> {
    private final XsdEdgeType type;

    public XsdEdge(XsdEdgeType type,
                   XsdNode source,
                   XsdNode target) {
        super(source, target);
        this.type = type;
    }

    public XsdEdgeType getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type,
                            getSource(),
                            getTarget());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof XsdEdge)) {
            return false;
        }
        final XsdEdge other = (XsdEdge) object;
        return Objects.equals(getType(), other.getType())
                && Objects.equals(getSource(), other.getSource())
                && Objects.equals(getTarget(), other.getTarget());
    }

    @Override
    public String toString() {
        return "[" + getType() + " " + getSource() + " -> " + getTarget() + "]";
    }

    public static Comparator<XsdEdge> comparator(XsdGraph graph) {
        Comparator<XsdEdge> cmp = Comparator.comparing(e -> graph.getId(e.getSource()));
        cmp = cmp.thenComparing(e -> graph.getId(e.getTarget()));
        cmp = cmp.thenComparing(XsdEdge::getType);
        return cmp;
    }
}