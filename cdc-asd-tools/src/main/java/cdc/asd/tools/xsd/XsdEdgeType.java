package cdc.asd.tools.xsd;

public enum XsdEdgeType {
    COMPOSED,
    TYPED,
    DEPENDS
}