package cdc.asd.tools.xsd;

import java.util.Comparator;
import java.util.List;
import java.util.function.UnaryOperator;

import cdc.util.lang.Checks;

public class XsdNode {
    public static final Comparator<XsdNode> TYPE_NAME_COMPARATOR =
            Comparator.comparing(XsdNode::getType)
                      .thenComparing(XsdNode::getLocalName);

    private final String systemId;
    private final XsdNode[] context;
    private final XsdNodeType type;
    private final String namespace;
    private final String localName;
    private final String path;

    XsdNode(String systemId,
            List<XsdNode> context,
            XsdNodeType type,
            String namespace,
            String localName) {
        Checks.isTrue(localName.indexOf(':') < 0, "Invalid local name '{}'", localName);

        this.systemId = systemId;
        this.context = context.toArray(new XsdNode[context.size()]);
        this.type = type;
        this.namespace = namespace;
        this.localName = localName;

        final StringBuilder builder = new StringBuilder();
        for (final XsdNode node : context) {
            builder.append(node.getType().getSeparator());
            builder.append(node.getLocalName());
        }
        builder.append(getType().getSeparator());
        builder.append(localName);
        this.path = builder.toString();
    }

    public String getSystemId() {
        return systemId;
    }

    public XsdNodeType getType() {
        return type;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getLocalName() {
        return localName;
    }

    public String getQName() {
        return namespace == null
                ? localName
                : namespace + ":" + localName;
    }

    public String getQName(UnaryOperator<String> namespaceConverter) {
        return namespace == null
                ? localName
                : namespaceConverter.apply(namespace) + ":" + localName;
    }

    public String getPath() {
        return path;
    }

    public int getDepth() {
        return context.length;
    }

    @Override
    public String toString() {
        return getType() + " " + getQName();
    }
}