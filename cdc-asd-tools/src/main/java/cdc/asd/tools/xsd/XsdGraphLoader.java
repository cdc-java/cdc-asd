package cdc.asd.tools.xsd;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.data.Attribute;
import cdc.io.data.Element;
import cdc.io.data.xml.XmlDataReader;

public final class XsdGraphLoader {
    private static final Logger LOGGER = LogManager.getLogger(XsdGraphLoader.class);

    private static final String NAME = "name";
    private static final String REF = "ref";
    private static final String TYPE = "type";
    private static final String ATTRIBUTE = "attribute";
    private static final String COMPLEX_TYPE = "complexType";
    private static final String ELEMENT = "element";
    private static final String GROUP = "group";
    private static final String SIMPLE_TYPE = "simpleType";
    private static final String RESTRICTION = "restriction";
    private static final String BASE = "base";
    private static final String UNION = "union";
    private static final String MEMBER_TYPES = "memberTypes";
    private static final String SCHEMA = "schema";
    private static final String XMLNS = "xmlns";
    private static final String TARGET_NAMESPACE = "targetNamespace";

    private static final Set<String> SILENTLY_IGNORED =
            Set.of("annotation",
                   "appinfo",
                   "assert",
                   "choice",
                   "description",
                   "documentation",
                   "enumeration",
                   "import",
                   "include",
                   "minLength",
                   "note",
                   "pattern",
                   "schema",
                   "sequence",
                   "source");

    private final List<File> schemas = new ArrayList<>();
    private final boolean verbose;
    private final XsdGraph graph;
    /** (node, qname) of referenced types that must be resolved once all schema are loaded. */
    private final List<NodeQName> lateTypes = new ArrayList<>();
    /** (node, qname) of referenced groups that must be resolved once all schema are loaded. */
    private final List<NodeQName> lateGroups = new ArrayList<>();
    /** (node, qname) of referenced elements that must be resolved once all schema are loaded. */
    private final List<NodeQName> lateElements = new ArrayList<>();
    /** Default namespace of currently loaded schema. */
    private String defaultNS;
    /** Target namespace of currently loaded schema. */
    private String targetNS;
    /** (prefix, namespace) mapping in currently loaded schema. */
    private final Map<String, String> namespaces = new HashMap<>();

    private static record NodeQName(XsdNode node,
                                    String qname) {
    }

    private XsdGraphLoader(XsdGraph graph,
                           List<File> schemas,
                           boolean verbose) {
        this.graph = graph;
        this.schemas.addAll(schemas);
        this.verbose = verbose;
    }

    public static void load(XsdGraph graph,
                            List<File> schemas,
                            boolean verbose) throws IOException {
        final XsdGraphLoader loader = new XsdGraphLoader(graph, schemas, verbose);

        loader.load();
    }

    private void load() throws IOException {
        for (final File schema : schemas) {
            if (verbose) {
                LOGGER.info("Load {}", schema);
            }
            final Element root = XmlDataReader.loadRoot(schema);

            final List<XsdNode> context = new ArrayList<>();
            this.namespaces.clear();
            this.defaultNS = null;
            this.targetNS = null;
            parse(schema.getName(), root, context);
        }

        processLates();
    }

    /**
     * @param qname The QName.
     * @return The local name of {@code qname}.
     */
    private static String getLocalName(String qname) {
        final int pos = qname.lastIndexOf(':');
        if (pos >= 0) {
            return qname.substring(pos + 1);
        } else {
            return qname;
        }
    }

    /**
     * @param qname The QName.
     * @return The namespace or prefix (the part before local name) of {@code qname}.
     */
    private static String getPrefixOrNamespace(String qname) {
        final int pos = qname.lastIndexOf(':');
        if (pos >= 0) {
            return qname.substring(0, pos);
        } else {
            return null;
        }
    }

    /**
     * @param qname The QName.
     * @return The namespace of {@code qname}.
     */
    private String resolveNamespace(String qname) {
        final String ns = getPrefixOrNamespace(qname);
        if (ns == null) {
            return targetNS;
        } else {
            return namespaces.get(ns);
        }
    }

    /**
     * @param qname The QName using prefix
     * @return A QName where prefix is replaced by namespace.
     */
    private String resolveQName(String qname) {
        return resolveNamespace(qname) + ":" + getLocalName(qname);
    }

    /**
     * Recursively parse a data element.
     * <p>
     * This is tailored to fit features currently used by S-Series.
     *
     * @param systemId The systemIf of the resource containing the element.
     * @param element The element to parse.
     * @param context The stack of context nodes.
     */
    private void parse(String systemId,
                       Element element,
                       List<XsdNode> context) {
        final XsdNode newNode;
        final String localName = getLocalName(element.getName());
        if (localName.equals(SCHEMA)) {
            defaultNS = element.getAttributeValue(XMLNS);
            targetNS = element.getAttributeValue(TARGET_NAMESPACE);
            for (final Attribute att : element.getAttributes()) {
                if (att.getName().startsWith(XMLNS + ":")) {
                    namespaces.put(getLocalName(att.getName()), att.getValue());
                }
            }
            LOGGER.debug("default ns: {}", defaultNS);
            LOGGER.debug("target ns: {}", targetNS);
            LOGGER.debug("ns: {}", namespaces);
            newNode = null;
        } else if (localName.equals(COMPLEX_TYPE)) {
            if (element.hasAttribute(NAME)) {
                // Found a global complex type
                newNode = graph.addNode(systemId,
                                        context,
                                        XsdNodeType.COMPLEX_TYPE,
                                        targetNS,
                                        getLocalName(element.getAttributeValue(NAME)));
            } else {
                // Found an anonymous complex type
                // Ignore it
                newNode = null;
            }
        } else if (localName.equals(SIMPLE_TYPE)) {
            if (element.hasAttribute(NAME)) {
                // Found a global simple type
                newNode = graph.addNode(systemId,
                                        context,
                                        XsdNodeType.SIMPLE_TYPE,
                                        targetNS,
                                        getLocalName(element.getAttributeValue(NAME)));
            } else {
                // Found an anonymous simple type
                // Ignore it
                newNode = null;
            }
        } else if (localName.equals(ELEMENT)) {
            if (element.hasAttribute(NAME)) {
                // Found the declaration of a named element, global or local
                newNode = graph.addNode(systemId,
                                        context,
                                        XsdNodeType.ELEMENT,
                                        targetNS,
                                        getLocalName(element.getAttributeValue(NAME)));
                if (element.hasAttribute(TYPE)) {
                    lateTypes.add(new NodeQName(newNode,
                                                resolveQName(element.getAttributeValue(TYPE))));
                }
            } else if (element.hasAttribute(REF)) {
                // Found the declaration of a local element referencing a global one
                lateElements.add(new NodeQName(context.get(context.size() - 1),
                                               resolveQName(element.getAttributeValue(REF))));
                newNode = null;
            } else {
                newNode = null;
                LOGGER.warn("TODO {}", element);
            }
        } else if (localName.equals(ATTRIBUTE)) {
            // Found the declaration of a named local attribute
            // Currently S-Series don't define global attributes
            newNode = graph.addNode(systemId,
                                    context,
                                    XsdNodeType.ATTRIBUTE,
                                    targetNS,
                                    getLocalName(element.getAttributeValue(NAME)));
            if (element.hasAttribute(TYPE)) {
                lateTypes.add(new NodeQName(newNode,
                                            resolveQName(element.getAttributeValue(TYPE))));
            }
        } else if (localName.equals(GROUP)) {
            if (element.hasAttribute(NAME)) {
                // Found the declaration of a global group
                newNode = graph.addNode(systemId,
                                        context,
                                        XsdNodeType.GROUP,
                                        targetNS,
                                        getLocalName(element.getAttributeValue(NAME)));
            } else if (element.hasAttribute(REF)) {
                // Found the declaration of a local group referencing a global one
                lateGroups.add(new NodeQName(context.get(context.size() - 1),
                                             resolveQName(element.getAttributeValue(REF))));
                newNode = null;
            } else {
                newNode = null;
            }
        } else if (localName.equals(RESTRICTION)) {
            newNode = null;
            if (element.hasAttribute(BASE)) {
                lateTypes.add(new NodeQName(context.get(context.size() - 1),
                                            resolveQName(element.getAttributeValue(BASE))));
            }
        } else if (localName.equals(UNION)) {
            newNode = null;
            for (final String type : element.getAttributeValue(MEMBER_TYPES).split(" ")) {
                lateTypes.add(new NodeQName(context.get(context.size() - 1),
                                            resolveQName(type)));
            }
        } else {
            if (!SILENTLY_IGNORED.contains(localName)) {
                LOGGER.warn("Ignore {}", element);
            }
            newNode = null;
        }

        if (newNode != null) {
            if (!context.isEmpty()) {
                final XsdNode last = context.get(context.size() - 1);
                graph.addEdge(XsdEdgeType.COMPOSED, last, newNode);
            }
            context.add(newNode);
        }
        for (final Element child : element.getChildren(Element.class)) {
            parse(systemId, child, context);
        }
        if (newNode != null) {
            context.remove(context.size() - 1);
        }
    }

    private void processLates() {
        // Process late types
        for (final NodeQName late : lateTypes) {
            final XsdNode source = late.node();
            final String qname = late.qname();
            LOGGER.debug("Late type {} {}", source, qname);
            if (!graph.hasType(qname)) {
                graph.addNode(source.getSystemId(),
                              Collections.emptyList(),
                              XsdNodeType.EXTERNAL_TYPE,
                              getPrefixOrNamespace(qname),
                              getLocalName(qname));
            }
            final XsdNode target = graph.getType(qname);
            graph.addEdge(XsdEdgeType.TYPED, source, target);
        }

        // Process late groups
        for (final NodeQName late : lateGroups) {
            final XsdNode source = late.node();
            final String qname = late.qname();
            LOGGER.debug("Late group {} {}", source, qname);
            final XsdNode target = graph.getGroup(qname);
            if (target != null) {
                graph.addEdge(XsdEdgeType.COMPOSED,
                              source,
                              target);
            }
        }

        // Process late elements
        for (final NodeQName late : lateElements) {
            final XsdNode source = late.node();
            final String qname = late.qname();
            LOGGER.debug("Late element {} {}", source, qname);
            if (!graph.hasElement(qname)) {
                graph.addNode(source.getSystemId(),
                              Collections.emptyList(),
                              XsdNodeType.EXTERNAL_ELEMENT,
                              getPrefixOrNamespace(qname),
                              getLocalName(qname));
            }
            final XsdNode target = graph.getElement(qname);
            graph.addEdge(XsdEdgeType.COMPOSED, source, target);
        }
    }
}