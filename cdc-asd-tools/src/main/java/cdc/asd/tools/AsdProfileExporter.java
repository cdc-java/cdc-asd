package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.checks.AsdProfile;
import cdc.issues.core.io.MdProfileIo;
import cdc.issues.io.ProfileFormat;
import cdc.issues.io.ProfileIo;
import cdc.issues.io.ProfileIoFeatures;
import cdc.mf.Config;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.time.Chronometer;

public final class AsdProfileExporter {
    private static final Logger LOGGER = LogManager.getLogger(AsdProfileExporter.class);
    /** Pattern of sections that must be surrounded by ``. */
    private static final Pattern EM_PATTERN = Pattern.compile("(<<[a-zA-Z0-9_ ]*>>|\\[[<>a-zA-Z0-9_\\. ]*\\])");
    private static final Pattern ASD_RULE_PATTERN = Pattern.compile("(R[A-Z]{2}\\d{2})");
    private static final Pattern PATTERN_START = Pattern.compile("pattern: ");
    private static final Pattern PATTERN_END_NL = Pattern.compile("(pattern: .*)\\R");
    private static final Pattern PATTERN_END = Pattern.compile("(pattern: .*)$");

    private final MainArgs margs;

    public static class MainArgs {
        /** Name of the generated file. */
        public File output;
        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            SHOW_EMPTY_SECTIONS("show-empty-sections", "If enabled, empty sections are exported."),
            SHOW_RULES_SEVERITY("show-rules-severities", "If enabled, rules severities are exported."),
            SHOW_RULES_ENABLING("show-rules-enabling", "If enabled, rules enabling is exported."),
            SHOW_RULES_DOMAIN("show-rules-domain", "If enabled, rules domain is exported."),
            PROFILE("profile", "Generate the profile (default)."),
            PROFILE_CONFIG("profile-config", "Generate the profile config."),
            VERBOSE("verbose", "Print messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdProfileExporter(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private static String convertMd(String s) {
        s = MdProfileIo.wrap(s, ASD_RULE_PATTERN, "**", "**");
        s = MdProfileIo.wrap(s, EM_PATTERN, "`", "`");
        s = MdProfileIo.wrap(s, PATTERN_END_NL, "", "`\n");
        s = MdProfileIo.wrap(s, PATTERN_END, "", "`");
        s = MdProfileIo.replace(s, PATTERN_START, "pattern: `");
        s = MdProfileIo.mdReplaceNL(s);
        return s;
    }

    private void execute() throws IOException {
        final Chronometer chrono = new Chronometer();

        final boolean exportConfig = margs.features.contains(MainArgs.Feature.PROFILE_CONFIG);
        final String kind = exportConfig ? "profile config" : "profile";
        log("Exporting ASD {} to {}", kind, margs.output);
        chrono.start();

        // Save profile
        final ProfileFormat format = ProfileFormat.from(margs.output);
        final UnaryOperator<String> converter;
        if (format == ProfileFormat.MD) {
            converter = AsdProfileExporter::convertMd;
        } else {
            converter = UnaryOperator.identity();
        }
        final ProfileIoFeatures.Builder features =
                ProfileIoFeatures.builder()
                                 .hint(ProfileIoFeatures.Hint.PRETTY_PRINT)
                                 .itemConverter(converter);
        if (margs.features.contains(MainArgs.Feature.SHOW_EMPTY_SECTIONS)) {
            features.hint(ProfileIoFeatures.Hint.SHOW_EMPTY_SECTIONS);
        }
        if (margs.features.contains(MainArgs.Feature.SHOW_RULES_DOMAIN)) {
            features.hint(ProfileIoFeatures.Hint.SHOW_RULE_DOMAIN);
        }
        if (margs.features.contains(MainArgs.Feature.SHOW_RULES_ENABLING)) {
            features.hint(ProfileIoFeatures.Hint.SHOW_RULE_ENABLING);
        }
        if (margs.features.contains(MainArgs.Feature.SHOW_RULES_SEVERITY)) {
            features.hint(ProfileIoFeatures.Hint.SHOW_RULE_SEVERITY);
        }

        margs.output.getParentFile().mkdirs();
        if (exportConfig) {
            ProfileIo.save(features.build(), AsdProfile.PROFILE.getProfileConfig(), margs.output);
        } else {
            ProfileIo.save(features.build(), AsdProfile.PROFILE, margs.output);
        }
        chrono.suspend();
        log("Exported ASD {} ({})", kind, chrono);
    }

    public static void execute(MainArgs margs) throws IOException {
        final AsdProfileExporter instance = new AsdProfileExporter(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {

        public MainSupport() {
            super(AsdProfileExporter.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return """
                   Utility that can export the ASD Profile or Profile Config.
                   Supported formats for Profile are: md, html, xml, json, xlsx, xls, xlsm, csv and ods.
                   Supported formats for Profile Config are: xlsx, xls, xlsm, csv and ods.
                   Some of them cannot be loaded.""";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .desc("Name of the export file. Its extension must be recognized and supported.")
                                    .hasArg()
                                    .required()
                                    .build());
            addNoArgOptions(options, MainArgs.Feature.class);
            createGroup(options, MainArgs.Feature.PROFILE, MainArgs.Feature.PROFILE_CONFIG);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.output = getValueAsFile(cl, OUTPUT);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            final ProfileFormat format = ProfileFormat.from(margs.output);
            if (format == null || !format.isProfileExportFormat()) {
                throw new ParseException("Non recognized or supported format: " + margs.output.getName());
            }

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException, SQLException {
            AsdProfileExporter.execute(margs);
            return null;
        }
    }
}