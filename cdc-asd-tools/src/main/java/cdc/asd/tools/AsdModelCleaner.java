package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.Config;
import cdc.asd.model.wrappers.AsdUtils;
import cdc.asd.tools.cleaner.AsdModelCleanerImpl;
import cdc.mf.model.MfModel;
import cdc.mf.model.io.MfModelXmlIo;
import cdc.mf.transform.io.MfWorkbookTransformStatsIo;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.time.Chronometer;

public final class AsdModelCleaner {
    private static final Logger LOGGER = LogManager.getLogger(AsdModelCleaner.class);
    private final MainArgs margs;

    public static final String CLEANED = "-model-cleaned.xml";
    public static final String STATS = "-model-cleaned-stats.xlsx";

    public static class MainArgs {
        /** Input ASD MF XML model file. */
        public File modelFile;
        /** Output directory that will contain cleaned model and associated stats. */
        public File outputDir;
        /** Base name of generated files. */
        public String basename;
        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            ALL("all", "Enable all cleaning options."),
            ADD_MISSING_XML_NAME_TAGS("add-missing-xml-name-tags",
                                      "Add xmlName tags that are missing."),
            ADD_MISSING_XML_REF_NAME_TAGS("add-missing-xml-ref-name-tags",
                                          "Add xmlRefName tags that are missing."),
            DEDUPLICATE_NAMES("deduplicate-names",
                              "When several sibling packages, classes, interfaces have the same name, they are renamed to make their name unique."),
            FIX_TAG_NAME_CASE("fix-tag-name-case",
                              "When a tag name case is invalid, fix it."),
            REMOVE_EXTRA_SPACES("remove-extra-spaces",
                                "All leading and trailing spaces are removed from notes and tag values."),
            REMOVE_HTML_TAGS("remove-html-tags",
                             "All html tags are removed from notes."),
            REPLACE_HTML_ENTITIES("replace-html-entities",
                                  "Some html entities are replaced in notes."),
            SPLIT_REF_TAGS("split-ref-tags",
                           "All ref tags that contain several refs are split into tags that contain one ref."),

            CREATE_ENUMERATIONS("create-enumerations",
                                "Create enumerations for properties that have validValue tags."),
            CREATE_BASE_OBJECT_INHERITANCE("create-base-object-inheritance",
                                           "Make BaseObject inheritance explicit.\nDO NOT USE with checks."),
            VERBOSE("verbose", "Prints messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdModelCleaner(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private Set<AsdModelCleanerImpl.Hint> getHints() {
        final Set<AsdModelCleanerImpl.Hint> hints = EnumSet.noneOf(AsdModelCleanerImpl.Hint.class);
        if (margs.features.contains(MainArgs.Feature.ALL)) {
            Collections.addAll(hints, AsdModelCleanerImpl.Hint.values());
        } else {
            // WARNING: Make sure both enums use the same names
            // Otherwise an exception will be thrown by valueof
            for (final AsdModelCleanerImpl.Hint hint : AsdModelCleanerImpl.Hint.values()) {
                final MainArgs.Feature feature = MainArgs.Feature.valueOf(hint.name());
                if (margs.features.contains(feature)) {
                    hints.add(hint);
                }
            }
        }
        return hints;
    }

    private void execute() throws IOException {
        final Chronometer chrono = new Chronometer();

        margs.outputDir.mkdirs();

        log("Loading model {}", margs.modelFile);
        chrono.start();
        // Pass a factory as it may be used by transformers
        final MfModel model = MfModelXmlIo.load(margs.modelFile, AsdUtils.FACTORY);
        chrono.suspend();
        log("Loaded model ({})", chrono);

        final AsdModelCleanerImpl cleaner = new AsdModelCleanerImpl(getHints());

        log("Cleaning model");
        chrono.start();
        final MfModel cleanedModel = cleaner.transform(model);
        chrono.suspend();
        log("Cleaned model ({})", chrono);

        final File cleanedModelFile = new File(margs.outputDir, margs.basename + CLEANED);
        final File cleanedModelStatsFile = new File(margs.outputDir, margs.basename + STATS);

        log("Saving cleaned model {}", cleanedModelFile);
        chrono.start();
        MfModelXmlIo.save(cleanedModelFile, cleanedModel);
        chrono.suspend();
        log("Saved cleaned model ({})", chrono);

        log("Saving cleaned model stats {}", cleanedModelStatsFile);
        chrono.start();
        MfWorkbookTransformStatsIo.save(cleaner.getStats(),
                                        cleanedModelStatsFile,
                                        WorkbookWriterFeatures.STANDARD_FAST);
        chrono.suspend();
        log("Saved cleaned model stats ({})", chrono);
    }

    public static void execute(MainArgs margs) throws IOException {
        final AsdModelCleaner instance = new AsdModelCleaner(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String MODEL_FILE = "model";
        private static final String BASENAME = "basename";

        public MainSupport() {
            super(AsdModelCleaner.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return """
                   Utility that can clean (partially fix) an ASD MF XML model.
                   A stats file is also generated.""";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(MODEL_FILE)
                                    .desc("Mandatory name of the ASD MF XML model to clean.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Mandatory name of the output directory. If this directory does not exist, it is created.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(BASENAME)
                                    .desc("Mandatory base name of generated files (cleaned model and stats).")
                                    .hasArg()
                                    .required()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.modelFile = getValueAsResolvedFile(cl, MODEL_FILE, IS_FILE);
            margs.outputDir = getValueAsResolvedFile(cl, OUTPUT_DIR);
            margs.basename = getValueAsString(cl, BASENAME, "xxx");

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException {
            AsdModelCleaner.execute(margs);
            return null;
        }
    }
}