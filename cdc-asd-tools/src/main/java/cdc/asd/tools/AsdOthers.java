package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.AsdEaNaming;
import cdc.util.files.Resources;

/**
 * Utility used to run {@link AsdSuite} on all internal EAP models that are located
 * in <code>src/main/resources/others</code> directory.
 * <p>
 * These models <b>MUST NOT</b> be committed to gitlab.
 * <p>
 * Useful to check robustness of AsdSuite.
 */
public class AsdOthers {
    private static final Logger LOGGER = LogManager.getLogger(AsdOthers.class);
    private static final String LINE = "==================================================================================";

    private static void process(File eapFile,
                                String progress) throws IOException, SQLException, ExecutionException {
        LOGGER.info(LINE);
        LOGGER.info("process ({}) {}", progress, eapFile);
        LOGGER.info(LINE);
        final AsdEaNaming naming = new AsdEaNaming(eapFile);

        LOGGER.info("   {} {} {}", naming.getSpecName(), naming.getSpecIssueNumber(), naming.getModelIssueNumber());

        final AsdSuite.MainArgs margs = new AsdSuite.MainArgs();
        margs.eapFile = eapFile;
        margs.outputDir = new File("target", "others");
        margs.features.add(AsdSuite.MainArgs.Feature.APPEND_BASENAME);
        // margs.features.add(AsdSuite.MainArgs.Feature.VERBOSE);
        margs.features.add(AsdSuite.MainArgs.Feature.RUN_ALL);
        margs.features.add(AsdSuite.MainArgs.Feature.DUMP_NO_FORCE);
        margs.features.add(AsdSuite.MainArgs.Feature.MODEL_FIX_DIRECTION);
        margs.features.add(AsdSuite.MainArgs.Feature.CLEAN_ALL);
        margs.features.add(AsdSuite.MainArgs.Feature.HTML_FLAT_DIRS);
        margs.features.add(AsdSuite.MainArgs.Feature.HTML_IMG_SVG);
        margs.features.add(AsdSuite.MainArgs.Feature.HTML_SHOW_IDS);
        margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_MD);
        margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_HTML);
        margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_XLSX);
        margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_XML);
        margs.features.add(AsdSuite.MainArgs.Feature.S1000D_GLOSSARY);
        margs.features.add(AsdSuite.MainArgs.Feature.S1000D_SPEC);

        margs.htmlIgnoreModelOverviewPackage("/Builtin");
        margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Compound_Attributes.*");
        margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Base_Object_Definition.*");
        margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Primitives.*");
        margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Remark");
        margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Digital File");
        margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Security Classification");
        margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Applicability Statement");
        margs.htmlIgnoreModelOverviewPackage(".*/S5000F Specializations");

        margs.htmlRemoveNamePart("^CDM ");
        margs.htmlRemoveNamePart("^.* UoF ");
        margs.htmlRemoveNamePart("^S2000M ");
        margs.htmlRemoveNamePart("^S3000L ");
        margs.htmlRemoveNamePart("^S5000F ");
        margs.htmlRemoveNamePart("^S6000T ");
        margs.htmlRemoveNamePart("^SX000i ");

        margs.xsdNamespace = "todo";
        margs.xsdSpecification = "???";
        margs.xsdSpecificationUrl = "???";
        margs.xsdIssueNumber = "???";
        margs.xsdIssueDate = "???";
        margs.xsdXmlSchemaReleaseNumber = "???";
        margs.xsdXmlSchemaReleaseDate = "???";
        margs.xsdCopyright = AsdModelToXsd.DEFAULT_COPYRIGHT;
        margs.xsdValidValuesUnits = Resources.getResource(AsdModelToXsd.DEFAULT_VALID_VALUES_UNITS);
        margs.xsdValidValuesLibraries = Resources.getResource(AsdModelToXsd.DEFAULT_VALID_VALUES_LIBRARIES);
        margs.xsdValidValuesExternalLibraries = Resources.getResource(AsdModelToXsd.DEFAULT_VALID_VALUES_EXTERNAL_LIBRARIES);

        AsdSuite.execute(margs);
    }

    public static void main(String[] args) {
        // Grab EAP models
        final File inputDir = new File("src/main/resources/others");
        final List<File> eapFiles = Arrays.asList(inputDir.listFiles((d,
                                                                      n) -> n.toLowerCase().endsWith(".eap")));
        // final List<File> eapFiles = Arrays.asList(inputDir.listFiles((d,
        // n) -> n.endsWith("SX002D_3-0_Data_model_006-00.EAP")));
        // final List<File> eapFiles = Arrays.asList(inputDir.listFiles((d,
        // n) -> n.contains("SX002D")));
        eapFiles.sort(Comparator.comparing(File::getPath));

        final Map<File, Exception> failures = new HashMap<>();

        int index = 0;
        final int total = eapFiles.size();

        if (total == 0) {
            LOGGER.warn("No files.");
        }

        // Process them
        for (final File eapFile : eapFiles) {
            try {
                index++;
                final String progress = index + "/" + total;
                process(eapFile, progress);
            } catch (final Exception e) {
                LOGGER.catching(e);
                failures.put(eapFile, e);
            }
        }

        if (!failures.isEmpty()) {
            LOGGER.error(LINE);
            LOGGER.error("Failed for {}/{} files", failures.size(), eapFiles.size());
            LOGGER.error(LINE);
            for (final File eapFile : eapFiles) {
                if (failures.containsKey(eapFile)) {
                    LOGGER.error(LINE);
                    LOGGER.error("{}", eapFile);
                    LOGGER.error(LINE);
                    LOGGER.catching(failures.get(eapFile));
                }
            }
        }
        LOGGER.info("Done");
    }
}