package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.checks.AsdProfile;
import cdc.asd.checks.models.ModelChecker;
import cdc.asd.model.AsdEaNaming;
import cdc.asd.model.Config;
import cdc.asd.model.wrappers.AsdUtils;
import cdc.issues.IssuesCollector;
import cdc.issues.Metas;
import cdc.issues.checks.CheckStats;
import cdc.issues.checks.SnapshotManager;
import cdc.issues.checks.io.WorkbookCheckStatsIo;
import cdc.issues.impl.SnapshotDataImpl;
import cdc.issues.io.IssuesIoFactoryFeatures;
import cdc.issues.io.IssuesWriter;
import cdc.issues.io.OutSettings;
import cdc.issues.io.ProfileIo;
import cdc.issues.io.ProfileIoFeatures;
import cdc.issues.locations.Location;
import cdc.issues.rules.ProfileConfig;
import cdc.issues.rules.RuleId;
import cdc.mf.model.MfModel;
import cdc.mf.model.io.MfModelXmlIo;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.events.ProgressController;
import cdc.util.time.Chronometer;

public final class AsdModelChecker {
    private static final Logger LOGGER = LogManager.getLogger(AsdModelChecker.class);
    private final MainArgs margs;

    public static final String META_MODEL_FILE = "model-file";
    public static final String META_SPEC_NAME = "spec-name";
    public static final String META_SPEC_ISSUE_NUMBER = "spec-issue-number";
    public static final String META_MODEL_ISSUE_NUMBER = "model-issue-number";
    public static final String META_MODEL_ISSUE_QUALIFIER = "model-issue-qualifier";

    public static final String ISSUES = "-model-issues.xlsx";
    public static final String STATS = "-model-check-stats.xlsx";
    public static final String PROFILE = "-model-check-profile.xml";

    public static class MainArgs {
        /** Input MF ASD XML model file. */
        public File modelFile;
        /** Input reference MF ASD XML model file. */
        public File refModelFile;
        /** Output directory. */
        public File outputDir;
        /** Base name of read / generated files. */
        public String basename;

        /** Names of individually enabled rules. */
        public final Set<String> enabledRules = new HashSet<>();
        /** Names of individually disabled rules. */
        public final Set<String> disabledRules = new HashSet<>();

        /** Name of the profile config file to load. */
        public File profileConfigFile;

        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            ENABLE_ALL("enable-all",
                       "Enable all rules checkers. Use with --disable to disable individual rule checkers."),
            DISABLE_ALL("disable-all",
                        "Disable all rules checkers. Use with --enable to enable individual rule checkers."),
            FASTEST("fastest",
                    "Use options that are fast to generate output (default)."),
            BEST("best",
                 "Use options that generate best output."),
            VERBOSE("verbose",
                    "Print messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdModelChecker(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private void execute() throws IOException {
        final Chronometer chrono = new Chronometer();

        log("Loading model {}", margs.modelFile);
        chrono.start();
        final MfModel model = MfModelXmlIo.load(margs.modelFile, AsdUtils.FACTORY);
        chrono.suspend();
        log("Loaded model ({})", chrono);

        final MfModel refModel;
        if (margs.refModelFile != null) {
            log("Loading ref model {}", margs.refModelFile);
            chrono.start();
            refModel = MfModelXmlIo.load(margs.refModelFile, AsdUtils.FACTORY);
            chrono.suspend();
            log("Loaded ref model ({})", chrono);
        } else {
            refModel = null;
        }

        margs.outputDir.mkdirs();

        final AsdEaNaming naming = new AsdEaNaming(margs.modelFile);
        final String projectName = naming.getProjectName();
        final Metas projectMetas =
                Metas.builder()
                     .entry(META_MODEL_FILE, margs.modelFile.getName())
                     .entry(META_SPEC_NAME, naming.getSpecName())
                     .entry(META_SPEC_ISSUE_NUMBER, naming.getSpecIssueNumber())
                     .entry(META_MODEL_ISSUE_NUMBER, naming.getModelIssueNumber())
                     .entry(META_MODEL_ISSUE_QUALIFIER, naming.getModelIssueQualifier())
                     .build();
        final String snapshotName = naming.getSnapshotName();
        final Metas snapshotMetas = Metas.NO_METAS;

        // Configure profile
        if (margs.features.contains(MainArgs.Feature.DISABLE_ALL)) {
            AsdProfile.PROFILE.setAllEnabled(false);
        }
        if (margs.features.contains(MainArgs.Feature.ENABLE_ALL)) {
            AsdProfile.PROFILE.setAllEnabled(true);
        }
        for (final String s : margs.disabledRules) {
            final RuleId ruleId = new RuleId(s);
            if (AsdProfile.PROFILE.hasRule(ruleId)) {
                AsdProfile.PROFILE.setEnabled(ruleId, false);
            } else {
                log("Unknown rule: {}", s);
            }
        }
        for (final String s : margs.enabledRules) {
            final RuleId ruleId = new RuleId(s);
            if (AsdProfile.PROFILE.hasRule(ruleId)) {
                AsdProfile.PROFILE.setEnabled(ruleId, true);
            } else {
                log("Unknown rule: {}", s);
            }
        }
        if (margs.profileConfigFile != null) {
            log("Loading profile config {}", margs.profileConfigFile);
            chrono.start();
            final ProfileConfig profileConfig = ProfileIo.loadProfileConfig(ProfileIoFeatures.FASTEST, margs.profileConfigFile);
            chrono.suspend();
            log("Loaded profile config ({})", chrono);

            AsdProfile.PROFILE.apply(profileConfig);
        }

        // Check model
        log("Checking model");
        chrono.start();
        final SnapshotManager manager = ModelChecker.check(projectName,
                                                           projectMetas,
                                                           snapshotName,
                                                           snapshotMetas,
                                                           model,
                                                           naming,
                                                           refModel,
                                                           new IssuesCollector<>());
        chrono.suspend();
        log("Checked model ({})", chrono);

        // Save issues
        final File issuesFile = new File(margs.outputDir, margs.basename + ISSUES);
        log("Saving issues to {}", issuesFile);
        chrono.start();

        final SnapshotDataImpl synthesis = new SnapshotDataImpl();
        synthesis.setNumberOfIssues(manager.getIssuesCollector().getIssues().size());
        synthesis.setProjectName(projectName);
        synthesis.setProjectMetas(projectMetas);
        synthesis.setSnapshotName(snapshotName);
        synthesis.setSnapshotMetas(snapshotMetas);
        synthesis.setProfile(AsdProfile.PROFILE);
        IssuesWriter.save(synthesis,
                          manager.getIssuesCollector().getIssues(),
                          OutSettings.builder()
                                     .hint(OutSettings.Hint.NO_ANSWERS)
                                     .hint(OutSettings.Hint.AUTO_LOCATIONS)
                                     .build(),
                          issuesFile,
                          ProgressController.VOID,
                          margs.features.isEnabled(MainArgs.Feature.BEST)
                                  ? IssuesIoFactoryFeatures.UTC_BEST
                                  : IssuesIoFactoryFeatures.UTC_FASTEST);
        chrono.suspend();
        log("Saved issues ({})", chrono);

        // Save stats
        final File statsFile = new File(margs.outputDir, margs.basename + STATS);

        final CheckStats<Location> stats = manager.getStats().orElseThrow();
        log("Saving stats to {}", statsFile);
        chrono.start();
        final WorkbookCheckStatsIo<Location> io = new WorkbookCheckStatsIo<>(x -> Location.toString(x, false));
        io.save(stats,
                statsFile,
                margs.features.isEnabled(MainArgs.Feature.BEST)
                        ? WorkbookWriterFeatures.STANDARD_BEST
                        : WorkbookWriterFeatures.STANDARD_FAST);
        chrono.suspend();
        log("Saved stats ({})", chrono);

        // Save check profile
        final File profileFile = new File(margs.outputDir, margs.basename + PROFILE);
        final ProfileIoFeatures pioFeatures =
                ProfileIoFeatures.builder()
                                 .hint(ProfileIoFeatures.Hint.PRETTY_PRINT)
                                 .build();

        log("Saving profile to {}", profileFile);
        chrono.start();

        ProfileIo.save(pioFeatures, AsdProfile.PROFILE, profileFile);

        chrono.suspend();
        log("Saved profile ({})", chrono);
    }

    public static void execute(MainArgs margs) throws IOException {
        final AsdModelChecker instance = new AsdModelChecker(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String ENABLE = "enable";
        private static final String DISABLE = "disable";
        private static final String MODEL_FILE = "model";
        private static final String REF_MODEL_FILE = "ref-model";
        private static final String BASENAME = "basename";
        private static final String PROFILE_CONFIG_FILE = "profile-config";

        public MainSupport() {
            super(AsdModelChecker.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return """
                   Utility that can load an ASD model (MF XML format), check it and save found issues.
                   It will also save the configured profile used to check the model.""";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(BASENAME)
                                    .desc("Mandatory base name of generated files.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(MODEL_FILE)
                                    .desc("Mandatory name of the XML ASD MF model to check.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(REF_MODEL_FILE)
                                    .desc("Optional name of the reference XML ASD MF model to check against."
                                            + "\nUsed by some checkers as a reference to compare the checked model.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Mandatory name of the output directory. If this directory does not exist, it is created.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(ENABLE)
                                    .desc("Name(s) of rule checkers to enable.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(DISABLE)
                                    .desc("Name(s) of rule checkers to disable.")
                                    .hasArgs()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PROFILE_CONFIG_FILE)
                                    .desc("Optional name of the ASD Profile Config file to load.")
                                    .hasArg()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
            createGroup(options, MainArgs.Feature.BEST, MainArgs.Feature.FASTEST);
            createGroup(options, MainArgs.Feature.ENABLE_ALL, MainArgs.Feature.DISABLE_ALL);
            createGroup(options, ENABLE, DISABLE);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.basename = getValueAsString(cl, BASENAME, "xxx");
            margs.modelFile = getValueAsResolvedFile(cl, MODEL_FILE, IS_FILE);
            margs.refModelFile = getValueAsResolvedFile(cl, REF_MODEL_FILE, IS_NULL_OR_FILE);
            margs.outputDir = getValueAsResolvedFile(cl, OUTPUT_DIR);
            margs.profileConfigFile = getValueAsResolvedFile(cl, PROFILE_CONFIG_FILE, IS_NULL_OR_FILE);
            fillValues(cl, ENABLE, margs.enabledRules);
            fillValues(cl, DISABLE, margs.disabledRules);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException {
            AsdModelChecker.execute(margs);
            return null;
        }
    }
}