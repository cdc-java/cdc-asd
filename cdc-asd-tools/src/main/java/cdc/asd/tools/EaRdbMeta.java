package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.checks.AsdProfileUtils;
import cdc.issues.checks.io.WorkbookCheckStatsIo;
import cdc.office.ss.SheetLoader;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.Row;
import cdc.office.tables.TableSection;
import cdc.util.time.Chronometer;

/**
 * Test to dump some EAP data.
 *
 * @author Damien Carbonne
 */
public class EaRdbMeta {
    private static final Logger LOGGER = LogManager.getLogger(EaRdbMeta.class);

    private static class GParams {
        final File targetDir;
        final File mergedIssuesFile;
        final File mergedStatsFile;

        public GParams() {
            this.targetDir = new File("target");
            this.mergedIssuesFile = new File(targetDir, "model-issues.xlsx");
            this.mergedStatsFile = new File(targetDir, "model-check-stats.xlsx");
        }

        File getOutputDir(String basename) {
            return new File(targetDir, basename);
        }

        File getIssuesFile(String basename) {
            return new File(getOutputDir(basename), basename + AsdModelChecker.ISSUES);
        }
    }

    private static class Params extends GParams {
        final File eapFile;

        public Params(String basename,
                      String name,
                      String eap,
                      String author) {
            this.eapFile = new File("src/main/resources/" + basename + "/" + eap);
        }
    }

    @FunctionalInterface
    private static interface Exec {
        public void run() throws Exception;
    }

    private static void line() {
        LOGGER.info("===================================================================================");
    }

    private static void invoke(String start,
                               Exec exec,
                               String end) throws Exception {
        final Chronometer chrono = new Chronometer();
        line();
        LOGGER.info("=== {}", start);
        line();
        chrono.start();
        exec.run();
        chrono.suspend();
        final String eformat = "=== " + end + " ({})";
        line();
        LOGGER.info(eformat, chrono);
        line();
    }

    private static void runSuite(Params params) throws Exception {
        invoke("Run suite: " + params.eapFile,
               () -> {
                   final AsdSuite.MainArgs margs = new AsdSuite.MainArgs();
                   margs.eapFile = params.eapFile;
                   margs.outputDir = new File("target");
                   margs.features.add(AsdSuite.MainArgs.Feature.APPEND_BASENAME);
                   margs.features.add(AsdSuite.MainArgs.Feature.VERBOSE);
                   margs.features.add(AsdSuite.MainArgs.Feature.RUN_ALL);
                   margs.features.add(AsdSuite.MainArgs.Feature.DUMP_NO_FORCE);
                   margs.features.add(AsdSuite.MainArgs.Feature.MODEL_FIX_DIRECTION);
                   margs.features.add(AsdSuite.MainArgs.Feature.CLEAN_ALL);
                   margs.features.add(AsdSuite.MainArgs.Feature.HTML_FLAT_DIRS);
                   margs.features.add(AsdSuite.MainArgs.Feature.HTML_IMG_SVG);
                   margs.features.add(AsdSuite.MainArgs.Feature.HTML_SHOW_IDS);
                   margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_MD);
                   margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_HTML);
                   margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_XLSX);
                   margs.features.add(AsdSuite.MainArgs.Feature.PROFILE_XML);
                   margs.features.add(AsdSuite.MainArgs.Feature.S1000D_GLOSSARY);
                   margs.features.add(AsdSuite.MainArgs.Feature.S1000D_SPEC);

                   margs.htmlIgnoreModelOverviewPackage("/Builtin");
                   margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Compound_Attributes.*");
                   margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Base_Object_Definition.*");
                   margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Primitives.*");
                   margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Remark");
                   margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Digital File");
                   margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Security Classification");
                   margs.htmlIgnoreModelOverviewPackage(".*/[A-Za-z0-9_ \\-]*Applicability Statement");
                   margs.htmlIgnoreModelOverviewPackage(".*/S5000F Specializations");

                   margs.htmlRemoveNamePart("^.* UoF ");
                   margs.htmlRemoveNamePart("^S2000M ");
                   margs.htmlRemoveNamePart("^S3000L ");
                   margs.htmlRemoveNamePart("^S5000F ");
                   margs.htmlRemoveNamePart("^S6000T ");
                   margs.htmlRemoveNamePart("^SX000i ");

                   AsdSuite.execute(margs);
               },
               "Ran suite");
    }

    private static void generate(String name,
                                 String eap,
                                 String author) throws Exception {
        line();
        LOGGER.info("=== {}", name);
        line();

        final Params params = new Params(name.toLowerCase(), name, eap, author);
        runSuite(params);
    }

    private static void addProfileSheet(WorkbookWriter<?> writer,
                                        SheetLoader loader,
                                        String basename) throws IOException {
        final String name = "Profile";
        final File in = new GParams().getIssuesFile(basename);
        final List<Row> rows = loader.load(in, null, name);
        writer.beginSheet(name);
        boolean first = true;
        for (final Row row : rows) {
            TableSection section;
            if (first) {
                first = false;
                section = TableSection.HEADER;
            } else {
                section = TableSection.DATA;
            }
            writer.addRow(section, row);
        }
    }

    // TODO move this to cdc-issues
    private static void mergeIssues(File file,
                                    String... basenames) throws Exception {
        invoke("Merge issues: " + file,
               () -> {
                   final WorkbookWriterFactory factory = new WorkbookWriterFactory();
                   factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
                   final SheetLoader loader = new SheetLoader();

                   try (final WorkbookWriter<?> writer = factory.create(file, WorkbookWriterFeatures.STANDARD_FAST)) {
                       addProfileSheet(writer, loader, basenames[0]);

                       writer.beginSheet("Issues");
                       boolean first = true;
                       for (final String basename : basenames) {
                           final File in = new File("target/" + basename, basename + "-model-issues.xlsx");
                           final List<Row> rows = loader.load(in, null, "Issues#1");
                           if (first) {
                               writer.addRow(TableSection.HEADER, rows.get(0));
                           }
                           for (final Row row : rows.subList(1, rows.size())) {
                               writer.addRow(TableSection.DATA, row);
                           }
                           first = false;
                       }
                       writer.flush();
                   }
               },
               "Merged issues");
    }

    // TODO move this to cdc-issues
    private static void mergeStats(File file,
                                   String... basenames) throws Exception {
        invoke("Merge Stats: " + file,
               () -> {
                   final List<File> inputs = new ArrayList<>();
                   for (final String basename : basenames) {
                       inputs.add(new File("target/" + basename, basename + "-model-check-stats.xlsx"));
                   }
                   WorkbookCheckStatsIo.merge(file, inputs);
               },
               "Merged Stats");
    }

    public static void main(String... args) throws Exception {
        final Chronometer chrono = new Chronometer();
        chrono.start();
        LOGGER.info("Start");

        final GParams gparams = new GParams();

        final String s2000m_7_0 = "S2000M-7.0";
        final String s3000l_2_0 = "S3000L-2.0";
        final String s5000f_3_0 = "S5000F-3.0";
        final String s6000t_2_0 = "S6000T-2.0";
        final String sx000i_3_0 = "SX000i-3.0";
        final String sx002d_2_1 = "SX002D-2.1";

        invoke("",
               AsdProfileUtils::showRules,
               "");

        invoke("Save profile into target",
               () -> {
                   AsdProfileUtils.saveProfileToHtml(new File("target/asd-profile.html"));
                   AsdProfileUtils.saveProfileToMd(new File("target/asd-profile.md"));
                   AsdProfileUtils.saveProfileToOffice(new File("target/asd-profile.xlsx"));
               },
               "Saved profile");

        invoke("Save mapping",
               () -> AsdProfileUtils.saveMapping(new File("target/mapping.xlsx")),
               "Saved mapping");

        generate(s2000m_7_0, "S2000M_7-0_Data_model_001-00.eap", "S2000M");
        generate(s3000l_2_0, "S3000L_2-0_Data_model_001-00.eap", "S3000L");
        generate(s5000f_3_0, "S5000F_3-0_Data_model_001-00.eap", "S5000F");
        generate(s6000t_2_0, "S6000T_2-0_Data_model_001-00.eap", "S6000T");
        generate(sx000i_3_0, "SX000i_3-0_Data_model_001-00.eap", "DMEWG");
        generate(sx002d_2_1, "SX002D_2-1_Data_model_002-00.eap", "DMEWG");

        mergeIssues(gparams.mergedIssuesFile,
                    s2000m_7_0.toLowerCase(),
                    s3000l_2_0.toLowerCase(),
                    s5000f_3_0.toLowerCase(),
                    s6000t_2_0.toLowerCase(),
                    sx000i_3_0.toLowerCase(),
                    sx002d_2_1.toLowerCase());

        mergeStats(gparams.mergedStatsFile,
                   s2000m_7_0.toLowerCase(),
                   s3000l_2_0.toLowerCase(),
                   s5000f_3_0.toLowerCase(),
                   s6000t_2_0.toLowerCase(),
                   sx000i_3_0.toLowerCase(),
                   sx002d_2_1.toLowerCase());

        chrono.suspend();
        LOGGER.info("Done ({})", chrono);
    }
}