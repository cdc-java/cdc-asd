package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.io.data.Element;
import cdc.io.data.Parent;
import cdc.io.data.util.DataUtils;
import cdc.io.data.xml.XmlDataReader;
import cdc.io.data.xml.XmlDataWriter;
import cdc.io.xml.XmlWriter;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.office.tables.TableSection;
import cdc.util.function.Evaluator;

/**
 * Quick and dirty EA XMI analyzer used to produce an Excel file.
 *
 * @author Damien Carbonne
 */
public final class EaXmiAnalyzer {
    private static final Logger LOGGER = LogManager.getLogger(EaXmiAnalyzer.class);
    private int depth = 0;

    private static final String ABSTRACT = "abstract";
    private static final String AGGREGATION = "aggregation";
    private static final String ASSOCIATION = "association";
    private static final String ATT = "att";
    private static final String CLASS = "class";
    private static final String DOCUMENTATION = "documentation";
    private static final String END = "end";
    private static final String EXTENDS = "extends";
    private static final String ID = "id";
    private static final String IDREF = "idref";
    private static final String IMPLEMENTS = "implements";
    private static final String INTERFACE = "interface";
    private static final String LOWER = "lower";
    private static final String MODEL = "model";
    private static final String NAME = "name";
    private static final String PACKAGE = "package";
    private static final String PRIMITIVE = "primitive";
    private static final String TAG = "tag";
    private static final String TAGS = "tags";
    private static final String TYPE = "type";
    private static final String TYPE_IDREF = "type-idref";
    private static final String SLOT = "slot";
    private static final String STEREOTYPE = "stereotype";
    private static final String STUB = "stub";
    private static final String UPPER = "upper";
    private static final String VALUE = "value";

    private static final String EA_ATTRIBUTE = "attribute";
    private static final String EA_ATTRIBUTES = "attributes";
    private static final String EA_DOCUMENTATION = "documentation";
    private static final String EA_DIAGRAM = "diagram";
    private static final String EA_ELEMENT = "element";
    private static final String EA_ELEMENTS = "elements";
    private static final String EA_LOWER_VALUE = "lowerValue";
    private static final String EA_MEMBER_END = "memberEnd";
    private static final String EA_NAME = "name";
    private static final String EA_NOTES = "notes";
    private static final String EA_OWNED_ATTRIBUTE = "ownedAttribute";
    private static final String EA_OWNED_END = "ownedEnd";
    private static final String EA_PACKAGED_ELEMENT = "packagedElement";
    private static final String EA_UPPER_VALUE = "upperValue";
    private static final String EA_STUB = "EAStub";
    private static final String EA_TAG = "tag";
    private static final String EA_TAGS = "tags";
    private static final String EA_TYPE = "type";
    private static final String EA_VALUE = "value";

    private static final String UML_ASSOCIATION = "uml:Association";
    private static final String UML_CLASS = "uml:Class";
    private static final String UML_INTERFACE = "uml:Interface";
    private static final String UML_MODEL = "uml:Model";
    private static final String UML_PACKAGE = "uml:Package";
    private static final String UML_PRIMITIVE_TYPE = "uml:PrimitiveType";
    private static final String UML_REALIZATION = "uml:Realization";

    private static final String XMI_ID = "xmi:id";
    private static final String XMI_IDREF = "xmi:idref";
    private static final String XMI_TYPE = "xmi:type";

    private Element root = null;
    private final List<Element> stack = new ArrayList<>();
    private final Map<String, Element> idToElement = new HashMap<>();
    private final Map<String, Set<String>> idToDirectDescendantIds = new HashMap<>();
    private final Map<String, Set<String>> idToAllDescendantIds = new HashMap<>();

    private Element getElement(String id,
                               String context) {
        final Element result = idToElement.get(id);
        if (result == null) {
            LOGGER.warn("Failed to retrieve element with id '{}' ({})", id, context);
        }
        return result;
    }

    private EaXmiAnalyzer() {
    }

    private Element getCurrent() {
        return stack.get(stack.size() - 1);
    }

    private void push(Element element) {
        if (root == null) {
            root = element;
        } else {
            stack.get(stack.size() - 1).addChild(element);
        }
        stack.add(element);
    }

    private void pushId(String name,
                        String id) {
        push(new Element(name));
        if (id != null) {
            getCurrent().addAttribute(ID, id);
            idToElement.put(id, getCurrent());
        }
    }

    private void pushIdRef(String name,
                           String idref) {
        push(new Element(name));
        if (idref != null) {
            getCurrent().addAttribute(IDREF, idref);
        }
    }

    private void pop() {
        stack.remove(stack.size() - 1);
    }

    private String indent() {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < depth - 1; i++) {
            builder.append("   ");
        }
        return builder.toString();
    }

    public void analyze(File file,
                        File to) throws IOException {
        final Element r = XmlDataReader.loadRoot(file);
        parse(r);

        resolveTypes(root);
        parseRealization(r);
        parseGeneralization(r);
        moveDocumentation(root);
        resolveEnds(root);

        XmlDataWriter.print(root, false, System.out, "   ", false, XmlWriter.Feature.PRETTY_PRINT);

        buildDescendants(root);
        dump(to);
        DataUtils.removePureElements(root, Evaluator.continueTraversal());
    }

    private void buildDescendants(Element element) {
        if (CLASS.equals(element.getName())
                || INTERFACE.equals(element.getName())
                || PRIMITIVE.equals(element.getName())) {
            final String id = element.getAttributeValue(ID);
            for (final String ancestorId : getAllAncestorIds(element)) {
                final Set<String> set = idToAllDescendantIds.computeIfAbsent(ancestorId, k -> new HashSet<>());
                set.add(id);
            }
            for (final String ancestorId : getDirectAncestorIds(element)) {
                final Set<String> set = idToDirectDescendantIds.computeIfAbsent(ancestorId, k -> new HashSet<>());
                set.add(id);
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            buildDescendants(child);
        }
    }

    private void dump(File file) throws IOException {
        LOGGER.info("Generating {}", file);
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        try (WorkbookWriter<?> writer = factory.create(file, WorkbookWriterFeatures.STANDARD_BEST)) {
            dumpClasses(root, writer);
            dumpProperties(root, writer);
            dumpImplementations(root, writer);
            writer.flush();
        }
        LOGGER.info("Generated {}", file);
    }

    private static String getDocumentation(Element element) {
        final StringBuilder builder = new StringBuilder();
        for (final Element child : element.getChildren(Element.class, e -> DOCUMENTATION.equals(e.getName()))) {
            builder.append(child.getText());
        }
        return builder.toString();
    }

    private static String getPackageName(Element element) {
        Parent index = element.getParent();
        do {
            if (index instanceof final Element p) {
                if (PACKAGE.equals(p.getName())) {
                    return p.getAttributeValue(NAME);
                } else {
                    index = p.getParent();
                }
            } else {
                index = null;
            }
        } while (index != null);
        return "";
    }

    private String idToName(String id) {
        final Element ref = getElement(id, "id to name");
        return ref.getAttributeValue(NAME);
    }

    private String idsToNames(Set<String> ids) {
        final List<String> names = new ArrayList<>();
        for (final String id : ids) {
            final Element ref = getElement(id, "ids to names");
            names.add(ref.getAttributeValue(NAME));
        }
        Collections.sort(names);
        return names.stream().collect(Collectors.joining("\n"));
    }

    private static Set<String> getDirectAncestorIds(Element element) {
        final Set<String> set = new HashSet<>();
        for (final Element child : element.getChildren(Element.class)) {
            if (EXTENDS.equals(child.getName()) || IMPLEMENTS.equals(child.getName())) {
                if (child.hasAttribute(IDREF)) {
                    set.add(child.getAttributeValue(IDREF));
                } else {
                    LOGGER.warn("No idref on {}", child);
                }
            }
        }
        return set;
    }

    private Set<String> getAllAncestorIds(Element element) {
        final Set<String> todo = getDirectAncestorIds(element);
        final Set<String> done = new HashSet<>();
        while (!todo.isEmpty()) {
            final String next = todo.iterator().next();
            todo.remove(next);
            done.add(next);
            final Element n = getElement(next, "all ancestors");
            if (n != null) {
                todo.addAll(getDirectAncestorIds(n));
                todo.removeAll(done);
            }
        }
        return done;
    }

    private Set<String> getAllInterfaceIds(Element element) {
        final Set<String> ancestorsIds = getAllAncestorIds(element);
        final Set<String> result = new HashSet<>();
        for (final String id : ancestorsIds) {
            final Element n = getElement(id, "all interfaces");
            if (n != null && n.getName().equals(INTERFACE)) {
                result.add(id);
            }
        }
        return result;
    }

    private Set<String> getDirectDescendantIds(Element element) {
        return idToDirectDescendantIds.getOrDefault(element.getAttributeValue(ID), Collections.emptySet());
    }

    private Set<String> getAllDescendantIds(Element element) {
        return idToAllDescendantIds.getOrDefault(element.getAttributeValue(ID), Collections.emptySet());
    }

    private static String getMultiplicity(Element element) {
        final String lower = element.getAttributeValue(LOWER, "");
        final String upper = element.getAttributeValue(UPPER, "");
        if (lower.equals(upper)) {
            if ("-1".equals(lower)) {
                return "*";
            } else {
                return lower;
            }
        } else if ("-1".equals(upper)) {
            return lower + "..*";
        } else {
            return lower + ".." + upper;
        }
    }

    private static String getTags(Element element) {
        final Element tags = element.getChild(Element.class, x -> TAGS.equals(x.getName()));
        final StringBuilder builder = new StringBuilder();
        if (tags != null) {
            for (final Element tag : tags.getChildren(Element.class, x -> TAG.equals(x.getName()))) {
                if (builder.length() > 0) {
                    builder.append("\n");
                }
                builder.append(tag.getAttributeValue(NAME));
                builder.append(":");
                builder.append(tag.getAttributeValue(VALUE));
            }
        }
        return builder.toString();
    }

    private static String getTags(Element element,
                                  String name) {
        final Element tags = element.getChild(Element.class, x -> TAGS.equals(x.getName()));
        final StringBuilder builder = new StringBuilder();
        if (tags != null) {
            for (final Element tag : tags.getChildren(Element.class, x -> TAG.equals(x.getName()))) {
                if (name.equals(tag.getAttributeValue(NAME))) {
                    if (builder.length() > 0) {
                        builder.append("\n");
                    }
                    builder.append(tag.getAttributeValue(VALUE));
                    final String doc = getDocumentation(tag);
                    if (doc != null && !doc.isEmpty()) {
                        builder.append(" (").append(doc).append(")");
                    }
                }
            }
        }
        return builder.toString();
    }

    private String getMetatype(String typeidref) {
        final Element ref = getElement(typeidref, "get metatype");
        if (ref != null) {
            return ref.getName();
        } else {
            return "";
        }
    }

    private String getStereotype(String typeidref) {
        final Element ref = getElement(typeidref, "get stereotype");
        if (ref != null) {
            return ref.getAttributeValue(STEREOTYPE, "");
        } else {
            return "";
        }
    }

    private static String getAbstractness(Element element) {
        final String s = element.getAttributeValue(ABSTRACT, null);
        if ("true".equalsIgnoreCase(s)) {
            return "abstract";
        } else if ("false".equalsIgnoreCase(s)) {
            return "concrete";
        } else {
            return "???";
        }
    }

    private void dumpClasses(Element element,
                             WorkbookWriter<?> writer) throws IOException {
        if (element == root) {
            writer.beginSheet("Classes");
            writer.addRow(TableSection.HEADER,
                          "Package",
                          "Type",
                          "Metatype",
                          "Stereotype",
                          "Abtractness",
                          "Direct Ancestors",
                          "All Ancestors",
                          "Direct Descendants",
                          "All Descendants",
                          "Tags",
                          "Examples",
                          "Documentation");
        } else {
            switch (element.getName()) {
            case CLASS:
            case INTERFACE:
            case PRIMITIVE:
            case STUB:
                writer.beginRow(TableSection.DATA);
                writer.addCell(getPackageName(element));
                writer.addCell(element.getAttributeValue(NAME));
                writer.addCell(element.getName());
                writer.addCell(element.getAttributeValue(STEREOTYPE, ""));
                writer.addCell(getAbstractness(element));
                writer.addCell(idsToNames(getDirectAncestorIds(element)));
                writer.addCell(idsToNames(getAllAncestorIds(element)));
                writer.addCell(idsToNames(getDirectDescendantIds(element)));
                writer.addCell(idsToNames(getAllDescendantIds(element)));
                writer.addCell(getTags(element));
                writer.addCell(getTags(element, "example"));
                writer.addCell(getDocumentation(element));
                break;
            default:
                break;
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            dumpClasses(child, writer);
        }
    }

    private void dumpImplementations(Element element,
                                     WorkbookWriter<?> writer) throws IOException {
        if (element == root) {
            writer.beginSheet("Implementations");
            writer.addRow(TableSection.HEADER,
                          "Package",
                          "Type",
                          "Metatype",
                          "Stereotype",
                          "Implements");
        } else {
            switch (element.getName()) {
            case CLASS:
                final Set<String> xfaces = getAllInterfaceIds(element);
                for (final String xface : xfaces) {
                    writer.beginRow(TableSection.DATA);
                    writer.addCell(getPackageName(element));
                    writer.addCell(element.getAttributeValue(NAME));
                    writer.addCell(element.getName());
                    writer.addCell(element.getAttributeValue(STEREOTYPE, ""));
                    writer.addCell(idToName(xface));
                }
                break;
            default:
                break;
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            dumpImplementations(child, writer);
        }
    }

    private void dumpProperties(Element element,
                                WorkbookWriter<?> writer) throws IOException {
        if (element == root) {
            writer.beginSheet("Properties");
            writer.addRow(TableSection.HEADER,
                          "Package",
                          "Owner Type",
                          "Name",
                          "Type",
                          "Metatype",
                          "Stereotype",
                          "Multiplicity",
                          "Definition",
                          "Inherited From",
                          "Tags",
                          "Examples",
                          "Valid Values",
                          "Documentation");
        } else {
            if (ATT.equals(element.getName())) {
                final Element owner = element.getParent(Element.class);
                dumpProperty(element, owner, "local", null, writer);
                for (final String descendantId : idToAllDescendantIds.getOrDefault(owner.getAttributeValue(ID),
                                                                                   Collections.emptySet())) {
                    final Element descendant = getElement(descendantId, "descendant");
                    dumpProperty(element, descendant, "inherited", owner, writer);
                }
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            dumpProperties(child, writer);
        }
    }

    private void dumpProperty(Element att,
                              Element owner,
                              String definition,
                              Element ancestor,
                              WorkbookWriter<?> writer) throws IOException {
        writer.beginRow(TableSection.DATA);
        writer.addCell(getPackageName(owner));
        writer.addCell(owner.getAttributeValue(NAME));
        writer.addCell(att.getAttributeValue(NAME, "???"));
        writer.addCell(att.getAttributeValue(TYPE));
        writer.addCell(getMetatype(att.getAttributeValue(TYPE_IDREF)));
        writer.addCell(getStereotype(att.getAttributeValue(TYPE_IDREF)));
        writer.addCell(getMultiplicity(att));
        writer.addCell(definition);
        if (ancestor == null) {
            writer.addEmptyCell();
        } else {
            writer.addCell(ancestor.getAttributeValue(NAME));
        }
        writer.addCell(getTags(att));
        writer.addCell(getTags(att, "example"));
        writer.addCell(getTags(att, "validValue"));
        writer.addCell(getDocumentation(att));
    }

    private void resolveTypes(Element element) {
        if (element.hasAttribute(TYPE)) {
            final String idref = element.getAttributeValue(TYPE, "");
            if (idref == null) {
                element.getAttribute(TYPE).setValue("???");
            } else if (idref.startsWith("EAnone_")) {
                element.getAttribute(TYPE).setValue("EA:" + idref.substring(7));
            } else {
                final Element ref = getElement(idref, "type resolution");
                if (ref != null) {
                    element.getAttribute(TYPE).setValue(ref.getAttributeValue(NAME));
                } else {
                    element.getAttribute(TYPE).setValue("???:" + idref);
                }
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            resolveTypes(child);
        }
    }

    private void resolveEnds(Element element) {
        if (END.equals(element.getName())) {
            final String idref = element.getAttributeValue(IDREF, null);
            final Element ref = getElement(idref, "end resolution");
            if (ref != null && (ATT.equals(ref.getName()) || SLOT.equals(ref.getName()))) {
                element.addAttribute(TYPE, ref.getAttributeValue(TYPE, null));
                element.addAttribute(TYPE_IDREF, idref);
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            resolveEnds(child);
        }
    }

    private void moveDocumentation(Element element) {
        if (DOCUMENTATION.equals(element.getName())) {
            element.setIndex(0);
        }
        for (final Element child : element.getChildren(Element.class)) {
            moveDocumentation(child);
        }
    }

    private void parseRealization(Element element) {
        if (EA_PACKAGED_ELEMENT.equals(element.getName())
                && UML_REALIZATION.equals(element.getAttributeValue(XMI_TYPE))) {
            final String sid = element.getAttributeValue("supplier", null);
            final String cid = element.getAttributeValue("client", null);
            final Element supplier = getElement(sid, "supplier realization");
            final Element client = getElement(cid, "client realization");
            if (client != null && supplier != null) {
                if (INTERFACE.equals(client.getName())) {
                    if (INTERFACE.equals(supplier.getName())) {
                        final Element child = client.addChild(new Element(EXTENDS));
                        child.addAttribute(NAME, supplier.getAttributeValue(NAME));
                        child.addAttribute(IDREF, sid);
                        child.setIndex(0);
                    } else {
                        LOGGER.warn("Realization {} {}", supplier, client);
                    }
                } else {
                    // Should not happen?
                    if (INTERFACE.equals(supplier.getName())) {
                        final Element child = client.addChild(new Element(IMPLEMENTS));
                        child.addAttribute(NAME, supplier.getAttributeValue(NAME));
                        child.addAttribute(IDREF, sid);
                        child.setIndex(0);
                    } else {
                        final Element child = client.addChild(new Element(EXTENDS));
                        child.addAttribute(NAME, supplier.getAttributeValue(NAME));
                        child.addAttribute(IDREF, sid);
                        child.setIndex(0);
                    }
                }
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            parseRealization(child);
        }
    }

    private void parseGeneralization(Element element) {
        if ("generalization".equals(element.getName())) {
            final Element context = element.getParent(Element.class);
            final String selfId = context.getAttributeValue(XMI_ID, null);
            final Element self = getElement(selfId, "particular");
            final String generalId = element.getAttributeValue("general");
            final Element general = getElement(generalId, "general");
            if (self != null && general != null) {
                final Element x = self.addElement(EXTENDS);
                x.addAttribute(NAME, general.getAttributeValue(NAME));
                x.addAttribute(IDREF, generalId);
                x.setIndex(0);
            } else {
                LOGGER.warn("Generalization {} {}", self, general);
            }
        }
        for (final Element child : element.getChildren(Element.class)) {
            parseGeneralization(child);
        }
    }

    private void parse(Element element) {
        depth++;
        final String s = indent();
        LOGGER.info("{}{}", s, element);

        boolean pushed = false;

        switch (element.getName()) {
        case UML_MODEL:
            push(new Element(MODEL));
            pushed = true;
            break;
        case EA_PACKAGED_ELEMENT:
            final String name1 = element.getAttributeValue(EA_NAME, null);
            final String xmiType1 = element.getAttributeValue(XMI_TYPE, null);
            final String id1 = element.getAttributeValue(XMI_ID, null);
            final String isAbstract = element.getAttributeValue("isAbstract", "false");
            LOGGER.info(">>>{} PELT {} ({}) [{}]", s, name1, xmiType1, id1);
            switch (xmiType1) {
            case UML_PACKAGE:
                pushId(PACKAGE, id1);
                getCurrent().addAttribute(NAME, name1);
                pushed = true;
                break;
            case UML_CLASS:
                pushId(CLASS, id1);
                getCurrent().addAttribute(NAME, name1);
                getCurrent().addAttribute(ABSTRACT, isAbstract);
                pushed = true;
                break;
            case UML_INTERFACE:
                pushId(INTERFACE, id1);
                getCurrent().addAttribute(NAME, name1);
                getCurrent().addAttribute(ABSTRACT, isAbstract);
                pushed = true;
                break;
            case UML_PRIMITIVE_TYPE:
                pushId(PRIMITIVE, id1);
                getCurrent().addAttribute(NAME, name1);
                getCurrent().addAttribute(ABSTRACT, isAbstract);
                pushed = true;
                break;
            case UML_ASSOCIATION:
                pushId(ASSOCIATION, id1);
                if (name1 != null) {
                    getCurrent().addAttribute(NAME, name1);
                }
                pushed = true;
                break;
            case UML_REALIZATION:
                // See parseRealization
                break;
            default:
                LOGGER.warn("Skip {}", xmiType1);
                break;
            }
            break;
        case EA_STUB:
            LOGGER.info(">>>{} STUB", s);
            final String name13 = element.getAttributeValue(EA_NAME, null);
            final String id13 = element.getAttributeValue(XMI_ID, null);
            final Element stub = root.addChild(new Element(STUB));
            stub.addAttribute(ID, id13);
            stub.addAttribute(NAME, name13);
            idToElement.put(id13, stub);
            break;
        case EA_MEMBER_END:
            final String idr9 = element.getAttributeValue(XMI_IDREF, null);
            pushIdRef(END, idr9);
            pushed = true;
            break;
        case EA_OWNED_END:
            final String id10 = element.getAttributeValue(XMI_ID, null);
            final String aggregation = element.getAttributeValue(AGGREGATION, null);
            pushId(SLOT, id10);
            getCurrent().addAttribute(AGGREGATION, aggregation);
            pushed = true;
            break;
        case EA_LOWER_VALUE:
            final String lower = element.getAttributeValue(EA_VALUE, null);
            getCurrent().addAttribute(LOWER, lower);
            break;
        case EA_UPPER_VALUE:
            final String upper = element.getAttributeValue(EA_VALUE, null);
            getCurrent().addAttribute(UPPER, upper);
            break;
        case EA_DOCUMENTATION:
            final String name2 = element.getAttributeValue(EA_NAME, null);
            final String doc2 = element.getAttributeValue(EA_VALUE, null);
            if (name2 != null || doc2 != null) {
                LOGGER.info(">>>{} DOC {}", s, doc2);
            }
            final Element ctx2 = element.getParent(Element.class);
            final String ctxidr2 = ctx2.getAttributeValue(XMI_IDREF);
            if (doc2 != null) {
                final Element ref = getElement(ctxidr2, "doc");
                if (ref != null) {
                    final Element doc = ref.addChild(new Element(DOCUMENTATION));
                    doc.addText(doc2);
                }
            }
            break;
        case EA_OWNED_ATTRIBUTE:
            final String name3 = element.getAttributeValue(EA_NAME, null);
            final String xmiType3 = element.getAttributeValue(XMI_TYPE, null);
            final String id3 = element.getAttributeValue(XMI_ID, null);
            LOGGER.info(">>>{} OATT {} ({}) [{}]", s, name3, xmiType3, id3);

            pushId(ATT, id3);
            if (name3 != null) {
                getCurrent().addAttribute(NAME, name3);
            }
            pushed = true;

            break;
        case EA_TYPE:
            // Set idref for type
            final String idr8 = element.getAttributeValue(XMI_IDREF, null);
            final Element ctx8 = element.getParent(Element.class);
            final String ctxid8 = ctx8.getAttributeValue(XMI_ID);
            final Element ref = getElement(ctxid8, "type context");
            if (ref != null) {
                ref.addAttribute(TYPE, idr8);
                ref.addAttribute(TYPE_IDREF, idr8);
            }
            break;

        case EA_ATTRIBUTE:
            final String name4 = element.getAttributeValue(EA_NAME, null);
            final String idr4 = element.getAttributeValue(XMI_IDREF, null);
            LOGGER.info(">>>{} ATT {} @[{}]", s, name4, idr4);
            break;
        case EA_ELEMENT:
            final String name5 = element.getAttributeValue(EA_NAME, null);
            final String xmiType5 = element.getAttributeValue(XMI_TYPE, null);
            final String idr5 = element.getAttributeValue(XMI_IDREF, null);
            if (idr5 != null) {
                LOGGER.info(">>>{} ELT {} ({}) @[{}]", s, name5, xmiType5, idr5);
            }
            break;
        case EA_TAGS:
            LOGGER.info(">>>{} TAGS", s);
            final Element owner12 = element.getParent(Element.class);
            final String idr12 = owner12.getAttributeValue(XMI_IDREF);
            final Element ref12 = getElement(idr12, "tags");
            if (ref12 != null) {
                final Element tags = ref12.addChild(new Element(TAGS));
                tags.setIndex(0);
            }
            break;
        case EA_TAG:
            final Element owner6 = element.getParent(Element.class).getParent(Element.class);
            final String idr6 = owner6.getAttributeValue(XMI_IDREF);
            final Element ref6 = getElement(idr6, "tag");
            final String name6 = element.getAttributeValue(EA_NAME, null);
            final String value6 = element.getAttributeValue(EA_VALUE, null);
            final String notes = element.getAttributeValue(EA_NOTES, null);
            if (ref6 != null) {
                final Element tags = ref6.getChild(Element.class, x -> TAGS.equals(x.getName()));
                final Element tag = tags.addChild(new Element(TAG));
                tag.addAttribute(NAME, name6);
                tag.addAttribute(VALUE, value6);
                if (notes != null && !notes.isEmpty()) {
                    final Element doc = tag.addChild(new Element(DOCUMENTATION));
                    doc.addText(notes);
                }
            }
            break;
        case EA_DIAGRAM:
            final String id7 = element.getAttributeValue(XMI_ID, null);
            LOGGER.info(">>>{} DIAG [{}]", s, id7);
            break;
        case EA_ELEMENTS:
            LOGGER.info(">>>{} ELTS", s);
            break;
        case EA_ATTRIBUTES:
            LOGGER.info(">>>{} ATTS", s);
            break;
        case "bounds":
        case "coords":
        case "containment":
        case "constraints":
        case "diagrams":
        case "extendedProperties":
        case "initial":
        case "labels":
        case "model":
        case "matrixitems":
        case "modifiers":
        case "options":
        case "parameterSubstitutions":
        case "project":
        case "stereotype":
        case "style":
        case "styleex":
        case "style1":
        case "style2":
        case "swimlanes":
        case "target":
        case "xrefs":
            break;
        default:
            break;

        }
        if (element.hasAttribute(EA_DOCUMENTATION)) {
            LOGGER.info(">>>{} EDOC {}", s, element.getAttributeValue(DOCUMENTATION));
            final Element ctx = element.getParent(Element.class);
            final String idr = ctx.getAttributeValue(XMI_IDREF, null);
            final String stereotype = element.getAttributeValue("stereotype", null);
            final Element ref = getElement(idr, "doc");
            if (ref != null) {
                ref.addAttribute(STEREOTYPE, stereotype);
                final Element doc = ref.addChild(new Element(DOCUMENTATION));
                doc.addText(element.getAttributeValue(DOCUMENTATION));
                doc.setIndex(0);
            }
        }

        for (

        final Element child : element.getChildren(Element.class)) {
            switch (child.getName()) {
            case "connectors":
            case "profiles":
            case "primitivetypes":
                break;
            default:
                parse(child);
                break;
            }
        }
        depth--;
        if (pushed) {
            pop();
        }
    }

    public static void main(String[] args) throws IOException {
        // new EaXmiAnalyzer().analyze(new File("src/main/resources/SX002D_2-1_Data_model_002-00.xmi"),
        // new File("target/SX002D_2-1_Data_model_002-00 A.xlsx"));
        new EaXmiAnalyzer().analyze(new File("src/main/resources/SX000i_3-0_Data_model_001-00.xmi"),
                                    new File("target/SX000i_3-0_Data_model_001-00 A.xlsx"));
        new EaXmiAnalyzer().analyze(new File("src/main/resources/S2000M_7-0_Data_model_001-00.xmi"),
                                    new File("target/S2000M_7-0_Data_model_001-00 A.xlsx"));
        new EaXmiAnalyzer().analyze(new File("src/main/resources/S3000L_2-0_Data_model_001-00.xmi"),
                                    new File("target/S3000L_2-0_Data_model_001-00 A.xlsx"));
        // new EaXmiAnalyzer().analyze(new File("src/main/resources/S5000F_3-0_Data_model.xmi"),
        // new File("target/S5000F_3-0_Data_model A.xlsx"));
        new EaXmiAnalyzer().analyze(new File("src/main/resources/S6000T_2-0_Data_model_001-00.xmi"),
                                    new File("target/S6000T_2-0_Data_model_001-00 A.xlsx"));
    }
}