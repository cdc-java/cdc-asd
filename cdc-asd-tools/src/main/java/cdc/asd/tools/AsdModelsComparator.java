package cdc.asd.tools;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.Config;
import cdc.asd.model.wrappers.AsdUtils;
import cdc.asd.tools.comparator.AsdModelsComparatorImpl;
import cdc.mf.model.MfModel;
import cdc.mf.model.io.MfModelXmlIo;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.time.Chronometer;

public final class AsdModelsComparator {
    private static final Logger LOGGER = LogManager.getLogger(AsdModelsComparator.class);
    private final MainArgs margs;

    public static class MainArgs {
        public List<File> modelFiles = new ArrayList<>();
        /** Output file that will contain comparison. */
        public File output;
        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public enum Feature implements OptionEnum {
            SHOW_ALL("show-all", "Show all details."),
            SHOW_CARDINALITIES("show-cardinalities", "Show cardinality of attributes."),
            SHOW_IDS("show-ids", "Show ids of model elements."),
            SHOW_NOTES("show-notes", "Show notes of model elements."),
            SHOW_STEREOTYPES("show-stereotypes", "Show stereotypes of model elements."),
            SHOW_TAGS("show-tags", "Show tags of model elements."),
            NO_SHOW_CARDINALITIES("no-show-cardinalities", "Do not show cardinality of attributes."),
            NO_SHOW_IDS("no-show-ids", "Do not show ids of model elements. Use with --show-all."),
            NO_SHOW_NOTES("no-show-notes", "Do not show notes of model elements. Use with --show-all."),
            NO_SHOW_STEREOTYPES("no-show-stereotypes", "Do not show stereotypes of model elements. Use with --show-all."),
            NO_SHOW_TAGS("no-show-tags", "Do not show tags of model elements. Use with --show-all."),
            DIFF_ID("diff-id",
                    """
                    Compare pairs of models using (kind, id) as key.
                    This should be used when all compared models are revision of the same specification."""),
            DIFF_NAME("diff-name",
                      """
                      Compare pairs of models using (kind, name, discriminator) as key.
                      The discriminator is used to differentiate elements with identical names.
                      This should be used when compared models belong to different specifications."""),
            FASTEST("fastest", "Use options that are fast to generate output (default)."),
            BEST("best", "Use options that generate best output."),
            VERBOSE("verbose", "Prints messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdModelsComparator(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private Set<AsdModelsComparatorImpl.Hint> getHints() {
        final Set<AsdModelsComparatorImpl.Hint> hints = EnumSet.noneOf(AsdModelsComparatorImpl.Hint.class);
        if (margs.features.contains(MainArgs.Feature.SHOW_ALL)) {
            hints.add(AsdModelsComparatorImpl.Hint.SHOW_CARDINALITIES);
            hints.add(AsdModelsComparatorImpl.Hint.SHOW_IDS);
            hints.add(AsdModelsComparatorImpl.Hint.SHOW_NOTES);
            hints.add(AsdModelsComparatorImpl.Hint.SHOW_STEREOTYPES);
            hints.add(AsdModelsComparatorImpl.Hint.SHOW_TAGS);
            if (margs.features.contains(MainArgs.Feature.NO_SHOW_CARDINALITIES)) {
                hints.remove(AsdModelsComparatorImpl.Hint.SHOW_CARDINALITIES);
            }
            if (margs.features.contains(MainArgs.Feature.NO_SHOW_IDS)) {
                hints.remove(AsdModelsComparatorImpl.Hint.SHOW_IDS);
            }
            if (margs.features.contains(MainArgs.Feature.NO_SHOW_NOTES)) {
                hints.remove(AsdModelsComparatorImpl.Hint.SHOW_NOTES);
            }
            if (margs.features.contains(MainArgs.Feature.NO_SHOW_STEREOTYPES)) {
                hints.remove(AsdModelsComparatorImpl.Hint.SHOW_STEREOTYPES);
            }
            if (margs.features.contains(MainArgs.Feature.NO_SHOW_TAGS)) {
                hints.remove(AsdModelsComparatorImpl.Hint.SHOW_TAGS);
            }
        } else {
            // WARNING: Make sure both enums use the same names
            // Otherwise an exception will be thrown by valueof
            for (final AsdModelsComparatorImpl.Hint hint : AsdModelsComparatorImpl.Hint.values()) {
                final MainArgs.Feature feature = MainArgs.Feature.valueOf(hint.name());
                if (margs.features.contains(feature)) {
                    hints.add(hint);
                }
            }
        }
        if (margs.features.contains(MainArgs.Feature.VERBOSE)) {
            hints.add(AsdModelsComparatorImpl.Hint.VERBOSE);
        }
        if (margs.features.contains(MainArgs.Feature.DIFF_ID)) {
            hints.add(AsdModelsComparatorImpl.Hint.DIFF_ID);
        }
        if (margs.features.contains(MainArgs.Feature.DIFF_NAME)) {
            hints.add(AsdModelsComparatorImpl.Hint.DIFF_NAME);
        }
        return hints;
    }

    private void execute() throws IOException {
        final Chronometer chrono = new Chronometer();

        final List<MfModel> models = new ArrayList<>();
        for (final File modelFile : margs.modelFiles) {
            log("Loading model {}", modelFile);
            chrono.start();
            final MfModel model = MfModelXmlIo.load(modelFile, AsdUtils.FACTORY);
            models.add(model);
            chrono.suspend();
            log("Loaded model ({})", chrono);
        }

        log("Comparing models into {}", margs.output);
        chrono.start();
        final AsdModelsComparatorImpl comparator =
                new AsdModelsComparatorImpl(models,
                                            getHints(),
                                            margs.features.isEnabled(MainArgs.Feature.BEST)
                                                    ? WorkbookWriterFeatures.STANDARD_BEST
                                                    : WorkbookWriterFeatures.STANDARD_FAST,
                                            LOGGER);
        comparator.compare(margs.output);
        chrono.suspend();
        log("Compared models ({})", chrono);
    }

    public static void execute(MainArgs margs) throws IOException {
        final AsdModelsComparator instance = new AsdModelsComparator(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String MODEL_FILES = "model";

        public MainSupport() {
            super(AsdModelsComparator.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return """
                   Utility that can compare several ASD MF XML models and save comparison into an Office file.
                   It generates:
                    - 'Packages' sheet, showing characteristics of each package in each model
                    - 'Classes' sheet, showing characteristics of each class in each model
                    - 'Interfaces' sheet, showing characteristics of each interface in each model
                    - 'Attributes' sheet, showing characteristics of each attribute in each model
                    - 'Tips' sheet, showing characteristics of each tip in each model
                    - 'Implementations' sheet, showing characteristics of each implementation in each model
                    - 'Specializations' sheet, showing characteristics of each specialization in each model
                    - 'Synthesis' sheet, showing presence of each element in each model
                    - 1 sheet for each model, showing characteristics of each element of the model
                    - 'All' sheet, showing characteristics of all elements of all models
                    - 1 sheet for each pair of models (optional); In that case model names are compressed.""";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(MODEL_FILES)
                                    .desc("Mandatory name(s) of the ASD MF XML models to compare.")
                                    .hasArgs()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT)
                                    .desc("Mandatory name of the output file. It must end with an Office extension (XLSX, CSV, ...).")
                                    .hasArg()
                                    .required()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);

            createGroup(options,
                        MainArgs.Feature.SHOW_CARDINALITIES,
                        MainArgs.Feature.NO_SHOW_CARDINALITIES);
            createGroup(options,
                        MainArgs.Feature.SHOW_IDS,
                        MainArgs.Feature.NO_SHOW_IDS);
            createGroup(options,
                        MainArgs.Feature.SHOW_NOTES,
                        MainArgs.Feature.NO_SHOW_NOTES);
            createGroup(options,
                        MainArgs.Feature.SHOW_STEREOTYPES,
                        MainArgs.Feature.NO_SHOW_STEREOTYPES);
            createGroup(options,
                        MainArgs.Feature.SHOW_TAGS,
                        MainArgs.Feature.NO_SHOW_TAGS);
            createGroup(options,
                        MainArgs.Feature.DIFF_ID,
                        MainArgs.Feature.DIFF_NAME);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            for (final String value : cl.getOptionValues(MODEL_FILES)) {
                final File file = new File(value);
                final File f = getReferencePath().resolve(file.toPath()).toFile();
                IS_FILE.check(cl, MODEL_FILES, f);
                margs.modelFiles.add(f);
            }
            margs.output = getValueAsResolvedFile(cl, OUTPUT);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException {
            AsdModelsComparator.execute(margs);
            return null;
        }
    }
}