package cdc.asd.tools.cleaner;

import cdc.asd.model.AsdBadges;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdModel;
import cdc.issues.Metas;
import cdc.mf.model.MfClass;
import cdc.mf.transform.MfElementFixer;
import cdc.mf.transform.MfTransformerContext;
import cdc.mf.transform.MfTransformerResult;

public class AsdCreateBaseObjectInheritance implements MfElementFixer<MfClass, MfClass.Builder<?>> {
    public static final String NAME = "ASD_CREATE_BASE_OBJECT_INHERITANCE";

    @Override
    public void fix(MfTransformerContext context,
                    MfClass srcElement,
                    MfClass.Builder<?> tgtBuilder) {
        final boolean match = srcElement.wrap(AsdElement.class).isBaseObjectDirectImplicitExtension();
        final MfTransformerResult result;
        if (match) {
            tgtBuilder.meta(CREATED_META_NAME, NAME, CREATED_META_SEPARATOR);
            result = MfTransformerResult.MODIFIED;
        } else {
            result = MfTransformerResult.UNCHANGED;
        }

        context.getStats().add(srcElement.getLocation(),
                               NAME,
                               result);
    }

    @Override
    public void postProcess(MfTransformerContext context,
                            MfClass srcElement,
                            MfClass tgtElement) {
        final boolean match = srcElement.wrap(AsdElement.class).isBaseObjectDirectImplicitExtension();
        if (match) {
            tgtElement.specialization()
                      .id(srcElement.getId() + "BOI")
                      .generalTypeId(srcElement.getModel().wrap(AsdModel.class).getBaseObject().getId())
                      .meta(Metas.BADGES, AsdBadges.EXTENSION)
                      .build();
        }
    }
}