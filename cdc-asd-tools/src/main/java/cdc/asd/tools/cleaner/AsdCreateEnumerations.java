package cdc.asd.tools.cleaner;

import java.util.List;

import cdc.asd.model.AsdBadges;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.issues.Metas;
import cdc.mf.model.MfDocumentation;
import cdc.mf.model.MfEnumeration;
import cdc.mf.model.MfEnumerationValue;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.mf.transform.MfElementFixer;
import cdc.mf.transform.MfTransformerContext;
import cdc.mf.transform.MfTransformerResult;
import cdc.util.strings.StringUtils;

public class AsdCreateEnumerations implements MfElementFixer<MfProperty, MfProperty.Builder<?>> {
    public static final String NAME = "ASD_CREATE_ENUMERATIONS";

    private static String toCapital(String s) {
        if (StringUtils.isNullOrEmpty(s)) {
            return "";
        } else {
            return Character.toUpperCase(s.charAt(0)) + s.substring(1);
        }
    }

    @Override
    public void fix(MfTransformerContext context,
                    MfProperty srcElement,
                    MfProperty.Builder<?> tgtBuilder) {
        final boolean hasValidValue = srcElement.wrap(AsdProperty.class).hasTags(AsdTagName.VALID_VALUE);

        final MfTransformerResult result;
        if (hasValidValue) {
            final MfPackage sourcePackage = srcElement.getSurroundingPackage();
            final MfPackage targetPackage = tgtBuilder.getModel()
                                                      .getItemWithId(sourcePackage.getId(),
                                                                     MfPackage.class)
                                                      .orElseThrow();

            final List<MfTag> values = srcElement.getTags(AsdTagName.VALID_VALUE);
            final String eId = srcElement.getId() + "E";
            final MfEnumeration targetEnumeration =
                    targetPackage.enumeration()
                                 .name(toCapital(srcElement.getName()) + "Enum")
                                 .id(eId)
                                 .meta(Metas.BADGES, AsdBadges.EXTENSION)
                                 .build();
            int index = 0;
            for (final MfTag value : values) {
                final String vId = eId + index;
                final MfEnumerationValue targetValue =
                        targetEnumeration.value()
                                         .name(value.getValue())
                                         .id(vId)
                                         .meta(Metas.BADGES, AsdBadges.EXTENSION)
                                         .build();
                for (final MfDocumentation documentation : value.getDocumentations()) {
                    targetValue.documentation()
                               .id(eId + documentation.getId())
                               .text(documentation.getText())
                               .build();
                }

                index++;
            }
            tgtBuilder.meta(CREATED_META_NAME, NAME, CREATED_META_SEPARATOR);
            result = MfTransformerResult.MODIFIED;
        } else {
            result = MfTransformerResult.UNCHANGED;
        }

        context.getStats().add(srcElement.getLocation(),
                               NAME,
                               result);
    }
}