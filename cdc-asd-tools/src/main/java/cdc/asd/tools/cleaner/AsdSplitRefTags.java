package cdc.asd.tools.cleaner;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdModel;
import cdc.asd.model.wrappers.AsdTag;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;
import cdc.mf.transform.MfElementFixer;
import cdc.mf.transform.MfTransformerContext;
import cdc.mf.transform.MfTransformerResult;

public class AsdSplitRefTags implements MfElementFixer<MfTag, MfTag.Builder<?>> {
    public static final String NAME = "ASD_SPLIT_REF_TAGS";

    private static final Pattern REF_BOUNDARY_PATTERN =
            Pattern.compile("(\\R|\\.)+");

    @Override
    public void fix(MfTransformerContext context,
                    MfTag srcElement,
                    MfTag.Builder<?> tgtBuilder) {
        final MfTagOwner tgtParent = tgtBuilder.getParent();

        final MfTransformerResult result;

        final AsdTagName tagName = srcElement.wrap(AsdTag.class).getTagName();
        if (tagName == AsdTagName.REF) {
            // Use source model as target model is under construction
            final String[] tokens = getRefTokens(srcElement.getModel().wrap(AsdModel.class), srcElement.getValue());
            if (tokens.length > 1) {
                // Quite confident that there are several concepts that should be split
                // Each tag has 1 token, and the input id with a suffix

                // First create N - 1 new tags
                for (int index = 0; index < tokens.length - 1; index++) {
                    tgtParent.tag()
                             .set(srcElement)
                             .id(srcElement.getId() + "'" + index)
                             .value(tokens[index])
                             .meta(FIXED_META_NAME, NAME, FIXED_META_SEPARATOR)
                             .build();
                }

                // Then modify the passed builder with last token
                tgtBuilder.value(tokens[tokens.length - 1])
                          .id(srcElement.getId() + "'" + (tokens.length - 1))
                          .meta(FIXED_META_NAME, NAME, FIXED_META_SEPARATOR);

                result = MfTransformerResult.MODIFIED;
            } else {
                // There is either one concept or something that can not be identified
                // Do nothing
                result = MfTransformerResult.UNCHANGED;
            }
        } else {
            // Do nothing
            result = MfTransformerResult.SKIPPED;
        }
        context.getStats().add(srcElement.getLocation(),
                               NAME,
                               result);
    }

    /**
     * Returns the tokens of a ref tag.
     * <p>
     * If there is a doubt, the input text is returned as the only token.
     *
     * @param model The model.
     * @param text The ref tag value.
     * @return The tokens contained in {@code text}.
     */
    private static String[] getRefTokens(AsdModel model,
                                         String text) {
        final List<String> tokens = new ArrayList<>();
        // true if tokens are all concepts
        boolean concepts = true;
        if (text != null) {
            for (final String token : REF_BOUNDARY_PATTERN.split(text)) {
                if (!token.isEmpty()) {
                    tokens.add(token);
                    if (!model.getSpecificTypeNames().contains(token)
                            && !model.getAttributeNames().contains(token)) {
                        concepts = false;
                    }
                }
            }
        }
        if (concepts) {
            // All tokens are concepts (types or attributes)
            return tokens.toArray(new String[tokens.size()]);
        } else {
            return new String[] { text };
        }
    }
}