package cdc.asd.tools.cleaner;

import java.util.HashMap;
import java.util.Map;

import cdc.asd.model.AsdTagName;
import cdc.mf.model.MfTag;
import cdc.mf.transform.MfElementFixer;
import cdc.mf.transform.MfTransformerContext;
import cdc.mf.transform.MfTransformerResult;

public class AsdFixTagNameCase implements MfElementFixer<MfTag, MfTag.Builder<?>> {
    public static final String NAME = "ASD_FIX_TAG_NAME_CASE";

    private final Map<String, String> map = new HashMap<>();

    public AsdFixTagNameCase() {
        for (final AsdTagName name : AsdTagName.values()) {
            map.put(name.getLiteral().toLowerCase(), name.getLiteral());
        }
    }

    @Override
    public void fix(MfTransformerContext context,
                    MfTag srcElement,
                    MfTag.Builder<?> tgtBuilder) {
        final String tagName = srcElement.getName();
        boolean changed = false;
        if (tagName != null) {
            final String expected = map.get(tagName.toLowerCase());
            if (expected != null && !expected.equals(tagName)) {
                tgtBuilder.name(expected);
                changed = true;
            }
        }
        if (changed) {
            tgtBuilder.meta(FIXED_META_NAME, NAME, FIXED_META_SEPARATOR);
        }
        context.getStats().add(srcElement.getLocation(),
                               NAME,
                               MfTransformerResult.ofModified(changed));
    }
}