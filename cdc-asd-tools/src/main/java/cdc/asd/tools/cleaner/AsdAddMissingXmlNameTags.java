package cdc.asd.tools.cleaner;

import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.mf.model.MfAbstractChildTaggedNamedElement;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;
import cdc.mf.transform.MfElementFixer;
import cdc.mf.transform.MfTransformerContext;
import cdc.mf.transform.MfTransformerResult;

public class AsdAddMissingXmlNameTags
        implements MfElementFixer<MfAbstractChildTaggedNamedElement<?>, MfAbstractChildTaggedNamedElement.Builder<?, ?, ?>> {
    public static final String NAME = "ASD_ADD_MISSING_XML_NAME_TAGS";

    @Override
    public void fix(MfTransformerContext context,
                    MfAbstractChildTaggedNamedElement<?> srcElement,
                    MfAbstractChildTaggedNamedElement.Builder<?, ?, ?> tgtBuilder) {
        final MfTransformerResult result;
        if (matches(srcElement)) {
            tgtBuilder.meta(FIXED_META_NAME, NAME, FIXED_META_SEPARATOR);
            result = MfTransformerResult.MODIFIED;
        } else {
            result = MfTransformerResult.UNCHANGED;
        }
        context.getStats().add(srcElement.getLocation(),
                               NAME,
                               result);
    }

    @Override
    public void postProcess(MfTransformerContext context,
                            MfAbstractChildTaggedNamedElement<?> srcElement,
                            MfAbstractChildTaggedNamedElement<?> tgtElement) {
        if (matches(srcElement)) {
            final MfTag tag =
                    tgtElement.tag()
                              .id(srcElement.getId() + "TXN")
                              .name(AsdTagName.XML_NAME)
                              .value("tbd_" + srcElement.getName().toLowerCase())
                              .build();
            tag.documentation()
               .id(tag.getId() + "D")
               .text("This tag was generated. It is missing in the original data model.")
               .build();
        }
    }

    private static boolean matches(MfTagOwner srcElement) {
        return needsXmlNameTag(srcElement) && !hasXmlNameTag(srcElement);
    }

    public static boolean hasXmlNameTag(MfTagOwner element) {
        return element.wrap(AsdElement.class).hasTags(AsdTagName.XML_NAME);
    }

    public static boolean needsXmlNameTag(MfTagOwner element) {
        // TODO
        final AsdElement ae = element.wrap(AsdElement.class);
        return ae.is(AsdStereotypeName.CLASS)
                || ae.is(AsdStereotypeName.RELATIONSHIP)
                || ae.is(AsdStereotypeName.METACLASS)
                || ae.is(AsdStereotypeName.EXCHANGE)
                || ae.is(AsdStereotypeName.SELECT)
                || ae.is(AsdStereotypeName.CHARACTERISTIC)
                || ae.is(AsdStereotypeName.KEY)
                || ae.is(AsdStereotypeName.COMPOSITE_KEY)
                || ae.is(AsdStereotypeName.RELATIONSHIP_KEY);
    }
}