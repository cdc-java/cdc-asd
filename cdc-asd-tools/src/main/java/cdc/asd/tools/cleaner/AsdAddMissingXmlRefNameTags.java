package cdc.asd.tools.cleaner;

import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdElement;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfType;
import cdc.mf.transform.MfElementFixer;
import cdc.mf.transform.MfTransformerContext;
import cdc.mf.transform.MfTransformerResult;

public class AsdAddMissingXmlRefNameTags
        implements MfElementFixer<MfType, MfType.Builder<?, ?, ?>> {
    public static final String NAME = "ASD_ADD_MISSING_XML_REF_NAME_TAGS";

    @Override
    public void fix(MfTransformerContext context,
                    MfType srcElement,
                    MfType.Builder<?, ?, ?> tgtBuilder) {
        final MfTransformerResult result;
        if (matches(srcElement)) {
            tgtBuilder.meta(FIXED_META_NAME, NAME, FIXED_META_SEPARATOR);
            result = MfTransformerResult.MODIFIED;
        } else {
            result = MfTransformerResult.UNCHANGED;
        }
        context.getStats().add(srcElement.getLocation(),
                               NAME,
                               result);
    }

    @Override
    public void postProcess(MfTransformerContext context,
                            MfType srcElement,
                            MfType tgtElement) {
        if (matches(srcElement)) {
            final String xmlName = srcElement.getTagValue(AsdTagName.XML_NAME) == null
                    ? srcElement.getName().toLowerCase()
                    : srcElement.getTagValue(AsdTagName.XML_NAME);

            final MfTag tag =
                    tgtElement.tag()
                              .id(srcElement.getId() + "TXRN")
                              .name(AsdTagName.XML_REF_NAME)
                              .value("tbd_" + xmlName + "Ref")
                              .build();
            tag.documentation()
               .id(tag.getId() + "D")
               .text("This tag was generated. It is missing in the original data model.")
               .build();
        }
    }

    private static boolean matches(MfType srcElement) {
        return needsXmlRefNameTag(srcElement) && !hasXmlRefNameTag(srcElement);
    }

    public static boolean hasXmlRefNameTag(MfType element) {
        return element.wrap(AsdElement.class).hasTags(AsdTagName.XML_REF_NAME);
    }

    public static boolean needsXmlRefNameTag(MfType element) {
        // <<primitive>> can not have this tag, but the value is use by code generator
        final AsdElement ae = element.wrap(AsdElement.class);
        return ae.is(AsdStereotypeName.CLASS)
                || ae.is(AsdStereotypeName.RELATIONSHIP)
                || ae.is(AsdStereotypeName.METACLASS)
        /* || ae.is(AsdStereotypeName.PRIMITIVE) */;
    }
}