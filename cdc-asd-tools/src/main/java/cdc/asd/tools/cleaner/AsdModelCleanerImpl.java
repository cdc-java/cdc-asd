package cdc.asd.tools.cleaner;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.asd.model.wrappers.AsdUtils;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfDocumentation;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfMemberOwner;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfPackageOwner;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTagOwner;
import cdc.mf.model.MfTypeOwner;
import cdc.mf.transform.MfChildTransformer;
import cdc.mf.transform.MfElementFixer;
import cdc.mf.transform.MfModelTransformer;
import cdc.mf.transform.MfTransformer;
import cdc.mf.transform.MfTransformerStats;
import cdc.mf.transform.defaults.MfDocumentationTextRemoveExtraSpaces;
import cdc.mf.transform.defaults.MfDocumentationTextRemoveHtmlTags;
import cdc.mf.transform.defaults.MfDocumentationTextUnescapeHtml;
import cdc.mf.transform.defaults.MfElementAddMetas;
import cdc.mf.transform.defaults.MfNamedMakeUnique;
import cdc.mf.transform.defaults.MfTagValueRemoveExtraSpaces;

public final class AsdModelCleanerImpl {
    private final MfTransformer transformer;

    public enum Hint {
        ADD_MISSING_XML_NAME_TAGS,
        ADD_MISSING_XML_REF_NAME_TAGS,
        DEDUPLICATE_NAMES,
        FIX_TAG_NAME_CASE,
        REMOVE_EXTRA_SPACES,
        REMOVE_HTML_TAGS,
        REPLACE_HTML_ENTITIES,
        SPLIT_REF_TAGS,

        CREATE_ENUMERATIONS,
        CREATE_BASE_OBJECT_INHERITANCE;

        public boolean isFix() {
            return this != CREATE_ENUMERATIONS
                    && this != CREATE_BASE_OBJECT_INHERITANCE;
        }
    }

    public AsdModelCleanerImpl(Set<Hint> hints) {
        final MfTransformer.Builder builder = MfTransformer.builder();

        final Map<String, String> modelMetas = new HashMap<>();
        modelMetas.put("clean-hints", hints.stream().sorted().map(Hint::name).collect(Collectors.joining(" ")));

        MfElementFixer<MfModel, MfModel.Builder> modelFixer =
                MfElementFixer.init(MfModel.class);
        modelFixer = modelFixer.andThen(new MfElementAddMetas(modelMetas));

        MfElementFixer<MfClass, MfClass.Builder<? extends MfTypeOwner>> classFixer =
                MfElementFixer.init(MfClass.class);
        MfElementFixer<MfDocumentation, MfDocumentation.Builder<? extends MfElement>> documentationFixer =
                MfElementFixer.init(MfDocumentation.class);
        MfElementFixer<MfInterface, MfInterface.Builder<? extends MfTypeOwner>> interfaceFixer =
                MfElementFixer.init(MfInterface.class);
        MfElementFixer<MfPackage, MfPackage.Builder<? extends MfPackageOwner>> packageFixer =
                MfElementFixer.init(MfPackage.class);
        MfElementFixer<MfTag, MfTag.Builder<? extends MfTagOwner>> tagFixer =
                MfElementFixer.init(MfTag.class);
        MfElementFixer<MfProperty, MfProperty.Builder<? extends MfMemberOwner>> propertyFixer =
                MfElementFixer.init(MfProperty.class);

        if (hints.contains(Hint.REPLACE_HTML_ENTITIES)) {
            documentationFixer = documentationFixer.andThen(new MfDocumentationTextUnescapeHtml());
        }

        if (hints.contains(Hint.REMOVE_HTML_TAGS)) {
            documentationFixer = documentationFixer.andThen(new MfDocumentationTextRemoveHtmlTags());
        }

        if (hints.contains(Hint.REMOVE_EXTRA_SPACES)) {
            documentationFixer = documentationFixer.andThen(new MfDocumentationTextRemoveExtraSpaces());
            tagFixer = tagFixer.andThen(new MfTagValueRemoveExtraSpaces());
        }

        if (hints.contains(Hint.FIX_TAG_NAME_CASE)) {
            tagFixer = tagFixer.andThen(new AsdFixTagNameCase());
        }

        if (hints.contains(Hint.SPLIT_REF_TAGS)) {
            // Do this last, because the splitter can create additional tags that would not be cleaned
            tagFixer = tagFixer.andThen(new AsdSplitRefTags());
        }

        if (hints.contains(Hint.DEDUPLICATE_NAMES)) {
            classFixer = classFixer.andThen(new MfNamedMakeUnique());
            interfaceFixer = interfaceFixer.andThen(new MfNamedMakeUnique());
            packageFixer = packageFixer.andThen(new MfNamedMakeUnique());
        }

        if (hints.contains(Hint.ADD_MISSING_XML_NAME_TAGS)) {
            classFixer = classFixer.andThen(new AsdAddMissingXmlNameTags());
            interfaceFixer = interfaceFixer.andThen(new AsdAddMissingXmlNameTags());
            propertyFixer = propertyFixer.andThen(new AsdAddMissingXmlNameTags());
        }
        if (hints.contains(Hint.ADD_MISSING_XML_REF_NAME_TAGS)) {
            classFixer = classFixer.andThen(new AsdAddMissingXmlRefNameTags());
            interfaceFixer = interfaceFixer.andThen(new AsdAddMissingXmlRefNameTags());
        }

        if (hints.contains(Hint.CREATE_ENUMERATIONS)) {
            propertyFixer = propertyFixer.andThen(new AsdCreateEnumerations());
        }

        if (hints.contains(Hint.CREATE_BASE_OBJECT_INHERITANCE)) {
            classFixer = classFixer.andThen(new AsdCreateBaseObjectInheritance());
        }

        builder.modelTransformer(MfModelTransformer.of(modelFixer));
        builder.childTransformer(MfChildTransformer.of(MfClass.class, classFixer));
        builder.childTransformer(MfChildTransformer.of(MfDocumentation.class, documentationFixer));
        builder.childTransformer(MfChildTransformer.of(MfInterface.class, interfaceFixer));
        builder.childTransformer(MfChildTransformer.of(MfPackage.class, packageFixer));
        builder.childTransformer(MfChildTransformer.of(MfProperty.class, propertyFixer));
        builder.childTransformer(MfChildTransformer.of(MfTag.class, tagFixer));

        this.transformer = builder.build();
    }

    public MfModel transform(MfModel model) {
        return transformer.transform(model, AsdUtils.FACTORY);
    }

    public MfTransformerStats getStats() {
        return transformer.getContext().getStats();
    }

    public static MfModel clean(MfModel model,
                                Hint... hints) {
        return clean(model, Set.of(hints));
    }

    public static MfModel clean(MfModel model,
                                Set<Hint> hints) {
        final AsdModelCleanerImpl cleaner = new AsdModelCleanerImpl(hints);
        return cleaner.transform(model);
    }
}