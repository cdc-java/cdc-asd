package cdc.asd.tools;

import java.io.File;
import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.Config;
import cdc.asd.model.wrappers.AsdUtils;
import cdc.asd.specgen.S1000DDataDictionaryXmlIo;
import cdc.asd.specgen.S1000DGlossaryXmlIo;
import cdc.asd.specgen.S1000DXmlIo;
import cdc.asd.tools.cleaner.AsdModelCleanerImpl;
import cdc.mf.model.MfModel;
import cdc.mf.model.io.MfModelXmlIo;
import cdc.util.cli.AbstractMainSupport;
import cdc.util.cli.FeatureMask;
import cdc.util.cli.MainResult;
import cdc.util.cli.OptionEnum;
import cdc.util.time.Chronometer;

/**
 * Utility used to generate S1000 Data Modules (spec and glossary) from an EA XML Data Model.
 *
 * @author Damien Carbonne
 */
public final class AsdModelToS1000D {
    private static final Logger LOGGER = LogManager.getLogger(AsdModelToS1000D.class);
    private final MainArgs margs;

    public static class MainArgs {
        /** Input ASD MF XML model file. */
        public File modelFile;
        /** Input ASD MF XML model file (in a previous issue for comparison) */
        public File previousModelFile;
        /** Output directory that will contain export(s). */
        public File outputDir;
        /** Base name of generated files. */
        public String basename;
        /** Set of enabled features. */
        public final FeatureMask<Feature> features = new FeatureMask<>();

        public boolean hasTarget() {
            return features.isEnabled(Feature.GLOSSARY)
                    || features.isEnabled(Feature.SPEC)
                    || features.isEnabled(Feature.DATA_DICTIONARY_DEFINITIONS);
        }

        public enum Feature implements OptionEnum {
            SPEC("spec", "Generate data model spec data modules (S2000M, S3000L, ...)."),
            GLOSSARY("glossary", "Generate glossary data modules (SX001G)."),
            DATA_DICTIONARY_DEFINITIONS("definitions", "Generate the data dictionary definitions (S3000L)."),
            CLEAN("clean", "Clean model before converting it to S1000D."),
            VERBOSE("verbose", "Prints messages.");

            private final String name;
            private final String description;

            private Feature(String name,
                            String description) {
                this.name = name;
                this.description = description;
            }

            @Override
            public final String getName() {
                return name;
            }

            @Override
            public final String getDescription() {
                return description;
            }
        }
    }

    private AsdModelToS1000D(MainArgs margs) {
        this.margs = margs;
    }

    private void log(String message,
                     Object... params) {
        if (margs.features.isEnabled(MainArgs.Feature.VERBOSE)) {
            LOGGER.info(message, params);
        }
    }

    private void execute() throws IOException {
        final Chronometer chrono = new Chronometer();
        MfModel previousModel = null;
        margs.outputDir.mkdirs();

        log("Loading model {}", margs.modelFile);
        chrono.start();
        MfModel model = MfModelXmlIo.load(margs.modelFile, AsdUtils.FACTORY);
        chrono.suspend();
        log("Loaded model ({})", chrono);

        if (margs.previousModelFile != null) {
            log("Loading previous model {}", margs.previousModelFile);
            chrono.start();
            previousModel = MfModelXmlIo.load(margs.previousModelFile, AsdUtils.FACTORY);
            chrono.suspend();
            log("Loaded previous model ({})", chrono);

            if (margs.features.isEnabled(MainArgs.Feature.CLEAN)) {
                log("Cleaning previous model");
                chrono.start();
                previousModel = AsdModelCleanerImpl.clean(previousModel,
                                                          AsdModelCleanerImpl.Hint.values());
                chrono.suspend();
                log("Cleaned previous model ({})", chrono);
            }
        }

        if (margs.features.isEnabled(MainArgs.Feature.CLEAN)) {
            log("Cleaning model");
            chrono.start();
            model = AsdModelCleanerImpl.clean(model,
                                              AsdModelCleanerImpl.Hint.values());
            chrono.suspend();
            log("Cleaned model ({})", chrono);
        }

        if (margs.features.isEnabled(MainArgs.Feature.SPEC) || !margs.hasTarget()) {
            log("Generating S1000D Data Model DM into {}", margs.outputDir);
            chrono.start();
            S1000DXmlIo.save(margs.outputDir, margs.basename, model, previousModel);
            chrono.suspend();
            log("Generated S1000D Data Model DM ({})", chrono);
        }

        if (margs.features.isEnabled(MainArgs.Feature.GLOSSARY)) {
            log("Generating S1000D Glossary DM into {}", margs.outputDir);
            chrono.start();
            S1000DGlossaryXmlIo.save(margs.outputDir, margs.basename, model, previousModel);
            chrono.suspend();
            log("Generated S1000D Glossary DM ({})", chrono);
        }

        if (margs.features.isEnabled(MainArgs.Feature.DATA_DICTIONARY_DEFINITIONS)) {
            log("Generating S1000D Data Dictionary definitions DM into {}", margs.outputDir);
            chrono.start();
            S1000DDataDictionaryXmlIo.save(margs.outputDir, margs.basename, model);
            chrono.suspend();
            log("Generated Data Dictionary definitions DM ({})", chrono);
        }
    }

    public static void execute(MainArgs margs) throws IOException {
        final AsdModelToS1000D instance = new AsdModelToS1000D(margs);
        instance.execute();
    }

    public static MainResult exec(String... args) {
        final MainSupport support = new MainSupport();
        support.main(args);
        return support.getResult();
    }

    public static void main(String... args) {
        final int code = exec(args).getCode();
        System.exit(code);
    }

    private static class MainSupport extends AbstractMainSupport<MainArgs, Void> {
        private static final String BASENAME = "basename";
        private static final String MODEL_FILE = "model";
        private static final String PREVIOUS_MODEL_FILE = "previous-model";

        public MainSupport() {
            super(AsdModelToS1000D.class, LOGGER);
        }

        @Override
        protected String getVersion() {
            return Config.VERSION;
        }

        @Override
        protected boolean addArgsFileOption(Options options) {
            return true;
        }

        @Override
        protected String getHelpHeader() {
            return "Utility that converts XML EA model to S1000D Spec (S2000M, S3000L, ...) and/or Glossary (SX001G) Data Modules."
                    + "\nIf no generation target is defined, data model DM are generated.";
        }

        @Override
        protected String getHelpFooter() {
            return null;
        }

        @Override
        protected void addSpecificOptions(Options options) {
            options.addOption(Option.builder()
                                    .longOpt(MODEL_FILE)
                                    .desc("Mandatory name of the ASD MF XML model to transform.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(BASENAME)
                                    .desc("Optional base name of generated files.")
                                    .hasArg()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(OUTPUT_DIR)
                                    .desc("Mandatory name of the output directory. If this directory does not exist, it is created.")
                                    .hasArg()
                                    .required()
                                    .build());
            options.addOption(Option.builder()
                                    .longOpt(PREVIOUS_MODEL_FILE)
                                    .desc("Optional name of the ASD MF XML model in a previous version for comparison.")
                                    .hasArg()
                                    .build());

            addNoArgOptions(options, MainArgs.Feature.class);
        }

        @Override
        protected MainArgs analyze(CommandLine cl) throws ParseException {
            final MainArgs margs = new MainArgs();
            margs.modelFile = getValueAsResolvedFile(cl, MODEL_FILE, IS_FILE);
            margs.basename = getValueAsString(cl, BASENAME, "");
            margs.outputDir = getValueAsResolvedFile(cl, OUTPUT_DIR);
            margs.previousModelFile = getValueAsResolvedFile(cl, PREVIOUS_MODEL_FILE, IS_NULL_OR_FILE);

            setMask(cl, MainArgs.Feature.class, margs.features::setEnabled);

            return margs;
        }

        @Override
        protected Void execute(MainArgs margs) throws IOException {
            AsdModelToS1000D.execute(margs);
            return null;
        }
    }
}