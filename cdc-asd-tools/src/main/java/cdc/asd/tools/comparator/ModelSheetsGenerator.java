package cdc.asd.tools.comparator;

import java.io.IOException;
import java.util.List;

import cdc.mf.model.MfModel;
import cdc.office.ss.WorkbookWriter;
import cdc.office.tables.TableSection;
import cdc.util.time.Chronometer;

/**
 * Class used to dump each model to a sheet, and all models to one sheet.
 */
final class ModelSheetsGenerator {
    private final AsdModelsComparatorImpl context;

    private ModelSheetsGenerator(AsdModelsComparatorImpl context) {
        this.context = context;
    }

    public static void generateSheets(AsdModelsComparatorImpl context,
                                      WorkbookWriter<?> writer) throws IOException {
        final ModelSheetsGenerator generator = new ModelSheetsGenerator(context);
        for (final MfModel model : context.models) {
            generator.dump(writer, model);
        }
        generator.dumpAll(writer);
    }

    private void dump(WorkbookWriter<?> writer,
                      MfModel model) throws IOException {
        final Chronometer chrono = new Chronometer();
        final String sheetName = model.getName();
        context.info("Generating " + sheetName);
        chrono.start();

        writer.beginSheet(sheetName);
        writer.beginRow(TableSection.HEADER);
        writer.addCell(AsdModelsComparatorImpl.KIND);
        writer.addCell(AsdModelsComparatorImpl.ID);
        writer.addCell(AsdModelsComparatorImpl.NAME);
        writer.addCell(AsdModelsComparatorImpl.NAME_KERNEL);
        writer.addCell(AsdModelsComparatorImpl.DISCRIMINATOR);
        writer.addCell(AsdModelsComparatorImpl.STEREOTYPE);
        writer.addCell(AsdModelsComparatorImpl.EFFECTIVE_STEREOTYPE);
        writer.addCell(AsdModelsComparatorImpl.NOTES);
        writer.addCell(AsdModelsComparatorImpl.TAGS);
        writer.addCell(AsdModelsComparatorImpl.CARDINALITY);

        final List<Data> datas = AsdModelsComparatorImpl.collectDatas(model);

        for (final Data data : datas) {
            writer.beginRow(TableSection.DATA);
            writer.addCell(data.kind());
            writer.addCell(data.id());
            writer.addCell(data.name());
            writer.addCell(data.nameKernel());
            writer.addCell(data.discriminator());
            writer.addCell(data.sterotype());
            writer.addCell(data.effectiveStereotype());
            writer.addCell(data.notes());
            writer.addCell(data.tags());
            writer.addCell(data.cardinality());
        }

        chrono.suspend();
        context.info("Generated " + sheetName + " (" + chrono + ")");
    }

    private void dumpAll(WorkbookWriter<?> writer) throws IOException {
        final Chronometer chrono = new Chronometer();
        final String sheetName = "All";
        context.info("Generating " + sheetName);
        chrono.start();

        writer.beginSheet(sheetName);
        writer.beginRow(TableSection.HEADER);
        writer.addCell(AsdModelsComparatorImpl.MODEL);
        writer.addCell(AsdModelsComparatorImpl.KIND);
        writer.addCell(AsdModelsComparatorImpl.ID);
        writer.addCell(AsdModelsComparatorImpl.NAME);
        writer.addCell(AsdModelsComparatorImpl.NAME_KERNEL);
        writer.addCell(AsdModelsComparatorImpl.DISCRIMINATOR);
        writer.addCell(AsdModelsComparatorImpl.STEREOTYPE);
        writer.addCell(AsdModelsComparatorImpl.EFFECTIVE_STEREOTYPE);
        writer.addCell(AsdModelsComparatorImpl.NOTES);
        writer.addCell(AsdModelsComparatorImpl.TAGS);
        writer.addCell(AsdModelsComparatorImpl.CARDINALITY);

        for (final MfModel model : context.models) {
            dumpAll(writer, model);
        }
        chrono.suspend();
        context.info("Generated " + sheetName + " (" + chrono + ")");
    }

    private static void dumpAll(WorkbookWriter<?> writer,
                                MfModel model) throws IOException {
        final List<Data> datas = AsdModelsComparatorImpl.collectDatas(model);

        for (final Data data : datas) {
            writer.beginRow(TableSection.DATA);
            writer.addCell(model.getName());
            writer.addCell(data.kind());
            writer.addCell(data.id());
            writer.addCell(data.name());
            writer.addCell(data.nameKernel());
            writer.addCell(data.discriminator());
            writer.addCell(data.sterotype());
            writer.addCell(data.effectiveStereotype());
            writer.addCell(data.notes());
            writer.addCell(data.tags());
            writer.addCell(data.cardinality());
        }
    }
}