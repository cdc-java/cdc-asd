package cdc.asd.tools.comparator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;

import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdPackage;
import cdc.mf.model.MfCardinalityItem;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfImplementation;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfSpecialization;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfUtils;
import cdc.office.ss.WorkbookWriter;
import cdc.office.ss.WorkbookWriterFactory;
import cdc.office.ss.WorkbookWriterFeatures;
import cdc.util.strings.StringUtils;

public class AsdModelsComparatorImpl {
    static final String MODEL = "Model";
    static final String KIND = "Kind";
    static final String ID = "Id";
    static final String DISCRIMINATOR = "Discriminator";
    static final String NAME = "Name";
    static final String NAME_KERNEL = "Name Kernel";
    static final String STEREOTYPE = "Stereotype";
    static final String EFFECTIVE_STEREOTYPE = "Effectve Stereotype";
    static final String NOTES = "Notes";
    static final String TAGS = "Tags";
    static final String CARDINALITY = "Cardinality";

    final List<MfModel> models = new ArrayList<>();
    final Set<Hint> hints;
    private final WorkbookWriterFeatures features;
    private final Logger logger;

    public enum Hint {
        SHOW_CARDINALITIES,
        SHOW_IDS,
        SHOW_NOTES,
        SHOW_STEREOTYPES,
        SHOW_TAGS,
        DIFF_ID,
        DIFF_NAME,
        VERBOSE
    }

    public AsdModelsComparatorImpl(List<MfModel> models,
                                   Set<Hint> hints,
                                   WorkbookWriterFeatures features,
                                   Logger logger) {
        this.models.addAll(models);
        this.hints = EnumSet.copyOf(hints);
        this.features = WorkbookWriterFeatures.builder()
                                              .set(features)
                                              .enable(WorkbookWriterFeatures.Feature.RICH_TEXT)
                                              .build();
        this.logger = logger;
    }

    public void compare(File output) throws IOException {
        final WorkbookWriterFactory factory = new WorkbookWriterFactory();
        factory.setEnabled(WorkbookWriterFactory.Hint.POI_STREAMING, true);
        try (WorkbookWriter<?> writer = factory.create(output, features)) {
            ElementSheetsGenerator.generateSheets(this, writer);
            SynthesisSheetGenerator.generateSheet(this, writer);
            ModelSheetsGenerator.generateSheets(this, writer);
            if (hints.contains(Hint.DIFF_ID) || hints.contains(Hint.DIFF_NAME)) {
                ModelsDiffSheetsGenerator.generateSheets(this, writer);
            }
            writer.flush();
        }
    }

    void info(String message) {
        if (hints.contains(Hint.VERBOSE)) {
            logger.info(message);
        }
    }

    void warn(String message) {
        if (hints.contains(Hint.VERBOSE)) {
            logger.warn(message);
        }
    }

    static String getKind(MfElement element) {
        return element instanceof MfProperty ? "attribute" : MfUtils.getKind(element);
    }

    static String getKind(Class<? extends MfElement> cls) {
        return MfProperty.class.equals(cls) ? "attribute" : MfUtils.getKind(cls);
    }

    static String getName(MfElement element) {
        if (element instanceof final MfTip tip) {
            return (StringUtils.isNullOrEmpty(tip.getName()) ? "?" : tip.getName())
                    + ":" + tip.getType().getName();
        } else if (element instanceof final MfImplementation e) {
            return e.getSource().getName() + " ..> " + e.getTarget().getName();
        } else if (element instanceof final MfSpecialization e) {
            return e.getSource().getName() + " --> " + e.getTarget().getName();
        } else if (element instanceof final MfNameItem e) {
            return e.getName();
        } else {
            throw new IllegalArgumentException();
        }
    }

    static String getNameKernel(MfElement element) {
        if (element instanceof final MfPackage p) {
            return element.wrap(AsdPackage.class).getNameKernel();
        } else {
            return getName(element);
        }

    }

    static boolean accept(MfElement element) {
        if (element instanceof final MfTip tip) {
            return tip.isNavigable();
        } else {
            return true;
        }
    }

    static String getTags(MfElement element) {
        return element.getChildren(MfTag.class)
                      .stream()
                      .sorted(MfTag.NAME_VALUE_COMPARATOR)
                      .map(tag -> tag.getName() + ":" + tag.getValue())
                      .collect(Collectors.joining("\n"));
    }

    static String getCardinality(MfElement element) {
        if (element instanceof final MfCardinalityItem i) {
            return i.getEffectiveCardinality().toExpandedString();
        } else {
            return "";
        }
    }

    static String getNotes(MfElement element) {
        return element.wrap(AsdElement.class).getNotes();
    }

    static String getStereotype(MfElement element) {
        return element.wrap(AsdElement.class).getStereotype();
    }

    static String getEffectiveStereotype(MfElement element) {
        return element.wrap(AsdElement.class).getEffectiveStereotype();
    }

    static Data of(MfElement element,
                   String discriminator) {
        return new Data(getKind(element),
                        element.getId(),
                        getName(element),
                        getNameKernel(element),
                        getStereotype(element),
                        getEffectiveStereotype(element),
                        discriminator,
                        getNotes(element),
                        getTags(element),
                        getCardinality(element));
    }

    private static void addDatas(List<Data> datas,
                                 Class<? extends MfElement> cls,
                                 MfModel model) {
        final Map<String, List<MfElement>> map = new HashMap<>();
        final Map<MfElement, String> discriminators = new HashMap<>();
        for (final MfElement element : model.collect(cls)
                                            .stream()
                                            .filter(AsdModelsComparatorImpl::accept)
                                            .toList()) {
            final List<MfElement> list = map.computeIfAbsent(getName(element), k -> new ArrayList<>());
            list.add(element);
        }
        for (final List<MfElement> list : map.values()) {
            int index = 0;
            for (final MfElement element : list) {
                index++;
                discriminators.put(element, "#" + index);
            }
        }
        for (final MfElement x : model.collect(cls)
                                      .stream()
                                      .filter(AsdModelsComparatorImpl::accept)
                                      .toList()) {
            datas.add(of(x, discriminators.get(x)));
        }
    }

    static List<Data> collectDatas(MfModel model,
                                   Class<? extends MfElement> cls) {
        final List<Data> records = new ArrayList<>();
        addDatas(records, cls, model);
        return records;
    }

    static List<Data> collectDatas(MfModel model) {
        final List<Data> records = new ArrayList<>();
        addDatas(records, MfPackage.class, model);
        addDatas(records, MfClass.class, model);
        addDatas(records, MfInterface.class, model);
        addDatas(records, MfProperty.class, model);
        addDatas(records, MfTip.class, model);
        addDatas(records, MfImplementation.class, model);
        addDatas(records, MfSpecialization.class, model);
        return records;
    }
}