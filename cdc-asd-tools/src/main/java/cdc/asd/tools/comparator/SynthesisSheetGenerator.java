package cdc.asd.tools.comparator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfImplementation;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfSpecialization;
import cdc.mf.model.MfTip;
import cdc.office.ss.WorkbookWriter;
import cdc.office.tables.TableSection;
import cdc.util.time.Chronometer;

/**
 * Class use to generate the synthesis for a particular element type of all models.
 */
final class SynthesisSheetGenerator {
    private final String kind;
    private final List<Map<SynthesisSheetGenerator.Key, List<Data>>> maps = new ArrayList<>();
    private final Set<SynthesisSheetGenerator.Key> keys = new HashSet<>();

    private record Key(String effectiveStereotype,
                       String nameKernel)
            implements Comparable<SynthesisSheetGenerator.Key> {

        @Override
        public int compareTo(SynthesisSheetGenerator.Key o) {
            final int nameCmp = nameKernel.compareTo(o.nameKernel);
            if (nameCmp == 0) {
                final String s1 = effectiveStereotype == null ? "" : effectiveStereotype;
                final String s2 = o.effectiveStereotype == null ? "" : o.effectiveStereotype;
                return s1.compareTo(s2);
            } else {
                return nameCmp;
            }
        }
    }

    private SynthesisSheetGenerator(AsdModelsComparatorImpl context,
                                    Class<? extends MfElement> cls) {
        this.kind = AsdModelsComparatorImpl.getKind(cls);
        for (final MfModel model : context.models) {
            final Map<SynthesisSheetGenerator.Key, List<Data>> map = new HashMap<>();
            maps.add(map);
            for (final Data x : AsdModelsComparatorImpl.collectDatas(model, cls)) {
                if (x.nameKernel() != null) {
                    final SynthesisSheetGenerator.Key key = new Key(x.effectiveStereotype(),
                                                                    x.nameKernel());
                    final List<Data> list = map.computeIfAbsent(key, k -> new ArrayList<>());
                    list.add(x);
                    keys.add(key);
                }
            }
        }
    }

    public static void generateSheet(AsdModelsComparatorImpl context,
                                     WorkbookWriter<?> writer) throws IOException {
        final Chronometer chrono = new Chronometer();
        final String sheetName = "Synthesis";
        context.info("Generating " + sheetName);
        chrono.start();
        writer.beginSheet(sheetName);

        writer.beginRow(TableSection.HEADER);
        writer.addCell("Effective Stereotype");
        writer.addCell("Kind");
        writer.addCell("Name Kernel");
        writer.addCell("Count");
        for (final MfModel model : context.models) {
            writer.addCell(model.getName());
        }

        compare(writer, context, MfPackage.class);
        compare(writer, context, MfClass.class);
        compare(writer, context, MfInterface.class);
        compare(writer, context, MfProperty.class);
        compare(writer, context, MfTip.class);
        compare(writer, context, MfImplementation.class);
        compare(writer, context, MfSpecialization.class);

        chrono.suspend();
        context.info("Generated " + sheetName + " (" + chrono + ")");
    }

    private void compare(WorkbookWriter<?> writer) throws IOException {
        // Rows
        for (final SynthesisSheetGenerator.Key key : keys.stream().sorted().toList()) {
            writer.beginRow(TableSection.DATA);
            writer.addCell(key.effectiveStereotype == null ? "" : key.effectiveStereotype);
            writer.addCell(kind);
            writer.addCell(key.nameKernel);
            int count = 0;
            for (final Map<SynthesisSheetGenerator.Key, List<Data>> map : maps) {
                if (map.containsKey(key)) {
                    count++;
                }
            }
            writer.addCell(count);
            for (final Map<SynthesisSheetGenerator.Key, List<Data>> map : maps) {
                if (map.containsKey(key)) {
                    final List<Data> list = map.get(key);
                    writer.addCell("X" + list.size());
                } else {
                    writer.addEmptyCell();
                }
            }
        }
    }

    private static void compare(WorkbookWriter<?> writer,
                                AsdModelsComparatorImpl context,
                                Class<? extends MfElement> cls) throws IOException {
        final SynthesisSheetGenerator collector = new SynthesisSheetGenerator(context, cls);
        collector.compare(writer);
    }
}