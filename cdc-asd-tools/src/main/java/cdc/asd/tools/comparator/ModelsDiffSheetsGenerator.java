package cdc.asd.tools.comparator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cdc.asd.model.AsdEaNaming;
import cdc.mf.model.MfModel;
import cdc.office.ss.WorkbookWriter;
import cdc.office.tables.Header;
import cdc.office.tables.Row;
import cdc.office.tables.diff.KeyedTableDiff;
import cdc.office.tools.KeyedTableDiffExporter;
import cdc.util.time.Chronometer;

/**
 * Class used to compare pairs of models and generate corresponding sheets.
 */
final class ModelsDiffSheetsGenerator {
    private final AsdModelsComparatorImpl context;
    private final Header header;

    private ModelsDiffSheetsGenerator(AsdModelsComparatorImpl context) {
        this.context = context;
        this.header = Header.builder()
                            .names(AsdModelsComparatorImpl.KIND,
                                   AsdModelsComparatorImpl.ID,
                                   AsdModelsComparatorImpl.NAME,
                                   AsdModelsComparatorImpl.DISCRIMINATOR,
                                   AsdModelsComparatorImpl.STEREOTYPE,
                                   AsdModelsComparatorImpl.NOTES,
                                   AsdModelsComparatorImpl.TAGS,
                                   AsdModelsComparatorImpl.CARDINALITY)
                            .build();
    }

    public static void generateSheets(AsdModelsComparatorImpl context,
                                      WorkbookWriter<?> writer) throws IOException {
        final ModelsDiffSheetsGenerator generator = new ModelsDiffSheetsGenerator(context);
        generator.generateSheets(writer);
    }

    private void generateSheets(WorkbookWriter<?> writer) throws IOException {
        final List<List<Row>> tables = new ArrayList<>();
        for (final MfModel model : context.models) {
            tables.add(toTable(model));
        }

        for (int index1 = 0; index1 < tables.size(); index1++) {
            final List<Row> table1 = tables.get(index1);
            final MfModel model1 = context.models.get(index1);
            for (int index2 = index1 + 1; index2 < tables.size(); index2++) {
                final List<Row> table2 = tables.get(index2);
                final MfModel model2 = context.models.get(index2);
                generateSheet(writer, model1, table1, model2, table2);
            }
        }
    }

    private static List<Row> toTable(MfModel model) {
        final List<Row> rows = new ArrayList<>();

        for (final Data data : AsdModelsComparatorImpl.collectDatas(model)) {
            rows.add(Row.builder()
                        .addValue(data.kind())
                        .addValue(data.id())
                        .addValue(data.name())
                        .addValue(data.discriminator())
                        .addValue(data.sterotype())
                        .addValue(data.notes())
                        .addValue(data.tags())
                        .addValue(data.cardinality())
                        .build());
        }
        return rows;
    }

    private void generateSheet(WorkbookWriter<?> writer,
                               MfModel model1,
                               List<Row> table1,
                               MfModel model2,
                               List<Row> table2) throws IOException {
        final String sheetName = AsdEaNaming.compress(model1.getName()) + "-" + AsdEaNaming.compress(model2.getName());
        final Chronometer chrono = new Chronometer();
        context.info("Generating " + sheetName);
        chrono.start();

        final List<String> keyNames = new ArrayList<>();
        if (context.hints.contains(AsdModelsComparatorImpl.Hint.DIFF_ID)) {
            Collections.addAll(keyNames,
                               AsdModelsComparatorImpl.KIND,
                               AsdModelsComparatorImpl.ID);
        } else if (context.hints.contains(AsdModelsComparatorImpl.Hint.DIFF_NAME)) {
            Collections.addAll(keyNames,
                               AsdModelsComparatorImpl.KIND,
                               AsdModelsComparatorImpl.NAME,
                               AsdModelsComparatorImpl.DISCRIMINATOR);
        }

        final KeyedTableDiff diff = KeyedTableDiff.builder()
                                                  .leftSystemId(model1.getName())
                                                  .leftHeader(header)
                                                  .leftRows(table1)
                                                  .rightSystemId(model2.getName())
                                                  .rightHeader(header)
                                                  .rightRows(table2)
                                                  .keyNames(keyNames)
                                                  .warn(context::warn)
                                                  .build();

        final KeyedTableDiffExporter exporter =
                KeyedTableDiffExporter.builder()
                                      .header1(header)
                                      .header2(header)
                                      .diffSheetName(sheetName)
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_CHANGE_DETAILS)
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_UNCHANGED_LINES)
                                      .hint(KeyedTableDiffExporter.Hint.ADD_LINE_DIFF_COLUMN)
                                      .hint(KeyedTableDiffExporter.Hint.SHOW_COLORS)
                                      .hint(KeyedTableDiffExporter.Hint.SORT_LINES)
                                      .hint(KeyedTableDiffExporter.Hint.NO_INDEXING)
                                      .build();

        exporter.addDiffSheets(diff, writer);
        chrono.suspend();
        context.info("Generated " + sheetName + " (" + chrono + ")");
    }
}