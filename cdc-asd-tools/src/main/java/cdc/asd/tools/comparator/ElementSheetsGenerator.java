package cdc.asd.tools.comparator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import cdc.asd.model.wrappers.AsdUtils;
import cdc.asd.tools.comparator.AsdModelsComparatorImpl.Hint;
import cdc.mf.model.MfCardinalityItem;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfImplementation;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfSpecialization;
import cdc.mf.model.MfTip;
import cdc.office.ss.WorkbookWriter;
import cdc.office.tables.TableSection;
import cdc.util.time.Chronometer;

/**
 * Class used to compare elements of a particular type, for all models.
 */
final class ElementSheetsGenerator {
    private final AsdModelsComparatorImpl context;
    private final Class<? extends MfElement> elementClass;
    private final List<Map<String, List<Data>>> maps = new ArrayList<>();
    private final Set<String> names = new HashSet<>();

    private ElementSheetsGenerator(AsdModelsComparatorImpl context,
                                   Class<? extends MfElement> cls) {
        this.context = context;
        this.elementClass = cls;
        for (final MfModel model : context.models) {
            final Map<String, List<Data>> map = new HashMap<>();
            maps.add(map);
            for (final Data data : AsdModelsComparatorImpl.collectDatas(model, cls)) {
                final String name = data.name();
                final List<Data> list = map.computeIfAbsent(name, k -> new ArrayList<>());
                list.add(data);
                names.add(name);
            }
        }
    }

    public static void generateSheets(AsdModelsComparatorImpl context,
                                      WorkbookWriter<?> writer) throws IOException {
        ElementSheetsGenerator.compare(writer, context, MfPackage.class, "Packages", "Package");
        ElementSheetsGenerator.compare(writer, context, MfClass.class, "Classes", "Class");
        ElementSheetsGenerator.compare(writer, context, MfInterface.class, "Interfaces", "Interface");
        ElementSheetsGenerator.compare(writer, context, MfProperty.class, "Attributes", "Attribute");
        ElementSheetsGenerator.compare(writer, context, MfTip.class, "Tips", "Tip");
        ElementSheetsGenerator.compare(writer, context, MfImplementation.class, "Implementations", "Implementation");
        ElementSheetsGenerator.compare(writer, context, MfSpecialization.class, "Specializations", "Specialization");
    }

    private void header(WorkbookWriter<?> writer,
                        String title) throws IOException {
        // Header
        writer.beginRow(TableSection.HEADER);
        writer.addCell(title);
        writer.addCell("Kernel");
        for (final MfModel model : context.models) {
            writer.addCell(model.getName());
            if (context.hints.contains(Hint.SHOW_IDS)) {
                writer.addCell(model.getName() + " ids");
            }
            if (context.hints.contains(Hint.SHOW_STEREOTYPES)) {
                writer.addCell(model.getName() + " stereotype");
            }
            if (context.hints.contains(Hint.SHOW_NOTES)) {
                writer.addCell(model.getName() + " notes");
            }
            if (context.hints.contains(Hint.SHOW_TAGS)) {
                writer.addCell(model.getName() + " tags");
            }
            if (context.hints.contains(Hint.SHOW_CARDINALITIES) && MfCardinalityItem.class.isAssignableFrom(elementClass)) {
                writer.addCell(model.getName() + " cardinalities");
            }
        }
    }

    private void compare(WorkbookWriter<?> writer,
                         String sheetName,
                         String elementName) throws IOException {
        final Chronometer chrono = new Chronometer();
        context.info("Generating " + sheetName);
        chrono.start();

        writer.beginSheet(sheetName);
        header(writer, elementName);

        // Rows
        for (final String name : names.stream().sorted().toList()) {
            writer.beginRow(TableSection.DATA);
            writer.addCell(name);
            writer.addCell(AsdUtils.getKernel(name));
            for (final Map<String, List<Data>> map : maps) {
                if (map.containsKey(name)) {
                    final List<Data> list = map.get(name);
                    // The first item
                    // They should all have the same features: stereotype, tag, notes
                    // They must have different ids
                    final Data x = list.get(0);

                    if (list.isEmpty()) {
                        writer.addEmptyCell();
                    } else {
                        writer.addCell("X" + list.size());
                    }
                    // writer.addCell(StringUtils.toString('X', list.size()));
                    if (context.hints.contains(Hint.SHOW_IDS)) {
                        writer.addCell(list.stream()
                                           .map(Data::id)
                                           .collect(Collectors.joining("\n")));
                    }
                    if (context.hints.contains(Hint.SHOW_STEREOTYPES)) {
                        final boolean many = list.stream()
                                                 .map(Data::sterotype)
                                                 .distinct()
                                                 .count() > 1;
                        if (many) {
                            writer.addCell(list.stream()
                                               .map(Data::sterotype)
                                               .collect(Collectors.joining("\n")));
                        } else {
                            writer.addCell(x.sterotype());
                        }
                    }
                    if (context.hints.contains(Hint.SHOW_NOTES)) {
                        final boolean many = list.stream()
                                                 .map(Data::notes)
                                                 .distinct()
                                                 .count() > 1;
                        if (many) {
                            writer.addCell(list.stream()
                                               .map(Data::notes)
                                               .collect(Collectors.joining("\n")));
                        } else {
                            writer.addCell(x.notes());
                        }
                    }
                    if (context.hints.contains(Hint.SHOW_TAGS)) {
                        // TODO many?
                        writer.addCell(x.tags());
                    }
                    if (context.hints.contains(Hint.SHOW_CARDINALITIES) && MfCardinalityItem.class.isAssignableFrom(elementClass)) {
                        final boolean many = list.stream()
                                                 .map(Data::cardinality)
                                                 .distinct()
                                                 .count() > 1;
                        if (many) {
                            writer.addCell(list.stream()
                                               .map(Data::cardinality)
                                               .collect(Collectors.joining("\n")));
                        } else {
                            writer.addCell(x.cardinality());
                        }
                    }
                } else {
                    writer.addEmptyCell();
                    if (context.hints.contains(Hint.SHOW_IDS)) {
                        writer.addEmptyCell();
                    }
                    if (context.hints.contains(Hint.SHOW_STEREOTYPES)) {
                        writer.addEmptyCell();
                    }
                    if (context.hints.contains(Hint.SHOW_NOTES)) {
                        writer.addEmptyCell();
                    }
                    if (context.hints.contains(Hint.SHOW_TAGS)) {
                        writer.addEmptyCell();
                    }
                    if (context.hints.contains(Hint.SHOW_CARDINALITIES) && MfCardinalityItem.class.isAssignableFrom(elementClass)) {
                        writer.addEmptyCell();
                    }
                }
            }
        }
        chrono.suspend();
        context.info("Generated " + sheetName + " (" + chrono + ")");
    }

    private static void compare(WorkbookWriter<?> writer,
                                AsdModelsComparatorImpl context,
                                Class<? extends MfElement> cls,
                                String sheetName,
                                String elementName) throws IOException {
        final ElementSheetsGenerator collector = new ElementSheetsGenerator(context, cls);
        collector.compare(writer, sheetName, elementName);
    }
}