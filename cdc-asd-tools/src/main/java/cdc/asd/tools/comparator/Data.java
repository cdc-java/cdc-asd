package cdc.asd.tools.comparator;

record Data(String kind,
            String id,
            String name,
            String nameKernel,
            String sterotype,
            String effectiveStereotype,
            String discriminator,
            String notes,
            String tags,
            String cardinality) {
}