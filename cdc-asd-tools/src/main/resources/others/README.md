This directory contains private EAP models that can are accessible to DMEWG members.

They must be downloaded and installed manually.

They **MUST NOT** be published.