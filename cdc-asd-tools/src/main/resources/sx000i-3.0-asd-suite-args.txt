--eap-file
others/SX000i_3-0_Data_model_001-00.eap
--output-dir
../../../target
--append-basename

###############################
# Profiles
--profile-all

###############################
# Dump
--dump-no-force

###############################
# Model
--model-fix-direction

###############################
# Cleaner
#--clean-all
--clean-remove-html-tags
--clean-replace-html-entities
#--clean-add-missing-xml-name-tags
#--clean-add-missing-xml-ref-name-tags
#--clean-create-enumerations
#--clean-create-base-object-inheritance

###############################
# Checker
#--check-disable-all

###############################
# HTML
--html-no-force
--html-show-ids
--html-show-guids
--html-show-indices
--html-img-svg
--html-img-show-ids
--html-img-show-all-tags
--html-img-show-all-metas
--html-frames
--html-flat-dirs
#--html-img-hide-meta
#badges
--html-model-overview-ignore-package
/Builtin
.*/[A-Za-z0-9_ \-]*Compound_Attributes.*
.*/[A-Za-z0-9_ \-]*Base_Object_Definition.*
.*/[A-Za-z0-9_ \-]*Primitives.*
.*/[A-Za-z0-9_ \-]*Remark
.*/[A-Za-z0-9_ \-]*Digital File
.*/[A-Za-z0-9_ \-]*Security Classification
.*/[A-Za-z0-9_ \-]*Applicability Statement
--html-name-remove-part
^SX000i 

###############################
# S1000D
--s1000d-spec
--s1000d-glossary

###############################
# XSD
--xsd-namespace
todo

--verbose
--run-all