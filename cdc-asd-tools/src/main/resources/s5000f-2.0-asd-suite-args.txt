--eap-file
others/S5000F_2-0_Data_model_001-00.EAP
--output-dir
../../../target
--append-basename

###############################
# Profiles
--profile-all

###############################
# Dump
--dump-no-force

###############################
# Model
--model-fix-direction

###############################
# Cleaner
#--clean-all
--clean-remove-html-tags
--clean-replace-html-entities
#--clean-add-missing-xml-name-tags
#--clean-add-missing-xml-ref-name-tags
#--clean-create-enumerations
#--clean-create-base-object-inheritance

###############################
# Checker
#--check-ref-model
#sx002d-2.1.002.00-model.xml

###############################
# HTML
--html-no-force
--html-show-ids
--html-show-guids
--html-show-indices
--html-img-svg
--html-img-show-ids
--html-img-show-all-tags
--html-img-show-all-metas
--html-frames
--html-flat-dirs
--html-model-overview-ignore-package
/Builtin
.*/[A-Za-z0-9_ \-]*Compound_Attributes.*
.*/[A-Za-z0-9_ \-]*Base_Object_Definition.*
.*/[A-Za-z0-9_ \-]*Primitives.*
.*/[A-Za-z0-9_ \-]*Remark
.*/[A-Za-z0-9_ \-]*Digital File
.*/[A-Za-z0-9_ \-]*Security Classification
.*/[A-Za-z0-9_ \-]*Applicability Statement
--html-name-remove-part
^.* UoF 

###############################
# S1000D
--s1000d-spec
--s1000d-glossary

###############################
# XSD
--xsd-namespace
http://www.asd-europe.org/s-series/dmewg
--xsd-specification
SX002D
--xsd-specification-url
http://www.sx000i.org
--xsd-issue-number
3.0
--xsd-issue-date
2024-04-30
--xsd-xml-schema-release-number
001-00
--xsd-xml-schema-release-date
2024-04-30


--verbose
--run-all
#--no-run-html-generator
#--no-run-office-generator
#--no-run-model-checker
#--no-run-s1000d-generator