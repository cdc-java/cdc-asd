package cdc.asd.model.wrappers;

import cdc.asd.model.AsdConstants;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInterface;

public class AsdInterface extends AsdType {
    static AsdInterface of(MfElement element) {
        return new AsdInterface(MfInterface.class.cast(element));
    }

    protected AsdInterface(MfInterface element) {
        super(element);
    }

    @Override
    public MfInterface getElement() {
        return getElement(MfInterface.class);
    }

    @Override
    public boolean isProjectSpecificAttributeValue() {
        return AsdConstants.PROJECT_SPECIFIC_ATTRIBUTE_VALUE.equals(getElement().getName());
    }
}