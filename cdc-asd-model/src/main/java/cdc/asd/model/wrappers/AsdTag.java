package cdc.asd.model.wrappers;

import cdc.asd.model.AsdTagName;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfTag;

public class AsdTag extends AsdElement {
    static AsdTag of(MfElement element) {
        return new AsdTag(MfTag.class.cast(element));
    }

    protected AsdTag(MfTag element) {
        super(element);
    }

    @Override
    public MfTag getElement() {
        return getElement(MfTag.class);
    }

    public AsdTagName getTagName() {
        return AsdTagName.of(getElement().getName());
    }

    public boolean is(AsdTagName tagName) {
        return getTagName() == tagName;
    }

    public String getSource() {
        if (is(AsdTagName.VALID_VALUE)) {
            return getNotes();
        } else {
            return null;
        }
    }
}