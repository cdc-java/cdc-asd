package cdc.asd.model.wrappers;

import cdc.asd.model.AsdStereotypeName;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.util.strings.StringUtils;

public class AsdPackage extends AsdElement {
    static AsdPackage of(MfElement element) {
        return new AsdPackage(MfPackage.class.cast(element));
    }

    protected AsdPackage(MfPackage element) {
        super(element);
    }

    @Override
    public MfPackage getElement() {
        return getElement(MfPackage.class);
    }

    /**
     * @return {@code true} if the package is directly defined in the model.
     */
    public boolean isRoot() {
        return getElement().getParent() instanceof MfModel;
    }

    @Override
    public String getEffectiveStereotype() {
        if (StringUtils.isNullOrEmpty(getStereotype())) {
            if (isUof()) {
                return AsdStereotypeName.UOF.getLiteral();
            } else {
                return null;
            }
        } else if ("uof".equalsIgnoreCase(getStereotype())) {
            return AsdStereotypeName.UOF.getLiteral();
        } else {
            return getStereotype();
        }
    }

    public boolean isUof() {
        if (StringUtils.isNullOrEmpty(getStereotype())) {
            // Package name doesn't always contain uof
            return getElement().getName().contains("UoF")
                    || !getElement().getChildren(MfClass.class).isEmpty()
                    || !getElement().getChildren(MfInterface.class).isEmpty();
        } else {
            return getStereotypeName() == AsdStereotypeName.UOF;
        }
    }

    public boolean isDomain() {
        return getStereotypeName() == AsdStereotypeName.DOMAIN;
    }

    public boolean isFunctionalArea() {
        return getStereotypeName() == AsdStereotypeName.FUNCTIONAL_AREA;
    }

    /**
     * @return The name with specific prefix (CDM, ...) removed.
     */
    public String getNameKernel() {
        return AsdUtils.getKernel(getElement().getName());
    }
}