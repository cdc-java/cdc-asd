package cdc.asd.model.wrappers;

import java.util.regex.Pattern;

import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfAnnotation;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfConstraint;
import cdc.mf.model.MfDependency;
import cdc.mf.model.MfDiagram;
import cdc.mf.model.MfDocumentation;
import cdc.mf.model.MfEnumeration;
import cdc.mf.model.MfEnumerationValue;
import cdc.mf.model.MfFactory;
import cdc.mf.model.MfImplementation;
import cdc.mf.model.MfInheritance;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfShape;
import cdc.mf.model.MfSpecialization;
import cdc.mf.model.MfTag;
import cdc.mf.model.MfTip;

public final class AsdUtils {
    private static final Pattern PREFIX = Pattern.compile("^(CDM|S2000M|S3000L|S4000P|S5000F|S6000T) ((UOF|UoF) )?");
    private static final String BASE_OBJECT_DEFINITION = "Base_Object_Definition";
    private static final String COMPOUND_ATTRIBUTES = "Compound_Attributes";
    private static final String PRIMITIVES = "Primitives";

    private AsdUtils() {
    }

    public static final MfFactory FACTORY =
            MfFactory.builder()
                     .add(MfAggregation.class, AsdConnector::of)
                     .add(MfAnnotation.class, AsdElement::of)
                     .add(MfAssociation.class, AsdConnector::of)
                     .add(MfClass.class, AsdClass::of)
                     .add(MfComposition.class, AsdConnector::of)
                     .add(MfConstraint.class, AsdElement::of)
                     .add(MfDependency.class, AsdElement::of)
                     .add(MfDiagram.class, AsdElement::of)
                     .add(MfDocumentation.class, AsdElement::of)
                     .add(MfEnumeration.class, AsdElement::of)
                     .add(MfEnumerationValue.class, AsdElement::of)
                     .add(MfInheritance.class, AsdElement::of)
                     .add(MfImplementation.class, AsdElement::of)
                     .add(MfInterface.class, AsdInterface::of)
                     .add(MfModel.class, AsdModel::of)
                     .add(MfPackage.class, AsdPackage::of)
                     .add(MfProperty.class, AsdProperty::of)
                     .add(MfShape.class, AsdElement::of)
                     .add(MfSpecialization.class, AsdElement::of)
                     .add(MfTag.class, AsdTag::of)
                     .add(MfTip.class, AsdElement::of)
                     .build();

    public static String getKernel(String name) {
        if (name == null) {
            return null;
        } else if (name.contains(BASE_OBJECT_DEFINITION)) {
            return BASE_OBJECT_DEFINITION;
        } else if (name.contains(PRIMITIVES)) {
            return PRIMITIVES;
        } else if (name.contains(COMPOUND_ATTRIBUTES)) {
            return COMPOUND_ATTRIBUTES;
        } else {
            return PREFIX.matcher(name).replaceAll("");
        }
    }
}