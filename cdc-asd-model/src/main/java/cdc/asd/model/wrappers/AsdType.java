package cdc.asd.model.wrappers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.asd.model.AsdBusinessId;
import cdc.asd.model.AsdException;
import cdc.asd.model.AsdKeyBusinessId;
import cdc.asd.model.AsdNoKeyBusinessId;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdSwitchBusinessId;
import cdc.asd.model.AsdTagName;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfType;

public class AsdType extends AsdElement {
    private static final Logger LOGGER = LogManager.getLogger(AsdType.class);

    /**
     * The business id is built lazily to avoid recursion issues.
     */
    private AsdBusinessId bid = null;

    protected AsdType(MfType element) {
        super(element);
    }

    @Override
    public MfType getElement() {
        return getElement(MfType.class);
    }

    public MfType getCompositionOwner() {
        final List<MfComposition> compositions = getElement().getReversedConnectors(MfComposition.class);
        if (compositions.isEmpty()) {
            return null;
        } else if (compositions.size() == 1) {
            return compositions.get(0).getSourceTip().getType();
        } else {
            LOGGER.warn("Too many compositions for {}", getElement().getName());
            return null;
        }
    }

    public AsdBusinessId getBusinessId() {
        if (bid == null) {
            bid = buildBusinessId(getElement());
        }
        return bid;
    }

    /**
     * @param type The class.
     * @return {@code true} if root genitor of {@code type} has local keys.
     */
    protected static boolean hasRootKeys(MfClass type) {
        final MfType root = type.getRootGenitor();
        return root.getProperties()
                   .stream()
                   .anyMatch(AsdProperty::isKeyOrCompositeKeyOrRelationshipKey);
    }

    private static List<MfProperty> getRootKeys(MfClass type) {
        final MfType root = type.getRootGenitor();
        return root.getProperties()
                   .stream()
                   .filter(AsdProperty::isKeyOrCompositeKeyOrRelationshipKey)
                   .toList();
    }

    private static List<MfTip> getKeyTips(MfClass type) {
        if (type.hasStereotype(AsdStereotypeName.RELATIONSHIP)) {
            final List<MfTip> list = new ArrayList<>();
            // Inputs
            list.addAll(type.getAllReversedConnectors(MfAssociation.class)
                            .stream()
                            .filter(c -> !c.hasTags(AsdTagName.NO_KEY))
                            .map(MfConnector::getSourceTip)
                            // TODO sort
                            .toList());
            // Outputs
            list.addAll(type.getAllConnectors(MfAssociation.class)
                            .stream()
                            .filter(c -> !c.hasTags(AsdTagName.NO_KEY))
                            .map(MfConnector::getTargetTip)
                            // TODO sort
                            .toList());
            return list;
        } else {
            return Collections.emptyList();
        }
    }

    /**
     * <b>WARNING:</b> must NOT be called on types that can be part of several compositions.
     *
     * @param type The type.
     * @return The composition whose part is {@code type}.
     */
    private static MfComposition getWholeComposition(MfType type) {
        // Collect all composition whose part is type
        // Ignoring <<exchange>>
        final List<MfComposition> compositions =
                type.getAllReversedConnectors(MfComposition.class)
                    .stream()
                    // Ignore whole that are <<exchange>>
                    .filter(c -> !c.getSourceTip().getType().wrap(AsdElement.class).is(AsdStereotypeName.EXCHANGE))
                    // .filter(Predicate.not(AsdConnector::hasLocalConstraint))
                    .toList();
        if (compositions.isEmpty()) {
            return null;
        } else if (compositions.size() == 1) {
            return compositions.get(0);
        } else {
            final List<MfComposition> nonLocalCompositions =
                    compositions.stream()
                                .filter(Predicate.not(AsdConnector::hasLocalConstraint))
                                .toList();
            if (nonLocalCompositions.isEmpty()) {
                return null;
            } else if (nonLocalCompositions.size() == 1) {
                return nonLocalCompositions.get(0);
            } else {
                throw new AsdException("Too many compositions (" + nonLocalCompositions.size() + ") for " + type.getName()
                        + ", owned by " + nonLocalCompositions.stream().map(c -> c.getSourceTip().getType()).toList());
            }
        }
    }

    private static AsdBusinessId buildBusinessId(MfType type) {
        final AsdBusinessId bid;
        if (type instanceof final MfInterface owner) {
            final Set<MfClass> choice =
                    owner.getDirectImplementations()
                         .stream()
                         .map(MfClass.class::cast)
                         .collect(Collectors.toSet());
            bid = new AsdSwitchBusinessId(owner, choice);
        } else if (type instanceof final MfClass owner) {
            final List<MfProperty> keyProperties = getRootKeys(owner);
            final List<MfTip> keyTips = getKeyTips(owner);
            if (keyProperties.isEmpty() && keyTips.isEmpty()) {
                // A class that has no key can not have a real business ID
                bid = new AsdNoKeyBusinessId(owner);
            } else {
                // The class has a real business ID
                // If it is a composition part, then we must have whole identification
                final MfComposition composition = getWholeComposition(owner);
                final MfType whole;
                if (composition == null) {
                    // This class is not a part of any composition
                    whole = null;
                } else {
                    whole = composition.getWholeTip().getType();
                }
                bid = new AsdKeyBusinessId(owner,
                                           keyProperties,
                                           keyTips,
                                           whole);
            }
        } else {
            throw new AsdException("Unexpected type: " + type.getName());
        }
        return bid;
    }
}