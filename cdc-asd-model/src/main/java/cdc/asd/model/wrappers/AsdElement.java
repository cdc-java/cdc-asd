package cdc.asd.model.wrappers;

import java.util.List;
import java.util.Set;

import cdc.asd.model.AsdBadges;
import cdc.asd.model.AsdConstants;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdTagName;
import cdc.issues.Metas;
import cdc.mf.model.MfAbstractWrapper;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfDiagram;
import cdc.mf.model.MfDocumentation;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfShape;
import cdc.mf.model.MfTagOwner;
import cdc.mf.model.MfType;
import cdc.util.strings.StringUtils;

/**
 * Base wrapper of {@link MfElement}.
 */
public class AsdElement extends MfAbstractWrapper {
    protected AsdElement(MfElement element) {
        super(element);
    }

    static AsdElement of(MfElement element) {
        return new AsdElement(element);
    }

    /**
     * @return The stereotype of this element.
     *         If this element has several stereotypes, this will fail.
     *         However, this does not seem to be supported at the moment.
     */
    public String getStereotype() {
        if (getElement() instanceof final MfTagOwner e) {
            final Set<String> stereotypes = e.getStereotypes();
            if (stereotypes.size() == 1) {
                return stereotypes.iterator().next();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Returns the stereotype if it is defined. Otherwise, tries to find it.
     * <p>
     * Typically useful for packages defined without stereotype and are however considered as UoF.
     *
     * @return The effective stereotype.
     */
    public String getEffectiveStereotype() {
        return getStereotype();
    }

    /**
     * @return The stereotype name of this element.
     *         If this element has several stereotypes, this will fail.
     *         However, this does not seem to be supported at the moment.
     */
    public final AsdStereotypeName getStereotypeName() {
        return AsdStereotypeName.of(getStereotype());
    }

    public final AsdStereotypeName getEffectiveStereotypeName() {
        return AsdStereotypeName.of(getEffectiveStereotype());
    }

    public boolean is(AsdStereotypeName stereotype) {
        return getStereotypeName() == stereotype;
    }

    public static boolean isBaseObject(MfElement element) {
        return element instanceof final MfNameItem x
                && AsdConstants.BASE_OBJECT.equals(x.getName());
    }

    /**
     * @return {@code true} if this element is the {@code BaseObject}.
     */
    public boolean isBaseObject() {
        return isBaseObject(getElement());
    }

    /**
     * @return {@code true} if this element is an implicit direct or indirect extension of {@code BaseObject}.
     */
    public boolean isBaseObjectImplicitExtension() {
        return isClass()
                && !isBaseObject()
                && (is(AsdStereotypeName.CLASS) || is(AsdStereotypeName.RELATIONSHIP));
    }

    /**
     * @return {@code true} if this element is an implicit direct extension of {@code BaseObject}.
     */
    public boolean isBaseObjectDirectImplicitExtension() {
        return isClass()
                && !isBaseObject()
                && (is(AsdStereotypeName.CLASS) || is(AsdStereotypeName.RELATIONSHIP))
                && ((MfClass) getElement()).getGenitor().isEmpty();
    }

    public boolean isProjectSpecificAttribute() {
        return getElement() instanceof final MfNameItem x
                && AsdConstants.PROJECT_SPECIFIC_ATTRIBUTE.equals(x.getName());
    }

    public boolean isProjectSpecificAttributeValue() {
        return getElement() instanceof final MfNameItem x
                && AsdConstants.PROJECT_SPECIFIC_ATTRIBUTE_VALUE.equals(x.getName());
    }

    /**
     * @return {@code true} if this element is {@code built-in}.
     */
    public boolean isBuiltin() {
        return getElement().getParent() == getModel().wrap(AsdModel.class).getBuiltin()
                || getElement() == getModel().wrap(AsdModel.class).getBuiltin();
    }

    static boolean isBuiltin(MfElement element) {
        return element instanceof final MfTagOwner s
                && s.hasStereotype(AsdStereotypeName.BUILTIN);
    }

    /**
     * @return {@code true} if this element is an {@code aggregation}.
     */
    public boolean isAggregation() {
        return element instanceof MfAggregation;
    }

    /**
     * @return {@code true} if this element is an {@code association}.
     */
    public boolean isAssociation() {
        return element instanceof MfAssociation;
    }

    /**
     * @return {@code true} if this element is a {@code composition}.
     */
    public boolean isComposition() {
        return element instanceof MfComposition;
    }

    public boolean isAttribute() {
        return element instanceof MfProperty;
    }

    /**
     * @return {@code true} if this element is a {@code type}.
     */
    public boolean isType() {
        return element instanceof MfType;
    }

    /**
     * @return {@code true} if this element is a {@code diagram}.
     */
    public boolean isDiagram() {
        return element instanceof MfDiagram;
    }

    /**
     * @return {@code true} if this element is a {@code shape}.
     */
    public boolean isShape() {
        return element instanceof MfShape;
    }

    /**
     * @return {@code true} if this element is a {@code class}.
     */
    public boolean isClass() {
        return element instanceof MfClass;
    }

    /**
     * @return {@code true} if this element is an {@code interface}.
     */
    public boolean isInterface() {
        return element instanceof MfInterface;
    }

    /**
     * @return {@code true} if this element is an {@code <<extend>> interface}.
     */
    public boolean isExtendInterface() {
        return element instanceof MfInterface
                && getStereotypeName() == AsdStereotypeName.EXTEND;
    }

    /**
     * @return {@code true} if this element is a {@code <<select>> interface}.
     */
    public boolean isSelectInterface() {
        return element instanceof MfInterface
                && getStereotypeName() == AsdStereotypeName.SELECT;
    }

    public boolean isExtension() {
        return element.getMetas()
                      .getValue(Metas.BADGES, "")
                      .contains(AsdBadges.EXTENSION);
    }

    /**
     * @return The author of this element.
     */
    public String getAuthor() {
        return getElement().getMetas().getValue(AsdConstants.AUTHOR);
    }

    /**
     * @return The version of this element.
     */
    public String getVersion() {
        return getElement().getMetas().getValue(AsdConstants.VERSION);
    }

    /**
     * @return The notes of this element.
     *         If this element has several doc, this will fail.
     *         However, this does not seem to be supported at the moment.
     */
    public String getNotes() {
        final List<MfDocumentation> docs = getElement().getDocumentations();
        if (docs.size() == 1) {
            return docs.get(0).getText();
        } else {
            return null;
        }
    }

    public boolean hasNotes() {
        return !StringUtils.isNullOrEmpty(getNotes());
    }

    public boolean hasTags(AsdTagName tag) {
        if (getElement() instanceof final MfTagOwner x) {
            return x.hasTags(tag);
        } else {
            return false;
        }
    }

    /**
     * @return {@code true} if the object is locally defined,
     *         {@code false} if it is imported.
     */
    public boolean isLocal() {
        // FIXME this is not sufficient
        // There are cases where the author is same, but the object is imported.
        final String author = getElement().getModel().wrap(AsdElement.class).getAuthor();
        return author != null && author.equals(getAuthor());
    }

    /**
     * @return The value of {@link AsdTagName#XML_NAME} of this element or {@code null}.
     */
    public String getXmlName() {
        if (getElement() instanceof final MfTagOwner to) {
            return to.getTagValue(AsdTagName.XML_NAME);
        } else {
            return null;
        }
    }

    /**
     * @return The value of {@link AsdTagName#XML_REF_NAME} of this element or {@code null}.
     */
    public String getXmlRefName() {
        if (getElement() instanceof final MfTagOwner to) {
            return to.getTagValue(AsdTagName.XML_REF_NAME);
        } else {
            return null;
        }
    }

    /**
     * @return The value of {@link AsdTagName#UID_PATTERN} of this element or {@code null}.
     */
    public String getUidPattern() {
        if (getElement() instanceof final MfTagOwner to) {
            return to.getTagValue(AsdTagName.UID_PATTERN);
        } else {
            return null;
        }
    }

    /**
     * @return The uidPattern of this element, or of its first ancestor that has one.
     */
    public String getEffectiveUidPattern() {
        final String tmp = getUidPattern();
        if (tmp != null) {
            return tmp;
        } else {
            if (element instanceof final MfClass cls) {
                final Set<? extends MfType> genitors = cls.getDirectGenitors();
                if (genitors.size() == 1) {
                    final MfClass genitor = (MfClass) genitors.iterator().next();
                    return genitor.wrap(AsdElement.class).getEffectiveUidPattern();
                }
            }
            return null;
        }
    }
}