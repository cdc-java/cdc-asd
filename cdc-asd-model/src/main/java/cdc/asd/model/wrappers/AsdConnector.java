package cdc.asd.model.wrappers;

import cdc.asd.model.AsdConstants;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfElement;

public class AsdConnector extends AsdElement {
    static AsdConnector of(MfElement element) {
        return new AsdConnector(MfConnector.class.cast(element));
    }

    protected AsdConnector(MfConnector element) {
        super(element);
    }

    @Override
    public MfConnector getElement() {
        return getElement(MfConnector.class);
    }

    public static boolean hasLocalConstraint(MfConnector connector) {
        return connector.getConstraints()
                        .stream()
                        .anyMatch(c -> AsdConstants.LOCAL.equals(c.getSpecification()));
    }

    /**
     * @return {code true} if this connector has a local constraint attached to it.
     */
    public boolean hasLocalConstraint() {
        return hasLocalConstraint(getElement());
    }
}