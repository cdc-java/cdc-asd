package cdc.asd.model.wrappers;

import cdc.asd.model.AsdCompoundAttributeTypeName;
import cdc.asd.model.AsdConstants;
import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.asd.model.AsdStereotypeName;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfProperty;
import cdc.util.lang.Checks;

/**
 * Wrapper of {@link MfProperty} (attributes).
 */
public class AsdProperty extends AsdElement {
    static AsdProperty of(MfElement element) {
        return new AsdProperty(MfProperty.class.cast(element));
    }

    protected AsdProperty(MfProperty element) {
        super(element);
    }

    @Override
    public MfProperty getElement() {
        return getElement(MfProperty.class);
    }

    public static boolean isKeyOrCompositeKeyOrRelationshipKey(MfProperty property) {
        return property.hasStereotype(AsdStereotypeName.KEY,
                                      AsdStereotypeName.COMPOSITE_KEY,
                                      AsdStereotypeName.RELATIONSHIP_KEY);
    }

    public boolean isKeyOrCompositeKeyOrRelationshipKey() {
        return isKeyOrCompositeKeyOrRelationshipKey(getElement());
    }

    /**
     * @return The {@link AsdPrimitiveTypeName} of this property, or {@code null}.<br>
     *         {@link AsdPrimitiveTypeName#ORGANIZATION_REF ORGANIZATION_REF} needs a special processing as
     *         {@link AsdConstants#ORGANIZATION ORGANIZATION} can be a {@code <<primitive>>} of a {@code <<class>>}.
     */
    public AsdPrimitiveTypeName getPrimitiveTypeName() {
        if (getElement().hasValidType()) {
            final AsdPrimitiveTypeName tmp = AsdPrimitiveTypeName.of(getElement().getType().getName());
            // If name of the type is Organization, we must check that the type is the primitive one
            if (tmp != null
                    && tmp.isOrganizationRef()
                    && !getElement().getType().wrap(AsdClass.class).isPrimitiveOrganization()) {
                return null;
            } else {
                return tmp;
            }
        } else {
            return null;
        }
    }

    /**
     * @param type The tested type.
     * @return {@code true} if the type of this property is {@code type}.
     */
    public boolean hasType(AsdPrimitiveTypeName type) {
        Checks.isNotNull(type, "type");
        return getPrimitiveTypeName() == type;
    }

    /**
     * @return The {@link AsdCompoundAttributeTypeName} of this property, or {@code null}.
     */
    public AsdCompoundAttributeTypeName getCompoundAttributeTypeName() {
        if (getElement().hasValidType()) {
            return AsdCompoundAttributeTypeName.of(getElement().getType().getName());
        } else {
            return null;
        }
    }

    /**
     * @param type The tested type.
     * @return {@code true} if the type of this property is {@code type}.
     */
    public boolean hasType(AsdCompoundAttributeTypeName type) {
        Checks.isNotNull(type, "type");
        return getCompoundAttributeTypeName() == type;
    }
}