package cdc.asd.model.wrappers;

import java.util.Set;
import java.util.stream.Collectors;

import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfType;

public class AsdModel extends AsdElement {
    /** The package names in this model. */
    private final Set<String> packageNames;
    /** The specific types names (exclude builtin type names) in this model. */
    private final Set<String> specificTypeNames;
    /** The attribute names in this model. */
    private final Set<String> attributeNames;

    private MfClass baseObject = null;

    static AsdModel of(MfElement element) {
        return new AsdModel(MfModel.class.cast(element));
    }

    protected AsdModel(MfModel model) {
        super(model);

        // Do not wrap elements in a wrapper: risk of recursion

        this.packageNames = model.collect(MfPackage.class)
                                 .stream()
                                 .map(MfNameItem::getName)
                                 .collect(Collectors.toSet());

        this.specificTypeNames = model.collect(MfType.class)
                                      .stream()
                                      .filter(x -> !AsdElement.isBuiltin(x))
                                      .map(MfNameItem::getName)
                                      .collect(Collectors.toSet());

        this.attributeNames = model.collect(MfProperty.class)
                                   .stream()
                                   .map(MfNameItem::getName)
                                   .collect(Collectors.toSet());
    }

    @Override
    public MfModel getElement() {
        return getElement(MfModel.class);
    }

    /**
     * @return The set of package names in this model.
     */
    public Set<String> getPackageNames() {
        return packageNames;
    }

    /**
     * @return The set of concepts (type names) in this model.
     */
    public Set<String> getSpecificTypeNames() {
        return specificTypeNames;
    }

    /**
     * @return The set of attribute names in this model.
     */
    public Set<String> getAttributeNames() {
        return attributeNames;
    }

    public MfPackage getBuiltin() {
        return getElement().getPackageWithName("Builtin"); // FIXME
    }

    public MfClass getBaseObject() {
        if (baseObject == null) {
            baseObject = getElement().collect(MfClass.class)
                                     .stream()
                                     .filter(AsdElement::isBaseObject)
                                     .findFirst()
                                     .orElseThrow();
        }
        return baseObject;
    }
}