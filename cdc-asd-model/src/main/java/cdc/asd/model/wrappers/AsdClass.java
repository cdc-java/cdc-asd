package cdc.asd.model.wrappers;

import java.util.List;

import cdc.asd.model.AsdConstants;
import cdc.asd.model.AsdKeyBusinessId;
import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfElement;

public class AsdClass extends AsdType {
    static AsdClass of(MfElement element) {
        return new AsdClass(MfClass.class.cast(element));
    }

    protected AsdClass(MfClass element) {
        super(element);
    }

    @Override
    public MfClass getElement() {
        return getElement(MfClass.class);
    }

    /**
     * @return {@code true} if this type has a business id.
     */
    public boolean hasKeyBusinessId() {
        return getBusinessId() instanceof AsdKeyBusinessId;
    }

    /**
     * @return The direct ancestor class of this class or {@code null}.
     */
    public MfClass getGenitor() {
        return (MfClass) getElement().getGenitor().orElse(null);
    }

    /**
     * @return The root genitor of this class.
     */
    public MfClass getRootGenitor() {
        return (MfClass) getElement().getRootGenitor();
    }

    /**
     * @return A list containing this class and all its genitors, recursively, starting by the most general class.
     */
    public List<MfClass> getGenitors() {
        return getElement().getGenitorsHierarchy()
                           .stream()
                           .map(MfClass.class::cast)
                           .toList();
    }

    /**
     * @return {@code true} if this class is a revision.
     */
    public boolean isRevision() {
        final String name = getElement().getName();
        if (name == null) {
            return false;
        } else {
            final int index = name.lastIndexOf(AsdConstants.REVISION);
            return index >= 0
                    && index == name.length() - AsdConstants.REVISION.length();
        }
    }

    /**
     * @return {@code true} if the class is named {@link AsdConstants#ORGANIZATION}
     *         that is considered as a {@code <<primitive>>}.
     * @see AsdPrimitiveTypeName#ORGANIZATION_REF
     */
    public boolean isPrimitiveOrganization() {
        return AsdConstants.ORGANIZATION.equals(getElement().getName())
                && !getElement().hasGenitors();
    }
}