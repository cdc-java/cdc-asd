package cdc.asd.model;

public class AsdException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public AsdException(String message,
                        Throwable cause) {
        super(message, cause);
    }

    public AsdException(String message) {
        super(message);
    }
}