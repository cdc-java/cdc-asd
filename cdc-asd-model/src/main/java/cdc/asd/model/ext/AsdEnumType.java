package cdc.asd.model.ext;

/**
 * Interface of enum types.
 * <p>
 * It has 2 implementations:
 * <ul>
 * <li>{@link AsdBaseEnumType}
 * <li>{@link AsdUnionEnumType}
 * </ul>
 */
public interface AsdEnumType {

    /**
     * @return The enum type name.
     */
    public String getName();

    /**
     * @return The enum type source.
     */
    public String getSource();

    /**
     * @return The enum type description.
     */
    public String getDescription();

    /**
     * @return The enum type copyright.
     */
    public String getCopyright();

    public default boolean isBase() {
        return this instanceof AsdBaseEnumType;
    }

    public default boolean isUnion() {
        return this instanceof AsdUnionEnumType;
    }

    public abstract static class Builder<B extends Builder<B, T>, T extends AsdEnumType> {
        protected String name;
        protected String source;
        protected String description;
        protected String copyright;

        protected B self() {
            @SuppressWarnings("unchecked")
            final B tmp = (B) this;
            return tmp;
        }

        public final B name(String name) {
            this.name = name;
            return self();
        }

        public final B source(String source) {
            this.source = source;
            return self();
        }

        public final B description(String description) {
            this.description = description;
            return self();
        }

        public final B copyright(String copyright) {
            this.copyright = copyright;
            return self();
        }

        public abstract T build();
    }
}