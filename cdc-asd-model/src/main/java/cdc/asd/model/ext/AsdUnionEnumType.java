package cdc.asd.model.ext;

import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * The union variant of {@link AsdEnumType}.
 */
public interface AsdUnionEnumType extends AsdEnumType {
    public List<String> getMemberTypes();

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdEnumType.Builder<Builder, AsdUnionEnumType> {
        private static final Pattern P = Pattern.compile("[ \n]");
        private List<String> memberTypes = Collections.emptyList();

        private Builder() {
        }

        public Builder memberTypes(List<String> memberTypes) {
            this.memberTypes = List.copyOf(memberTypes);
            return self();
        }

        public Builder memberTypes(String memberTypes) {
            final String[] tmp = P.split(memberTypes);
            final List<String> list = List.of(tmp);
            return memberTypes(list);
        }

        @Override
        public AsdUnionEnumType build() {
            return new AsdUnionEnumTypeImpl(name,
                                            source,
                                            description,
                                            copyright,
                                            memberTypes);
        }
    }
}

record AsdUnionEnumTypeImpl(String name,
                            String source,
                            String description,
                            String copyright,
                            List<String> memberTypes)
        implements AsdUnionEnumType {

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getCopyright() {
        return copyright;
    }

    @Override
    public List<String> getMemberTypes() {
        return memberTypes;
    }
}