package cdc.asd.model.ext;

import java.util.Comparator;
import java.util.Set;

/**
 * Description, of an enum type value.
 */
public interface AsdEnumValue {
    public static final Comparator<AsdEnumValue> VALUE_COMPARATOR = Comparator.comparing(AsdEnumValue::getValue);

    public static final AsdEnumValue EMPTY = builder().value("/EMPTY").source("SX001G:nonSharedValue").build();
    public static final AsdEnumValue NULL = builder().value("/NULL").source("SX001G:currentlyUnknownValue").build();
    public static final AsdEnumValue NA = builder().value("N/A").source("SX001G:notApplicableValue").build();

    public static final Set<AsdEnumValue> STANDARD_VALUES = Set.of(EMPTY, NULL, NA);

    /**
     * @return The value name.
     */
    public String getValue();

    /**
     * @return The value source.
     */
    public String getSource();

    /**
     * @return The value description.
     */
    public String getDescription();

    /**
     * @return The value processing.
     */
    public AsdProcessing getProcessing();

    public default boolean isKeep() {
        return getProcessing() == AsdProcessing.KEEP;
    }

    public default boolean isIgnore() {
        return getProcessing() == AsdProcessing.IGNORE;
    }

    public default boolean isComment() {
        return getProcessing() == AsdProcessing.COMMENT;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String value;
        private String source;
        private String description;
        private AsdProcessing processing = AsdProcessing.KEEP;

        public Builder value(String value) {
            this.value = value;
            return this;
        }

        public Builder source(String source) {
            this.source = source;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder processing(AsdProcessing processing) {
            this.processing = processing;
            return this;
        }

        public AsdEnumValue build() {
            return new AsdEnumValueImpl(value,
                                        source,
                                        description,
                                        processing);
        }
    }
}

record AsdEnumValueImpl(String value,
                        String source,
                        String description,
                        AsdProcessing processing)
        implements AsdEnumValue {
    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public AsdProcessing getProcessing() {
        return processing;
    }
}