package cdc.asd.model.ext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;

/**
 * The base variant of {@link AsdEnumType}.
 * <p>
 * It is a list of {@link AsdEnumValue}s.
 */
public interface AsdBaseEnumType extends AsdEnumType {
    /**
     * @return The XSD base type name.
     */
    public String getBase();

    /**
     * @return The values.
     */
    public List<AsdEnumValue> getValues();

    public default List<AsdEnumValue> getNonIgnoredValues() {
        return getValues().stream().filter(v -> !v.isIgnore()).toList();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdEnumType.Builder<Builder, AsdBaseEnumType> {
        private String base;
        private List<AsdEnumValue> values = Collections.emptyList();
        private AsdEnumValueSorting sorting = AsdEnumValueSorting.NONE;
        private boolean addStandardValues = false;

        private Builder() {
        }

        public Builder base(String base) {
            this.base = base;
            return self();
        }

        public Builder values(List<AsdEnumValue> values) {
            this.values = List.copyOf(values);
            return self();
        }

        public Builder values(AsdEnumValue... values) {
            this.values = List.of(values);
            return self();
        }

        public Builder sorting(AsdEnumValueSorting sorting) {
            this.sorting = sorting;
            return self();
        }

        public Builder addStandardValues(boolean addStandardValues) {
            this.addStandardValues = addStandardValues;
            return self();
        }

        @Override
        public AsdBaseEnumType build() {
            final List<AsdEnumValue> list = new ArrayList<>(values);
            switch (sorting) {
            case DESCRIPTION -> Collections.sort(list, Comparator.comparing(AsdEnumValue::getDescription));
            case VALUE -> Collections.sort(list, Comparator.comparing(AsdEnumValue::getValue));
            case NONE -> {
                // Ignore
            }
            }

            if (addStandardValues) {
                Checks.isFalse(list.contains(AsdEnumValue.EMPTY), "/EMPTY already defined.");
                Checks.isFalse(list.contains(AsdEnumValue.NULL), "/NULL already defined.");
                Checks.isFalse(list.contains(AsdEnumValue.NA), "N/A already defined.");
                list.add(AsdEnumValue.EMPTY);
                list.add(AsdEnumValue.NULL);
                list.add(AsdEnumValue.NA);
            }

            return new AsdBaseEnumTypeImpl(name,
                                           source,
                                           description,
                                           copyright,
                                           base,
                                           list);
        }
    }
}

record AsdBaseEnumTypeImpl(String name,
                           String source,
                           String description,
                           String copyright,
                           String base,
                           List<AsdEnumValue> values)
        implements AsdBaseEnumType {

    private static final Logger LOGGER = LogManager.getLogger(AsdBaseEnumType.class);

    AsdBaseEnumTypeImpl {
        // TODO move checks to a checker
        final Map<String, List<AsdEnumValue>> nameToValues = new HashMap<>();
        final Set<String> done = new HashSet<>();
        final Set<String> duplicates = new HashSet<>();
        for (final AsdEnumValue value : values) {
            if (value.isKeep()) {
                final List<AsdEnumValue> list =
                        nameToValues.computeIfAbsent(value.getValue(), k -> new ArrayList<>());
                list.add(value);
                if (done.contains(value.getValue())) {
                    duplicates.add(value.getValue());
                } else {
                    done.add(value.getValue());
                }
            }
        }
        if (!duplicates.isEmpty()) {
            LOGGER.warn("Duplicate (kept) values in base enum type '{}'", name);
            for (final String s : duplicates.stream().sorted().toList()) {
                LOGGER.warn("   {}", s);
                for (final AsdEnumValue v : nameToValues.get(s)) {
                    LOGGER.warn("      {}", v);
                }
            }
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getCopyright() {
        return copyright;
    }

    @Override
    public String getBase() {
        return base;
    }

    @Override
    public List<AsdEnumValue> getValues() {
        return values;
    }

    @Override
    public List<AsdEnumValue> getNonIgnoredValues() {
        return values.stream().filter(v -> !v.isIgnore()).toList();
    }
}