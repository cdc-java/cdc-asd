package cdc.asd.model.ext;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.office.ss.SheetLoader;
import cdc.office.tables.Header;
import cdc.office.tables.HeaderCell;
import cdc.office.tables.HeaderMapper;
import cdc.office.tables.Row;
import cdc.util.lang.FailureReaction;
import cdc.util.strings.StringUtils;

public final class AsdEnumTypeIo {
    private static final Logger LOGGER = LogManager.getLogger(AsdEnumTypeIo.class);

    private static final String ADD_STANDARD_VALUES = "Add Standard Values";
    private static final String BASE = "Base";
    private static final String COPYRIGHT = "Copyright";
    private static final String DESCRIPTION = "Description";
    private static final String LIBRARIES = "Libraries";
    private static final String MEMBER_TYPES = "Member Types";
    private static final String NAME = "Name";
    private static final String PROCESSING = "Processing";
    private static final String SOURCE = "Source";
    private static final String VALUE = "Value";

    private static final Header LIBRARY_HEADER =
            Header.builder()
                  .names(NAME,
                         SOURCE,
                         DESCRIPTION,
                         BASE,
                         MEMBER_TYPES,
                         ADD_STANDARD_VALUES,
                         COPYRIGHT)
                  .build();

    private static final Header BASE_ENUM_TYPE_HEADER =
            Header.builder()
                  .names(VALUE,
                         SOURCE,
                         DESCRIPTION,
                         PROCESSING)
                  .build();

    private AsdEnumTypeIo() {
    }

    public static AsdEnumTypeSet loadEnumTypeSet(URL url,
                                                 AsdEnumValueSorting sorting) throws IOException {
        LOGGER.debug("loadEnumTypeSet({}, {})", url, sorting);
        final SheetLoader loader = new SheetLoader();
        final List<Row> rows = loader.load(url, null, LIBRARIES);

        if (rows.isEmpty()) {
            throw new IllegalArgumentException("No data in " + url + "/" + LIBRARIES + ".");
        }
        // The actual header
        final Header header = Header.builder().names(rows.get(0)).build();

        final HeaderMapper mapper = HeaderMapper.builder()
                                                .mandatory(LIBRARY_HEADER)
                                                .actual(header)
                                                .build();
        if (!mapper.hasAllMandatoryCells()) {
            throw new IllegalArgumentException("Missing keys: "
                    + mapper.getMissingMandatoryCells().stream().map(HeaderCell::toString).sorted()
                            .collect(Collectors.joining(",", "[", "]"))
                    + " in header: " + header + " of " + url + ".");
        }

        // Remove first row (header)
        rows.remove(0);

        final List<AsdEnumType> types = new ArrayList<>();
        for (final Row row : rows) {
            final String name = row.getValue(header.getMatchingIndex(NAME));
            final String source = row.getValue(header.getMatchingIndex(SOURCE));
            final String description = row.getValue(header.getMatchingIndex(DESCRIPTION));
            final String base = row.getValue(header.getMatchingIndex(BASE));
            final String memberTypes = row.getValue(header.getMatchingIndex(MEMBER_TYPES));
            final boolean addStandardValues = row.getValueAsBoolean(header.getMatchingIndex(ADD_STANDARD_VALUES), Boolean.TRUE);
            final String copyright = row.getValue(header.getMatchingIndex(COPYRIGHT));

            final AsdEnumType type;
            if (StringUtils.isNullOrEmpty(memberTypes)) {
                final List<AsdEnumValue> values = loadValues(url, name);
                type = AsdBaseEnumType.builder()
                                      .name(name)
                                      .source(source)
                                      .description(description)
                                      .copyright(copyright)
                                      .base(base)
                                      .values(values)
                                      .addStandardValues(addStandardValues)
                                      .sorting(sorting)
                                      .build();
            } else {
                type = AsdUnionEnumType.builder()
                                       .name(name)
                                       .source(source)
                                       .description(description)
                                       .copyright(copyright)
                                       .memberTypes(memberTypes)
                                       .build();
            }
            types.add(type);
        }

        return AsdEnumTypeSet.builder()
                             .types(types)
                             .build();
    }

    private static List<AsdEnumValue> loadValues(URL url,
                                                 String sheetName) throws IOException {
        LOGGER.debug("loadValues({}, {})", url, sheetName);
        final SheetLoader loader = new SheetLoader();
        final List<Row> rows = loader.load(url, null, sheetName);

        if (rows.isEmpty()) {
            throw new IllegalArgumentException("No data in " + url + "/" + sheetName + ".");
        }

        // The actual header
        final Header header = Header.builder().names(rows.get(0)).build();

        final HeaderMapper mapper = HeaderMapper.builder()
                                                .mandatory(BASE_ENUM_TYPE_HEADER)
                                                .actual(header)
                                                .build();
        if (!mapper.hasAllMandatoryCells()) {
            throw new IllegalArgumentException("Missing keys: "
                    + mapper.getMissingMandatoryCells().stream().map(HeaderCell::toString).sorted()
                            .collect(Collectors.joining(",", "[", "]"))
                    + " in header: " + header + " of " + url + "/" + sheetName + ".");
        }

        // Remove first row (header)
        rows.remove(0);

        final List<AsdEnumValue> list = new ArrayList<>();
        for (final Row row : rows) {
            final String value = row.getValue(header.getMatchingIndex(VALUE));
            final String source = row.getValue(header.getMatchingIndex(SOURCE));
            final String description = row.getValue(header.getMatchingIndex(DESCRIPTION));
            final AsdProcessing processing =
                    row.getValueAsEnum(header.getMatchingIndex(PROCESSING),
                                       AsdProcessing.class,
                                       AsdProcessing.KEEP,
                                       FailureReaction.DEFAULT);
            list.add(AsdEnumValue.builder()
                                 .value(value)
                                 .source(source)
                                 .description(description)
                                 .processing(processing)
                                 .build());
        }

        return list;
    }
}