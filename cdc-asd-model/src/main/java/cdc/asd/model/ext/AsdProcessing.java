package cdc.asd.model.ext;

public enum AsdProcessing {
    /** Value is ignored. */
    IGNORE,
    /** Value is normally processed. */
    KEEP,
    /** Value is processed, but commented out. */
    COMMENT
}