package cdc.asd.model.ext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.util.lang.Checks;

/**
 * Set of {@link AsdEnumType}s.
 */
public final class AsdEnumTypeSet {
    private static final Logger LOGGER = LogManager.getLogger(AsdEnumTypeSet.class);
    private final List<? extends AsdEnumType> types;

    private record TypeValue(String type,
                             AsdEnumValue value) {
    }

    private AsdEnumTypeSet(Builder builder) {
        this.types = Checks.isNotNull(builder.types, "types");

        // Checks
        // TODO move them to a checker
        final Map<String, AsdEnumType> map = new HashMap<>();
        for (final AsdEnumType type : types) {
            map.put(type.getName(), type);
        }
        if (types.size() != map.size()) {
            LOGGER.warn("Duplicate types");
        }

        for (final AsdEnumType type : types) {
            if (type instanceof final AsdUnionEnumType u) {
                final Map<String, List<TypeValue>> nameToValues = new HashMap<>();
                final Set<String> done = new HashSet<>();
                final Set<String> duplicates = new HashSet<>();
                for (final String memberType : u.getMemberTypes()) {
                    final AsdEnumType mt = map.get(memberType);
                    if (mt instanceof final AsdBaseEnumType b) {
                        for (final AsdEnumValue value : b.getValues()) {
                            if (value.isKeep()
                                    && !AsdEnumValue.STANDARD_VALUES.contains(value)) {
                                final List<TypeValue> list =
                                        nameToValues.computeIfAbsent(value.getValue(), k -> new ArrayList<>());
                                list.add(new TypeValue(b.getName(), value));
                                if (done.contains(value.getValue())) {
                                    duplicates.add(value.getValue());
                                } else {
                                    done.add(value.getValue());
                                }
                            }
                        }
                    }
                }
                if (!duplicates.isEmpty()) {
                    LOGGER.warn("Duplicate (kept) values in union enum type '{}'",
                                u.getName());
                    for (final String v : duplicates.stream().sorted().toList()) {
                        LOGGER.warn("   {}", v);
                        for (final TypeValue tv : nameToValues.get(v)) {
                            LOGGER.warn("      {} {}", tv.type, tv.value);
                        }
                    }
                }
            }
        }

    }

    public List<? extends AsdEnumType> getTypes() {
        return types;
    }

    public List<String> getCopyrights() {
        return getTypes().stream()
                         .map(AsdEnumType::getCopyright)
                         .distinct()
                         .toList();
    }

    public static Builder builder() {
        return new Builder();
    }

    /**
     * Builder of {@link AsdEnumTypeSet}.
     */
    public static final class Builder {
        private List<? extends AsdEnumType> types;

        private Builder() {
        }

        public Builder types(List<? extends AsdEnumType> types) {
            this.types = types;
            return this;
        }

        public AsdEnumTypeSet build() {
            return new AsdEnumTypeSet(this);
        }
    }
}