package cdc.asd.model.ext;

public final class AsdCrudCode {
    private AsdCrudCode() {
    }

    public static final AsdEnumValue INSERTED =
            AsdEnumValue.builder()
                        .value("I")
                        .source("SX001G:insertedElement")
                        .build();
    public static final AsdEnumValue DELETED =
            AsdEnumValue.builder()
                        .value("D")
                        .source("SX001G:deletedElement")
                        .build();
    public static final AsdEnumValue UPDATED =
            AsdEnumValue.builder()
                        .value("U")
                        .source("SX001G:updatedElement")
                        .build();
    public static final AsdEnumValue REPLACED =
            AsdEnumValue.builder()
                        .value("R")
                        .source("SX001G:replacedElement")
                        .build();
    public static final AsdEnumValue NON_CHANGED =
            AsdEnumValue.builder()
                        .value("N")
                        .source("SX001G:nonChangedElement")
                        .build();

    public static final AsdBaseEnumType INSTANCE =
            AsdBaseEnumType.builder()
                           .name("crudCode")
                           .base("xsd:string")
                           .source("DMEWG")
                           .values(INSERTED,
                                   DELETED,
                                   UPDATED,
                                   REPLACED,
                                   NON_CHANGED)
                           .build();
}