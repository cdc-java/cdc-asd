package cdc.asd.model.ext;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cdc.asd.model.AsdConstants;
import cdc.asd.model.AsdPrimitiveTypeName;
import cdc.asd.model.AsdTagName;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.asd.model.wrappers.AsdTag;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTag;
import cdc.util.strings.StringUtils;

public final class AsdModelToExt {
    private final MfModel model;
    private final String copyright;

    private AsdModelToExt(Builder builder) {
        this.model = builder.model;
        this.copyright = builder.copyright;
    }

    /**
     * Extract all enum types (classification types) from the model.
     * <p>
     * Only types that are associated to validValue tags are extracted.
     *
     * @return An enum type set of all enum types contained in the model.
     */
    public AsdEnumTypeSet getModelEnumTypeSet() {
        // Collect properties that have an associated enum type
        final List<MfProperty> properties = model.collect(MfProperty.class, AsdModelToExt::isEnumType);
        final List<AsdBaseEnumType> types = new ArrayList<>();
        // A property can appear several times, so we keep just one of them
        final Set<String> done = new HashSet<>();
        for (final MfProperty property : properties) {
            if (!done.contains(property.getName())) {
                done.add(property.getName());
                final AsdBaseEnumType type = getEnumType(property);
                types.add(type);
            }
        }
        // Sort types
        Collections.sort(types, Comparator.comparing(t -> t.getName().toLowerCase()));

        return AsdEnumTypeSet.builder()
                             .types(types)
                             .build();
    }

    /**
     * @param property The property
     * @return {@code true} if {@code property} is associated to an enum type.
     */
    private static boolean isEnumType(MfProperty property) {
        return (property.wrap(AsdProperty.class).getPrimitiveTypeName() == AsdPrimitiveTypeName.CLASSIFICATION
                || property.wrap(AsdProperty.class).getPrimitiveTypeName() == AsdPrimitiveTypeName.IDENTIFIER
                || property.wrap(AsdProperty.class).getPrimitiveTypeName() == AsdPrimitiveTypeName.STATE
                || property.getType().getName().equals(AsdConstants.VALID_VALUE)
                || property.hasTags(AsdTagName.VALID_VALUE))
                && !property.hasTags(AsdTagName.VALID_VALUE_LIBRARY)
                && !property.getName().equals("unit")
                && !property.getName().equals("classifier");
    }

    private static String getEnumTypeName(MfProperty property) {

        if (property.wrap(AsdProperty.class).getPrimitiveTypeName() == AsdPrimitiveTypeName.IDENTIFIER) {
            return property.getName() + "ClassCode";
        } else {
            return property.getName() + "Code";
        }
    }

    /**
     * @param property The property
     * @return The enum type associated to {@code property}.
     */
    private AsdBaseEnumType getEnumType(MfProperty property) {
        // Extract valid values tags
        final List<MfTag> tags = property.getTags(AsdTagName.VALID_VALUE);
        final List<AsdEnumValue> values = new ArrayList<>();
        for (final MfTag tag : tags) {
            final String value = tag.getValue();
            final String source = tag.wrap(AsdTag.class).getSource();
            if (!StringUtils.isNullOrEmpty(value)) {
                values.add(AsdEnumValue.builder()
                                       .value(value)
                                       .source(source)
                                       .build());
            }
        }

        return AsdBaseEnumType.builder()
                              .name(getEnumTypeName(property))
                              .base("xsd:string")
                              .values(values)
                              .sorting(AsdEnumValueSorting.VALUE)
                              .copyright(copyright)
                              .addStandardValues(true)
                              .build();
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private MfModel model;
        private String copyright;

        private Builder() {
        }

        public Builder model(MfModel model) {
            this.model = model;
            return this;
        }

        public Builder copyright(String copyright) {
            this.copyright = copyright;
            return this;
        }

        public AsdModelToExt build() {
            return new AsdModelToExt(this);
        }
    }
}