package cdc.asd.model;

public final class AsdConstants {
    private AsdConstants() {
    }

    public static final String AUTHOR = "author";
    public static final String BASE_OBJECT = "BaseObject";
    public static final String DATE = "Date";
    public static final String DATED_CLASSIFICATION = "DatedClassification";
    public static final String LOCAL = "local";
    public static final String MESSAGE_CONTENT = "MessageContent";
    public static final String ORGANIZATION = "Organization";
    public static final String PROJECT_SPECIFIC_ATTRIBUTE = "ProjectSpecificAttribute";
    public static final String PROJECT_SPECIFIC_ATTRIBUTE_VALUE = "ProjectSpecificAttributeValue";
    public static final String RATIONALE = "Rationale";
    public static final String REVISION = "Revision";
    public static final String STATUS = "Status";
    public static final String TIME_STAMPED_CLASSIFICATION = "TimeStampedClassification";
    public static final String VALID_VALUE = "validValue";
    public static final String VERSION = "version";
}