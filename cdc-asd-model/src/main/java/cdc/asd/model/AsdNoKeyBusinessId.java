package cdc.asd.model;

import java.io.PrintStream;

import cdc.mf.model.MfClass;

/**
 * Id associated to classes that don't have or inherit any key attributes.
 */
public final class AsdNoKeyBusinessId extends AsdBusinessId {
    public AsdNoKeyBusinessId(MfClass owner) {
        super(owner);
    }

    @Override
    public MfClass getOwner() {
        return (MfClass) super.getOwner();
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getClass().getSimpleName() + "@" + getOwner().getName());
    }
}