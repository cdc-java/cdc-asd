package cdc.asd.model;

import cdc.mf.model.MfType;
import cdc.util.debug.Printable;

/**
 * Base class of identifiers.
 * <p>
 * It can be:
 * <ul>
 * <li>A class that has no business identifier: it does not have or inherit any key attributes.
 * <li>A class that has business identifier: it has or inherits one or more key attributes.
 * <li>An interface.
 * </ul>
 */
public abstract class AsdBusinessId implements Printable {
    private final MfType owner;

    protected AsdBusinessId(MfType owner) {
        this.owner = owner;
    }

    /**
     * @return The type that owns this identifier.
     */
    public MfType getOwner() {
        return owner;
    }

    public boolean isNoKey() {
        return this instanceof AsdNoKeyBusinessId;
    }

    public boolean isKey() {
        return this instanceof AsdKeyBusinessId;
    }

    public boolean isSwitch() {
        return this instanceof AsdSwitchBusinessId;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "@" + getOwner().getName();
    }
}