package cdc.asd.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.similarity.EditDistanceFrom;
import org.apache.commons.text.similarity.JaccardDistance;

import cdc.mf.model.MfClass;
import cdc.mf.model.MfDiagram;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfEnum;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfPackage;
import cdc.mf.model.MfProperty;

/**
 * Enumeration of possible stereotypes.
 *
 * @author Damien Carbonne
 */
public enum AsdStereotypeName implements MfEnum {
    /** {@link MfClass} stereotype. */
    ATTRIBUTE_GROUP("attributeGroup", MfClass.class),

    /** {@link MfClass} stereotype (extension). */
    BUILTIN("builtin", MfPackage.class, MfClass.class), // Not defined by S-Series

    /** {@link MfProperty} stereotype. */
    CHARACTERISTIC("characteristic", MfProperty.class),

    /** {@link MfClass} stereotype. */
    CLASS("class", MfClass.class),

    /** {@link MfProperty} stereotype. */
    COMPOSITE_KEY("compositeKey", MfProperty.class),

    /** {@link MfClass} stereotype. */
    COMPOUND_ATTRIBUTE("compoundAttribute", MfClass.class),

    /** {@link MfPackage} stereotype. */
    DOMAIN("Domain", MfPackage.class),

    /** {@link MfClass} stereotype. */
    EXCHANGE("exchange", MfClass.class),

    /** {@link MfInterface} stereotype. */
    EXTEND("extend", MfInterface.class),

    /** {@link MfPackage} stereotype. */
    FUNCTIONAL_AREA("FunctionalArea", MfPackage.class),

    /** {@link MfProperty} stereotype. */
    KEY("key", MfProperty.class),

    /** {@link MfClass} stereotype. */
    METACLASS("metaclass", MfClass.class),

    /** {@link MfProperty} stereotype. */
    METADATA("metadata", MfProperty.class),

    /** {@link MfDiagram} stereotype. */
    NO_PRINT("noPrint", MfDiagram.class),

    /** {@link MfClass} stereotype. */
    PRIMITIVE("primitive", MfClass.class),

    PROXY("proxy", MfClass.class),

    /** {@link MfClass} stereotype. */
    RELATIONSHIP("relationship", MfClass.class),

    /** {@link MfProperty} stereotype. */
    RELATIONSHIP_KEY("relationshipKey", MfProperty.class),

    /** {@link MfInterface} stereotype. */
    SELECT("select", MfInterface.class),

    /** {@link MfPackage} stereotype. */
    UOF("uof", MfPackage.class),

    /** {@link MfClass} stereotype. */
    UML_PRIMITIVE("umlPrimitive", MfClass.class);

    private static final Map<String, AsdStereotypeName> MAP = new HashMap<>();
    private static final Map<String, AsdStereotypeName> MAPIC = new HashMap<>();

    static {
        for (final AsdStereotypeName x : AsdStereotypeName.values()) {
            MAP.put(x.getLiteral(), x);
            MAPIC.put(x.getLiteral().toLowerCase(), x);
        }
    }

    private final String literal;
    private final List<Class<? extends MfElement>> scope;

    @SafeVarargs
    private AsdStereotypeName(String literal,
                              Class<? extends MfElement>... scope) {
        this.literal = literal;
        this.scope = List.of(scope);
    }

    @Override
    public String getLiteral() {
        return literal;
    }

    public List<Class<? extends MfElement>> getScope() {
        return scope;
    }

    public boolean isAnyKey() {
        return this == COMPOSITE_KEY
                || this == KEY
                || this == RELATIONSHIP_KEY;
    }

    public static AsdStereotypeName of(String literal) {
        return MAP.get(literal);
    }

    public static boolean isRecognized(String literal) {
        return of(literal) != null;
    }

    public static AsdStereotypeName ofIgnoreCase(String literal) {
        return literal == null ? null : MAPIC.get(literal.toLowerCase());
    }

    public static AsdStereotypeName ofMostSimilar(String literal) {
        if (literal == null) {
            return null;
        } else {
            // This seems to give bets results
            final JaccardDistance ed = new JaccardDistance();
            final EditDistanceFrom<Double> edf = new EditDistanceFrom<>(ed, literal.toLowerCase());
            Double shortestDistance = null;
            AsdStereotypeName mostSimilar = null;
            for (final AsdStereotypeName name : AsdStereotypeName.values()) {
                final Double distance = edf.apply(name.getLiteral().toLowerCase());
                if (shortestDistance == null || distance < shortestDistance) {
                    shortestDistance = distance;
                    mostSimilar = name;
                }
            }
            if (shortestDistance != null && shortestDistance < 5) {
                return mostSimilar;
            } else {
                return null;
            }
        }
    }
}