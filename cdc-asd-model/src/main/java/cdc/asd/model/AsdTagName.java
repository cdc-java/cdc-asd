package cdc.asd.model;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.text.similarity.EditDistanceFrom;
import org.apache.commons.text.similarity.JaccardDistance;

import cdc.mf.model.MfEnum;

/**
 * Enumeration of standard ASD tag names.
 * <p>
 * <b>WARNING</b>: currently, we suppose they are case sensitive.
 *
 * @author Damien Carbonne
 */
public enum AsdTagName implements MfEnum {
    /**
     * 10.8.5: used to clarify changes made to a Class between two revisions.
     */
    CHANGE_NOTE("changeNote"),

    /**
     * 10.8.9: can be used to further clarify a definition by providing examples.
     */
    EXAMPLE("example"),

    /**
     * 6.3.1
     */
    ICN("ICN"),

    LONG_DESCRIPTION("longDescription"),

    /**
     * 10.8.8: can be used to add additional information relevant to a definition which is
     * not part of the immediate subject.
     */
    NOTE("note"),

    /**
     * 10.4.7 (Mantis 1315): All directed associations of {@code <<relationship>>}
     * classes are keys to the relationship, in addition to any possible
     * {@code <<relationshipKey>>} the class may contain. If the association is
     * not intended to be a key to the relationship, a tagged value named "noKey"
     * must be created for that association.
     */
    NO_KEY("noKey"),

    /**
     * 10.8.6: can be used to refer to external sources of information relevant to a
     * definition which is not part of the immediate subject.
     * <p>
     * Must also be populated with references to other data model concepts if
     * included or referenced by the definition.
     */
    REF("ref"),

    /**
     * 10.8.10: must be used when there is a change to the name for a published
     * Class or Interface.
     */
    REPLACES("replaces"),

    /**
     * 10.8.7: must be used when the definition for a Class or attribute is copied
     * or derived from a 'source' external to the S-Series IPS specifications.
     */
    SOURCE("source"),

    /**
     * 10.8.3: used to define a unique pattern for Class instances when
     * instantiated in an XML exchange file.
     */
    UID_PATTERN("uidPattern"),

    /**
     * 10.8.4: defines the units that are allowed when instantiating
     * a specific PropertType attribute.
     */
    UNIT("unit"),

    /**
     * 10.8.11: defines individual values that are allowed to be used for the
     * classification attribute under consideration.
     * <p>
     * Each validValue tagged values is used to populate enumeration values
     * for the corresponding attribute in the validValues XML schema.
     */
    VALID_VALUE("validValue"),

    /**
     * 10.8.12: where many classification attributes share the same list of valid values
     * or where the list of valid values comes from an external source
     * not directly tied to the attribute that is defined, can there
     * be a reference to a list of valid values through the use
     * of the validValueLibrary tagged value.
     */
    VALID_VALUE_LIBRARY("validValueLibrary"),

    /**
     * 10.8.1: used to create XML element names for UML constructs
     * in the XML schema.
     */
    XML_NAME("xmlName"),

    /**
     * 10.8.2: used to define XML element names which enables references to
     * be made to instances of a Class in the XML schema.
     */
    XML_REF_NAME("xmlRefName"),

    /**
     * 10.8.13: must be used (and must only be used) for classes that are
     * first indenture level specializations of the MessageContent
     * {@code <<exchange>>} class defined in the CDM UoF Message.
     */
    XML_SCHEMA_NAME("xmlSchemaName");

    private static final Map<String, AsdTagName> MAP = new HashMap<>();
    private static final Map<String, AsdTagName> MAPIC = new HashMap<>();
    private final String literal;

    static {
        for (final AsdTagName x : AsdTagName.values()) {
            MAP.put(x.getLiteral(), x);
            MAPIC.put(x.getLiteral().toLowerCase(), x);
        }
    }

    private AsdTagName(String literal) {
        this.literal = literal;
    }

    @Override
    public String getLiteral() {
        return literal;
    }

    public static AsdTagName of(String literal) {
        return MAP.get(literal);
    }

    public static AsdTagName ofIgnoreCase(String literal) {
        return literal == null ? null : MAPIC.get(literal.toLowerCase());
    }

    public static AsdTagName ofMostSimilar(String literal) {
        if (literal == null) {
            return null;
        } else {
            // This seems to give bets results
            final JaccardDistance ed = new JaccardDistance();
            final EditDistanceFrom<Double> edf = new EditDistanceFrom<>(ed, literal.toLowerCase());
            Double shortestDistance = null;
            AsdTagName mostSimilar = null;
            for (final AsdTagName name : AsdTagName.values()) {
                final Double distance = edf.apply(name.getLiteral().toLowerCase());
                if (shortestDistance == null || distance < shortestDistance) {
                    shortestDistance = distance;
                    mostSimilar = name;
                }
            }
            if (shortestDistance < 5) {
                return mostSimilar;
            } else {
                return null;
            }
        }
    }
}