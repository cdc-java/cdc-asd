package cdc.asd.model.ftags;

import java.util.List;
import java.util.regex.Pattern;

import cdc.util.lang.Checks;

/**
 * Formatting Tag.
 */
public interface FTag {
    public static final Pattern VALID_NAME_PATTERN =
            Pattern.compile("[a-zA-Z][a-zA-Z0-9]*");
    public static final Pattern MARKUP_PATTERN =
            Pattern.compile("\\[[^\\]]*\\]");

    public enum Kind {
        /** Open tag: [xxx] */
        OPEN,
        /** Close tag: [/xxx] */
        CLOSE,
        /** Open+Close tag: [xxx/] */
        OPEN_CLOSE,
        /** [/xxx/] */
        INVALID
    }

    /**
     * @return The source text that contains the tag.
     */
    public String getSource();

    public default String getSourceContext(int length) {
        final int from = Math.max(getBeginIndex() - length, 0);
        final String prefix = from == 0 ? "" : "...";
        final int to = Math.min(getEndIndex() + length, getSource().length());
        final String suffix = to == getSource().length() ? "" : "...";
        return prefix + getSource().substring(from, to) + suffix;
    }

    /**
     * @return The begin index (inclusive) of the tag in source.
     */
    public int getBeginIndex();

    /**
     * @return The end index (exclusive) of the tag in source.
     */
    public int getEndIndex();

    /**
     * @return The tag text, including markup.
     */
    public String getText();

    /**
     * @return The tag name, without markup.
     */
    public String getName();

    /**
     * @return {@code true} if this tag has a valid name (may be unrecognized).
     */
    public default boolean hasValidName() {
        return isValidName(getName());
    }

    public default FTagName getFTagName() {
        return FTagName.of(getName());
    }

    /**
     * @return The tag kind.
     */
    public Kind getKind();

    public static boolean isValidName(String name) {
        return name != null
                && VALID_NAME_PATTERN.matcher(name).matches();
    }

    /**
     * Creates a tag.
     *
     * @param source The source text.
     * @param beginIndex The begin index (inclusive).
     * @param endIndex The end index (exclusive).
     * @return A new tag.
     */
    public static FTag of(String source,
                          int beginIndex,
                          int endIndex) {
        Checks.isNotNullOrEmpty(source, "source");
        final String text = source.substring(beginIndex, endIndex);
        final int length = text.length();
        Checks.isTrue(text.charAt(0) == '[', "'{}' does not start with '['", text);
        Checks.isTrue(text.charAt(length - 1) == ']', "'{}' does not end with ']'", text);
        final boolean startSlash = text.charAt(1) == '/';
        final boolean endSlash = text.charAt(length - 2) == '/';
        final String name;
        final Kind kind;
        if (startSlash) {
            if (endSlash) {
                kind = Kind.INVALID;
                name = text.substring(2, length - 2);
            } else {
                kind = Kind.CLOSE;
                name = text.substring(2, length - 1);
            }
        } else {
            if (endSlash) {
                kind = Kind.OPEN_CLOSE;
                name = text.substring(1, length - 2);
            } else {
                kind = Kind.OPEN;
                name = text.substring(1, length - 1);
            }
        }

        return new FTagImpl(source,
                            beginIndex,
                            endIndex,
                            text,
                            name,
                            kind);
    }

    public static List<FTag> parse(String source) {
        return FTag.MARKUP_PATTERN.matcher(source)
                                  .results()
                                  .map(mr -> FTag.of(source, mr.start(), mr.end()))
                                  .toList();
    }

}

record FTagImpl(String source,
                int beginIndex,
                int endIndex,
                String text,
                String name,
                FTag.Kind kind)
        implements FTag {

    @Override
    public String getSource() {
        return source;
    }

    @Override
    public int getBeginIndex() {
        return beginIndex;
    }

    @Override
    public int getEndIndex() {
        return endIndex;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Kind getKind() {
        return kind;
    }
}