package cdc.asd.model.ftags;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration of possible formatting tag names.
 */
public enum FTagName {
    NOTE("note"),
    LI("li"),
    RANDOM_LIST("randomList"),
    REFER_TO("referTo");

    private final String name;
    private static final Map<String, FTagName> MAP = new HashMap<>();

    private FTagName(String name) {
        this.name = name;
    }

    static {
        for (final FTagName x : FTagName.values()) {
            MAP.put(x.getName(), x);
        }
    }

    public String getName() {
        return name;
    }

    public static FTagName of(String name) {
        return MAP.get(name);
    }
}