package cdc.asd.model.ftags;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class FTagsAnalyzer {
    private final String source;
    private final List<FTag> ftags;
    private final List<Message> messages = new ArrayList<>();
    private final boolean balanced;

    public enum MessageKind {
        /** The tag name is not recognized. */
        UNRECOGNIZED_NAME,
        /** A close tag has no matching open tag. */
        NO_MATCHING_OPEN,
        /** An open tag has no matching close tag. */
        NO_MATCHING_CLOSE,
        /** The tag markup is invalid. */
        INVALID
    }

    public record Message(FTag ftag,
                          MessageKind kind) {
        @Override
        public String toString() {
            return kind + " " + ftag.getText() + " at " + ftag.getBeginIndex() + " '" + ftag.getSourceContext(10) + "'";
        }
    }

    public FTagsAnalyzer(String source) {
        this.source = source;
        this.ftags = FTag.parse(source);
        final List<FTag> stack = new ArrayList<>();
        boolean b = true;
        for (final FTag ftag : ftags) {
            if (ftag.getFTagName() == null) {
                messages.add(new Message(ftag, MessageKind.UNRECOGNIZED_NAME));
            }
            switch (ftag.getKind()) {
            case OPEN -> {
                if (b) {
                    stack.add(ftag);
                }
            }

            case OPEN_CLOSE -> {
                // Ignore
            }

            case CLOSE -> {
                if (b) {
                    if (stack.isEmpty()
                            || !Objects.equals(stack.get(stack.size() - 1).getName(),
                                               ftag.getName())) {
                        messages.add(new Message(ftag, MessageKind.NO_MATCHING_OPEN));
                        b = false;
                    } else {
                        stack.remove(stack.size() - 1);
                    }
                }
            }
            case INVALID -> {
                messages.add(new Message(ftag, MessageKind.INVALID));
            }
            }
        }
        if (!stack.isEmpty()) {
            b = false;
            messages.add(new Message(stack.get(stack.size() - 1), MessageKind.NO_MATCHING_CLOSE));
        }
        this.balanced = b;
    }

    public String getSource() {
        return source;
    }

    public List<FTag> getFTags() {
        return ftags;
    }

    public boolean isBalanced() {
        return balanced;
    }

    public List<Message> getMessages() {
        return messages;
    }
}