package cdc.asd.model;

import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfTipRole;

public final class AsdModelUtils {
    private AsdModelUtils() {
    }

    public static String identify(MfTipRole role) {
        return role.getLiteral();
    }

    public static String identify(Class<? extends MfConnector> connectorClass) {
        if (MfAggregation.class.equals(connectorClass)) {
            return "aggregation";
        } else if (MfComposition.class.equals(connectorClass)) {
            return "composition";
        } else {
            return "association";
        }
    }

    public static String identify(AsdStereotypeName stereotypeName) {
        return "<<" + stereotypeName.getLiteral() + ">>";
    }

    public static String identify(AsdTagName tagName) {
        return tagName.getLiteral();
    }

    public static String identify(AsdPrimitiveTypeName typeName) {
        return typeName.getName();
    }

    public static String identify(AsdCompoundAttributeTypeName typeName) {
        return typeName.getName();
    }
}