package cdc.asd.model;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import cdc.asd.model.wrappers.AsdInterface;
import cdc.asd.model.wrappers.AsdType;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfType;

/**
 * Id associated to classes that have or inherit one or more key attributes.
 */
public final class AsdKeyBusinessId extends AsdBusinessId {
    private final List<MfProperty> keyProperties;
    private final List<MfTip> keyTips;
    private final Optional<MfType> whole;

    public AsdKeyBusinessId(MfType owner,
                            List<MfProperty> keyProperties,
                            List<MfTip> keyTips,
                            MfType whole) {
        super(owner);
        this.keyProperties = List.copyOf(keyProperties);
        this.keyTips = List.copyOf(keyTips);
        this.whole = Optional.ofNullable(whole);
    }

    /**
     * @return The list of key attributes.
     *         They can belong to the owner of this id or be inherited.
     */
    public List<MfProperty> getKeyProperties() {
        return keyProperties;
    }

    /**
     * @return The list of key tips.
     *         They can belong to the owner of this id or be inherited.
     */
    public List<MfTip> getKeyTips() {
        return keyTips;
    }

    /**
     * @return All key properties, including those from whole, recursively.
     */
    public List<MfProperty> getAllKeyProperties() {
        final List<MfProperty> plist = new ArrayList<>();
        addKeys(plist);
        return plist;
    }

    private void addKeys(List<MfProperty> plist) {
        if (whole.isPresent()) {
            final AsdBusinessId wholeId = getWholeId().orElseThrow();
            if (wholeId instanceof final AsdKeyBusinessId wbid) {
                wbid.addKeys(plist);
            }
        }
        plist.addAll(keyProperties);
    }

    public Optional<MfType> getWhole() {
        return whole;
    }

    /**
     * @return The optional id of the whole of which the owner of this id is a part.
     */
    public Optional<AsdBusinessId> getWholeId() {
        if (whole.isEmpty()) {
            return Optional.empty();
        } else {
            return Optional.of(whole.orElseThrow()
                                    .wrap(AsdType.class)
                                    .getBusinessId());
        }
    }

    public Optional<AsdSwitchBusinessId> getRootWholeSwitchId() {
        AsdKeyBusinessId iter = this;
        while (iter.getWholeId().isPresent()) {
            final AsdBusinessId wid = iter.getWholeId().orElseThrow();
            if (wid instanceof final AsdSwitchBusinessId sid) {
                return Optional.of(sid);
            } else if (wid instanceof final AsdKeyBusinessId next) {
                iter = next;
            } else {
                // Should not happen?
                return Optional.empty();
            }
        }
        return Optional.empty();
    }

    public List<AsdSwitchBusinessId> getTipSwitchIds() {
        // TODO tips with noKey connectors are excluded, so this returns an empty list
        return keyTips.stream()
                      .filter(t -> t.getParent().hasTags(AsdTagName.NO_KEY))
                      .map(MfTip::getType)
                      .filter(MfInterface.class::isInstance)
                      .map(MfInterface.class::cast)
                      .map(x -> x.wrap(AsdInterface.class).getBusinessId())
                      .map(AsdSwitchBusinessId.class::cast)
                      .toList();
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getClass().getSimpleName() + "@" + getOwner().getName());

        if (!getKeyTips().isEmpty()) {
            indent(out, level + 1);
            out.println("Key tips");
            for (final MfTip tip : getKeyTips()) {
                indent(out, level + 2);
                out.println(tip);
            }
        }
        if (!getAllKeyProperties().isEmpty()) {
            indent(out, level + 1);
            out.println("All key properties");
            for (final MfProperty property : getAllKeyProperties()) {
                indent(out, level + 2);
                out.println(property);
            }
        }
        if (getWhole().isPresent()) {
            indent(out, level + 1);
            out.println("Whole: " + getWhole().orElseThrow());
        }
        if (getRootWholeSwitchId().isPresent()) {
            indent(out, level + 1);
            out.println("Root Whole: " + getRootWholeSwitchId().orElseThrow().getOwner());
        }
    }
}