package cdc.asd.model;

import java.io.File;

import cdc.util.files.Files;

/**
 * Utility that can extract the different parts of an EA file name.
 * <p>
 * A file name (with extension removed) looks like: <b>{@code SSSSSS_X-Y[_Data_model]_AAA-BBQQQ...}</b>, where:
 * <ul>
 * <li><b>{@code SSSSSS}</b> is the spec name;
 * <li><b>{@code X-Y}</b> is the spec issue number (major + revision);
 * <li><b>{@code AAA-BB}</b> is the data model issue number (information) (release + progress);
 * <li><b>{@code QQQ...}</b> is optional the data model issue qualifier.
 * </ul>
 */
public final class AsdEaNaming {
    private final File eapFile;
    private final String specName;
    private final String specIssueNumber;
    private final String modelIssueNumber;
    private final String modelIssueQualifier;

    public AsdEaNaming(File eapFile) {
        this.eapFile = eapFile;

        final String name = Files.getNakedBasename(eapFile.getName()).replace("_Data_model", "");
        final int pos1 = name.indexOf('_');
        this.specName = name.substring(0, pos1);

        final int pos2 = name.indexOf('_', pos1 + 1);
        this.specIssueNumber = name.substring(pos1 + 1, pos2);

        this.modelIssueNumber = name.substring(pos2 + 1, pos2 + 7);

        this.modelIssueQualifier = name.length() > pos2 + 7
                ? name.substring(pos2 + 7)
                : "";
    }

    /**
     * @return The file used to construct this instance.
     */
    public File getEapFile() {
        return eapFile;
    }

    /**
     * @return The specification name: <b>{@code SSSSSS}</b>{@code _X-Y_Data_model_AAA-BB}.<br>
     *         It should be one of:
     *         <ul>
     *         <li>SX002D
     *         <li>S1000D
     *         <li>S2000M
     *         <li>S3000L
     *         <li>S4000P
     *         <li>S5000F
     *         <li>S6000T
     *         <li>SX000i
     *         </ul>
     */
    public String getSpecName() {
        return specName;
    }

    /**
     * @return The specification issue number: {@code SSSSSS_}<b>{@code X-Y}</b>{@code _Data_model_AAA-BB}.<br>
     *         It should be three (3) characters.
     *
     */
    public String getSpecIssueNumber() {
        return specIssueNumber;
    }

    /**
     * @return The specification issue major number: {@code SSSSSS_}<b>{@code X}</b>{@code -Y_Data_model_AAA-BB}.<br>
     *         It should be one (1) digit.
     */
    public String getSpecIssueMajorNumber() {
        final String s = getSpecIssueNumber();
        final int pos = s.indexOf('-');
        return s.substring(0, pos);
    }

    /**
     * @return The specification issue revision number: {@code SSSSSS_X-}<b>{@code Y}</b>{@code _Data_model_AAA-BB}.<br>
     *         It should be one (1) digit.
     */
    public String getSpecIssueRevisionNumber() {
        final String s = getSpecIssueNumber();
        final int pos = s.indexOf('-');
        return s.substring(pos + 1);
    }

    /**
     * @return The data model issue number (information): {@code SSSSSS_X-Y_Data_model_}<b>{@code AAA-BB}</b>.<br>
     *         It should be six (6) characters.
     */
    public String getModelIssueNumber() {
        return modelIssueNumber;
    }

    /**
     * @return The data model issue release number: {@code SSSSSS_X-Y_Data_model_}<b>{@code AAA}</b>{@code -BB}.<br>
     *         It should be three (3) digits.
     */
    public String getModelIssueReleaseNumber() {
        final String s = getModelIssueNumber();
        final int pos = s.indexOf('-');
        return s.substring(0, pos);
    }

    /**
     * @return The data model issue progress number: {@code SSSSSS_X-Y_Data_model_AAA-}<b>{@code BB}</b>.<br>
     *         It should be two (2) digits.
     */
    public String getModelIssueProgressNumber() {
        final String s = getModelIssueNumber();
        final int pos = s.indexOf('-');
        return s.substring(pos + 1);
    }

    public String getModelIssueQualifier() {
        return modelIssueQualifier;
    }

    /**
     * @return The model author.
     *         It is the spec name, or DMEWG for SX002D.
     */
    public String getModelAuthor() {
        if ("SX002D".equalsIgnoreCase(getSpecName())) {
            return "DMEWG";
        } else {
            return getSpecName().toUpperCase(); // FIXME upper case?
        }
    }

    /**
     * @return <b>{@code SSSSSS_X-Y_AAA-BB}</b>.
     */
    public String getModelName() {
        return getSpecName() + "_" + getSpecIssueNumber() + "_" + getModelIssueNumber();
    }

    /**
     * @return The project name for issues.
     */
    public String getProjectName() {
        return getSpecName();
    }

    /**
     * @return The snapshot name for issues.
     */
    public String getSnapshotName() {
        return getSpecIssueNumber() + "_" + getModelIssueNumber() + getModelIssueQualifier();
    }

    /**
     * @return The basename to use for ASD tools: <b>{@code SSSSSS_X-Y_AAA-BB}</b>.
     */
    public String getBasename() {
        return getModelName();
    }

    /**
     * @param name The specification name.
     * @return A compressed version of {@code name}, with the following replacements:
     *         <ul>
     *         <li>SX002D: SX2D
     *         <li>SX000i: SXi
     *         <li>S1000: S1
     *         <li>S2000: S2
     *         <li>S3000: S3
     *         <li>S4000: S4
     *         <li>S5000: S5
     *         <li>S6000: S6
     *         </ul>
     */
    public static String compress(String name) {
        return name.replace("SX002D", "SX2D")
                   .replace("SX000i", "SXi")
                   .replace("S1000", "S1")
                   .replace("S2000", "S2")
                   .replace("S3000", "S3")
                   .replace("S4000", "S4")
                   .replace("S5000", "S5")
                   .replace("S6000", "S6");
    }
}