package cdc.asd.model;

import java.io.PrintStream;
import java.util.Set;

import cdc.mf.model.MfClass;
import cdc.mf.model.MfInterface;

/**
 * Identifier associated to interfaces.
 * TODO Couldn't it be associated to classes?
 */
public final class AsdSwitchBusinessId extends AsdBusinessId {
    private final Set<MfClass> choice;

    public AsdSwitchBusinessId(MfInterface owner,
                               Set<MfClass> choice) {
        super(owner);
        this.choice = Set.copyOf(choice);
    }

    @Override
    public MfInterface getOwner() {
        return (MfInterface) super.getOwner();
    }

    public Set<MfClass> getChoice() {
        return choice;
    }

    @Override
    public void print(PrintStream out,
                      int level) {
        indent(out, level);
        out.println(getClass().getSimpleName() + "@" + getOwner().getName());
        for (final MfClass c : getChoice()) {
            indent(out, level + 1);
            out.println(c);
        }
    }
}