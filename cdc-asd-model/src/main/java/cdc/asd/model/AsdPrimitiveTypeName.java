package cdc.asd.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration of {@code <<primitive>>} types.
 * <p>
 * Organization exists as a {@code <<primitive>>} type, even if its stereotype is {@code <<class>>}.
 */
public enum AsdPrimitiveTypeName {
    CLASSIFICATION("ClassificationType"),

    // Temporal
    DATE("DateType"),
    DATE_TIME("DateTimeType"),
    TIME("TimeType"),

    DESCRIPTOR("DescriptorType"),
    IDENTIFIER("IdentifierType"),
    NAME("NameType"),

    // Properties
    PROPERTY("PropertyType"),
    NUMERICAL_PROPERTY("NumericalPropertyType"),
    SINGLE_VALUE_PROPERTY("SingleValuePropertyType"),
    VALUE_WITH_TOLERANCE_PROPERTY("ValueWithTolerancePropertyType"),
    VALUE_RANGE_PROPERTY("ValueRangePropertyType"),
    TEXT_PROPERTY("TextPropertyType"),

    STATE("StateType"),

    /**
     * Organization, as an attribute type, is used to <em>reference</em> to an Organization as a {@code <<class>>} type.
     * <p>
     * <b>WARNING:</b> recognition of Organization as an attribute type must not simply rely on the type name.
     */
    ORGANIZATION_REF("Organization");

    private static final Map<String, AsdPrimitiveTypeName> MAP = new HashMap<>();

    static {
        for (final AsdPrimitiveTypeName x : AsdPrimitiveTypeName.values()) {
            MAP.put(x.getName(), x);
        }
    }

    private final String name;

    private AsdPrimitiveTypeName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * @return {@code true} if this primitive type is a {@link #CLASSIFICATION}.
     */
    public boolean isClassificationType() {
        return this == CLASSIFICATION;
    }

    /**
     * @return {@code true} if this primitive type is a {@link #DATE} or {@link #DATE_TIME}.
     */
    public boolean isTemporalType() {
        return this == DATE
                || this == DATE_TIME
                || this == TIME;
    }

    /**
     * @return {@code true} if this primitive type is a {@link #DESCRIPTOR}.
     */
    public boolean isDescriptorType() {
        return this == DESCRIPTOR;
    }

    /**
     * @return {@code true} if this primitive type is an {@link #IDENTIFIER}.
     */
    public boolean isIdentifierType() {
        return this == IDENTIFIER;
    }

    /**
     * @return {@code true} if this primitive type is an {@link #NAME}.
     */
    public boolean isNameType() {
        return this == NAME;
    }

    /**
     * @return {@code true} if this primitive type is a {@link #PROPERTY},
     *         {@link #TEXT_PROPERTY}, {@link #NUMERICAL_PROPERTY}, {@link #SINGLE_VALUE_PROPERTY},
     *         {@link #VALUE_RANGE_PROPERTY} or {@link #VALUE_WITH_TOLERANCE_PROPERTY} .
     */
    public boolean isPropertyType() {
        return this == PROPERTY
                || isTextPropertyType()
                || isNumericalPropertyType();
    }

    /**
     * @return {@code true} if this primitive type is an {@link #TEXT_PROPERTY}.
     */
    public boolean isTextPropertyType() {
        return this == TEXT_PROPERTY;
    }

    /**
     * @return {@code true} if this primitive type is a {@link #NUMERICAL_PROPERTY},
     *         {@link #SINGLE_VALUE_PROPERTY}, {@link #VALUE_RANGE_PROPERTY}
     *         or {@link #VALUE_WITH_TOLERANCE_PROPERTY} .
     */
    public boolean isNumericalPropertyType() {
        return this == NUMERICAL_PROPERTY
                || this == SINGLE_VALUE_PROPERTY
                || this == VALUE_RANGE_PROPERTY
                || this == VALUE_WITH_TOLERANCE_PROPERTY;
    }

    /**
     * @return {@code true} if this primitive type is an {@link #STATE}.
     */
    public boolean isStateType() {
        return this == STATE;
    }

    /**
     * @return {@code true} if this primitive type is an {@link #ORGANIZATION_REF}.
     */
    public boolean isOrganizationRef() {
        return this == ORGANIZATION_REF;
    }

    public static AsdPrimitiveTypeName of(String name) {
        return MAP.get(name);
    }
}