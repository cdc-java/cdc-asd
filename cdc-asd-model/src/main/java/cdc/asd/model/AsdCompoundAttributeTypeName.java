package cdc.asd.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Enumeration of declared {@code <<compoundAttribute>>} classes.
 */
public enum AsdCompoundAttributeTypeName {
    AUTHORIZED_LIFE("AuthorizedLife"),

    DATED_CLASSIFICATION("DatedClassification"),
    TIME_STAMPED_CLASSIFICATION("TimeStampedClassification"),

    DATE_RANGE("DateRange,"),
    DATE_TIME_RANGE("DateTimeRange,"),
    TIME_RANGE("TimeRange,"),
    SERIAL_NUMBER_RANGE("SerialNumberRange"),

    DIMENSIONS("Dimensions"),

    THREE_DIMENSIONAL("ThreeDimensional"),
    CYLINDER("Cylinder"),
    CUBOID("Cuboid"),
    SPHERE("Sphere"),

    AREA("Area"),
    TRIANGLE("Triangle"),
    RECTANGLE("Rectangle"),
    CIRCLE("Circle");

    private static final Map<String, AsdCompoundAttributeTypeName> MAP = new HashMap<>();

    static {
        for (final AsdCompoundAttributeTypeName x : AsdCompoundAttributeTypeName.values()) {
            MAP.put(x.getName(), x);
        }
    }

    private final String name;

    private AsdCompoundAttributeTypeName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static AsdCompoundAttributeTypeName of(String name) {
        return MAP.get(name);
    }
}