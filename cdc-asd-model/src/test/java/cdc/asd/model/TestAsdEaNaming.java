package cdc.asd.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;

import org.junit.jupiter.api.Test;

class TestAsdEaNaming {
    @Test
    void test5000FStandard() {
        final AsdEaNaming naming = new AsdEaNaming(new File("S5000F_3-1_Data_model_001-02.EAP"));
        assertEquals("S5000F", naming.getSpecName());
        assertEquals("3-1", naming.getSpecIssueNumber());
        assertEquals("3", naming.getSpecIssueMajorNumber());
        assertEquals("1", naming.getSpecIssueRevisionNumber());
        assertEquals("001-02", naming.getModelIssueNumber());
        assertEquals("001", naming.getModelIssueReleaseNumber());
        assertEquals("02", naming.getModelIssueProgressNumber());
        assertEquals("", naming.getModelIssueQualifier());
        assertEquals("S5000F_3-1_001-02", naming.getModelName());
        assertEquals("S5000F_3-1_001-02", naming.getBasename());
        assertEquals("S5000F", naming.getModelAuthor());
    }

    @Test
    void test5000FNonStandard() {
        final AsdEaNaming naming = new AsdEaNaming(new File("S5000F_3-1_001-02 (qualifier).EAP"));
        assertEquals("S5000F", naming.getSpecName());
        assertEquals("3-1", naming.getSpecIssueNumber());
        assertEquals("3", naming.getSpecIssueMajorNumber());
        assertEquals("1", naming.getSpecIssueRevisionNumber());
        assertEquals("001-02", naming.getModelIssueNumber());
        assertEquals("001", naming.getModelIssueReleaseNumber());
        assertEquals("02", naming.getModelIssueProgressNumber());
        assertEquals(" (qualifier)", naming.getModelIssueQualifier());
        assertEquals("S5000F_3-1_001-02", naming.getModelName());
        assertEquals("S5000F_3-1_001-02", naming.getBasename());
        assertEquals("S5000F", naming.getModelAuthor());
    }

    @Test
    void testCompress() {
        assertEquals("S1D", AsdEaNaming.compress("S1000D"));
        assertEquals("S2M", AsdEaNaming.compress("S2000M"));
        assertEquals("S5F", AsdEaNaming.compress("S5000F"));
    }
}