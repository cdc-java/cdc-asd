package cdc.asd.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import cdc.asd.model.ftags.FTagsAnalyzer;

class FTagsAnalyzerTest {
    private static void checkValid(String source,
                                   int count) {
        final FTagsAnalyzer analyzer = new FTagsAnalyzer(source);
        assertEquals(count, analyzer.getFTags().size());
        assertEquals(source, analyzer.getSource());
        assertTrue(analyzer.isBalanced());
        assertEquals(0, analyzer.getMessages().size());
    }

    @Test
    void testInvalid() {
        final String source = "[/note/]";
        final FTagsAnalyzer analyzer = new FTagsAnalyzer(source);
        assertEquals(1, analyzer.getFTags().size());
        assertEquals(source, analyzer.getSource());
        assertTrue(analyzer.isBalanced());
        assertEquals(1, analyzer.getMessages().size());
        assertEquals(FTagsAnalyzer.MessageKind.INVALID, analyzer.getMessages().get(0).kind());
    }

    @Test
    void testUnrecognizedName() {
        final String source = "[tag][/tag]";
        final FTagsAnalyzer analyzer = new FTagsAnalyzer(source);
        assertEquals(2, analyzer.getFTags().size());
        assertEquals(source, analyzer.getSource());
        assertTrue(analyzer.isBalanced());
        assertEquals(2, analyzer.getMessages().size());
        assertEquals(FTagsAnalyzer.MessageKind.UNRECOGNIZED_NAME, analyzer.getMessages().get(0).kind());
        assertEquals(FTagsAnalyzer.MessageKind.UNRECOGNIZED_NAME, analyzer.getMessages().get(1).kind());
    }

    @Test
    void testNoMatchinghOpen() {
        final String source = "[/note]";
        final FTagsAnalyzer analyzer = new FTagsAnalyzer(source);
        assertEquals(1, analyzer.getFTags().size());
        assertEquals(source, analyzer.getSource());
        assertFalse(analyzer.isBalanced());
        assertEquals(1, analyzer.getMessages().size());
        assertEquals(FTagsAnalyzer.MessageKind.NO_MATCHING_OPEN, analyzer.getMessages().get(0).kind());
    }

    @Test
    void testNoMatchinghClose() {
        final String source = "[note]";
        final FTagsAnalyzer analyzer = new FTagsAnalyzer(source);
        assertEquals(1, analyzer.getFTags().size());
        assertEquals(source, analyzer.getSource());
        assertFalse(analyzer.isBalanced());
        assertEquals(1, analyzer.getMessages().size());
        assertEquals(FTagsAnalyzer.MessageKind.NO_MATCHING_CLOSE, analyzer.getMessages().get(0).kind());
    }

    @Test
    void testValid() {
        checkValid("[note][/note]", 2);
        checkValid("[referTo/]", 1);
        checkValid("[li]XXX[/li]", 2);
        checkValid("[randomList][/randomList]", 2);
        checkValid("[randomList][li]XXX[/li][/randomList]", 4);
    }
}