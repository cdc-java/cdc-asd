package cdc.asd.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import cdc.asd.model.ftags.FTag;

class FTagTest {
    @Test
    void testOf() {
        final String source = "[tag1][/tag2][tag3/][/tag4/][tag/5]";

        final FTag ft1 = FTag.of(source, 0, 6);
        assertEquals(source, ft1.getSource());
        assertEquals("tag1", ft1.getName());
        assertEquals("[tag1]", ft1.getText());
        assertSame(FTag.Kind.OPEN, ft1.getKind());
        assertTrue(ft1.hasValidName());

        final FTag ft2 = FTag.of(source, 6, 13);
        assertEquals(source, ft2.getSource());
        assertEquals("tag2", ft2.getName());
        assertEquals("[/tag2]", ft2.getText());
        assertSame(FTag.Kind.CLOSE, ft2.getKind());
        assertTrue(ft2.hasValidName());

        final FTag ft3 = FTag.of(source, 13, 20);
        assertEquals(source, ft3.getSource());
        assertEquals("tag3", ft3.getName());
        assertEquals("[tag3/]", ft3.getText());
        assertSame(FTag.Kind.OPEN_CLOSE, ft3.getKind());
        assertTrue(ft3.hasValidName());

        final FTag ft4 = FTag.of(source, 20, 28);
        assertEquals(source, ft4.getSource());
        assertEquals("tag4", ft4.getName());
        assertEquals("[/tag4/]", ft4.getText());
        assertSame(FTag.Kind.INVALID, ft4.getKind());
        assertTrue(ft4.hasValidName());

        final FTag ft5 = FTag.of(source, 28, 35);
        assertEquals(source, ft5.getSource());
        assertEquals("tag/5", ft5.getName());
        assertEquals("[tag/5]", ft5.getText());
        assertSame(FTag.Kind.OPEN, ft5.getKind());
        assertFalse(ft5.hasValidName());
    }

    @Test
    void testParse() {
        final String source = "[tag1][/tag2][tag3/][/tag4/][tag/5]";
        final List<FTag> expected =
                List.of(FTag.of(source, 0, 6),
                        FTag.of(source, 6, 13),
                        FTag.of(source, 13, 20),
                        FTag.of(source, 20, 28),
                        FTag.of(source, 28, 35));
        final List<FTag> parsed = FTag.parse(source);
        assertEquals(expected, parsed);
    }
}