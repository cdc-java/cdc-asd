# AsdModelChecker

`AsdModelChecker` loads an `ASD MF XML model` and checks it.  

See [RULES](RULES.md) for a list of rules.

## AsdModelChecker options
Options of `AsdModelChecker `are:

````
USAGE
AsdModelChecker [--args-file <arg>] [--args-file-charset <arg>] --basename <arg> [--best |
                      --fastest] [--disable <arg> | --enable <arg>] [--disable-all | --enable-all]
                      [-h | -v] [--help-width <arg>] --model <arg> --output-dir <arg>
                      [--profile-config <arg>] [--ref-model <arg>]  [--verbose]

Utility that can load an ASD model (MF XML format), check it and save found issues.
It will also save the configured profile used to check the model.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option
                                or value).
                                A line is ignored when it is empty or starts by any number of white
                                spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is
                                read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file
                                encoding.
    --basename <arg>            Mandatory base name of generated files.
    --best                      Use options that generate best output.
    --disable <arg>             Name(s) of rule checkers to disable.
    --disable-all               Disable all rules checkers. Use with --enable to enable individual
                                rule checkers.
    --enable <arg>              Name(s) of rule checkers to enable.
    --enable-all                Enable all rules checkers. Use with --disable to disable individual
                                rule checkers.
    --fastest                   Use options that are fast to generate output (default).
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --model <arg>               Mandatory name of the XML ASD MF model to check.
    --output-dir <arg>          Mandatory name of the output directory. If this directory does not
                                exist, it is created.
    --profile-config <arg>      Optional name of the ASD Profile Config file to load.
    --ref-model <arg>           Optional name of the reference XML ASD MF model to check against.
                                Used by some checkers as a reference to compare the checked model.
 -v,--version                   Prints version and exits.
    --verbose                   Print messages.
````