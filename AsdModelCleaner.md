# AsdModelCleaner

`AsdModelCleaner` loads an `ASD MF XML model` and applies some simple transformations to make the model cleaner.  
It has the following capabilities, each enabled with an option:
- Removal of leading and trailing spaces in notes and tag values.
- Removal of html tags.
- Replacement of html entities.
- Split of tag values that contain several tokens.


## AsdModelCleaner options
Options of `AsdModelCleaner `are:

````
USAGE
AsdModelCleaner [--add-missing-xml-name-tags] [--add-missing-xml-ref-name-tags] [--all] [--args-file
                      <arg>] [--args-file-charset <arg>] --basename <arg>
                      [--create-base-object-inheritance] [--create-enumerations]
                      [--deduplicate-names] [--fix-tag-name-case] [-h | -v] [--help-width <arg>]
                      --model <arg> --output-dir <arg> [--remove-extra-spaces] [--remove-html-tags]
                      [--replace-html-entities] [--split-ref-tags]  [--verbose]

Utility that can clean (partially fix) an ASD MF XML model.
A stats file is also generated.

OPTIONS
    --add-missing-xml-name-tags        Add xmlName tags that are missing.
    --add-missing-xml-ref-name-tags    Add xmlRefName tags that are missing.
    --all                              Enable all cleaning options.
    --args-file <arg>                  Optional name of the file from which options can be read.
                                       A line is either ignored or interpreted as a single argument
                                       (option or value).
                                       A line is ignored when it is empty or starts by any number of
                                       white spaces followed by '#'.
                                       A line that only contains white spaces is an argument.
                                       A comment starts by a '#' not following a '\'. The "\#"
                                       sequence is read as '#'.
    --args-file-charset <arg>          Optional name of the args file charset.
                                       It may be used if args file encoding is not the OS default
                                       file encoding.
    --basename <arg>                   Mandatory base name of generated files (cleaned model and
                                       stats).
    --create-base-object-inheritance   Make BaseObject inheritance explicit.
                                       DO NOT USE with checks.
    --create-enumerations              Create enumerations for properties that have validValue tags.
    --deduplicate-names                When several sibling packages, classes, interfaces have the
                                       same name, they are renamed to make their name unique.
    --fix-tag-name-case                When a tag name case is invalid, fix it.
 -h,--help                             Prints this help and exits.
    --help-width <arg>                 Optional help width (default: 74).
    --model <arg>                      Mandatory name of the ASD MF XML model to clean.
    --output-dir <arg>                 Mandatory name of the output directory. If this directory
                                       does not exist, it is created.
    --remove-extra-spaces              All leading and trailing spaces are removed from notes and
                                       tag values.
    --remove-html-tags                 All html tags are removed from notes.
    --replace-html-entities            Some html entities are replaced in notes.
    --split-ref-tags                   All ref tags that contain several refs are split into tags
                                       that contain one ref.
 -v,--version                          Prints version and exits.
    --verbose                          Prints messages.
````