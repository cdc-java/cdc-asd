# AsdProfileExporter
`AsdProfileExporter` exports the profile used by `AsdModelChecker`.


## AsdProfileExporter options
Options of `AsdProfileExporter` are:

````
USAGE
AsdProfileExporter [--args-file <arg>] [--args-file-charset <arg>] [-h | -v] [--help-width <arg>]
                         --output <arg> [--profile | --profile-config]  [--show-empty-sections]
                         [--show-rules-domain] [--show-rules-enabling] [--show-rules-severities]
                         [--verbose]

Utility that can export the ASD Profile or Profile Config.
Supported formats for Profile are: md, html, xml, json, xlsx, xls, xlsm, csv and ods.
Supported formats for Profile Config are: xlsx, xls, xlsm, csv and ods.
Some of them cannot be loaded.

OPTIONS
    --args-file <arg>           Optional name of the file from which options can be read.
                                A line is either ignored or interpreted as a single argument (option
                                or value).
                                A line is ignored when it is empty or starts by any number of white
                                spaces followed by '#'.
                                A line that only contains white spaces is an argument.
                                A comment starts by a '#' not following a '\'. The "\#" sequence is
                                read as '#'.
    --args-file-charset <arg>   Optional name of the args file charset.
                                It may be used if args file encoding is not the OS default file
                                encoding.
 -h,--help                      Prints this help and exits.
    --help-width <arg>          Optional help width (default: 74).
    --output <arg>              Name of the export file. Its extension must be recognized and
                                supported.
    --profile                   Generate the profile (default).
    --profile-config            Generate the profile config.
    --show-empty-sections       If enabled, empty sections are exported.
    --show-rules-domain         If enabled, rules domain is exported.
    --show-rules-enabling       If enabled, rules enabling is exported.
    --show-rules-severities     If enabled, rules severities are exported.
 -v,--version                   Prints version and exits.
    --verbose                   Print messages.
````