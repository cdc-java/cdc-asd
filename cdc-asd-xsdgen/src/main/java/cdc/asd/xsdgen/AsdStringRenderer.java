package cdc.asd.xsdgen;

import java.util.Locale;

import org.apache.commons.text.StringEscapeUtils;
import org.stringtemplate.v4.AttributeRenderer;

public class AsdStringRenderer implements AttributeRenderer<String> {
    private static String xmlEscape(String s) {
        return StringEscapeUtils.escapeXml11(s);
    }

    private static String descriptionEscape(String s) {
        return s.replace("<<", "[").replace(">>", "]");
    }

    @Override
    public String toString(String value,
                           String formatString,
                           Locale locale) {
        if (formatString == null) {
            return value;
        } else {
            return switch (formatString) {
            case "xml-escape" -> xmlEscape(value);
            case "description-escape" -> descriptionEscape(value);
            // There is an issue when accumulating formatting in ST4
            case "xml-description-escape" -> xmlEscape(descriptionEscape(value));
            default -> throw new IllegalArgumentException("Unknown format: " + formatString);
            };
        }
    }
}