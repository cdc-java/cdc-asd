package cdc.asd.xsdgen;

import java.io.File;

public final class AsdXsdParams {
    private AsdXsdParams() {
    }

    public static final class Files {
        private Files() {
        }

        private static String getPrefix(AsdXsdGenerationArgs args) {
            return "todo_"; // TODO
        }

        public static File getDatasetFile(AsdXsdGenerationArgs args) {
            // FIXME name seems to depend on the spec
            return new File(getPrefix(args) + "Dataset.xsd");
        }

        public static File getProjectSpecificExchangeDefinitionsFile(AsdXsdGenerationArgs args) {
            return new File(getPrefix(args) + "project_specific_exchange_definitions.xsd");
        }

        public static File getProjectSpecificExchangesFile(AsdXsdGenerationArgs args) {
            return new File(getPrefix(args) + "project_specific_exchanges.xsd");
        }

        public static File getValidValuesFile(AsdXsdGenerationArgs args) {
            return new File(getPrefix(args) + "valid_values.xsd");
        }

        public static File getValidValuesExternalLibrariesFile(AsdXsdGenerationArgs args) {
            return new File(getPrefix(args) + "valid_values_external_libraries.xsd");
        }

        public static File getValidValuesLibrariesFile(AsdXsdGenerationArgs args) {
            return new File(getPrefix(args) + "valid_values_libraries.xsd");
        }

        public static File getValidValuesProjectExtensionsFile(AsdXsdGenerationArgs args) {
            return new File(getPrefix(args) + "valid_values_project_extensions.xsd");
        }

        public static File getValidValuesUnitsFile(AsdXsdGenerationArgs args) {
            return new File(getPrefix(args) + "valid_values_units.xsd");
        }
    }

    /**
     * Names of parameters passed to ST4 templates.
     */
    public static final class Names {
        private Names() {
        }

        public static final String ARGS = "args";
        public static final String DATASET = "dataset";
        public static final String SET = "set";
        public static final String DEPS = "deps";
    }
}