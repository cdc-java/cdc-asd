package cdc.asd.xsdgen.data;

import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

public final class AsdExtendInterface extends AsdBase {
    private final List<AsdParticle> attributeGroups;
    private final List<AsdParticle> aggregations;
    private final List<AsdParticle> compositions;
    private final List<AsdParticle> associations;

    private AsdExtendInterface(Builder builder) {
        super(builder);
        this.attributeGroups = Checks.isNotNull(builder.attributeGroups, "attributeGroups");
        this.aggregations = Checks.isNotNull(builder.aggregations, "aggregations");
        this.compositions = Checks.isNotNull(builder.compositions, "compositions");
        this.associations = Checks.isNotNull(builder.associations, "associations");
    }

    public List<AsdParticle> getAttributeGroups() {
        return attributeGroups;
    }

    public List<AsdParticle> getAggregations() {
        return aggregations;
    }

    public List<AsdParticle> getCompositions() {
        return compositions;
    }

    public List<AsdParticle> getAssociations() {
        return associations;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdBase.Builder<Builder> {
        private List<AsdParticle> attributeGroups = Collections.emptyList();
        private List<AsdParticle> aggregations = Collections.emptyList();
        private List<AsdParticle> compositions = Collections.emptyList();
        private List<AsdParticle> associations = Collections.emptyList();

        private Builder() {
        }

        public Builder attributeGroups(List<AsdParticle> attributeGroups) {
            this.attributeGroups = attributeGroups;
            return this;
        }

        public Builder aggregations(List<AsdParticle> aggregations) {
            this.aggregations = aggregations;
            return this;
        }

        public Builder compositions(List<AsdParticle> compositions) {
            this.compositions = compositions;
            return this;
        }

        public Builder associations(List<AsdParticle> associations) {
            this.associations = associations;
            return this;
        }

        @Override
        public AsdExtendInterface build() {
            return new AsdExtendInterface(this);
        }
    }
}