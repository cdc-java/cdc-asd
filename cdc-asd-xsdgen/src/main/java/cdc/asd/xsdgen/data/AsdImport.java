package cdc.asd.xsdgen.data;

import cdc.util.lang.Checks;

public interface AsdImport extends AsdDep {
    public String getNamespace();

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdDep.Builder<Builder> {
        private String namespace;

        public Builder namespace(String namespace) {
            this.namespace = namespace;
            return self();
        }

        @Override
        public AsdImport build() {
            return new AsdImportImpl(schemaLocation,
                                     namespace);
        }
    }
}

record AsdImportImpl(String schemaLocation,
                     String namespace)
        implements AsdImport {
    AsdImportImpl {
        Checks.isNotNull(schemaLocation, "schemaLocations");
        Checks.isNotNull(namespace, "namespace");
    }

    @Override
    public String getSchemaLocation() {
        return schemaLocation;
    }

    @Override
    public String getNamespace() {
        return namespace;
    }
}