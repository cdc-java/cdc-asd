package cdc.asd.xsdgen.data;

import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

public final class AsdSelectInterface extends AsdBase {
    private final Kind kind;
    private final List<AsdSelectInterfaceElement> elements;

    public enum Kind {
        GROUP,
        COMPLEX_TYPE
    }

    private AsdSelectInterface(Builder builder) {
        super(builder);
        this.kind = Checks.isNotNull(builder.kind, "kind");
        this.elements = Checks.isNotNull(builder.elements, "elements");
    }

    public Kind getKind() {
        return kind;
    }

    public boolean isGroup() {
        return getKind() == Kind.GROUP;
    }

    public boolean isComplexType() {
        return getKind() == Kind.COMPLEX_TYPE;
    }

    public List<AsdSelectInterfaceElement> getElements() {
        return elements;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdBase.Builder<Builder> {
        private Kind kind;
        private List<AsdSelectInterfaceElement> elements = Collections.emptyList();

        private Builder() {
        }

        public Builder kind(Kind kind) {
            this.kind = kind;
            return this;
        }

        public Builder elements(List<AsdSelectInterfaceElement> elements) {
            this.elements = elements;
            return this;
        }

        @Override
        public AsdSelectInterface build() {
            return new AsdSelectInterface(this);
        }
    }
}