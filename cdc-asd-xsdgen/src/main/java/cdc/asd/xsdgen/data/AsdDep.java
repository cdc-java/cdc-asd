package cdc.asd.xsdgen.data;

import java.io.File;

public interface AsdDep {
    public default boolean isInclude() {
        return this instanceof AsdInclude;
    }

    public default boolean isImport() {
        return this instanceof AsdImport;
    }

    public String getSchemaLocation();

    public abstract static class Builder<B extends Builder<B>> {
        protected String schemaLocation;

        protected Builder() {
        }

        protected B self() {
            @SuppressWarnings("unchecked")
            final B tmp = (B) this;
            return tmp;
        }

        public B schemaLocation(String schemaLocation) {
            this.schemaLocation = schemaLocation;
            return self();
        }

        public B schemaLocation(File schemaLocation) {
            this.schemaLocation = schemaLocation.getName();
            return self();
        }

        public abstract AsdDep build();
    }
}