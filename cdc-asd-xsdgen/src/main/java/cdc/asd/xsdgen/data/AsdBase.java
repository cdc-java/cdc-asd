package cdc.asd.xsdgen.data;

import cdc.util.lang.Checks;

public abstract class AsdBase {
    private final String name;
    private final String source;
    private final String description;

    protected AsdBase(Builder<?> builder) {
        this.name = Checks.isNotNull(builder.name, "name");
        this.source = builder.source;
        this.description = builder.description;
    }

    public final String getName() {
        return name;
    }

    public final String getSource() {
        return source;
    }

    public final String getDescription() {
        return description;
    }

    public abstract static class Builder<B extends Builder<B>> {
        private String name;
        private String source;
        private String description;

        protected Builder() {
        }

        @SuppressWarnings("unchecked")
        protected B self() {
            return (B) this;
        }

        public final B name(String name) {
            this.name = name;
            return self();
        }

        public final B source(String source) {
            this.source = source;
            return self();
        }

        public final B description(String description) {
            this.description = description;
            return self();
        }

        public abstract AsdBase build();
    }
}