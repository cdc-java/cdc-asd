package cdc.asd.xsdgen.data;

import cdc.util.lang.Checks;

public interface AsdSelectInterfaceElement {
    public enum Kind {
        ELEMENT,
        GROUP_REF
    }

    public Kind getKind();

    public default boolean isElement() {
        return getKind() == Kind.ELEMENT;
    }

    public default boolean isGroupRef() {
        return getKind() == Kind.GROUP_REF;
    }

    public String getName();

    public String getType();

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Kind kind;
        private String name;
        private String type;

        private Builder() {
        }

        public Builder kind(Kind kind) {
            this.kind = kind;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public AsdSelectInterfaceElement build() {
            return new AsdSelectInterfaceElementImpl(kind,
                                                     name,
                                                     type);
        }
    }
}

record AsdSelectInterfaceElementImpl(AsdSelectInterfaceElement.Kind kind,
                                     String name,
                                     String type)
        implements AsdSelectInterfaceElement {

    AsdSelectInterfaceElementImpl {
        Checks.isNotNull(kind, "kind");
        // Checks.isNotNull(name, "name");
        Checks.isNotNull(type, "type");
    }

    @Override
    public AsdSelectInterfaceElement.Kind getKind() {
        return kind;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }
}