package cdc.asd.xsdgen.data;

import java.util.Comparator;
import java.util.List;

import cdc.asd.model.wrappers.AsdClass;
import cdc.mf.model.MfClass;
import cdc.util.lang.CollectionUtils;

/**
 * Specialization element.
 */
public interface AsdSpecializationElement {
    /**
     * Comparator that uses Depth First Search, using alphabetical order of class name for siblings.
     * <p>
     * This is the comparator that gives the closest result to what is generated by current XSD generator.
     * There is no specified order.
     */
    public static final Comparator<AsdSpecializationElement> DFS_COMPARATOR =
            (o1,
             o2) -> {
                final List<String> l1 = o1.getOrigin().wrap(AsdClass.class)
                                          .getGenitors()
                                          .stream()
                                          .map(MfClass::getName)
                                          .toList();
                final List<String> l2 = o2.getOrigin().wrap(AsdClass.class)
                                          .getGenitors()
                                          .stream()
                                          .map(MfClass::getName)
                                          .toList();
                return CollectionUtils.compareLexicographic(l1, l2);
            };

    public MfClass getOrigin();

    public String getName();

    public String getType();

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private MfClass origin;
        private String name;
        private String type;

        private Builder() {
        }

        public Builder origin(MfClass origin) {
            this.origin = origin;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public AsdSpecializationElement build() {
            return new AsdSpecializationElementImpl(origin,
                                                    name,
                                                    type);
        }
    }
}

record AsdSpecializationElementImpl(MfClass origin,
                                    String name,
                                    String type)
        implements AsdSpecializationElement {

    @Override
    public MfClass getOrigin() {
        return origin;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }
}