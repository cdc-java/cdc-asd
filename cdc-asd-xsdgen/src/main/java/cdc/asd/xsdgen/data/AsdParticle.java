package cdc.asd.xsdgen.data;

import cdc.util.lang.Checks;

public interface AsdParticle {
    public enum Kind {
        ELEMENT,
        GROUP
    }

    public Kind getKind();

    public default boolean isElement() {
        return getKind() == Kind.ELEMENT;
    }

    public default boolean isGroup() {
        return getKind() == Kind.GROUP;
    }

    public String getName();

    public String getType();

    public Boolean getNillable();

    public Integer getMinOccurs();

    public Integer getMaxOccurs();

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private Kind kind;
        private String name;
        private String type;
        private Boolean nillable;
        private Integer minOccurs;
        private Integer maxOccurs;

        private Builder() {
        }

        public Builder kind(Kind kind) {
            this.kind = kind;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder nillable(Boolean nillable) {
            this.nillable = nillable;
            return this;
        }

        public Builder minOccurs(Integer minOccurs) {
            this.minOccurs = minOccurs;
            return this;
        }

        public Builder maxOccurs(Integer maxOccurs) {
            this.maxOccurs = maxOccurs;
            return this;
        }

        public AsdParticle build() {
            return new AsdParticleImpl(kind,
                                       name,
                                       type,
                                       nillable,
                                       minOccurs,
                                       maxOccurs);
        }
    }
}

record AsdParticleImpl(AsdParticle.Kind kind,
                       String name,
                       String type,
                       Boolean nillable,
                       Integer minOccurs,
                       Integer maxOccurs)
        implements AsdParticle {

    AsdParticleImpl {
        Checks.isNotNull(kind, "kind");
        Checks.isNotNull(name, "name");
        Checks.isNotNull(type, "type");
    }

    @Override
    public AsdParticle.Kind getKind() {
        return kind;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public Boolean getNillable() {
        return nillable;
    }

    @Override
    public Integer getMinOccurs() {
        return minOccurs;
    }

    @Override
    public Integer getMaxOccurs() {
        return maxOccurs;
    }
}