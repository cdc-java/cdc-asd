package cdc.asd.xsdgen.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cdc.args.Strictness;
import cdc.asd.model.AsdKeyBusinessId;
import cdc.asd.model.AsdStereotypeName;
import cdc.asd.model.AsdSwitchBusinessId;
import cdc.asd.model.wrappers.AsdClass;
import cdc.asd.model.wrappers.AsdElement;
import cdc.asd.model.wrappers.AsdInterface;
import cdc.asd.model.wrappers.AsdProperty;
import cdc.asd.model.wrappers.AsdType;
import cdc.asd.xsdgen.data.AsdSpecialization.Kind;
import cdc.mf.model.MfAggregation;
import cdc.mf.model.MfAssociation;
import cdc.mf.model.MfCardinality;
import cdc.mf.model.MfClass;
import cdc.mf.model.MfComposition;
import cdc.mf.model.MfConnector;
import cdc.mf.model.MfElement;
import cdc.mf.model.MfInterface;
import cdc.mf.model.MfModel;
import cdc.mf.model.MfNameItem;
import cdc.mf.model.MfProperty;
import cdc.mf.model.MfTip;
import cdc.mf.model.MfType;
import cdc.util.strings.StringUtils;

public class AsdModelToDataset {
    private static final Logger LOGGER = LogManager.getLogger(AsdModelToDataset.class);
    private final MfModel model;
    private static final String NON_ABSTRACT_CLASSES = "NonAbstractClasses";
    private static final String REF = "Ref";
    private static final String TBD = "TBD_";

    public AsdModelToDataset(MfModel model) {
        this.model = model;
    }

    public AsdDataset getDataset() {
        return AsdDataset.builder()
                         .attributeGroups(AttributeGroups.buildAttributeGroups(model))
                         .classReferences(ClassReferences.buildClassReferences(model))
                         .selectInterfaces(SelectInterfaces.buildSelectInterfaces(model))
                         .extendInterfaces(ExtendInterfaces.buildExtendInterfaces(model))
                         .specializations(Specializations.buildSpecializations(model))
                         .build();
    }

    private static String orElse(String context,
                                 String value,
                                 String def) {
        if (StringUtils.isNullOrEmpty(value)) {
            LOGGER.error("Replaced invalid {} by {}", context, def);
            return def;
        } else {
            return value;
        }
    }

    /**
     * @param item The item.
     * @return The {@code item} name if its is <em>NOT</em> {@code null}, or a non {@code null} string.
     */
    private static String getName(MfNameItem item) {
        // TODO We could throw an exception in that case (generator option)
        return orElse("name (" + item.getId() + ")",
                      item.getName(),
                      TBD + item.getKind() + item.getId());
    }

    /**
     * @param type The type.
     * @return The name of the XML attribute/element used for {@code type}.
     */
    private static String getXmlName(MfType type) {
        return orElse("xmlName (" + type.getId() + ")",
                      type.wrap(AsdType.class).getXmlName(),
                      (TBD + getName(type)).toLowerCase());
    }

    /**
     * @param type The type.
     * @return The name of the XSD type / group generated for {@code type}.
     */
    private static String getXsdTypeName(MfType type) {
        final String name = getName(type);
        if (Specializations.isSpecializationRootClass(type)) {
            final AsdSpecialization.Kind kind = Specializations.getSpecializationKind((MfClass) type);
            if (kind == Kind.GROUP) {
                return toLowerCamelCase(name) + NON_ABSTRACT_CLASSES;
            } else {
                return toLowerCamelCase(name);
            }
        } else {
            return toLowerCamelCase(name);
        }
    }

    /**
     * @param type The type.
     * @return The name of the XML attribute/element used to reference a {@code type} XML element / group.
     */
    private static String getXmlRefName(MfType type) {
        return orElse("xmlRefName (" + type.getId() + ")",
                      type.wrap(AsdType.class).getXmlRefName(),
                      TBD.toLowerCase() + getXmlName(type) + REF);
    }

    /**
     * @param type The type.
     * @return The name of the XSD type generated to reference a {@code type}.
     */
    private static String getXsdTypeRefName(MfType type) {
        // return getTypeName(type) + REF;
        return toLowerCamelCase(getName(type) + REF);
    }

    /**
     * @param property The property.
     * @return The name of the XML attribute/element used for {@code property}.
     */
    private static String getXmlName(MfProperty property) {
        return orElse("xmlName (" + property.getId() + ")",
                      property.wrap(AsdProperty.class).getXmlName(),
                      (TBD + getName(property)).toLowerCase());
    }

    /**
     * @param property The property.
     * @return The name of the XSD type generated for {@code property}.
     */
    private static String getXsdTypeName(MfProperty property) {
        return toLowerCamelCase(getName(property));
    }

    private static String toLowerCamelCase(String name) {
        return Character.toLowerCase(name.charAt(0)) + name.substring(1);
    }

    static String getSource(MfNameItem element) {
        return "SX001G:" + getName(element);
    }

    // TODO: remove when official XSD Generator is fixed
    static String getLCCSource(MfNameItem element) {
        return "SX001G:" + toLowerCamelCase(getName(element));
    }

    private static String getDescription(MfElement element) {
        return element.wrap(AsdElement.class).getNotes();
    }

    /**
     * Support of UML attribute groups.
     */
    private static final class AttributeGroups {
        private AttributeGroups() {
        }

        private static boolean isAttributeGroup(MfClass cls) {
            return cls.wrap(AsdClass.class).is(AsdStereotypeName.ATTRIBUTE_GROUP);
        }

        public static List<AsdAttributeGroup> buildAttributeGroups(MfModel model) {
            final List<AsdAttributeGroup> list = new ArrayList<>();
            for (final MfClass in : model.collect(MfClass.class, AttributeGroups::isAttributeGroup)) {
                final String name = getXsdTypeName(in);
                final AsdAttributeGroup out =
                        AsdAttributeGroup.builder()
                                         .name(name)
                                         .source(getSource(in))
                                         .description(getDescription(in))
                                         // TODO
                                         .build();
                list.add(out);
            }
            // Sort
            Collections.sort(list, Comparator.comparing(x -> x.getName().replace(REF, "").toLowerCase()));
            return list;
        }
    }

    /**
     * Support of UML class references.
     */
    private static final class ClassReferences {
        private ClassReferences() {
        }

        private static boolean isClassReference(MfClass cls) {
            final AsdStereotypeName stereotypeName = cls.wrap(AsdClass.class).getStereotypeName();
            return (stereotypeName == AsdStereotypeName.CLASS
                    || stereotypeName == AsdStereotypeName.RELATIONSHIP)
                    && !cls.wrap(AsdClass.class).isPrimitiveOrganization();
        }

        public static List<AsdClassReference> buildClassReferences(MfModel model) {
            final List<AsdClassReference> list = new ArrayList<>();
            for (final MfClass in : model.collect(MfClass.class, ClassReferences::isClassReference)) {
                // in.wrap(AsdClass.class).getBusinessId().print(System.err, 0);
                final String name = getXsdTypeRefName(in);
                final AsdClassReference out =
                        AsdClassReference.builder()
                                         .name(name)
                                         .source(getLCCSource(in))
                                         .description(getDescription(in))
                                         .uidPattern(getUidPattern(in))
                                         .idName(getIdName(in))
                                         .relations(getRelations(in))
                                         .identifiers(getIdentifiers(in))
                                         .build();
                list.add(out);
            }
            // Sort
            Collections.sort(list, Comparator.comparing(x -> x.getName().replace(REF, "").toLowerCase()));
            return list;
        }

        private static String getUidPattern(MfClass cls) {
            return cls.wrap(AsdClass.class).getEffectiveUidPattern();
        }

        private static String getIdName(MfClass cls) {
            final List<AsdParticle> particles = getIdentifiers(cls);
            if (particles.isEmpty()) {
                return null;
            } else {
                return particles.get(0).getName();
            }
        }

        private static List<AsdParticle> getRelations(MfClass cls) {
            if (cls.wrap(AsdClass.class).getBusinessId() instanceof final AsdKeyBusinessId kbid) {
                final AsdSwitchBusinessId rwsbid = kbid.getRootWholeSwitchId().orElse(null);
                final List<AsdSwitchBusinessId> tsids = kbid.getTipSwitchIds();
                if (rwsbid == null && tsids.isEmpty()) {
                    return Collections.emptyList();
                } else {
                    final List<AsdParticle> list = new ArrayList<>();
                    if (rwsbid != null) {
                        list.addAll(rwsbid.getChoice()
                                          .stream()
                                          .map(ClassReferences::toParticle)
                                          .toList());
                    }
                    for (final AsdSwitchBusinessId tsid : tsids) {
                        list.addAll(tsid.getChoice()
                                        .stream()
                                        .map(ClassReferences::toParticle)
                                        .toList());
                    }
                    return list.stream()
                               .sorted(Comparator.comparing(p -> p.getType().replace(REF, "").toLowerCase()))
                               .toList();
                }
            } else {
                return Collections.emptyList();
            }
        }

        private static List<AsdParticle> getIdentifiers(MfClass cls) {
            if (cls.wrap(AsdClass.class).getBusinessId() instanceof final AsdKeyBusinessId kbid) {
                final ArrayList<AsdParticle> list = new ArrayList<>();
                list.addAll(kbid.getKeyTips()
                                .stream()
                                .map(ClassReferences::toParticle)
                                .toList());
                list.addAll(kbid.getAllKeyProperties()
                                .stream()
                                .map(ClassReferences::toParticle)
                                .toList());
                return list;
            } else {
                return Collections.emptyList();
            }
        }

        /**
         * @param property The property (should to be a key).
         * @return The conversion of {@code property} to a particle.
         *         It is an element with the type.
         */
        private static AsdParticle toParticle(MfProperty property) {
            final String name = getXmlName(property);
            final String type = getXsdTypeName(property);
            return AsdParticle.builder()
                              .kind(AsdParticle.Kind.ELEMENT)
                              .name(name)
                              .type(type)
                              .build();
        }

        /**
         * @param tip The tip.
         * @return The conversion of {@code tip} to a particle.
         *         It is either an element with a ref to the type
         *         or an element with the type.
         */
        private static AsdParticle toParticle(MfTip tip) {
            final MfType tipType = tip.getType();
            final String name;
            final String type;
            if (tipType instanceof MfClass) {
                name = getXmlRefName(tipType);
                type = getXsdTypeRefName(tipType);
            } else {
                name = getXmlName(tipType);
                type = getXsdTypeName(tipType);
            }

            return AsdParticle.builder()
                              .kind(AsdParticle.Kind.ELEMENT)
                              .name(name)
                              .type(type)
                              .build();
        }

        /**
         * @param type The class.
         * @return The conversion of {@code type} to a particle.
         *         It is an element with a ref to the type.
         */
        private static AsdParticle toParticle(MfClass type) {
            return AsdParticle.builder()
                              .kind(AsdParticle.Kind.ELEMENT)
                              .name(getXmlRefName(type))
                              .type(getXsdTypeRefName(type))
                              .build();
        }
    }

    /**
     * Support of UML select interface utility classes.
     */
    private static final class SelectInterfaces {
        private SelectInterfaces() {
        }

        private static boolean isSelectInterface(MfInterface xface) {
            return xface.wrap(AsdElement.class).isSelectInterface();
        }

        public static List<AsdSelectInterface> buildSelectInterfaces(MfModel model) {
            final List<AsdSelectInterface> list = new ArrayList<>();
            for (final MfInterface in : model.collect(MfInterface.class, SelectInterfaces::isSelectInterface)) {
                final String name = toLowerCamelCase(getName(in));
                final AsdSelectInterface out =
                        AsdSelectInterface.builder()
                                          .kind(getSelectInterfaceKind(in))
                                          .name(name)
                                          .source(getSource(in))
                                          .description(getDescription(in))
                                          .elements(getElements(in))
                                          .build();
                list.add(out);
            }
            // Sort
            // FIXME sort order. This currently works with CDM, but it must be by chance
            Collections.sort(list, Comparator.comparing(x -> x.getName().toLowerCase()));
            return list;
        }

        private static AsdSelectInterface.Kind getSelectInterfaceKind(MfInterface xface) {
            if (xface.wrap(AsdInterface.class).isProjectSpecificAttributeValue()) {
                return AsdSelectInterface.Kind.GROUP;
            } else {
                return AsdSelectInterface.Kind.COMPLEX_TYPE;
            }
        }

        private static List<AsdSelectInterfaceElement> getElements(MfInterface in) {
            if (in.wrap(AsdInterface.class).isProjectSpecificAttributeValue()) {
                // ProjectSpecificAttributeValue <<select>> interface has a special handling
                // TODO This should not be the case
                return getNonAbstractImplementations(in).stream()
                                                        .map(SelectInterfaces::toElement)
                                                        .sorted(Comparator.comparing(x -> x.getType().replace(REF, "")
                                                                                           .toLowerCase()))
                                                        .toList();
            } else {
                // ReportedIssueItem is implemented by BaseObject and should need a special handling
                return getDirectImplementations(in).stream()
                                                   .map(SelectInterfaces::toElementRef)
                                                   .sorted(Comparator.comparing(x -> x.getType().replace(REF, "")
                                                                                      .toLowerCase()))
                                                   .toList();
            }
        }

        private static AsdSelectInterfaceElement toElementRef(MfClass cls) {
            final AsdSelectInterfaceElement.Kind kind = AsdSelectInterfaceElement.Kind.ELEMENT;
            return AsdSelectInterfaceElement.builder()
                                            .kind(kind)
                                            .name(getXmlRefName(cls))
                                            .type(getXsdTypeRefName(cls))
                                            .build();
        }

        private static AsdSelectInterfaceElement toElement(MfClass cls) {
            // TODO It seems that Dimension is the only occurrence for projectSpecificAttributeValue
            // This does not seem necessary
            // Should also apply to PropertyType
            final AsdSelectInterfaceElement.Kind kind = AsdSelectInterfaceElement.Kind.ELEMENT;
            return AsdSelectInterfaceElement.builder()
                                            .kind(kind)
                                            .name(getXmlName(cls))
                                            .type(getXsdTypeName(cls))
                                            .build();
        }

        /**
         * @param xfce the interface.
         * @return The list of classes that directly implement {@code xfce}.<br>
         *         Special handling of BaseObject that is implicitly extended.
         */
        private static List<MfClass> getDirectImplementations(MfInterface xfce) {
            final Set<MfClass> imps = xfce.getDirectImplementations(MfClass.class);
            MfClass bo = null;
            for (final MfClass cls : imps) {
                if (cls.wrap(AsdClass.class).isBaseObject()) {
                    bo = cls;
                }
            }
            if (bo != null) {
                imps.remove(bo);
                // Add all classes that implicitly extend BaseObject
                imps.addAll(xfce.getModel().collect(MfClass.class,
                                                    x -> x.wrap(AsdElement.class).isBaseObjectImplicitExtension()));
            }

            return imps.stream().toList();
        }

        private static List<MfClass> getNonAbstractImplementations(MfInterface xfce) {
            return xfce.getAllImplementors()
                       .stream()
                       .filter(MfClass.class::isInstance)
                       .map(MfClass.class::cast)
                       .filter(Predicate.not(MfClass::isAbstract))
                       .toList();
        }

    }

    /**
     * Support of UML extend interface utility classes.
     */
    private static final class ExtendInterfaces {
        private ExtendInterfaces() {
        }

        public static List<AsdExtendInterface> buildExtendInterfaces(MfModel model) {
            final List<AsdExtendInterface> list = new ArrayList<>();
            for (final MfInterface in : model.collect(MfInterface.class, ExtendInterfaces::isExtendInterface)) {
                final String name = toLowerCamelCase(getName(in));
                final AsdExtendInterface out =
                        AsdExtendInterface.builder()
                                          .name(name)
                                          .source(getSource(in))
                                          .description(getDescription(in))
                                          .attributeGroups(getParticles(in, ExtendInterfaces::isAttributeGroup))
                                          .aggregations(getParticles(in, ExtendInterfaces::isAggregation))
                                          .compositions(getParticles(in, ExtendInterfaces::isComposition))
                                          .associations(getParticles(in, ExtendInterfaces::isAssociation))
                                          .build();
                list.add(out);
            }
            // Sort
            // FIXME sort order. This currently works with CDM, but it must be by chance
            Collections.sort(list, Comparator.comparing(x -> x.getName().toLowerCase()));
            return list;
        }

        /**
         * @param xface The interface.
         * @return {@code true} if {@code xface} is an extend interface.
         */
        private static boolean isExtendInterface(MfInterface xface) {
            return xface.wrap(AsdElement.class).isExtendInterface();
        }

        private static <T> T selectIfNotDef(T x,
                                            T def) {
            return def.equals(x) ? null : x;
        }

        private static List<AsdParticle> getParticles(MfType type,
                                                      Predicate<MfConnector> predicate) {
            // Get all connectors (local and inherited)
            return type.getAllConnectors()
                       .stream()
                       .filter(predicate)
                       .map(ExtendInterfaces::toParticle)
                       .sorted(Comparator.comparing(p -> p.getName().toLowerCase()))
                       .toList();
        }

        /**
         * @param connector The connector.
         * @return The conversion of {@code connector} to the corresponding particle.
         */
        private static AsdParticle toParticle(MfConnector connector) {
            final MfType targetType = connector.getTargetTip().getType();
            final MfCardinality targetCardinality = connector.getTargetTip().getEffectiveCardinality();
            final boolean isRef = isRef(connector);
            final boolean targetIsSpecializationRoot = Specializations.isSpecializationRootClass(targetType);
            final AsdParticle.Kind kind = targetIsSpecializationRoot ? AsdParticle.Kind.GROUP : AsdParticle.Kind.ELEMENT;

            final String name = isRef ? getXmlRefName(targetType) : getXmlName(targetType);
            final String type = isRef ? getXsdTypeRefName(targetType) : getXsdTypeName(targetType);
            final Boolean nillable = !isRef;
            // Must always be optional because exchange can be partial
            final Integer minOccurs = 0;
            final Integer maxOccurs = selectIfNotDef(targetCardinality.getMax(), 1);

            return AsdParticle.builder()
                              .kind(kind)
                              .name(name)
                              .type(type)
                              .nillable(nillable)
                              .minOccurs(minOccurs)
                              .maxOccurs(maxOccurs)
                              .build();
        }

        /**
         * @param connector The connector.
         * @return {@code true} if {@code connector} target is considered as a reference.
         */
        private static boolean isRef(MfConnector connector) {
            final int max = connector.getSourceTip().getEffectiveCardinality().getMax();
            return max > 1 || MfCardinality.isUnbounded(max);
        }

        private static boolean isAttributeGroup(MfConnector connector) {
            return connector.getTargetTip().getType().wrap(AsdClass.class).is(AsdStereotypeName.ATTRIBUTE_GROUP);
        }

        private static boolean isAggregation(MfConnector connector) {
            return connector instanceof MfAggregation
                    && !connector.getTargetTip().getType().wrap(AsdClass.class).is(AsdStereotypeName.ATTRIBUTE_GROUP);
        }

        private static boolean isComposition(MfConnector connector) {
            return connector instanceof MfComposition
                    && !connector.getTargetTip().getType().wrap(AsdClass.class).is(AsdStereotypeName.ATTRIBUTE_GROUP);
        }

        private static boolean isAssociation(MfConnector connector) {
            return connector instanceof MfAssociation
                    && !connector.getTargetTip().getType().wrap(AsdClass.class).is(AsdStereotypeName.ATTRIBUTE_GROUP);
        }

    }

    /**
     * Support of UML specialization classes.
     */
    private static final class Specializations {
        private Specializations() {
        }

        public static List<AsdSpecialization> buildSpecializations(MfModel model) {
            final List<AsdSpecialization> list = new ArrayList<>();
            for (final MfClass in : model.collect(MfClass.class, Specializations::isSpecializationRootClass)) {
                final AsdSpecialization.Kind kind = getSpecializationKind(in);
                final String name = switch (kind) {
                case COMPLEX_TYPE -> toLowerCamelCase(getName(in));
                case GROUP -> toLowerCamelCase(getName(in) + NON_ABSTRACT_CLASSES);
                };

                final AsdSpecialization out =
                        AsdSpecialization.builder()
                                         .kind(kind)
                                         .name(name)
                                         .source(getSource(in))
                                         .description(getDescription(in))
                                         .elements(getElements(in))
                                         .build();
                list.add(out);
            }
            // Sort specializations
            Collections.sort(list, Comparator.comparing(s -> s.getName().replace(NON_ABSTRACT_CLASSES, "")));

            return list;
        }

        private static List<AsdSpecializationElement> getElements(MfClass in) {
            // Note that sorting result is not 100% compliant with current XSD
            // But the order has no specification and has no semantic impact on generated XSD (choice)
            return getNonAbstractSpecializations(in).stream()
                                                    .map(Specializations::toElement)
                                                    .sorted(AsdSpecializationElement.DFS_COMPARATOR)
                                                    .toList();
        }

        private static AsdSpecializationElement toElement(MfClass in) {
            return AsdSpecializationElement.builder()
                                           .origin(in)
                                           .name(in.wrap(AsdClass.class).getXmlName())
                                           .type(toLowerCamelCase(getName(in)))
                                           .build();
        }

        /**
         * @param cls The class.
         * @return {@code true} when {@code cls} must be ignored, even if it is specialization root.
         *         This is the case with DateType which has special mapping to XSD.
         */
        private static boolean isIgnoredRoot(MfClass cls) {
            return "DateType".equals(cls.getName());
        }

        /**
         * @param type The type
         * @return {@code true} if {@code type} must be considered as a specialization root class.
         */
        private static boolean isSpecializationRootClass(MfType type) {
            // 1) It is extended by other classes
            // 2) it is not ignored
            // 3) if it is Organization, it is not the absolute root one (used as an attribute)
            if (type instanceof final MfClass cls) {
                return cls.hasSpecializations()
                        && !isIgnoredRoot(cls)
                        && !cls.wrap(AsdClass.class).isPrimitiveOrganization();
            } else {
                return false;
            }
        }

        private static List<MfClass> getNonAbstractSpecializations(MfClass cls) {
            return cls.getAllDescendants(Strictness.LOOSE, MfClass.class).stream()
                      .filter(Predicate.not(MfClass::isAbstract))
                      .toList();
        }

        private static AsdSpecialization.Kind getSpecializationKind(MfClass root) {
            // TODO this should NOT happen. We should generate a group for <<primitive>> classes
            if (root.wrap(AsdClass.class).is(AsdStereotypeName.PRIMITIVE)) {
                return AsdSpecialization.Kind.COMPLEX_TYPE;
            } else {
                return AsdSpecialization.Kind.GROUP;
            }
        }
    }
}