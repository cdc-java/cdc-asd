package cdc.asd.xsdgen.data;

import java.util.Collections;
import java.util.List;

import cdc.asd.model.ext.AsdBaseEnumType;
import cdc.asd.model.ext.AsdCrudCode;

/**
 * The tip level wrapper for ASD XSD Dataset.
 *
 * @author Damien Carbonne
 */
public interface AsdDataset {
    public List<AsdAttribute> getAttributes();

    public List<AsdAttributeGroup> getAttributeGroups();

    public List<AsdClassReference> getClassReferences();

    public List<AsdSelectInterface> getSelectInterfaces();

    public List<AsdExtendInterface> getExtendInterfaces();

    public List<AsdSpecialization> getSpecializations();

    public AsdBaseEnumType getCrudCode();

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private List<AsdAttribute> attributes = Collections.emptyList();
        private List<AsdAttributeGroup> attributeGroups = Collections.emptyList();
        private List<AsdClassReference> classReferences = Collections.emptyList();
        private List<AsdSelectInterface> selectInterfaces = Collections.emptyList();
        private List<AsdExtendInterface> extendInterfaces = Collections.emptyList();
        private List<AsdSpecialization> specializations;

        private Builder() {
        }

        public Builder attributes(List<AsdAttribute> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder attributeGroups(List<AsdAttributeGroup> attributeGroups) {
            this.attributeGroups = attributeGroups;
            return this;
        }

        public Builder classReferences(List<AsdClassReference> classReferences) {
            this.classReferences = classReferences;
            return this;
        }

        public Builder selectInterfaces(List<AsdSelectInterface> selectInterfaces) {
            this.selectInterfaces = selectInterfaces;
            return this;
        }

        public Builder extendInterfaces(List<AsdExtendInterface> extendInterfaces) {
            this.extendInterfaces = extendInterfaces;
            return this;
        }

        public Builder specializations(List<AsdSpecialization> specializations) {
            this.specializations = specializations;
            return this;
        }

        public AsdDataset build() {
            return new AsdDatasetImpl(attributes,
                                      attributeGroups,
                                      classReferences,
                                      selectInterfaces,
                                      extendInterfaces,
                                      specializations);
        }
    }
}

record AsdDatasetImpl(List<AsdAttribute> attributes,
                      List<AsdAttributeGroup> attributeGroups,
                      List<AsdClassReference> classReferences,
                      List<AsdSelectInterface> selectInterfaces,
                      List<AsdExtendInterface> extendInterfaces,
                      List<AsdSpecialization> specializations)
        implements AsdDataset {

    @Override
    public List<AsdAttribute> getAttributes() {
        return attributes;
    }

    @Override
    public List<AsdAttributeGroup> getAttributeGroups() {
        return attributeGroups;
    }

    @Override
    public List<AsdClassReference> getClassReferences() {
        return classReferences;
    }

    @Override
    public List<AsdSelectInterface> getSelectInterfaces() {
        return selectInterfaces;
    }

    @Override
    public List<AsdExtendInterface> getExtendInterfaces() {
        return extendInterfaces;
    }

    @Override
    public List<AsdSpecialization> getSpecializations() {
        return specializations;
    }

    @Override
    public AsdBaseEnumType getCrudCode() {
        return AsdCrudCode.INSTANCE;
    }
}