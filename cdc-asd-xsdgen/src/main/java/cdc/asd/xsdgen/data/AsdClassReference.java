package cdc.asd.xsdgen.data;

import java.util.Collections;
import java.util.List;

public final class AsdClassReference extends AsdBase {
    private final String uidPattern;
    private final String idName;
    private final List<AsdParticle> relations;
    private final List<AsdParticle> identifiers;

    private AsdClassReference(Builder builder) {
        super(builder);
        this.uidPattern = builder.uidPattern;
        this.idName = builder.idName;
        this.relations = builder.relations;
        this.identifiers = builder.identifiers;
    }

    public String getUidPattern() {
        return uidPattern;
    }

    public String getIdName() {
        return idName;
    }

    public List<AsdParticle> getRelations() {
        return relations;
    }

    public List<AsdParticle> getIdentifiers() {
        return identifiers;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdBase.Builder<Builder> {
        private String uidPattern;
        private String idName;
        private List<AsdParticle> relations = Collections.emptyList();
        private List<AsdParticle> identifiers = Collections.emptyList();

        private Builder() {
        }

        public Builder uidPattern(String uidPattern) {
            this.uidPattern = uidPattern;
            return this;
        }

        public Builder idName(String idName) {
            this.idName = idName;
            return this;
        }

        public Builder relations(List<AsdParticle> relations) {
            this.relations = relations;
            return this;
        }

        public Builder identifiers(List<AsdParticle> identifiers) {
            this.identifiers = identifiers;
            return this;
        }

        @Override
        public AsdClassReference build() {
            return new AsdClassReference(this);
        }
    }
}