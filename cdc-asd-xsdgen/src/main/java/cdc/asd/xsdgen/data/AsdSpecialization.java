package cdc.asd.xsdgen.data;

import java.util.Collections;
import java.util.List;

import cdc.util.lang.Checks;

/**
 * UML specialization utility class.
 * <p>
 * It can either be a complex type or a group. It contains a choice of elements.
 */
public final class AsdSpecialization extends AsdBase {
    private final Kind kind;
    private final List<AsdSpecializationElement> elements;

    private AsdSpecialization(Builder builder) {
        super(builder);
        this.kind = Checks.isNotNull(builder.kind, "kind");
        this.elements = Checks.isNotNull(builder.elements, "elements");
    }

    public enum Kind {
        GROUP,
        COMPLEX_TYPE
    }

    public Kind getKind() {
        return kind;
    }

    public boolean isGroup() {
        return getKind() == Kind.GROUP;
    }

    public boolean isComplexType() {
        return getKind() == Kind.COMPLEX_TYPE;
    }

    public List<AsdSpecializationElement> getElements() {
        return elements;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdBase.Builder<Builder> {
        private Kind kind;
        private List<AsdSpecializationElement> elements = Collections.emptyList();

        private Builder() {
        }

        public Builder kind(Kind kind) {
            this.kind = kind;
            return this;
        }

        public Builder elements(List<AsdSpecializationElement> elements) {
            this.elements = elements;
            return this;
        }

        @Override
        public AsdSpecialization build() {
            return new AsdSpecialization(this);
        }
    }
}