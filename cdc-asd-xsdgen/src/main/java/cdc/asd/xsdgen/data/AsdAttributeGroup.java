package cdc.asd.xsdgen.data;

public final class AsdAttributeGroup extends AsdBase {
    private AsdAttributeGroup(Builder builder) {
        super(builder);
    }

    // TODO

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdBase.Builder<Builder> {

        @Override
        public AsdAttributeGroup build() {
            return new AsdAttributeGroup(this);
        }
    }
}