package cdc.asd.xsdgen.data;

import cdc.util.lang.Checks;

public interface AsdInclude extends AsdDep {
    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder extends AsdDep.Builder<Builder> {
        @Override
        public AsdInclude build() {
            return new AsdIncludeImpl(schemaLocation);
        }
    }
}

record AsdIncludeImpl(String schemaLocation) implements AsdInclude {
    AsdIncludeImpl {
        Checks.isNotNull(schemaLocation, "schemaLocations");
    }

    @Override
    public String getSchemaLocation() {
        return schemaLocation;
    }
}