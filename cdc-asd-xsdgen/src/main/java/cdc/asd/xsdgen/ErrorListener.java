package cdc.asd.xsdgen;

import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.STErrorListener;
import org.stringtemplate.v4.misc.STMessage;

final class ErrorListener implements STErrorListener {
    private final Logger logger;

    ErrorListener(Logger logger) {
        this.logger = logger;
    }

    @Override
    public void compileTimeError(STMessage msg) {
        logger.error("Compile time error {}", msg);
    }

    @Override
    public void runTimeError(STMessage msg) {
        logger.error("Run time error {}", msg);
    }

    @Override
    public void IOError(STMessage msg) {
        logger.error("IO error {}", msg);
    }

    @Override
    public void internalError(STMessage msg) {
        logger.error("Internal error {}", msg);
    }
}