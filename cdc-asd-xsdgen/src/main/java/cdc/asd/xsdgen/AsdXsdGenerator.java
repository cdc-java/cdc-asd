package cdc.asd.xsdgen;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;
import org.stringtemplate.v4.STGroupDir;

import cdc.asd.model.ext.AsdEnumType;
import cdc.asd.model.ext.AsdEnumTypeIo;
import cdc.asd.model.ext.AsdEnumTypeSet;
import cdc.asd.model.ext.AsdModelToExt;
import cdc.asd.xsdgen.data.AsdDataset;
import cdc.asd.xsdgen.data.AsdDep;
import cdc.asd.xsdgen.data.AsdImport;
import cdc.asd.xsdgen.data.AsdInclude;
import cdc.asd.xsdgen.data.AsdModelToDataset;
import cdc.util.files.Resources;
import cdc.util.time.Chronometer;

/**
 * Class used to generate XSD using ST4.
 */
public class AsdXsdGenerator {
    private final Logger logger;
    private final AsdXsdGenerationArgs args;

    public AsdXsdGenerator(AsdXsdGenerationArgs args) {
        this.logger = LogManager.getLogger(getClass());
        this.args = args;
    }

    public void generate() throws IOException, ExecutionException {
        final URL templates = Resources.getResource("cdc/asd/xsdgen/templates");
        final STGroupDir stg = new STGroupDir(templates);

        stg.registerRenderer(String.class, new AsdStringRenderer());
        stg.registerRenderer(Integer.class, new AsdIntegerRenderer());

        final List<STWorker> tasks = new ArrayList<>();

        // valid values
        addModelEnumTypeSet(stg,
                            tasks);

        // valid values units
        addEnumTypeSet(stg,
                       tasks,
                       args.getValidValuesUnits(),
                       List.of(AsdInclude.builder()
                                         .schemaLocation(AsdXsdParams.Files.getValidValuesExternalLibrariesFile(args))
                                         .build()),
                       AsdXsdParams.Files.getValidValuesUnitsFile(args));

        // project specific exchange definitions
        addBasic(stg,
                 tasks,
                 "asd-xsd/generateProjectSpecificExchangeDefinitions",
                 AsdXsdParams.Files.getProjectSpecificExchangeDefinitionsFile(args));

        // project specific exchanges
        addBasic(stg,
                 tasks,
                 "asd-xsd/generateProjectSpecificExchanges",
                 AsdXsdParams.Files.getProjectSpecificExchangesFile(args));

        // project valid values project extensions
        addBasic(stg,
                 tasks,
                 "asd-xsd/generateValidValuesProjectExtensions",
                 AsdXsdParams.Files.getValidValuesProjectExtensionsFile(args));

        // valid values libraries
        addEnumTypeSet(stg,
                       tasks,
                       args.getValidValuesLibraries(),
                       Collections.emptyList(),
                       AsdXsdParams.Files.getValidValuesLibrariesFile(args));

        // valid values external libraries
        addEnumTypeSet(stg,
                       tasks,
                       args.getValidValuesExternalLibraries(),
                       Collections.emptyList(),
                       AsdXsdParams.Files.getValidValuesExternalLibrariesFile(args));

        // dataset
        addDataSet(stg,
                   tasks);

        invokeTasks("XSD generation", tasks);
    }

    /**
     * Creates an STWorker to invoke an basic template (1 argument: ARGS).
     *
     * @param stg The STGroup.
     * @param tasks The list of STWorkers.
     * @param templateName The ST4 template name.
     * @param file The file to generate.
     */
    private void addBasic(STGroup stg,
                          List<STWorker> tasks,
                          String templateName,
                          File file) {
        log("addBasic({})", templateName);
        final ST st = stg.getInstanceOf(templateName);
        st.add(AsdXsdParams.Names.ARGS, args);

        tasks.add(new STWorker(st, file));
    }

    /**
     * Creates an STWorker to invoke the EnumTypeSet generation template (2 arguments: ARGS, SET).
     *
     * @param stg The STGroup.
     * @param tasks The list of STWorkers.
     * @param url The URL of the enum type set to load.
     * @param deps The dependencies.
     * @param file The file to generate.
     * @throws IOException When an IO error occurs.
     */
    private void addEnumTypeSet(STGroup stg,
                                List<STWorker> tasks,
                                URL url,
                                List<AsdDep> deps,
                                File file) throws IOException {
        log("addEnumTypeSet({})", url);
        log("   Loading {}", url);
        final AsdEnumTypeSet set = AsdEnumTypeIo.loadEnumTypeSet(url,
                                                                 args.getSorting());

        log("      types: {}", set.getTypes().stream().map(AsdEnumType::getName).toList());
        final ST st = stg.getInstanceOf("asd-xsd/generateEnumTypeSet");
        st.add(AsdXsdParams.Names.ARGS, args);
        st.add(AsdXsdParams.Names.SET, set);
        st.add(AsdXsdParams.Names.DEPS, deps);

        tasks.add(new STWorker(st, file));
    }

    /**
     * Create an STWorker to generate valid values file (extracted from UML model).
     *
     * @param stg The STGroup.
     * @param tasks The list of STWorkers.
     */
    private void addModelEnumTypeSet(STGroup stg,
                                     List<STWorker> tasks) {
        log("addModelEnumTypeSet()");
        final AsdModelToExt modelToExt = AsdModelToExt.builder()
                                                      .model(args.getModel())
                                                      .copyright(args.getCopyright())
                                                      .build();
        final AsdEnumTypeSet set = modelToExt.getModelEnumTypeSet();
        final List<AsdDep> deps = new ArrayList<>();
        deps.add(AsdInclude.builder()
                           .schemaLocation(AsdXsdParams.Files.getValidValuesExternalLibrariesFile(args))
                           .build());
        deps.add(AsdInclude.builder()
                           .schemaLocation(AsdXsdParams.Files.getValidValuesLibrariesFile(args))
                           .build());
        deps.add(AsdInclude.builder()
                           .schemaLocation(AsdXsdParams.Files.getValidValuesUnitsFile(args))
                           .build());
        deps.add(AsdInclude.builder()
                           .schemaLocation(AsdXsdParams.Files.getValidValuesProjectExtensionsFile(args))
                           .build());

        final ST st = stg.getInstanceOf("asd-xsd/generateEnumTypeSet");
        st.add(AsdXsdParams.Names.ARGS, args);
        st.add(AsdXsdParams.Names.SET, set);
        st.add(AsdXsdParams.Names.DEPS, deps);

        tasks.add(new STWorker(st, AsdXsdParams.Files.getValidValuesFile(args)));
    }

    private void addDataSet(STGroup stg,
                            List<STWorker> tasks) {
        log("addDataSet()");
        final AsdModelToDataset modelToDataSet = new AsdModelToDataset(args.getModel());
        final AsdDataset dateset = modelToDataSet.getDataset();
        final List<AsdDep> deps = new ArrayList<>();
        deps.add(AsdImport.builder()
                          .schemaLocation(AsdXsdParams.Files.getValidValuesFile(args))
                          .namespace(args.getValidValuesNamespace())
                          .build());
        deps.add(AsdInclude.builder()
                           .schemaLocation(AsdXsdParams.Files.getProjectSpecificExchangesFile(args))
                           .build());

        final ST st = stg.getInstanceOf("asd-xsd/generateDataset");
        st.add(AsdXsdParams.Names.ARGS, args);
        st.add(AsdXsdParams.Names.DATASET, dateset);
        st.add(AsdXsdParams.Names.DEPS, deps);

        tasks.add(new STWorker(st, AsdXsdParams.Files.getDatasetFile(args)));
    }

    private void log(String message,
                     Object... params) {
        if (args.getHints().contains(AsdXsdGenerationHint.VERBOSE)) {
            logger.info(message, params);
        }
    }

    private class STWorker implements Callable<File> {
        final ST st;
        final File relativePath;

        protected STWorker(ST st,
                           File relativePath) {
            this.st = st;
            this.relativePath = relativePath;
        }

        @Override
        public final File call() throws IOException {
            return save(st, relativePath);
        }

        private File save(ST st,
                          File relativePath) throws IOException {
            final Chronometer chrono = new Chronometer();
            final File targetFile = new File(args.getOutputDir(), relativePath.getPath()).getCanonicalFile();
            targetFile.getParentFile().mkdirs();
            log("Generating {}", targetFile);
            chrono.start();
            st.write(targetFile, new ErrorListener(logger), "UTF-8", null, -1);
            chrono.suspend();
            log("Generated {} ({})", targetFile, chrono);
            return targetFile;
        }
    }

    private <T> List<T> invokeTasks(String title,
                                    List<? extends Callable<T>> tasks,
                                    boolean parallel) throws ExecutionException {
        log("{}: invoking {} {}", title, tasks.size(), parallel ? "parallel tasks" : "sequential tasks");
        final List<T> list = new ArrayList<>();
        final Chronometer chrono = new Chronometer();
        chrono.start();
        final int threads = parallel ? Runtime.getRuntime().availableProcessors() * 4 : 1;
        final ExecutorService service = Executors.newFixedThreadPool(threads);
        final List<Future<T>> futures = new ArrayList<>();
        for (final Callable<T> task : tasks) {
            futures.add(service.submit(task));
        }
        for (final Future<T> future : futures) {
            try {
                list.add(future.get());
            } catch (final InterruptedException e) {
                Thread.currentThread().interrupt();
            } catch (final ExecutionException e) {
                logger.catching(e);
                throw e;
            }
        }
        service.shutdown();
        chrono.suspend();
        log("{}: invoked {} {} ({})", title, tasks.size(), parallel ? "parallel tasks" : "sequential tasks", chrono);
        return list;
    }

    private <T> List<T> invokeTasks(String title,
                                    List<? extends Callable<T>> tasks) throws ExecutionException {
        return invokeTasks(title,
                           tasks,
                           args.getHints().contains(AsdXsdGenerationHint.PARALLEL));
    }
}