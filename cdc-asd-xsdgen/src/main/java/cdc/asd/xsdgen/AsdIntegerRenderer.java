package cdc.asd.xsdgen;

import java.util.Locale;

import org.stringtemplate.v4.AttributeRenderer;

import cdc.mf.model.MfCardinality;

public class AsdIntegerRenderer implements AttributeRenderer<Integer> {
    private static String boundToString(int bound) {
        if (bound >= 0) {
            return Integer.toString(bound);
        } else if (bound == MfCardinality.UNBOUNDED) {
            return "unbounded";
        } else {
            return "error";
        }
    }

    @Override
    public String toString(Integer value,
                           String formatString,
                           Locale locale) {
        if (formatString == null) {
            return Integer.toString(value);
        } else {
            return switch (formatString) {
            case "bound" -> boundToString(value);
            default -> Integer.toString(value);
            };
        }
    }
}