package cdc.asd.xsdgen;

/**
 * Enumeration of hints used for XSD generation.
 */
public enum AsdXsdGenerationHint {
    /** Use threads. */
    PARALLEL,

    SORT_VALUES,

    SORT_DESCRIPTIONS,

    /** Be verbose. */
    VERBOSE
}