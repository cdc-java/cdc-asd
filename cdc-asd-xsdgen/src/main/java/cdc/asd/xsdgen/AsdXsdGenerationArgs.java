package cdc.asd.xsdgen;

import java.io.File;
import java.net.URL;
import java.util.EnumSet;
import java.util.Set;

import cdc.asd.model.Config;
import cdc.asd.model.ext.AsdEnumValueSorting;
import cdc.mf.model.MfModel;
import cdc.util.lang.Checks;

/**
 * Arguments to configure XSD generation.
 */
public final class AsdXsdGenerationArgs {
    /** The directory containing generated files. */
    private final File outputDir;
    /** The generation boolean hints. */
    private final Set<AsdXsdGenerationHint> hints;

    private final MfModel model;
    private final File eaFile;

    private final String namespace;

    private final String specification;
    private final String specificationUrl;
    private final String issueNumber;
    private final String issueDate;
    private final String xmlSchemaReleaseNumber;
    private final String xmlSchemaReleaseDate;

    private final String copyright;

    private final URL validValuesUnits;
    private final URL validValuesLibraries;
    private final URL validValuesExternalLibraries;

    private AsdXsdGenerationArgs(Builder builder) {
        this.outputDir = Checks.isNotNull(builder.outputDir, "outputDir");
        this.hints = builder.hints;

        this.model = Checks.isNotNull(builder.model, "model");
        this.eaFile = Checks.isNotNull(builder.eaFile, "eaFile");

        this.namespace = Checks.isNotNull(builder.namespace, "namespace");

        this.specification = builder.specification;
        this.specificationUrl = builder.specificationUrl;
        this.issueNumber = builder.issueNumber;
        this.issueDate = builder.issueDate;
        this.xmlSchemaReleaseNumber = builder.xmlSchemaReleaseNumber;
        this.xmlSchemaReleaseDate = builder.xmlSchemaReleaseDate;

        this.copyright = builder.copyright;

        this.validValuesUnits = Checks.isNotNull(builder.validValuesUnits, "validValuesUnits");
        this.validValuesLibraries = Checks.isNotNull(builder.validValuesLibraries, "validValuesLibraries");
        this.validValuesExternalLibraries = Checks.isNotNull(builder.validValuesExternalLibraries, "validValuesExternalLibraries");
    }

    public static String getGenerator() {
        return AsdXsdGenerator.class.getSimpleName();
    }

    public static String getGeneratorVersion() {
        return Config.VERSION;
    }

    public File getOutputDir() {
        return outputDir;
    }

    public Set<AsdXsdGenerationHint> getHints() {
        return hints;
    }

    public MfModel getModel() {
        return model;
    }

    public File getEaFile() {
        return eaFile;
    }

    public String getNamespace() {
        return namespace;
    }

    public String getValidValuesNamespace() {
        return namespace + "/validValues";
    }

    public String getSpecification() {
        return specification;
    }

    public String getSpecificationUrl() {
        return specificationUrl;
    }

    public String getIssueNumber() {
        return issueNumber;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public String getXmlSchemaReleaseNumber() {
        return xmlSchemaReleaseNumber;
    }

    public String getXmlSchemaReleaseDate() {
        return xmlSchemaReleaseDate;
    }

    public String getCopyright() {
        return copyright;
    }

    public URL getValidValuesUnits() {
        return validValuesUnits;
    }

    public URL getValidValuesLibraries() {
        return validValuesLibraries;
    }

    public URL getValidValuesExternalLibraries() {
        return validValuesExternalLibraries;
    }

    public AsdEnumValueSorting getSorting() {
        if (hints.contains(AsdXsdGenerationHint.SORT_DESCRIPTIONS)) {
            return AsdEnumValueSorting.DESCRIPTION;
        } else if (hints.contains(AsdXsdGenerationHint.SORT_VALUES)) {
            return AsdEnumValueSorting.VALUE;
        } else {
            return AsdEnumValueSorting.NONE;
        }
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private File outputDir;
        private final Set<AsdXsdGenerationHint> hints = EnumSet.noneOf(AsdXsdGenerationHint.class);

        private MfModel model;
        private File eaFile;

        private String namespace;

        private String specification = "???";
        private String specificationUrl = "???";
        private String issueNumber = "???";
        private String issueDate = "???";
        private String xmlSchemaReleaseNumber = "???";
        private String xmlSchemaReleaseDate = "???";

        private String copyright;

        private URL validValuesUnits;
        private URL validValuesLibraries;
        private URL validValuesExternalLibraries;

        protected Builder() {
        }

        public Builder outputDir(File outputDir) {
            this.outputDir = outputDir;
            return this;
        }

        public Builder hint(AsdXsdGenerationHint hint) {
            this.hints.add(hint);
            return this;
        }

        public Builder hint(AsdXsdGenerationHint hint,
                            boolean enabled) {
            if (enabled) {
                this.hints.add(hint);
            } else {
                this.hints.remove(hint);
            }
            return this;
        }

        public Builder model(MfModel model) {
            this.model = model;
            return this;
        }

        public Builder eaFile(File eaFile) {
            this.eaFile = eaFile;
            return this;
        }

        public Builder namespace(String namespace) {
            this.namespace = namespace;
            return this;
        }

        public Builder specification(String specification) {
            this.specification = specification;
            return this;
        }

        public Builder specificationUrl(String specificationUrl) {
            this.specificationUrl = specificationUrl;
            return this;
        }

        public Builder issueNumber(String issueNumber) {
            this.issueNumber = issueNumber;
            return this;
        }

        public Builder issueDate(String issueDate) {
            this.issueDate = issueDate;
            return this;
        }

        public Builder xmlSchemaReleaseNumber(String xmlSchemaReleaseNumber) {
            this.xmlSchemaReleaseNumber = xmlSchemaReleaseNumber;
            return this;
        }

        public Builder xmlSchemaReleaseDate(String xmlSchemaReleaseDate) {
            this.xmlSchemaReleaseDate = xmlSchemaReleaseDate;
            return this;
        }

        public Builder copyright(String copyright) {
            this.copyright = copyright;
            return this;
        }

        public Builder validValuesUnits(URL validValuesUnits) {
            this.validValuesUnits = validValuesUnits;
            return this;
        }

        public Builder validValuesLibraries(URL validValuesLibraries) {
            this.validValuesLibraries = validValuesLibraries;
            return this;
        }

        public Builder validValuesExternalLibraries(URL validValuesExternalLibraries) {
            this.validValuesExternalLibraries = validValuesExternalLibraries;
            return this;
        }

        public AsdXsdGenerationArgs build() {
            return new AsdXsdGenerationArgs(this);
        }
    }
}